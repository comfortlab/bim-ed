﻿using System.Threading.Tasks;
using Autodesk.Revit.UI;
using CoLa.BimEd.Infrastructure.Revit.Async.Extensions;
using CoLa.BimEd.Infrastructure.Revit.Async.Interfaces;

namespace CoLa.BimEd.Infrastructure.Revit.Async.ExternalEvents;

public abstract class AsyncExternalEventHandler<TRequest, TResponse> : ExternalEventHandler<TRequest, TResponse>
{
    protected sealed override void Execute( 
        UIApplication app,
        TRequest request,
        IExternalEventResponseHandler<TResponse> responseHandler)
    {
        responseHandler.Await(Handle(app, request), responseHandler.SetResult);
    }

    protected abstract Task<TResponse> Handle(UIApplication app, TRequest request);
}