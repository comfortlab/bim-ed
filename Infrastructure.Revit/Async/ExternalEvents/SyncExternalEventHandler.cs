﻿using Autodesk.Revit.UI;
using CoLa.BimEd.Infrastructure.Revit.Async.Extensions;
using CoLa.BimEd.Infrastructure.Revit.Async.Interfaces;

namespace CoLa.BimEd.Infrastructure.Revit.Async.ExternalEvents;

public abstract class SyncExternalEventHandler<TRequest, TResponse> : ExternalEventHandler<TRequest, TResponse>
{
    protected sealed override void Execute(
        UIApplication app,
        TRequest request,
        IExternalEventResponseHandler<TResponse> responseHandler)
    {
        responseHandler.Wait(() => Handle(app, request));
    }
        
    protected abstract TResponse Handle(UIApplication app, TRequest request);
}