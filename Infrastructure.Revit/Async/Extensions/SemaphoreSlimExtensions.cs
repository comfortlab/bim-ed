﻿using System.Threading;
using System.Threading.Tasks;

namespace CoLa.BimEd.Infrastructure.Revit.Async.Extensions;

public static class SemaphoreSlimExtensions
{
    public static Task AsyncWait(this SemaphoreSlim semaphore)
    {
        return semaphore.WaitAsync();
    }
}