﻿using System.Threading;
using System.Threading.Tasks;
using CoLa.BimEd.Infrastructure.Revit.Async.Extensions;

namespace CoLa.BimEd.Infrastructure.Revit.Async.Utils;

public class AsyncLocker
{
    public AsyncLocker()
    {
        Semaphore = new SemaphoreSlim(1, 1);
    }
        
    private SemaphoreSlim Semaphore { get; }

    public Task<UnlockKey> LockAsync()
    {
        var waitTask = Semaphore.AsyncWait();
        return waitTask.IsCompleted
            ? TaskUtils.FromResult(new UnlockKey(this))
            : waitTask.ContinueWith(task => new UnlockKey(this),
                CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
    }

    internal void Release()
    {
        Semaphore.Release();
    }
}