﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.Comparer;

namespace CoLa.BimEd.Infrastructure.Revit.Comparers;

public class ElementIdComparer : IComparer<ElementId>
{
    public int Compare(ElementId x, ElementId y) =>
        new IntComparer().Compare(
            x?.IntegerValue ?? -1,
            y?.IntegerValue ?? -1);
}