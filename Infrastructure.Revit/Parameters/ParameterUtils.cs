﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Parameters;

public static class ParameterUtils
{
    public static bool IsValidParameter(this Parameter parameter) =>
        parameter is { HasValue: true };

    public static bool IsValidParameters(IEnumerable<Parameter> parameters) =>
        parameters.All(parameter => parameter.IsValidParameter());

    public static Parameter GetParameter(this Element element, Guid guid)
    {
        switch (element)
        {
            case FamilyInstance familyInstance:
                return GetParameter(familyInstance, guid);

            default:
                return element.get_Parameter(guid);
        }
    }

    public static Parameter GetParameter(this FamilyInstance familyInstance, Guid guid)
    {
        var familyInstanceParameter = familyInstance.get_Parameter(guid);
        var symbolParameter = familyInstance.Symbol.get_Parameter(guid);

        if (symbolParameter == null)
            return familyInstanceParameter;

        else if (symbolParameter.IsReadOnly)
            return familyInstanceParameter ?? symbolParameter;

        else
            return symbolParameter;
    }

    public static Parameter GetInstanceParameter(FamilyInstance familyInstance, Guid guid, string alternativeName = null)
    {
        if (familyInstance == null)
            return null;

        return familyInstance.get_Parameter(guid) ??
               (string.IsNullOrEmpty(alternativeName) == false
                   ? familyInstance.LookupParameter(alternativeName)
                   : null);
    }

    public static Parameter GetSymbolOrInstanceParameter(FamilyInstance familyInstance, Guid guid, string alternativeName = null)
    {
        if (familyInstance == null)
            return null;

        var familySymbol = familyInstance.Symbol;

        return familySymbol.get_Parameter(guid) ??
               familyInstance.get_Parameter(guid) ??
               (string.IsNullOrEmpty(alternativeName) == false
                   ? familySymbol.LookupParameter(alternativeName) ??
                     familyInstance.LookupParameter(alternativeName)
                   : null);
    }

    public static Parameter GetSymbolOrInstanceParameter(FamilyInstance familyInstance, string parameterName)
    {
        if (familyInstance == null)
            return null;

        var familySymbol = familyInstance.Symbol;

        return familySymbol.LookupParameter(parameterName) ??
               familyInstance.LookupParameter(parameterName);
    }

    public static double? GetValueAsDouble(this Parameter parameter)
    {
        if (parameter is not { HasValue: true })
            return null;

        return parameter.StorageType switch
        {
            StorageType.Double => parameter.AsDouble(),
            StorageType.Integer => parameter.AsInteger(),
            StorageType.ElementId => parameter.AsInteger(),
            StorageType.String => parameter.AsString().FirstDouble(),
            _ => throw new ArgumentOutOfRangeException($"{parameter.StorageType}")
        };
    }

    public static int? GetValueAsInt(this Parameter parameter)
    {
        if (parameter is not { HasValue: true })
            return null;

        return parameter.StorageType switch
        {
            StorageType.Integer => parameter.AsInteger(),
            StorageType.ElementId => parameter.AsInteger(),
            StorageType.String => parameter.AsString().FirstInt(),
            StorageType.Double => (int)Math.Round(parameter.AsDouble()),
            _ => throw new ArgumentOutOfRangeException($"{parameter.StorageType}")
        };
    }

    public static string GetValueAsString(this Parameter parameter)
    {
        if (parameter is not { HasValue: true })
            return null;

        return parameter.StorageType switch
        {
            StorageType.Double => parameter.AsValueString(),
            StorageType.Integer => parameter.AsInteger().ToString(),
            StorageType.ElementId => parameter.AsElementId().ToString(),
            StorageType.String => parameter.AsString(),
            _ => throw new ArgumentOutOfRangeException($"{parameter.StorageType}")
        };
    }

    public static ElementId GetValueAsElementId(this Parameter parameter)
    {
        if (parameter is not { HasValue: true })
            return ElementId.InvalidElementId;

        return parameter.StorageType switch
        {
            StorageType.Integer => new ElementId(parameter.AsInteger()),
            StorageType.ElementId => parameter.AsElementId(),
            StorageType.String => new ElementId(parameter.AsString().FirstInt(defaultValue: 0)),
            StorageType.Double => new ElementId((int)Math.Round(parameter.AsDouble())),
            _ => throw new ArgumentOutOfRangeException($"{parameter.StorageType}")
        };
    }

    public static Parameter GetSimilarParameter(Element element, Parameter sourceParameter)
    {
        if (sourceParameter.IsShared)
        {
            var guid = sourceParameter.GUID;
            return element.get_Parameter(guid);
        }

        var sourceInternalDefinition = (InternalDefinition)sourceParameter.Definition;
        if (sourceInternalDefinition != null &&
            sourceInternalDefinition.BuiltInParameter != BuiltInParameter.INVALID)
        {
            return element.get_Parameter(sourceInternalDefinition.BuiltInParameter);
        }

        var sourceParameterName = sourceParameter.Definition.Name;
        return element.LookupParameter(sourceParameterName);
    }

    public static void SetParameter(this Element element, BuiltInParameter builtInParameter, object value)
    {
        if (element == null) throw new NullReferenceException($"Element is NULL.");

        var parameter = element.get_Parameter(builtInParameter);

        if (parameter == null)
        {
            DI.Get<Logger>().Warn($"Parameter '{builtInParameter}' is not found in Element '[{element.Id}] {element.Name}'.");
            return;
        }

        SetParameterValue(element, parameter, value);
    }

    public static void SetParameter(this Element element, Guid parameterGuid, object value)
    {
        if (element == null) throw new NullReferenceException($"Element is NULL.");

        var parameter = element.get_Parameter(parameterGuid);

        if (parameter == null)
        {
            DI.Get<Logger>().Warn($"Parameter '{parameterGuid}' is not found in Element '[{element.Id}] {element.Name}'.");
            return;
        }

        SetParameterValue(element, parameter, value);
    }

    public static void SetParameter(this Element element, string parameterName, object value)
    {
        if (element == null) throw new NullReferenceException($"Element is NULL.");

        var parameter = element.LookupParameter(parameterName);

        if (parameter == null)
        {
            DI.Get<Logger>().Warn($"Parameter '{parameterName}' is not found in Element '[{element.Id}] {element.Name}'.");
            return;
        }

        SetParameterValue(element, parameter, value);
    }

    private static void SetParameterValue(Element element, Parameter parameter, object value)
    {
        if (parameter.IsReadOnly)
        {
            DI.Get<Logger>().Warn($"Parameter '[{parameter.Id} / {parameter.GUID}] {parameter.Definition.Name}' of Element '[{element.Id}] {element.Name}' is read only.");
            return;
        }

        SetValue(parameter, value);
    }

    private static void SetValue(Parameter parameter, object value)
    {
        switch (parameter.StorageType)
        {
            case StorageType.Integer when value is bool @bool:
                parameter.Set(@bool ? 1 : 0);
                return;

            case StorageType.Integer when value is int @int:
                parameter.Set(@int);
                return;

            case StorageType.Integer when value is double @double:
                parameter.Set((int)@double);
                return;

            case StorageType.Integer when value is string @string:
                parameter.Set(@string.FirstInt(defaultValue: 0));
                return;

            case StorageType.Double when value is double @double:
                parameter.Set(@double);
                return;

            case StorageType.Double when value is int @int:
                parameter.Set(@int);
                return;

            case StorageType.Double when value is decimal @decimal:
                parameter.Set((double)@decimal);
                return;

            case StorageType.String:
                parameter.Set(value?.ToString());
                return;

            case StorageType.ElementId when value is ElementId elementId:
                parameter.Set(elementId);
                return;

            case StorageType.ElementId when value is int @int:
                parameter.Set(new ElementId(@int));
                return;
        }
    }
}