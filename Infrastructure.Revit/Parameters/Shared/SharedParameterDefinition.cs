﻿// ReSharper disable LocalizableElement

using System;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

public class SharedParameterDefinition
{
    public SharedParameterDefinition(
        Guid guid,
        BuiltInParameterGroup builtInParameterGroup = BuiltInParameterGroup.INVALID,
        bool isInstance = false)
    {
        Guid = guid;
        BuiltInParameterGroup = builtInParameterGroup;
        IsInstance = isInstance;
    }
        
    public Guid Guid { get; }
    public BuiltInParameterGroup BuiltInParameterGroup { get; }
    public bool IsInstance { get; set; }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() &&
               Equals((SharedParameterDefinition)obj);
    }

    private bool Equals(SharedParameterDefinition other) => Guid.Equals(other.Guid);
    public override int GetHashCode() => Guid.GetHashCode();
    public override string ToString() => $"[{Guid}]";
}