﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Revit.Resources;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

public class SharedParameterUtils : IDisposable
{
    private readonly Document _document;
    private readonly Logger _logger;
    private readonly string _applicationSharedParametersFile;
    private readonly string _temporarySharedParametersFile;
    private readonly DefinitionFileUtils _definitionFileUtils;

    // public static void AddBimEdSharedParameters(Document document)
    // {
    //     using (var sharedParameterService = new SharedParameterService(document))
    //     {
    //         sharedParameterService.AddSharedParametersToProject(SharedParameters.BimEd.ProjectSharedParameters);
    //     }
    // }
    //
    // public static void AddAdskSharedParameters(Document document)
    // {
    //     using (var sharedParameterService = new SharedParameterService(document))
    //     {
    //         sharedParameterService.AddSharedParametersToProject(SharedParameters.Adsk.ProjectSharedParameters);
    //     }
    // }

    public SharedParameterUtils(Document document, AppResources appResources, AppPaths appPaths, Logger logger)
    {
        _document = document;
        _applicationSharedParametersFile = _document.Application.SharedParametersFilename;
        _temporarySharedParametersFile = Path.Combine(appPaths.TemporaryFiles, "SharedParameterDefinitions.txt");
        _logger = logger;
        _definitionFileUtils = new DefinitionFileUtils(
            _temporarySharedParametersFile,
            appResources.AppSharedParameterFile);
    }

    public void AddSharedParametersToProject(List<ProjectSharedParameter> sharedParameters)
    {
        _document.ExecuteTransaction(() =>
        {
            var documentSharedParameterGuids = GetSharedParameterGuids();
            var createdParameters = sharedParameters.Where(n => documentSharedParameterGuids.Contains(n.Guid)).ToList();
            var notCreatedParameters = sharedParameters.Where(n => false == documentSharedParameterGuids.Contains(n.Guid)).ToList();

            CheckSharedParameterCategories(createdParameters);

            notCreatedParameters.ForEach(AddSharedParameterToProject);

        }, TransactionNames.ParametersAdd);
    }

    public List<Guid> GetSharedParameterGuids()
    {
        return _document.IsFamilyDocument
            ? GetFamilySharedParameterGuids()
            : GetProjectSharedParameterGuids();
    }

    private List<Guid> GetFamilySharedParameterGuids() =>
        _document.FamilyManager.GetParameters().Where(n => n.IsShared)
            .Select(n => n.GUID).ToList();

    private List<Guid> GetProjectSharedParameterGuids() =>
        GetProjectSharedParameters()
            .Select(n => n.GuidValue).ToList();

    public void AddSharedParameterToProject(ProjectSharedParameter projectSharedParameter)
    {
        try
        {
            var bindingMap = _document.ParameterBindings;
            var categorySet = projectSharedParameter.GetCategorySet(_document);
            var sharedParameterDefinition = projectSharedParameter.SharedParameterDefinition;
            var definition = GetExternalDefinition(sharedParameterDefinition);

            if (false == bindingMap.Contains(definition))
            {
                var binding = projectSharedParameter.IsInstance
                    ? (Binding) _document.Application.Create.NewInstanceBinding(categorySet)
                    : _document.Application.Create.NewTypeBinding(categorySet);

                bindingMap.Insert(definition, binding, sharedParameterDefinition.BuiltInParameterGroup);

                _logger.Info($"Add shared parameter: {sharedParameterDefinition}");
            }
            else
            {
                var guid = sharedParameterDefinition.Guid;
                var mapIterator = bindingMap.ForwardIterator();

                mapIterator.Reset();

                while (mapIterator.MoveNext())
                {
                    var iteratorKey = mapIterator.Key;

                    if ((iteratorKey as ExternalDefinition)?.GUID != guid)
                        continue;

                    var binding = mapIterator.Current as ElementBinding;

                    projectSharedParameter.Categories.ForEach(builtInCategory =>
                    {
                        var category = Category.GetCategory(_document, builtInCategory);

                        if (binding != null && !binding.Categories.Contains(category))
                            binding.Categories.Insert(category);
                    });

                    break;
                }
            }
        }
        catch (Exception exception)
        {
            _logger.Error(projectSharedParameter.ToString());
            _logger.Error(exception);
        }
    }

    private void CheckSharedParameterCategories(IEnumerable<ProjectSharedParameter> projectSharedParameters)
    {
        var parameterBindings = GetProjectParameterBindings();
        var sharedParameterElements = GetProjectSharedParameters();
        var projectParameterDefinitions = parameterBindings.Keys.OfType<InternalDefinition>().ToList();

        foreach (var projectSharedParameter in projectSharedParameters)
        {
            try
            {
                var giud = projectSharedParameter.Guid;
                var sharedParameter = sharedParameterElements.FirstOrDefault(n => n.GuidValue == giud);
                var internalDefinition = sharedParameter?.GetDefinition();
                var projectDefinition = projectParameterDefinitions.FirstOrDefault(n => n.Name == internalDefinition?.Name);

                if (projectDefinition == null)
                    continue;

                if (parameterBindings.TryGetValue(projectDefinition, out var binding))
                    CheckCategorySetInProject(projectSharedParameter, internalDefinition, binding);
            }
            catch (Exception exception)
            {
                _logger.Error(projectSharedParameter.ToString());
                _logger.Error(exception);
            }
        }
    }

    private  Dictionary<Definition, ElementBinding> GetProjectParameterBindings()
    {
        var definitionBinding = new Dictionary<Definition, ElementBinding>();
        var bindingMap = _document.ParameterBindings;
        var mapIterator = bindingMap.ForwardIterator();

        mapIterator.Reset();

        while (mapIterator.MoveNext())
        {
            if (false == definitionBinding.ContainsKey(mapIterator.Key))
                definitionBinding.Add(mapIterator.Key, mapIterator.Current as ElementBinding);
        }

        return definitionBinding;
    }

    private List<SharedParameterElement> GetProjectSharedParameters()
    {
        using var collector = new FilteredElementCollector(_document);
        return collector.OfClass(typeof(SharedParameterElement))
            .OfType<SharedParameterElement>()
            .ToList();
    }

    private void CheckCategorySetInProject(ProjectSharedParameter projectSharedParameter, Definition definition, ElementBinding binding)
    {
        try
        {
            var isNotContainsAnyCategory = false;

            projectSharedParameter.Categories.ForEach(n =>
            {
                var category = Category.GetCategory(_document, n);

                if (binding.Categories.Contains(category))
                    return;

                binding.Categories.Insert(category);
                isNotContainsAnyCategory = true;
            });

            if (isNotContainsAnyCategory)
                _document.ParameterBindings.ReInsert(definition, binding);
        }
        catch (Exception exception)
        {
            _logger.Warn(projectSharedParameter.ToString());
            _logger.Warn(exception);
        }
    }

    public List<FamilyParameter> AddSharedParametersToFamily(List<SharedParameterDefinition> sharedParameterDefinitions) =>
        sharedParameterDefinitions.Select(n => AddSharedParameterToFamily(n)).ToList();
        
    public FamilyParameter AddSharedParameterToFamily(SharedParameterDefinition sharedParameterDefinition, bool? isInstance = null)
    {
        if (false == _document.IsFamilyDocument)
            throw new InvalidOperationException($"Document '{_document.Title}' must be a Family.");
            
        var existParameter = _document.FamilyManager.get_Parameter(sharedParameterDefinition.Guid);

        if (existParameter != null)
            return existParameter;
        
        var externalDefinition = GetExternalDefinition(sharedParameterDefinition);
        var group = sharedParameterDefinition.BuiltInParameterGroup;
        isInstance ??= sharedParameterDefinition.IsInstance;
        
        return _document.FamilyManager.AddParameter(externalDefinition, group, (bool)isInstance);
    }

    public FamilyParameter ReplaceSharedParameter(FamilyParameter familyParameter, SharedParameterDefinition newSharedParameter, BuiltInParameterGroup? newParameterGroup = null, bool? isInstance = null)
    {
        var newExternalDefinition = GetExternalDefinition(newSharedParameter);

        return _document.FamilyManager.ReplaceParameter(
            familyParameter,
            newExternalDefinition,
            newParameterGroup ?? newSharedParameter.BuiltInParameterGroup,
            isInstance ?? familyParameter.IsInstance);
    }

    public ExternalDefinition GetExternalDefinition(SharedParameterDefinition sharedParameterDefinition)
    {
        _document.Application.SharedParametersFilename = _temporarySharedParametersFile;

        var definitionFile = _document.Application.OpenSharedParameterFile();
        var groupName = _definitionFileUtils.GetGroupName(sharedParameterDefinition.Guid);
        var parameterName = _definitionFileUtils.GetParameterName(sharedParameterDefinition.Guid);
        var group = definitionFile.Groups.get_Item(groupName);

        return group.Definitions.get_Item(parameterName) as ExternalDefinition;
    }

    public List<ExternalDefinition> GetExternalDefinitions(List<SharedParameterDefinition> sharedParameterDefinitions)
    {
        _document.Application.SharedParametersFilename = _temporarySharedParametersFile;

        var definitionFile = _document.Application.OpenSharedParameterFile();

        return sharedParameterDefinitions
            .Select(n =>
            {
                var groupName = _definitionFileUtils.GetGroupName(n.Guid);
                var parameterName = _definitionFileUtils.GetParameterName(n.Guid);
                var group = definitionFile.Groups.get_Item(groupName);
                return group.Definitions.get_Item(parameterName) as ExternalDefinition;
            })
            .Where(n => n != null).ToList();
    }

    public void Dispose()
    {
        if (_applicationSharedParametersFile != null &&
            _applicationSharedParametersFile != _document.Application.SharedParametersFilename)
        {
            _document.Application.SharedParametersFilename = _applicationSharedParametersFile;
        }

        _definitionFileUtils.Dispose();
    }
}