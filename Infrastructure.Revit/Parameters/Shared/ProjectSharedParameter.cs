﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

public class ProjectSharedParameter
{
    public ProjectSharedParameter(
        SharedParameterDefinition sharedParameterDefinition,
        params BuiltInCategory[] categories)
    {
        if (categories.IsEmpty())
            throw new ArgumentException($"{nameof(categories)} must not be empty.");
            
        SharedParameterDefinition = sharedParameterDefinition;
        IsInstance = sharedParameterDefinition.IsInstance;
        Categories = new List<BuiltInCategory>(categories);
    }

    public ProjectSharedParameter(
        SharedParameterDefinition sharedParameterDefinition,
        bool isInstance,
        params BuiltInCategory[] categories)
    {
        if (categories.IsEmpty())
            throw new ArgumentException($"{nameof(categories)} must not be empty.");

        SharedParameterDefinition = sharedParameterDefinition;
        IsInstance = isInstance;
        Categories = new List<BuiltInCategory>(categories);
    }

    public SharedParameterDefinition SharedParameterDefinition { get; }
    public bool IsInstance { get; }
    public List<BuiltInCategory> Categories { get; }
    public Guid Guid => SharedParameterDefinition.Guid;

    public CategorySet GetCategorySet(Document document)
    {
        var categorySet = document.Application.Create.NewCategorySet();

        foreach (var builtInCategory in Categories)
            categorySet.Insert(document.Settings.Categories.get_Item(builtInCategory));

        return categorySet;
    }

    public override string ToString() => $"{SharedParameterDefinition}{Categories.JoinToString()}";
}