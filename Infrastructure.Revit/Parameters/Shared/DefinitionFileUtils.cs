﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

public class DefinitionFileUtils : IDisposable
{
    private const string PARAM = "PARAM";
    private const string GROUP = "GROUP";
    private readonly string _path;
    private Dictionary<int, string> _groups;
    private Dictionary<Guid, (string Name, int GroupId)> _parametersNames;

    public DefinitionFileUtils(string path, byte[] definitionFile)
    {
        _path = path;
        DeleteFileIfExists(_path);
        File.WriteAllBytes(_path, definitionFile);
        PullSharedParameters();
    }
    
    private void PullSharedParameters()
    {
        var content = File.ReadAllLines(_path);
        var rowIndex = 0;

        _groups = new Dictionary<int, string>();
        _parametersNames = new Dictionary<Guid, (string, int)>();

        foreach (var line in content)
        {
            rowIndex++;

            if (line.StartsWith(GROUP))
                PullGroupFromFile(line, rowIndex);

            if (line.StartsWith(PARAM))
                PullSharedParameterFromFile(line, rowIndex);
        }
    }

    private void PullGroupFromFile(string line, int rowIndex)
    {
        var values = line.Split('\t');

        if (values.Length < 3)
            throw new InvalidDataException($"[line:{rowIndex}] {GROUP} has invalid format.");

        if (!int.TryParse(values[1], out var id))
            throw new InvalidCastException($"{nameof(values)}[1] is not a {nameof(Int32)}");

        var name = values[2];

        _groups.Add(id, name);
    }

    private void PullSharedParameterFromFile(string line, int rowIndex)
    {
        var values = line.Split('\t');

        if (values.Length < 9)
            throw new InvalidDataException($"[line:{rowIndex}]: {PARAM} has invalid format.");

        if (!Guid.TryParse(values[1], out var guid))
            throw new InvalidCastException($"[line:{rowIndex}]: {nameof(values)}[1] = {values[1]} is not a {nameof(Guid)}");

        if (_parametersNames.ContainsKey(guid))
        {
            Debug.WriteLine($"[{guid}] is contained more than once in the DefinitionFile: {_path}");
            return;
        }

        if (!int.TryParse(values[5], out var groupIndex))
        {
            Debug.WriteLine($"[line:{rowIndex}]: {nameof(values)}[5] = {values[5]} is not a {nameof(Int32)}");
            return;
        }

        if (!_groups.TryGetValue(groupIndex, out var group))
        {
            Debug.WriteLine($"[line:{rowIndex}]: {nameof(groupIndex)} = {groupIndex}");
            return;
        }

        if (!int.TryParse(values[6], out var visibleIndex))
        {
            Debug.WriteLine($"[line:{rowIndex}]: {nameof(values)}[6] = {values[6]} is not a {nameof(Int32)}");
            return;
        }

        if (!int.TryParse(values[8], out var userModifiableIndex))
        {
            Debug.WriteLine($"[line:{rowIndex}]: {nameof(values)}[8] = {values[8]} is not a {nameof(Int32)}");
            return;
        }
        
        var name = values[2];
        var groupId = int.TryParse(values[5], out var id) ? id : -1;

        _parametersNames.Add(guid, (name, groupId));
    }

    public string GetGroupName(Guid guid)
    {
        return _parametersNames.TryGetValue(guid, out var parameter) && _groups.TryGetValue(parameter.GroupId, out var groupName)
            ? groupName
            : throw new KeyNotFoundException($"{nameof(guid)} [{guid}] not found in the file {_path}");
    }

    public string GetParameterName(Guid guid)
    {
        return _parametersNames.TryGetValue(guid, out var parameter)
            ? parameter.Name
            : throw new KeyNotFoundException($"{nameof(guid)} [{guid}] not found in the file {_path}");
    }
    
    public void Dispose() => DeleteFileIfExists(_path);

    private static void DeleteFileIfExists(string path)
    {
        try
        {
            if (File.Exists(path))
                File.Delete(path);
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
        }
    }
}