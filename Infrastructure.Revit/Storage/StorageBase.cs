﻿using System;
using Autodesk.Revit.DB.ExtensibleStorage;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Revit.Storage;

public abstract class StorageBase
{
    protected const string Data = "Data";
    private readonly Guid _guid;
    protected readonly string Name;
    protected readonly Schema Schema;

    protected StorageBase(Guid guid, string name)
    {
        _guid = guid;
        Name = name;
        Schema = GetSchema();
    }
    
    public abstract string GetValue();
    public abstract void SetValue(string value);
    public T GetAs<T>() where T : class
    {
        var value = GetValue();
        return value != null ? JsonConvert.DeserializeObject<T>(value) : null;
    }
    public void SetAsJson<T>(T value) => SetValue(JsonConvert.SerializeObject(value));

    protected Entity CreateEntity(string value)
    {
        var entity = new Entity(Schema);
        entity.Set(Data, value);
        return entity;
    }
    
    private Schema GetSchema() => Schema.Lookup(_guid) ?? BuildSchema(_guid);
    private Schema BuildSchema(Guid guid)
    {
        var schemaBuilder = new SchemaBuilder(guid);
        schemaBuilder.SetReadAccessLevel(AccessLevel.Public);
        schemaBuilder.AddSimpleField(Data, typeof(string));
        schemaBuilder.SetSchemaName(Name);
        return schemaBuilder.Finish();
    }
}