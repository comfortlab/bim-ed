using System;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.Storage;

public abstract class ElementStorageBase : StorageBase
{
    private readonly Element _element;
    protected ElementStorageBase(Element element, Guid guid, string name) : base(guid, name) => _element = element;
    public override string GetValue()
    {
        var entity = _element.GetEntity(Schema); 
        return entity != null && entity.IsValid() ? entity.Get<string>(Data) : default;
    }
    public override void SetValue(string value) => _element.SetEntity(CreateEntity(value));
}