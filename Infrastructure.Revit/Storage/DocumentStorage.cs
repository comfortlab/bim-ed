using System;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using CoLa.BimEd.Infrastructure.Revit.Resources;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Storage;

public abstract class DocumentStorage<T> : StorageBase
{
    private readonly Document _document;
    private DataStorage _dataStorage;

    protected DocumentStorage(Document document, Guid guid, string name) : base(guid, name)
    {
        _document = document;
        _dataStorage = GetDataStorage();
    }
    
    public override string GetValue()
    {
        if (_dataStorage is not { IsValidObject: true })
            _dataStorage = GetDataStorage();
        
        var entity = _dataStorage.GetEntity(Schema); 
        return entity != null && entity.IsValid() ? entity.Get<string>(Data) : default;
    }
    public override void SetValue(string value) => _dataStorage.SetEntity(CreateEntity(value));

    private DataStorage GetDataStorage()
    {
        var dataStorage = _document.GetElementsOfClass<DataStorage>()
            .FirstOrDefault(x =>
            {
                var entity = x.GetEntity(Schema);
                return entity != null && entity.IsValid();
            });
        
        if (dataStorage != null)
            return dataStorage;
        
        _document.ExecuteTransaction(() =>
            {
                dataStorage = DataStorage.Create(_document);
                dataStorage.Name = Name;
            },
            TransactionNames.DataStorage);

        return dataStorage;
    }
}