﻿using System;
using System.IO;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;
using CoLa.BimEd.Infrastructure.Revit.Resources;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public class FamilyLoader
{
    private readonly IServiceProvider _serviceProvider;
    private readonly Logger _logger;
    public const string FamilyExtension = "rfa";
    public const string FamilyTemplateExtension = "rft";

    public FamilyLoader(IServiceProvider serviceProvider, Logger logger)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
    }
    
    public Family LoadFamily(
        Document targetDocument,
        Document familyDocument,
        bool saveModified)
    {
        try
        {
            return familyDocument.LoadFamily(targetDocument, new FamilyLoadOptions());
        }
        finally
        {
            var path = familyDocument.PathName;
            familyDocument.Close(saveModified);
            DeleteAllReserveFiles(Path.GetDirectoryName(path));
        }
    }

    public Family LoadFamily(
        Document targetDocument,
        Document familyDocument,
        string familyName,
        bool deleteFileIfCreated = true)
    {
        string path = null;
            
        try
        {
            if (!string.IsNullOrWhiteSpace(familyDocument.PathName) &&
                familyName == null)
                return LoadFamily(targetDocument, familyDocument, saveModified: false);
                
            path = GetPath(familyName, FamilyExtension);
                    
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception exception)
            {
                _logger.Warn(exception);
            }
                    
            familyDocument.SaveAs(path);

            return LoadFamily(targetDocument, familyDocument, saveModified: false);
        }
        finally
        {
            if (deleteFileIfCreated && path != null)
                DeleteFile(path);
        }
    }
        
    internal Family LoadFamily(
        Document targetDocument,
        string familyName,
        byte[] bytes,
        bool deleteFileAfterLoading = true)
    {
        var path = GetPath(familyName, FamilyExtension);

        try
        {
            if (!File.Exists(path))
                File.WriteAllBytes(path, bytes);

            if (!File.Exists(path))
                throw new FileNotFoundException("File not found.", path);

            Family newFamily = null;

            targetDocument.ExecuteTransaction(() =>
            {
                newFamily = targetDocument.LoadFamily(path, new FamilyLoadOptions(), out var family)
                    ? family
                    : throw new InvalidOperationException($"Family loading error:\n\t{familyName}\n\t{path}");
            }, TransactionNames.LoadFamily);
                
            return newFamily;
        }
        finally
        {
            if (deleteFileAfterLoading)
                DeleteFile(path);
        }
    }

    // internal Family LoadFamily(
    //     Document targetDocument,
    //     Guid bimEdGuid)
    // {
    //     var familyResource = FamilyStorage.GetFamilyResource(bimEdGuid);
    //     var familyName = familyResource.Name;
    //     var familyFile = familyResource.File;
    //     var actualVersion = familyResource.Version;
    //     var path = GetPath(familyName, FamilyExtension);
    //     var application = DI.Get<RevitContext>().UIApplication.Application;
    //
    //     if (File.Exists(path))
    //     {
    //         var existFamilyDocument = application.OpenDocumentFile(path);
    //         var existVersionParameter = existFamilyDocument.FamilyManager.get_Parameter(SharedParameters.Adsk.Common.Guids.ADSK_Family_Version);
    //         var existGuidParameter = existFamilyDocument.FamilyManager.get_Parameter(SharedParameters.App.Guids.BimEdKey);
    //
    //         if (IsActualVersion(existFamilyDocument.FamilyManager, existVersionParameter, actualVersion) &&
    //             IsActualGuid(existFamilyDocument.FamilyManager, existGuidParameter, bimEdGuid))
    //         {
    //             return LoadFamily(targetDocument, existFamilyDocument, saveModified: false);
    //         }
    //     }
    //
    //     File.WriteAllBytes(path, familyFile);
    //
    //     var familyDocument = application.OpenDocumentFile(path);
    //     var versionParameter = familyDocument.FamilyManager.get_Parameter(SharedParameters.Adsk.Common.Guids.ADSK_Family_Version);
    //     var guidParameter = familyDocument.FamilyManager.get_Parameter(SharedParameters.App.Guids.BimEdKey);
    //
    //     familyDocument.ExecuteTransaction(() =>
    //     {
    //         using var parameterService = _serviceProvider.Get<SharedParameterService>();
    //
    //         versionParameter ??= parameterService.AddSharedParameterToFamily(SharedParameters.Adsk.Common.ADSK_Family_Version);
    //         guidParameter ??= parameterService.AddSharedParameterToFamily(SharedParameters.App.BimEdKey);
    //                 
    //         familyDocument.FamilyManager.SetFormula(versionParameter, $"\"{actualVersion}\"");
    //         familyDocument.FamilyManager.SetFormula(guidParameter, $"\"{bimEdGuid}\"");
    //
    //         UpdateSharedParameters(familyDocument, familyResource, parameterService);
    //     }, "Modify Family");
    //
    //     return LoadFamily(targetDocument, familyDocument, saveModified: true);
    // }

    private static void UpdateSharedParameters(
        Document familyDocument,
        FamilyResource familyResource,
        SharedParameterUtils utils)
    {
        foreach (var definition in familyResource.SharedParameters)
        {
            var familyParameter = familyDocument.FamilyManager.get_Parameter(definition.Guid.ToString().ToUpper());

            if (familyParameter != null)
            {
                utils.ReplaceSharedParameter(familyParameter, definition);
            }
            else
            {
                utils.AddSharedParameterToFamily(definition);
            }
        }
    }

    private static bool IsActualVersion(FamilyManager familyManager, FamilyParameter versionParameter, Version actualVersion)
    {
        if (versionParameter == null)
            return false;
            
        var familyVersionAsString = familyManager.CurrentType.AsString(versionParameter);
            
        return familyVersionAsString != null &&
               Version.TryParse(familyVersionAsString, out var familyVersion) &&
               familyVersion == actualVersion;
    }

    private static bool IsActualGuid(FamilyManager familyManager, FamilyParameter guidParameter, Guid actualGuid)
    {
        if (guidParameter == null)
            return false;
            
        var guidAsString = familyManager.CurrentType.AsString(guidParameter);

        return guidAsString != null &&
               Guid.TryParse(guidAsString, out var familyGuid) &&
               familyGuid == actualGuid;
    }

    internal static string GetPath(string familyName, string extension)
    {
        familyName ??= "FamilyTemplate";
            
        var familiesPath = DI.Get<AppPaths>().Families;
            
        if (Directory.Exists(familiesPath) == false)
            Directory.CreateDirectory(familiesPath);

        var path = Path.Combine(familiesPath, familyName);

        return Path.ChangeExtension(path, extension);
    }

    private void DeleteFile(string path, bool logWarn = true)
    {
        try
        {
            if (File.Exists(path))
                File.Delete(path);
        }
        catch (Exception exception)
        {
            if (logWarn)
                _logger.Warn(exception);
        }
    }

    private static void DeleteAllReserveFiles(string directoryPath)
    {
        if (string.IsNullOrWhiteSpace(directoryPath))
            return;
            
        var directoryInfo = new DirectoryInfo(directoryPath);
        var files = directoryInfo.GetFiles("*.0*.rfa");

        foreach (var fileInfo in files)
            File.Delete(fileInfo.FullName);
    }
}