﻿using System;
using System.IO;
using System.Resources;
using Autodesk.Revit.Creation;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Revit.Utils;
using Application = Autodesk.Revit.ApplicationServices.Application;
using Document = Autodesk.Revit.DB.Document;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public abstract class FamilyBuilder
{
    private readonly FamilyLoader _familyLoader;
    protected readonly Document TargetDocument;
    protected readonly string FamilyName;
    protected readonly byte[] FamilyTemplate;

    protected Application Application => TargetDocument?.Application;
    protected Document FamilyDocument;
    protected Family Family;
    protected FamilyManager FamilyManager => FamilyDocument?.FamilyManager;
    protected FamilyItemFactory FamilyCreate => FamilyDocument?.FamilyCreate;

    protected FamilyBuilder(FamilyLoader familyLoader, Document targetDocument, string familyName, Type templateResourcesType, string templateName)
    {
        _familyLoader = familyLoader;
        TargetDocument = targetDocument;
        FamilyName = familyName;

        if (new ResourceManager(templateResourcesType).GetObject(templateName) is not byte[] familyTemplate)
            throw new FileNotFoundException($"File not found in {templateResourcesType}", templateName);

        FamilyTemplate = FamilyService.GetActualRevitVersionFamilyTemplate(Application, templateName, familyTemplate);
    }
        
    public Family ConstructFamily()
    {
        FamilyDocument = FamilyService.NewFamilyDocument(Application, FamilyName, FamilyTemplate);
        Family = FamilyDocument.OwnerFamily;

        ExecuteTransaction(SetCategory);
        ExecuteTransaction(CreateTypes);
        ExecuteTransaction(CreateParameters);
        ExecuteTransaction(SetParameterValues);
        ExecuteTransaction(CreateReferencePlanes);
        ExecuteTransaction(CreateGraphicStyles);
        ExecuteTransaction(CreateGeometry);
        ExecuteTransaction(CreateConnectors);
            
        CreateAttachedFamilies();

        return _familyLoader.LoadFamily(TargetDocument, FamilyDocument, FamilyName);
    }

    protected abstract void SetCategory();
    protected abstract void CreateTypes();
    protected abstract void CreateParameters();
    protected abstract void SetParameterValues();
    protected abstract void CreateReferencePlanes();
    protected abstract void CreateGraphicStyles();
    protected abstract void CreateGeometry();
    protected abstract void CreateConnectors();
    protected abstract void CreateAttachedFamilies();

    protected void ExecuteTransaction(Action action, string transactionName = null) =>
        FamilyDocument.ExecuteTransaction(action, transactionName ?? "Modify");
}