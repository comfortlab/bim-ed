﻿using System;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.Math;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public static class TextNoteHelper
{
    private const string FontName = "Arial";
    private static readonly double FontSize = 2.5.Convert(Converting.MillimetersToInternal);
    private static readonly double BorderOffset = 0.5.Convert(Converting.MillimetersToInternal);
    private const int Background = 1; // Transparent
    private const int Color = 0; // Black
    private const int Border = 0;
    private const double WidthFactor = 1d;
    private const int IsBold = 0;
    private const int IsItalic = 0;
    private const int IsUnderline = 0;

    public static TextNoteType GetOrCreateTextNoteType(Document document, double fontSizeMm = 2.5, double widthFactor = 1.0, bool isTransparent = true, bool isBorder = false)
    {
        using (var collector = new FilteredElementCollector(document))
        {
            var allTextNoteTypes = collector.OfClass(typeof(TextNoteType)).OfType<TextNoteType>().ToList();

            var border = isBorder ? 1 : 0;
            var background = isTransparent ? 1 : 0;
            var fontSize = fontSizeMm.Convert(Converting.MillimetersToInternal);

            var type = allTextNoteTypes.FirstOrDefault(n =>
                n.get_Parameter(BuiltInParameter.TEXT_FONT)?.AsString() == FontName &&
                Math.Abs((n.get_Parameter(BuiltInParameter.TEXT_SIZE)?.AsDouble() ?? 0) - fontSize) < 1e-6 &&
                Math.Abs((n.get_Parameter(BuiltInParameter.LEADER_OFFSET_SHEET)?.AsDouble() ?? 0) - BorderOffset) < 1e-6 &&
                Math.Abs((n.get_Parameter(BuiltInParameter.TEXT_WIDTH_SCALE)?.AsDouble() ?? 0) - widthFactor) < 1e-2 &&
                n.get_Parameter(BuiltInParameter.TEXT_BACKGROUND)?.AsInteger() == background &&
                n.get_Parameter(BuiltInParameter.TEXT_BOX_VISIBILITY)?.AsInteger() == border &&
                n.get_Parameter(BuiltInParameter.LINE_COLOR)?.AsInteger() == Color &&
                n.get_Parameter(BuiltInParameter.TEXT_STYLE_BOLD)?.AsInteger() == IsBold &&
                n.get_Parameter(BuiltInParameter.TEXT_STYLE_ITALIC)?.AsInteger() == IsItalic &&
                n.get_Parameter(BuiltInParameter.TEXT_STYLE_UNDERLINE)?.AsInteger() == IsUnderline);

            if (type != null)
                return type;

            if (!(allTextNoteTypes.FirstOrDefault()?.Duplicate($"Arial - {fontSizeMm:0.0}{(widthFactor < 1 ? $" / {widthFactor:0.0}" : string.Empty)}{(isBorder ? " - Border" : string.Empty)}") is TextNoteType newDefaultTextNoteType))
                throw new InvalidOperationException($"Fault of creating new text note type.");

            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_FONT)?.Set(FontName);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_SIZE)?.Set(fontSize);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.LEADER_OFFSET_SHEET)?.Set(BorderOffset);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_WIDTH_SCALE)?.Set(widthFactor);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_BACKGROUND)?.Set(background);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_BOX_VISIBILITY)?.Set(border);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.LINE_COLOR)?.Set(Color);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_STYLE_BOLD)?.Set(IsBold);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_STYLE_ITALIC)?.Set(IsItalic);
            newDefaultTextNoteType.get_Parameter(BuiltInParameter.TEXT_STYLE_UNDERLINE)?.Set(IsUnderline);

            return newDefaultTextNoteType;
        }
    }
}