using System;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public class FamilyResource
{
    public FamilyResource(
        string name, byte[] file, Version version,
        SharedParameterDefinition[] sharedParameters = null)
    {
        Name = name;
        File = file;
        Version = version;
        SharedParameters = sharedParameters ?? Array.Empty<SharedParameterDefinition>();
    }

    public string Name { get; }
    public byte[] File { get; }
    public Version Version { get; }
    public SharedParameterDefinition[] SharedParameters { get; }
}