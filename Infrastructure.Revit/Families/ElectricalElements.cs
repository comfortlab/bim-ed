﻿namespace CoLa.BimEd.Infrastructure.Revit.Families;

public static class ElectricalElements
{
    // public static List<FamilyInstance> GetMediumVoltageCubicleInstances() =>
    //     ElementCollector.GetFamilyInstances()
    //         .Where(n => n.IsMediumVoltageSwitchgear())
    //         // .Where(n => n.IsCubicleInstance())
    //         .ToList();
    //
    // public static List<FamilyInstance> GetCubicleQuestionnaireInstances() =>
    //     ElementCollector.GetFamilyInstances()
    //         .Where(n => n.IsCubicleQuestionnaireInstance())
    //         .ToList();
    //
    // public static List<FamilyInstance> GetTransformerInstances() =>
    //     ElementCollector.GetFamilyInstances()
    //         .Where(n => n.IsTransformer())
    //         .ToList();
    //
    // public static bool IsBimEdFamilyInstance(this FamilyInstance familyInstance, Guid key) =>
    //     familyInstance.GetParameter(SharedParameters.App.Guids.BimEdKey)?.AsString() == key.ToString();
    //
    // public static bool IsCubicleInstance(this FamilyInstance familyInstance) =>
    //     familyInstance.IsBimEdFamilyInstance(BimEdConstants.ElementGuids.CubicleInstance);
    //
    // public static bool IsCubicleQuestionnaireInstance(this FamilyInstance familyInstance) =>
    //     familyInstance.IsBimEdFamilyInstance(BimEdConstants.ElementGuids.CubicleQuestionnaireInstance);

    // public static bool IsBimEdTransformerInstance(this FamilyInstance familyInstance) =>
    //     familyInstance.IsBimEdFamilyInstance(BimEdConstants.ElementGuids.TransformerSymbol);

    // public static bool IsCubicleElectricalSystem(this ElectricalSystem electricalSystem) =>
    //     electricalSystem?.BaseEquipment.IsCubicleInstance() ?? false;
        
    // public static List<ElectricalSystem> GetCubicleElectricalSystems() =>
    //     ElementCollector.GetElectricalSystems()
    //         .Where(n => n.IsCubicleElectricalSystem())
    //         .ToList();

    // public FamilySymbol GetElectricalMockupFamilySymbol(Document document)
    // {
    //     var mockupFamilySymbol = ElementCollector.GetElementsOfClass<FamilySymbol>()
    //         .FirstOrDefault(n =>
    //             n.get_Parameter(SharedParameters.BimEd.Guids.BimEdKey)?.AsString() ==
    //             BimEdConstants.ElementGuids.MockupElectricalSymbol.ToString());
    //
    //     if (mockupFamilySymbol == null)
    //         return null;
    //
    //     if (mockupFamilySymbol.IsActive == false)
    //         document.ExecuteTransaction(() =>
    //         {
    //             mockupFamilySymbol.Activate();
    //         }, TransactionNames.Activate);
    //
    //     return mockupFamilySymbol;
    // }
    //
    // public static void UploadElectricalMockupFamilySymbol()
    // {
    //     if (GetElectricalMockupFamilySymbol(DI.Get<RevitContext>().Document) == null)
    //         FamilyBuildService.CreateMockupElectricalFamily();
    // }
    //
    // public static FamilySymbol GetTransformerFamily(Document document)
    // {
    //     using var collector = document.NewElementCollector();
    //         
    //     var result = collector.OfClass(typeof(FamilySymbol))
    //         .OfType<FamilySymbol>()
    //         .FirstOrDefault(n =>
    //         {
    //             var bimEdKeyParameter = n.get_Parameter(SharedParameters.BimEd.Guids.BimEdKey);
    //
    //             return bimEdKeyParameter != null &&
    //                    bimEdKeyParameter.AsString() == BimEdConstants.ElementGuids.TransformerSymbol.ToString();
    //         });
    //
    //     if (result != null)
    //     {
    //         return result;
    //     }
    //
    //     var families = FamilyService.LoadFamily(document, FamilyNames.Transformer, ElectricalFamilies.Transformer_R2016);
    //
    //     var symbolId = families.GetFamilySymbolIds().FirstOrDefault();
    //     result = document.GetElement(symbolId) as FamilySymbol;
    //
    //     if (result?.IsActive ?? true)
    //     {
    //         return result;
    //     }
    //
    //     result.Activate();
    //     document.Regenerate();
    //
    //     return result;
    // }
}