﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Revit.Comparers;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public class FamilyDocuments
{
    private readonly SortedDictionary<ElementId, Document> _idFamilyDocuments;

    public FamilyDocuments()
    {
        _idFamilyDocuments = new SortedDictionary<ElementId, Document>(new ElementIdComparer());
    }

    public Document GetFamilyDocument(FamilyInstance familyInstance)
    {
        var family = familyInstance?.Symbol.Family;
            
        return family != null
            ? GetFamilyDocument(family.Id) ?? CreateFamilyDocument(familyInstance)
            : null;
    }

    public Document GetFamilyDocument(ElementId familyId)
    {
        try
        {
            return _idFamilyDocuments.TryGetValue(familyId, out var familyDocument)
                ? familyDocument
                : null;
        }
        catch (Exception exception)
        {
            Debug.WriteLine($"family: {familyId}\n{exception}", GetType().Namespace);
            return null;
        }
    }

    private Document CreateFamilyDocument(FamilyInstance familyInstance)
    {
        var family = familyInstance?.Symbol.Family;

        if (family == null)
            return null;

        try
        {
            var familyId = family.Id;

            if (_idFamilyDocuments.ContainsKey(familyId) ||
                family.IsInPlace ||
                family.IsEditable == false)
                // TODO : # | Petrov | Если есть элемнты цепи In-Place, то сообщить, что они не учитываются в расчётах
                return null;

            var familyDocument = familyInstance.Document?.EditFamily(family);
            _idFamilyDocuments.Add(familyId, familyDocument);

            return familyDocument;
        }
        catch
        {
            Debug.WriteLine($"family: {family.Name}", GetType().Namespace);
            throw;
        }
    }

    public void Destroy()
    {
        foreach (var n in _idFamilyDocuments.Values.Where(n => n.IsValidObject))
            n.Close(saveModified: false);
        
        _idFamilyDocuments.Clear();
    }
}