﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;
using CoLa.BimEd.Infrastructure.Revit.Resources;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public static class FamilyService
{
    // public static void CreateFamilyInstance(Guid bimEdElementGuid) =>
    //     CreateFamilyInstance(GetFamilySymbol(bimEdElementGuid));
    //
    // public static void CreateInstance(Family family) =>
    //     CreateFamilyInstance(GetFirstFamilySymbol(family));

    // public static void CreateFamilyInstance(FamilySymbol familySymbol) =>
    //     RevitServices.Application.PromptForFamilyInstancePlacement(familySymbol);
    //
    // public static FamilySymbol GetFamilySymbol(Guid bimEdElementGuid)
    // {
    //     var familyResource = FamilyStorage.GetFamilyResource(bimEdElementGuid);
    //         
    //     if (familyResource == null)
    //         throw new KeyNotFoundException($"{nameof(bimEdElementGuid)} = {bimEdElementGuid} not found in {nameof(FamilyStorage)}.");
    //
    //     var bimEdGuidAsString = bimEdElementGuid.ToString();
    //     var familySymbol = ElementCollector
    //         .GetElementsOfClass<FamilySymbol>()
    //         .FirstOrDefault(n =>
    //             n.FamilyName == familyResource.Name &&
    //             n.get_Parameter(SharedParameters.App.Guids.BimEdKey)?.AsString() == bimEdGuidAsString);
    //
    //     if (familySymbol != null && IsActualFamilyParameterValue(familySymbol.Family, SharedParameters.Adsk.Common.ADSK_Family_Version, familyResource.Version.ToString()))
    //         return familySymbol;
    //
    //     var family = FamilyLoader.LoadFamily(DI.Get<RevitContext>().Document, bimEdElementGuid);
    //     var familySymbols = GetFamilySymbols(family);
    //
    //     familySymbol = familySymbols.Count == 1
    //         ? familySymbols.First()
    //         : familySymbols.FirstOrDefault(n => n.get_Parameter(SharedParameters.App.Guids.BimEdKey)?.AsString() == bimEdGuidAsString) ??
    //           familySymbols.FirstOrDefault();
    //
    //     if (familySymbol == null)
    //         throw new NullReferenceException($"{nameof(familySymbol)} is NULL.");
    //         
    //     return ActivatedFamilySymbol(familySymbol);
    // }

    private static FamilySymbol ActivatedFamilySymbol(FamilySymbol familySymbol)
    {
        if (familySymbol.IsActive)
            return familySymbol;

        familySymbol.Document.ExecuteTransaction(() =>
        {
            familySymbol.Activate();
            DI.Get<RevitContext>().Document.Regenerate();
        }, TransactionNames.Activate);

        return familySymbol;
    }

    public static List<FamilySymbol> GetFamilySymbols(this Family family)
    {
        if (family == null) throw new ArgumentNullException(nameof(family));
            
        var firstFamilySymbolIds = family.GetFamilySymbolIds();
        var document = family.Document;
        var familySymbols = firstFamilySymbolIds.Select(n => document.GetElement(n) as FamilySymbol).ToList();

        if (familySymbols.Any(n => n.IsActive == false))
        {
            document.ExecuteTransaction(() =>
                    familySymbols.Where(n => !n.IsActive).ForEach(n => n.Activate()),
                TransactionNames.Activate);
        }

        return familySymbols;
    }

    public static FamilySymbol GetFirstFamilySymbol(this Family family, ElementId symbolElementId = null)
    {
        if (family == null) throw new ArgumentNullException(nameof(family));
            
        var firstFamilySymbolId = symbolElementId != null
            ? family.GetFamilySymbolIds().FirstOrDefault(n => n == symbolElementId)
            : family.GetFamilySymbolIds().OrderBy(n => n.IntegerValue).First();

        var document = family.Document;
        var familySymbol = document.GetElement(firstFamilySymbolId) as FamilySymbol;

        if (familySymbol?.IsActive == false)
        {
            document.ExecuteTransaction(() =>
                    familySymbol.Activate(),
                TransactionNames.Activate);
        }

        return familySymbol;
    }

    public static string GetModelValue(this Family family, ElementId symbolElementId = null) =>
        family.GetFirstFamilySymbol(symbolElementId)
            .get_Parameter(BuiltInParameter.ALL_MODEL_MODEL)
            .AsString();

    public static void EditFamily(Family family, Document targetDocument, Action<Document> action)
    {
        using var familyDocument = targetDocument.EditFamily(family);
        action?.Invoke(familyDocument);
        familyDocument.LoadFamily(targetDocument, new FamilyLoadOptions());
        familyDocument.Close(saveModified: false);
    }
        
    // public static Family LoadFamily(Document targetDocument, Document familyDocument, string familyName = null)
    // {
    //     return FamilyLoader.LoadFamily(targetDocument, familyDocument, familyName);
    // }
    //
    // public static Family LoadFamily(Document targetDocument, string familyName, byte[] bytes)
    // {
    //     return FamilyLoader.LoadFamily(targetDocument, familyName, bytes);
    // }

    public static bool IsActualFamilyParameterValue(Family family, SharedParameterDefinition sharedParameter, string value)
    {
        var firstSymbol = family.GetFirstFamilySymbol();
        var familyParameterAsString = firstSymbol.get_Parameter(sharedParameter.Guid)?.AsString();

        return
            !string.IsNullOrWhiteSpace(familyParameterAsString) &&
            familyParameterAsString == value;
    }

    // internal static void SetActualValue(
    //     Document familyDocument,
    //     SharedParameterDefinition sharedParameterDefinition,
    //     string value)
    // {
    //     var familyParameter = familyDocument.FamilyManager.get_Parameter(sharedParameterDefinition.Guid);
    //
    //     if (familyParameter == null)
    //     {
    //         var doc = familyDocument;
    //
    //         familyDocument.ExecuteTransaction(() =>
    //         {
    //             using var parameterService = new SharedParameterService(doc);
    //             familyParameter = parameterService.AddSharedParameterToFamily(sharedParameterDefinition);
    //         }, "Add Parameter");
    //     }
    //
    //     familyDocument.ExecuteTransaction(() =>
    //     {
    //         familyDocument.FamilyManager.SetFormula(familyParameter, $"\"{value}\"");
    //     }, "Set Formula");
    // }

    public static byte[] GetActualRevitVersionFamilyTemplate(Application application, string templateName, byte[] bytes)
    {
        var templatePath = FamilyLoader.GetPath(templateName.Replace("_R2016", string.Empty), FamilyLoader.FamilyTemplateExtension);
            
        if (File.Exists(templatePath))
            return File.ReadAllBytes(templatePath);

        var sourceTemplatePath = FamilyLoader.GetPath(Guid.NewGuid().ToString(), FamilyLoader.FamilyTemplateExtension);
        var newVersionFamilyPath = FamilyLoader.GetPath(Guid.NewGuid().ToString(), FamilyLoader.FamilyExtension);

        if (!File.Exists(sourceTemplatePath))
            File.WriteAllBytes(sourceTemplatePath, bytes);

        if (!File.Exists(sourceTemplatePath))
            throw new FileNotFoundException("File not found.", templatePath);

        var document = application.NewFamilyDocument(sourceTemplatePath);

        document.SaveAs(newVersionFamilyPath);
        document.Close(false);

        new FileInfo(newVersionFamilyPath).CopyTo(templatePath, true);

        DeleteFile(sourceTemplatePath, logWarn: false);
        DeleteFile(newVersionFamilyPath, logWarn: false);

        return File.ReadAllBytes(templatePath);
    }

    // public static Family GetFamilyByCodeFunction(string codeFunction, string name = null) =>
    //     GetFamilyByCodeFunction(DI.Get<RevitContext>().Document, codeFunction, name);
    // public static Family GetFamilyByCodeFunction(Document document, string codeFunction, string name = null)
    // {
    //     var families = ElementCollector.GetFamilies(document)
    //         .Where(n => document.GetElement(n.GetFamilySymbolIds().FirstOrDefault() ?? ElementId.InvalidElementId)
    //             ?.get_Parameter(SharedParameters.BimEd.Code_Function.Guid)?.AsString() == codeFunction)
    //         .ToList();
    //
    //     return name != null
    //         ? families.FirstOrDefault(n => n.Name == name)
    //         : families.FirstOrDefault();
    // }

    public static Document NewFamilyDocument(Application application, string familyName, byte[] bytes)
    {
        var templatePath = FamilyLoader.GetPath(familyName, FamilyLoader.FamilyTemplateExtension);
            
        try
        {
            if (!File.Exists(templatePath))
                File.WriteAllBytes(templatePath, bytes);

            if (!File.Exists(templatePath))
                throw new FileNotFoundException("File not found.", templatePath);

            return application.NewFamilyDocument(templatePath);
        }
        finally
        {
            DeleteFile(templatePath);
        }
    }

    private static void DeleteFile(string path, bool logWarn = true)
    {
        try
        {
            if (File.Exists(path))
                File.Delete(path);
        }
        catch (Exception exception)
        {
            if (logWarn)
                Debug.WriteLine(exception);
        }
    }

    // public static bool IsFamilyExist(string name) => IsFamilyExist(DI.Get<RevitContext>().Document, name);
    // public static bool IsFamilyExist(Document document, string name) =>
    //     ElementCollector.GetFamilies(document).Any(n => n.Name == name);
    //
    // public static Family GetFamilyByName(string name) => GetFamilyByName(DI.Get<RevitContext>().Document, name);
    // public static Family GetFamilyByName(Document document, string name) =>
    //     ElementCollector.GetFamilies(document).FirstOrDefault(n => n.Name == name);

    public static void OpenAndActivateFamily(Family family)
    {
        var document = family.Document;
        using var familyDocument = document.EditFamily(family);
        var path = Environment.GetFolderPath(Environment.SpecialFolder.Templates);
        var fileName = Path.Combine(path, $"{family.Name}.rfa");
        var saveAsOptions = new SaveAsOptions { OverwriteExistingFile = true };
        familyDocument.SaveAs(fileName, saveAsOptions);
        familyDocument.Close(false);
        var uiDocument = new UIDocument(document);
        var uiApplication = uiDocument.Application;
        var modelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(fileName);
        uiApplication.OpenAndActivateDocument(modelPath, new OpenOptions(), false);
    }

    // public static Guid GetBimEdGuid(FamilySymbol familySymbol)
    // {
    //     var asString = familySymbol.get_Parameter(SharedParameters.App.Guids.BimEdKey)?.AsString();
    //     return !string.IsNullOrWhiteSpace(asString) && Guid.TryParse(asString, out var guid) ? guid : Guid.Empty;
    // }
    //
    // public static string GetCodeFunction(FamilySymbol familySymbol)
    // {
    //     var codeFunctionParameter =
    //         familySymbol.get_Parameter(SharedParameters.App.Guids.Code_Function) ??
    //         familySymbol.LookupParameter(SharedParameters.App.Code_Function.Name);
    //
    //     return codeFunctionParameter?.AsString() ?? string.Empty;
    // }
    //
    // public static string GetFamilyVersion(FamilySymbol familySymbol)
    // {
    //     var familyVersionParameter = familySymbol.get_Parameter(SharedParameters.Adsk.Common.Guids.ADSK_Family_Version) ??
    //                                  familySymbol.LookupParameter(SharedParameters.Adsk.Common.ADSK_Family_Version.Name) ??
    //                                  familySymbol.LookupParameter(ParametersAnnotation.FamilyVersion);
    //
    //     return familyVersionParameter?.AsString();
    // }

    public static void SetCategory(Document familyDocument, BuiltInCategory category)
    {
        var ownerFamily = familyDocument.OwnerFamily;
        ownerFamily.FamilyCategory = Category.GetCategory(familyDocument, category);
    }

    public static void SetPartType(Document familyDocument, PartType partType)
    {
        var ownerFamily = familyDocument.OwnerFamily;
        ownerFamily.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)?.Set((int)partType);
    }
}