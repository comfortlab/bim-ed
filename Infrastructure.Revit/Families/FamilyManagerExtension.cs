﻿using System;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public static class FamilyManagerExtension
{
    public static void AssociateParameters(this FamilyManager familyManager, Element element, BuiltInParameter builtInParameter, FamilyParameter familyParameter) =>
        AssociateParameters(familyManager, element.get_Parameter(builtInParameter), familyParameter);

    public static void AssociateParameters(this FamilyManager familyManager, Element element, Guid guid, FamilyParameter familyParameter) =>
        AssociateParameters(familyManager, element.get_Parameter(guid), familyParameter);

    public static void AssociateParameters(this FamilyManager familyManager, Element element, string parameterName, FamilyParameter familyParameter) =>
        AssociateParameters(familyManager, element.LookupParameter(parameterName), familyParameter);

    public static void AssociateParameters(this FamilyManager familyManager, Parameter parameter, FamilyParameter familyParameter)
    {
        if (parameter != null && familyManager.CanElementParameterBeAssociated(parameter))
            familyManager.AssociateElementParameterToFamilyParameter(parameter, familyParameter);
    }

    public static void CheckFamilyDocument(Document familyDocument)
    {
        if (familyDocument.IsFamilyDocument == false)
            throw new ArgumentException("Document must be Family.", nameof(familyDocument));
    }
}