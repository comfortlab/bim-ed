﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.Families;

public class FamilyLoadOptions : IFamilyLoadOptions
{
    public bool OnFamilyFound(bool familyInUse, out bool overwriteParameterValues)
    {
        overwriteParameterValues = true;
        return true;
        //return _familyLoadOptionsImplementation.OnFamilyFound(familyInUse, out overwriteParameterValues);
    }

    public bool OnSharedFamilyFound(Family sharedFamily, bool familyInUse, out FamilySource resource,
        out bool overwriteParameterValues)
    {
        overwriteParameterValues = true;
        resource = FamilySource.Project;
        return true;
        //return _familyLoadOptionsImplementation.OnSharedFamilyFound(sharedFamily, familyInUse, out source, out overwriteParameterValues);
    }
}