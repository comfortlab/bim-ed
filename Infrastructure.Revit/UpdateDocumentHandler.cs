using System;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit;

public class UpdateDocumentHandler
{
    public Action<Document> OnDocumentChanged { get; set; }
}