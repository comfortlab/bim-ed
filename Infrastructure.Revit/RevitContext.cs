﻿using System;
using System.IO;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Events;
using Autodesk.Revit.UI.Selection;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.Infrastructure.Revit;

public class RevitContext
{
    private readonly ProcessingScope _processingScope;
    private readonly UpdateDocumentHandler _updateDocumentHandler;
    private readonly Logger _logger;
        
    public RevitContext(
        UIControlledApplication uiControlledApplication,
        ProcessingScope processingScope,
        UpdateDocumentHandler updateDocumentHandler,
        Logger logger)
    {
        _processingScope = processingScope;
        _updateDocumentHandler = updateDocumentHandler;
        _logger = logger;

        UIControlledApplication = uiControlledApplication;
        Version = UIControlledApplication.ControlledApplication.VersionNumber.FirstInt();
    }

    public UIControlledApplication UIControlledApplication { get; set; }
    public UIApplication UIApplication { get; set; }
    public UIDocument UIDocument => UIApplication.ActiveUIDocument;
    public Selection Selection => UIDocument.Selection;
    public Document Document { get; set; }
    public string AssemblyPath { get; set; }
    public string AssemblyDirectory => Path.GetDirectoryName(AssemblyPath);
    public int Version { get; set; }
    public bool ShowWarnings { get; set; }

    public void ContextUpdating(bool activate)
    {
        UIControlledApplication.ViewActivated -= OnViewActivated;
        if (activate) UIControlledApplication.ViewActivated += OnViewActivated;
    }

    private void OnViewActivated(object sender, ViewActivatedEventArgs e)
    {
        var document = e.Document;
        
        if (document == null)
            return;
        
        _logger.Debug($"Document: {e.Document?.Title}");
        
        try
        {
            if (!IsDocumentChanged(e))
                return;


            Document = document;
            UIApplication = new UIApplication(document.Application);
            
            _processingScope.Current = document;
                
            document.DocumentClosing -= OnDocumentClosing;
            document.DocumentClosing += OnDocumentClosing;
            
            _updateDocumentHandler.OnDocumentChanged?.Invoke(document);
            
            // if (document.IsFamilyDocument)
            //     return;

            // _appInfo.Revit.StorageObjectId = Document.ProjectInformation.Id.IntegerValue;
            // LegacySettingStorage.SetSettingStorage(Document);
            // RibbonBindingProperties.UpdateItemValues();
        }
        catch (Exception exception)
        {
            _logger.Error(exception);
        }
    }

    private static bool IsDocumentChanged(ViewActivatedEventArgs e)
    {
        return !Equals(e.CurrentActiveView?.Document, e.PreviousActiveView?.Document);
    }

    private void OnDocumentClosing(object sender, DocumentClosingEventArgs e)
    {
        _processingScope?.Remove(e.Document);
    }
}