﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.Infrastructure.Revit.RevitVersions.Relevant;
using CoLa.BimEd.Infrastructure.Revit2016;

namespace CoLa.BimEd.Infrastructure.Revit.RevitVersions;

public class RevitVersionResolver
{
    private readonly int _revitVersion;

    public RevitVersionResolver(RevitContext revitContext)
    {
        _revitVersion = revitContext.Version;
    }

    public ElectricalSystem CreateElectricalSystem(Document document, Connector connector, ElectricalSystemType electricalSystemType) =>
        _revitVersion < 2019
            ? ElectricalSystemRevit2016.Create(document, connector, electricalSystemType)
            : Revit2019.ElectricalSystem.Create(connector, electricalSystemType);
       
    public ElectricalSystem CreateElectricalSystem(Document document, IList<ElementId> elementIds, ElectricalSystemType electricalSystemType) =>
        _revitVersion < 2019
            ? ElectricalSystemRevit2016.Create(document, elementIds, electricalSystemType)
            : Revit2019.ElectricalSystem.Create(document, elementIds, electricalSystemType);

    public ParameterFilterElement CreateParameterFilterElement(Document document, string name, IList<ElementId> idsCategories, IList<FilterRule> rules) =>
        _revitVersion < 2019
            ? ParameterFilterElementRevit2016.Create(document, name, idsCategories, rules)
            : Revit2019.ParameterFilterElement.Create(document, name, idsCategories, rules);
        
    public void SetSurfaceForegroundPatternColor(OverrideGraphicSettings graphicSettings, Color color)
    {
        if (_revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternColor(graphicSettings, color);
        else
            Revit2019.OverrideGraphicSettings.SetSurfaceForegroundPatternColor(graphicSettings, color);
    }

    public void SetSurfaceForegroundPatternId(OverrideGraphicSettings graphicSettings, FillPatternElement solidPattern)
    {
        if (_revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternId(graphicSettings, solidPattern.Id);
        else
            Revit2019.OverrideGraphicSettings.SetSurfaceForegroundPatternId(graphicSettings, solidPattern.Id);
    }

    public void SetSurfaceForegroundPatternVisible(OverrideGraphicSettings graphicSettings)
    {
        if (_revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternVisible(graphicSettings, false);
        else
            Revit2019.OverrideGraphicSettings.SetSurfaceForegroundPatternVisible(graphicSettings, false);
    }
        
    public bool Equals(InternalDefinition internalDefinition1, InternalDefinition internalDefinition2) =>
        _revitVersion < 2017
            ? InternalDefinitionRevit2016.Equals(internalDefinition1, internalDefinition2)
            : Revit2017.InternalDefinition.Equals(internalDefinition1, internalDefinition2);
        
    public IList<ElementId> GetViewPlans(Level level) =>
        _revitVersion < 2018
            ? LevelRevit2016.GetViewPlans(level)
            : Revit2018.Element.GetDependentElements<ViewPlan>(level);
        
    public IEnumerable<ElectricalSystem> GetElectricalSystems(FamilyInstance familyInstance) =>
        _revitVersion < 2021
            ? MEPModelRevit2016.GetElectricalSystems(familyInstance)
            : Revit2021.MEPModel.GetElectricalSystems(familyInstance);

    public IEnumerable<ElectricalSystem> GetAssignedElectricalSystems(FamilyInstance familyInstance) =>
        _revitVersion < 2021
            ? MEPModelRevit2016.GetAssignedElectricalSystems(familyInstance)
            : Revit2021.MEPModel.GetAssignedElectricalSystems(familyInstance);
    public IEnumerable<ElectricalSystem> GetElectricalSystems(MEPModel mepModel) =>
        _revitVersion < 2021
            ? MEPModelRevit2016.GetElectricalSystems(mepModel)
            : Revit2021.MEPModel.GetElectricalSystems(mepModel);

    public IEnumerable<ElectricalSystem> GetAssignedElectricalSystems(MEPModel mepModel) =>
        _revitVersion < 2021
            ? MEPModelRevit2016.GetAssignedElectricalSystems(mepModel)
            : Revit2021.MEPModel.GetAssignedElectricalSystems(mepModel);
        
    public Plane CreatePlane(XYZ normal, XYZ origin) =>
        _revitVersion < 2017
            ? PlaneRevit2016.CreateByNormalAndOrigin(normal, origin)
            : Revit2017.Plane.CreateByNormalAndOrigin(normal, origin);
        
    public double ConvertToInternalUnits(double value, Parameter targetParameter) =>
        _revitVersion < 2021
            ? UnitUtilsRevit2016.ConvertToInternalUnits(value, targetParameter)
            : Revit2021.UnitUtils.ConvertToInternalUnits(value, targetParameter);

    public void SetMetersAsUnitType(FormatOptions formatOptions)
    {
        if (_revitVersion < 2021)
            FormatOptionsRevit2016.SetMetersAsDisplayUnitType(formatOptions);
        else
            Revit2021.FormatOptions.SetMetersAsUnitType(formatOptions);
    }

    // public double ConvertFromInternalUnitsToOhmMeters(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertFromInternalUnitsToOhmMeters(value)
    //         : Revit2021.UnitUtils.ConvertFromInternalUnitsToOhmMeters(value);

    // public double ConvertFromInternalUnitsToMillimeters(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertFromInternalUnitsToMillimeters(value)
    //         : Revit2021.UnitUtils.ConvertFromInternalUnitsToMillimeters(value);

    // public double ConvertFromInternalUnitsToVoltAmperes(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertFromInternalUnitsToVoltAmperes(value)
    //         : Revit2021.UnitUtils.ConvertFromInternalUnitsToVoltAmperes(value);

    // public double ConvertFromInternalUnitsToVolts(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertFromInternalUnitsToVolts(value)
    //         : Revit2021.UnitUtils.ConvertFromInternalUnitsToVolts(value);

    // public double ConvertFromInternalUnitsToWatts(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertFromInternalUnitsToWatts(value)
    //         : Revit2021.UnitUtils.ConvertFromInternalUnitsToWatts(value);

    // public double ConvertToInternalUnitsFromMillimeters(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertToInternalUnitsFromMillimeters(value)
    //         : Revit2021.UnitUtils.ConvertToInternalUnitsFromMillimeters(value);

    // public double ConvertToInternalUnitsFromVolts(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertToInternalUnitsFromVolts(value)
    //         : Revit2021.UnitUtils.ConvertToInternalUnitsFromVolts(value);

    // public double ConvertToInternalUnitsFromVoltAmperes(double value) =>
    //    BimEdInfo.Revit.VersionNumber < 2021
    //        ? UnitUtilsRevit2016.ConvertToInternalUnitsFromVoltAmperes(value)
    //        : Revit2021.UnitUtils.ConvertToInternalUnitsFromVoltAmperes(value);

    // public double ConvertToInternalUnitsFromWatts(double value) =>
    //     BimEdInfo.Revit.VersionNumber < 2021
    //         ? UnitUtilsRevit2016.ConvertToInternalUnitsFromWatts(value)
    //         : Revit2021.UnitUtils.ConvertToInternalUnitsFromWatts(value);
        
    public IEnumerable<ElementId> GetGenericAnnotationElements(ViewSheet viewSheet) =>
        _revitVersion < 2018
            ? ViewSheetRevit2016.GetGenericAnnotationElements(viewSheet)
            : Revit2018.Element.GetDependentElements(viewSheet, BuiltInCategory.OST_GenericAnnotation);
}