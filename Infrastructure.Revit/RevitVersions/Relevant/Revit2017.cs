﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.RevitVersions.Relevant;

public class Revit2017
{
    public class Plane
    {
        public static Autodesk.Revit.DB.Plane CreateByNormalAndOrigin(
            XYZ normal,
            XYZ origin)
        {
            return Autodesk.Revit.DB.Plane.CreateByNormalAndOrigin(normal, origin);
        }
    }

    public class InternalDefinition
    {
        public static bool Equals(
            Autodesk.Revit.DB.InternalDefinition internalDefinition1,
            Autodesk.Revit.DB.InternalDefinition internalDefinition2)
        {
            return internalDefinition1.Id.IntegerValue ==
                   internalDefinition2.Id.IntegerValue;
        }
    }
}