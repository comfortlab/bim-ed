﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.RevitVersions.Relevant;

public class Revit2018
{
    public class Element
    {
        public static IList<ElementId> GetDependentElements<T>(Autodesk.Revit.DB.Element level) =>
            level.GetDependentElements(new ElementClassFilter(typeof(T)));
            
        public static IList<ElementId> GetDependentElements(Autodesk.Revit.DB.Element level, BuiltInCategory builtInCategory) =>
            level.GetDependentElements(new ElementCategoryFilter(builtInCategory));
    }
}