﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;

namespace CoLa.BimEd.Infrastructure.Revit.RevitVersions.Relevant;

public class Revit2019
{
    public class ElectricalSystem
    {
        public static Autodesk.Revit.DB.Electrical.ElectricalSystem Create(
            Connector connector,
            ElectricalSystemType electricalSystemType)
        {
            return Autodesk.Revit.DB.Electrical.ElectricalSystem.Create(
                connector,
                electricalSystemType);
        }
            
        public static Autodesk.Revit.DB.Electrical.ElectricalSystem Create(
            Document document,
            IList<ElementId> connector,
            ElectricalSystemType electricalSystemType)
        {
            return Autodesk.Revit.DB.Electrical.ElectricalSystem.Create(
                document,
                connector,
                electricalSystemType);
        }
    }

    public class OverrideGraphicSettings
    {
        public static void SetSurfaceForegroundPatternColor(
            Autodesk.Revit.DB.OverrideGraphicSettings overrideGraphicSettings,
            Color color)
        {
            overrideGraphicSettings.SetSurfaceForegroundPatternColor(color);
        }

        public static void SetSurfaceForegroundPatternId(
            Autodesk.Revit.DB.OverrideGraphicSettings overrideGraphicSettings,
            ElementId elementId)
        {
            overrideGraphicSettings.SetSurfaceForegroundPatternId(elementId);
        }

        public static void SetSurfaceForegroundPatternVisible(
            Autodesk.Revit.DB.OverrideGraphicSettings overrideGraphicSettings,
            bool fillPatternVisible)
        {
            overrideGraphicSettings.SetSurfaceForegroundPatternVisible(fillPatternVisible);
        }
    }

    public class ParameterFilterElement
    {
        public static Autodesk.Revit.DB.ParameterFilterElement Create(
            Document document,
            string name,
            ICollection<ElementId> idsCategories,
            IList<FilterRule> rules)
        {
            return Autodesk.Revit.DB.ParameterFilterElement.Create(
                document,
                name,
                idsCategories,
                new ElementParameterFilter(rules));
        }
    }
}