﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;

namespace CoLa.BimEd.Infrastructure.Revit.RevitVersions.Relevant;

public class Revit2021
{
    public class MEPModel
    {
        public static IEnumerable<ElectricalSystem> GetElectricalSystems(
            FamilyInstance familyInstance)
        {
            return familyInstance.MEPModel?.GetElectricalSystems();
        }

        public static IEnumerable<ElectricalSystem> GetAssignedElectricalSystems(
            FamilyInstance familyInstance)
        {
            return familyInstance.MEPModel?.GetAssignedElectricalSystems();
        }

        public static IEnumerable<ElectricalSystem> GetElectricalSystems(
            Autodesk.Revit.DB.MEPModel mepModel)
        {
            return mepModel?.GetElectricalSystems();
        }

        public static IEnumerable<ElectricalSystem> GetAssignedElectricalSystems(
            Autodesk.Revit.DB.MEPModel mepModel)
        {
            return mepModel?.GetAssignedElectricalSystems();
        }
    }

    public class FormatOptions
    {
        public static void SetMetersAsUnitType(Autodesk.Revit.DB.FormatOptions formatOptions) =>
            formatOptions.SetUnitTypeId(UnitTypeId.Meters);
    }

    public class UnitUtils
    {
        public static double ConvertFromInternalUnitsToMillimeters(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertFromInternalUnits(value, UnitTypeId.Millimeters);

        public static double ConvertFromInternalUnitsToOhmMeters(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertFromInternalUnits(value, UnitTypeId.OhmMeters);

        public static double ConvertFromInternalUnitsToVoltAmperes(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertFromInternalUnits(value, UnitTypeId.VoltAmperes);

        public static double ConvertFromInternalUnitsToVolts(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertFromInternalUnits(value, UnitTypeId.Volts);

        public static double ConvertFromInternalUnitsToWatts(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertFromInternalUnits(value, UnitTypeId.Watts);

        public static double ConvertToInternalUnits(double value, Parameter targetParameter) =>
            Autodesk.Revit.DB.UnitUtils.ConvertToInternalUnits(value, targetParameter.GetUnitTypeId());

        public static double ConvertToInternalUnitsFromMillimeters(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertToInternalUnits(value, UnitTypeId.Millimeters);

        public static double ConvertToInternalUnitsFromVolts(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertToInternalUnits(value, UnitTypeId.Volts);
            
        public static double ConvertToInternalUnitsFromVoltAmperes(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertToInternalUnits(value, UnitTypeId.VoltAmperes);
            
        public static double ConvertToInternalUnitsFromWatts(double value) =>
            Autodesk.Revit.DB.UnitUtils.ConvertToInternalUnits(value, UnitTypeId.Watts);
    }
}