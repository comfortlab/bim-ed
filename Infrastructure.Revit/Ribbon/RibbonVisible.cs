﻿namespace CoLa.BimEd.Infrastructure.Revit.Ribbon;

public enum RibbonVisible
{
    All,
    Project,
    Family
}