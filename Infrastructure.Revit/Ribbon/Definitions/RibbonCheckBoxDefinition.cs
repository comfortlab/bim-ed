﻿using CoLa.BimEd.Infrastructure.Revit.Ribbon.Bindings;

namespace CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

public class RibbonCheckBoxDefinition : IRibbonDefinition
{
    public string Text { get; set; }
    public RibbonCheckedBinding RibbonCheckedBinding { get; set; }
}