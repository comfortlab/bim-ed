﻿using System.Collections.Generic;
using System.Windows.Data;
using Autodesk.Windows;

namespace CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

public class RibbonTextBoxDefinition : IRibbonDefinition
{
    public double Width { get; set; }
    public Binding Binding { get; set; }
    public List<RibbonTextBox> SubscribedTextBoxes = new();
}