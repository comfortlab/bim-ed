﻿namespace CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

public class RibbonLabelDefinition : IRibbonDefinition
{
    public string Text { get; set; }
}