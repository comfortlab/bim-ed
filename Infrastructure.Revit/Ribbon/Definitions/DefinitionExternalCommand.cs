using System;
using System.Drawing;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;

namespace CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

public abstract class DefinitionExternalCommand : IExternalCommand
{
    private readonly string _commandName;
    private readonly AppInfo _appInfo;
        
    protected readonly IServiceProvider ServiceProvider;
    protected readonly AssemblyCommands AssemblyCommands;
    protected readonly RevitContext RevitContext;
    protected readonly Logger Logger;
    protected Document Document;
    protected ExternalCommandData ExternalCommandData;
    protected ElementSet Elements;
    protected string Message;
    
    protected abstract Result Execute();
        
    public CommandDefinition Definition { get; }

    protected DefinitionExternalCommand(
        string caption, Bitmap largeImage = null, Bitmap image = null,
        string longDescription = null, string toolTipText = null, Bitmap toolTipImage = null)
    {
        _commandName = GetType().Name;
        
        Definition = new CommandDefinition
        {
            Name = _commandName,
            Caption = caption,
            LargeImage = largeImage,
            Image = image,
            LongDescription = longDescription,
            ToolTipText = toolTipText,
            ToolTipImage = toolTipImage,
        };
        
        ServiceProvider = DI.ServiceProvider;
        AssemblyCommands = ServiceProvider.Get<AssemblyCommands>();
        RevitContext = ServiceProvider.Get<RevitContext>();
        Logger = ServiceProvider.Get<Logger>();
        
        _appInfo = ServiceProvider.Get<AppInfo>();
    }
        
    public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
    {
        try
        {
            ExternalCommandData = commandData;
            Message = message;
            Elements = elements;
            Document = ExternalCommandData.Application.ActiveUIDocument.Document;

            Logger.Info(
                $"Command: {_commandName}\n\t" +
                $"BIM ED: {_appInfo.Version}, " +
                $"Revit: {RevitContext.Version}, " +
                $".NET Framework: {_appInfo.DotNetFrameworkVersion}, " +
                $"Session: {_appInfo.CurrentSessionGuid}\n\t" +
                $"Document: {Document.Title}");
                
            return Execute();
        }
        catch (OperationCanceledException)
        {
            Logger.Warn($"{GetType().Name} was canceled.");
            return Result.Cancelled;
        }
        catch (Exception exception)
        {
            Logger.Error(exception);
            return Result.Failed;
        }
        finally
        {
            try
            {
                // RibbonBindingProperties.UpdateItemValues();
                // DialogStackView.Reset();
                // ProgressBarController.Close();
                // ReportErrors.Reset();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }
    }
}