﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CoLa.BimEd.Infrastructure.Framework.Logs;

namespace CoLa.BimEd.Infrastructure.Revit.Ribbon;

public class RibbonVisibleUtils
{
    private readonly UIControlledApplication _application;
    private readonly Logger _logger;

    public RibbonVisibleUtils(UIControlledApplication application, Logger logger)
    {
        _application = application;
        _logger = logger;
    }
        
    public void SetVisibleRibbonPanelHandler(RibbonPanel ribbonPanel, RibbonVisible ribbonVisible)
    {
        if (ribbonVisible != RibbonVisible.All)
        {
            _application.ViewActivated += (sender, e) =>
            {
                try
                {
                    SetVisibleRibbonPanel(e.Document, ribbonPanel, ribbonVisible);
                }
                catch (Exception exception)
                {
                    _logger.Error(exception);
                }
            };
        }
    }

    public void SetVisibleRibbonPanel(Document document, RibbonPanel ribbonPanel, RibbonVisible ribbonVisible)
    {
        ribbonPanel.Visible = ribbonVisible switch
        {
            RibbonVisible.Family => document.IsFamilyDocument,
            RibbonVisible.Project => !document.IsFamilyDocument,
            RibbonVisible.All => true,
            _ => throw new ArgumentOutOfRangeException(nameof(ribbonVisible), ribbonVisible, null)
        };
    }

    public void SetVisibleRibbonItemHandler(RibbonItem ribbonItem, RibbonVisible ribbonVisible)
    {
        if (ribbonVisible != RibbonVisible.All)
        {
            _application.ViewActivated += (sender, e) =>
            {
                try
                {
                    SetVisibleRibbonItem(e.Document, ribbonItem, ribbonVisible);
                }
                catch (Exception exception)
                {
                    _logger.Error(exception);
                }
            };
        }
    }

    public void SetVisibleRibbonItem(Document document, RibbonItem ribbonItem, RibbonVisible ribbonVisible)
    {
        ribbonItem.Visible = ribbonVisible switch
        {
            RibbonVisible.Family => document.IsFamilyDocument,
            RibbonVisible.Project => !document.IsFamilyDocument,
            RibbonVisible.All => true,
            _ => throw new ArgumentOutOfRangeException(nameof(ribbonVisible), ribbonVisible, null)
        };
    }

    public void SetVisibleRibbonItemOnViewHandler(RibbonItem ribbonItem, Func<View, bool> checkFunc)
    {
        _application.ViewActivated += (sender, e) =>
        {
            try
            {
                SetVisibleRibbonItemOnView(ribbonItem, e.CurrentActiveView, checkFunc);
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
            }
        };
    }

    public void SetVisibleRibbonItemOnViewHandler(RibbonItem ribbonItem, ViewType checkViewType, Func<View, bool> checkFunc = null)
    {
        _application.ViewActivated += (sender, e) =>
        {
            try
            {
                SetVisibleRibbonItemOnView(ribbonItem, checkViewType, e.CurrentActiveView, checkFunc);
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
            }
        };
    }

    private static void SetVisibleRibbonItemOnView(RibbonItem ribbonItem, View currentView, Func<View, bool> checkFunc)
    {
        ribbonItem.Visible = checkFunc(currentView);
    }

    private static void SetVisibleRibbonItemOnView(RibbonItem ribbonItem, ViewType checkViewType, View currentView, Func<View, bool> checkFunc)
    {
        ribbonItem.Visible = checkViewType == currentView.ViewType && (checkFunc?.Invoke(currentView) ?? true);
    }
}