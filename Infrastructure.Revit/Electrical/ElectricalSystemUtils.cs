﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Comparers;
using CoLa.BimEd.Infrastructure.Revit.RevitVersions;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Infrastructure.Revit.Electrical;

public class ElectricalSystemUtils
{
    private readonly Document _document;
    private readonly RevitVersionResolver _revitVersionResolver;

    public ElectricalSystemUtils(Document document, RevitVersionResolver revitVersionResolver)
    {
        _document = document;
        _revitVersionResolver = revitVersionResolver;
    }

    public IEnumerable<FamilyInstance> GetAllBaseEquipments()
    {
        var electricalSystems = _document.GetElectricalSystems();
        return electricalSystems.Select(n => n.BaseEquipment).Distinct(new ElementComparer<FamilyInstance>());
    }
        
    public IEnumerable<ElectricalSystem> GetAllChildElectricalCircuits(FamilyInstance baseEquipment, ElectricalSystemType electricalSystemType = ElectricalSystemType.UndefinedSystemType)
    {
        var allChildElectricalSystems = new List<ElectricalSystem>();
        var childElectricalSystems = new Stack<ElectricalSystem>(GetChildElectricalCircuits(baseEquipment, electricalSystemType));

        while (childElectricalSystems.Any())
        {
            var electricalSystem = childElectricalSystems.Pop();
            allChildElectricalSystems.Add(electricalSystem);

            foreach (var element in electricalSystem.Elements)
            {
                if (element is not FamilyInstance familyInstance)
                    continue;

                var elementChildElectricalSystems = GetChildElectricalCircuits(familyInstance);

                foreach (var elementChildElectricalSystem in elementChildElectricalSystems)
                    childElectricalSystems.Push(elementChildElectricalSystem);
            }
        }

        return allChildElectricalSystems;
    }

    public IEnumerable<ElectricalSystem> GetChildElectricalCircuits(FamilyInstance baseEquipment, ElectricalSystemType electricalSystemType = ElectricalSystemType.UndefinedSystemType) =>
        baseEquipment?.MEPModel != null
            ? electricalSystemType != ElectricalSystemType.UndefinedSystemType
                ? _revitVersionResolver.GetAssignedElectricalSystems(baseEquipment).Where(n => n.SystemType == electricalSystemType)
                : _revitVersionResolver.GetAssignedElectricalSystems(baseEquipment)
            : new List<ElectricalSystem>();

    public IEnumerable<ElectricalSystem> GetParentElectricalCircuits(FamilyInstance baseEquipment, ElectricalSystemType electricalSystemType = ElectricalSystemType.UndefinedSystemType) =>
        baseEquipment?.MEPModel != null
            ? electricalSystemType != ElectricalSystemType.UndefinedSystemType
                ? GetAllParentElectricalCircuits(baseEquipment).Where(n => n.SystemType == electricalSystemType)
                : GetAllParentElectricalCircuits(baseEquipment)
            : new List<ElectricalSystem>();

    private IEnumerable<ElectricalSystem> GetAllParentElectricalCircuits(FamilyInstance baseEquipment) =>
        _revitVersionResolver.GetElectricalSystems(baseEquipment)
            .Where(n => n.BaseEquipment?.Id != baseEquipment.Id);

    public IEnumerable<Connector> GetElementConnectors(ElectricalSystem electricalSystem)
    {
        var idBaseEquipment = electricalSystem.BaseEquipment?.Id;
            
        return electricalSystem.GetConnectors()
            .Select(n => n.AllRefs?.OfType<Connector>().FirstOrDefault())
            .Where(n => n != null && n.Owner.Id != idBaseEquipment)
            .ToList();
    }
}