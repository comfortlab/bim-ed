﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Math;
using CoLa.BimEd.Infrastructure.Revit.Parameters;

namespace CoLa.BimEd.Infrastructure.Revit.Electrical
{
    public static class ConnectorUtils
    {
        public static Connector GetConnector(this FamilyInstance familyInstance, Domain domain = Domain.DomainUndefined) =>
            GetConnectors(familyInstance, domain)
                .OrderBy(n => n.Id)
                .FirstOrDefault();

        public static Connector GetConnector(this FamilyInstance familyInstance, ElectricalSystem electricalSystem)
        {
            return GetConnectors(electricalSystem)
                .FirstOrDefault(n =>
                    n.AllRefs != null &&
                    n.AllRefs.OfType<Connector>().First().Owner.Id == familyInstance.Id)
                ?.AllRefs.OfType<Connector>()
                .First();
        }

        public static Connector GetPrimaryElectricalConnector(this Element element)
        {
            Connector connector = null;

            if (element is FamilyInstance)
            {
                connector = element.GetConnectors()
                    .FirstOrDefault(n =>
                        n.Domain == Domain.DomainElectrical &&
                        n.GetMEPConnectorInfo().IsPrimary);
            }

            return connector;
        }

        public static IEnumerable<Connector> GetConnectors(this Element element, Domain domain = Domain.DomainUndefined)
        {
            var connectors = element.GetConnectorManager()?.Connectors.OfType<Connector>();

            if (connectors == null)
                return new List<Connector>();

            return domain != Domain.DomainUndefined
                ? connectors.Where(n => n.Domain == domain)
                : connectors;
        }

        public static ConnectorManager GetConnectorManager(this Element element)
        {
            return element switch
            {
                CableTrayConduitBase cableTrayConduitBase => cableTrayConduitBase.ConnectorManager,
                FamilyInstance familyInstance => familyInstance.MEPModel?.ConnectorManager,
                ElectricalSystem electricalSystem => electricalSystem.ConnectorManager,
                _ => null
            };
        }

        public static Connector GetRefConnector(this Connector connector)
        {
            foreach (Connector connRef in connector.AllRefs)
            {
                if (connRef.Owner.Id != connector.Owner.Id)
                    return connRef;
            }

            return null;
        }

        public static ConnectorElement GetConnectorElement(this Connector connector, Document familyDocument = null)
        {
            if (connector.Owner is not FamilyInstance familyInstance)
                return null;

            var connectorIndexes = familyInstance.GetConnectors().Select(n => n.Id).OrderBy(n => n).ToList();
            var id = connector.Id;
            var indexId = connectorIndexes.IndexOf(id);

            familyDocument ??=
                familyInstance.Document.IsModifiable ||
                familyInstance.Document.IsReadOnly
                    ? null
                    : familyInstance.Document.EditFamily(familyInstance.Symbol.Family);

            if (familyDocument == null)
                return null;

            var connectorElements = new FilteredElementCollector(familyDocument)
                .WhereElementIsNotElementType()
                .OfClass(typeof(ConnectorElement))
                .OfType<ConnectorElement>()
                .OrderBy(n => n.Id.IntegerValue)
                .ToList();

            return indexId < connectorElements.Count
                ? connectorElements[indexId]
                : null;
        }

        public static int GetValueAsInteger(
            this ConnectorElement connectorElement,
            FamilyInstance familyInstance,
            BuiltInParameter builtInParameter)
        {
            var parameter = connectorElement.GetConnectorElementParameter(familyInstance, builtInParameter, out var _);
            return parameter?.GetValueAsInt() ?? 0;
        }

        public static double GetVoltAmperesFromInternal(
            this ConnectorElement connectorElement,
            FamilyInstance familyInstance,
            BuiltInParameter builtInParameter)
        {
            var parameter = connectorElement.GetConnectorElementParameter(familyInstance, builtInParameter, out var _);
            var valueInternal = parameter?.GetValueAsDouble() ?? 0;
            return valueInternal.Convert(Converting.VoltAmperesFromInternal);
        }

        public static double GetVoltAmperesFromInternal(
            this MEPFamilyConnectorInfo connectorInfo,
            BuiltInParameter builtInParameter)
        {
            var parameterValue = connectorInfo.GetConnectorParameterValue(new ElementId(builtInParameter));
            var valueInternal = ((DoubleParameterValue)parameterValue)?.Value ?? 0;
            return valueInternal.Convert(Converting.VoltAmperesFromInternal);
        }

        public static int GetIntegerValue(
            this MEPFamilyConnectorInfo connectorInfo,
            BuiltInParameter builtInParameter)
        {
            var parameterValue = connectorInfo?.GetConnectorParameterValue(new ElementId(builtInParameter));
            return ((IntegerParameterValue)parameterValue)?.Value ?? 0;
        }

        public static ElementId GetElementIdValue(
            this MEPFamilyConnectorInfo connectorInfo,
            BuiltInParameter builtInParameter)
        {
            var parameterValue = connectorInfo?.GetConnectorParameterValue(new ElementId(builtInParameter));
            return ((ElementIdParameterValue)parameterValue)?.Value ?? ElementId.InvalidElementId;
        }

        public static Parameter GetConnectorElementParameter(
            this ConnectorElement connectorElement,
            FamilyInstance familyInstance,
            BuiltInParameter builtInParameter,
            out AssociatedType associatedType)
        {
            var connectorParameter = connectorElement?.get_Parameter(builtInParameter);

            if (connectorParameter == null)
            {
                associatedType = AssociatedType.None;
                return null;
            }

            var fDoc = connectorElement.Document;
            var associatedParameter = fDoc.FamilyManager.GetAssociatedFamilyParameter(connectorParameter);

            switch (associatedParameter)
            {
                case null:
                    associatedType = AssociatedType.Self;
                    return connectorParameter;

                default:
                    return associatedParameter.IsShared
                        ? GetAssociatedParameterShared(familyInstance, associatedParameter, out associatedType)
                        : GetAssociatedParameter(familyInstance, associatedParameter, out associatedType);
            }
        }

        private static Parameter GetAssociatedParameterShared(
            FamilyInstance familyInstance,
            FamilyParameter associatedParameter,
            out AssociatedType associatedType)
        {
            var guid = associatedParameter.GUID;

            if (associatedParameter.IsInstance)
            {
                associatedType = associatedParameter.IsDeterminedByFormula
                    ? AssociatedType.ParameterInstanceFormula
                    : AssociatedType.ParameterInstance;

                return familyInstance.get_Parameter(guid);
            }
            else
            {
                associatedType = associatedParameter.IsDeterminedByFormula
                    ? AssociatedType.ParameterTypeFormula
                    : AssociatedType.ParameterType;

                return familyInstance.Symbol.get_Parameter(guid);
            }
        }

        private static Parameter GetAssociatedParameter(FamilyInstance familyInstance,
            FamilyParameter associatedParameter,
            out AssociatedType associatedType)
        {
            var name = associatedParameter.Definition?.Name;

            if (associatedParameter.IsInstance)
            {
                associatedType = associatedParameter.IsDeterminedByFormula
                    ? AssociatedType.ParameterInstanceFormula
                    : AssociatedType.ParameterInstance;

                return familyInstance.LookupParameter(name);
            }
            else
            {
                associatedType = associatedParameter.IsDeterminedByFormula
                    ? AssociatedType.ParameterTypeFormula
                    : AssociatedType.ParameterType;

                return familyInstance.Symbol.LookupParameter(name);
            }
        }

        public static ConnectorElement GetConnectorElementParameters(this Connector connector, Document familyDocument = null)
        {
            if (connector.Owner is not FamilyInstance familyInstance)
                return null;

            var connectorIndexes = familyInstance.GetConnectors().Select(n => n.Id).OrderBy(n => n).ToList();
            var id = connector.Id;
            var indexId = connectorIndexes.IndexOf(id);
            var familySymbol = familyInstance.Symbol;
            var family = familySymbol.Family;

            familyDocument ??= familyInstance.Document.EditFamily(family);

            var connectorElements = new FilteredElementCollector(familyDocument)
                .WhereElementIsNotElementType()
                .OfClass(typeof(ConnectorElement))
                .OfType<ConnectorElement>()
                .OrderBy(n => n.Id.IntegerValue)
                .ToList();

            var connectorElement = indexId < connectorElements.Count
                ? connectorElements[indexId]
                : null;

            // TODO : | Petrov | Определить параметры влияющие на соединитель из семейства.

            if (connectorElement == null)
                return null;

            switch (connectorElement.SystemClassification)
            {
                case MEPSystemClassification.PowerBalanced:

                    break;

                case MEPSystemClassification.PowerUnBalanced:

                    break;
            }

            return connectorElement;
        }

        public static Connector GetRefConnector(this Connector connector, Domain domain)
        {
            return connector.Domain != Domain.DomainUndefined &&
                   connector.Domain == domain &&
                   connector.ConnectorType != ConnectorType.MasterSurface &&
                   connector.IsConnected
                ? GetRefConnector(connector)
                : null;
        }

        public static Element GetRefElement(this Connector connector)
        {
            Element element = null;

            foreach (Connector connRef in connector.AllRefs)
            {
                if (connRef.Owner.Id != connector.Owner.Id)
                {
                    element = connRef.Owner;
                    break;
                }
            }

            return element;
        }

        public static Element GetRefElement(this Connector connector, Domain domain)
        {
            return connector.Domain != Domain.DomainUndefined &&
                   connector.Domain == domain &&
                   connector.ConnectorType != ConnectorType.MasterSurface &&
                   connector.IsConnected
                ? GetRefElement(connector)
                : null;
        }
    }

    public enum AssociatedType
    {
        None,
        Self,
        ParameterInstance,
        ParameterType,
        ParameterInstanceFormula,
        ParameterTypeFormula
    }
}