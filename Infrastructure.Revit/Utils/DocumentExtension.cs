﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Electrical;

namespace CoLa.BimEd.Infrastructure.Revit.Utils;

public static class DocumentExtension
{
    public static void ExecuteTransactionGroup(this Document document, Action action, string transactionGroupName)
    {
        if (TransactionIsOpened(document))
        {
            action?.Invoke();
            return;
        }

        using var transactionGroup = new TransactionGroup(document, $"BIM ED: {transactionGroupName}");

        try
        {
            transactionGroup.Start();
            action();
            transactionGroup.Assimilate();
        }
        catch
        {
            if (transactionGroup.HasStarted()) transactionGroup.RollBack();
            throw;
        }
    }

    public static void ExecuteTransaction(this Document document, Action action, string transactionName, bool? showWarnings = null)
    {
        if (TransactionIsOpened(document))
        {
            action?.Invoke();
            return;
        }

        using var transaction = new Transaction(document, $"BIM ED: {transactionName}");

        try
        {
            if (transaction.Start() == TransactionStatus.Started)
            {
                // FailureController.SetWarningDisplay(transaction, showWarnings ?? AppInfo.Revit.ShowWarnings);
                action?.Invoke();
            }

            if (transaction.Commit() != TransactionStatus.Committed)
            {
                transaction.RollBack();
            }
        }
        catch (Exception)
        {
            if (transaction.HasStarted())
            {
                transaction.RollBack();
            }

            throw;
        }
    }

    public static void ExecuteSubTransaction(this Document document, Action action)
    {
        using var subTransaction = new SubTransaction(document);

        try
        {
            if (subTransaction.Start() == TransactionStatus.Started)
            {
                action?.Invoke();
            }

            if (subTransaction.Commit() != TransactionStatus.Committed)
            {
                subTransaction.RollBack();
            }
        }
        catch (Exception)
        {
            if (subTransaction.HasStarted())
            {
                subTransaction.RollBack();
            }

            throw;
        }
    }

    private static bool TransactionIsOpened(this Document document) => document.IsReadOnly || document.IsModifiable;
    
    public static Element GetElement(this Document document, ElementId elementId) => document.GetElement(elementId);
    public static Element GetElement(this Document document, int id) => document.GetElement(new ElementId(id));
    public static T GetElementAs<T>(this Document document, Element element) => document.GetElement(element.Id) is T t ? t : default;
    public static T GetElementAs<T>(this Document document, ElementId elementId) => document.GetElement(elementId) is T t ? t : default;
    public static T GetElementAs<T>(this Document document, int id) => document.GetElement(new ElementId(id)) is T t ? t : default;

    public static List<T> GetElementsOfCategory<T>(
        this Document document, BuiltInCategory builtInCategory)
    {
        using var collector = document.NewElementCollector();
        return collector.OfCategory(builtInCategory).OfType<T>().ToList();
    }
    
    public static List<T> GetElementsOfCategory<T>(
        this Document document, BuiltInCategory builtInCategory, ElementFilter elementFilter)
    {
        using var collector = document.NewElementCollector();
        return collector.OfCategory(builtInCategory).WherePasses(elementFilter).OfType<T>().ToList();
    }

    public static List<T> GetElementsOfCategoryIsNotElementType<T>(
        this Document document, BuiltInCategory builtInCategory)
    {
        using var collector = document.NewElementCollector();
        return collector.OfCategory(builtInCategory)
            .WhereElementIsNotElementType()
            .OfType<T>().ToList();
    }
    
    public static List<T> GetElementsOfClass<T>(
        this Document document)
    {
        using var collector = document.NewElementCollector();
        return collector.OfClass(typeof(T)).OfType<T>().ToList();
    }
    
    public static List<T> GetElementsOfClass<T>(
        this Document document, ElementFilter elementFilter)
    {
        using var collector = document.NewElementCollector();
        return collector.OfClass(typeof(T)).WherePasses(elementFilter).OfType<T>().ToList();
    }
    
    public static List<T> GetElementsOfClassIsElementType<T>(
        this Document document)
    {
        using var collector = document.NewElementCollector();
        return collector.OfClass(typeof(T)).WhereElementIsElementType().OfType<T>().ToList();
    }
    
    public static List<T> GetElementsOfClassIsNotElementType<T>(
        this Document document)
    {
        using var collector = document.NewElementCollector();
        return collector.OfClass(typeof(T)).WhereElementIsNotElementType().OfType<T>().ToList();
    }
    
    public static List<ElectricalSystem> GetElectricalSystems(
        this Document document, bool includeSpare = false)
    {
        return includeSpare
            ? document.GetElementsOfClass<ElectricalSystem>()
            : document.GetElementsOfCategory<ElectricalSystem>(BuiltInCategory.OST_ElectricalCircuit);
    }

    public static List<Family> GetFamilies(this Document document) => document.GetElementsOfClass<Family>();
    public static List<Level> GetLevels(this Document document) => document.GetElementsOfCategoryIsNotElementType<Level>(BuiltInCategory.OST_Levels);
    public static IEnumerable<Room> GetRooms(
        this Document document, bool isPlacedOnly = false)
    {
        var allRooms = document.NewElementCollector()
            .OfCategory(BuiltInCategory.OST_Rooms)
            .OfType<Room>();

        return isPlacedOnly
            ? allRooms.Where(n => n.Location != null)
            : allRooms;
    }
    
    public static FilteredElementCollector NewElementCollector(this Document document) => new(document);
    
    public static void DeleteById(this Document document, int id) => Delete(document, new ElementId(id));
    public static void Delete(this Document document, ElementId elementId)
    {
        try
        {
            if (document.GetElement(elementId) != null) document.Delete(elementId);
        }
        catch
        {
            Debug.WriteLine($"[{elementId}]", typeof(DocumentExtension).Namespace);
            throw;
        }
    }
}