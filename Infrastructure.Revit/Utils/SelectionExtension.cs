using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

namespace CoLa.BimEd.Infrastructure.Revit.Utils;

public static class SelectionExtension
{
    public static void SelectElements(this Selection selection, IEnumerable<int> elementIds)
    {
        var idsElements = elementIds.Select(n => new ElementId(n)).ToList();
        selection.SetElementIds(idsElements);
    }

    public static void SelectElements(this Selection selection, IEnumerable<Element> elements)
    {
        var idsElements = elements.Select(n => n.Id).ToList();
        selection.SetElementIds(idsElements);
    }
}