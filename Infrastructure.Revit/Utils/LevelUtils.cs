﻿using System;
using System.Linq;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit.Utils;

public class LevelUtils
{
    public static Level GetLevel(FamilyInstance familyInstance)
    {
        var document = familyInstance.Document;

        Level level = null;

        if (familyInstance.LevelId.IntegerValue != -1)
            level = (Level) document.GetElement(familyInstance.LevelId);
        else if (familyInstance.Host != null && familyInstance.Host.LevelId.IntegerValue != -1)
            level = (Level) document.GetElement(familyInstance.Host.LevelId);
        else if (familyInstance.Room != null)
            level = (Level) document.GetElement(familyInstance.Room.LevelId);
        else if (familyInstance.Space != null)
            level = (Level) document.GetElement(familyInstance.Space.LevelId);
        else
        {
            var pointZ = GetPointZ(familyInstance);
            var levels = new FilteredElementCollector(document)
                .OfCategory(BuiltInCategory.OST_Levels)
                .WhereElementIsNotElementType()
                .Select(n => (Level) n)
                .OrderByDescending(n => n.Elevation)
                .ToList();

            foreach (var l in levels.Where(l => l.Elevation < pointZ))
            {
                level = l;
                break;
            }

            if (level == null)
                level = levels.LastOrDefault();
        }

        return level;
    }

    private static double GetPointZ(FamilyInstance familyInstance)
    {
        switch (familyInstance.Location)
        {
            case LocationPoint locationPoint:
                return locationPoint.Point.Z;

            case LocationCurve locationCurve:
                return locationCurve.Curve.GetEndPoint(0).Z;

            default:
                throw new ArgumentOutOfRangeException(nameof(familyInstance.Location), familyInstance.Location, new ArgumentOutOfRangeException().Message);
        }
    }
}