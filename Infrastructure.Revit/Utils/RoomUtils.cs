﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;

namespace CoLa.BimEd.Infrastructure.Revit.Utils;

public class RoomUtils
{
    public static Room GetRoom(Element element, Level level, IEnumerable<Room> roomsOfLevel)
    {
        if (element.Location is not LocationPoint locationPoint)
            return null;
            
        var point = locationPoint.Point;

        if (element is FamilyInstance familyInstance)
        {
            if (familyInstance.Room != null)
                return familyInstance.Room;

            var projectPoint = new XYZ(point.X, point.Y, level.Elevation);
            var roomAtPoint = familyInstance.Document.GetRoomAtPoint(projectPoint);

            if (roomAtPoint != null)
                return roomAtPoint;
        }

        var projectFloorPoint = new XYZ(point.X, point.Y, level.Elevation);

        return roomsOfLevel.FirstOrDefault(n => IsPointInSpatialElement(n, projectFloorPoint, 0.1));
    }

    private static bool IsPointInSpatialElement(SpatialElement spatialElement, XYZ projectFloorPoint, double tolerance)
    {
        var line = Line.CreateBound(projectFloorPoint, new XYZ(projectFloorPoint.X + 1000000, projectFloorPoint.Y, projectFloorPoint.Z));
        var intersectionCount = 0;
        var listListBoundaries = spatialElement.GetBoundarySegments(new SpatialElementBoundaryOptions());

        foreach (var boundarySegments in listListBoundaries)
        {
            foreach (var boundarySegment in boundarySegments)
            {
                var curve = boundarySegment.GetCurve();
                var projectResult = curve.Project(projectFloorPoint);

                if (projectResult.Distance < tolerance)
                    return true;

                var intersectResult = line.Intersect(curve);

                if (intersectResult == SetComparisonResult.Overlap)
                    intersectionCount++;
            }
        }

        return intersectionCount % 2 == 1;
    }
}