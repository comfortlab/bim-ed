﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class ElectricalElementProxy : ElectricalBase
{
    public ElectricalElementProxy() :
        this(VirtualRevitId, null) { }

    public ElectricalElementProxy(int revitId, string name) :
        this(Guid.NewGuid(), revitId, name) { }

    public ElectricalElementProxy(Guid guid, int revitId, string name) :
        base(guid, revitId, name) { }
    
    public BuiltInCategoryProxy Category { get; set; }
    public string ServiceType { get; set; }
    public ConnectorProxy Connector { get; set; }
    public XYZProxy Origin => Connector?.Origin;
    public int LevelId { get; set; }
    public int RoomId { get; set; }
    public LevelProxy Level { get; set; }
    public RoomProxy Room { get; set; }
    public bool InRoom => (Room?.RevitId ?? -1) > 0;
    
    /// <summary>
    /// <see cref="ElectricalElementProxy"> must not have children</see>
    /// </summary>
    /// <param name="electrical"></param>
    public override void Add(ElectricalBase electrical) => ThrowChildrenException();

    /// <summary>
    /// <see cref="ElectricalElementProxy"> must not have children</see>
    /// </summary>
    /// <param name="electricals"></param>
    public override void AddRange(IEnumerable<ElectricalBase> electricals) => ThrowChildrenException();

    /// <summary>
    /// <see cref="ElectricalElementProxy"> must not have children</see>
    /// </summary>
    /// <param name="index"></param>
    /// <param name="electrical"></param>
    public override void Insert(int index, ElectricalBase electrical) => ThrowChildrenException();

    /// <summary>
    /// <see cref="ElectricalElementProxy"/> must not have children
    /// </summary>
    /// <param name="electrical"></param>
    public override void Remove(ElectricalBase electrical) => ThrowChildrenException();

    /// <summary>
    /// <see cref="ElectricalElementProxy"/> must not have children
    /// </summary>
    /// <param name="electrical1"></param>
    /// <param name="electrical2"></param>
    public override void Move(ElectricalBase electrical1, ElectricalBase electrical2) => ThrowChildrenException();

    private static void ThrowChildrenException() => throw new NotImplementedException(
        $"{nameof(ElectricalElementProxy)} must not have children");
}