﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

/// <summary>
/// Combinations of <see cref="SwitchBoardUnit"/> - low voltage panels.<br/>
/// Any <see cref="SwitchBoardUnit"/> is a part of <see cref="SwitchBoard"/>.<br/>
/// Any <see cref="SwitchBoard"/> must have one or more <see cref="SwitchBoardUnit"/>.<br/>
/// Less then 1000 V.
/// </summary>
public class SwitchBoard : EquipmentSet<SwitchBoardUnit>
{
    public SwitchBoard(
        int revitId, string name) :
        base(revitId, name)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoard(
        int revitId, string name,
        ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.UndefinedSystemType) :
        base(revitId, name)
    {
        var firstSection = new SwitchBoardUnit(revitId, name, systemType);
        AddUnit(firstSection);
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoard(
        int revitId, string name,
        params SwitchBoardUnit[] sections) :
        base(revitId, name, sections)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoard(
        int revitId, string name,
        DistributionSystemProxy distributionSystem) :
        base(revitId, name, distributionSystem)
    {
        var firstSection = DistributionSystem != null
            ? new SwitchBoardUnit(revitId, name, DistributionSystem)
            : new SwitchBoardUnit(revitId, name, ElectricalSystemTypeProxy.PowerCircuit);
        
        AddUnit(firstSection);
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }
    
    public SwitchBoardUnitProducts SwitchBoardUnitProducts { get; }
}

public class PanelInput
{
    public PanelInput(ElectricalBase electrical)
    {
        Electrical = electrical;
    }

    public ElectricalBase Electrical { get; set; }
}

public class CabinetSettings : IPropertyPrototype<CabinetSettings>
{
    // public CabinetType CabinetType { get; set; }
    public double BusCurrent { get; set; }
    // public EnclosureType EnclosureType { get; set; }
    public Material Material { get; set; }
    // public MountingType MountingType { get; set; }
    public bool AutomaticTransferSwitch { get; set; }

    public void PullProperties(CabinetSettings prototype)
    {
        // this.CabinetType = prototype.CabinetType;
        this.BusCurrent = prototype.BusCurrent;
        // this.EnclosureType = prototype.EnclosureType;
        this.Material = prototype.Material;
        // this.MountingType = prototype.MountingType;
        this.AutomaticTransferSwitch = prototype.AutomaticTransferSwitch;
    }
}