﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public enum Function
{
    BusRiser,
    CableConnection,
    GeneralProtection,
    GeneratorIncomer,
    LineFeeder,
    LineProtection,
    Metering,
    MotorProtection,
    SectionSwitch,
    Special,
    TransformerProtection,
}