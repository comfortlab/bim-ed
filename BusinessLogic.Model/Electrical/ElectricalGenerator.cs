﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class ElectricalGenerator :
    ElectricalSource,
    IPropertyPrototype<ElectricalGenerator>
{
    public ElectricalGenerator() { }
        
    public ElectricalGenerator(
        int revitId,
        string name,
        ElectricalSystemTypeProxy electricalSystemType) :
        base(revitId, name, electricalSystemType) { }
        
    public ElectricalGenerator(
        int revitId,
        string name,
        DistributionSystemProxy distributionSystem) :
        base(revitId, name, distributionSystem) { }

    public void PullProperties(ElectricalGenerator prototype)
    {
        base.PullProperties(prototype);
    }
}