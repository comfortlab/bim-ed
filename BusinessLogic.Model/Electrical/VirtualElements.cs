﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class VirtualElements
{
    private readonly Dictionary<Type, PropertyInfo> _collectionProperties;

    public VirtualElements()
    {
        var equipmentType = typeof(ElectricalEquipmentProxy);
        
        _collectionProperties = GetType().GetProperties()
            .Where(x =>
                x.PropertyType.IsGenericType &&
                x.PropertyType.GenericTypeArguments[0].IsSubclassOf(equipmentType))
            .ToDictionary(x => x.PropertyType.GenericTypeArguments[0]);
    }

    public int Id { private get; set; }
    public List<ElectricalGenerator> Generators { get; set; } = new();
    public List<ElectricalNetwork> Networks { get; set; } = new();
    public List<ElectricalSystemProxy> ElectricalSystems { get; set; } = new();
    public List<SwitchBoardUnit> SwitchboardUnits { get; set; } = new();
    public List<Transformer> Transformers { get; set; } = new();

    public void Add<T>(T equipment) where T : ElectricalEquipmentProxy
    {
        if (TryGetCollection<T>(out var collection))
            collection.Add(equipment);
    }

    public void Remove<T>(T equipment) where T : ElectricalEquipmentProxy
    {
        if (TryGetCollection<T>(out var collection))
            collection.Remove(equipment);
    }

    private bool TryGetCollection<T>(out IList collection) where T : ElectricalEquipmentProxy
    {
        var isTypeExists = _collectionProperties.TryGetValue(typeof(T), out var propertyInfo);
        collection = isTypeExists ? (IList)propertyInfo.GetValue(this) : null;
        return isTypeExists;
    }

    public IEnumerable<ElectricalEquipmentProxy> GetElectricalEquipments()
    {
        IEnumerable<ElectricalEquipmentProxy> electricalEquipments = new List<ElectricalEquipmentProxy>();
        
        return _collectionProperties.Values.Select(x => (IList)x.GetValue(this))
            .Aggregate(electricalEquipments, (current, value) => current.Concat(value.OfType<ElectricalEquipmentProxy>()));
    }
    
    public IEnumerable<ElectricalSource> GetElectricalSources() =>
        Networks.OfType<ElectricalSource>()
            .Concat(Generators);
    
    public int NewId() => --Id;
}