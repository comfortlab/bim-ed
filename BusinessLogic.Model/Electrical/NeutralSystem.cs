﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public enum NeutralSystem
{
    Isolated,
    Earthed
}