﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class OperatingMode : IPrototype<OperatingMode>, IPropertyPrototype<OperatingMode>
{
    public OperatingMode() { }
    public OperatingMode(string name) : this(Guid.NewGuid(), name) { }
    public OperatingMode(Guid guid, string name = null) { Guid = guid; Name = name; }
    private OperatingMode(OperatingMode prototype) : this(prototype.Guid) => this.PullProperties(prototype);
    public OperatingMode Clone() => new(this);
    public void PullProperties(OperatingMode prototype)
    {
        this.Name = prototype.Name;
        this.DisabledElements = prototype.DisabledElements.ToHashSet();
    }
    public Guid Guid { get; }
    public string Name { get; set; }
    // public Dictionary<ElementProxy, bool> ElementSwitches { get; set; } = new();
    public HashSet<int> DisabledElements { get; private set; } = new();
    public override string ToString() => $"[{Guid}] <{GetType().Name}> {Name}";
    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() && Equals((OperatingMode)obj);
    }
    private bool Equals(OperatingMode other) => Guid.Equals(other.Guid);
    public override int GetHashCode() => Guid.GetHashCode();

    public static IEqualityComparer<OperatingMode> GuidComparer { get; } = new GuidEqualityComparer();
    private sealed class GuidEqualityComparer : IEqualityComparer<OperatingMode>
    {
        public bool Equals(OperatingMode x, OperatingMode y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            return x.GetType() == y.GetType() && x.Guid.Equals(y.Guid);
        }
        public int GetHashCode(OperatingMode obj) => obj.Guid.GetHashCode();
    }
}