using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class MotorControlDevice : ElectricalEquipmentProxy
{
    public MotorControlDevice() : base() { }
    public MotorControlDevice(string name) : base(name) { }
    public MotorControlDevice(int revitId, string name) : base(revitId, name) { }

    public Product Product { get; set; }
    public List<Product> Accessories { get; set; }
}