﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Comparers;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Comparer;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Prototype;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public sealed class ElectricalSystemProxy : ElectricalBase,
    IPrototype<ElectricalSystemProxy>,
    IPropertyPrototype<ElectricalSystemProxy>
{
    private double _cableLength;
    private double _cableLengthInCableTray;
    private double _cableLengthMax;

    public CircuitTypeProxy CircuitType { get; set; } = CircuitTypeProxy.Circuit;
    public Topology Topology { get; set; } = Topology.Tree;
    public string CircuitNumber { get; set; }
    public string CircuitDesignation { get; set; }
    public string CableDesignation { get; set; }
    public string LoadName { get; set; }
        
    public ElectricalSystemProxy() : this(VirtualRevitId) { }
    public ElectricalSystemProxy(int revitId, string circuitNumber = null) : this(revitId, circuitNumber, ElectricalSystemTypeProxy.UndefinedSystemType) { }
    public ElectricalSystemProxy(int revitId, string circuitNumber, ElectricalSystemTypeProxy systemType) : base(revitId, circuitNumber)
    {
        CircuitNumber = circuitNumber;
        Products = new CircuitProducts(this);
        SetBaseConnector(new ConnectorProxy(this, 1, systemType));
    }
    public ElectricalSystemProxy(int revitId, string circuitNumber, DistributionSystemProxy distributionSystem) : base(revitId, circuitNumber)
    {
        CircuitNumber = circuitNumber;
        Products = new CircuitProducts(this);
        SetBaseConnector(new ConnectorProxy(this, 1, distributionSystem));
    }

    private ElectricalSystemProxy(ElectricalSystemProxy prototype)
    {
        this.Cabling = prototype.Cabling.Clone();
        this.Products = new CircuitProducts(this);
        this.Rooms = prototype.Rooms;

        PullProperties(prototype);
    }

    public ElectricalSystemProxy Clone() => new(prototype: this);

    public void PullProperties(ElectricalSystemProxy prototype)
    {
        this.SetBaseConnector(prototype.BaseConnector);
        this.Cabling.PullProperties(prototype.Cabling);
        this.CircuitType = prototype.CircuitType;
        this.Products.PullProperties(prototype.Products);
        this.Topology = prototype.Topology;
        this.CircuitPowerParameters = new CircuitPowerParameters(this);
        this.CircuitPowerParameters.SetReducingFactor(prototype.CircuitPowerParameters.ReducingFactor);
        // this.Power.PullProperties(prototype.Power);
    }

    public CircuitPowerParameters CircuitPowerParameters { get; set; }
    public Cabling Cabling { get; } = new();
    public CircuitProducts Products { get; }
    public bool IsGroupCircuit { get; set; }
    public IList<RoomProxy> Rooms { get; } = new List<RoomProxy>();

    public double CableLength
    {
        get => _cableLength;
        set => _cableLength = Math.Round(value);
    }

    public double CableLengthMax
    {
        get => _cableLengthMax;
        set => _cableLengthMax = Math.Round(value);
    }

    public double CableLengthInCableTray
    {
        get => _cableLengthInCableTray;
        set => _cableLengthInCableTray = Math.Round(value);
    }

    public double CableLengthOutsideCableTray { get; set; }
    public string CableMounting { get; set; }
    public bool LockCableLength { get; set; }

    public string RoomsAsString => string.Join(", ", this.Rooms.Select(n => n.Number).OrderBy(n => n, new NumbersStringComparer()));

    // TODO : # | Petrov | Исправить зависание при соединении в кольцо
        
    public override string ToString() =>
        $"[{Guid} / {RevitId}] <{GetType().Name}> {Name} : {ConsumerConnectors.Count} elements";

    public IEnumerable<ElectricalElementProxy> GetConsumers() => GetConsumersOf<ElectricalElementProxy>();

    private void AddRoom(ElectricalElementProxy electricalElement)
    {
        var room = electricalElement.Room;

        if (room == null)
            return;

        if (false == Rooms.Contains(room, new RevitProxyComparer<RoomProxy>()))
            Rooms.Add(room);
    }

    private void RemoveRoom(ElectricalElementProxy electricalElement)
    {
        var room = electricalElement.Room;

        if (room == null)
            return;

        if (Rooms.Count(n => n.Guid == room.Guid) == 1)
            Rooms.Remove(room);
    }

    public static ElectricalSystemProxy Create(ElectricalEquipmentProxy baseEquipment, params ElectricalBase[] items)
    {
        var virtualElementsRepository = DI.Get<Repository<VirtualElements>>();
        var virtualElements = virtualElementsRepository.First();
        var lastCircuitNumber = baseEquipment.Consumers.OfType<ElectricalSystemProxy>().LastOrDefault()?.CircuitNumber;
        var newCircuitNumber = NameUtils.GetNextName(lastCircuitNumber);
        var electricalSystem = baseEquipment.IsPower
            ? new ElectricalSystemProxy(virtualElements.NewId(), newCircuitNumber, baseEquipment.DistributionSystem)
            : new ElectricalSystemProxy(virtualElements.NewId(), newCircuitNumber, baseEquipment.SystemType);
        
        electricalSystem.ConnectTo(baseEquipment);
        
        foreach (var item in items)
            item.ConnectTo(electricalSystem);

        virtualElements.ElectricalSystems.Add(electricalSystem);
        virtualElementsRepository.ChangedElements.UpdateStorage();

        return electricalSystem;
    }
}