﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

/// <summary>
/// Low Voltage Panel. Part of <see cref="SwitchBoard"/>.<br/>
/// Any <see cref="SwitchBoardUnit"/> is a part of <see cref="SwitchBoard"/>.<br/>
/// Any <see cref="SwitchBoard"/> must have one or more <see cref="SwitchBoardUnit"/>.<br/>
/// Less then 1000 V.
/// </summary>
public sealed class SwitchBoardUnit : EquipmentUnit
{
    public SwitchBoard SwitchBoard => EquipmentSet as SwitchBoard;

    public SwitchBoardUnit()
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoardUnit(
        string name,
        ElectricalSystemTypeProxy systemType) :
        base(VirtualRevitId, name, systemType)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoardUnit(
        int revitId, string name,
        ElectricalSystemTypeProxy systemType) :
        base(revitId, name, systemType)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoardUnit(
        string name,
        DistributionSystemProxy distributionSystem) :
        base(VirtualRevitId, name, distributionSystem)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoardUnit(
        int revitId, string name,
        DistributionSystemProxy distributionSystem,
        ConnectorProxy connector = null) :
        base(revitId, name, distributionSystem, connector)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }
    
    public SwitchBoardUnit(int revitId, string name) : base(revitId, name)
    {
        Products = SwitchBoardUnitProducts = new SwitchBoardUnitProducts();
    }

    public SwitchBoardUnitProducts SwitchBoardUnitProducts { get; }
    
    public static SwitchBoardUnit CreateSingle(
        int revitId, string name, ElectricalSystemTypeProxy systemType)
    {
        var switchboardUnit = new SwitchBoardUnit(revitId, name, systemType);
        switchboardUnit.EquipmentSet = new SwitchBoard(revitId, name, new[] { switchboardUnit });
        return switchboardUnit;
    }
    
    public static SwitchBoardUnit CreateSingle(
        int revitId, string name, DistributionSystemProxy distributionSystem, ConnectorProxy connector = null)
    {
        var switchboardUnit = new SwitchBoardUnit(revitId, name, distributionSystem, connector);
        switchboardUnit.EquipmentSet = new SwitchBoard(revitId, name, new[] { switchboardUnit });
        return switchboardUnit;
    }
}