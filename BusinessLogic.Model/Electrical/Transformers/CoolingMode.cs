﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;

public enum CoolingMode
{
    AF,
    AF25,
    AF40,
    AN,
}