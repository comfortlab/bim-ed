﻿using CoLa.BimEd.BusinessLogic.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.Attributes;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;

public enum TransformerProperty
{
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_VibrationDampingStands))]
    VibrationDampingStands,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_InstallationSupervision))]
    InstallationSupervision,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_ReducedNoiseLevel))]
    ReducedNoiseLevel,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_ExtendedWarranty))]
    ExtendedWarranty,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_SupplyOfSpareParts))]
    SupplyOfSpareParts,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_MaintenanceContract))]
    MaintenanceContract,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_MarinePackaging))]
    MarinePackaging,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_NameplateInRussian))]
    NameplateInRussian,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_PassportGOST))]
    PassportGost,
        
    [ResourceDescription(
        typeof(ConsumerDescriptions),
        nameof(ConsumerDescriptions.Property_FactoryTestReports))]
    FactoryTestReports,
}