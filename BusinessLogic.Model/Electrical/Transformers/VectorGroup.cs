﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;

public enum VectorGroup
{
    Dyn01,
    Dyn05,
    Dyn11,
    Yyn0
}