﻿using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

public class CableInsulation
{
    public bool FireResistant { get; set; }
    public bool LowSmoke { get; set; }
    public bool LowToxic { get; set; }
    public bool ZeroHalogen { get; set; }
    public override string ToString() => this.AllValuesToString();
}