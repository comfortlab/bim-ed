﻿using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products.Characteristics;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

public class ElectricalProduct : Product
{
    public ElectricalProduct(BaseProduct baseProduct) : base(baseProduct)
    {

    }

    public string CurveCode { get; set; }
    public double BreakingCapacity { get; set; }
    public double EarthLeakageSensitivity { get; set; }
    public double RatedCurrent { get; set; }
    public MagneticTrippingLimit MagneticTrippingLimit { get; set; }
    public Poles Poles { get; set; } = new();
    public override string ToString()
    {
        var characteristics = new[]
        {
            Poles?.ToString(),
            CurveCode,
            RatedCurrent.ToStringInvariant(),
            EarthLeakageSensitivity.ToStringInvariant(),
            BreakingCapacity.ToStringInvariant(),
        };

        return $"{base.ToString()} {characteristics.Where(n => n != null).JoinToString(", ")}";
    }
}