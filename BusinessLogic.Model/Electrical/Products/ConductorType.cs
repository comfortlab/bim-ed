﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

public enum ConductorType
{
    Solid,
    Stranded,
    SectionStranded,
}