﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class MeasuringVoltageTransformer : Manufactured
{
    public MeasuringVoltageTransformer(Guid guid) : base(guid) { }
    public MeasuringAccuracyClass MeasuringAccuracyClass { get; set; }
}