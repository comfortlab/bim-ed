﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class MotorMechanism : Manufactured
{
    public MotorMechanism(Guid guid) : base(guid) { }
    public ElectricalQuantity Voltage { get; set; }
    public ElectricalQuantity Power { get; set; }
}