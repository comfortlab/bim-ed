﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class ShuntTripRelease : Manufactured
{
    public ShuntTripRelease(Guid guid) : base(guid) { }
    public ElectricalQuantity Voltage { get; set; }
    public ElectricalQuantity Power { get; set; }
    public double ResponseTime { get; set; }
}