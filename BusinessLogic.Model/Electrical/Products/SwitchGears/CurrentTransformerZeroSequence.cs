﻿using System;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class CurrentTransformerZeroSequence : CurrentTransformer
{
    public CurrentTransformerZeroSequence(Guid guid) : base(guid) { }
}