﻿using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class SwitchGearSwitch : Manufactured
{
    public double RatedCurrent { get; set; }
}