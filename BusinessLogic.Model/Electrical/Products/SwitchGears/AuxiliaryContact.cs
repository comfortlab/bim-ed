﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class AuxiliaryContact : Manufactured
{
    public AuxiliaryContact(Guid guid, string name = null) : base(guid, name) { }
}