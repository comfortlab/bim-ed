﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public class SwitchGearComponent : Manufactured
{
    public SwitchGearComponent(Guid guid, string name = null) : base(guid, name) { }
    public UniqueCollection<SwitchGearSeries> SwitchGearSeries { get; set; } = new();
}