﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;

public interface ICompatibleComponents<T> where T : IManufactured
{
    UniqueCollection<T> CompatibleComponents { get; }
    bool IsCompatible(T component);
    T GetDefault(Type type = null);
}