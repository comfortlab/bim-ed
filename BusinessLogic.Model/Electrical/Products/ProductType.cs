﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

public enum ProductType
{
    MainSwitch = 0,
    Switch = 1,
    Enclosure = 2
}