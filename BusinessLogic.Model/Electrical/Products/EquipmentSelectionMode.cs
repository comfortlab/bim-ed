﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

public enum EquipmentSelectionMode
{
    ByCable,
    ByLoad,
}