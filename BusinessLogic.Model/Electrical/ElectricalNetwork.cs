﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Resources;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class ElectricalNetwork : ElectricalSource
{
    public ElectricalNetwork() { }
        
    public ElectricalNetwork(
        int revitId,
        ElectricalSystemTypeProxy electricalSystemType) :
        base(revitId, GetName(electricalSystemType), electricalSystemType) { }
        
    public ElectricalNetwork(
        int revitId,
        string name,
        ElectricalSystemTypeProxy electricalSystemType) :
        base(revitId, name, electricalSystemType) { }
        
    public ElectricalNetwork(
        int revitId,
        DistributionSystemProxy distributionSystem) :
        base(revitId, distributionSystem.Name, distributionSystem) { }
        
    public ElectricalNetwork(
        int revitId,
        string name,
        DistributionSystemProxy distributionSystem) :
        base(revitId, name, distributionSystem) { }

    private static string GetName(ElectricalSystemTypeProxy systemTypeProxy) =>
        systemTypeProxy switch
        {
            ElectricalSystemTypeProxy.UndefinedSystemType => ElectricalSystemTypes.Undefined,
            ElectricalSystemTypeProxy.Data => ElectricalSystemTypes.Data,
            ElectricalSystemTypeProxy.PowerCircuit => ElectricalSystemTypes.PowerCircuit,
            ElectricalSystemTypeProxy.Telephone => ElectricalSystemTypes.Telephone,
            ElectricalSystemTypeProxy.Security => ElectricalSystemTypes.Security,
            ElectricalSystemTypeProxy.FireAlarm => ElectricalSystemTypes.FireAlarm,
            ElectricalSystemTypeProxy.NurseCall => ElectricalSystemTypes.NurseCall,
            ElectricalSystemTypeProxy.Controls => ElectricalSystemTypes.Controls,
            ElectricalSystemTypeProxy.Communication => ElectricalSystemTypes.Communication,
            ElectricalSystemTypeProxy.PowerBalanced => ElectricalSystemTypes.PowerBalanced,
            ElectricalSystemTypeProxy.PowerUnBalanced => ElectricalSystemTypes.PowerUnbalanced,
            _ => throw new ArgumentOutOfRangeException(nameof(systemTypeProxy), systemTypeProxy, null)
        };
}