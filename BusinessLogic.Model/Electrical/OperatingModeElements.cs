﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class OperatingModeElements
{
    public List<ElementProxy> Elements { get; } = new();
}