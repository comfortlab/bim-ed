﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum TracePathType
{
    ByCableTrayConduit,
    ByElement,
}