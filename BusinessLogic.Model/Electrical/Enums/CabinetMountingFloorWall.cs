﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum CabinetMountingFloorWall
{
    Floor,
    Wall,
}