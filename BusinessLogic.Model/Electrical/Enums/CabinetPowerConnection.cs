﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum CabinetPowerConnection
{
    Busway,
    Cable,
}