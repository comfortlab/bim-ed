﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum ProtectionIndex
{
    IP00,
    IP2X,
    IP21,
    IP3X,
    IP31,
    IP32,
    IP4X,
    IP41,
    IP42,
    IP44,
    IP54,
    IP65,
    IP67,
}