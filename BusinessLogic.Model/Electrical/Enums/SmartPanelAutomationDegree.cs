﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum SmartPanelAutomationDegree
{
    None,
    TechnicalAccounting,
    TechnicalAccountingStatus,
    TechnicalAccountingStatusControl,
}