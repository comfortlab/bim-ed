﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum DemandFactorRuleProxy
{
    Constant = 0,
    QuantityTable,
    LoadTable,
    QuantityTablePerPortion,
    LoadTablePerPortion,
}