﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum DemandFactorCalculationOption
{
    TotalAtOnePercentage,
    IncrementallyForEachRange,
}