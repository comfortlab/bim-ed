﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum DemandFactorOptionsComboBox
{
    Constant = 0,
    QuantityTable,
    LoadTable,
}