﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum PowerDevices
{
    CircuitBreaker = 1,
    AdditionalDevice1,
    AdditionalDevice2,
    EnergyMeter,
    AuxiliaryDevice1,
    AuxiliaryDevice2,
    AuxiliaryDevice3,
    CurrentTransformer
}