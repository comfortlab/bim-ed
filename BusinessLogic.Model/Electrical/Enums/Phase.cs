﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum Phase
{
    Undefined = -1,
    L123,
    L1,
    L2,
    L3,
    L12,
    L23,
    L13,
}