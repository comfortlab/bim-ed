﻿using System.ComponentModel;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum CableCoresMode
{
    [Description("N, PE")]
    NandPE,

    [Description("N / PE / PEN")]
    NorPE
}