﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum Topology
{
    Undefined = -1,
    Tree = 0,
    Star = 1,
    //Bus = 2,
    //Ring = 3
}