﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum ElectricalType
{
    ElectricalElement,
    ElectricalCircuit,
    ElectricalPanel,
    ElectricalSectionPanel,
    ElectricalTransformer,
}