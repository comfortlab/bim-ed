﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum PhasesNumber
{
    // Значения не изменять, используются в расчётах
    Undefined = 0,
    One = 1,
    Two = 2,
    Three = 3
}