﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum CabinetMountingFlushSurface
{
    Flush,
    Surface,
}