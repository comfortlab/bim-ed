﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

public enum PowerElectricalPanelType
{
    MainDistributionPanel,
    InputDistributionPanel,
    DistributionPanel,
    GroupPanel125,
    GroupPanel63,
}