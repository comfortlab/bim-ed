﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;

public enum GasExhaust
{
    Upwards,
    Downwards,
    Rear
}