﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;

public enum InternalArcClassification
{
    NIAC,
    IAC
}