﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;

public enum InsulationType
{
    Air,
    Gas,
    Solid,
    Vacuum,
}