﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public enum CablingEnvironment
{
    Air,
    Ground,
}