﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

public abstract class EquipmentUnit : ElectricalEquipmentProxy
{
    public EquipmentSet EquipmentSet { get; set; }
    public ConnectorProxy ReserveSourceConnector { get; set; }
    public LeftConnector<EquipmentUnit> LeftConnector { get; set; }
    public RightConnector<EquipmentUnit> RightConnector { get; set; }
    public ElectricalProducts Products { get; set; }

    protected EquipmentUnit() :
        this(default(int), default) { }

    protected EquipmentUnit(Guid guid, string name) :
        base(guid, default, name) { }

    protected EquipmentUnit(string name, ElectricalSystemTypeProxy systemType) :
        this(VirtualRevitId, name, systemType) { }

    protected EquipmentUnit(int revitId, string name, ElectricalSystemTypeProxy systemType) :
        this(revitId, name) => SetBaseConnector(new ConnectorProxy(this, 1, systemType));

    protected EquipmentUnit(string name, DistributionSystemProxy distributionSystem) :
        this(VirtualRevitId, name, distributionSystem) { }

    protected EquipmentUnit(int revitId, string name, DistributionSystemProxy distributionSystem, ConnectorProxy connector = null) : this(revitId, name)
    {
        DistributionSystem = distributionSystem;
        SetBaseConnector(connector ?? new ConnectorProxy(this, 1, DistributionSystem));
        BaseConnector.PowerParameters.PhasesNumber = DistributionSystem.PhasesNumber;
        BaseConnector.PowerParameters.LineToGroundVoltage = DistributionSystem.GetLineToGroundVoltage();
    }

    protected EquipmentUnit(string name) : base(name)
    {
        LeftConnector = new LeftConnector<EquipmentUnit>(this);
        RightConnector = new RightConnector<EquipmentUnit>(this);
    }

    protected EquipmentUnit(int revitId, string name) : base(revitId, name)
    {
        LeftConnector = new LeftConnector<EquipmentUnit>(this);
        RightConnector = new RightConnector<EquipmentUnit>(this);
    }

    public IEnumerable<EquipmentUnit> GetSideConnected(bool includeOwner = false, OperatingMode operatingMode = null)
    {
        return includeOwner
            ? new[] {this}.Concat(SideConnected(operatingMode))
            : SideConnected(operatingMode);
    }

    public IEnumerable<EquipmentUnit> GetLeftConnected(bool includeOwner = false, OperatingMode operatingMode = null)
    {
        return includeOwner
            ? new[] {this}.Concat(LeftConnected(operatingMode))
            : LeftConnected(operatingMode);
    }

    public IEnumerable<EquipmentUnit> GetRightConnected(bool includeOwner = false, OperatingMode operatingMode = null)
    {
        return includeOwner
            ? new[] {this}.Concat(RightConnected(operatingMode))
            : RightConnected(operatingMode);
    }

    private IEnumerable<EquipmentUnit> SideConnected(OperatingMode operatingMode)
    {
        return LeftConnected(operatingMode).Concat(RightConnected(operatingMode));
    }

    private IEnumerable<EquipmentUnit> LeftConnected(OperatingMode operatingMode)
    {
        var connected = GetNextLeftConnected(this, operatingMode);

        return connected != null
            ? new[] { connected }.Concat(connected.LeftConnected(operatingMode))
            : new List<EquipmentUnit>();
    }

    private IEnumerable<EquipmentUnit> RightConnected(OperatingMode operatingMode)
    {
        var connected = GetNextRightConnected(this, operatingMode);
            
        return connected != null
            ? new[] { connected }.Concat(connected.RightConnected(operatingMode))
            : new List<EquipmentUnit>();
    }

    public override ElectricalBase GetConnectedSource(OperatingMode operatingMode = null)
    {
        if (BaseConnector.GetState(operatingMode))
            return BaseConnector.Source;
            
        if (ReserveSourceConnector?.GetState(operatingMode) ?? false)
            return ReserveSourceConnector.Source;

        var leftConnected = GetNextLeftConnected(this, operatingMode);

        while (leftConnected != null)
        {
            if (TryGetSource(leftConnected, operatingMode, out var source))
                return source;

            leftConnected = GetNextLeftConnected(leftConnected, operatingMode);
        }

        var rightConnected = GetNextRightConnected(this, operatingMode);

        while (rightConnected != null)
        {
            if (TryGetSource(rightConnected, operatingMode, out var source))
                return source;

            rightConnected = GetNextRightConnected(rightConnected, operatingMode);
        }

        return null;
    }

    private static EquipmentUnit GetNextLeftConnected(EquipmentUnit equipmentUnit, OperatingMode operatingMode)
    {
        return equipmentUnit.LeftConnector.IsConnected &&
               equipmentUnit.LeftConnector.GetState(operatingMode)
            ? equipmentUnit.LeftConnector.ReferenceConnector?.Owner
            : null;
    }

    private static EquipmentUnit GetNextRightConnected(EquipmentUnit equipmentUnit, OperatingMode operatingMode)
    {
        return equipmentUnit.RightConnector.IsConnected &&
               equipmentUnit.RightConnector.GetState(operatingMode)
            ? equipmentUnit.RightConnector.ReferenceConnector?.Owner
            : null;
    }

    private static bool TryGetSource(EquipmentUnit equipmentUnit, OperatingMode operatingMode,
        out ElectricalBase source)
    {
        source = null;
            
        if (equipmentUnit.BaseConnector.IsConnected &&
            equipmentUnit.BaseConnector.GetState(operatingMode))
        {
            source = equipmentUnit.BaseConnector.Source;
            return true;
        }

        if (equipmentUnit.ReserveSourceConnector is { IsConnected: true } &&
            equipmentUnit.ReserveSourceConnector.GetState(operatingMode))
        {
            source = equipmentUnit.ReserveSourceConnector.Source;
            return true;
        }

        return false;
    }
}