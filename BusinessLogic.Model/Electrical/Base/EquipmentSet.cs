﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

public abstract class EquipmentSet : ElementProxy
{
    protected EquipmentSet(int revitId, string name) :
        base(revitId, name) { }
}

public abstract class EquipmentSet<TEquipmentUnit> : EquipmentSet
    where TEquipmentUnit : EquipmentUnit
{
    protected EquipmentSet(
        int revitId, string name,
        params TEquipmentUnit[] sections) :
        this(revitId, name) 
    {
        foreach (var section in sections)
            AddUnit(section);

        DistributionSystem = Units.FirstOrDefault()?.DistributionSystem;
    }

    protected EquipmentSet(
        int revitId, string name,
        DistributionSystemProxy distributionSystem) :
        this(revitId, name)
    {
        DistributionSystem = distributionSystem;
    }

    protected EquipmentSet(int revitId, string name) :
        base(revitId, name)
    {
        // SelectAndConfigSetting = SelectAndConfigSetting<ElectricalPanel>.Create();
    }
    
    public List<TEquipmentUnit> Units { get; } = new();
    public BuiltInCategoryProxy Category { get; set; } = BuiltInCategoryProxy.ElectricalEquipment;
    public ElectricalProducts Products { get; set; }
    public DistributionSystemProxy DistributionSystem { get; set; }
    public TEquipmentUnit FirstUnit => Units.First();
    public List<ElectricalProducts> SectionSwitches { get; } = new();
        
    public void AddUnit(TEquipmentUnit unit)
    {
        var previousUnit = Units.LastOrDefault();
        Units.Add(unit);
        unit.EquipmentSet = this as EquipmentSet<EquipmentUnit>;
        previousUnit?.RightConnector?.ConnectTo(unit.LeftConnector);
    }
        
    public void InsertUnit(int index, TEquipmentUnit unit)
    {
        var previousUnit = index > 0 ? Units[index - 1] : null;
        var nextUnit = index < Units.Count - 1 ? Units[index] : null;
            
        Units.Insert(index, unit);
        unit.EquipmentSet = this as EquipmentSet<EquipmentUnit>;
            
        previousUnit?.RightConnector?.ConnectTo(unit.LeftConnector);
        nextUnit?.LeftConnector?.ConnectTo(unit.RightConnector);
    }

    public bool IsSingle => Units.Count == 1;
}