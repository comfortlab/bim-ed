﻿namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public enum SwitchMountType
{
    Fixed,
    Withdrawable,
}