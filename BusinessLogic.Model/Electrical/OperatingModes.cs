﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.BusinessLogic.Model.Electrical;

public class OperatingModes : List<OperatingMode>, IPrototype<OperatingModes>, IPropertyPrototype<OperatingModes>
{
    public static OperatingModes CreateDefault()
    {
        var operatingModes = new OperatingModes { new(Dictionary.Normal) };
        operatingModes.Current = operatingModes[0];
        return operatingModes;
    }

    public OperatingModes() { }
    private OperatingModes(OperatingModes prototype) => this.PullProperties(prototype);
    public void PullProperties(OperatingModes prototype)
    {
        this.Clear();
        this.AddRange(CollectionExtension.Clone(prototype));
        this.Current = this.FirstOrDefault(x => Equals(x.Name, prototype.Current.Name));
    }
    public OperatingModes Clone() => new(this);
    public OperatingMode Current { get; set; }
    public bool CheckForNameExists(OperatingMode item) => this.Any(x => x.Name == item?.Name);
}