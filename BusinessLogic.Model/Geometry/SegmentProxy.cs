﻿namespace CoLa.BimEd.BusinessLogic.Model.Geometry;

public class SegmentProxy
{
    public XYZProxy Point1 { get; }
    public XYZProxy Point2 { get; }

    public SegmentProxy(XYZProxy point1, XYZProxy point2)
    {
        Point1 = point1;
        Point2 = point2;
    }

    public SegmentProxy(UVProxy uv1, UVProxy uv2)
    {
        Point1 = new XYZProxy(uv1);
        Point2 = new XYZProxy(uv2);
    }

    public double DistanceToPoint(XYZProxy point) =>
        point.DistanceToSegment(this.Point1, this.Point2);

    public override bool Equals(object obj)
    {
        if (obj is not SegmentProxy segment)
            return false;

        return this.Point1.Equals(segment.Point1) &&
               this.Point2.Equals(segment.Point2);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = Point1.GetHashCode() ^ Point2.GetHashCode();
            return hashCode;
        }
    }

    public override string ToString() => $"{Point1} <-> {Point2}";
}