﻿namespace CoLa.BimEd.BusinessLogic.Model.Geometry;

public class UVProxy
{
    public static UVProxy Zero => new();

    public UVProxy(double u, double v) { U = u; V = v; }
    public UVProxy() { U = 0; V = 0; }
    public double U { get; set; }
    public double V { get; set; }
    public override string ToString() => $"({U}, {V})";
}