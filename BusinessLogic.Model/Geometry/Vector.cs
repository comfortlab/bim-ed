﻿using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.BusinessLogic.Model.Geometry;

public class Vector
{
    public Vector(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public Vector(XYZProxy point1, XYZProxy point2)
    {
        X = point2.X - point1.X;
        Y = point2.Y - point1.Y;
        Z = point2.Z - point1.Z;
    }
    
    public double X { get; }
    public double Y { get; }
    public double Z { get; }
    
    public static Vector operator *(Vector vector1, Vector vector2)
    {
        return new Vector(
            x: vector1.Y * vector2.Z - vector1.Z * vector2.Y,
            y: vector1.Z * vector2.X - vector1.X * vector2.Z,
            z: vector1.X * vector2.Y - vector1.Y * vector2.X);
    }

    public override string ToString() => $"({X}, {Y}, {Z})";
    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() && Equals((Vector) obj);
    }

    private bool Equals(Vector other) =>
        X.IsEqualTo(other.X) &&
        Y.IsEqualTo(other.Y) &&
        Z.IsEqualTo(other.Z);


    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = X.GetHashCode();
            hashCode = (hashCode * 397) ^ Y.GetHashCode();
            hashCode = (hashCode * 397) ^ Z.GetHashCode();
            return hashCode;
        }
    }
}