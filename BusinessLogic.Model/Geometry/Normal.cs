namespace CoLa.BimEd.BusinessLogic.Model.Geometry;

public enum Normal
{
    X,
    Y,
    Z
}