﻿using System;

namespace CoLa.BimEd.BusinessLogic.Model.Geometry;

public class XYZProxy
{
    public XYZProxy() { }

    public XYZProxy(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public XYZProxy(UVProxy uv)
    {
        X = uv.U;
        Y = uv.V;
        Z = 0;
    }

    public double X { get; }
    public double Y { get; }
    public double Z { get; }

    public double DistanceTo(XYZProxy point) =>
        Math.Sqrt(this.RatingDistanceTo(point));

    public double RatingDistanceTo(XYZProxy point) =>
        Math.Pow(this.X - point.X, 2) +
        Math.Pow(this.Y - point.Y, 2) +
        Math.Pow(this.Z - point.Z, 2);

    public double DistanceToLine(XYZProxy point1, XYZProxy point2) =>
        this.DistanceTo(this.ProjectToLine(point1, point2));

    public double DistanceToSegment(XYZProxy point1, XYZProxy point2)
    {
        var projectPoint = this.ProjectToLine(point1, point2);
        var lengthLine = point1.RatingDistanceTo(point2);
        var segment1 = point1.RatingDistanceTo(projectPoint);
        var segment2 = point2.RatingDistanceTo(projectPoint);

        return segment1 + segment2 > lengthLine + 0.0001
            ? Math.Min(this.DistanceTo(point1), this.DistanceTo(point2))
            : this.DistanceTo(projectPoint);
    }

    public bool IsOnSegment(XYZProxy point1, XYZProxy point2, double tolerance)
    {
        var projectPoint = this.ProjectToLine(point1, point2);
        var lengthLine = point1.RatingDistanceTo(point2);
        var segment1 = point1.RatingDistanceTo(projectPoint);
        var segment2 = point2.RatingDistanceTo(projectPoint);

        return segment1 + segment2 <= lengthLine + tolerance;
    }

    public double RatingDistanceToSegment(XYZProxy point1, XYZProxy point2)
    {
        var projectPoint = this.ProjectToLine(point1, point2);
        var lengthLine = point1.RatingDistanceTo(point2);
        var segment1 = point1.RatingDistanceTo(projectPoint);
        var segment2 = point2.RatingDistanceTo(projectPoint);

        return segment1 + segment2 > lengthLine
            ? Math.Min(this.RatingDistanceTo(point1), this.RatingDistanceTo(point2))
            : this.RatingDistanceTo(projectPoint);
    }

    public double DistanceToOnPlane(XYZProxy point, Normal normalToPlane)
    {
        var thisProject = this.OnPlane(normalToPlane);
        var pointProject = point.OnPlane(normalToPlane);

        return thisProject.DistanceTo(pointProject);
    }

    public double DistanceToLineOnPlane(XYZProxy point1, XYZProxy point2, Normal normalToPlane)
    {
        var thisProject = this.OnPlane(normalToPlane);
        var point1Project = point1.OnPlane(normalToPlane);
        var point2Project = point2.OnPlane(normalToPlane);

        return thisProject.DistanceToLine(point1Project, point2Project);
    }

    public double DistanceToSegmentOnPlane(XYZProxy point1, XYZProxy point2, Normal normalToPlane)
    {
        var thisProject = this.OnPlane(normalToPlane);
        var point1Project = point1.OnPlane(normalToPlane);
        var point2Project = point2.OnPlane(normalToPlane);

        return thisProject.DistanceToSegment(point1Project, point2Project);
    }

    public XYZProxy OnPlane(Normal normalToPlane)
    {
        return normalToPlane switch
        {
            Normal.X => new XYZProxy(0, Y, Z),
            Normal.Y => new XYZProxy(X, 0, Z),
            Normal.Z => new XYZProxy(X, Y, 0),
            _ => throw new ArgumentOutOfRangeException(nameof(normalToPlane), normalToPlane, null)
        };
    }

    public XYZProxy ProjectToLine(XYZProxy point1, XYZProxy point2)
    {
        var r0 = this;
        var r1 = point2;
        var s1 = point1 - point2;
        return r1 - (r1 - r0) * s1 / (s1 ^ 2) * s1;
    }

    public XYZProxy ProjectToSegment(XYZProxy point1, XYZProxy point2)
    {
        var projectPoint = this.ProjectToLine(point1, point2);
        var lengthLine = point1.RatingDistanceTo(point2);
        var segment1 = point1.RatingDistanceTo(projectPoint);
        var segment2 = point2.RatingDistanceTo(projectPoint);

        return segment1 + segment2 < lengthLine
            ? projectPoint
            : (segment1 < segment2
                ? point1
                : point2);
    }

    public double SumAbsXYZ() => Math.Abs(this.X) + Math.Abs(this.Y) + Math.Abs(this.Z);

    public static XYZProxy operator +(XYZProxy point1, XYZProxy point2) =>
        new(point1.X + point2.X,
            point1.Y + point2.Y,
            point1.Z + point2.Z);

    public static XYZProxy operator -(XYZProxy point1, XYZProxy point2) =>
        new(point1.X - point2.X,
            point1.Y - point2.Y,
            point1.Z - point2.Z);

    public static XYZProxy operator *(XYZProxy point1, double multiplier) =>
        new(point1.X * multiplier,
            point1.Y * multiplier,
            point1.Z * multiplier);

    public static XYZProxy operator *(double multiplier, XYZProxy point1) =>
        point1 * multiplier;

    public static double operator *(XYZProxy point1, XYZProxy point2) =>
        point1.X * point2.X +
        point1.Y * point2.Y +
        point1.Z * point2.Z;

    public static double operator ^(XYZProxy point1, double power) =>
        Math.Pow(point1.X, power) +
        Math.Pow(point1.Y, power) +
        Math.Pow(point1.Z, power);

    public bool IsAlmostEquals(object obj, double tolerance)
    {
        if (obj is not XYZProxy point)
            return false;

        return Math.Abs(this.X - point.X) <= tolerance &&
               Math.Abs(this.Y - point.Y) <= tolerance &&
               Math.Abs(this.Z - point.Z) <= tolerance;
    }

    public override bool Equals(object obj)
    {
        if (obj is not XYZProxy point)
            return false;

        return this.X.Equals(point.X) &&
               this.Y.Equals(point.Y) &&
               this.Z.Equals(point.Z);
    }

    public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();
    public override string ToString() => $"({X}, {Y}, {Z})";
}