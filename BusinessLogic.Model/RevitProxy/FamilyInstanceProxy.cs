﻿using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public sealed class FamilyInstanceProxy : ElementProxy
{
    public FamilyInstanceProxy(SymbolProxy proxySymbol, int revitId, string name) : base(revitId, name)
    {
        ProxySymbol = proxySymbol;
        FamilyProxy = ProxySymbol.FamilyProxy;
    }

    public SymbolProxy ProxySymbol { get; }
    public FamilyProxy FamilyProxy { get; }
}