﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class ScheduleProxy : ElementProxy
{
    public ScheduleProxy(int revitId, string name) : base(revitId, name)
    {
        SheetInstanceIds = new List<int>();
    }

    public List<int> SheetInstanceIds { get; }

    public bool IsReportCreated => SheetInstanceIds?.Any() ?? false;

    public override string ToString()
    {
        return $"[{RevitId}] {Name}; IsReportCreated: {IsReportCreated}; IdsSheetInstances: {SheetInstanceIds.JoinToString()}";
    }
}