﻿using System.Collections.Generic;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class LoadClassificationDirectory
{
    public List<DemandFactorProxy> DemandFactors { get; } = new();
    public List<LoadClassificationProxy> LoadClassifications { get; } = new();
}