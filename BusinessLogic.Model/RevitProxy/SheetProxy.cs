﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class SheetProxy : ElementProxy
{
    public SheetProxy(int revitId, string name, string number, int ownerId) : base(revitId, name)
    {
        Number = number;
        OwnerId = ownerId;
        AnnotationInstances = new List<FamilyInstanceProxy>();
    }

    public int OwnerId { get; set; }
    public string Number { get; set; }
    public List<FamilyInstanceProxy> AnnotationInstances { get; }
}