﻿namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public enum ElectricalPhaseConfigurationProxy
{
    Undefined = 0,
    Wye = 1,
    Delta = 2,
}