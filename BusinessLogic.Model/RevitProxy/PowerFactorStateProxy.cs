﻿namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public enum PowerFactorStateProxy
{
    Leading = 0,
    Lagging = 1
}