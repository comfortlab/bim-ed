﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public sealed class FamilyProxy : ElementProxy
{
    public FamilyProxy(int revitId, string name) : base(revitId, name)
    {
        SymbolProxies = new List<SymbolProxy>();
    }

    public FamilyProxy(FamilyProxy familyProxy) : this(familyProxy.RevitId, familyProxy.Name)
    {
        CodeFunction = familyProxy.CodeFunction;
        FamilyVersion = familyProxy.FamilyVersion;
    }
        
    public string CodeFunction { get; set; }
    public string FamilyVersion { get; set; }
    public IFamilyBuilder FamilyBuilder { get; set; }
    public List<SymbolProxy> SymbolProxies { get; }
}