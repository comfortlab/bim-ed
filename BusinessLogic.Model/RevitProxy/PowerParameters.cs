﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class PowerParameters
{
    public PowerParameters(PhasesNumber phasesNumber, double lineToGroundVoltage)
    {
        LineToGroundVoltage = lineToGroundVoltage;
        PhasesNumber = phasesNumber;
            
        if (PhasesNumber == PhasesNumber.Three)
            Phase = Phase.L123;
    }

    public Phase Phase { get; set; } = Phase.Undefined;
    public PhasesNumber PhasesNumber { get; set; }
    public bool IsOnePhase => PhasesNumber == PhasesNumber.One;
    public bool IsTwoPhases => PhasesNumber == PhasesNumber.Two;
    public bool IsThreePhases => PhasesNumber == PhasesNumber.Three;
    public LoadClassificationProxy LoadClassification { get; set; }
    public double LineToGroundVoltage { get; set; }
    public double OwnApparentLoad { get; set; }
    public double OwnApparentLoad1 { get; set; }
    public double OwnApparentLoad2 { get; set; }
    public double OwnApparentLoad3 { get; set; }
    public double OwnPowerFactor { get; set; }
}