﻿namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public enum BuiltInCategoryProxy
{
    Invalid = -1,
    ElectricalEquipment = -2001040,
    CommunicationDevices = -2008081,
    DataDevices = -2008083,
    ElectricalFixtures = -2001060,
    FireAlarmDevices = -2008085,
    LightingDevices = -2008087,
    LightingFixtures = -2001120,
    MechanicalEquipment = -2001140,
    NurseCallDevices = -2008077,
    SecurityDevices = -2008079,
    TelephoneDevices = -2008075,
}