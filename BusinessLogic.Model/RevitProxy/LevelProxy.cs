﻿using System;
using System.Collections;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.Infrastructure.Framework.Math;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class LevelProxy : ElementProxy, ICollection<RoomProxy>
{
    private readonly List<RoomProxy> _rooms;
        
    public LevelProxy(int revitId, double elevationFeet, string name = null) : base(revitId, name)
    {
        _rooms = new List<RoomProxy>();

        Elevation = elevationFeet.Convert(Converting.MillimetersFromFeet);
        Elements = new List<ElementProxy>();
        ViewPlans = new List<ViewProxy>();
    }

    public Building Building { get; set; }
    public double Elevation { get; set; }
    public XYZProxy MinPoint { get; set; }
    public XYZProxy MaxPoint { get; set; }
    public List<ElementProxy> Elements { get; set; }
    public List<ViewProxy> ViewPlans { get; set; }

    public IEnumerator<RoomProxy> GetEnumerator() => _rooms.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void Add(RoomProxy room)
    {
        if (room == null) throw new NullReferenceException(nameof(room));

        if (room.Level != null && !room.Level.Equals(this))
            room.Level.Remove(room);

        room.Level = this;
            
        if (Contains(room))
            return;

        _rooms.Add(room);

        Building?.AddRoom(room, this);
    }

    public void Clear() => _rooms.Clear();
    public bool Contains(RoomProxy room) => _rooms.Contains(room);
    public void CopyTo(RoomProxy[] rooms, int arrayIndex) => _rooms.CopyTo(rooms, arrayIndex);
    public bool Remove(RoomProxy room)
    {
        if (room == null) throw new NullReferenceException(nameof(room));
            
        room.Level = null;

        return _rooms.Remove(room);
    }

    public int Count => _rooms.Count;
    public bool IsReadOnly => false;
        
    public override bool Equals(object obj) => Equals(obj as LevelProxy);

    protected bool Equals(LevelProxy levelProxy)
    {
        if (levelProxy == null) return false;

        var sameElevation = Math.Abs((Elevation - levelProxy.Elevation)) < 1;
            
        if (sameElevation) return true;
            
        if (this.RevitId == levelProxy.RevitId) throw new Exception($"Levels have same Id ({this.RevitId}), but elevations are not equal ({this.Elevation} ≠ {levelProxy.Elevation}).");
            
        return false;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (base.GetHashCode()* 397) ^ Elevation.GetHashCode();
        }
    }
}