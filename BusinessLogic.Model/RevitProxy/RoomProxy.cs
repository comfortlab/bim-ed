﻿using CoLa.BimEd.BusinessLogic.Model.Geometry;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class RoomProxy : SpatialElementProxy
{
    public RoomProxy(int revitId, string name = null, string number = null, UVProxy uv = null) :
        base(revitId, name, number, uv) { }
}