﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public sealed class LoadClassificationProxy :
    ElementProxy,
    IPrototype<LoadClassificationProxy>,
    IPropertyPrototype<LoadClassificationProxy>
{
    private SelectAndConfigSetting<ElectricalSystemProxy> _circuitSelectAndConfigSetting;
    private SelectAndConfigSetting<SwitchBoardUnit> _panelSelectAndConfigSetting;
    
    public LoadClassificationProxy() { }

    public LoadClassificationProxy(int revitId, string name, DemandFactorProxy demandFactor = null) :
        base(revitId, name) => DemandFactor = demandFactor;

    public LoadClassificationProxy(LoadClassificationProxy prototype) : base(prototype.Guid) => PullProperties(prototype);
    public LoadClassificationProxy Clone() => new(this);
    public void PullProperties(LoadClassificationProxy prototype)
    {
        this.Name = prototype.Name;
        this.RevitId = prototype.RevitId;
        this.DemandFactor = prototype.DemandFactor;
        this._circuitSelectAndConfigSetting = prototype._circuitSelectAndConfigSetting?.Clone();
        this._panelSelectAndConfigSetting = prototype._panelSelectAndConfigSetting?.Clone();
    }

    public DemandFactorProxy DemandFactor { get; set; }
    public SelectAndConfigSetting<ElectricalSystemProxy> CircuitSelectAndConfigSetting => _circuitSelectAndConfigSetting ??= SelectAndConfigSetting<ElectricalSystemProxy>.Create();
    public SelectAndConfigSetting<SwitchBoardUnit> PanelSelectAndConfigSetting => _panelSelectAndConfigSetting ??= SelectAndConfigSetting<SwitchBoardUnit>.Create();
    public string ServiceType { get; set; }

    public override string ToString() => $"[{RevitId}] {Name} - {nameof(DemandFactor)}: {DemandFactor}";
    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() && Equals((LoadClassificationProxy) obj);
    }
    private bool Equals(LoadClassificationProxy other) => other.RevitId == this.RevitId;
    public override int GetHashCode() => Guid.GetHashCode();
}