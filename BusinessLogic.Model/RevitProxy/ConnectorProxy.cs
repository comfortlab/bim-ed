﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class ConnectorProxy
{
    public ConnectorProxy(
        ElectricalBase owner, int id,
        ElectricalSystemTypeProxy systemType,
        XYZProxy origin = null,
        bool defaultState = true,
        bool isPrimary = true)
    {
        Owner = owner;
        Id = id;
        SystemType = systemType;
        Origin = origin ?? new XYZProxy();
        IsPrimary = isPrimary;
        ConnectorOperatingModes = new ConnectorOperatingModes(this, defaultState);
    }

    public ConnectorProxy(ElectricalBase owner, int id,
        DistributionSystemProxy distributionSystem,
        XYZProxy origin = null, bool defaultState = true, bool isPrimary = true) :
        this(owner, id, distributionSystem.PhasesNumber, distributionSystem.GetLineToGroundVoltage(),
            origin, defaultState, isPrimary) { }

    public ConnectorProxy(
        ElectricalBase owner, int id,
        PhasesNumber phasesNumber, double lineToGroundVoltage,
        XYZProxy origin = null,
        bool defaultState = true,
        bool isPrimary = true)
    {
        Owner = owner;
        Id = id;
        SystemType = ElectricalSystemTypeProxy.PowerCircuit;
        CreatePowerParameters(phasesNumber, lineToGroundVoltage);
        Origin = origin ?? new XYZProxy();
        IsPrimary = isPrimary;
        ConnectorOperatingModes = new ConnectorOperatingModes(this, defaultState);
    }

    public ConnectorOperatingModes ConnectorOperatingModes { get; set; }
    public ElectricalBase Source { get; set; }
    public ElectricalBase Owner { get; set; }
    public ElectricalSystemTypeProxy SystemType { get; set; }
    public int Id { get; }
    public bool IsPower => PowerParameters is not null;
    public bool IsConnected => Source is not null;
    public bool IsPrimary { get; set; }
    public XYZProxy Origin { get; set; }
    public PowerParameters PowerParameters { get; set; }

    public PowerParameters CreatePowerParameters(DistributionSystemProxy distributionSystem)
    {
        return CreatePowerParameters(distributionSystem.PhasesNumber, distributionSystem.GetLineToGroundVoltage());
    }
        
    public PowerParameters CreatePowerParameters(PhasesNumber phasesNumber, double lineToGroundVoltage)
    {
        return PowerParameters ??= new PowerParameters(phasesNumber, lineToGroundVoltage);
    }
        
    public EstimatedPowerParameters GetEstimatedPowerParameters(OperatingMode operatingMode = null)
    {
        return ConnectorOperatingModes.GetEstimatedPowerParameters(operatingMode);
    }
        
    public bool GetState(OperatingMode operatingMode = null)
    {
        return ConnectorOperatingModes.GetState(operatingMode);
    }
        
    public void SetState(bool newState)
    {
        var operatingMode = DI.Get<OperatingModes>().Current;
        ConnectorOperatingModes.SetState(newState, operatingMode);
    }
        
    public void SetState(bool newState, params OperatingMode[] operatingModes)
    {
        ConnectorOperatingModes.SetState(newState, operatingModes);
    }
        
    public void SetAllStates(bool newState)
    {
        ConnectorOperatingModes.SetAllStates(newState);
    }

    public bool IsCompatible(ConnectorProxy otherConnector)
    {
        return IsPower == otherConnector.IsPower || SystemType == otherConnector.SystemType;
        // throw new InvalidOperationException($"{Owner}: {nameof(ElectricalBase.BaseConnector)}.{nameof(IsPower)} = {IsPower} but\n{otherConnector.Owner}: {nameof(ElectricalBase.BaseConnector)}.{nameof(IsPower)} = {otherConnector.IsPower}");
        // throw new InvalidOperationException($"{Owner}: {nameof(ElectricalBase.BaseConnector)}.{nameof(SystemType)} = {SystemType} but\n{otherConnector.Owner}: {nameof(ElectricalBase.BaseConnector)}.{nameof(SystemType)} = {otherConnector.SystemType}");
    }
}

public class ConnectorOperatingModes : Dictionary<OperatingMode, ConnectorOperatingMode>
{
    private readonly ConnectorProxy _connector;
    private readonly bool _defaultState;
        
    public ConnectorOperatingModes(ConnectorProxy connector, bool defaultState)
    {
        _connector = connector;
        _defaultState = defaultState;
    }

    public EstimatedPowerParameters GetEstimatedPowerParameters(OperatingMode operatingMode)
    {
        operatingMode ??= DI.Get<OperatingModes>().Current;

        return TryGetValue(operatingMode, out var value)
            ? value.EstimatedPowerParameters
            : CreateConnectorOperatingMode(operatingMode).EstimatedPowerParameters;
    }

    public bool GetState(OperatingMode operatingMode = null)
    {
        operatingMode ??= DI.Get<OperatingModes>().Current;

        return TryGetValue(operatingMode, out var value)
            ? value.State
            : CreateConnectorOperatingMode(operatingMode).State;
    }
        
    public void SetState(bool newState, params OperatingMode[] operatingModes)
    {
        foreach (var operatingMode in operatingModes)
        {
            SetState(newState, operatingMode);
        }
    }

    private void SetState(bool newState, OperatingMode operatingMode)
    {
        if (TryGetValue(operatingMode, out var connectorOperatingMode))
        {
            connectorOperatingMode.State = newState;
        }
        else
        {
            CreateConnectorOperatingMode(operatingMode).State = newState;
        }
    }
        
    public void SetAllStates(bool newState)
    {
        foreach (var value in Values)
            value.State = newState;
    }

    private ConnectorOperatingMode CreateConnectorOperatingMode(OperatingMode operatingMode)
    {
        var newEstimatedPowerParameters = _connector.IsPower
            ? new EstimatedPowerParameters(_connector.PowerParameters.PhasesNumber, _connector.PowerParameters.LineToGroundVoltage)
            : null;
            
        var newConnectorOperatingMode = new ConnectorOperatingMode(operatingMode, newEstimatedPowerParameters)
        {
            State = _defaultState,
        };

        Add(operatingMode, newConnectorOperatingMode);
            
        return newConnectorOperatingMode;
    }
}

public class ConnectorOperatingMode
{
    public ConnectorOperatingMode(OperatingMode operatingMode, EstimatedPowerParameters estimatedPowerParameters = null)
    {
        OperatingMode = operatingMode;
        EstimatedPowerParameters = estimatedPowerParameters;
    }
    public OperatingMode OperatingMode { get; }
    public bool State { get; set; } = true;
    public EstimatedPowerParameters EstimatedPowerParameters { get; }
}

public class EstimatedPowerParameters
{
    public EstimatedPowerParameters(PhasesNumber phasesNumber, double lineToGroundVoltage)
    {
        LineToGroundVoltage = lineToGroundVoltage;
        ThreePhases = phasesNumber > PhasesNumber.One ? new ThreePhasesParameters() : null;
        ResistanceReactance = new ResistanceReactance();
    }

    public ThreePhasesParameters ThreePhases { get; }
    public double Current { get; set; }
    public double ShortCurrent1 { get; set; }
    public double ShortCurrent3 { get; set; }
    public double SurgeShortCurrent3 { get; set; }
    public double TrueLoad { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double ApparentLoad { get; set; }
    public double EstimateApparentLoad { get; set; }
    public double PowerFactor { get; set; }
    public ResistanceReactance ResistanceReactance { get; set; }
    public double LineToGroundVoltage { get; }
    public double VoltageDrop { get; set; }
    public double VoltageDropPercent => VoltageDrop / LineToGroundVoltage;
    public double DemandFactor { get; set; }
    public double AdditionalDemandFactor { get; set; } = 1;
    public double TotalDemandFactor { get; set; } = 1;
}