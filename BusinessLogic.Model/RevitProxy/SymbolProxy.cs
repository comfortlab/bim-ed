﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public sealed class SymbolProxy : ElementProxy
{
    public SymbolProxy(FamilyProxy familyProxy, int revitId, string name) : base(revitId, name)
    {
        FamilyProxy = familyProxy;
    }

    public SymbolProxy(FamilyProxy familyProxy, SymbolProxy proxySymbol) : base(proxySymbol.RevitId, proxySymbol.Name)
    {
        FamilyProxy = familyProxy;
        CodeFunction = proxySymbol.CodeFunction;
        FamilyVersion = proxySymbol.FamilyVersion;
    }

    public Guid BimEdGuid { get; set; }
    public string CodeFunction { get; set; }
    public string FamilyVersion { get; set; }
    public FamilyProxy FamilyProxy { get; }
}