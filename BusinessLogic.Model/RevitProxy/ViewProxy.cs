﻿using CoLa.BimEd.BusinessLogic.Model.Common;

namespace CoLa.BimEd.BusinessLogic.Model.RevitProxy;

public class ViewProxy : ElementProxy
{
    public ViewProxy(int revitId, string name) : base(revitId, name) { }
}