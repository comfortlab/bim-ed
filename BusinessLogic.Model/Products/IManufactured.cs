﻿using System;

namespace CoLa.BimEd.BusinessLogic.Model.Products;

public interface IManufactured
{
    Product Product { get; }
    bool IsProductOf(Guid productGuid);
    bool IsSameProduct(IManufactured manufactured);
}