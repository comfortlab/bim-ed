using Newtonsoft.Json;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public record SelectorProductSettingDto
{
    [JsonProperty("SI")]
    public string SelectorId { get; set; }
    
    [JsonProperty("PR")]
    public ProductRangeDto ProductRange { get; set; }
    
    [JsonProperty("MinC")]
    public double MinCurrent { get; set; }
    
    [JsonProperty("MaxC")]
    public double MaxCurrent { get; set; }
}