using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public record ProductRangeDto
{
    [JsonProperty("SV")]
    public Dictionary<string, string> SelectedValues { get; set; }
}