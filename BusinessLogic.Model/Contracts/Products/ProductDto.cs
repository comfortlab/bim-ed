namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public record ProductDto
{
    public string Reference { get; set; }
}