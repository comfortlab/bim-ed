using System.Collections.Generic;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public record SelectorProductDto
{
    public string Designation { get; set; }
    public ProductDto Product { get; set; }
    public List<ProductDto> Accessories { get; set; }
    public List<string> References { get; set; }
    public string SelectorId { get; set; }
    public string SerializedConfiguration { get; set; }
}