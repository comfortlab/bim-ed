﻿namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public class ConductorCountDto
{
    public ConductorCountDto() { }
    public ConductorCountDto(int count, double section)
    {
        Count = count;
        Section = section;
        Id = $"{Count}×{Section}";
    }

    public string Id { get; set; }
    public double Section { get; set; }
    public int Count { get; set; }
}