﻿using System.Collections.Generic;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public class CableSeriesDto
{
    public string Id { get; set; }
    public string ConductorMaterialId { get; set; }
    public bool Armor { get; set; }
    public bool Shield { get; set; }
    public bool FireResistant { get; set; }
    public bool LowSmoke { get; set; }
    public bool LowToxic { get; set; }
    public bool ZeroHalogen { get; set; }
    public string CableProperties { get; set; }
    public List<CableDto> Cables { get; set; } = new();
}