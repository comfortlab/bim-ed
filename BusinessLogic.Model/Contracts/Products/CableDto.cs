﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

public class CableDto
{
    public string Id { get; set; }
    public string Description { get; set; }
    public int PermissibleCurrent { get; set; }
    public int GroundPermissibleCurrent { get; set; }
    public double Diameter { get; set; }
    public double R1 { get; set; }
    public double X1 { get; set; }
    public double R0 { get; set; }
    public double X0 { get; set; }
    public double WeightPerMeter { get; set; }
    public ConductorType ConductorType { get; set; }
    public virtual List<ConductorCountDto> Conductors { get; set; }
}