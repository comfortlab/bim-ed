namespace CoLa.BimEd.BusinessLogic.Model.Contracts;

public record ElementProxyDto
{
    public int RevitId { get; set; }
}