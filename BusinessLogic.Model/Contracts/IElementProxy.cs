﻿namespace CoLa.BimEd.BusinessLogic.Model.Contracts;

public interface IElementProxy : IEntity
{
    int RevitId { get; set; }
}