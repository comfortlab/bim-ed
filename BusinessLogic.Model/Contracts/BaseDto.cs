﻿namespace CoLa.BimEd.BusinessLogic.Model.Contracts;

public record BaseDto
{
    public int RevitId { get; set; }
    public string Name { get; set; }
}