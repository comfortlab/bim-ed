﻿using System;

namespace CoLa.BimEd.BusinessLogic.Model.Contracts;

public interface IEntity
{
    Guid Guid { get; }
    string Name { get; set; }
}