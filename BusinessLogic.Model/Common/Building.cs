﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Math;

namespace CoLa.BimEd.BusinessLogic.Model.Common;

public class Building : ICollection<LevelProxy>
{
    private readonly List<LevelProxy> _levels;
        
    public Building(IEnumerable<LevelProxy> levels = null, IEnumerable<RoomProxy> rooms = null)
    {
        _levels = levels?.ToList() ?? new List<LevelProxy>();
        Rooms = rooms?.ToList() ?? new List<RoomProxy>();
        OutBuilding = new RoomProxy(-1, "Out");
    }

    public RoomProxy OutBuilding { get; }
    public List<RoomProxy> Rooms { get; }

    public LevelProxy GetLevel(int id) =>
        id > 0 ? _levels.FirstOrDefault(n => n.RevitId == id) : null;

    public LevelProxy GetLevel(XYZProxy point)
    {
        foreach (var level in _levels)
        {
            if (level.Elevation.MillimetersToInternal() <= point.Z)
                return level;
        }

        return _levels.LastOrDefault();
    }

    public RoomProxy GetRoom(int id) => Rooms.FirstOrDefault(n => n.RevitId == id);

    public IEnumerator<LevelProxy> GetEnumerator() => _levels.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void Add(LevelProxy level)
    {
        if (level == null) throw new NullReferenceException(nameof(level));

        level.Building = this;
        _levels.Add(level);

        foreach (var room in level.Where(room => !Rooms.Contains(room)))
            Rooms.Add(room);
    }

    public void AddRoom(RoomProxy room, LevelProxy level = null)
    {
        if (room == null) throw new NullReferenceException(nameof(room));
            
        if (level != null && !Contains(level))
            Add(level);

        if (level != null && !Equals(room.Level, level))
            level.Add(room);

        if (!Rooms.Contains(room))
            Rooms.Add(room);
    }

    public void Clear() => _levels.Clear();
    public bool Contains(LevelProxy level) => _levels.Contains(level);
    public void CopyTo(LevelProxy[] levels, int arrayIndex) => _levels.CopyTo(levels, arrayIndex);
        
    public bool Remove(LevelProxy level)
    {
        if (level == null) throw new NullReferenceException(nameof(level));

        foreach (var room in level.Where(room => Rooms.Contains(room)))
            Rooms.Remove(room);

        level.Building = null;

        return _levels.Remove(level);
    }
   
    public bool RemoveRoom(RoomProxy room)
    {
        if (room == null) throw new NullReferenceException(nameof(room));

        _levels.FirstOrDefault(n => n.Contains(room))?.Remove(room);
            
        return Rooms.Remove(room);
    }

    public int Count => _levels.Count;
    public bool IsReadOnly => false;
}