﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts;

namespace CoLa.BimEd.BusinessLogic.Model.Common;

public abstract class ElementProxy : IElementProxy
{
    public const int VirtualRevitId = -1;
    public Guid Guid { get; }
    public int RevitId { get; set; } = VirtualRevitId;
    public string Name { get; set; }
    public bool IsVirtual => RevitId < 0;

    protected ElementProxy(Guid guid, int revitId, string name) : this(guid, name) => RevitId = revitId;
    protected ElementProxy(Guid guid, string name) : this(guid) => Name = name;
    protected ElementProxy(int revitId, string name) : this(name) => RevitId = revitId;
    protected ElementProxy(string name) : this() => Name = name;
    protected ElementProxy() : this(Guid.NewGuid()) { }
    protected ElementProxy(Guid guid) => Guid = guid;

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() &&
               Equals((ElementProxy) obj);
    }
    protected bool Equals(ElementProxy other) => Guid.Equals(other.Guid);
    public override int GetHashCode() => Guid.GetHashCode();
    
    public static IEqualityComparer<ElementProxy> RevitIdComparer { get; } = new RevitIdEqualityComparer();
    private sealed class RevitIdEqualityComparer : IEqualityComparer<ElementProxy>
    {
        public bool Equals(ElementProxy x, ElementProxy y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.RevitId > 0 && y.RevitId > 0 ? x.RevitId == y.RevitId : x.Guid.Equals(y.Guid);
        }
        public int GetHashCode(ElementProxy obj) => obj.RevitId;
    }
    
    public override string ToString() => $"[{Guid} / {RevitId}] <{GetType().Name}> {Name}";
}