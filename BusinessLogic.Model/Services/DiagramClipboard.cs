﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.BusinessLogic.Model.Services;

public class DiagramClipboard
{
    public SelectorProduct Switch { get; set; }
    public SelectorProduct EnergyMeter { get; set; }
    public List<SelectorProduct> OtherProducts { get; set; } = new();
    public Cable Cable { get; set; }
    public double ReducingFactor { get; set; }

    public bool IsAnyCopied()
    {
        return
            Cable != null ||
            EnergyMeter != null ||
            OtherProducts.Any() ||
            ReducingFactor > 0 ||
            Switch != null;
    }


    public void Reset()
    {
        Switch = null;
        EnergyMeter = null;
        OtherProducts = new List<SelectorProduct>();
        Cable = null;
        ReducingFactor = default;
    }
}