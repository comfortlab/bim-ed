﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Math;

namespace CoLa.BimEd.BusinessLogic.Model.Factories;

public static class ElectricalFactory
{
    public static class MotorControlDevices
    {
        public static MotorControlDevice Create(
            string name)
        {
            return Create(ElementProxy.VirtualRevitId, name);
        }

        public static MotorControlDevice Create(
            int revitId, string name)
        {
            return new MotorControlDevice(revitId, name);
        }
    }

    public static class Transformers
    {
        public static Transformer Create(
            string name,
            DistributionSystemProxy primaryDistributionSystem = default,
            DistributionSystemProxy secondaryDistributionSystem = default)
        {
            return Create(ElementProxy.VirtualRevitId, name, primaryDistributionSystem, secondaryDistributionSystem);
        }

        public static Transformer Create(
            int revitId, string name,
            DistributionSystemProxy primaryDistributionSystem = default,
            DistributionSystemProxy secondaryDistributionSystem = default)
        {
            return new Transformer(revitId, name, primaryDistributionSystem, secondaryDistributionSystem);
        }
    }
        
    public static class Switchboards
    {
        public static SwitchBoard Create(
            string name,
            BuiltInCategoryProxy category = BuiltInCategoryProxy.ElectricalEquipment,
            ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.UndefinedSystemType)
        {
            return Create(ElementProxy.VirtualRevitId, name, category, systemType);
        }
            
        public static SwitchBoard Create(
            int revitId, string name,
            BuiltInCategoryProxy category = BuiltInCategoryProxy.ElectricalEquipment,
            ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.UndefinedSystemType)
        {
            var panel = new SwitchBoard(revitId, name, systemType)
            {
                Category = category,
            };

            if (systemType == ElectricalSystemTypeProxy.PowerCircuit)
            {
                panel.FirstUnit.BaseConnector.CreatePowerParameters(PhasesNumber.Undefined, 0);
            }

            return panel;
        }

        public static SwitchBoard Create(
            string name,
            DistributionSystemProxy distributionSystem,
            params SwitchBoard[] connectedPanels)
        {
            return Create(ElementProxy.VirtualRevitId, name, distributionSystem, connectedPanels);
        }
            
        public static SwitchBoard Create(
            int revitId, string name,
            DistributionSystemProxy distributionSystem,
            params SwitchBoard[] connectedPanels)
        {
            var panel = new SwitchBoard(revitId, name, distributionSystem)
            {
                Category = BuiltInCategoryProxy.ElectricalEquipment,
            };

            var circuitNumber = 1;
                
            foreach (var connectedPanel in connectedPanels)
            {
                var connectedPanelConnector = connectedPanel.FirstUnit.BaseConnector;
                var circuit = Circuits.Create($"{circuitNumber++}", ElectricalSystemTypeProxy.PowerCircuit, connectedPanelConnector);
                circuit.BaseConnector.ConnectTo(panel.FirstUnit);
            }
                
            return panel;
        }

        public static SwitchBoard Create(
            string name,
            params SwitchBoardUnit[] sections)
        {
            return Create(ElementProxy.VirtualRevitId, name, sections);
        }
            
        public static SwitchBoard Create(
            int revitId, string name,
            params SwitchBoardUnit[] sections)
        {
            var switchboard = new SwitchBoard(revitId, name, sections)
            {
                Category = BuiltInCategoryProxy.ElectricalEquipment,
            };
                
            return switchboard;
        }

        public static SwitchBoardUnit CreateUnit(
            string name,
            ElectricalSystemTypeProxy systemType)
        {
            return CreateUnit(ElementProxy.VirtualRevitId, name, systemType);
        }
            
        public static SwitchBoardUnit CreateUnit(
            int revitId, string name,
            ElectricalSystemTypeProxy systemType)
        {
            return new SwitchBoardUnit(revitId, name, systemType);
        }

        public static SwitchBoardUnit CreateUnit(
            string name,
            DistributionSystemProxy distributionSystem,
            ConnectorProxy connector)
        {
            return CreateUnit(ElementProxy.VirtualRevitId, name, distributionSystem, connector);
        }
            
        public static SwitchBoardUnit CreateUnit(
            int revitId, string name,
            DistributionSystemProxy distributionSystem,
            ConnectorProxy connector)
        {
            return new SwitchBoardUnit(revitId, name, distributionSystem, connector);
        }

        public static SwitchBoardUnit CreateUnit(
            string name,
            DistributionSystemProxy distributionSystem,
            params SwitchBoard[] connectedSwitchboards)
        {
            return CreateUnit(ElementProxy.VirtualRevitId, name, distributionSystem, connectedSwitchboards);
        }
            
        public static SwitchBoardUnit CreateUnit(
            int revitId, string name,
            DistributionSystemProxy distributionSystem,
            params SwitchBoard[] connectedSwitchboards)
        {
            var section = new SwitchBoardUnit(revitId, name, distributionSystem);
            var circuitNumber = 1;
                
            foreach (var connectedPanel in connectedSwitchboards)
            {
                var connectedPanelConnector = connectedPanel.FirstUnit.BaseConnector;
                var circuit = Circuits.Create($"{circuitNumber++}", ElectricalSystemTypeProxy.PowerCircuit, connectedPanelConnector);
                circuit.BaseConnector.ConnectTo(section);
            }
                
            return section;
        }
    
        public static SwitchBoardUnit CreateUnit(string name, SwitchBoard switchBoard, params SwitchBoard[] connectedSwitchboards)
        {
            return CreateUnit(ElementProxy.VirtualRevitId, name, switchBoard, connectedSwitchboards);
        }
            
        public static SwitchBoardUnit CreateUnit(int revitId, string name, SwitchBoard switchBoard, params SwitchBoard[] connectedSwitchboards)
        {
            var section = new SwitchBoardUnit(revitId, name, switchBoard.DistributionSystem);
                
            switchBoard.AddUnit(section);
                
            var circuitNumber = 1;
                
            foreach (var connectedPanel in connectedSwitchboards)
            {
                var connectedPanelConnector = connectedPanel.FirstUnit.BaseConnector;
                var circuit = Circuits.Create($"{circuitNumber++}", ElectricalSystemTypeProxy.PowerCircuit, connectedPanelConnector);
                circuit.BaseConnector.ConnectTo(section);
            }
                
            return section;
        }
    }
        
    public static class CircuitGroups
    {
        public static ElectricalCircuitGroup Create(
            string name,
            IReadOnlyCollection<ElectricalSystemProxy> circuits,
            PhasesNumber phasesNumber = PhasesNumber.Undefined)
        {
            return new ElectricalCircuitGroup(name, circuits, phasesNumber);
        }
    }
        
    public static class Circuits
    {
        public static ElectricalSystemProxy Create(
            string circuitNumber,
            params ElectricalBase[] consumers)
        {
            return Create(ElementProxy.VirtualRevitId, circuitNumber, consumers);
        }
            
        public static ElectricalSystemProxy Create(
            int revitId, string circuitNumber,
            params ElectricalBase[] consumers)
        {
            if (consumers.IsEmpty())
                Create(revitId, circuitNumber, ElectricalSystemTypeProxy.UndefinedSystemType);
                
            if (consumers.Select(n => n.SystemType).Distinct().Count() > 1)
                throw new ArgumentException($"All {nameof(consumers)} must have same {nameof(ElectricalBase.SystemType)}.");

            var connectors = consumers.Select(n => n.BaseConnector).ToArray();
            var firstConsumer = consumers.First();

            return firstConsumer is ElectricalEquipmentProxy electricalEquipment
                ? Create(revitId, circuitNumber, electricalEquipment.DistributionSystem, connectors)
                : Create(revitId, circuitNumber, firstConsumer.SystemType, connectors);
        }

        public static ElectricalSystemProxy Create(
            string circuitNumber,
            ElectricalSystemTypeProxy electricalSystemTypeProxy,
            params ConnectorProxy[] connectors)
        {
            return Create(ElementProxy.VirtualRevitId, circuitNumber, electricalSystemTypeProxy, connectors);
        }

        public static ElectricalSystemProxy Create(
            int revitId, string circuitNumber,
            ElectricalSystemTypeProxy systemType,
            params ConnectorProxy[] connectors)
        {
            var circuit = new ElectricalSystemProxy(revitId, circuitNumber, systemType)
            {
                CircuitType = CircuitTypeProxy.Circuit,
            };
                
            foreach (var connector in connectors)
            {
                connector?.ConnectTo(circuit);
            }

            return circuit;
        }

        public static ElectricalSystemProxy Create(
            string circuitNumber,
            DistributionSystemProxy distributionSystem,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create(ElementProxy.VirtualRevitId, circuitNumber, distributionSystem, consumerConnectors);
        }
            
        public static ElectricalSystemProxy Create(
            int revitId, string circuitNumber,
            DistributionSystemProxy distributionSystem,
            params ConnectorProxy[] consumerConnectors)
        {
            var phasesNumber = distributionSystem.PhasesNumber;
            var voltage =
                distributionSystem.LineToGroundVoltage?.ActualValue ??
                distributionSystem.LineToLineVoltage.ActualValue / MathConstants.Sqrt3;
                
            return Create(revitId, circuitNumber, phasesNumber, voltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create(
            string circuitNumber,
            PhasesNumber phasesNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create(ElementProxy.VirtualRevitId, circuitNumber, phasesNumber, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create(
            int revitId, string circuitNumber,
            PhasesNumber phasesNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            if (consumerConnectors.Any() && consumerConnectors.All(c => c.IsPower == false))
                throw new ArgumentException($"All {nameof(consumerConnectors)} must be power.");

            var circuit = Create(revitId, circuitNumber, ElectricalSystemTypeProxy.PowerCircuit);

            circuit.BaseConnector.CreatePowerParameters(phasesNumber, lineToGroundVoltage);

            foreach (var connector in consumerConnectors)
            {
                connector.PowerParameters.LineToGroundVoltage = lineToGroundVoltage;
                connector.ConnectTo(circuit);
            }

            circuit.CircuitPowerParameters = new CircuitPowerParameters(circuit);

            return circuit;
        }

        public static ElectricalSystemProxy Create1Phase(
            string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create1Phase(ElementProxy.VirtualRevitId, circuitNumber, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create1Phase(
            int revitId, string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create(revitId, circuitNumber, PhasesNumber.One, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create2Phases(
            string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create2Phases(ElementProxy.VirtualRevitId, circuitNumber, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create2Phases(
            int revitId, string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create(revitId, circuitNumber, PhasesNumber.Two, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create3Phases(
            string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create3Phases(ElementProxy.VirtualRevitId, circuitNumber, lineToGroundVoltage, consumerConnectors);
        }

        public static ElectricalSystemProxy Create3Phases(
            int revitId, string circuitNumber, double lineToGroundVoltage,
            params ConnectorProxy[] consumerConnectors)
        {
            return Create(revitId, circuitNumber, PhasesNumber.Three, lineToGroundVoltage, consumerConnectors);
        }
            
        public static ElectricalSystemProxy CreateSpareCircuit(int revitId, PhasesNumber phasesNumber, double lineToGroundVoltage)
        {
            const ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.PowerCircuit;
                
            var circuit = new ElectricalSystemProxy(revitId, null, systemType)
            {
                CircuitType = CircuitTypeProxy.Spare,
            };

            circuit.BaseConnector.CreatePowerParameters(phasesNumber, lineToGroundVoltage);
                
            return circuit;
        }
            
        public static ElectricalSystemProxy CreateSpaceCircuit(int revitId, PhasesNumber phasesNumber, double lineToGroundVoltage)
        {
            const ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.PowerCircuit;
                
            var circuit = new ElectricalSystemProxy(revitId, null, systemType)
            {
                CircuitType = CircuitTypeProxy.Space,
            };

            circuit.BaseConnector.CreatePowerParameters(phasesNumber, lineToGroundVoltage);

            return circuit;
        }
    }

    public static class Elements
    {
        public static ElectricalElementProxy Create(
            string name,
            BuiltInCategoryProxy category = BuiltInCategoryProxy.Invalid,
            ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.UndefinedSystemType,
            XYZProxy point = null)
        {
            return Create(ElementProxy.VirtualRevitId, name, category, systemType, point);
        }
            
        public static ElectricalElementProxy Create(
            int revitId, string name,
            BuiltInCategoryProxy category = BuiltInCategoryProxy.Invalid,
            ElectricalSystemTypeProxy systemType = ElectricalSystemTypeProxy.UndefinedSystemType,
            XYZProxy point = null)
        {
            var element = new ElectricalElementProxy(revitId, name)
            {
                Category = category,
            };

            var connector = new ConnectorProxy(element, 1, systemType, point);
            
            element.SetBaseConnector(connector);
            // element.Connectors.Add(connector);

            return element;
        }

        public static ElectricalElementProxy Create(
            string name,
            BuiltInCategoryProxy category,
            params ConnectorProxy[] connectors)
        {
            return Create(ElementProxy.VirtualRevitId, name, category, connectors);
        }

        public static ElectricalElementProxy Create(
            int revitId, string name,
            BuiltInCategoryProxy category,
            params ConnectorProxy[] connectors)
        {
            var element = new ElectricalElementProxy(revitId, name)
            {
                Category = category,
            };

            foreach (var connector in connectors)
            {
                element.SetBaseConnector(connector);
                // connector.Owner = element;
                // element.Connectors.Add(connector);
            }
                
            return element;
        }
            
        public static ElectricalElementProxy Create(
            string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            PhasesNumber phasesNumber, double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create(ElementProxy.VirtualRevitId, name, category, 
                loadClassification, phasesNumber, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create(
            int revitId, string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            PhasesNumber phasesNumber, double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            var element = new ElectricalElementProxy(revitId, name)
            {
                Category = category,
            };

            var phaseApparentLoad = phasesNumber != PhasesNumber.Undefined ? apparentLoad / (int)phasesNumber : 0;
                
            var connector = new ConnectorProxy(element, 1, phasesNumber, lineToGroundVoltage, point)
            {
                PowerParameters =
                {
                    LoadClassification = loadClassification,
                    OwnApparentLoad = apparentLoad,
                    OwnApparentLoad1 = phasesNumber == PhasesNumber.Three ? phaseApparentLoad : default,
                    OwnApparentLoad2 = phasesNumber == PhasesNumber.Three ? phaseApparentLoad : default,
                    OwnApparentLoad3 = phasesNumber == PhasesNumber.Three ? phaseApparentLoad : default,
                    OwnPowerFactor = powerFactor,
                },
                SystemType = ElectricalSystemTypeProxy.PowerCircuit,
            };

            element.SetBaseConnector(connector);
            // element.Connectors.Add(connector);

            return element;
        }

        public static ElectricalElementProxy Create1Phase(
            string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create1Phase(ElementProxy.VirtualRevitId, name, category, loadClassification, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create1Phase(
            int revitId, string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create(revitId, name, category, loadClassification, PhasesNumber.One, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create2Phases(
            string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create2Phases(ElementProxy.VirtualRevitId, name, category, loadClassification, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create2Phases(
            int revitId, string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create(revitId, name, category, loadClassification, PhasesNumber.Two, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create3Phases(
            string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create3Phases(ElementProxy.VirtualRevitId, name, category, loadClassification, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }

        public static ElectricalElementProxy Create3Phases(
            int revitId, string name,
            BuiltInCategoryProxy category,
            LoadClassificationProxy loadClassification,
            double lineToGroundVoltage,
            double apparentLoad = 0, double powerFactor = 1,
            XYZProxy point = null)
        {
            return Create(revitId, name, category, loadClassification, PhasesNumber.Three, lineToGroundVoltage, apparentLoad, powerFactor, point);
        }
    }
}