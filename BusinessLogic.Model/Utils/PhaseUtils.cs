﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.BusinessLogic.Model.Utils;

public class PhaseUtils
{
    public List<ElectricalSystemProxy> ChangedCircuits { get; } = new();
        
    public static List<Phase> GetPhases(ElectricalBase consumer, ElectricalBase source)
    {
        var consumerParameters = consumer.PowerParameters;
        var sourceParameters = source.PowerParameters;
            
        switch (sourceParameters.PhasesNumber)
        {
            case PhasesNumber.One:
                var phase = sourceParameters?.Phase ?? Phase.Undefined;
                return new List<Phase> { phase };
                
            case PhasesNumber.Two
                when consumerParameters.PhasesNumber == PhasesNumber.One &&
                     sourceParameters.Phase == Phase.L12:
                return new List<Phase> { Phase.L1, Phase.L2 };
                
            case PhasesNumber.Two
                when consumerParameters.PhasesNumber == PhasesNumber.One &&
                     sourceParameters.Phase == Phase.L13:
                return new List<Phase> { Phase.L1, Phase.L3 };
                
            case PhasesNumber.Two
                when consumerParameters.PhasesNumber == PhasesNumber.One &&
                     sourceParameters.Phase == Phase.L23:
                return new List<Phase> { Phase.L2, Phase.L3 };
                
            case PhasesNumber.Two
                when consumerParameters.PhasesNumber == PhasesNumber.Two:
                return new List<Phase> { sourceParameters.Phase };
                
            case PhasesNumber.Three
                when consumerParameters.PhasesNumber == PhasesNumber.One:
                return new List<Phase> { Phase.L1, Phase.L2, Phase.L3 };

            case PhasesNumber.Three
                when consumerParameters.PhasesNumber == PhasesNumber.Two:
                return new List<Phase> { Phase.L12, Phase.L13, Phase.L23 };
                
            case PhasesNumber.Three
                when consumerParameters.PhasesNumber == PhasesNumber.Three:
                return new List<Phase> { Phase.L123 };
                
            default:
                return new List<Phase>();
        }
    }
        
    public void SetPhases(SwitchBoardUnit switchBoardUnit)
    {
        if (switchBoardUnit.DistributionSystem?.PhasesNumber != PhasesNumber.Three)
            return;

        var currentPhase = 1;

        foreach (var electricalCircuit in switchBoardUnit.GetConsumersOf<ElectricalSystemProxy>())
        {
            var circuitPowerParameters = electricalCircuit.PowerParameters;

            if (circuitPowerParameters == null)
                return;

            var phase = circuitPowerParameters.PhasesNumber switch
            {
                PhasesNumber.Undefined => Phase.Undefined,
                PhasesNumber.One when circuitPowerParameters.Phase is < Phase.L1 or > Phase.L3 => Phase.Undefined,
                PhasesNumber.Two when circuitPowerParameters.Phase is < Phase.L12 or > Phase.L13 => Phase.Undefined,
                PhasesNumber.Three when circuitPowerParameters.Phase != Phase.L123 => Phase.Undefined,
                _ => circuitPowerParameters.Phase
            };

            if (phase != Phase.Undefined)
                continue;
                
            switch (circuitPowerParameters.PhasesNumber)
            {
                case PhasesNumber.One:
                    SetPhase(electricalCircuit, (Phase)currentPhase);
                    currentPhase = currentPhase % 3 + 1;
                    continue;

                case PhasesNumber.Two:
                    SetPhase(electricalCircuit, (Phase)(currentPhase + 3));
                    currentPhase = currentPhase % 3 + 2;
                    continue;

                case PhasesNumber.Three:
                    continue;

                default:
                    throw new ArgumentOutOfRangeException(nameof(circuitPowerParameters.PhasesNumber), circuitPowerParameters.PhasesNumber, new ArgumentOutOfRangeException().Message);
            }
        }
    }
        
    public void SetPhase(
        ElectricalBase consumer,
        ElectricalBase source,
        Phase phase = Phase.Undefined)
    {
        SetPhase(consumer.BaseConnector, source, phase);
    }
        
    public void SetPhase(
        ConnectorProxy consumerConnector,
        ElectricalBase source,
        Phase phase = Phase.Undefined)
    {
        var sourcePhasesNumber = source.PowerParameters.PhasesNumber;
        var sourcePhase = source.PowerParameters.Phase;
        var consumerPhasesNumber = consumerConnector.PowerParameters.PhasesNumber;
        var consumerPhase = consumerConnector.PowerParameters.Phase;
            
        if (sourcePhasesNumber == PhasesNumber.One ||
            sourcePhasesNumber == consumerPhasesNumber)
        {
            SetPhase(consumerConnector, sourcePhase);
        }
        else if (consumerPhase == Phase.Undefined)
        {
            phase = phase != Phase.Undefined ? phase : GetNextPhase(source, consumerPhasesNumber);
            SetPhase(consumerConnector, phase);
        }
    }
        
    private static Phase GetNextPhase(ElectricalBase source, PhasesNumber consumerPhasesNumber)
    {
        var consumerConnectors = source.ConsumerConnectors
            .Where(n => n.PowerParameters.Phase != Phase.Undefined)
            .ToList();
        var onePhaseCircuits = consumerConnectors.Count(n => n.PowerParameters.IsOnePhase);
        var twoPhasesCircuits = consumerConnectors.Count(n => n.PowerParameters.IsTwoPhases);
        var phasesNumber = source.PowerParameters.PhasesNumber;
        var phaseIndex = (onePhaseCircuits + 2 * twoPhasesCircuits) % (int) phasesNumber + 1; 
            
        return consumerPhasesNumber switch
        {
            PhasesNumber.One => (Phase)phaseIndex,
            PhasesNumber.Two => (Phase)(phaseIndex + 3),
            _ => Phase.L123
        }; 
    }
        
    public static Phase GetDefaultPhase(ElectricalBase electrical)
    {
        return electrical.PowerParameters.PhasesNumber switch
        {
            PhasesNumber.One => Phase.L1,
            PhasesNumber.Two => Phase.L12,
            _ => Phase.L123
        };
    }
        
    public void SetPhaseAndCalculate(ElectricalBase electrical, Phase value)
    {
        SetPhase(electrical, value);
    }

    public void SetPhase(ConnectorProxy connector, Phase value)
    {
        if (connector.PowerParameters.Phase == value)
            return;
            
        connector.PowerParameters.Phase = value;

        if (connector.Owner is ElectricalSystemProxy circuit &&
            connector.PowerParameters.IsThreePhases == false)
            UpdateChildCircuitPhase(circuit, value);
    }
        
    public void SetPhase(ElectricalBase electrical, Phase value)
    {
        if (electrical.PowerParameters.Phase == value)
            return;
            
        electrical.PowerParameters.Phase = value;

        if (electrical is not ElectricalSystemProxy circuit ||
            electrical.PowerParameters.IsThreePhases)
            return;
            
        ChangedCircuits.Add(circuit);
        UpdateChildCircuitPhase(circuit, value);
    }

    public void UpdateChildCircuitPhase(ElectricalSystemProxy electricalSystem, Phase phase)
    {
        switch (electricalSystem.PowerParameters?.PhasesNumber)
        {
            case PhasesNumber.One:
                SetSamePhaseChildren(electricalSystem, phase);
                break;

            case PhasesNumber.Two:
                SetOneAndTwoPhasesChildren(electricalSystem, phase);
                break;

            case PhasesNumber.Three:
                return;
        }
    }

    private void SetSamePhaseChildren(ElectricalBase electricalCircuit, Phase phase)
    {
        foreach (var childCircuit in electricalCircuit.GetConsumersOf<ElectricalSystemProxy>())
        {
            SetPhase(childCircuit, phase);
        }
    }

    private void SetOneAndTwoPhasesChildren(ElectricalSystemProxy electricalSystem, Phase phase)
    {
        SetOnePhaseChildrenInTwoPhasesCircuit(electricalSystem, phase);
        SetTwoPhasesChildrenInTwoPhasesCircuit(electricalSystem, phase);
    }

    private void SetOnePhaseChildrenInTwoPhasesCircuit(ElectricalSystemProxy electricalSystem, Phase phase)
    {
        var onePhaseCircuits = electricalSystem.GetConsumersOf<ElectricalSystemProxy>().Where(n => n.PowerParameters.IsOnePhase)
            .ToList();

        foreach (var onePhaseCircuit in onePhaseCircuits)
            SetPhaseInTwoPhasesCircuit(onePhaseCircuit, phase);
    }

    private void SetTwoPhasesChildrenInTwoPhasesCircuit(ElectricalSystemProxy electricalSystem, Phase phase)
    {
        var twoPhasesCircuits = electricalSystem.GetConsumersOf<ElectricalSystemProxy>().Where(n => n.PowerParameters.IsTwoPhases);

        foreach (var twoPhasesCircuit in twoPhasesCircuits)
        {
            SetPhase(twoPhasesCircuit, phase);
        }
    }

    private void SetPhaseInTwoPhasesCircuit(ElectricalSystemProxy onePhaseSystem, Phase phase)
    {
        switch (phase)
        {
            case Phase.L12
                when onePhaseSystem.PowerParameters.Phase == Phase.L3:
                SetPhase(onePhaseSystem, Phase.L1);
                return;

            case Phase.L23
                when onePhaseSystem.PowerParameters.Phase == Phase.L1:
                SetPhase(onePhaseSystem, Phase.L2);
                return;

            case Phase.L13
                when onePhaseSystem.PowerParameters.Phase == Phase.L2:
                SetPhase(onePhaseSystem, Phase.L3);
                return;
        }
    }
}