﻿using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.BusinessLogic.Model.Utils;

public class VirtualElementsUtils
{
    private readonly Repository<DistributionSystemProxy> _distributionSystems;
    private readonly Repository<VirtualElements> _virtualElementsRepository;

    public VirtualElementsUtils(
        Repository<DistributionSystemProxy> distributionSystems,
        Repository<VirtualElements> virtualElementsRepository)
    {
        _distributionSystems = distributionSystems;
        _virtualElementsRepository = virtualElementsRepository;
    }
    
    public ElectricalNetwork CreateCompatibleNetwork(ElectricalBase electrical)
    {
        var virtualElements = _virtualElementsRepository.First();
        var networkId = virtualElements.NewId();
        var network = electrical.IsPower
            ? new ElectricalNetwork(networkId, GetDistributionSystem(electrical))
            : new ElectricalNetwork(networkId, electrical.SystemType);
        virtualElements.Add(network);
        _virtualElementsRepository.ChangedElements.UpdateStorage();
        return network;
    }

    private DistributionSystemProxy GetDistributionSystem(ElectricalBase electrical)
    {
        return electrical is ElectricalEquipmentProxy electricalEquipment
            ? electricalEquipment.DistributionSystem
            : _distributionSystems.FirstOrDefault(x => x.IsCompatible(electrical));
    }
}