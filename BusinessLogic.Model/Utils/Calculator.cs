﻿using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.BusinessLogic.Model.Utils;

public static class Calculator
{
    public static double GetEfficiency(double mechanicalPower, double electricalPower) =>
        (mechanicalPower / electricalPower).Round(3, 3);

    public static double GetMechanicalPower(double electricalPower, double electricalEfficiency) =>
        (electricalPower * electricalEfficiency).Round(4, 4);

    public static double GetActivePower(double mechanicalPower, double electricalEfficiency)
    {
        if (mechanicalPower.IsEqualToZero() || electricalEfficiency.IsEqualToZero())
            return default;

        return (mechanicalPower / electricalEfficiency).Round(4, 4);
    }

    public static double GetApparentPower(double activePower, double powerFactor)
    {
        if (activePower.IsEqualToZero() || powerFactor.IsEqualToZero())
            return default;

        return (activePower / powerFactor).Round(4, 4);
    }

    public static double GetPowerFactor(double apparentPower, double activePower)
    {
        if (activePower.IsEqualToZero() || apparentPower.IsEqualToZero())
            return default;

        return (activePower / apparentPower).Round(3, 3);
    }
}