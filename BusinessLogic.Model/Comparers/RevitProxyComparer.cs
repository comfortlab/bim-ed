﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts;

namespace CoLa.BimEd.BusinessLogic.Model.Comparers;

public class RevitProxyComparer : IEqualityComparer<IElementProxy>
{
    public bool Equals(IElementProxy x, IElementProxy y)
    {

        if (ReferenceEquals(x, y)) return true;

        if (x == null || y == null)
            return false;

        return x.RevitId == y.RevitId;
    }

    public int GetHashCode(IElementProxy element) => element.RevitId;
}

public class RevitProxyComparer<T> : IEqualityComparer<T> where T : IElementProxy
{
    public bool Equals(T x, T y)
    {
        if (ReferenceEquals(x, y))
            return true;

        if (x == null || y == null)
            return false;

        return x.RevitId == y.RevitId;
    }

    public int GetHashCode(T element) => element.RevitId;
}