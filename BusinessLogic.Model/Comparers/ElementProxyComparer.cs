﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts;

namespace CoLa.BimEd.BusinessLogic.Model.Comparers;

public class ElementProxyComparer : IEqualityComparer<IElementProxy>
{
    public bool Equals(IElementProxy x, IElementProxy y)
    {

        if (ReferenceEquals(x, y)) return true;

        if (x == null || y == null)
            return false;

        return x.Guid == y.Guid;
    }

    public int GetHashCode(IElementProxy element) => element.RevitId;
}

public class ElementProxyComparer<T> : IEqualityComparer<T> where T : IElementProxy
{
    public bool Equals(T x, T y)
    {
        if (ReferenceEquals(x, y))
            return true;

        if (x == null || y == null)
            return false;

        return x.Guid == y.Guid;
    }

    public int GetHashCode(T element) => element.RevitId;
}