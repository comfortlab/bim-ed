﻿using System.Windows.Input;

namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public partial class ProgressBarWindow
    {
        public ProgressBarWindow(ProgressBarController progressBarController)
        {
            InitializeComponent();
            DataContext = new ProgressBarWindowViewModel(progressBarController);
            this.Closed += (o, e) => { progressBarController.IsCanceled = false; };
        }

        private void ProgressBarWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}