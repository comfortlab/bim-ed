﻿namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public delegate void ProgressBarPropertyChanged(object sender, ProgressBarPropertyChangedArgs args);
}