﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public class ProgressBarController
    {
        private readonly IServiceProvider _serviceProvider;
        private Dispatcher _progressBarDispatcher;
        private Window _progressBarWindow;
        private bool _progressBarIsShown;
        private string _title;

        public ProgressBarController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        private string ProgressBarIsNotVisibleExceptionMessage => $"{nameof(ProgressBarWindow)} is not visible.";
        public bool ShowProgressBar { get; set; } = true;

        private void Show()
        {
            var progressBarThread = new Thread(() =>
            {
                _progressBarDispatcher = Dispatcher.CurrentDispatcher;
                _progressBarWindow = _serviceProvider.Get<ProgressBarWindow>();
                _progressBarWindow.ShowDialog();

                Dispatcher.Run();
            });

            _progressBarIsShown = true;
            
            progressBarThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            progressBarThread.Start();
        }
        
        internal List<ProgressBarProcess> Processes { get; } = new();

        public ProgressBarProcess StartProcess(int maxValue, string startDescription = null, bool counter = true, int startValue = 0)
        {
            if (ShowProgressBar == false)
                return null;

            var process = new ProgressBarProcess(this, maxValue, startDescription, counter, startValue);

            CheckWindowIsShown();

            Processes.Add(process);
            OnPropertyChanged(ProgressBarChangedProperty.Process);

            _currentProcess = process;
            
            return process;
        }

        private ProgressBarProcess _currentProcess;
        public void Next(string description = null) => _currentProcess?.Next(description);
        public void Reset() => _currentProcess?.Reset();
        internal void Finish()
        {
            Processes.Remove(_currentProcess);
            OnPropertyChanged(ProgressBarChangedProperty.Process);

            if (!Processes.Any())
                Close();

            _currentProcess = Processes.LastOrDefault();
        }

        private void CheckWindowIsShown()
        {
            if (_progressBarIsShown == false)
                Show();

            const int timeout = 3000;
            const int interval = 20;
            var time = 0;
            
            while (_progressBarWindow == null)
            {
                Thread.Sleep(interval);
                time += interval;

                if (time > timeout && _progressBarWindow == null)
                    throw new InvalidOperationException(ProgressBarIsNotVisibleExceptionMessage);
            }

            OnPropertyChanged(ProgressBarChangedProperty.Title);
        }

        public void Cancel() => IsCanceled = true;

        public void Close()
        {
            if (_progressBarWindow == null)
                return;
            
            _progressBarDispatcher?.Invoke(() =>
            {
                _progressBarWindow?.Close();
            });

            _progressBarDispatcher?.InvokeShutdown();
            _progressBarDispatcher = null;
            _progressBarWindow = null;
            _progressBarIsShown = false;

            _currentProcess = null;
            Processes.Clear();
            Title = null;
        }

        public void ThrowCancelException()
        {
            IsCanceled = false;
            throw new OperationCanceledException();
        }

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged(ProgressBarChangedProperty.Title);
            }
        }

        public bool IsCanceled { get; set; }

        internal event ProgressBarPropertyChanged ProgressBarPropertyChanged;

        internal void OnPropertyChanged(ProgressBarChangedProperty changedProperty)
        {
            ProgressBarPropertyChanged?.Invoke(null, new ProgressBarPropertyChangedArgs(changedProperty));
        }
    }
}