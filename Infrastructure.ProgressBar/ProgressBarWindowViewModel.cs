﻿using System.Windows;
using CoLa.BimEd.Infrastructure.ProgressBar.Properties;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public class ProgressBarWindowViewModel : ViewModel
    {
        private readonly ProgressBarController _progressBarController;

        public ProgressBarWindowViewModel(ProgressBarController progressBarController)
        {
            _progressBarController = progressBarController;
            Content = new ProgressBarView(progressBarController);
            Topmost = true;
        }

        public FrameworkElement Content { get; set; }
        public bool Topmost { get; set; }
        public string TopmostToolTip { get; set; }
        public Command SetTopmostCommand => new(() => Topmost = !Topmost);
        public override Command CancelCommand => new(() => _progressBarController?.Cancel());

        private void OnTopmostChanged()
        {
            TopmostToolTip = Topmost ? Resources.TopmostOff : Resources.TopmostOn;
        }
    }
}