﻿namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public enum ProgressBarChangedProperty
    {
        Process,
        Title,
    }
}