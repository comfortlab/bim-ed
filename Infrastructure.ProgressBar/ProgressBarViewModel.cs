﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    internal class ProgressBarViewModel : ViewModel
    {
        private readonly ProgressBarController _progressBarController;

        internal ProgressBarViewModel(ProgressBarController progressBarController)
        {
            _progressBarController = progressBarController;
            _progressBarController.ProgressBarPropertyChanged += OnProgressBarPropertyChanged;
            
            Processes = _progressBarController.Processes.ToList();
            Title = _progressBarController.Title;
        }

        private void OnProgressBarPropertyChanged(object sender, ProgressBarPropertyChangedArgs args)
        {
            switch (args.NewValue)
            {
                case ProgressBarChangedProperty.Process:
                    Processes = _progressBarController.Processes.ToList();
                    break;

                case ProgressBarChangedProperty.Title:
                    Title = _progressBarController.Title;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(args.NewValue), args.NewValue, new ArgumentOutOfRangeException().Message);
            }
        }

        public string Title { get; set; }
        public List<ProgressBarProcess> Processes { get; set; }
        public override Command CancelCommand => new(() => _progressBarController?.Cancel());
    }
}