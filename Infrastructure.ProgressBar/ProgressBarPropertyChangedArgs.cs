﻿namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public class ProgressBarPropertyChangedArgs
    {
        public ProgressBarPropertyChangedArgs(ProgressBarChangedProperty newValue) => NewValue = newValue;
        public ProgressBarChangedProperty NewValue { get; }
    }
}