﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CoLa.BimEd.Infrastructure.ProgressBar.Properties;

namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public class ProgressBarProcess : INotifyPropertyChanged, IDisposable
    {
        private readonly ProgressBarController _progressBarController;
        private int _value;
        private bool _counter;
        private string _counterValue;
        private string _description;
        private double _progressValue;

        internal ProgressBarProcess(
            ProgressBarController progressBarController,
            int maxValue,
            string startDescription = null,
            bool counter = true,
            int startValue = 0)
        {
            _progressBarController = progressBarController;
            MaxValue = maxValue;
            Description = startDescription;
            Counter = counter;
            Value = startValue;
        }

        public bool Counter
        {
            get => _counter;
            set
            {
                _counter = value;
                OnPropertyChanged();
            }
        }

        public string CounterValue
        {
            get => _counterValue;
            set
            {
                _counterValue = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public double ProgressValue
        {
            get => _progressValue;
            set
            {
                _progressValue = value;
                OnPropertyChanged();
            }
        }

        public int MaxValue { get; }

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                if (MaxValue == 0) return;
                ProgressValue = (double)Value / MaxValue * 100;

                if (Counter)
                    CounterValue = $"{Value} / {MaxValue}{(string.IsNullOrEmpty(Description) ? string.Empty : " : ")}";
            }
        }

        public void Next(string description = null)
        {
            if (_progressBarController.IsCanceled)
                _progressBarController.ThrowCancelException();

            Value++;

            if (description != null)
                Description = description;
        }

        public void Reset()
        {
            Description = default;
            Value = default;
        }

        public void Dispose()
        {
            Counter = false;
            Description = Resources.Done;
            Value = MaxValue;
            _progressBarController.Finish();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}