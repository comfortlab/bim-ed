﻿namespace CoLa.BimEd.Infrastructure.ProgressBar
{
    public partial class ProgressBarView
    {
        public ProgressBarView(ProgressBarController progressBarController)
        {
            InitializeComponent();
            DataContext = new ProgressBarViewModel(progressBarController);
        }
    }
}