﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class DependencyObjectUtils
{
    public static T FindAncestor<T>(this FrameworkElement from) where T : class
    {
        var candidate = @from.Parent as FrameworkElement;
        
        while (true)
        {
            switch (candidate)
            {
                case null:
                    return null;

                case T ancestor:
                    return ancestor;

                default:
                    candidate = candidate.Parent as FrameworkElement;
                    continue;
            }
        }
    }

    public static T FindVisualAncestor<T>(this DependencyObject from) where T : class
    {
        var candidate = VisualTreeHelper.GetParent(from);
        
        while (true)
        {
            switch (candidate)
            {
                case null:
                    return null;

                case T ancestor:
                    return ancestor;

                default:
                    candidate = VisualTreeHelper.GetParent(candidate);
                    continue;
            }
        }
    }

    public static DependencyObject FindAncestor(this DependencyObject from, string name)
    {
        var candidate = VisualTreeHelper.GetParent(from);
        
        while (true)
        {
            switch (candidate)
            {
                case null:
                    return null;

                case FrameworkElement frameworkElement when frameworkElement.Name == name:
                    return frameworkElement;

                default:
                    candidate = VisualTreeHelper.GetParent(candidate);
                    continue;
            }
        }
    }

    public static IList<DependencyObject> GetChildren(this DependencyObject reference)
    {
        if (reference == null)
            return new List<DependencyObject>();

        var children = new List<DependencyObject>();
        var count = VisualTreeHelper.GetChildrenCount(reference);

        for (var i = 0; i < count; i++)
        {
            children.Add(VisualTreeHelper.GetChild(reference, i));
        }

        return children;
    }

    public static IList<DependencyObject> GetAllChildren(this DependencyObject reference)
    {
        var children = reference.GetChildren();
        var queue = new Queue<DependencyObject>();

        foreach (var child in children)
            queue.Enqueue(child);

        while (queue.Any())
        {
            var child = queue.Dequeue();
            var childrenOfChild = child.GetChildren();

            children.AddRange(childrenOfChild);

            foreach (var childOfChild in childrenOfChild)
                queue.Enqueue(childOfChild);
        }

        return children;
    }

    public static T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
    {
        for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        {
            var child = VisualTreeHelper.GetChild(obj, i);

            if (child is T dependencyObject)
                return dependencyObject;

            var childOfChild = FindVisualChild<T>(child);

            if (childOfChild != null)
                return childOfChild;
        }

        return null;
    }

    public static DependencyObject FindVisualChild(DependencyObject obj, string name)
    {
        if (obj == null)
            return null;

        for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        {
            var child = VisualTreeHelper.GetChild(obj, i);

            if (child is FrameworkElement frameworkElement &&
                frameworkElement.Name == name)
                return frameworkElement;

            var childOfChild = FindVisualChild(child, name);

            if (childOfChild != null)
                return childOfChild;
        }

        return null;
    }

    public static DependencyObject FindVisualChildByTag(this DependencyObject reference, object tag)
    {
        var children = reference.GetChildren();
        var queue = new Queue<DependencyObject>();

        foreach (var child in children)
            queue.Enqueue(child);

        while (queue.Any())
        {
            var child = queue.Dequeue();

            if (((FrameworkElement)child)?.Tag?.Equals(tag) ?? false)
                return child;

            var childrenOfChild = child.GetChildren();

            children.AddRange(childrenOfChild);

            foreach (var childOfChild in childrenOfChild)
                queue.Enqueue(childOfChild);
        }

        return null;
    }
}