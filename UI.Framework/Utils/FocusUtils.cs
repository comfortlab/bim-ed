﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class FocusUtils
{
    public static void AutoFocus(UIElement uiElement)
    {
        uiElement.IsVisibleChanged += (_, args) =>
        {
            if (args.NewValue is true)
                uiElement.Focus();

            if (uiElement is TextBox textBox && !string.IsNullOrWhiteSpace(textBox.Text))
                textBox.CaretIndex = textBox.Text.Length;
        };
    }
    
    public static void KeyMoveFocus(object sender, KeyEventArgs e)
    {
        if (sender is not UIElement uiElement)
            return;
        
        switch (e.Key)
        {
            case Key.Down:
                uiElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                return;
        
            case Key.Up:
                uiElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Up));
                return;
        }
    }
}