﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Threading;
using Cursor = System.Windows.Forms.Cursor;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class WindowUtils
{
    public delegate bool Win32Callback(IntPtr hwnd, IntPtr lParam);

    public static void SetPositionAndSizes(this Window window, double scaleFactor, WindowStartupLocation windowStartupLocation = WindowStartupLocation.CenterScreen)
    {
        SetPositionAndSizes(window, scaleFactor, scaleFactor, windowStartupLocation);
    }
    
    public static void SetPositionAndSizes(this Window window, double heightScaleFactor, double widthScaleFactor, WindowStartupLocation windowStartupLocation = WindowStartupLocation.CenterScreen)
    {
        using var graphics = Graphics.FromHwnd(IntPtr.Zero);
        var dpiX = graphics.DpiX;
        var scale = dpiX / 96;

        var mousePosition = Cursor.Position;
        var screen = Screen.FromPoint(mousePosition);

        window.WindowStartupLocation = windowStartupLocation;

        window.Height = screen.Bounds.Height * heightScaleFactor / scale;
        window.Width = screen.Bounds.Width * widthScaleFactor / scale;
    }

    public static void ShowWindowOverRevit(Func<Window> createWindow, string windowUid)
    {
        var process = Process.GetCurrentProcess();

        if (TryGetExistWindow(process, windowUid, out var existWindow))
        {
            existWindow.Dispatcher.BeginInvoke(() => ActivateWindow(existWindow));
        }
        else
        {
            var culture = CultureInfo.CurrentCulture;
            var uiCulture = CultureInfo.CurrentUICulture;
                
            var thread = new Thread(() =>
            {
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = uiCulture;
                CreateWindow(process, createWindow, windowUid);
                Dispatcher.Run();
            });
            
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }
    }

    private static void CreateWindow(Process process, Func<Window> createWindow, string windowUid)
    {
        var window = createWindow.Invoke();
        window.Uid = windowUid;

        var setWindowOverOwner = new WindowInteropHelper(window)
        {
            Owner = process.MainWindowHandle
        };

        ActivateWindow(window);
    }

    private static void ActivateWindow(Window window)
    {
        if (!window.IsVisible)
            window.Show();

        if (window.WindowState == WindowState.Minimized)
            window.WindowState = WindowState.Normal;

        window.Activate();
        window.Topmost = true;
        window.Topmost = false;
        window.Focus();
    }

    public static bool TryGetExistWindow(Process process, string windowUid, out Window window)
    {
        var rootWindowsOfProcess = GetRootWindowsOfProcess(process.Id)
            .Where(x => HwndSource.FromHwnd(x) != null)
            .Select(x => HwndSource.FromHwnd(x)?.RootVisual as Window);
            
        window = rootWindowsOfProcess
            .FirstOrDefault(x => x != null && x.Dispatcher.Invoke(() => x.Uid == windowUid));

        return window != null;
    }

    private static IEnumerable<IntPtr> GetRootWindowsOfProcess(int pid)
    {
        var rootWindows = GetRootWindows(IntPtr.Zero);
        var rootWindowsOfProcess = new List<IntPtr>();
            
        foreach (var hWnd in rootWindows)
        {
            GetWindowThreadProcessId(hWnd, out var lpdwProcessId);
                
            if (lpdwProcessId == pid)
                rootWindowsOfProcess.Add(hWnd);
        }
        return rootWindowsOfProcess;
    }

    private static IEnumerable<IntPtr> GetRootWindows(IntPtr parent)
    {
        var result = new List<IntPtr>();
        var listHandle = GCHandle.Alloc(result);
            
        try
        {
            var childProc = new Win32Callback(EnumWindow);
            EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
        }
        finally
        {
            if (listHandle.IsAllocated)
                listHandle.Free();
        }
            
        return result;
    }

    public static void CloseWindow(string windowUid)
    {
        var process = Process.GetCurrentProcess();

        if (TryGetExistWindow(process, windowUid, out var existWindow))
        {
            existWindow.Close();
        }
    }
        
    [DllImport("user32.dll")]
    private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    [DllImport("user32.Dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool EnumChildWindows(IntPtr parentHandle, Win32Callback callback, IntPtr lParam);

    private static bool EnumWindow(IntPtr handle, IntPtr pointer)
    {
        var gcHandle = GCHandle.FromIntPtr(pointer);
            
        if (gcHandle.Target is not List<IntPtr> list)
        {
            throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
        }
            
        list.Add(handle);
            
        return true;
    }
}