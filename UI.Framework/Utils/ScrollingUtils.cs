﻿using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class ScrollingUtils
{
    public static void MultiScrollMouseWheel(object sender, MouseWheelEventArgs mouseWheelEventArgs)
    {
        if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            HorizontalScrollMouseWheel(sender, mouseWheelEventArgs);
        else
            ScrollMouseWheel(sender, mouseWheelEventArgs);
    }
    
    public static void ScrollMouseWheel(object sender, MouseWheelEventArgs e)
    {
        switch (sender)
        {
            case ScrollViewer scrollViewer:
                DownUpScroll(scrollViewer, e);
                break;

            case Selector selector:
                var childScrollViewer = DependencyObjectUtils.FindVisualChild<ScrollViewer>(selector);
                ScrollMouseWheel(childScrollViewer, e);
                break;
        }
    }

    public static void HorizontalScrollMouseWheel(object sender, MouseWheelEventArgs e)
    {
        switch (sender)
        {
            case ScrollViewer scrollViewer:
                LeftRightScroll(scrollViewer, e);
                break;

            case Selector selector:
                var childScrollViewer = DependencyObjectUtils.FindVisualChild<ScrollViewer>(selector);
                HorizontalScrollMouseWheel(childScrollViewer, e);
                break;
        }
    }

    private static void DownUpScroll(ScrollViewer scrollViewer, MouseWheelEventArgs e)
    {
        if (double.IsNaN(scrollViewer.VerticalOffset))
            return;

        const int lines = 5;

        if (e.Delta > 0)
            for (var i = 0; i < lines; i++)
                scrollViewer.LineUp();
        else
            for (var i = 0; i < lines; i++)
                scrollViewer.LineDown();

        e.Handled = true;
    }

    private static void LeftRightScroll(ScrollViewer scrollViewer, MouseWheelEventArgs e)
    {
        if (double.IsNaN(scrollViewer.HorizontalOffset))
            return;

        const int lines = 5;

        if (e.Delta > 0)
            for (var i = 0; i < lines; i++)
                scrollViewer.LineLeft();
        else
            for (var i = 0; i < lines; i++)
                scrollViewer.LineRight();

        e.Handled = true;
    }
}