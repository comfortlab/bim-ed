﻿using System.Windows;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class DataObjectUtils
{
    public static bool TryGetData<T>(this IDataObject dataObject, out T data)
        where T : class
    {
        data = dataObject.GetData(typeof(T)) as T;
        return data is not null;
    }
}