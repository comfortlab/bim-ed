using System;
using System.Windows;
using System.Windows.Controls;

namespace CoLa.BimEd.UI.Framework.Utils;

public static class TemplateGenerator
{
    private sealed class TemplateGeneratorControl:
        ContentControl
    {
        internal static readonly DependencyProperty FactoryProperty = DependencyProperty.Register(
            "Factory", typeof(Func<object>), typeof(TemplateGeneratorControl), new PropertyMetadata(null, FactoryChanged));

        private static void FactoryChanged(DependencyObject instance, DependencyPropertyChangedEventArgs args)
        {
            var control = (TemplateGeneratorControl)instance;
            var factory = (Func<object>)args.NewValue;
            control.Content = factory();
        }
    }
    
    public static DataTemplate CreateDataTemplate(Func<object> factory)
    {
        if (factory == null)
            throw new ArgumentNullException(nameof(factory));

        var frameworkElementFactory = new FrameworkElementFactory(typeof(TemplateGeneratorControl));
        frameworkElementFactory.SetValue(TemplateGeneratorControl.FactoryProperty, factory);

        return new DataTemplate(typeof(DependencyObject))
        {
            VisualTree = frameworkElementFactory
        };
    }

    public static ControlTemplate CreateControlTemplate(Type controlType, Func<object> factory)
    {
        if (controlType == null)
            throw new ArgumentNullException(nameof(controlType));

        if (factory == null)
            throw new ArgumentNullException(nameof(factory));

        var frameworkElementFactory = new FrameworkElementFactory(typeof(TemplateGeneratorControl));
        frameworkElementFactory.SetValue(TemplateGeneratorControl.FactoryProperty, factory);

        return new ControlTemplate(controlType)
        {
            VisualTree = frameworkElementFactory
        };
    }
}