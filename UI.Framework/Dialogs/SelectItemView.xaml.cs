﻿using System.Collections;
using System.Windows;

namespace CoLa.BimEd.UI.Framework.Dialogs;

public partial class SelectItemView
{
    public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
        "Title", typeof(string), typeof(SelectItemView), new PropertyMetadata(default));

    public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register(
        "Items", typeof(IList), typeof(SelectItemView), new PropertyMetadata(default));

    public static readonly DependencyProperty ItemsWrappingProperty = DependencyProperty.Register(
        "ItemsWrapping", typeof(bool), typeof(SelectItemView), new PropertyMetadata(true));

    public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
        "SelectedItem", typeof(object), typeof(SelectItemView), new FrameworkPropertyMetadata(default, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public SelectItemView() => InitializeComponent();

    public string Title
    {
        get => (string)GetValue(TitleProperty);
        set => SetValue(TitleProperty, value);
    }

    public IList Items
    {
        get => (IList)GetValue(ItemsProperty);
        set => SetValue(ItemsProperty, value);
    }

    public bool ItemsWrapping
    {
        get => (bool)GetValue(ItemsWrappingProperty);
        set => SetValue(ItemsWrappingProperty, value);
    }

    public object SelectedItem
    {
        get => GetValue(SelectedItemProperty);
        set => SetValue(SelectedItemProperty, value);
    }
}