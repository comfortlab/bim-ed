﻿using System.Windows;
using CoLa.BimEd.UI.Framework.Utils;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.Framework.Dialogs;

public partial class RenameView
{
    public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
        "Title", typeof(string), typeof(RenameView), new PropertyMetadata(CommandNames.Rename));
    
    public static readonly DependencyProperty NewNameHeaderProperty = DependencyProperty.Register(
        "NewNameHeader", typeof(string), typeof(RenameView), new PropertyMetadata(CommonDictionary.NewName));

    public static readonly DependencyProperty NewNameProperty = DependencyProperty.Register(
        "NewName", typeof(string), typeof(RenameView), new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public static readonly DependencyProperty OldNameHeaderProperty = DependencyProperty.Register(
        "OldNameHeader", typeof(string), typeof(RenameView), new PropertyMetadata(CommonDictionary.OldName));

    public static readonly DependencyProperty OldNameProperty = DependencyProperty.Register(
        "OldName", typeof(string), typeof(RenameView), new PropertyMetadata(default(string)));
    
    public RenameView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(NewNameTextBox);
    }

    public string Title
    {
        get => (string)GetValue(TitleProperty);
        set => SetValue(TitleProperty, value);
    }

    public string NewNameHeader
    {
        get => (string)GetValue(NewNameHeaderProperty);
        set => SetValue(NewNameHeaderProperty, value);
    }

    public string NewName
    {
        get => (string)GetValue(NewNameProperty);
        set => SetValue(NewNameProperty, value);
    }

    public string OldNameHeader
    {
        get => (string)GetValue(OldNameHeaderProperty);
        set => SetValue(OldNameHeaderProperty, value);
    }

    public string OldName
    {
        get => (string)GetValue(OldNameProperty);
        set => SetValue(OldNameProperty, value);
    }
}