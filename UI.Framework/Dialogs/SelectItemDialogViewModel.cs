﻿using System.Collections;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.Framework.Dialogs;

public class SelectItemDialogViewModel : ViewModel
{
    public SelectItemDialogViewModel(IList items, string title = null)
    {
        Items = items;
        Title = title;
    }

    public IList Items { get; set; }
    public object SelectedItem { get; set; }
    public string Title { get; set; }
    public bool ItemsWrapping { get; set; }
    public bool IsOkEnabled { get; set; }
    protected override void OkAction()
    {
        try
        {
            OkCallback?.Invoke(SelectedItem);
        }
        finally
        {
            Close?.Invoke();
        }
    }

    private void OnSelectedItemChanged() => IsOkEnabled = SelectedItem != null;
}