﻿using System.Windows;
using CoLa.BimEd.UI.Framework.Utils;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.Framework.Dialogs;

public partial class CreateView
{
    public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
        "Title", typeof(string), typeof(CreateView), new PropertyMetadata(CommandNames.Create));
    
    public static readonly DependencyProperty NewNameHeaderProperty = DependencyProperty.Register(
        "NewNameHeader", typeof(string), typeof(CreateView), new PropertyMetadata(CommonDictionary.NewName));

    public static readonly DependencyProperty NewNameProperty = DependencyProperty.Register(
        "NewName", typeof(string), typeof(CreateView), new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public CreateView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(NewNameTextBox);
    }

    public string Title
    {
        get => (string)GetValue(TitleProperty);
        set => SetValue(TitleProperty, value);
    }

    public string NewNameHeader
    {
        get => (string)GetValue(NewNameHeaderProperty);
        set => SetValue(NewNameHeaderProperty, value);
    }

    public string NewName
    {
        get => (string)GetValue(NewNameProperty);
        set => SetValue(NewNameProperty, value);
    }
}