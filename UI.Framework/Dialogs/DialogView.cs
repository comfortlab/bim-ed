﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CoLa.BimEd.UI.Framework.Dialogs;

public class DialogView : ContentControl
{
    public static readonly DependencyProperty ContentHorizontalAlignmentProperty = DependencyProperty.Register(
        "ContentHorizontalAlignment", typeof(HorizontalAlignment), typeof(DialogView), new PropertyMetadata(defaultValue: HorizontalAlignment.Stretch));

    public static readonly DependencyProperty ContentVerticalAlignmentProperty = DependencyProperty.Register(
        "ContentVerticalAlignment", typeof(VerticalAlignment), typeof(DialogView), new PropertyMetadata(defaultValue: VerticalAlignment.Stretch));

    public static readonly DependencyProperty ContentMarginProperty = DependencyProperty.Register(
        "ContentMargin", typeof(Thickness), typeof(DialogView), new PropertyMetadata(defaultValue: new Thickness(10)));

    public static readonly DependencyProperty BackgroundColorProperty = DependencyProperty.Register(
        "BackgroundColor", typeof(Color), typeof(DialogView), new PropertyMetadata(defaultValue: Colors.White));

    public static readonly DependencyProperty OkCommandProperty = DependencyProperty.Register(
        "OkCommand", typeof(Command), typeof(DialogView), new PropertyMetadata(default(Command)));

    public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register(
        "CancelCommand", typeof(Command), typeof(DialogView), new PropertyMetadata(default(Command)));

    public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register(
        "DeleteCommand", typeof(Command), typeof(DialogView), new PropertyMetadata(default(Command)));

    public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
        "Title", typeof(string), typeof(DialogView), new PropertyMetadata());

    public static readonly DependencyProperty OkVisibleProperty = DependencyProperty.Register(
        "OkVisible", typeof(bool), typeof(DialogView), new PropertyMetadata(defaultValue: true));

    public static readonly DependencyProperty CancelVisibleProperty = DependencyProperty.Register(
        "CancelVisible", typeof(bool), typeof(DialogView), new PropertyMetadata(defaultValue: true));

    public static readonly DependencyProperty DeleteVisibleProperty = DependencyProperty.Register(
        "DeleteVisible", typeof(bool), typeof(DialogView), new PropertyMetadata(defaultValue: false));

    public static readonly DependencyProperty IsOkEnabledProperty = DependencyProperty.Register(
        "IsOkEnabled", typeof(bool), typeof(DialogView), new PropertyMetadata(defaultValue: true));

    public static readonly DependencyProperty IsDefaultButtonsProperty = DependencyProperty.Register(
        "IsDefaultButtons", typeof(bool), typeof(DialogView), new PropertyMetadata(defaultValue: false));

    public Command EnterCommand => new(() =>
    {
        if (IsDefaultButtons && IsOkEnabled) OkCommand?.Execute(null);
    });
    
    public Command OkCommand
    {
        get => (Command)GetValue(OkCommandProperty);
        set => SetValue(OkCommandProperty, value);
    }
    
    public Command EscCommand => new(() =>
    {
        if (IsDefaultButtons) CancelCommand?.Execute(null);
    });

    public Command DeleteLocalCommand => new(() =>
    {
        if (DeleteCommand != null) DeleteCommand.Execute(null);
        else OkCommand.Execute(null);
    });
    
    public Command CancelCommand
    {
        get => (Command)GetValue(CancelCommandProperty);
        set => SetValue(CancelCommandProperty, value);
    }
    
    public Command DeleteCommand
    {
        get => (Command)GetValue(DeleteCommandProperty);
        set => SetValue(DeleteCommandProperty, value);
    }

    public HorizontalAlignment ContentHorizontalAlignment
    {
        get => (HorizontalAlignment)GetValue(ContentHorizontalAlignmentProperty);
        set => SetValue(ContentHorizontalAlignmentProperty, value);
    }

    public VerticalAlignment ContentVerticalAlignment
    {
        get => (VerticalAlignment)GetValue(ContentVerticalAlignmentProperty);
        set => SetValue(ContentVerticalAlignmentProperty, value);
    }

    public Color BackgroundColor
    {
        get => (Color)GetValue(BackgroundColorProperty);
        set => SetValue(BackgroundColorProperty, value);
    }

    public string Title
    {
        get => (string)GetValue(OkVisibleProperty);
        set => SetValue(OkVisibleProperty, value);
    }

    public bool OkVisible
    {
        get => (bool)GetValue(OkVisibleProperty);
        set => SetValue(OkVisibleProperty, value);
    }

    public bool DeleteVisible
    {
        get => (bool)GetValue(DeleteVisibleProperty);
        set => SetValue(DeleteVisibleProperty, value);
    }

    public bool CancelVisible
    {
        get => (bool)GetValue(CancelVisibleProperty);
        set => SetValue(CancelVisibleProperty, value);
    }

    public bool IsOkEnabled
    {
        get => (bool)GetValue(IsOkEnabledProperty);
        set => SetValue(IsOkEnabledProperty, value);
    }

    public bool IsDefaultButtons
    {
        get => (bool)GetValue(IsDefaultButtonsProperty);
        set => SetValue(IsDefaultButtonsProperty, value);
    }

    public Thickness ContentMargin
    {
        get => (Thickness)GetValue(ContentMarginProperty);
        set => SetValue(ContentMarginProperty, value);
    }
}