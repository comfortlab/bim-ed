﻿using System.Windows;

namespace CoLa.BimEd.UI.Framework;
// https://stackoverflow.com/a/12498528/7671820

public class Deferred
{
    public static readonly DependencyProperty ContentProperty = DependencyProperty.RegisterAttached(
        "Content", typeof(object), typeof(Deferred), new PropertyMetadata());

    public static object GetContent(DependencyObject obj) =>
        obj.GetValue(ContentProperty);

    public static void SetContent(DependencyObject obj, object value) =>
        obj.SetValue(ContentProperty, value);
}