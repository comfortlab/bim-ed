﻿using System.Windows;
using System.Windows.Media;

namespace CoLa.BimEd.UI.Framework.Controls.ProgressRings;

public partial class ProgressRing
{
    public static readonly DependencyProperty BrushProperty = DependencyProperty.Register(
        "Brush", typeof(Brush), typeof(ProgressRingDot), new PropertyMetadata(default(Brush)));

    public ProgressRing()
    {
        InitializeComponent();
    }

    public Brush Brush
    {
        get => (Brush)GetValue(BrushProperty);
        set => SetValue(BrushProperty, value);
    }
}