﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace CoLa.BimEd.UI.Framework.Controls.ProgressRings;

public partial class ProgressRingDot : INotifyPropertyChanged
{
    public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
        "Fill", typeof(Brush), typeof(ProgressRingDot), new PropertyMetadata(default(Brush)));

    public static readonly DependencyProperty StartAngleProperty = DependencyProperty.Register(
        "StartAngle", typeof(double), typeof(ProgressRingDot), new PropertyMetadata(0d, StartAnglePropertyChanged));
        
    public static readonly DependencyProperty PeriodProperty = DependencyProperty.Register(
        "Period", typeof(double), typeof(ProgressRingDot), new PropertyMetadata(1d, TimePropertyChanged));
        
    public static readonly DependencyProperty DelayProperty = DependencyProperty.Register(
        "Delay", typeof(double), typeof(ProgressRingDot), new PropertyMetadata(0d, TimePropertyChanged));
        
    public static readonly DependencyProperty TotalTimeProperty = DependencyProperty.Register(
        "TotalTime", typeof(double), typeof(ProgressRingDot), new PropertyMetadata(2d, TimePropertyChanged));
        
    private KeyTime _firstRevTime;
    private KeyTime _secondRevTime;
    private KeyTime _localTotalTime;

    public ProgressRingDot()
    {
        InitializeComponent();
        SetAngles();
        SetTimes();
    }

    public Brush Fill
    {
        get => (Brush) GetValue(FillProperty);
        set => SetValue(FillProperty, value);
    }

    public double StartAngle
    {
        get => (double) GetValue(StartAngleProperty);
        set => SetValue(StartAngleProperty, value);
    }
        
    public double Period
    {
        get => (double) GetValue(PeriodProperty);
        set => SetValue(PeriodProperty, value);
    }

    public double Delay
    {
        get => (double) GetValue(DelayProperty);
        set => SetValue(DelayProperty, value);
    }

    public double TotalTime
    {
        get => (double) GetValue(TotalTimeProperty);
        set => SetValue(TotalTimeProperty, value);
    }

    private static void StartAnglePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (!(d is ProgressRingDot progressRingDot))
            return;

        progressRingDot.SetAngles();
    }

    private void SetAngles()
    {
        ZeroAngle = 225 + StartAngle;
        FirstRevAngle = ZeroAngle + 360;
        SecondRevAngle = FirstRevAngle + 360;
    }

    private static void TimePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (!(d is ProgressRingDot progressRingDot))
            return;

        progressRingDot.SetTimes();
    }

    private void SetTimes()
    {
        FirstRevTime = KeyTime.FromTimeSpan(new TimeSpan((int)((Period + Delay) * 1e7)));
        SecondRevTime = KeyTime.FromTimeSpan(new TimeSpan((int)((2 * Period + Delay) * 1e7)));
        LocalTotalTime = KeyTime.FromTimeSpan(new TimeSpan((int)(TotalTime * 1e7)));
    }

    public KeyTime FirstRevTime
    {
        get => _firstRevTime;
        set
        {
            if (value.Equals(_firstRevTime)) return;
            _firstRevTime = value;
            OnPropertyChanged();
        }
    }

    public KeyTime SecondRevTime
    {
        get => _secondRevTime;
        set
        {
            if (value.Equals(_secondRevTime)) return;
            _secondRevTime = value;
            OnPropertyChanged();
        }
    }

    public KeyTime LocalTotalTime
    {
        get => _localTotalTime;
        set
        {
            if (value.Equals(_localTotalTime)) return;
            _localTotalTime = value;
            OnPropertyChanged();
        }
    }

    public double ZeroAngle { get; set; }

    public double FirstRevAngle { get; set; }

    public double SecondRevAngle { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}