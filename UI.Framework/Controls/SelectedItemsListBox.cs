﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace CoLa.BimEd.UI.Framework.Controls;

public class SelectedItemsListBox : ListBox
{
    private bool _isMultipleSelectedItemsChanging;

    public SelectedItemsListBox()
    {
        this.SelectionMode = SelectionMode.Multiple;
        this.SelectionChanged += CustomControl_SelectionChanged;
    }

    public static readonly DependencyProperty MultipleSelectedItemsProperty =
        DependencyProperty.Register(
            "MultipleSelectedItems",
            typeof(IList),
            typeof(SelectedItemsListBox),
            new FrameworkPropertyMetadata(
                default,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                BindingSelectedItemsChanged
            ));

    public static readonly DependencyProperty ItemMustBeSelectedProperty =
        DependencyProperty.Register(
            "ItemMustBeSelected",
            typeof(bool),
            typeof(SelectedItemsListBox),
            new PropertyMetadata(default(bool)));

    public IList MultipleSelectedItems
    {
        get => (IList)GetValue(MultipleSelectedItemsProperty);
        set => SetValue(MultipleSelectedItemsProperty, value);
    }

    public bool ItemMustBeSelected
    {
        get => (bool)GetValue(ItemMustBeSelectedProperty);
        set => SetValue(ItemMustBeSelectedProperty, value);
    }

    private static void BindingSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not SelectedItemsListBox bindingSelectedItemsListBox)
            return;

        bindingSelectedItemsListBox.SetSelectedItems();
    }

    private void SetSelectedItems()
    {
        if (SelectionMode == SelectionMode.Single)
            return;

        _isMultipleSelectedItemsChanging = true;

        base.SetSelectedItems(MultipleSelectedItems);

        _isMultipleSelectedItemsChanging = false;
    }

    private void CustomControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        this.MultipleSelectedItems = this.SelectedItems;

        if (this.SelectionMode == SelectionMode.Single)
        {
            if (ItemMustBeSelected && e.AddedItems.Count == 0 && e.RemovedItems.Count > 0)
                base.SetSelectedItems(e.RemovedItems);

            return;
        }

        if (_isMultipleSelectedItemsChanging)
            return;

        if (this.MultipleSelectedItems == null)
        {
            this.MultipleSelectedItems = this.SelectedItems;
        }
        else
        {
            foreach (var item in e.AddedItems)
                this.MultipleSelectedItems.Add(item);

            foreach (var item in e.RemovedItems)
                this.MultipleSelectedItems.Remove(item);
        }
    }
}