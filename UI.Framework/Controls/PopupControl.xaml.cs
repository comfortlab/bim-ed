﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CoLa.BimEd.UI.Framework.Controls;

[ContentProperty(nameof(ContentPresenter))]
public partial class PopupControl
{
    public static readonly DependencyProperty ContentPresenterProperty = DependencyProperty.Register(
        nameof(ContentPresenter), typeof(FrameworkElement), typeof(PopupControl), new PropertyMetadata(default(FrameworkElement)));
    
    public static readonly DependencyProperty PopupPresenterProperty = DependencyProperty.Register(
        nameof(PopupPresenter), typeof(FrameworkElement), typeof(PopupControl), new PropertyMetadata(default(FrameworkElement)));

    public static readonly DependencyProperty PopupControlTemplateProperty = DependencyProperty.Register(
        nameof(PopupControlTemplate), typeof(ControlTemplate), typeof(PopupControl), new PropertyMetadata(default(ControlTemplate)));

    public static readonly DependencyProperty PopupStyleProperty = DependencyProperty.Register(
        nameof(PopupStyle), typeof(Style), typeof(PopupControl), new PropertyMetadata(default(Style)));

    public static readonly DependencyProperty PopupMarginProperty = DependencyProperty.Register(
        nameof(PopupMargin), typeof(Thickness), typeof(PopupControl), new PropertyMetadata(default(Thickness)));

    public PopupControl()
    {
        InitializeComponent();
    }

    public FrameworkElement ContentPresenter
    {
        get => (FrameworkElement)GetValue(ContentPresenterProperty);
        set => SetValue(ContentPresenterProperty, value);
    }

    public FrameworkElement PopupPresenter
    {
        get => (FrameworkElement)GetValue(PopupPresenterProperty);
        set => SetValue(PopupPresenterProperty, value);
    }

    public ControlTemplate PopupControlTemplate
    {
        get => (ControlTemplate)GetValue(PopupControlTemplateProperty);
        set => SetValue(PopupControlTemplateProperty, value);
    }

    public Style PopupStyle
    {
        get => (Style)GetValue(PopupStyleProperty);
        set => SetValue(PopupStyleProperty, value);
    }

    public Thickness PopupMargin
    {
        get => (Thickness)GetValue(PopupMarginProperty);
        set => SetValue(PopupMarginProperty, value);
    }
}