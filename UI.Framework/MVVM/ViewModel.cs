﻿using System;
using System.ComponentModel;

namespace CoLa.BimEd.UI.Framework.MVVM;

public class ViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;
    public Command OkCommand => new(OkAction);
    public virtual Command CancelCommand => new(() => Close?.Invoke());
    protected virtual void OkAction()
    {
        try
        {
            OkCallback?.Invoke(null);
        }
        finally
        {
            Close?.Invoke();
        }
    }
    public Action<object> OkCallback { get; set; }
    public Action Close { get; set; }
}