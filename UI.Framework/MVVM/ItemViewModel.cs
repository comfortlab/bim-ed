﻿namespace CoLa.BimEd.UI.Framework.MVVM;

public class ItemViewModel<T> : ViewModel
    where T : class
{
    protected ItemViewModel(T item) => Item = item;
    public T Item { get; }
    public bool IsSelected { get; set; }
}