﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CoLa.BimEd.UI.Framework.Converters;

public class BoolBrushConverter : IValueConverter
{
    public Brush TrueBrush { get; set; } = Brushes.Black;
    public Brush FalseBrush { get; set; } = Brushes.LightGray;


    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        value is not bool @bool ? FalseBrush : @bool ? TrueBrush : FalseBrush;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}