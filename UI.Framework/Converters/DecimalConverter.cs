﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.UI.Framework.Converters;

public class DecimalConverter : IValueConverter
{
    public int SignificantDigits { get; set; } = 4;
    public int? Digits { get; set; }
    public string Unit { get; set; }
    public double UnitFactor { get; set; } = 1;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        try
        {
            var unitFactor = UnitFactor != 0 ? UnitFactor : 1;
            var stringValue = value switch
            {
                double @double when double.IsPositiveInfinity(@double) => "∞",
                double @double when double.IsNegativeInfinity(@double) => "-∞",
                double @double => (@double * unitFactor).ToStringInvariant(SignificantDigits, Digits),
                decimal @decimal => ((double)@decimal * unitFactor).ToStringInvariant(SignificantDigits, Digits),
                _ => "0"
            };

            return $"{stringValue}{(!string.IsNullOrWhiteSpace(Unit) ? $" {Unit}" : string.Empty)}";
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
            return null;
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var stringValue = value is string @string ? @string.FirstDouble(0).ToString(CultureInfo.InvariantCulture) : value?.ToString();
        var unitFactor = UnitFactor != 0 ? UnitFactor : 1;

        if (targetType == typeof(int))
            return (int)(stringValue.FirstInt() / unitFactor);

        if (targetType == typeof(decimal) && decimal.TryParse(stringValue, NumberStyles.Any, CultureInfo.InvariantCulture, out var @decimal))
            return (decimal)((double)@decimal / unitFactor);

        if (targetType != typeof(double))
            return 0;

        return stringValue switch
        {
            "∞" => double.PositiveInfinity,
            "-∞" => double.NegativeInfinity,
            _ => stringValue.FirstDouble() / unitFactor,
        };
    }
}