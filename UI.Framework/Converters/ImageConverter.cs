﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.UI.Framework.Converters;

public class ImageConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is Bitmap bitmap ? bitmap.ToBitmapSource() : value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}