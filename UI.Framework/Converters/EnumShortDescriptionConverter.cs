﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using CoLa.BimEd.Infrastructure.Framework.Attributes;

namespace CoLa.BimEd.UI.Framework.Converters;

public class EnumShortDescriptionConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        switch (value)
        {
            case Enum @enum:
                return GetEnumShortDescription(@enum);
                
            default:
                return value?.ToString();
        }
    }

    private static string GetEnumShortDescription(Enum enumValue)
    {
        var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
        var attributes = fieldInfo.GetCustomAttributes(false);

        return attributes.OfType<ShortDescriptionAttribute>().FirstOrDefault()?.Description ??
               attributes.OfType<DescriptionAttribute>().FirstOrDefault()?.Description ??
               enumValue.ToString();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return string.Empty;
    }
}