﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class NullBoolConverter : IValueConverter
{
    public bool Invert { get; set; }
    
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return Invert switch
        {
            false => value is not null,
            true => value is null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}