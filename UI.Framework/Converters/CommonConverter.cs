using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class CommonConverter : IValueConverter
{
    public bool EnumShortDescription { get; set; }
        
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        switch (value)
        {
            case double _:
            case int _:
            case string _:
                return value.ToString();

            case Enum _ when EnumShortDescription:
                return new EnumShortDescriptionConverter().Convert(value, targetType, parameter, culture);

            case Enum _:
                return new EnumDescriptionConverter().Convert(value, targetType, parameter, culture);

            default:
                return value?.ToString();
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}