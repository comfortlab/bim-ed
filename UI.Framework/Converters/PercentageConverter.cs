﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class PercentageConverter : IValueConverter
{
    public int Decimals { get; set; }

    //E.g. DB 0.042367 --> UI "4.24 %"
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return double.TryParse(value?.ToString().Replace(',', '.') ?? "0", NumberStyles.Number, CultureInfo.InvariantCulture, out var fraction)
            ? fraction.ToString($"P{Decimals}", CultureInfo.InvariantCulture)
            : 0.ToString($"P{Decimals}", CultureInfo.InvariantCulture);
    }

    //E.g. UI "4.2367 %" --> DB 0.042367
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        //Trim any trailing percentage symbol that the user MAY have included
        var valueWithoutPercentage = value?.ToString().TrimEnd(' ', '%').Replace(',', '.');
        var validValue = !string.IsNullOrEmpty(valueWithoutPercentage) ? valueWithoutPercentage : "0";

        return double.TryParse(validValue, NumberStyles.Number, CultureInfo.InvariantCulture, out var valueResult)
            ? valueResult / 100
            : 0;
    }
}