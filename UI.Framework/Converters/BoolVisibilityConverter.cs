﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class BoolVisibilityConverter : IValueConverter
{
    public bool Invert { get; set; } = false;
    public Visibility FalseVisibility { get; set; } = Visibility.Collapsed;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            bool boolValue when Invert => boolValue ? FalseVisibility : Visibility.Visible,
            bool boolValue => boolValue ? Visibility.Visible : FalseVisibility,
            null => FalseVisibility,
            _ => null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        null;
}