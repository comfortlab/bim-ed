﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class AnyVisibilityConverter : IValueConverter
{
    public bool Invert { get; set; } = false;
    public Visibility FalseVisibility { get; set; } = Visibility.Collapsed;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var any = value switch
        {
            string @string => !string.IsNullOrWhiteSpace(@string),
            IEnumerable enumerable => enumerable.GetEnumerator().MoveNext(),
            _ => value is not null
        };
        
        return !Invert && any || Invert && !any
            ? Visibility.Visible
            : FalseVisibility;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}