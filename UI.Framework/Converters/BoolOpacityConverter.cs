﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class BoolOpacityConverter : IValueConverter
{
    public double TrueOpacity { get; set; } = 1;
    public double FalseOpacity { get; set; } = 0.3;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is true ? TrueOpacity : FalseOpacity;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}