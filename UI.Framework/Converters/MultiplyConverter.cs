﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class MultiplyConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is double doubleValue1 && parameter is double doubleValue2
            ? doubleValue1 * doubleValue2
            : value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}