﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class NullVisibilityConverter : IValueConverter
{
    public bool Invert { get; set; } = false;
    public Visibility NotNullVisibility { get; set; } = Visibility.Visible;
    public Visibility NullVisibility { get; set; } = Visibility.Collapsed;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var valueIsNotNull = value != default;

        return !Invert && valueIsNotNull || Invert && !valueIsNotNull
            ? NotNullVisibility
            : NullVisibility;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}