using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.UI.Framework.Converters;

public class VerticalOffsetVisibilityConverter : IMultiValueConverter
{
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) => values.Length switch
    {
        1 => values[0] is double doubleValue && doubleValue.IsEqualToZero()
            ? Visibility.Collapsed
            : Visibility.Visible,
        > 1 when values[0] is double offset && values[1] is double maximum => offset.IsEqualTo(maximum)
            ? Visibility.Collapsed
            : Visibility.Visible,
        _ => Visibility.Collapsed
    };

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}