using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class EnumDescriptionConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        switch (value)
        {
            case Enum @enum:
                return GetEnumDescription(@enum);

            default:
                return value?.ToString();
        }
    }

    private static string GetEnumDescription(Enum enumValue)
    {
        var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
        var attributes = fieldInfo.GetCustomAttributes(false);

        return attributes.OfType<DescriptionAttribute>().FirstOrDefault()?.Description ??
               enumValue.ToString();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return string.Empty;
    }
}