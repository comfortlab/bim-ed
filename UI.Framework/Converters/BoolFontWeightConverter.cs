﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters;

public class BoolFontWeightConverter : IValueConverter
{
    public FontWeight TrueFontWeight { get; set; } = FontWeights.DemiBold;
    public FontWeight FalseFontWeight { get; set; } = FontWeights.Normal;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        value is true
            ? TrueFontWeight
            : FalseFontWeight;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}