using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters.Local;

internal class HorizontalCenterToolTipConverter : IMultiValueConverter
{
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values.FirstOrDefault(n => n == DependencyProperty.UnsetValue) != null)
            return double.NaN;
            
        var placementTarget = (FrameworkElement)values[0];
        var placementTargetWidth = placementTarget?.ActualWidth ?? 0.0;
        var toolTipWidth = (double)values[1];
            
        return -(placementTargetWidth / 2.0) + (toolTipWidth / 2.0);
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}