﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.Framework.Converters.Local;

public class UnselectItemCommandConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        CheckBox checkBox => new Command(() => { checkBox.IsChecked = false; }),
        ListBoxItem listBoxItem => new Command(() =>
        {
            listBoxItem.IsSelected = false;

            var listBox = listBoxItem.FindVisualAncestor<ListBox>();

            if (listBox == null) return;

            if (Equals(listBox.SelectedItem, listBoxItem.DataContext)) listBox.SelectedItem = null;

            if (listBox.SelectedItems.Contains(listBoxItem.DataContext))
                listBox.SelectedItems.Remove(listBoxItem.DataContext);
        }),
        _ => null
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}