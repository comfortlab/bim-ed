using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters.Local;

internal class AddConverter : IMultiValueConverter
{
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values.FirstOrDefault(n => n == DependencyProperty.UnsetValue) != null)
            return double.NaN;

        var sign = parameter is "-1" ? -1 : 1;
        var firstAddend = Convert(values.FirstOrDefault());
        return firstAddend + sign * values.Skip(1).Sum(Convert);
    }

    private static double Convert(object value)
    {
        return value switch
        {
            double d => d,
            int i => i,
            string s => s.FirstOrDefault(),
            null => 0,
            _ => throw new ArgumentOutOfRangeException(nameof(value), value, null),
        };
    }
    
    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}