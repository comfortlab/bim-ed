using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace CoLa.BimEd.UI.Framework.Converters.Local;

internal class VerticalCenterToolTipConverter : IMultiValueConverter
{
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values.FirstOrDefault(n => n == DependencyProperty.UnsetValue) != null)
            return double.NaN;
            
        var placementTarget = (FrameworkElement)values[0];
        var placementTargetHeight = placementTarget?.ActualHeight ?? 0.0;
        var toolTipHeight = (double)values[1];
            
        return (placementTargetHeight / 2.0) - (toolTipHeight / 2.0);
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}