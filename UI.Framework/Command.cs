﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework;

public class Command : ICommand
{
    public Command() { }
    public Command(Action execute, Func<bool> canExecute = null)
    {
        CommandAction = execute;
        CanExecuteFunc = canExecute ?? (() => true);
    }

    public Action CommandAction { set; get; }
    public Func<bool> CanExecuteFunc { set; get; }

    public event EventHandler CanExecuteChanged
    {
        add => CommandManager.RequerySuggested += value;
        remove => CommandManager.RequerySuggested -= value;
    }

    public bool CanExecute(object parameter) => CanExecuteFunc == null || CanExecuteFunc();
    
    public void Execute(object parameter)
    {
        try
        {
            CommandAction?.Invoke();
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception, GetType().Namespace);
        }
    }
}

public class Command<T> : ICommand
{
    public Command() { }
    public Command(Action<T> execute, Func<bool> canExecute = null)
    {
        CommandAction = execute;
        CanExecuteFunc = canExecute ?? (() => true);
    }

    public Command(Action<T> commandAction) : this()
    {
        CommandAction = commandAction;
    }

    public Action<T> CommandAction { set; get; }
    public Func<bool> CanExecuteFunc { set; get; }

    public event EventHandler CanExecuteChanged
    {
        add => CommandManager.RequerySuggested += value;
        remove => CommandManager.RequerySuggested -= value;
    }

    public bool CanExecute(object parameter) => CanExecuteFunc == null || CanExecuteFunc();
    public void Execute(object parameter)
    {
        try
        {
            CommandAction((T) parameter);
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception, GetType().Namespace);
        }
    }
}