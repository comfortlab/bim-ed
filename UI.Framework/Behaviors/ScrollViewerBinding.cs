﻿using System.Windows;
using System.Windows.Controls;

namespace CoLa.BimEd.UI.Framework.Behaviors
{
    public static class ScrollViewerBinding
    {
        public static readonly DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.RegisterAttached("HorizontalOffset", typeof(double),
                typeof(ScrollViewerBinding), new FrameworkPropertyMetadata(double.NaN,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnHorizontalOffsetPropertyChanged));

        public static readonly DependencyProperty VerticalOffsetProperty =
            DependencyProperty.RegisterAttached("VerticalOffset", typeof(double),
                typeof(ScrollViewerBinding), new FrameworkPropertyMetadata(double.NaN,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnVerticalOffsetPropertyChanged));

        public static readonly DependencyProperty ScrollableHeightProperty =
            DependencyProperty.RegisterAttached("ScrollableHeight", typeof(double),
                typeof(ScrollViewerBinding), new FrameworkPropertyMetadata(double.NaN,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnScrollableHeightPropertyChanged));

        public static readonly DependencyProperty ScrollableWidthProperty =
            DependencyProperty.RegisterAttached("ScrollableWidth", typeof(double),
                typeof(ScrollViewerBinding), new FrameworkPropertyMetadata(double.NaN,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    OnScrollableWidthPropertyChanged));

        private static readonly DependencyProperty HorizontalScrollBindingProperty =
            DependencyProperty.RegisterAttached("HorizontalScrollBinding", typeof(bool?), typeof(ScrollViewerBinding));

        private static readonly DependencyProperty VerticalScrollBindingProperty =
            DependencyProperty.RegisterAttached("VerticalScrollBinding", typeof(bool?), typeof(ScrollViewerBinding));

        private static readonly DependencyProperty ScrollableHeightBindingProperty =
            DependencyProperty.RegisterAttached("ScrollableHeightBinding", typeof(bool?), typeof(ScrollViewerBinding));

        private static readonly DependencyProperty ScrollableWidthBindingProperty =
            DependencyProperty.RegisterAttached("ScrollableWidthBinding", typeof(bool?), typeof(ScrollViewerBinding));

        public static double GetHorizontalOffset(DependencyObject depObj) =>
            (double)depObj.GetValue(HorizontalOffsetProperty);

        public static void SetHorizontalOffset(DependencyObject depObj, double value) =>
            depObj.SetValue(HorizontalOffsetProperty, value);

        public static double GetVerticalOffset(DependencyObject depObj) =>
            (double)depObj.GetValue(VerticalOffsetProperty);

        public static void SetVerticalOffset(DependencyObject depObj, double value) =>
            depObj.SetValue(VerticalOffsetProperty, value);

        public static double GetScrollableHeight(DependencyObject depObj) =>
            (double)depObj.GetValue(ScrollableHeightProperty);

        public static void SetScrollableHeight(DependencyObject depObj, double value) =>
            depObj.SetValue(ScrollableHeightProperty, value);

        public static double GetScrollableWidth(DependencyObject depObj) =>
            (double)depObj.GetValue(ScrollableWidthProperty);

        public static void SetScrollableWidth(DependencyObject depObj, double value) =>
            depObj.SetValue(ScrollableWidthProperty, value);

        private static void OnHorizontalOffsetPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is not ScrollViewer scrollViewer)
                return;

            if (double.IsNaN((double)e.NewValue))
                return;

            BindHorizontalOffset(scrollViewer);
            scrollViewer.ScrollToHorizontalOffset((double)e.NewValue);
        }

        private static void OnVerticalOffsetPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is not ScrollViewer scrollViewer)
                return;

            if (double.IsNaN((double)e.NewValue))
                return;

            BindVerticalOffset(scrollViewer);
            scrollViewer.ScrollToVerticalOffset((double)e.NewValue);
        }

        private static void OnScrollableHeightPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is not ScrollViewer scrollViewer)
                return;

            if (double.IsNaN((double)e.NewValue))
                return;

            BindScrollableHeight(scrollViewer);
        }

        private static void OnScrollableWidthPropertyChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is not ScrollViewer scrollViewer)
                return;

            if (double.IsNaN((double)e.NewValue))
                return;

            BindScrollableWidth(scrollViewer);
        }

        public static void BindHorizontalOffset(ScrollViewer scrollViewer)
        {
            if (scrollViewer.GetValue(HorizontalScrollBindingProperty) != null)
                return;
            
            scrollViewer.SetValue(HorizontalScrollBindingProperty, true);
            scrollViewer.ScrollChanged += (s, se) =>
            {
                if (se.HorizontalChange == 0)
                    return;

                SetHorizontalOffset(scrollViewer, se.HorizontalOffset);
            };
        }

        public static void BindVerticalOffset(ScrollViewer scrollViewer)
        {
            if (scrollViewer.GetValue(VerticalScrollBindingProperty) != null)
                return;
            
            scrollViewer.SetValue(VerticalScrollBindingProperty, true);
            scrollViewer.ScrollChanged += (s, se) =>
            {
                if (se.VerticalChange == 0)
                    return;

                SetVerticalOffset(scrollViewer, se.VerticalOffset);
            };
        }

        public static void BindScrollableHeight(ScrollViewer scrollViewer)
        {
            if (scrollViewer.GetValue(ScrollableHeightBindingProperty) != null)
                return;
            
            scrollViewer.SetValue(ScrollableHeightBindingProperty, true);
            scrollViewer.ScrollChanged += (s, se) =>
            {
                if (se.VerticalChange == 0)
                    return;

                SetScrollableHeight(scrollViewer, scrollViewer.ScrollableHeight);
            };
        }

        public static void BindScrollableWidth(ScrollViewer scrollViewer)
        {
            if (scrollViewer.GetValue(ScrollableWidthBindingProperty) != null)
                return;
            
            scrollViewer.SetValue(ScrollableWidthBindingProperty, true);
            scrollViewer.ScrollChanged += (s, se) =>
            {
                if (se.HorizontalChange == 0)
                    return;

                SetScrollableWidth(scrollViewer, scrollViewer.ScrollableWidth);
            };
        }
    }
}