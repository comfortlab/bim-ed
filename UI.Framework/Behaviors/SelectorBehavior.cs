﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public static class SelectorBehavior
{
    public static readonly DependencyProperty SelectItemOnMouseUpProperty =
        DependencyProperty.RegisterAttached(
            "SelectItemOnMouseUp", typeof(bool), typeof(SelectorBehavior),
            new PropertyMetadata(default(bool), HandleShouldSelectItemOnMouseUpChange));

    public static void SetSelectItemOnMouseUp(DependencyObject element, bool value) =>
        element.SetValue(SelectItemOnMouseUpProperty, value);

    public static bool GetSelectItemOnMouseUp(DependencyObject element) =>
        (bool)element.GetValue(SelectItemOnMouseUpProperty);

    private static void HandleShouldSelectItemOnMouseUpChange(
        DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not Selector selector)
            return;

        selector.PreviewMouseDown -= HandleSelectPreviewMouseDown;
        selector.MouseUp -= HandleSelectMouseUp;

        if (!Equals(e.NewValue, true))
            return;

        selector.PreviewMouseDown += HandleSelectPreviewMouseDown;
        selector.MouseUp += HandleSelectMouseUp;
    }

    private static void HandleSelectMouseUp(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton != MouseButton.Left &&
            e.ChangedButton != MouseButton.Right ||
            sender is not Selector selector ||
            e.OriginalSource is not Visual source)
            return;
            
        var container = selector.ContainerFromElement(source);
                
        if (container == null)
            return;
        
        var index = selector.ItemContainerGenerator.IndexFromContainer(container);
                
        if (index >= 0)
            selector.SelectedIndex = index;
    }

    private static void HandleSelectPreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        var element = (DependencyObject)e.OriginalSource;
       
        while (element != null)
        {
            element = VisualTreeHelper.GetParent(element);
            
            switch (element)
            {
                case ListBoxItem:
                    e.Handled = true;
                    return;
                
                case Button:
                case CheckBox:
                case ComboBox:
                case TextBox:
                    return;
            }
        }
    }
}