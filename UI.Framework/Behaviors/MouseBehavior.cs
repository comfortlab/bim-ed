﻿using System.Windows;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public class MouseBehavior
{
    #region MouseMoveCommand

    public static readonly DependencyProperty MouseMoveCommandProperty =
        DependencyProperty.RegisterAttached("MouseMoveCommand", typeof(Command), typeof(MouseBehavior), new FrameworkPropertyMetadata(MouseMoveCommandChanged));

    private static void MouseMoveCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var element = (FrameworkElement)d;
        element.MouseMove += new MouseEventHandler(ElementMouseMove);
    }

    public static void ElementMouseMove(object sender, MouseEventArgs e)
    {
        var element = (FrameworkElement)sender;
        var command = GetMouseMoveCommand(element);

        command.Execute(e);
    }

    public static void SetMouseMoveCommand(UIElement element, Command value)
    {
        element.SetValue(MouseMoveCommandProperty, value);
    }

    public static Command GetMouseMoveCommand(UIElement element)
    {
        return (Command)element.GetValue(MouseMoveCommandProperty);
    }

    #endregion

    #region MouseLeaveCommand

    public static readonly DependencyProperty MouseLeaveCommandProperty =
        DependencyProperty.RegisterAttached("MouseLeaveCommand", typeof(Command), typeof(MouseBehavior), new FrameworkPropertyMetadata(MouseLeaveCommandChanged));

    private static void MouseLeaveCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var element = (FrameworkElement)d;
        element.MouseLeave += new MouseEventHandler(ElementMouseLeave);
    }

    private static void ElementMouseLeave(object sender, MouseEventArgs e)
    {
        var element = (FrameworkElement)sender;
        var command = GetMouseLeaveCommand(element);

        command.Execute(e);
    }

    public static void SetMouseLeaveCommand(UIElement element, Command value)
    {
        element.SetValue(MouseLeaveCommandProperty, value);
    }

    public static Command GetMouseLeaveCommand(UIElement element)
    {
        return (Command)element.GetValue(MouseLeaveCommandProperty);
    }

    #endregion

    #region MouseDownCommand

    public static readonly DependencyProperty MouseDownCommandProperty =
        DependencyProperty.RegisterAttached("MouseDownCommand", typeof(Command), typeof(MouseBehavior), new FrameworkPropertyMetadata(MouseDownCommandChanged));

    private static void MouseDownCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var element = (FrameworkElement)d;
        element.MouseDown += new MouseButtonEventHandler(ElementMouseDown);
    }

    private static void ElementMouseDown(object sender, MouseButtonEventArgs e)
    {
        var element = (FrameworkElement)sender;
        var command = GetMouseDownCommand(element);

        command.Execute(e);
    }

    public static void SetMouseDownCommand(UIElement element, Command value)
    {
        element.SetValue(MouseDownCommandProperty, value);
    }

    public static Command GetMouseDownCommand(UIElement element)
    {
        return (Command)element.GetValue(MouseDownCommandProperty);
    }

    #endregion
}