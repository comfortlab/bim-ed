﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public class TextBoxBehavior
{
    public static readonly DependencyProperty ScrollOnTextChangedProperty = DependencyProperty.RegisterAttached(
        "ScrollOnTextChanged",
        typeof(bool),
        typeof(TextBoxBehavior),
        new UIPropertyMetadata(false, OnScrollOnTextChanged));

    private static readonly Dictionary<TextBox, Capture> Associations = new();

    public static bool GetScrollOnTextChanged(DependencyObject dependencyObject)
    {
        return (bool)dependencyObject.GetValue(ScrollOnTextChangedProperty);
    }

    public static void SetScrollOnTextChanged(DependencyObject dependencyObject, bool value)
    {
        dependencyObject.SetValue(ScrollOnTextChangedProperty, value);
    }

    private static void OnScrollOnTextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
    {
        if (dependencyObject is not TextBox textBox)
            return;

        bool oldValue = (bool)e.OldValue,
            newValue = (bool)e.NewValue;

        if (newValue == oldValue)
        {
            return;
        }
            
        else if (newValue)
        {
            textBox.Loaded += TextBoxLoaded;
            textBox.Unloaded += TextBoxUnloaded;
        }

        else
        {
            textBox.Loaded -= TextBoxLoaded;
            textBox.Unloaded -= TextBoxUnloaded;
            if (Associations.ContainsKey(textBox))
            {
                Associations[textBox].Dispose();
            }
        }
    }

    private static void TextBoxUnloaded(object sender, RoutedEventArgs routedEventArgs)
    {
        var textBox = (TextBox)sender;
        Associations[textBox].Dispose();
        textBox.Unloaded -= TextBoxUnloaded;
    }

    private static void TextBoxLoaded(object sender, RoutedEventArgs routedEventArgs)
    {
        var textBox = (TextBox)sender;
        textBox.Loaded -= TextBoxLoaded;
        Associations[textBox] = new Capture(textBox);
    }

    private class Capture : IDisposable
    {
        private TextBox TextBox { get; set; }

        public Capture(TextBox textBox)
        {
            TextBox = textBox;
            TextBox.TextChanged += OnTextBoxOnTextChanged;
        }

        private void OnTextBoxOnTextChanged(object sender, TextChangedEventArgs args)
        {
            TextBox.ScrollToEnd();
        }

        public void Dispose()
        {
            TextBox.TextChanged -= OnTextBoxOnTextChanged;
        }
    }

    public static readonly DependencyProperty EnterToMoveFocusProperty = DependencyProperty.RegisterAttached(
        "EnterToMoveFocus",
        typeof(bool),
        typeof(TextBoxBehavior),
        new UIPropertyMetadata(false, OnEnterToMoveFocusChanged));

    public static bool GetEnterToMoveFocus(ListBox listBox)
    {
        return (bool)listBox.GetValue(EnterToMoveFocusProperty);
    }

    public static void SetEnterToMoveFocus(ListBox listBox, bool value)
    {
        listBox.SetValue(EnterToMoveFocusProperty, value);
    }

    private static void OnEnterToMoveFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not TextBox textBox)
            return;

        if (e.NewValue is bool == false)
            return;

        if ((bool)e.NewValue)
            textBox.AddHandler(UIElement.PreviewKeyUpEvent,
                new RoutedEventHandler(TextBoxPreviewKeyDownHandler));
        else
            textBox.RemoveHandler(UIElement.PreviewKeyUpEvent,
                new RoutedEventHandler(TextBoxPreviewKeyDownHandler));
    }

    private static void TextBoxPreviewKeyDownHandler(object sender, RoutedEventArgs e)
    {
        if (sender is not TextBox textBox ||
            e is not KeyEventArgs { Key: Key.Enter })
            return;

        var request = new TraversalRequest(FocusNavigationDirection.Next)
        {
            Wrapped = true
        };

        textBox.MoveFocus(request);
        textBox.Focus();
    }

    public static readonly DependencyProperty EnterPressedUpdatePropertyProperty = DependencyProperty.RegisterAttached(
        "EnterPressedUpdateProperty",
        typeof(DependencyProperty),
        typeof(TextBoxBehavior),
        new PropertyMetadata(null, OnUpdatePropertySourceWhenEnterPressedPropertyChanged));

    public static void SetEnterPressedUpdateProperty(DependencyObject dp, DependencyProperty value)
    {
        dp.SetValue(EnterPressedUpdatePropertyProperty, value);
    }

    public static DependencyProperty GetEnterPressedUpdateProperty(DependencyObject dp)
    {
        return (DependencyProperty)dp.GetValue(EnterPressedUpdatePropertyProperty);
    }

    private static void OnUpdatePropertySourceWhenEnterPressedPropertyChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
    {
        if (dp is not UIElement element)
        {
            return;
        }

        if (e.OldValue != null)
        {
            element.PreviewKeyDown -= HandlePreviewKeyDown;
        }

        if (e.NewValue != null)
        {
            element.PreviewKeyDown += HandlePreviewKeyDown;
        }
    }

    private static void HandlePreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            DoUpdateSource(e.Source);
        }
    }

    private static void DoUpdateSource(object source)
    {
        var property = GetEnterPressedUpdateProperty(source as DependencyObject);

        if (property == null)
        {
            return;
        }

        if (source is not UIElement element)
        {
            return;
        }

        var binding = BindingOperations.GetBindingExpression(element, property);

        binding?.UpdateSource();

        element.Focus();
        element.Focus();
    }
}