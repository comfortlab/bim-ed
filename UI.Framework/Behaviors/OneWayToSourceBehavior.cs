﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public class OneWayToSourceBehavior
{
    public static readonly DependencyProperty ActivateActualSizeProperty =
        DependencyProperty.RegisterAttached("ActivateActualSize", typeof(bool), typeof(OneWayToSourceBehavior), new FrameworkPropertyMetadata(ActivateActualSizeChanged));

    public static readonly DependencyProperty ActivateSelectedItemsProperty =
        DependencyProperty.RegisterAttached("ActivateSelectedItems", typeof(bool), typeof(OneWayToSourceBehavior), new FrameworkPropertyMetadata(ActivateSelectedItemsChanged));

    public static readonly DependencyProperty ActualHeightProperty =
        DependencyProperty.RegisterAttached("ActualHeight", typeof(double), typeof(OneWayToSourceBehavior));

    public static readonly DependencyProperty ActualWidthProperty =
        DependencyProperty.RegisterAttached("ActualWidth", typeof(double), typeof(OneWayToSourceBehavior));

    public static readonly DependencyProperty SelectedItemsProperty =
        DependencyProperty.RegisterAttached("SelectedItems", typeof(IList), typeof(OneWayToSourceBehavior), new FrameworkPropertyMetadata(default, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    private static void ActivateActualSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not FrameworkElement element ||
            !GetActivateActualSize(element))
            return;

        SetActualHeight(element, element.ActualHeight);
        SetActualWidth(element, element.ActualWidth);

        element.SizeChanged += ElementOnSizeChanged;
    }

    private static void ElementOnSizeChanged(object sender, SizeChangedEventArgs e)
    {
        var element = (FrameworkElement)sender;

        SetActualHeight(element, e.NewSize.Height);
        SetActualWidth(element, e.NewSize.Width);
    }

    private static void ActivateSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        switch (d)
        {
            case MultiSelector multiSelector when GetActivateSelectedItems(multiSelector):
                SetSelectedItems(multiSelector, multiSelector.SelectedItems);
                multiSelector.SelectionChanged += ElementOnSelectionChanged;
                break;
            
            case ListBox listBox when GetActivateSelectedItems(listBox):
                SetSelectedItems(listBox, listBox.SelectedItems);
                listBox.SelectionChanged += ElementOnSelectionChanged;
                break;
        }
    }

    private static void ElementOnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var selectorItems = sender switch
        {
            ListBox listBox => listBox.SelectedItems,
            MultiSelector multiSelector => multiSelector.SelectedItems,
            _ => default
        };

        SetSelectedItems((UIElement)sender, selectorItems);
    }

    public static bool GetActivateActualSize(UIElement element) =>
        (bool)element.GetValue(ActivateActualSizeProperty);

    public static void SetActivateActualSize(UIElement element, bool value) =>
        element.SetValue(ActivateActualSizeProperty, value);

    public static bool GetActivateSelectedItems(UIElement element) =>
        (bool)element.GetValue(ActivateSelectedItemsProperty);

    public static void SetActivateSelectedItems(UIElement element, bool value) =>
        element.SetValue(ActivateSelectedItemsProperty, value);

    public static double GetActualHeight(UIElement element) =>
        (double)element.GetValue(ActualHeightProperty);

    public static void SetActualHeight(UIElement element, double value) =>
        element.SetValue(ActualHeightProperty, value);

    public static double GetActualWidth(UIElement element) =>
        (double)element.GetValue(ActualWidthProperty);

    public static void SetActualWidth(UIElement element, double value) =>
        element.SetValue(ActualWidthProperty, value);

    public static IList GetSelectedItems(UIElement element) =>
        (IList)element.GetValue(SelectedItemsProperty);

    public static void SetSelectedItems(UIElement element, IList value) =>
        element.SetValue(SelectedItemsProperty, value);
}