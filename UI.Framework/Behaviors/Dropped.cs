﻿using System;
using System.Windows;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public class Dropped<T> : IDropped where T : class
{
    public Dropped(Action<T> droppedAction) => DroppedAction = droppedAction;
    public Action<T> DroppedAction { get; }
    public void OnDropped(IDataObject dataObject) => DroppedAction?.Invoke(dataObject.GetData(typeof(T)) as T);
}