﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace CoLa.BimEd.UI.Framework.Behaviors;

public interface IDropped
{
    void OnDropped(IDataObject dataObject);
}

public interface IDragged
{
    void DragCallback();
    void DropCallback();
}
    
public class DragDropBehavior
{
    public static readonly DependencyProperty IsDragSourceProperty = DependencyProperty.RegisterAttached(
        "IsDragSource", typeof(bool), typeof(DragDropBehavior), new FrameworkPropertyMetadata(default(bool), IsDragSourceChanged)
        {
            BindsTwoWayByDefault = false,
        });

    public static readonly DependencyProperty IsDropTargetProperty = DependencyProperty.RegisterAttached(
        "IsDropTarget", typeof(bool), typeof(DragDropBehavior), new FrameworkPropertyMetadata(default(bool), IsDropTargetChanged)
        {
            BindsTwoWayByDefault = false,
        });

    public static void SetIsDragSource(DependencyObject element, bool value) =>
        element.SetValue(IsDragSourceProperty, value);

    public static bool GetIsDragSource(DependencyObject element) =>
        (bool)element.GetValue(IsDragSourceProperty);

    public static void SetIsDropTarget(DependencyObject element, bool value) =>
        element.SetValue(IsDropTargetProperty, value);

    public static bool GetIsDropTarget(DependencyObject element) =>
        (bool)element.GetValue(IsDropTargetProperty);

    private static void IsDragSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not FrameworkElement frameworkElement)
            throw new InvalidOperationException();
            
        frameworkElement.PreviewMouseMove -= OnPreviewDrag;

        if ((bool)e.NewValue)
            frameworkElement.PreviewMouseMove += OnPreviewDrag;
    }

    private static void OnPreviewDrag(object sender, MouseEventArgs e)
    {
        if (e.LeftButton != MouseButtonState.Pressed)
            return;

        var frameworkElement = (FrameworkElement)sender;
        var dataContext = frameworkElement.DataContext;
        var draggedDataContext = dataContext as IDragged;
            
        draggedDataContext?.DragCallback();

        DragDrop.DoDragDrop((DependencyObject)sender, dataContext, DragDropEffects.All);

        draggedDataContext?.DropCallback();
    }

    private static void IsDropTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not FrameworkElement frameworkElement)
            throw new InvalidOperationException();
            
        frameworkElement.AllowDrop = true;
        frameworkElement.Drop -= OnDrop;
        frameworkElement.PreviewDragOver -= OnPreviewDragOver;

        if ((bool) e.NewValue == false)
            return;

        frameworkElement.Drop += OnDrop;
        frameworkElement.PreviewDragOver += OnPreviewDragOver;
    }

    private static void OnPreviewDragOver(object sender, DragEventArgs e)
    {
        // NOTE: PreviewDragOver subscription is required at least when FrameworkElement is a TextBox
        // because it appears that TextBox by default prevent Drag on preview...
        e.Effects = DragDropEffects.Move;
        e.Handled = true;
    }

    private static void OnDrop(object sender, DragEventArgs e)
    {
        var dataContext = ((FrameworkElement)sender).DataContext;
            
        if (dataContext is not IDropped dropped)
        {
            if (dataContext != null)
                Debug.WriteLine($"Binding error, '{dataContext.GetType().Name}' doesn't implement '{nameof(IDropped)}'.");
            
            return;
        }

        dropped.OnDropped(e.Data);
    }
}