﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalSetting.Views.LoadClassifications;

public partial class SelectorSetting
{
    public SelectorSetting() => InitializeComponent();
    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) => ScrollingUtils.ScrollMouseWheel(sender, e);
}