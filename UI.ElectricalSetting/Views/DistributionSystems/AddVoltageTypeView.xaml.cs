﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalSetting.Views.DistributionSystems;

public partial class AddVoltageTypeView
{
    public AddVoltageTypeView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(NameTextBox);
    }

    private void UIElement_OnKeyDown(object sender, KeyEventArgs e) => FocusUtils.KeyMoveFocus(sender, e);
}