﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalSetting.Views.DistributionSystems;

public partial class DistributionSystemsView
{
    public DistributionSystemsView() => InitializeComponent();
    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) => ScrollingUtils.MultiScrollMouseWheel(sender, e);
}