using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

namespace CoLa.BimEd.UI.ElectricalSetting.Converters;

public class CharacteristicValueConverter : IValueConverter
{
    private static readonly SeApi.Translation Translation = DI.Get<SeApi.Translation>();
        
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is string id
            ? Translation.Translations.GetValueTranslation(id, DI.Get<Localization>().Culture)
            : null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}