﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.ElectricalSetting.Converters;

public class CalculationOptionConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            DemandFactorCalculationOption.IncrementallyForEachRange => ElectricalParameters.DemandFactorCalculationOption_Incrementally,
            DemandFactorCalculationOption.TotalAtOnePercentage => ElectricalParameters.DemandFactorCalculationOption_Total,
            _ => value,
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}