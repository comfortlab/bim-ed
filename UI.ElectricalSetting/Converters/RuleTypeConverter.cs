﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.ElectricalSetting.Converters;

public class RuleTypeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            DemandFactorRuleProxy.Constant => ElectricalParameters.DemandFactorRule_Constant,
            DemandFactorRuleProxy.LoadTable => ElectricalParameters.DemandFactorRule_ByLoad,
            DemandFactorRuleProxy.LoadTablePerPortion => ElectricalParameters.DemandFactorRule_ByLoad,
            DemandFactorRuleProxy.QuantityTable => ElectricalParameters.DemandFactorRule_ByQuantity,
            DemandFactorRuleProxy.QuantityTablePerPortion => ElectricalParameters.DemandFactorRule_ByQuantity,
            _ => value
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}