using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.Views.LoadClassifications;
using CoLa.BimEd.UI.RtmDemandFactors.ViewModels;
using CoLa.BimEd.UI.RtmDemandFactors.Views;

namespace CoLa.BimEd.UI.ElectricalSetting.Converters;

public class DataContextViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            ImportLoadClassificationsViewModel => new ImportLoadClassificationsView(),
            RtmDemandFactorsViewModel => new RtmDemandFactorsView(),
            _ => value
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}