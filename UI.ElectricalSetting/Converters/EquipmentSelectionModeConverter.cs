using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.ElectricalSetting.Converters;

public class EquipmentSelectionModeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is EquipmentSelectionMode equipmentSelectionMode
            ? equipmentSelectionMode switch
            {
                EquipmentSelectionMode.ByCable => ElectricalParameters.EquipmentSelectionByCable,
                EquipmentSelectionMode.ByLoad => ElectricalParameters.EquipmentSelectionByLoad,
                _ => null,
            }
            : null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}