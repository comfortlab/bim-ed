﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;
using CoLa.BimEd.UI.Resources.Electrical;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.OperatingModeSetting;

public class OperatingModesViewModel : ViewModel
{
    private readonly Repository<OperatingModes> _operatingModesRepository;
    private readonly OperatingModes _operatingModes;
    private readonly OperatingModes _operatingModesClone;
    private string[] _forbiddenNames;
    private int _newRevitId = -1;
    private int NewRevitId() => _newRevitId--;
    
    internal ChangedElements ChangedElements => _operatingModesRepository.ChangedElements;
    
    public OperatingModesViewModel(
        Repository<OperatingModes> operatingModesRepository,
        OperatingModes operatingModes,
        OperatingModeElements operatingModeElements)
    {
        _operatingModesRepository = operatingModesRepository;
        _operatingModes = operatingModes;
        _operatingModesClone = _operatingModes.Clone();
        OperatingModeElements = operatingModeElements;
        
        AddOperatingModeColumns();
        AddElementRows();
        UpdateCanDelete();

        OperatingModesDataTable.ColumnChanged += OperatingModesDataTableOnColumnChanged;
    }

    private OperatingModeElements OperatingModeElements { get; }
    public DataTable OperatingModesDataTable { get; set; } = new();
    public OperatingMode SelectedOperatingMode { get; set; }
    public string DialogTitle { get; set; }
    public string OldName { get; set; }
    public string NewName { get; set; }
    public bool IsMainContentEnabled { get; set; } = true;
    public bool CanDelete { get; set; }
    public bool CreateVisible { get; set; }
    public bool DeleteVisible { get; set; }
    public bool RenameVisible { get; set; }
    public bool NameOkEnabled { get; set; }

    private void OnCreateVisibleChanged() => IsMainContentEnabled = !CreateVisible;
    private void OnDeleteVisibleChanged() => IsMainContentEnabled = !DeleteVisible;
    private void OnRenameVisibleChanged() => IsMainContentEnabled = !RenameVisible;
    private void OnNewNameChanged() => NameOkEnabled = !_forbiddenNames?.Contains(NewName.Trim()) ?? true;

    protected override void OkAction()
    {
        if (!ChangedElements.IsAnyChanged)
            return;
        
        _operatingModes.PullProperties(_operatingModesClone);
        _operatingModesRepository.Save();
        
        base.OkAction();
    }

    public Command<string> SelectedOperatingModeCommand => new(name => SelectedOperatingMode = _operatingModesClone.FirstOrDefault(x => x.Name == name));
    public Command CreateCommand => new(() =>
    {
        _forbiddenNames = GetForbiddenNames();
        NewName = NameUtils.GetNextName(ElectricalParameters.OperatingMode, _forbiddenNames);
        DialogTitle = $"{CommandNames.Create} {ElectricalParameters.OperatingMode.ToLower()}";
        CreateVisible = true;
    });
    public Command CreateOkCommand => new(() =>
    {
        CreateOperatingMode(NewName);
        UpdateCanDelete();
        CreateVisible = false;
    });
    
    public Command CopyCommand => new(() =>
    {
        var newName = NameUtils.GetNextName(SelectedOperatingMode.Name, GetForbiddenNames());
        CreateOperatingMode(newName, SelectedOperatingMode.Name);
        UpdateCanDelete();
    });
    
    public Command DeleteCommand => new(() =>
    {
        if (SelectedOperatingMode == null)
            return;
        
        DialogTitle = $"{CommandNames.Delete} {ElectricalParameters.OperatingMode.ToLower()}";
        DeleteVisible = true;
    });
    public Command DeleteOkCommand => new(() =>
    {
        _operatingModesClone.RemoveAll(x => x.Name == SelectedOperatingMode.Name);
        OperatingModesDataTable.Columns.Remove(SelectedOperatingMode.Name);
        ChangedElements.UpdateStorage();
        SelectedOperatingMode = null;
        UpdateCanDelete();
        UpdateTable();
        DeleteVisible = false;
    });

    public Command RenameCommand => new(() =>
    {
        if (SelectedOperatingMode == null)
            return;

        _forbiddenNames = GetForbiddenNames(SelectedOperatingMode.Name);
        NewName = NameUtils.GetNextName(SelectedOperatingMode.Name, _forbiddenNames);
        OldName = NewName;
        DialogTitle = $"{CommandNames.Rename} {ElectricalParameters.OperatingMode.ToLower()}";
        RenameVisible = true;
    });
    public Command RenameOkCommand => new(() =>
    {
        OperatingModesDataTable.Columns[SelectedOperatingMode.Name].ColumnName = NewName;
        SelectedOperatingMode.Name = NewName;
        ChangedElements.UpdateStorage();
        UpdateTable();
        RenameVisible = false;
    });
    
    public Command ItemCancelCommand => new(() => CreateVisible = DeleteVisible = RenameVisible = false);

    private void AddOperatingModeColumns()
    {
        OperatingModesDataTable.Columns.Add();

        foreach (var operatingMode in _operatingModesClone)
        {
            OperatingModesDataTable.Columns.Add(operatingMode.Name, typeof(bool));
        }
    }

    private void AddElementRows()
    {
        foreach (var element in OperatingModeElements.Elements)
        {
            var row = OperatingModesDataTable.NewRow();
            OperatingModesDataTable.Rows.Add(row);

            row[0] = element.Name;

            foreach (var operatingMode in _operatingModesClone)
                row[operatingMode.Name] = !operatingMode.DisabledElements.Contains(element.RevitId);
        }
    }
    
    private void CreateOperatingMode(string name, string prototypeName = null)
    {
        var newOperatingMode = new OperatingMode(name);
        _operatingModesClone.Add(newOperatingMode);
        ChangedElements.UpdateStorage();
        
        var newColumn = OperatingModesDataTable.Columns.Add(name, typeof(bool));

        if (prototypeName != null)
        {
            var prototypeIndex = OperatingModesDataTable.Columns.IndexOf(prototypeName);
            
            if (prototypeIndex >= 0)
                newColumn.SetOrdinal(prototypeIndex + 1);
        }

        foreach (DataRow row in OperatingModesDataTable.Rows)
        {
            row[name] = prototypeName != null ? row[prototypeName] : true;
        }

        UpdateTable();
    }

    private void OperatingModesDataTableOnColumnChanged(object sender, DataColumnChangeEventArgs e)
    {
        var columnName = e.Column.ColumnName;
        var operatingMode = _operatingModesClone.FirstOrDefault(x => x.Name == columnName);
        if (operatingMode == null)
        {
            Debug.WriteLine($"{nameof(operatingMode)} is NULL. {nameof(columnName)}: {columnName}", GetType().Namespace);
            return;
        }

        var rowIndex = OperatingModesDataTable.Rows.IndexOf(e.Row);
        var element = OperatingModeElements.Elements.ElementAtOrDefault(rowIndex);
        if (element == null)
        {
            Debug.WriteLine($"{nameof(element)} is NULL. {nameof(rowIndex)}: {rowIndex}", GetType().Namespace);
            return;
        }

        if (e.ProposedValue is false)
            operatingMode.DisabledElements.Add(element.RevitId);
        else
            operatingMode.DisabledElements.Remove(element.RevitId);

        ChangedElements.UpdateStorage();
    }
    
    private string[] GetForbiddenNames(params string[] exceptionNames) => _operatingModesClone.Select(x => x.Name).Where(x => !exceptionNames.Contains(x)).ToArray();
    private void UpdateCanDelete() => CanDelete = _operatingModesClone.Count > 1;
    internal void UpdateOperatingModesOrder()
    {
        var oldOperatingModes = _operatingModesClone.ToArray();
        _operatingModesClone.Clear();
        
        try
        {
            foreach (DataColumn column in OperatingModesDataTable.Columns)
            {
                var operatingMode = oldOperatingModes.FirstOrDefault(x => x.Name == column.ColumnName);
                
                if (operatingMode != null)
                    _operatingModesClone.Add(operatingMode);
            }

            ChangedElements.UpdateStorage();
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception, GetType().Namespace);
            _operatingModesClone.Clear();
            _operatingModesClone.AddRange(oldOperatingModes);
        }
    }
    private void UpdateTable()
    {
        var temp = OperatingModesDataTable;
        OperatingModesDataTable = new DataTable();
        OperatingModesDataTable = temp;
    }
}