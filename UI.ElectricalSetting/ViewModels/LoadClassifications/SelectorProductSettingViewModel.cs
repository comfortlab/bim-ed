﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Framework;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class SelectorProductSettingViewModel : SelectorViewModel
{
    private readonly bool _isInitialized;
    private readonly Action _update;
    private readonly ProductRange _productRange;
    private readonly SelectorProductSettings _allSelectorProductSettings;
    private readonly SelectorProductSetting _selectorProductSetting;

    private bool _isCharacteristicsUpdating;

    public double MaxCurrent { get; set; }
    public double MinCurrent { get; set; }
    public List<CharacteristicViewModel> CharacteristicViewModels { get; set; }
    public ObservableCollection<string> AccessoryReferences { get; set; }
    public string Text { get; set; }
    public bool ShowButtonForAddAccessories { get; set; }
    public bool IsActualAccessories { get; set; }

    public SelectorProductSettingViewModel(
        SelectorProductSettings allSelectorProductSettings,
        SelectorProductSetting selectorProductSetting,
        Action update) :
        base(selectorProductSetting.SelectorId)
    {
        _allSelectorProductSettings = allSelectorProductSettings;
        _selectorProductSetting = selectorProductSetting;
        _productRange = selectorProductSetting.ProductRange;
        _update = update;

        MaxCurrent = selectorProductSetting.MaxCurrent;
        MinCurrent = selectorProductSetting.MinCurrent;

        CharacteristicViewModels = _productRange.CharacteristicValues
            .Select(n => new CharacteristicViewModel(this, n.Key, n.Value))
            .ToList();

        UpdateSelectedCharacteristics();

        Text = _productRange.CharacteristicValues?.Select(n => $"{n.Key}: {n.Value.JoinToString(", ")}")
            .JoinToString("\n");

        var filterProducts = _productRange.Filter((SeApi.Characteristics.CurveCode, "C"));

        Text += $"\n Products.Count = {_productRange.Products.Count} \n filterProducts.Count() = {filterProducts.Count()}";

        var selector = DI.Get<SeApi.Selectors>().GetById(selectorProductSetting.SelectorId);
        ShowButtonForAddAccessories = selector.CanHaveAccessories;

        AccessoryReferences = _selectorProductSetting.AccessoryReferences.ToObservableCollection();
        IsActualAccessories = _selectorProductSetting.IsActualAccessories;

        _isInitialized = true;
    }

    public Command DeleteProductCommand => new(() =>
    {
        _allSelectorProductSettings.Remove(_selectorProductSetting);
        _update?.Invoke();
    });

    public Command AddAccessoriesCommand => new(AddAccessories);

    private async void AddAccessories()
    {
        try
        {
            // var accessoriesSelectorView = await AccessoriesSelectorView.CreateInstance(
            //     selectorId: _selectorProductSetting.SelectorId,
            //     configuration: _selectorProductSetting.SerializedConfiguration,
            //     selectedValues: _productRange.SelectedValues,
            //     minCurrent: MinCurrent.Value,
            //     maxCurrent: MaxCurrent.Value,
            //     callback: ApplyAccessoriesSetting);
                
            // DialogStackView.PushView(viewContent: accessoriesSelectorView, buttons: DialogButtons.CancelClose);
        }
        catch (Exception exception)
        {
            // Logger.Error(exception);
        }
    }

    internal void ApplyAccessoriesSetting(SelectorConfig config)
    {
        try
        {
            _selectorProductSetting.SerializedConfiguration = config.Serialize();

            var firstReference = config.Data.Selections.State.References.FirstOrDefault();

            _selectorProductSetting.AccessoryReferences = config.Data.Selections.State.References
                .Where(r => r != firstReference)
                .Select(r => r.ReferenceId)
                .Distinct()
                .ToList();

            AccessoryReferences = _selectorProductSetting.AccessoryReferences.ToObservableCollection();

            IsActualAccessories = true;
        }
        finally
        {
            // DialogStackView.CloseLastView();
        }
    }

    private void OnIsActualAccessoriesChanged()
    {
        _selectorProductSetting.IsActualAccessories = IsActualAccessories;
    }

    private bool _isUpdatingCurrent;

    private void OnMaxCurrentChanged()
    {
        if (!_isInitialized || _isCharacteristicsUpdating || _isUpdatingCurrent || MaxCurrent == null)
        {
            return;
        }

        _isUpdatingCurrent = true;

        if (MaxCurrent < MinCurrent)
        {
            MaxCurrent = MinCurrent;
        }

        if (MaxCurrent == MinCurrent)
        {
            ResetBadCurrentRange();
        }

        _allSelectorProductSettings.SetMaxCurrent(_selectorProductSetting, MaxCurrent);

        UpdateCurrentsValues();
        UpdateCharacteristics();

        _isUpdatingCurrent = false;

        _update();
    }

    private void OnMinCurrentChanged()
    {
        if (!_isInitialized || _isCharacteristicsUpdating || _isUpdatingCurrent || MinCurrent == null)
        {
            return;
        }

        _isUpdatingCurrent = true;

        if (MinCurrent > MaxCurrent)
        {
            MinCurrent = MaxCurrent;
        }

        if (MinCurrent == MaxCurrent)
        {
            ResetBadCurrentRange();
        }

        _allSelectorProductSettings.SetMinCurrent(_selectorProductSetting, MinCurrent);

        UpdateCurrentsValues();
        UpdateCharacteristics();

        _isUpdatingCurrent = false;

        _update();
    }

    public void UpdateCurrentsValues()
    {
        if (MinCurrent != _selectorProductSetting.MinCurrent)
        {
            MinCurrent = _selectorProductSetting.MinCurrent;
        }

        if (MaxCurrent != _selectorProductSetting.MaxCurrent)
        {
            MaxCurrent = _selectorProductSetting.MaxCurrent;
        }
    }

    private void ResetBadCurrentRange()
    {
        var currents = _productRange.Products
            .Select(p =>
                p.GetCharacteristic(SeApi.Characteristics.RatedOperationalCurrent)?.Value?.FirstDoubleOrNull() ??
                p.GetCharacteristic(SeApi.Characteristics.RatedCurrent)?.Value?.FirstDoubleOrNull())
            .Where(c => c != null)
            .Distinct()
            .OrderBy(c => c)
            .ToArray();

        var minCurrent = currents.Where(c => c < MinCurrent).Max() ??
                         _allSelectorProductSettings.GetNewMinCurrentValue(_selectorProductSetting);

        var maxCurrent = currents.Where(c => c > minCurrent).Min() ??
                         currents.Max() ??
                         _selectorProductSetting.SelectorMaxCurrent;

        MinCurrent = minCurrent;
        MaxCurrent = maxCurrent;

        _allSelectorProductSettings.SetMaxCurrent(_selectorProductSetting, MaxCurrent);
        _allSelectorProductSettings.SetMinCurrent(_selectorProductSetting, MinCurrent);

        UpdateCurrentsValues();
    }

    private void RemoveBadSelectedValues()
    {
        var productsFilteredByCurrent = GetFilteredProducts(null);

        _productRange.SelectedValues = _productRange.SelectedValues
            .Where(sv => productsFilteredByCurrent.Any(p => p.GetCharacteristic(sv.Key).Value == sv.Value))
            .ToDictionary(sv => sv.Key, sv => sv.Value);
    }

    private void SetSelectedValues()
    {
        _productRange.SelectedValues = CharacteristicViewModels
            .Where(n => n.SelectedValue != null)
            .ToDictionary(n => n.Name, n => n.SelectedValue);
    }

    private static readonly List<string> CurrentCharacteristics = new List<string>
    {
        SeApi.Characteristics.RatedOperationalCurrent,
        SeApi.Characteristics.RatedCurrent
    };

    internal void UpdateCharacteristics()
    {
        if (_isCharacteristicsUpdating)
            return;

        IsActualAccessories = false;

        _isCharacteristicsUpdating = true;

        SetSelectedValues();
        RemoveBadSelectedValues();

        var filteredProducts = GetFilteredProducts(_productRange.SelectedValues).ToArray();

        if (filteredProducts.IsEmpty())
        {
            ResetBadCurrentRange();
            RemoveBadSelectedValues();

            filteredProducts = GetFilteredProducts(_productRange.SelectedValues).ToArray();
        }

        CharacteristicViewModels = (from characteristic in _productRange.CharacteristicValues.Select(n => n.Key)
            let values = filteredProducts.SelectMany(n => n.Characteristics)
                .Where(n => n.Key == characteristic)
                .Select(n => n.Value.Value)
                .Distinct()
            select new CharacteristicViewModel(this, characteristic, values)).ToList();

        UpdateSelectedCharacteristics();

        _isCharacteristicsUpdating = false;
    }

    private IEnumerable<BaseProduct> GetFilteredProducts(Dictionary<string, string> filterCharacteristics)
    {
        return _productRange
            .Filter(filterCharacteristics, CurrentCharacteristics, MinCurrent, MaxCurrent);
    }

    private void UpdateSelectedCharacteristics()
    {
        foreach (var selectedValue in _productRange.SelectedValues)
        {
            var viewModel = CharacteristicViewModels.FirstOrDefault(n => n.Name == selectedValue.Key);

            if (viewModel != null)
                viewModel.SelectedValue = selectedValue.Value;
        }
    }
}