﻿using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class ImportLoadClassificationsViewModel : ViewModel
{
    public ImportLoadClassificationsViewModel(
        Repository<LoadClassificationProxy> existLoadClassifications,
        Repository<LoadClassificationDirectory> loadClassificationDirectories)
    {
        LoadClassifications = existLoadClassifications.ToObservableCollection();
        LoadClassificationDirectory = loadClassificationDirectories.First().LoadClassifications.ToArray();
    }
    
    public ObservableCollection<LoadClassificationProxy> LoadClassifications { get; set; }
    public LoadClassificationProxy[] LoadClassificationDirectory { get; set; }
}