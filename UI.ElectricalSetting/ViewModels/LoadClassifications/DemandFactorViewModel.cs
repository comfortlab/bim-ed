﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class DemandFactorViewModel : ItemViewModel
{
    private readonly bool _isInitialized;
    private ChangedElements ChangedElements => LoadClassificationsViewModel.ChangedElements;
    
    public DemandFactorViewModel(LoadClassificationsViewModel loadClassificationsViewModel, DemandFactorProxy item) :
        base(loadClassificationsViewModel, item)
    {
        try
        {
            DemandFactor = item;

            if (DemandFactor == null)
                return;

            Values = DemandFactor.Values.Select(x => new ValueViewModel(this, x)).ToObservableCollection();
            UpdateFirstLastValues();
            ConstantValue = DemandFactor.Values[0].Factor;
            AdditionalLoad = DemandFactor.AdditionalLoad;
            IncludeAdditionalLoad = DemandFactor.IncludeAdditionalLoad;
            SelectedRuleType = _previousRuleType = DemandFactor?.RuleType switch
            {
                DemandFactorRuleProxy.LoadTable => DemandFactorRuleProxy.LoadTable,
                DemandFactorRuleProxy.LoadTablePerPortion => DemandFactorRuleProxy.LoadTable,
                DemandFactorRuleProxy.QuantityTable => DemandFactorRuleProxy.QuantityTable,
                DemandFactorRuleProxy.QuantityTablePerPortion => DemandFactorRuleProxy.QuantityTable,
                _ => DemandFactorRuleProxy.Constant
            };
            SelectedCalculationOption = DemandFactor?.RuleType switch
            {
                DemandFactorRuleProxy.LoadTablePerPortion => DemandFactorCalculationOption.IncrementallyForEachRange,
                DemandFactorRuleProxy.QuantityTablePerPortion => DemandFactorCalculationOption.IncrementallyForEachRange,
                _ => DemandFactorCalculationOption.TotalAtOnePercentage
            };
        }
        finally
        {
            _isInitialized = true;
        }
    }

    public DemandFactorProxy DemandFactor { get; }

    public DemandFactorRuleProxy[] RuleTypes { get; } =
    {
        DemandFactorRuleProxy.Constant,
        DemandFactorRuleProxy.QuantityTable,
        DemandFactorRuleProxy.LoadTable,
    };

    public DemandFactorCalculationOption[] CalculationOptions { get; } =
    {
        DemandFactorCalculationOption.TotalAtOnePercentage,
        DemandFactorCalculationOption.IncrementallyForEachRange,
    };

    private DemandFactorRuleProxy _previousRuleType;
    public DemandFactorRuleProxy SelectedRuleType { get; set; }
    public DemandFactorCalculationOption SelectedCalculationOption { get; set; }
    public double ConstantValue { get; set; }
    public double AdditionalLoad { get; set; }
    public ObservableCollection<ValueViewModel> Values { get; set; }
    public ValueViewModel SelectedValue { get; set; }
    public bool IncludeAdditionalLoad { get; set; }
    public bool AnyLoadClassificationSelected { get; set; }
    public bool ByRange { get; set; }
    public bool ByLoad { get; set; }
    public bool ByQuantity { get; set; }
    public bool CanBeDeleted { get; set; }
    public bool CanBeApplied { get; set; }
    public bool CanAddValue { get; set; } = true;
    public bool CanRemoveValue { get; set; }

    private void OnConstantValueChanged()
    {
        DemandFactor.Values[0].Factor = ConstantValue;

        if (Values?.Any() ?? false)
            Values[0].Factor = ConstantValue;
        
        Modify();
    }

    private void OnAdditionalLoadChanged()
    {
        DemandFactor.AdditionalLoad = AdditionalLoad;
        Modify();
    }

    private void OnIncludeAdditionalLoadChanged()
    {
        DemandFactor.IncludeAdditionalLoad = IncludeAdditionalLoad;
        Modify();
    }

    private void OnSelectedCalculationOptionChanged()
    {
        UpdateDemandFactorRuleType();
        Modify();
    }

    private void OnSelectedValueChanged()
    {
        CanAddValue = SelectedValue == null || SelectedValue.MinRange < SelectedValue.MaxRange - 1;
        CanRemoveValue = SelectedValue != null && Values.Count > 1;
    }

    private void OnSelectedRuleTypeChanged()
    {
        if (_previousRuleType == DemandFactorRuleProxy.LoadTable &&
            SelectedRuleType == DemandFactorRuleProxy.QuantityTable)
        {
            foreach (var value in Values.ToArray())
            {
                if (value.MaxRange - value.MinRange < 1000) RemoveValue(value, out _);
            }
            
            foreach (var value in Values)
            {
                if (!value.IsLast) value.MaxRange = (value.MaxRange / 1000).Round();
            }
        }
        else if (_previousRuleType == DemandFactorRuleProxy.QuantityTable &&
                 SelectedRuleType == DemandFactorRuleProxy.LoadTable)
        {
            foreach (var value in Values.Inverse())
            {
                if (!value.IsLast) value.MaxRange *= 1000;
            }
        }

        _previousRuleType = SelectedRuleType;
        
        UpdateDemandFactorRuleType();
        Modify();
    }

    private void UpdateDemandFactorRuleType()
    {
        ByRange = SelectedRuleType is DemandFactorRuleProxy.LoadTable or DemandFactorRuleProxy.QuantityTable;
        ByLoad = SelectedRuleType is DemandFactorRuleProxy.LoadTable or DemandFactorRuleProxy.LoadTablePerPortion;
        ByQuantity = SelectedRuleType is DemandFactorRuleProxy.QuantityTable or DemandFactorRuleProxy.QuantityTablePerPortion;

        if (!_isInitialized)
            return;

        DemandFactor.RuleType = SelectedRuleType switch
        {
            DemandFactorRuleProxy.Constant => DemandFactorRuleProxy.Constant,
            DemandFactorRuleProxy.LoadTable when SelectedCalculationOption is DemandFactorCalculationOption.IncrementallyForEachRange => DemandFactorRuleProxy.LoadTablePerPortion,
            DemandFactorRuleProxy.LoadTable => DemandFactorRuleProxy.LoadTable,
            DemandFactorRuleProxy.QuantityTable when SelectedCalculationOption is DemandFactorCalculationOption.IncrementallyForEachRange => DemandFactorRuleProxy.QuantityTablePerPortion,
            DemandFactorRuleProxy.QuantityTable => DemandFactorRuleProxy.QuantityTable,
            _ => throw new ArgumentOutOfRangeException(nameof(DemandFactor.RuleType), DemandFactor.RuleType, null)
        };
    }

    public Command ApplyCommand => new(() =>
    {
        if (LoadClassificationsViewModel.SelectedLoadClassification
                ?.Item is LoadClassificationProxy loadClassification &&
            Item is DemandFactorProxy demandFactor)
            loadClassification.DemandFactor = demandFactor;

        LoadClassificationsViewModel.UpdateCanBeAppliedDemandFactors();
        LoadClassificationsViewModel.UpdateCanBeDeletedDemandFactors();
        LoadClassificationsViewModel.SelectedLoadClassification.Modify();
    });

    public Command AddValueCommand => new(() =>
    {
        if (SelectedValue == null || SelectedValue.IsLast || Values.Count == 1)
            AddLastValue();
        else
            AddMediumValue();

        UpdateFirstLastValues();
        Modify();
    });

    private void AddLastValue()
    {
        var add = ByQuantity ? 10 : 10000;
        var lastValue = Values.Last();
        lastValue.MaxRange = lastValue.MinRange + add;
        var newValue = new DemandFactorValueProxy(lastValue.Factor, lastValue.MaxRange);
        var newValueViewModel = new ValueViewModel(this, newValue);
        DemandFactor.Values.Add(newValue);
        Values.Add(newValueViewModel);
        SelectedValue = newValueViewModel;
    }

    private void AddMediumValue()
    {
        var mediumValue = (SelectedValue.MaxRange + SelectedValue.MinRange) / 2;
        mediumValue = ByQuantity ? mediumValue.Round() : mediumValue;
        var newValue = new DemandFactorValueProxy(SelectedValue.Factor, SelectedValue.MinRange, mediumValue);
        var newValueViewModel = new ValueViewModel(this, newValue);
        var index = Values.IndexOf(SelectedValue);
        DemandFactor.Values.Insert(index, newValue);
        Values.Insert(index, newValueViewModel);
        SelectedValue.MinRange = mediumValue;
        SelectedValue = newValueViewModel;
    }

    public Command RemoveValueCommand => new(() =>
    {
        if (!RemoveValue(SelectedValue, out var newIndex))
            return;
        
        SelectedValue = Values.ElementAtOrDefault(newIndex);
        Modify();
    });

    private bool RemoveValue(ValueViewModel valueViewModel, out int newIndex)
    {
        newIndex = default;
        
        if (valueViewModel == null)
            return false;

        var nextValue = valueViewModel.NextValue;
        var previousValue = valueViewModel.PreviousValue;
        var index = Values.IndexOf(valueViewModel);
        newIndex = valueViewModel.IsLast ? index - 1 : index;
        DemandFactor.Values.Remove(valueViewModel.DemandFactorValue);
        Values.Remove(valueViewModel);
        CanRemoveValue = Values.Count > 1;

        if (previousValue != null && nextValue != null)
            previousValue.MaxRange = nextValue.MinRange;
        
        UpdateFirstLastValues();
        
        return true;
    }

    private void UpdateFirstLastValues()
    {
        Values.ForEach(value => value.IsFirst = value.IsLast = false);
        Values.First().IsFirst = true;
        Values.First().MinRange = 0;
        Values.Last().IsLast = true;
        Values.Last().MaxRange = double.PositiveInfinity;
    }

    public override  void Modify()
    {
        if (_isInitialized)
            ChangedElements.Modify(DemandFactor.RevitId);
    }
}

public class ValueViewModel : ViewModel
{
    private double _maxRange;
    private double _minRange;
    
    public ValueViewModel(DemandFactorViewModel demandFactorViewModel, DemandFactorValueProxy demandFactorValue)
    {
        DemandFactorViewModel = demandFactorViewModel;
        DemandFactorValue = demandFactorValue;
        Factor = DemandFactorValue.Factor;
        MaxRange = _maxRange = DemandFactorValue.MaxRange;
        MinRange = _minRange = DemandFactorValue.MinRange;
    }

    public DemandFactorValueProxy DemandFactorValue { get; }
    public DemandFactorViewModel DemandFactorViewModel { get; }
    public bool IsFirst { get; set; }
    public bool IsLast { get; set; }
    public double Factor { get; set; }
    public double MaxRange { get; set; }
    public double MinRange { get; set; }
    internal ValueViewModel NextValue => DemandFactorViewModel.Values?.ElementAtOrDefault(DemandFactorViewModel.Values.IndexOf(this) + 1);
    internal ValueViewModel PreviousValue => DemandFactorViewModel.Values?.ElementAtOrDefault(DemandFactorViewModel.Values.IndexOf(this) - 1);
    
    private void OnFactorChanged()
    {
        DemandFactorValue.Factor = Factor;

        if (IsFirst)
            DemandFactorViewModel.ConstantValue = Factor;

        DemandFactorViewModel.Modify();
    }

    private void OnMaxRangeChanged()
    {
        var value = DemandFactorViewModel.ByQuantity ? MaxRange.Round() : MaxRange;
        
        if (value < MinRange + 1)
        {
            MaxRange = _maxRange;
            return;
        }
        
        if (NextValue != null)
        {
            if (value > NextValue.MaxRange - 1)
            {
                MaxRange = _maxRange;
                return;
            }
        
            NextValue.MinRange = value;
        }
        
        _maxRange = value;
        DemandFactorValue.MaxRange = value;
        DemandFactorViewModel.Modify();
    }

    private void OnMinRangeChanged()
    {
        var value = DemandFactorViewModel.ByQuantity ? MinRange.Round() : MinRange;
        
        if (value > MaxRange - 1)
        {
            MinRange = _minRange;
            return;
        }
        
        if (PreviousValue != null)
        {
            if (value < PreviousValue.MinRange + 1)
            {
                MinRange = _minRange;
                return;
            }
        
            PreviousValue.MaxRange = value;
        }
        
        _minRange = value;
        DemandFactorValue.MinRange = value;
        DemandFactorViewModel.Modify();
    }
}