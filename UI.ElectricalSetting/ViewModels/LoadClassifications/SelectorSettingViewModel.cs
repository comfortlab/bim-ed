using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class SelectorSettingViewModel : ViewModel
{
    private readonly bool _isInitialized;

    public SelectorSettingViewModel(LoadClassificationViewModel loadClassificationViewModel)
    {
        LoadClassificationViewModel = loadClassificationViewModel;
        CircuitBreakNeutral = LoadClassification.CircuitSelectAndConfigSetting.BreakNeutralCore;
        CircuitNeutralCore = LoadClassification.CircuitSelectAndConfigSetting.NeutralCore;
        PanelBreakNeutral = LoadClassification.PanelSelectAndConfigSetting.BreakNeutralCore;
        _isInitialized = true;
    }
    
    public LoadClassificationViewModel LoadClassificationViewModel { get; }
    public LoadClassificationProxy LoadClassification => LoadClassificationViewModel.LoadClassification;

    public bool CircuitBreakNeutral { get; set; }
    public bool CircuitNeutralCore { get; set; }
    public bool PanelBreakNeutral { get; set; }

    private void OnCircuitBreakNeutralChanged()
    {
        if (!_isInitialized) return;
        LoadClassification.CircuitSelectAndConfigSetting.BreakNeutralCore = CircuitBreakNeutral;
        Modify();
    }

    private void OnCircuitNeutralCoreChanged()
    {
        if (!_isInitialized) return;
        LoadClassification.CircuitSelectAndConfigSetting.NeutralCore = CircuitNeutralCore;
        Modify();
    }

    private void OnPanelBreakNeutralChanged()
    {
        if (!_isInitialized) return;
        LoadClassification.PanelSelectAndConfigSetting.BreakNeutralCore = PanelBreakNeutral;
        Modify();
    }

    private void Modify()
    {
        if (_isInitialized)
            LoadClassificationViewModel.Modify();
    }
}