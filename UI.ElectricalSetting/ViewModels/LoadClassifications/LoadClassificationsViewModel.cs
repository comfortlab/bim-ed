﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Math;
using CoLa.BimEd.Infrastructure.Framework.Prototype;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;
using CoLa.BimEd.UI.Resources.Electrical;
using CoLa.BimEd.UI.RtmDemandFactors.ViewModels;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class LoadClassificationsViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly Repository<LoadClassificationProxy> _loadClassifications;
    private readonly Repository<DemandFactorProxy> _demandFactors;
    private readonly VirtualElements _virtualElements;
    private readonly List<LoadClassificationProxy> _loadClassificationClones;
    private readonly List<DemandFactorProxy> _demandFactorClones;
    private ImportLoadClassificationsViewModel _importLoadClassificationsViewModel;
    private RtmDemandFactorsViewModel _rtmDemandFactorsViewModel;
    
    internal ChangedElements ChangedElements => _loadClassifications.ChangedElements;
    
    private string[] _forbiddenNames;

    public LoadClassificationsViewModel(
        IServiceProvider serviceProvider,
        Repository<LoadClassificationProxy> loadClassifications,
        Repository<DemandFactorProxy> demandFactors,
        VirtualElements virtualElements)
    {
        _serviceProvider = serviceProvider;
        _loadClassifications = loadClassifications;
        _demandFactors = demandFactors;
        _virtualElements = virtualElements;
        _loadClassificationClones = _loadClassifications.Clone();
        _demandFactorClones = _demandFactors.Clone();
        LoadClassifications = _loadClassificationClones.Select(x => new LoadClassificationViewModel(this, x)).ToObservableCollection();
        DemandFactors = _demandFactorClones.Select(x => new DemandFactorViewModel(this, x)).ToObservableCollection();
        SelectedLoadClassification = LoadClassifications.FirstOrDefault();
        UpdateCanBeDeletedDemandFactors();

        _virtualElements.Id = Comparer.Min(new [] { 0 },
            _demandFactorClones.Select(x => x.RevitId),
            _loadClassificationClones.Select(x => x.RevitId)) - 1;
    }
    
    public ObservableCollection<DemandFactorViewModel> DemandFactors { get; set; }
    public ObservableCollection<LoadClassificationViewModel> LoadClassifications { get; set; }
    public DemandFactorViewModel SelectedDemandFactor { get; set; }
    public LoadClassificationViewModel SelectedLoadClassification { get; set; }
    public ItemViewModel OperatingItemViewModel { get; set; }
    public ViewModel SecondaryDialogViewModel { get; set; }
    public string DialogTitle { get; set; }
    public string OldName { get; set; }
    public string NewName { get; set; }
    public bool IsMainContentEnabled { get; set; } = true;
    public bool CreateVisible { get; set; }
    public bool DeleteVisible { get; set; }
    public bool RenameVisible { get; set; }
    public bool NameOkEnabled { get; set; }
    public bool ImportSourcesVisible { get; set; }
    public string[] ImportSources { get; set; }
    public string SelectedImportSource { get; set; }

    protected override void OkAction()
    {
        _demandFactors.ReplaceAll(_demandFactorClones);
        _loadClassifications.ReplaceAll(_loadClassificationClones);
        _loadClassifications.Save();
        base.OkAction();
    }
    
    public Command CreateDemandFactorCommand => new(() =>
    {
        var newDemandFactor = new DemandFactorProxy { RevitId = _virtualElements.NewId() };
        CreateItem(new DemandFactorViewModel(this, newDemandFactor));
    });
    public Command CreateLoadClassificationCommand => new(() =>
    {
        var newLoadClassification = new LoadClassificationProxy { RevitId = _virtualElements.NewId() };
        CreateItem(new LoadClassificationViewModel(this, newLoadClassification));
    });
    public Command CreateOkCommand => new(() =>
    {
        var newName = NewName.Trim();
        OperatingItemViewModel.Name = newName;
        OperatingItemViewModel.Item.Name = newName;
        
        switch (OperatingItemViewModel.Item)
        {
            case DemandFactorProxy demandFactor:
                _demandFactorClones.Add(demandFactor);
                DemandFactors.Add(OperatingItemViewModel as DemandFactorViewModel);
                ChangedElements.Create(demandFactor.RevitId);
                break;
            
            case LoadClassificationProxy loadClassification:
                _loadClassificationClones.Add(loadClassification);
                loadClassification.DemandFactor = _demandFactorClones.OrderBy(x => x.RevitId).FirstOrDefault();
                LoadClassifications.Add(OperatingItemViewModel as LoadClassificationViewModel);
                ChangedElements.Create(loadClassification.RevitId);
                break;
        }
        
        UpdateItemSelection();
        UpdateCanBeAppliedDemandFactors();
        UpdateCanBeDeletedDemandFactors();
        CreateVisible = false;
    });
    public Command DeleteCommand => new(() => DeleteItem(OperatingItemViewModel));
    public Command DeleteOkCommand => new(() =>
    {
        switch (OperatingItemViewModel.Item)
        {
            case DemandFactorProxy demandFactor:
                DemandFactors.Remove(OperatingItemViewModel as DemandFactorViewModel);
                ChangedElements.Delete(demandFactor.RevitId);
                _demandFactorClones.Remove(demandFactor);
                break;
            
            case LoadClassificationProxy loadClassification:
                LoadClassifications.Remove(OperatingItemViewModel as LoadClassificationViewModel);
                ChangedElements.Delete(loadClassification.RevitId);
                _loadClassificationClones.Remove(loadClassification);
                break;
        }
        
        DeleteVisible = false;
    });

    public Command RenameCommand => new(() => RenameItem(OperatingItemViewModel));
    public Command RenameOkCommand => new(() =>
    {
        var newName = NewName.Trim();
        OperatingItemViewModel.Name = newName;
        OperatingItemViewModel.Item.Name = newName;
        OperatingItemViewModel.Modify();
        UpdateItemSelection();
        RenameVisible = false;
    });
    public Command ImportLoadClassificationCommand => new(() =>
    {
        ImportSources = new[]
        {
            ElectricalParameters.DemandFactorSource_Sp256,
            ElectricalParameters.DemandFactorSource_Rtm,
        };
        ImportSourcesVisible = true;
    });
    public Command ImportSourceOkCommand => new(() =>
    {
        ImportSourcesVisible = false;
        if (SelectedImportSource == ElectricalParameters.DemandFactorSource_Sp256)
        {
            _importLoadClassificationsViewModel ??= _serviceProvider.Get<ImportLoadClassificationsViewModel>();
            _importLoadClassificationsViewModel.OkCallback = ImportLoadClassification;
            SecondaryDialogViewModel = _importLoadClassificationsViewModel;
        }
        else if (SelectedImportSource == ElectricalParameters.DemandFactorSource_Rtm)
        {
            _rtmDemandFactorsViewModel ??= _serviceProvider.Get<RtmDemandFactorsViewModel>();
            _rtmDemandFactorsViewModel.OkCallback = ImportRtmLoadClassification;
            SecondaryDialogViewModel = _rtmDemandFactorsViewModel;
        }
        
        SecondaryDialogViewModel.Close = () => SecondaryDialogViewModel = null;
    });

    private void ImportLoadClassification(object loadClassification)
    {
        
    }

    private void ImportRtmLoadClassification(object demandFactorDefinition)
    {
        if (demandFactorDefinition is not ValueTuple<string, double>(var name, var demandFactorValue))
            return;

        var newDemandFactor = new DemandFactorProxy(
            _virtualElements.NewId(), name,
            DemandFactorRuleProxy.Constant, new DemandFactorValueProxy(demandFactorValue));
        var demandFactorViewModel = new DemandFactorViewModel(this, newDemandFactor);
        _demandFactorClones.Add(newDemandFactor);
        DemandFactors.Add(demandFactorViewModel);
        ChangedElements.Create(newDemandFactor.RevitId);

        var newLoadClassification = new LoadClassificationProxy(_virtualElements.NewId(), name, newDemandFactor);
        var loadClassificationViewModel = new LoadClassificationViewModel(this, newLoadClassification);
        _loadClassificationClones.Add(newLoadClassification);
        LoadClassifications.Add(loadClassificationViewModel);
        ChangedElements.Create(newLoadClassification.RevitId);

        SelectedLoadClassification = loadClassificationViewModel;
    }

    public Command ItemCancelCommand => new(() => CreateVisible = DeleteVisible = RenameVisible = ImportSourcesVisible = false);

    private void OnSelectedDemandFactorChanged()
    {
        OperatingItemViewModel = SelectedDemandFactor;
    }
    
    private void OnSelectedLoadClassificationChanged()
    {
        UpdateCanBeAppliedDemandFactors();
        SelectedDemandFactor = DemandFactors.FirstOrDefault(x => Equals(x.DemandFactor, SelectedLoadClassification?.LoadClassification.DemandFactor));
        OperatingItemViewModel = SelectedLoadClassification;
    }

    private void OnCreateVisibleChanged() => UpdateIsMainContentEnabled();
    private void OnDeleteVisibleChanged() => UpdateIsMainContentEnabled();
    private void OnRenameVisibleChanged() => UpdateIsMainContentEnabled();
    private void OnImportSourcesVisibleChanged() => UpdateIsMainContentEnabled();
    private void OnSecondaryDialogViewModelChanged() => UpdateIsMainContentEnabled();
    private void OnNewNameChanged() => NameOkEnabled = !_forbiddenNames?.Contains(NewName.Trim()) ?? true;

    private void UpdateIsMainContentEnabled() => IsMainContentEnabled =
        !CreateVisible &&
        !DeleteVisible &&
        !RenameVisible &&
        !ImportSourcesVisible &&
        SecondaryDialogViewModel == null;

    private void CreateItem(ItemViewModel itemViewModel)
    {
        if (itemViewModel == null)
            return;

        _forbiddenNames = GetForbiddenNames(itemViewModel.Item.GetType());
        OperatingItemViewModel = itemViewModel;
        DialogTitle = GetTitle(CommandNames.Create, OperatingItemViewModel.Item);
        NewName = GetNewName(OperatingItemViewModel.Item, _forbiddenNames);
        CreateVisible = true;
    }

    internal void CopyItem(ItemViewModel itemViewModel)
    {
        if (itemViewModel == null)
            return;

        switch (itemViewModel.Item)
        {
            case DemandFactorProxy demandFactor: CopyDemandFactor(demandFactor); break;
            case LoadClassificationProxy loadClassification: CopyLoadClassification(loadClassification); break;
        }

        UpdateItemSelection();
        UpdateCanBeAppliedDemandFactors();
        UpdateCanBeDeletedDemandFactors();
    }

    private void CopyDemandFactor(DemandFactorProxy demandFactor)
    {
        var clone = Clone(demandFactor);
        var cloneViewModel = new DemandFactorViewModel(this, clone);
        _demandFactorClones.Add(clone);
        DemandFactors.Add(cloneViewModel);
        ChangedElements.Create(clone.RevitId);
        OperatingItemViewModel = cloneViewModel;
    }

    private void CopyLoadClassification(LoadClassificationProxy loadClassification)
    {
        var clone = Clone(loadClassification);
        var cloneViewModel = new LoadClassificationViewModel(this, clone);
        _loadClassificationClones.Add(clone);
        LoadClassifications.Add(cloneViewModel);
        ChangedElements.Create(clone.RevitId);
        OperatingItemViewModel = cloneViewModel;
    }

    private T Clone<T>(T prototype) where T : ElementProxy, IPrototype<T>
    {
        var clone = prototype.Clone();
        clone.Name = NameUtils.GetNextName(prototype.Name, GetForbiddenNames(typeof(T)));
        clone.RevitId = _virtualElements.NewId();
        return clone;
    }

    internal void DeleteItem(ItemViewModel itemViewModel)
    {
        if (itemViewModel is null or DemandFactorViewModel { CanBeDeleted: false })
            return;
        
        OperatingItemViewModel = itemViewModel;
        DialogTitle = GetTitle(CommandNames.Delete, OperatingItemViewModel.Item);
        DeleteVisible = true;
    }
    
    internal void RenameItem(ItemViewModel itemViewModel)
    {
        if (itemViewModel == null)
            return;
        
        var renameItem = itemViewModel.Item;

        _forbiddenNames = GetForbiddenNames(renameItem.GetType(), exceptionNames: renameItem.Name);
        OperatingItemViewModel = itemViewModel;
        DialogTitle = GetTitle(CommandNames.Rename, renameItem);
        OldName = renameItem.Name;
        NewName = renameItem.Name;
        RenameVisible = true;
    }

    private static string GetTitle(string commandTitle, ElementProxy item)
    {
        return item switch
        {
            DemandFactorProxy => $"{commandTitle} {ElectricalParameters.DemandFactor.ToLower()}",
            LoadClassificationProxy => $"{commandTitle} {ElectricalParameters.LoadClassification.ToLower()}",
            _ => CommandNames.Rename,
        };
    }

    private static string GetNewName(ElementProxy item, params string[] otherNames)
    {
        var name = item switch
        {
            DemandFactorProxy => $"{ElectricalParameters.DemandFactor}",
            LoadClassificationProxy => $"{ElectricalParameters.LoadClassification}",
            _ => CommonDictionary.Element,
        };

        return NameUtils.GetNextName(name, otherNames);
    }

    private string[] GetForbiddenNames(Type type, params string[] exceptionNames)
    {
        if (type == typeof(DemandFactorProxy))
            return DemandFactors.Select(x => x.Name).Where(x => !exceptionNames.Contains(x)).ToArray();

        if (type == typeof(LoadClassificationProxy))
            return LoadClassifications.Select(x => x.Name).Where(x => !exceptionNames.Contains(x)).ToArray();
        
        return Array.Empty<string>();
    }

    private void UpdateItemSelection()
    {
        switch (OperatingItemViewModel.Item)
        {
            case DemandFactorProxy:
                SelectedDemandFactor = OperatingItemViewModel as DemandFactorViewModel;
                break;
            
            case LoadClassificationProxy:
                SelectedLoadClassification = OperatingItemViewModel as LoadClassificationViewModel;
                break;
        }
    }

    internal void UpdateCanBeAppliedDemandFactors()
    {
        foreach (var demandFactorViewModel in DemandFactors)
        {
            demandFactorViewModel.AnyLoadClassificationSelected = SelectedLoadClassification != null;
            demandFactorViewModel.CanBeApplied = SelectedLoadClassification != null && !Equals(demandFactorViewModel.Item, SelectedLoadClassification.LoadClassification.DemandFactor);
        }
    }

    internal void UpdateCanBeDeletedDemandFactors()
    {
        foreach (var demandFactorViewModel in DemandFactors)
        {
            demandFactorViewModel.CanBeDeleted = _loadClassificationClones.All(x =>
                !Equals(x.DemandFactor, demandFactorViewModel.Item));
        }
    }
}