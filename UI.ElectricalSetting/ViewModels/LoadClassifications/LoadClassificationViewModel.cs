﻿using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class LoadClassificationViewModel : ItemViewModel
{
    private readonly bool _isInitialized;

    private ChangedElements ChangedElements => LoadClassificationsViewModel.ChangedElements;
    public LoadClassificationViewModel(LoadClassificationsViewModel loadClassificationsViewModel, LoadClassificationProxy item) :
        base(loadClassificationsViewModel, item)
    {
        try
        {
            LoadClassification = item;
            SelectorSettingViewModel = new SelectorSettingViewModel(this);
        }
        finally
        {
            _isInitialized = true;
        }
    }
    
    public LoadClassificationProxy LoadClassification { get; }
    public SelectorSettingViewModel SelectorSettingViewModel { get; set; }

    public override void Modify()
    {
        if (_isInitialized)
            ChangedElements.Modify(LoadClassification.RevitId);
    }
}