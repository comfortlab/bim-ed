﻿using System;
using System.Windows.Media;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications
{
    public class SelectorViewModel : ViewModel
    {
        public Selector Selector { get; set; }
        public string Id { get; set; }
        public ImageSource Image { get; set; }

        public SelectorViewModel(string selectorId) :
            this(DI.Get<SeApi.Selectors>().GetById(selectorId))
        {
            
        }

        public SelectorViewModel(Selector selector)
        {
            Selector = selector ?? throw new ArgumentNullException(nameof(selector));
            Id = Selector.Id;
            Image = Selector.Image.ToBitmapSource();
        }
    }
}