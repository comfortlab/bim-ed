﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public abstract class ItemViewModel : ViewModel
{
    internal readonly ElementProxy Item;

    protected ItemViewModel(LoadClassificationsViewModel loadClassificationsViewModel, ElementProxy item)
    {
        Item = item;
        LoadClassificationsViewModel = loadClassificationsViewModel;
        Name = Item.Name;
    }

    public abstract void Modify();
    
    public LoadClassificationsViewModel LoadClassificationsViewModel { get; }
    public string Name { get; set; }
    public Command CopyCommand => new(() => LoadClassificationsViewModel.CopyItem(this));
    public Command DeleteCommand => new(() => LoadClassificationsViewModel.DeleteItem(this));
    public Command RenameCommand => new(() => LoadClassificationsViewModel.RenameItem(this));
}