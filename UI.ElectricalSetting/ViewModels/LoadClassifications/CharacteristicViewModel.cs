﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Comparer;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;

public class CharacteristicViewModel : ViewModel
{
    private readonly SelectorProductSettingViewModel _ownerViewModel;

    public CharacteristicViewModel(
        SelectorProductSettingViewModel selectorProductSettingViewModel,
        string name,
        IEnumerable<string> values)
    {
        _ownerViewModel = selectorProductSettingViewModel;
        Name = name;
        Values = values.OrderBy(n => n, new NumbersStringComparer()).ToList();
    }

    public string Name { get; set; }
    public List<string> Values { get; set; }
    public string SelectedValue { get; set; }

    private void OnSelectedValueChanged() => _ownerViewModel.UpdateCharacteristics();
}