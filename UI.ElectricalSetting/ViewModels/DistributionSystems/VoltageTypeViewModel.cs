using System.Linq;
using System.Windows;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;

public class VoltageTypeViewModel : ViewModel
{
    private ChangedElements ChangedElements => _distributionSystemsViewModel.ChangedElements;
    private readonly DistributionSystemsViewModel _distributionSystemsViewModel;
    private readonly bool _isInitialized;
    private bool _isChanging;

    public VoltageTypeViewModel(
        DistributionSystemsViewModel distributionSystemsViewModel,
        VoltageTypeProxy voltageType)
    {
        _distributionSystemsViewModel = distributionSystemsViewModel;
        VoltageType = voltageType;
        Name = VoltageType.Name;
        ActualVoltage = VoltageType.ActualValue;
        MinVoltage = VoltageType.MinValue;
        MaxVoltage = VoltageType.MaxValue;

        _isInitialized = true;
    }

    public VoltageTypeProxy VoltageType { get; set; }
    public string Name { get; set; }
    public double ActualVoltage { get; set; }
    public double MinVoltage { get; set; }
    public double MaxVoltage { get; set; }

    private void OnNameChanged()
    {
        if (!_isInitialized)
            return;
            
        if (string.IsNullOrWhiteSpace(Name))
        {
            Name = VoltageType.Name;
        }
        else if (IsNameExists())
        {
            MessageBox.Show(Messages.Instruction_NameExists);
            Name = VoltageType.Name;
        }
        else
        {
            VoltageType.Name = Name;
            Modify();
            UpdateDistributionSystems();
        }
    }

    private void UpdateDistributionSystems() => _distributionSystemsViewModel?.UpdateVoltageTypes(VoltageType);

    private bool IsNameExists()
    {
        return _distributionSystemsViewModel.VoltageTypeViewModels?
            .Where(n => !Equals(n.VoltageType, VoltageType))
            .Select(n => n.Name)
            .Contains(Name) ?? false;
    }

    private void OnActualVoltageChanged()
    {
        if (!_isInitialized || _isChanging)
            return;

        try
        {
            _isChanging = true;

            if (MinVoltage > ActualVoltage)
            {
                MinVoltage = ActualVoltage;
            }

            if (MaxVoltage < ActualVoltage)
            {
                MaxVoltage = ActualVoltage;
            }

            VoltageType.ActualValue = ActualVoltage;
            Modify();
        }
        finally
        {
            _isChanging = false;
            UpdateDistributionSystems();
        }
    }

    private void OnMinVoltageChanged()
    {
        if (MinVoltage > ActualVoltage)
            MinVoltage = ActualVoltage;

        VoltageType.MinValue = MinVoltage;
        UpdateDistributionSystems();

        Modify();
    }

    private void OnMaxVoltageChanged()
    {
        if (MaxVoltage < ActualVoltage)
            MaxVoltage = ActualVoltage;

        VoltageType.MaxValue = MaxVoltage;
        UpdateDistributionSystems();
        Modify();
    }

    private void Modify()
    {
        if (_isInitialized)
            ChangedElements.Modify(VoltageType.RevitId);
    }
}