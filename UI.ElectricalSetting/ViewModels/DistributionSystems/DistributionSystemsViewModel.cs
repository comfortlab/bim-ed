﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;

public class DistributionSystemsViewModel : ViewModel
{
    private readonly Repository<DistributionSystemProxy> _distributionSystems;
    private readonly Repository<VoltageTypeProxy> _voltageTypes;
    private readonly List<DistributionSystemProxy> _distributionSystemClones;
    private readonly List<VoltageTypeProxy> _voltageTypeClones;
    private readonly VirtualElements _virtualElements;
    internal ChangedElements ChangedElements => _distributionSystems.ChangedElements;

    public DistributionSystemsViewModel(
        Repository<DistributionSystemProxy> distributionSystems,
        Repository<VoltageTypeProxy> voltageTypes,
        VirtualElements virtualElements)
    {
        _distributionSystems = distributionSystems;
        _voltageTypes = voltageTypes;
        _virtualElements = virtualElements;
        _distributionSystemClones = _distributionSystems.Clone();
        _voltageTypeClones = _voltageTypes.Clone();

        VoltageTypeViewModels = _voltageTypeClones
            .Select(n => new VoltageTypeViewModel(this, n))
            .ToObservableCollection();

        DistributionSystemViewModels = _distributionSystemClones
            .Select(n => new DistributionSystemViewModel(this, n))
            .ToObservableCollection();
    }
    
    public ObservableCollection<DistributionSystemViewModel> DistributionSystemViewModels { get; set; }
    public DistributionSystemViewModel NewDistributionSystemViewModel { get; set; }
    public DistributionSystemViewModel SelectedDistributionSystem { get; set; }
    public ObservableCollection<VoltageTypeViewModel> VoltageTypeViewModels { get; set; }
    public VoltageTypeViewModel NewVoltageTypeViewModel { get; set; }
    public VoltageTypeViewModel SelectedVoltageType { get; set; }
    public bool IsMainContentEnabled { get; set; } = true;
    public bool CanDeleteDistributionSystem { get; set; }
    public bool CanDeleteVoltageType { get; set; }

    internal void OnSelectedDistributionSystemChanged()
    {
        CanDeleteDistributionSystem = SelectedDistributionSystem?.DistributionSystem.CanBeDeleted ?? false;
    }

    internal void OnSelectedVoltageTypeChanged()
    {
        CanDeleteVoltageType =
            SelectedVoltageType != null &&
            DistributionSystemViewModels.All(x =>
                !Equals(x.DistributionSystem.LineToGroundVoltage, SelectedVoltageType.VoltageType) &&
                !Equals(x.DistributionSystem.LineToLineVoltage, SelectedVoltageType.VoltageType));
    }

    private void OnNewDistributionSystemViewModelChanged() => UpdateIsMainContentEnabled();
    private void OnNewVoltageTypeViewModelChanged() => UpdateIsMainContentEnabled();

    private void UpdateIsMainContentEnabled()
    {
        IsMainContentEnabled = NewDistributionSystemViewModel == null && NewVoltageTypeViewModel == null;
    }

    internal void UpdateVoltageTypes(VoltageTypeProxy voltageType = null)
    {
        if (DistributionSystemViewModels == null)
            return;

        foreach (var distributionSystemViewModel in DistributionSystemViewModels)
            distributionSystemViewModel.UpdateVoltageTypes(voltageType);
    }

    protected override void OkAction()
    {
        _distributionSystems.ReplaceAll(_distributionSystemClones);
        _voltageTypes.ReplaceAll(_voltageTypeClones);
        _distributionSystems.Save();
        base.OkAction();
    }

    public Command NewDistributionSystemCommand => new(() =>
    {
        var newName = NameUtils.GetNextName($"{Properties.Resources.NewElectricalDistributionSystemName} ", DistributionSystemViewModels.Select(n => n.Name).ToArray());
        var newDistributionSystem = DistributionSystemProxy.Create1Phase(_virtualElements.NewId(), newName, null);
        NewDistributionSystemViewModel = new DistributionSystemViewModel(this, newDistributionSystem);
    });

    public Command NewVoltageTypeCommand => new(() =>
    {
        var newName = NameUtils.GetNextName($"{Properties.Resources.NewVoltageName} ", VoltageTypeViewModels.Select(n => n.Name).ToArray());
        var newVoltageType = new VoltageTypeProxy(_virtualElements.NewId(), newName);
        NewVoltageTypeViewModel = new VoltageTypeViewModel(this, newVoltageType);
    });

    public Command AddDistributionSystemCommand => new(() =>
    {
        var newDistributionSystem = NewDistributionSystemViewModel.DistributionSystem;
        _distributionSystemClones.Add(newDistributionSystem);
        DistributionSystemViewModels.Add(NewDistributionSystemViewModel);
        ChangedElements.Create(newDistributionSystem.RevitId);
        SelectedDistributionSystem = NewDistributionSystemViewModel;
        NewDistributionSystemViewModel = null;
    });

    public Command AddVoltageTypeCommand => new(() =>
    {
        var newVoltageType = NewVoltageTypeViewModel.VoltageType;
        _voltageTypeClones.Add(newVoltageType);
        VoltageTypeViewModels.Add(NewVoltageTypeViewModel);
        ChangedElements.Create(newVoltageType.RevitId);
        SelectedVoltageType = NewVoltageTypeViewModel;
        NewVoltageTypeViewModel = null;
        foreach (var distributionSystemViewModel in DistributionSystemViewModels)
        {
            distributionSystemViewModel.UpdateVoltageTypes();
        }
    });

    public Command DeleteDistributionSystemCommand => new(() =>
    {
        var distributionSystem = SelectedDistributionSystem.DistributionSystem;
        _distributionSystemClones.Remove(distributionSystem);
        DistributionSystemViewModels.Remove(SelectedDistributionSystem);
        ChangedElements.Delete(distributionSystem.RevitId);
    });

    public Command DeleteVoltageTypeCommand => new(() =>
    {
        var voltageType = SelectedVoltageType.VoltageType;
        _voltageTypeClones.Remove(voltageType);
        VoltageTypeViewModels.Remove(SelectedVoltageType);
        ChangedElements.Delete(voltageType.RevitId);
        UpdateVoltageTypes();
    });

    public Command CancelDistributionSystemCommand => new(() => NewDistributionSystemViewModel = null);
    public Command CancelVoltageTypeCommand => new(() => NewVoltageTypeViewModel = null);

    public Command Create230Command => new(() =>
    {
        Create1PhDefaultDistributionSystem(
            lineToGroundName: Properties.Resources.AC230V, lineToGroundActualValue: 230);
    });

    public Command Create400Command => new(() =>
    {
        Create3PhDefaultDistributionSystem(
            lineToLineName: Properties.Resources.AC400V, lineToLineActualValue: 400,
            lineToGroundName: Properties.Resources.AC230V, lineToGroundActualValue: 230);
    });

    public Command Create690Command => new(() =>
    {
        Create3PhDefaultDistributionSystem(
            lineToLineName: Properties.Resources.AC690V, lineToLineActualValue: 690,
            lineToGroundName: Properties.Resources.AC400V, lineToGroundActualValue: 400);
    });

    public Command Create6000Command => new(() =>
    {
        Create3PhDefaultDistributionSystem(
            lineToLineName: Properties.Resources.AC6kV, lineToLineActualValue: 6000);
    });

    public Command Create10000Command => new(() =>
    {
        Create3PhDefaultDistributionSystem(
            lineToLineName: Properties.Resources.AC10kV, lineToLineActualValue: 10000);
    });

    public Command Create20000Command => new(() =>
    {
        Create3PhDefaultDistributionSystem(
            lineToLineName: Properties.Resources.AC20kV, lineToLineActualValue: 20000);
    });

    private void Create1PhDefaultDistributionSystem(string lineToGroundName, double lineToGroundActualValue)
    {
        var lineToGroundVoltageTypeViewModel = CreateVoltageType(lineToGroundName, lineToGroundActualValue);

        foreach (var viewModel in DistributionSystemViewModels)
        {
            viewModel.UpdateVoltageTypes();
        }

        var newDistributionSystemName = NameUtils.GetNextName(lineToGroundName, DistributionSystemViewModels.Select(n => n.Name).ToArray());
        var distributionSystem = DistributionSystemProxy.Create1Phase(_virtualElements.NewId(), newDistributionSystemName, lineToGroundVoltageTypeViewModel.VoltageType);
        var distributionSystemViewModel = new DistributionSystemViewModel(this, distributionSystem)
        {
            SelectedLineToGroundVoltage = lineToGroundVoltageTypeViewModel.VoltageType
        };

        _distributionSystemClones.Add(distributionSystem);
        DistributionSystemViewModels.Add(distributionSystemViewModel);
        ChangedElements.Create(distributionSystem.RevitId);
    }

    private void Create3PhDefaultDistributionSystem(
        string lineToLineName, double lineToLineActualValue,
        string lineToGroundName = null, double? lineToGroundActualValue = null)
    {
        var lineToGroundVoltageTypeViewModel =
            !string.IsNullOrWhiteSpace(lineToGroundName) && lineToGroundActualValue != null
                ? CreateVoltageType(lineToGroundName, (double)lineToGroundActualValue)
                : null;

        var lineToLineVoltageTypeViewModel = CreateVoltageType(lineToLineName, lineToLineActualValue);

        foreach (var viewModel in DistributionSystemViewModels)
        {
            viewModel.UpdateVoltageTypes();
        }

        var newDistributionSystemName = NameUtils.GetNextName(lineToLineName, DistributionSystemViewModels.Select(n => n.Name).ToArray());
        var distributionSystem = DistributionSystemProxy.Create3PhasesWye(_virtualElements.NewId(), newDistributionSystemName, lineToLineVoltageTypeViewModel.VoltageType, lineToGroundVoltageTypeViewModel?.VoltageType);
        var distributionSystemViewModel = new DistributionSystemViewModel(this, distributionSystem)
        {
            SelectedLineToGroundVoltage = lineToGroundVoltageTypeViewModel?.VoltageType,
            SelectedLineToLineVoltage = lineToLineVoltageTypeViewModel.VoltageType,
        };

        _distributionSystemClones.Add(distributionSystem);
        DistributionSystemViewModels.Add(distributionSystemViewModel);
        ChangedElements.Create(distributionSystem.RevitId);
    }

    private VoltageTypeViewModel CreateVoltageType(string lineToGroundName, double lineToGroundActualValue)
    {
        var voltageTypeViewModel = VoltageTypeViewModels.FirstOrDefault(n =>
            n.Name == lineToGroundName && n.ActualVoltage.IsEqualTo(lineToGroundActualValue));

        if (voltageTypeViewModel != null)
            return voltageTypeViewModel;
        
        var newVoltageTypeName = NameUtils.GetNextName(lineToGroundName, VoltageTypeViewModels.Select(n => n.Name).ToArray());
        var voltageType = new VoltageTypeProxy(_virtualElements.NewId(), newVoltageTypeName, lineToGroundActualValue, 0, lineToGroundActualValue);
        voltageTypeViewModel = new VoltageTypeViewModel(this, voltageType);
        _voltageTypeClones.Add(voltageType);
        VoltageTypeViewModels.Add(voltageTypeViewModel);
        ChangedElements.Create(voltageType.RevitId);

        return voltageTypeViewModel;
    }
}