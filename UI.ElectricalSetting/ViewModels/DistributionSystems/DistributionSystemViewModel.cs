﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;

public class DistributionSystemViewModel : ViewModel
{
    private ChangedElements ChangedElements => _distributionSystemsViewModel.ChangedElements;
    private readonly DistributionSystemsViewModel _distributionSystemsViewModel;
    private readonly IReadOnlyCollection<VoltageTypeViewModel> _voltageTypeViewModels;
    private readonly bool _isInitialized;

    public DistributionSystemViewModel(
        DistributionSystemsViewModel distributionSystemsViewModel,
        DistributionSystemProxy distributionSystem)
    {
        _distributionSystemsViewModel = distributionSystemsViewModel;
        _voltageTypeViewModels = _distributionSystemsViewModel.VoltageTypeViewModels;
            
        DistributionSystem = distributionSystem;
        Name = DistributionSystem.Name;
        SelectedPhasesNumber = DistributionSystem.PhasesNumber;
        SelectedConfiguration = DistributionSystem.ElectricalPhaseConfiguration;
        SelectedWiresNumber = DistributionSystem.NumWires;
            
        UpdateVoltageTypes();

        SelectedLineToLineVoltage = _voltageTypeViewModels.FirstOrDefault(viewModel => viewModel.VoltageType.RevitId == DistributionSystem.LineToLineVoltage?.RevitId)?.VoltageType;
        SelectedLineToGroundVoltage = _voltageTypeViewModels.FirstOrDefault(viewModel => viewModel.VoltageType.RevitId == DistributionSystem.LineToGroundVoltage?.RevitId)?.VoltageType;
            
        _isInitialized = true;
    }

    internal void UpdateVoltageTypes(VoltageTypeProxy newVoltageType = null)
    {
        VoltageTypes = _voltageTypeViewModels.Select(n => n.VoltageType).ToList();
        
        if (newVoltageType == null)
            return;

        if (Equals(newVoltageType, SelectedLineToGroundVoltage))
        {
            SelectedLineToGroundVoltage = null;
            SelectedLineToGroundVoltage = newVoltageType;
        }

        if (Equals(newVoltageType, SelectedLineToLineVoltage))
        {
            SelectedLineToLineVoltage = null;
            SelectedLineToLineVoltage = newVoltageType;
        }
    }

    public DistributionSystemProxy DistributionSystem { get; set; }
    public string Name { get; set; }
    public List<PhasesNumber> Phases { get; } = new() { PhasesNumber.One, PhasesNumber.Three };
    public List<ElectricalPhaseConfigurationProxy> Configurations { get; set; }
    public ElectricalPhaseConfigurationProxy SelectedConfiguration { get; set; }
    public PhasesNumber SelectedPhasesNumber { get; set; }
    public bool IsThreePhases { get; set; }
    public List<int> WiresNumbers { get; set; }
    public int SelectedWiresNumber { get; set; }
    public List<VoltageTypeProxy> VoltageTypes { get; set; }
    public VoltageTypeProxy SelectedLineToLineVoltage { get; set; }
    public VoltageTypeProxy SelectedLineToGroundVoltage { get; set; }

    private void OnNameChanged()
    {
        if (!_isInitialized)
            return;
            
        if (string.IsNullOrWhiteSpace(Name))
        {
            Name = DistributionSystem.Name;
        }
        else if (IsNameExists())
        {
            MessageBox.Show(Messages.Instruction_NameExists);
            Name = DistributionSystem.Name;
        }
        else
        {
            DistributionSystem.Name = Name;
            Modify();
        }
    }

    private bool IsNameExists()
    {
        return _distributionSystemsViewModel.DistributionSystemViewModels?
            .Where(n => !Equals(n.DistributionSystem, DistributionSystem))
            .Select(n => n.Name)
            .Contains(Name) ?? false;
    }

    private void OnSelectedPhasesNumberChanged()
    {
        DistributionSystem.PhasesNumber = SelectedPhasesNumber;
        
        Modify();
        
        switch (SelectedPhasesNumber)
        {
            case PhasesNumber.One:
                IsThreePhases = false;
                Configurations = new List<ElectricalPhaseConfigurationProxy>
                {
                    ElectricalPhaseConfigurationProxy.Undefined,
                };
                WiresNumbers = new List<int>
                {
                    2,
                    3,
                };
                IsThreePhases = SelectedWiresNumber == 3;
                break;

            case PhasesNumber.Three:
                IsThreePhases = true;
                Configurations = new List<ElectricalPhaseConfigurationProxy>
                {
                    ElectricalPhaseConfigurationProxy.Wye,
                    ElectricalPhaseConfigurationProxy.Delta,
                };
                WiresNumbers = new List<int>
                {
                    3,
                    4,
                };
                break;
        }
    }

    private void OnConfigurationsChanged()
    {
        if (Configurations.Contains(DistributionSystem.ElectricalPhaseConfiguration) == false)
        {
            DistributionSystem.ElectricalPhaseConfiguration = Configurations.FirstOrDefault();
            Modify();
        }

        SelectedConfiguration = DistributionSystem.ElectricalPhaseConfiguration;
    }

    private void OnSelectedConfigurationChanged()
    {
        DistributionSystem.ElectricalPhaseConfiguration = SelectedConfiguration;
        Modify();
    }

    private void OnWiresNumbersChanged()
    {
        if (WiresNumbers.Contains(DistributionSystem.NumWires) == false)
        {
            DistributionSystem.NumWires = WiresNumbers.FirstOrDefault();
            Modify();
        }

        SelectedWiresNumber = DistributionSystem.NumWires;
    }

    private void OnSelectedWiresNumberChanged()
    {
        DistributionSystem.NumWires = SelectedWiresNumber;
        IsThreePhases = SelectedPhasesNumber == PhasesNumber.Three || SelectedWiresNumber == 3;
        Modify();
    }

    private void OnSelectedLineToLineVoltageChanged()
    {
        DistributionSystem.LineToLineVoltage = SelectedLineToLineVoltage;
        _distributionSystemsViewModel.OnSelectedDistributionSystemChanged();
        Modify();
    }

    private void OnSelectedLineToGroundVoltageChanged()
    {
        DistributionSystem.LineToGroundVoltage = SelectedLineToGroundVoltage;
        _distributionSystemsViewModel.OnSelectedDistributionSystemChanged();
        Modify();
    }

    private void Modify()
    {
        if (_isInitialized)
            ChangedElements.Modify(DistributionSystem.RevitId);
    }
}