﻿using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalSetting.Windows;

public partial class LoadClassificationsWindow
{
    public LoadClassificationsWindow(LoadClassificationsViewModel viewModel)
    {
        InitializeComponent();
        this.SetPositionAndSizes(heightScaleFactor: 0.9, widthScaleFactor: 0.8);
        viewModel.Close = Close;
        DataContext = viewModel;
    }
}