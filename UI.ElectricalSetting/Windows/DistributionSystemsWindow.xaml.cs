﻿using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalSetting.Windows;

public partial class DistributionSystemsWindow
{
    public DistributionSystemsWindow(DistributionSystemsViewModel viewModel)
    {
        InitializeComponent();
        this.SetPositionAndSizes(heightScaleFactor: 0.9, widthScaleFactor: 0.8);
        viewModel.Close = Close;
        DataContext = viewModel;
    }
}