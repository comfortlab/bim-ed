﻿using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.UI.Presentation.CommandPrompt
{
    public class CommandPromptViewModel : ViewModel
    {
        private readonly DebugCommandPrompt _debugCommandPrompt;

        public CommandPromptViewModel(DebugCommandPrompt debugCommandPrompt, Logger logger)
        {
            _debugCommandPrompt = debugCommandPrompt;
            _debugCommandPrompt.LogAction = Log;

            logger.OnLog += (_, args) =>
            {
                var message = _debugCommandPrompt.ShowTimeInLog
                    ? args.TimeLevelMessage
                    : args.LevelMessage;
                
                Log(message);
            };
        }

        public void Log(string text) => LogText += $"{text}\n";
        public string LogText { get; set; }
        public string CommandText { get; set; }

        public Command ExecuteCommand => new(() =>
        {
            var command = CommandText.ToLower();

            if (command.StartsWith("[") &&
                command.EndsWith("]") &&
                int.TryParse(command.Trim('[', ']'), out var revitId))
            {
                CommandText = string.Empty;
                var obj = _debugCommandPrompt.GetObjectFunc?.Invoke(revitId);
                Log($"{obj.AllValuesToString()}");
            }
            else
            {
                switch (command)
                {
                    case "clear":
                    case "clr":
                        CommandText = string.Empty;
                        LogText = string.Empty;
                        return;

                    default:
                        Log(_debugCommandPrompt.ExecuteCommand(CommandText));
                        ResetCommand?.Execute(null);
                        return;
                }
            }
        });

        public Command SetPreviousCommand => new(() => CommandText = _debugCommandPrompt.GetPreviousCommand().Command);
        public Command SetNextCommand => new(() => CommandText = _debugCommandPrompt.GetNextCommand().Command);
        public Command ResetCommand => new(() => CommandText = string.Empty);
        public Command ResetLogCommand => new(() => LogText = string.Empty);
    }
}