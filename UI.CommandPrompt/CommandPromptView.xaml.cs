﻿using System.Windows.Input;
using CoLa.BimEd.Infrastructure.Framework.Logs;

namespace CoLa.UI.Presentation.CommandPrompt
{
    public partial class CommandPromptView
    {
        public CommandPromptView(DebugCommandPrompt debugCommandPrompt, Logger logger)
        {
            InitializeComponent();
            DataContext = new CommandPromptViewModel(debugCommandPrompt, logger);
        }

        private void Result_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                return;

            CommandLine.Focus();
        }
    }
}