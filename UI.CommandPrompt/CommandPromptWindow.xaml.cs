﻿using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.UI.Presentation.CommandPrompt;

public partial class CommandPromptWindow
{
    public CommandPromptWindow(DebugCommandPrompt debugCommandPrompt, Logger logger)
    {
        InitializeComponent();
        Content = new CommandPromptView(debugCommandPrompt, logger);
        this.SetPositionAndSizes(0.9, 0.7);
    }
}