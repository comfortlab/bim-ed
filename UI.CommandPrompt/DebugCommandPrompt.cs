﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Comparer;

namespace CoLa.UI.Presentation.CommandPrompt;

public class DebugCommandPrompt
{
    private List<DebugCommand> _allCommands;
    private int _commandIndex;
        
    public DebugCommandPrompt()
    {
        _allCommands = new List<DebugCommand>
        {
            new("?", () => $"All Commands:{_allCommands.JoinToString("\n\n")}", description: "Show all commands"),

            new("clr", () => string.Empty, description: "Clear screen"),
        };

        ShowTimeInLog = true;
    }

    public Func<int, object> GetObjectFunc { get; set; }
    internal Action<string> LogAction { get; set; }
    public bool ShowTimeInLog { get; set; }

    public void Log(string text) => LogAction?.Invoke(text);

    public void Register(string command, Func<string> commandFunc, string description)
    {
        Register(new DebugCommand(command, commandFunc, description));
    }

    public void Register(DebugCommand debugCommand)
    {
        _allCommands.Add(debugCommand);
        _allCommands = _allCommands.OrderBy(n => n.Command.First(), new NumbersStringComparer()).ToList();
    }

    public string ExecuteCommand(string inputText)
    {
        var input = inputText.Split(' ');

        if (input.Length < 1)
            return string.Empty;

        var commandText = input[0].ToLower();
        var command = _allCommands.FirstOrDefault(n =>
            n.Command == commandText &&
            n.ParametersCount == input.Length - 1);

        var arg = input.Length > 1 ? input[1] : null;

        var result =
            $"{new string('=', 20)}\n" +
            $"# {DateTime.Now} > {inputText}\n" +
            $"{new string('=', 20)}\n";
            
        if (command != null)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            result += arg != null
                ? command.Execute(arg)
                : command.Execute();
                
            stopwatch.Stop();

            result += $"\n\n{new string('-', 28)}\n" +
                      $"Executing Time: {stopwatch.ElapsedMilliseconds} ms ({stopwatch.ElapsedTicks})\n";
        }

        else
        {
            result += "Unknown command\n";
        }

        return result;
    }

    public DebugCommand GetPreviousCommand()
    {
        var command = _allCommands.ElementAt(_commandIndex);
        if (_commandIndex > 0) _commandIndex--;

        return command;
    }

    public DebugCommand GetNextCommand()
    {
        var command = _allCommands.ElementAt(_commandIndex);
        if (_commandIndex < _allCommands.Count - 1) _commandIndex++;

        return command;
    }
}