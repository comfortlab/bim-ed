using System;

namespace CoLa.UI.Presentation.CommandPrompt;

public class DebugCommand
{
    public DebugCommand(string command, Func<string> commandFunc, string description)
    {
        Command = command;
        Description = description;
        CommandFunc = commandFunc;
        ParametersCount = 0;
    }

    public DebugCommand(string command, Func<string, string> parameterCommandFunc, string description)
    {
        Command = command;
        Description = description;
        ParameterCommandFunc = parameterCommandFunc;
        ParametersCount = 1;
    }

    public string Command { get; }
    public string Description { get; }
    public Func<string> CommandFunc { get; }
    public Func<string, string> ParameterCommandFunc { get; }
    public int ParametersCount { get; }
    public string Execute() => CommandFunc?.Invoke();
    public string Execute(string arg) => ParameterCommandFunc?.Invoke(arg);
    public override string ToString() => $"{string.Join(", ", Command)}\n\t{Description}";
}