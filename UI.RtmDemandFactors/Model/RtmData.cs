﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.UI.RtmDemandFactors.Model;

public record RtmData
{
    public List<RtmDemandFactorGroup> Groups { get; set; } = new();
    public List<RtmDemandFactor> DemandFactors { get; set; } = new();
}

public record RtmDemandFactor(int GroupId, string Name)
{
    public string Name { get; } = Name;
    public double MinDemandFactor { get; set; }
    public double MaxDemandFactor { get; set; }
    public double? MinPowerFactor { get; set; }
    public double? MaxPowerFactor { get; set; }
    public int GroupId { get; } = GroupId;
    
    [JsonIgnore]
    public RtmDemandFactorGroup Group { get; set; }

    public virtual bool Equals(RtmDemandFactor other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Name == other.Name && GroupId == other.GroupId;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ GroupId;
        }
    }

    public override string ToString() => Name;
}

public record RtmDemandFactorGroup(int Id, int Level, string Number, string Name)
{
    public int Id { get; } = Id;
    public int Level { get; set; } = Level;
    public string Number { get; set; } = Number;
    public string Name { get; set; } = Name;
    public int? ParentGroupId { get; set; }
    
    [JsonIgnore]
    public RtmDemandFactorGroup ParentGroup { get; set; }
    
    [JsonIgnore]
    public List<RtmDemandFactor> DemandFactors { get; } = new();
    
    [JsonIgnore]
    public List<RtmDemandFactorGroup> Groups { get; } = new();

    public virtual bool Equals(RtmDemandFactorGroup other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Id == other.Id;
    }
    public override int GetHashCode() => Id;
    public override string ToString() => !string.IsNullOrWhiteSpace(Number) ? $"{Number} {Name}" : Name;
}