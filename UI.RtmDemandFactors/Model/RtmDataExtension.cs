﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.UI.RtmDemandFactors.Model;

public static class RtmDataExtension
{
    public static IEnumerable<RtmDemandFactorGroup> GetAllGroups(this RtmDemandFactor demandFactor)
    {
        return new [] { demandFactor.Group }.Concat(demandFactor.Group.GetAllGroups());
    }
    
    public static IEnumerable<RtmDemandFactorGroup> GetAllGroups(this RtmDemandFactorGroup demandFactorGroup)
    {
        var parents = new List<RtmDemandFactorGroup>();
        var parent = demandFactorGroup.ParentGroup;
        
        while (parent != null)
        {
            parents.Add(parent);
            parent = parent.ParentGroup;
        }
        
        return parents;
    }
    
    public static IEnumerable<RtmDemandFactor> GetAllDemandFactors(this RtmDemandFactorGroup demandFactorGroup)
    {
        var demandFactors = new List<RtmDemandFactor>();
        var childGroups = demandFactorGroup.Groups.ToList();

        while (childGroups.Any())
        {
            var childGroup = childGroups[0];
            childGroups.RemoveAt(0);
            childGroups.AddRange(childGroup.Groups);
            demandFactors.AddRange(childGroup.DemandFactors);
        }
        
        demandFactors.AddRange(demandFactorGroup.DemandFactors);
        
        return demandFactors;
    }

    public static string DemandFactorAsString(this RtmDemandFactor demandFactor)
    {
        return !demandFactor.MinDemandFactor.IsEqualTo(demandFactor.MaxDemandFactor)
            ? $"{demandFactor.MinDemandFactor.ToStringInvariant()}...{demandFactor.MaxDemandFactor.ToStringInvariant()}"
            : demandFactor.MinDemandFactor.ToStringInvariant();
    }

    public static string PowerFactorAsString(this RtmDemandFactor demandFactor)
    {
        return !(demandFactor.MinPowerFactor?.IsEqualTo(demandFactor.MaxPowerFactor ?? 0) ?? true)
            ? $"{demandFactor.MinPowerFactor?.ToStringInvariant()}...{demandFactor.MaxPowerFactor?.ToStringInvariant()}"
            : demandFactor.MinPowerFactor?.ToStringInvariant();
    }
}