﻿using System;
using System.Linq;

namespace CoLa.BimEd.UI.RtmDemandFactors.Model;

public class RtmDemandFactorsFinder
{
    public readonly RtmDemandFactorGroup[] Groups;
    public readonly RtmDemandFactor[] DemandFactors;
    public RtmDemandFactorGroup[] GroupsWithSearchText { get; private set; }
    public RtmDemandFactorGroup[] FilteredGroups { get; private set; }
    public RtmDemandFactor[] DemandFactorsWithSearchText { get; private set; }
    public RtmDemandFactor[] FilteredDemandFactors { get; private set; }

    public string SearchText { get; private set; }
    public bool HasFilter => !string.IsNullOrEmpty(SearchText);
    public bool HasResults => FilteredDemandFactors.Length > 0;

    public RtmDemandFactorsFinder(RtmData rtmData)
    {   
        Groups = rtmData?.Groups.ToArray();
        DemandFactors = rtmData?.DemandFactors.ToArray();
        ResetFilter();
    }

    public void FilterBy(string searchText)
    {
        if (string.IsNullOrEmpty(searchText))
        {
            ResetFilter();
            return;
        }

        var isSearchContinue = !string.IsNullOrWhiteSpace(SearchText) && searchText.StartsWith(SearchText); 
        var groups = isSearchContinue ? GroupsWithSearchText : Groups;
        var demandFactors = isSearchContinue ? DemandFactorsWithSearchText : DemandFactors;

        SearchText = searchText.ToLower();
        var searchParts = SearchText.Split(' ', ',', ';', '/', '\\').ToArray();

        GroupsWithSearchText = groups
            .Where(group => searchParts.All(x => group.ToString().ToLower().Contains(x)))
            .ToArray();

        DemandFactorsWithSearchText = demandFactors
            .Where(demandFactor => searchParts.All(x => demandFactor.Name.ToLower().Contains(x)))
            .ToArray();

        FilteredDemandFactors = GroupsWithSearchText
            .SelectMany(group => group.GetAllDemandFactors())
            .Concat(DemandFactorsWithSearchText)
            .ToArray();

        FilteredGroups = FilteredDemandFactors
            .SelectMany(demandFactor => demandFactor.GetAllGroups())
            .Distinct()
            .ToArray();
    }

    public void ResetFilter()
    {
        SearchText = null;
        GroupsWithSearchText = Array.Empty<RtmDemandFactorGroup>();
        DemandFactorsWithSearchText = Array.Empty<RtmDemandFactor>();
        FilteredGroups = Groups;
        FilteredDemandFactors = DemandFactors;
    }
}