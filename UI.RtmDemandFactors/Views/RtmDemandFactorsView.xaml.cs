﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.RtmDemandFactors.Views;

public partial class RtmDemandFactorsView
{
    public RtmDemandFactorsView() =>
        InitializeComponent();
    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.MultiScrollMouseWheel(sender, e);
}