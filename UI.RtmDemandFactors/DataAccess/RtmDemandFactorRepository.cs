﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.RtmDemandFactors.Model;
using CoLa.BimEd.UI.RtmDemandFactors.Resources;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace CoLa.BimEd.UI.RtmDemandFactors.DataAccess;

public sealed class RtmDemandFactorRepository : FileRepository<RtmData>
{
    public RtmDemandFactorRepository(AppPaths appPaths) :
        base(new RtmDemandFactorRepositoryInitializer(appPaths), appPaths)
    {
        Load();
    }

    protected override void Read(string path = null)
    {
        base.Read(path);
        RestoreRtmData();
    }
    
    private void RestoreRtmData()
    {
        var rtmData = this.First();

        foreach (var group in rtmData.Groups)
        {
            group.ParentGroup = rtmData.Groups.FirstOrDefault(x => x.Id == group.ParentGroupId);
            group.ParentGroup?.Groups.Add(group);
        }

        foreach (var demandFactor in rtmData.DemandFactors)
        {
            demandFactor.Group = rtmData.Groups.FirstOrDefault(x => x.Id == demandFactor.GroupId);
            demandFactor.Group?.DemandFactors.Add(demandFactor);
        }
    }

    private class RtmDemandFactorRepositoryInitializer : IRepositoryInitializer<RtmData>
    {
        private const string RtmDemandFactors = "RtmDemandFactors.json";
        private const string RtmDemandFactorsResource = "RtmDemandFactors.xlsx";
        // private const string RtmDemandFactors = "RtmDemandFactors.xlsx";
        private readonly AppPaths _appPaths;
        public RtmDemandFactorRepositoryInitializer(AppPaths appPaths) => _appPaths = appPaths;
        public List<RtmData> Initialize() => new() { CreateRtmData() };
        
        private RtmData CreateRtmData()
        {
            var resourcePath = System.IO.Path.Combine(_appPaths.TemporaryFiles, RtmDemandFactorsResource);
            
            CreateIfNotExist(resourcePath);

            var data = new RtmData();
            var workbook = new XSSFWorkbook(resourcePath);
            var sheet = workbook.GetSheet("Data");
            var groupId = 0;
            
            RtmDemandFactorGroup lastGroup = null;
            RtmDemandFactorGroup lastGroupLevel1 = null;
            RtmDemandFactorGroup lastGroupLevel2 = null;
            RtmDemandFactorGroup lastGroupLevel3 = null;

            for (var i = 1; i <= sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);
                var cell = row.GetCell(0);
                var value = cell.StringCellValue;

                if (string.IsNullOrWhiteSpace(value))
                    continue;

                var style = cell.CellStyle;
                var fontSize = (int)style.GetFont(workbook).FontHeightInPoints;

                switch (fontSize)
                {
                    case 15:
                        lastGroup = lastGroupLevel1 = GetGroup(++groupId, 1, value);
                        data.Groups.Add(lastGroup);
                        break;
                    case 13:
                        lastGroup = lastGroupLevel2 = GetGroup(++groupId, 2, value, lastGroupLevel1);
                        data.Groups.Add(lastGroup);
                        break;
                    case 12:
                        lastGroup = lastGroupLevel3 = GetGroup(++groupId, 3, value, lastGroupLevel2);
                        data.Groups.Add(lastGroup);
                        break;
                    case 11:
                        lastGroup = GetGroup(++groupId, 4, value, lastGroupLevel3);
                        data.Groups.Add(lastGroup);
                        break;
                    default:
                        var demandFactor = GetDemandFactor(value, lastGroup);
                        SetProperties(demandFactor, row.GetCell(1), row.GetCell(2));
                        data.DemandFactors.Add(demandFactor);
                        break;
                }
            }
            
            workbook.Close();
            
            DeleteFile(resourcePath);

            return data;
        }

        private static RtmDemandFactorGroup GetGroup(
            int id, int level, string value,
            RtmDemandFactorGroup parentGroup = null)
        {
            GetGroupDefinition(value, out var number, out var name);
            
            var group = new RtmDemandFactorGroup(id, level, !string.IsNullOrWhiteSpace(number) ? number : null, name)
            {
                ParentGroup = parentGroup,
                ParentGroupId = parentGroup?.Id ?? -1,
            };
            
            parentGroup?.Groups.Add(group);
            
            return group;
        }

        private static void GetGroupDefinition(string value, out string number, out string name)
        {
            int i;
            var items = new List<int>();
            var split = value.Split('.');
            
            for (i = 0; i < split.Length; i++)
            {
                if (!int.TryParse(split[i].Trim(), out var item))
                    break;

                items.Add(item);
            }
            
            name = string.Join("", split.Skip(i)).Trim();
            number = string.Join(".", items);
        }

        private static RtmDemandFactor GetDemandFactor(string name, RtmDemandFactorGroup group)
        {
            var demandFactor = new RtmDemandFactor(group?.Id ?? -1, name)
            {
                Group = group,
            };
            
            group?.DemandFactors.Add(demandFactor);

            return demandFactor;
        }

        private static void SetProperties(RtmDemandFactor demandFactor, ICell demandFactorCell, ICell powerFactorCell)
        {
            if (TryGetFactors(demandFactorCell, out var minDemandFactor, out var maxDemandFactor))
            {
                demandFactor.MinDemandFactor = minDemandFactor;
                demandFactor.MaxDemandFactor = maxDemandFactor;
            }
            
            if (TryGetFactors(powerFactorCell, out var minPowerFactor, out var maxPowerFactor))
            {
                demandFactor.MinPowerFactor = minPowerFactor;
                demandFactor.MaxPowerFactor = maxPowerFactor;
            }
        }

        private static bool TryGetFactors(ICell cell, out double minValue, out double maxValue)
        {
            switch (cell?.CellType)
            {
                case CellType.String:
                    return TryGetFactors(cell.StringCellValue, out minValue, out maxValue);

                case CellType.Numeric:
                    minValue = maxValue = cell.NumericCellValue;
                    return true;
                
                default:
                    minValue = maxValue = default;
                    return false;
            }
        }
        
        private static bool TryGetFactors(string factorLine, out double minValue, out double maxValue)
        {
            if (string.IsNullOrWhiteSpace(factorLine))
            {
                minValue = default;
                maxValue = default;
                return false;
            }

            var values = factorLine
                .Trim().Replace(",", ".")
                .Split(' ').First()
                .Split('-')
                .Select(s => double.Parse(s, NumberStyles.Any, CultureInfo.InvariantCulture))
                .ToArray();

            switch (values.Length)
            {
                case 1:
                    minValue = maxValue = values[0];
                    return true;
                
                case 2:
                    minValue = values[0];
                    maxValue = values[1];
                    return true;
                
                default:
                    throw new ArgumentException($"Bad format ({factorLine})");
            }
        }

        private static void CreateIfNotExist(string resourcePath)
        {
            if (File.Exists(resourcePath))
                return;
            
            var demandFactorsBytes = Files.RtmDemandFactors;
            File.WriteAllBytes(resourcePath, demandFactorsBytes);
        }

        private static void DeleteFile(string resourcePath)
        {
            if (!File.Exists(resourcePath))
                return;
            
            try
            {
                File.Delete(resourcePath);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception);
            }
        }
    }
}