﻿using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.RtmDemandFactors.Model;
// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.RtmDemandFactors.ViewModels;

public class RtmDemandFactorsViewModel : ViewModel
{
    private readonly RtmDemandFactorsFinder _finder;

    public string SearchText { get; set; }
    public ObservableCollection<DemandFactorViewModel> FilteredItems { get; private set; }
    public DemandFactorViewModel SelectedItem { get; set; }
    public DemandFactorViewModel SelectedDemandFactorItem { get; set; }
    public string NewName { get; set; }
    public double NewDemandFactor { get; set; }
    public double MinDemandFactor { get; set; }
    public double MaxDemandFactor { get; set; }
    public bool ContainsSelectedItem { get; set; }
    public bool HasResults { get; set; }
    public bool IsOkEnabled { get; set; }
    public bool IsNameInput { get; set; }

    public RtmDemandFactorsViewModel(
        Repository<RtmData> rtmDataRepository)
    {
        _finder = new RtmDemandFactorsFinder(rtmDataRepository.First());
        ResetFilter();
    }
    
    protected override void OkAction()
    {
        IsNameInput = true;
        var demandFactor = SelectedDemandFactorItem.RtmDemandFactor;
        var groups = demandFactor.GetAllGroups().Reverse().ToList();
        var lastGroupNumber = groups.LastOrDefault(x => x.Number != null);
        var lastGroupNumberIndex = lastGroupNumber != null ? groups.IndexOf(lastGroupNumber) : 0;
        NewName = $"{string.Join(" - ", groups.Skip(lastGroupNumberIndex).Select(x => x.ToString()))} - {SelectedDemandFactorItem.Name}"
            .Replace(":", " -").Replace("  ", " ");
        MinDemandFactor = demandFactor.MinDemandFactor;
        MaxDemandFactor = demandFactor.MaxDemandFactor;
        NewDemandFactor = (MinDemandFactor + MaxDemandFactor) / 2;
    }

    public Command OkNameInputCommand => new(() =>
    {
        try
        {
            OkCallback?.Invoke((NewName, NewDemandFactor));
        }
        finally
        {
            SelectedItem = null;
            SelectedDemandFactorItem = null;
            IsNameInput = false;
            Close();
        }
    });

    public Command CancelNameInputCommand => new(() => IsNameInput = false);
    public Command MinDemandFactorCommand => new(() => NewDemandFactor = MinDemandFactor);
    public Command MaxDemandFactorCommand => new(() => NewDemandFactor = MaxDemandFactor);
    private void OnNewDemandFactorChanged()
    {
        if (NewDemandFactor < MinDemandFactor) NewDemandFactor = MinDemandFactor;
        if (NewDemandFactor > MaxDemandFactor) NewDemandFactor = MaxDemandFactor;
    }
    
    private void OnSelectedItemChanged()
    {
        if (SelectedItem?.IsDemandFactor ?? false)
            SelectedDemandFactorItem = SelectedItem;

        IsOkEnabled = SelectedDemandFactorItem != null;
    }
    
    private void OnSearchTextChanged()
    {
        var searchText = SearchText.ToLower();

        if (searchText == _finder.SearchText)
            return;

        if (string.IsNullOrWhiteSpace(searchText))
        {
            ResetFilter();
            return;
        }

        var searchParts = searchText.Split(' ', '.', ',')
            .Where(x => int.TryParse(x, out _))
            .Select(int.Parse)
            .ToArray();
        
        if (searchText.Length < 4 && searchParts.Length < 2)
            return;

        var alreadyFiltered = _finder.HasFilter;

        _finder.FilterBy(searchText);

        if (alreadyFiltered)
            FilterFilteredItems();
        else
            CreateFilteredItems();
    }

    private void FilterFilteredItems()
    {
        FilteredItems = FilteredItems
            .Where(e => e.ContainsInResults())
            .ToObservableCollection();

        FilteredItems.ForEach(e => e.FilterItems());
        FilteredItems.ForEach(e => e.Update());
        HasResults = _finder.HasResults;
        UpdateSelectedItem();
    }

    private void CreateFilteredItems()
    {
        FilteredItems = _finder.Groups
            .Where(g => g.Level == 1)
            .Select(g => new DemandFactorViewModel(g, _finder))
            .ToObservableCollection();

        FilteredItems.ForEach(item => item.IsExpanded = true);
        HasResults = _finder.HasResults;
        UpdateSelectedItem();
    }

    private void UpdateSelectedItem()
    {
        if (SelectedDemandFactorItem == null)
        {
            return;
        }
        if (!HasResults)
        {
            ContainsSelectedItem = false;
            return;
        }

        var selectedItem = FilteredItems
            .Select(e => e.FindViewModel(SelectedDemandFactorItem.RtmDemandFactor))
            .FirstOrDefault(e => e != null);

        if (selectedItem == null)
        {
            ContainsSelectedItem = false;
            return;
        }

        SelectedItem.IsSelected = false;
        SelectedItem = selectedItem;
        SelectedItem.IsSelected = true;
        ContainsSelectedItem = true;
        ExpandPathToSelected();
    }

    private void ExpandPathToSelected()
    {
        FilteredItems.ForEach(e => e.ExpandPathToSelected());
    }

    private void ResetFilter()
    {
        _finder.ResetFilter();
        CreateFilteredItems();
    }
}