﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.RtmDemandFactors.Model;

namespace CoLa.BimEd.UI.RtmDemandFactors.ViewModels;

public class DemandFactorViewModel : ViewModel
{
    public DemandFactorViewModel Parent { get; }
    private readonly RtmDemandFactorsFinder _finder;

    public readonly RtmDemandFactorGroup Group;
    public readonly RtmDemandFactor RtmDemandFactor;

    public string Name { get; set; }
    public string DemandFactor { get; set; }
    public string PowerFactor { get; set; }
    public bool HasSearchText { get; set; }
    public bool IsDemandFactor { get; set; }
    public bool IsSelected { get; set; }
    public bool IsExpanded { get; set; }
    public ObservableCollection<DemandFactorViewModel> Items { get; set; } = new();
    
    public string[] GroupsNames => RtmDemandFactor != null
        ? RtmDemandFactor.GetAllGroups()
            .Select(group => group.ToString())
            .Reverse()
            .ToArray()
        : Array.Empty<string>();

    public DemandFactorViewModel(RtmDemandFactor rtmDemandFactor, RtmDemandFactorsFinder finder, DemandFactorViewModel parent = null)
    {
        Parent = parent;
        _finder = finder;

        Group = null;
        IsDemandFactor = true;
        RtmDemandFactor = rtmDemandFactor;
        Name = rtmDemandFactor.Name;
        DemandFactor = rtmDemandFactor.DemandFactorAsString();
        PowerFactor = rtmDemandFactor.PowerFactorAsString();
        HasSearchText = finder.DemandFactorsWithSearchText.Contains(rtmDemandFactor);
    }

    public DemandFactorViewModel(RtmDemandFactorGroup group, RtmDemandFactorsFinder finder, DemandFactorViewModel parent = null)
    {
        Parent = parent;
        _finder = finder;

        Group = group;
        Name = group.ToString();
        IsDemandFactor = false;
        RtmDemandFactor = null;
        DemandFactor = null;
        PowerFactor = null;
        HasSearchText = finder.GroupsWithSearchText.Contains(group);
        
        Items = group.Groups
            .Where(x => !finder.HasFilter || finder.FilteredGroups.Contains(x))
            .Select(x => new DemandFactorViewModel(x, finder, this))
            .Concat(group.DemandFactors
                .Where(x => !finder.HasFilter || finder.FilteredDemandFactors.Contains(x))
                .Select(x => new DemandFactorViewModel(x, finder, this)))
            .ToObservableCollection();

        if (finder.HasFilter && HasItemsWithString())
            IsExpanded = true;
    }
    
    public bool ContainsInResults()
    {
        return IsDemandFactor
            ? _finder.FilteredDemandFactors.Contains(RtmDemandFactor)
            : _finder.FilteredGroups.Contains(Group);
    }

    public void FilterItems()
    {
        var itemsForRemove = new List<DemandFactorViewModel>(Items.Count);
        itemsForRemove.AddRange(Items.Where(item => !item.ContainsInResults()));
        itemsForRemove.ForEach(item => Items.Remove(item));
        Items.ForEach(x => x.FilterItems());
    }

    public void Update()
    {
        if (IsDemandFactor)
        {
            HasSearchText = _finder.DemandFactorsWithSearchText.Contains(RtmDemandFactor);
        }
        else
        {
            HasSearchText = _finder.GroupsWithSearchText.Contains(Group);

            if (_finder.HasFilter && HasItemsWithString())
                IsExpanded = true;
        }

        foreach (var item in Items)
        {
            item.Update();
        }
    }

    public void ExpandPathToSelected()
    {
        if (IsDemandFactor || !HasSelectedItem())
            return;
        
        IsExpanded = true;
        Items.ForEach(x => x.ExpandPathToSelected());
    }

    public DemandFactorViewModel FindViewModel(RtmDemandFactor rtmDemandFactor)
    {
        foreach (var item in Items)
        {
            if (item.IsDemandFactor && item.RtmDemandFactor == rtmDemandFactor)
                return item;
        }

        return Items
            .Select(item => item.FindViewModel(rtmDemandFactor))
            .FirstOrDefault(result => result != null);
    }

    private bool HasItemsWithString() => Items.Any(item => item.HasSearchText || item.HasItemsWithString());
    private bool HasSelectedItem() => Items.Any(item => item.IsSelected || item.HasSelectedItem());
}