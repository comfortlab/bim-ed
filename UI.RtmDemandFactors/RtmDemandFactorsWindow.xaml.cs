﻿using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.Utils;
using CoLa.BimEd.UI.RtmDemandFactors.Model;
using CoLa.BimEd.UI.RtmDemandFactors.ViewModels;

namespace CoLa.BimEd.UI.RtmDemandFactors;

public partial class RtmDemandFactorsWindow
{
    public RtmDemandFactorsWindow(Repository<RtmData> rtmDataRepository)
    {
        InitializeComponent();
        DataContext = new RtmDemandFactorsViewModel(rtmDataRepository) { Close = Close };
        this.SetPositionAndSizes(0.9);
    }
}