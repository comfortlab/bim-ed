﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WixSharp.CommonTasks;

namespace CoLa.BimEd.Infrastructure.Installer.Utils;

public static class InstallUtils
{
    public static Version GetVersionFromName(FileInfo bimEdMsi)
    {
        var split = Path.GetFileNameWithoutExtension(bimEdMsi.Name).Split('.').Reverse();
        var versionNumbers = new List<int>();

        foreach (var ch in split)
        {
            if (versionNumbers.Count > 4 || !int.TryParse(ch, out var number))
                break;

            versionNumbers.Insert(0, number);
        }

        return new Version(string.Join(".", versionNumbers));
    }

    public static FileInfo GetFile(string currentDirectory, string searchPattern)
    {
        var files = Directory.GetFiles(currentDirectory, searchPattern);
        var dllFilePath = files.FirstOrDefault();

        if (!string.IsNullOrEmpty(dllFilePath))
            return new FileInfo(dllFilePath);

        var directories = Directory.GetDirectories(currentDirectory).ToList();

        while (directories.Any())
        {
            var directory = directories.First();
            directories.RemoveAt(0);

            var dllFile = GetFile(directory, searchPattern);

            if (dllFile != null)
                return dllFile;
        }

        return null;
    }
        
    public static string GetWixLocation()
    {
        var allWixLocation = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
            @".nuget\packages\wix\");

        if (!Directory.Exists(allWixLocation))
            throw new DirectoryNotFoundException($"{allWixLocation}\nInstall Wix nuget package.");

        var wixCurrentVersion = new DirectoryInfo(allWixLocation).GetDirectories()
            .OrderBy(x => x.Name).Last();

        var wixLocation = Path.Combine(wixCurrentVersion.FullName, "tools");

        if (!Directory.Exists(wixLocation))
            throw new DirectoryNotFoundException($"{wixLocation}");
            
        return wixLocation;
    }
        
    public static DirectoryInfo GetSolutionDirectoryInfo(string currentPath = null)
    {
        var directory = new DirectoryInfo(currentPath ?? Directory.GetCurrentDirectory());
            
        while (directory != null && !directory.GetFiles("*.sln").Any())
        {
            directory = directory.Parent;
        }
            
        return directory;
    }
        
    public static void Sign(string filename)
    {
        const string signPath = @"C:\Program Files (x86)\Microsoft SDKs\ClickOnce\SignTool\signtool.exe";
            
        if (!File.Exists(signPath))
            return;
            
        var signTool = new ExternalTool
        {
            ExePath = @"C:\Program Files (x86)\Microsoft SDKs\ClickOnce\SignTool\signtool.exe",
            Arguments = $@"sign /tr http://timestamp.comodoca.com /td sha256 /fd sha256 /a ""{filename}"""
        };

        signTool.ConsoleRun();
    }
        
    public static string CreateDirectory(string directory)
    {
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);

        return directory;
    }
    
    public static DirectoryInfo CreateTempDirectory(string tempLocation)
    {
        return Directory.Exists(tempLocation)
            ? new DirectoryInfo(tempLocation)
            : Directory.CreateDirectory(tempLocation);
    }

    public static void CopyFile(string sourceDir, string fileName, string targetDir)
    {
        File.Copy(Path.Combine(sourceDir, fileName), Path.Combine(targetDir, fileName), overwrite: true);
    }

    public static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
    {
        var dir = new DirectoryInfo(sourceDir);

        if (!dir.Exists)
            throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

        var dirs = dir.GetDirectories();
        Directory.CreateDirectory(destinationDir);

        foreach (var file in dir.GetFiles())
        {
            var targetFilePath = Path.Combine(destinationDir, file.Name);
            file.CopyTo(targetFilePath, overwrite: true);
        }

        if (!recursive) return;
            
        foreach (var subDir in dirs)
        {
            var newDestinationDir = Path.Combine(destinationDir, subDir.Name);
            CopyDirectory(subDir.FullName, newDestinationDir, true);
        }
    }
        
    public static void DeleteTempFiles(string tempLocation)
    {
        try
        {
            if (Directory.Exists(tempLocation))
                Directory.Delete(tempLocation, recursive: true);
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
        }
    }

    public static string ConvertDate(string dateAsString)
    {
        var versionDate = dateAsString
            .Split('.', '-', ' ')
            .Select(x => int.TryParse(x, out var i) ? i : -1)
            .Where(x => x > 0)
            .ToArray();

        return new DateTime(versionDate[0], versionDate[1], versionDate[2])
            .ToString("dd MMMM yyyy");
    }
}