﻿using CoLa.BimEd.Infrastructure.Installer.UI;
using Microsoft.Tools.WindowsInstallerXml.Bootstrapper;

namespace CoLa.BimEd.Infrastructure.Installer;

public class ManagedBA : BootstrapperApplication
{
    /// <summary>
    /// Entry point that is called when the bootstrapper application is ready to run.
    /// </summary>
    protected override void Run()
    {
        new SetupWindow(this).ShowDialog();
        Engine.Quit(0);
    }
}