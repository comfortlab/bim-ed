﻿namespace CoLa.BimEd.Infrastructure.Installer.Projects;

internal class CertificateInfo
{
    internal const string CertificateThumbprint = "84e5396d02fb68d2b941784d8f5d7e52c54b4111";
    internal const string CertificateTimestampUrl = @"http://timestamp.digicert.com?td=sha256";
}