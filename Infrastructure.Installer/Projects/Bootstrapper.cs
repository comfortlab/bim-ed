﻿using System;
using System.IO;
using System.Linq;
using CoLa.BimEd.Infrastructure.Installer.Utils;
using WixSharp;
using WixSharp.Bootstrapper;

// ReSharper disable LocalizableElement

namespace CoLa.BimEd.Infrastructure.Installer.Projects;

public class Bootstrapper
{
    public static void Build()
    {
        Compiler.WixLocation = InstallUtils.GetWixLocation();

        AutoElements.SupportEmptyDirectories = CompilerSupportState.Enabled;

        var currentDirectory = new DirectoryInfo(Directory.GetCurrentDirectory());
        var msiFiles = currentDirectory.GetFiles(searchPattern: "*.msi");

        if (!TryGetAppMsi(msiFiles, out var appMsi, out var version, out var lastWriteTime) ||
            !TryGetBrowserMsi(msiFiles, out var browserEdMsi)
           )
            return;

        var bootstrapper =
            new Bundle("BIM-ED",
                new MsiPackage(appMsi.FullName)
                {
                    Id = "Application",
                    DisplayInternalUI = false,
                    Visible = true,
                },
                new RollbackBoundary(),
                new MsiPackage(browserEdMsi.FullName)
                {
                    Id = "Browser",
                    DisplayInternalUI = false,
                    Visible = true,
                }
            )
            {
                OutFileName = Path.GetFileNameWithoutExtension(appMsi.Name),
                // UpgradeCode = new Guid("89659031-7ADD-43C1-BCC3-6517F5031325"),
                Version = version,
                DisableRemove = true,
                DisableModify = "yes",
                Application = new ManagedBootstrapperApplication("%this%", "BootstrapperCore.config")
                {
                    Variables = new []
                    {
                        new Variable(Constants.Version, version.ToString(fieldCount: 3), VariableType.version),
                        new Variable(Constants.VersionDate, lastWriteTime.ToString("yyyy.MM.dd"), VariableType.@string)
                    }
                },
                DigitalSignature = new DigitalSignatureBootstrapper()
                {
                    Description = "BIM Electrical Design",
                    TimeUrl = new Uri(CertificateInfo.CertificateTimestampUrl),
                    OptionalArguments = $"/v /sha1 {CertificateInfo.CertificateThumbprint} /fd sha256",
                }
            };

        var installer = bootstrapper.Build();
            
        // InstallUtils.Sign(installer);
    }

    private static bool TryGetAppMsi(FileInfo[] msiFiles, out FileInfo appMsi, out Version version, out DateTime versionDate)
    {
        appMsi = msiFiles
            .Where(x => x.Name.StartsWith("BIM"))
            .OrderBy(x => x.LastWriteTimeUtc).LastOrDefault();

        if (appMsi != null)
        {
            version = InstallUtils.GetVersionFromName(appMsi);
            versionDate = appMsi.LastWriteTime;
            return true;
        }

        var appMsiPath = ApplicationInstaller.Build(out version, out versionDate);

        if (!string.IsNullOrWhiteSpace(appMsiPath))
        {
            appMsi = new FileInfo(appMsiPath);
            return true;
        }
        else
        {
            Console.WriteLine("MSI was not build.");
            version = null;
            versionDate = DateTime.MinValue;
            return false;
        }
    }

    private static bool TryGetBrowserMsi(FileInfo[] msiFiles, out FileInfo browserMsi)
    {
        browserMsi = msiFiles
            .Where(x => x.Name.StartsWith("Chromium"))
            .OrderBy(x => x.LastWriteTimeUtc).LastOrDefault();

        if (browserMsi != null)
            return true;

        var browserMsiPath = ChromiumInstaller.Build();

        if (!string.IsNullOrWhiteSpace(browserMsiPath))
        {
            browserMsi = new FileInfo(browserMsiPath);
            return true;
        }
        else
        {
            Console.WriteLine("Browser MSI was not build.");
            return false;
        }
    }
}