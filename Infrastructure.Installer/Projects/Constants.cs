namespace CoLa.BimEd.Infrastructure.Installer.Projects;

internal static class Constants
{
    internal const string Guid = "Guid";
    internal const string ProductId = "ProductId";
    internal const string Progress = "ProgressOnly";
    internal const string Version = "Version";
    internal const string VersionDate = "VersionDate";
}