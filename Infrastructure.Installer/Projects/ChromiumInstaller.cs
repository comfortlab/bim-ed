﻿using System;
using System.IO;
using System.Linq;
using CoLa.BimEd.Infrastructure.Installer.Utils;
using WixSharp;

namespace CoLa.BimEd.Infrastructure.Installer.Projects;

internal static class ChromiumInstaller
{
    private static readonly Guid ProductId = Guid.NewGuid();
    private static readonly Guid GUID = new("C231AE86-6C4F-4553-9EA1-3D32946C671D");
    private static readonly Guid UpgradeCode = new("FB0943FF-5AA5-4342-B03B-5DD9C6E9D347");

    public static string Build()
    {
        Compiler.WixLocation = InstallUtils.GetWixLocation();
        AutoElements.SupportEmptyDirectories = CompilerSupportState.Enabled;

        var cefLocation = GetCefLocation();

        const string tempLocation = "Temp";

        var tempDirectory = Directory.Exists(tempLocation)
            ? new DirectoryInfo(tempLocation)
            : Directory.CreateDirectory(tempLocation);

        AddSourceFiles(tempDirectory, out var version);

        var targetPath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
            "ComfortLab", "Browser", "Application", version.ToString());

        var dir = new Dir(
            targetPath: targetPath,
            items: new WixEntity[]
            {
                // new Files("readme.txt"),
                new Files(Path.Combine(cefLocation, "*.*")),
                new Files(Path.Combine(tempDirectory.FullName, "*.dll")),
                new Files(Path.Combine(tempDirectory.FullName, "*.exe")),
                // new Files(Path.Combine(cefWpfLocation, "*.dll"))
            });

        var outFilename = $"ChromiumRuntimeBrowser_v.{version}";

        var project = new Project(name: "Chromium Runtime Browser", items: dir)
        {
            Name = "Chromium Runtime Browser",
            ProductId = ProductId,
            GUID = GUID,
            UpgradeCode = UpgradeCode,
            InstallScope = InstallScope.perMachine,
            MajorUpgrade = new MajorUpgrade
            {
                AllowDowngrades = true,
                Schedule = UpgradeSchedule.afterInstallInitialize,
            },
            OutFileName = outFilename,
            Platform = Platform.x64,
            UI = WUI.WixUI_ProgressOnly,
            Version = version,
            DigitalSignature = new DigitalSignature()
            {
                Description = "Chromium Runtime Browser",
                TimeUrl = new Uri(CertificateInfo.CertificateTimestampUrl),
                OptionalArguments = $"/v /sha1 {CertificateInfo.CertificateThumbprint} /fd sha256",
            },
        };

        var msi = project.BuildMsi();

        try
        {
            if (Directory.Exists(tempLocation))
                Directory.Delete(tempLocation, recursive: true);
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
        }

        return msi;
    }

    private static string GetCefLocation()
    {
        var lastVersionDirectory = GetLastVersionDirectory("cef.redist.x64");
        var cefSourceLocation = Path.Combine(lastVersionDirectory.FullName, "CEF");

        if (!Directory.Exists(cefSourceLocation))
            throw new DirectoryNotFoundException($"{cefSourceLocation}");

        return cefSourceLocation;
    }

    private static void AddSourceFiles(DirectoryInfo tempDirectory, out Version version)
    {
        var commonDirectory = GetLastVersionDirectory("cefsharp.common");
        var wpfDirectory = GetLastVersionDirectory("cefsharp.wpf");

        version = new Version(commonDirectory.Name);

        var files = new[]
        {
            commonDirectory.GetFiles("CefSharp.dll", SearchOption.AllDirectories).LastOrDefault(NotX86),
            commonDirectory.GetFiles("CefSharp.Core.dll", SearchOption.AllDirectories).LastOrDefault(NotX86),
            commonDirectory.GetFiles("CefSharp.Core.Runtime.dll", SearchOption.AllDirectories).LastOrDefault(NotX86),
            commonDirectory.GetFiles("CefSharp.BrowserSubprocess.exe", SearchOption.AllDirectories).LastOrDefault(NotX86),
            commonDirectory.GetFiles("CefSharp.BrowserSubprocess.Core.dll", SearchOption.AllDirectories).LastOrDefault(NotX86),
            wpfDirectory.GetFiles("CefSharp.Wpf.dll", SearchOption.AllDirectories).LastOrDefault(NotX86),
        };

        foreach (var file in files)
        {
            file.CopyTo(Path.Combine(tempDirectory.FullName, file.Name), overwrite: true);
        }
    }

    private static bool NotX86(FileInfo fileInfo)
    {
        var directoryName = fileInfo.DirectoryName;

        return !string.IsNullOrWhiteSpace(directoryName) &&
               !directoryName.Contains(@"\x86\") &&
               !directoryName.EndsWith(@"\x86");
    }

    private static DirectoryInfo GetLastVersionDirectory(string folderName)
    {
        var path = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
            ".nuget", "packages", folderName);

        if (!Directory.Exists(path))
            throw new DirectoryNotFoundException($"{path}\nInstall CEF Sharp nuget package.");

        return new DirectoryInfo(path).GetDirectories()
            .Where(x =>
            {
                var split = x.Name.Split('.');
                return split.Length is >= 2 and <= 4 && split.All(s => int.TryParse(s, out _));
            })
            .OrderBy(x => new Version(x.Name))
            .Last();
    }
}