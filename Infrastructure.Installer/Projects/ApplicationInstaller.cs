﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CoLa.BimEd.Infrastructure.Installer.Utils;
using WixSharp;

namespace CoLa.BimEd.Infrastructure.Installer.Projects;

public static class ApplicationInstaller
{
    private static readonly Guid ProductId = Guid.NewGuid();
    private static readonly Guid Guid = new("55BB6689-B7EA-4949-92FB-3201AD8A28AB");
    private static readonly Guid UpgradeCode = new("43F64894-FDA4-4C51-9D72-6DB691FDB400");

    public static string Build(out Version version, out DateTime versionDate)
    {
        Compiler.WixLocation = InstallUtils.GetWixLocation();

        AutoElements.SupportEmptyDirectories = CompilerSupportState.Enabled;

        const string tempLocation = "Temp";

        var tempDirectory = InstallUtils.CreateTempDirectory(tempLocation);
        var tempBundleDir = InstallUtils.CreateDirectory(Path.Combine(tempDirectory.FullName, "BIM Electrical Design.bundle"));
        var tempContentsDir = InstallUtils.CreateDirectory(Path.Combine(tempBundleDir, "Contents"));
        var tempProgramDir = InstallUtils.CreateDirectory(Path.Combine(tempContentsDir, "Program"));
            
        var solutionDir = InstallUtils.GetSolutionDirectoryInfo();
        var shareDir = new DirectoryInfo(Path.Combine(solutionDir.FullName, ".share"));
        var dlls = shareDir.GetFiles("*.dll");
        var folders = shareDir.GetDirectories();
            
        if (!dlls.Any())
        {
            Console.WriteLine($@"Source *.dll files are not found");
            Console.ReadKey();
            version = null;
            versionDate = DateTime.MinValue;
            return null;
        }

        foreach (var dll in dlls)
        {
            InstallUtils.Sign(dll.FullName);
            dll.CopyTo(Path.Combine(tempProgramDir, dll.Name), overwrite: true);
        }

        foreach (var folder in folders)
        {
            InstallUtils.CopyDirectory(folder.FullName, Path.Combine(tempProgramDir, folder.Name), recursive: true);
        }
            
        var addInsProjectDir = Path.Combine(solutionDir.FullName, "Add-Ins.FullVersion");
        var installSourceDir = Path.Combine(addInsProjectDir, "Install");
            
        InstallUtils.CopyFile(installSourceDir, "PackageContents.xml", tempBundleDir);
        InstallUtils.CopyFile(installSourceDir, "BIM Electrical Design.addin", tempContentsDir);
        InstallUtils.CopyFile(installSourceDir, "BIMElectricalDesign.Updater.exe", tempProgramDir);
        InstallUtils.CopyFile(installSourceDir, "DevicesScheme.sdf", tempProgramDir);
        InstallUtils.CopyFile(installSourceDir, "NkuScheme.sdf", tempProgramDir);
        InstallUtils.CopyFile(installSourceDir, "SeBim.Infrastructure.Net.AuthClient.exe", tempProgramDir);
        InstallUtils.CopyFile(installSourceDir, "SeBim.Infrastructure.Net.AuthClient.exe.config", tempProgramDir);
        InstallUtils.CopyFile(installSourceDir, "SeBim.Infrastructure.Net.Proxy.exe", tempProgramDir);

        // InstallUtils.CopyDirectory(Path.Combine(sourceDir, "ru"), Path.Combine(tempProgramDir, "ru"), true);
        // InstallUtils.CopyDirectory(Path.Combine(sourceDir, "ru"), Path.Combine(tempProgramDir, "ru"), true);
        InstallUtils.CopyDirectory(Path.Combine(installSourceDir, "amd64"), Path.Combine(tempProgramDir, "amd64"), recursive: true);

        var sourceAppDll = dlls.First();
        var fileVersionInfo = FileVersionInfo.GetVersionInfo(sourceAppDll.FullName);
        var appName = fileVersionInfo.ProductName;
        version = new Version(fileVersionInfo.ProductVersion);
        versionDate = sourceAppDll.LastWriteTime;

        var targetPath = Path.Combine("%CommonAppDataFolder%", "Autodesk", "ApplicationPlugins");
        var sourceFiles = new Files(Path.Combine(tempDirectory.FullName, "*.*"));
        var outFilename = $"BimElectricalDesign_v.{version}";
            
        var project = new Project(appName, new Dir(targetPath, sourceFiles))
        {
            ControlPanelInfo =
            {
                Manufacturer = "LK-Proekt LTD",
            },
            EmbeddedUI = new EmbeddedAssembly(System.Reflection.Assembly.GetExecutingAssembly().Location),
            GUID = Guid,
            ProductId = ProductId,
            UpgradeCode = UpgradeCode,
            Properties = new Property[]
            {
                new(Constants.ProductId, ProductId.ToString()),
                new(Constants.Guid, Guid.ToString()),
                new(Constants.Version, version.ToString(fieldCount: 3)),
                new(Constants.VersionDate, versionDate.ToString("yyyy.MM.dd")),
            },
            InstallerVersion = 200,
            // InstallScope = InstallScope.perMachine, // нет доступа к файлам БД
            InstallScope = InstallScope.perUser,
            Language = "ru-RU",
            MajorUpgrade = new MajorUpgrade
            {
                AllowDowngrades = true,
                Schedule = UpgradeSchedule.afterInstallInitialize,
            },
            OutFileName = outFilename,
            Platform = Platform.x64,
            Version = version,
            DigitalSignature = new DigitalSignature()
            {
                Description = "BIM Electrical Design",
                TimeUrl = new Uri(CertificateInfo.CertificateTimestampUrl),
                OptionalArguments = $"/v /sha1 {CertificateInfo.CertificateThumbprint} /fd sha256",
            },
        };

        var msi = project.BuildMsi();
            
        // InstallUtils.Sign($"{outFilename}.msi");
        InstallUtils.DeleteTempFiles(tempLocation);

        return msi;
    }
}