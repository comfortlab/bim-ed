﻿using System;
using CoLa.BimEd.Infrastructure.Installer;
using CoLa.BimEd.Infrastructure.Installer.Projects;
using Microsoft.Tools.WindowsInstallerXml.Bootstrapper;

// ReSharper disable LocalizableElement

[assembly: BootstrapperApplication(typeof(ManagedBA))]

namespace CoLa.BimEd.Infrastructure.Installer;

public class Program
{
    [STAThread]
    public static void Main()
    {
        while (true)
        {
            Console.WriteLine("1 - Build Bootstrapper installer");
            Console.WriteLine("2 - Build App installer");
            Console.WriteLine("3 - Build Chromium Runtime Browser installer");

            var key = Console.ReadLine();

            switch (key)
            {
                case "1":
                    Bootstrapper.Build();
                    return;

                case "2":
                    ApplicationInstaller.Build(out var version, out var versionDate);
                    return;

                case "3":
                    ChromiumInstaller.Build();
                    return;

                default:
                    return;
            }
        }
    }
}