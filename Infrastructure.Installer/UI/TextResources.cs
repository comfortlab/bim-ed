﻿using System.Globalization;

namespace CoLa.BimEd.Infrastructure.Installer.UI;

public class TextResources
{
    private readonly string _currentCulture;

    public TextResources()
    {
        _currentCulture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
    }
        
    public string Header { get; set; } = "BIM Electrical Design";

    public string RevitAddIn => _currentCulture switch
    {
        "ru" => "Работа с электрическими цепями в Revit",
        _ => "Revit add-in for electrical circuits"
    };

    public string Install => _currentCulture switch
    {
        "ru" => "Установить",
        _ => "Install"
    };

    public string Repair => _currentCulture switch
    {
        "ru" => "Восстановить",
        _ => "Repair"
    };

    public string Uninstall => _currentCulture switch
    {
        "ru" => "Удалить",
        _ => "Uninstall"
    };

    public string Cancel => _currentCulture switch
    {
        "ru" => "Отмена",
        _ => "Cancel"
    };

    public string Exit => _currentCulture switch
    {
        "ru" => "Закрыть",
        _ => "Exit"
    };

    public string Apply => _currentCulture switch
    {
        "ru" => "Принять",
        _ => "Apply"
    };

    public string AppInstalled => _currentCulture switch
    {
        "ru" => "Приложение успешно установлено.",
        _ => "The application is successfully installed."
    };

    public string AppUninstalled => _currentCulture switch
    {
        "ru" => "Приложение удалено.",
        _ => "The application has been removed."
    };

    public string AppCancelled => _currentCulture switch
    {
        "ru" => "Установка приложения была прервана.",
        _ => "The application installation was cancelled."
    };

    public string AppExistLaterVersion => _currentCulture switch
    {
        "ru" => "Уже установлена более новая версия приложения.",
        _ => "A newer version of the application is already installed."
    };
}