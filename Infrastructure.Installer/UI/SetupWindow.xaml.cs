﻿using System.Threading;
using CoLa.BimEd.Infrastructure.Installer.UI.ViewModels;
using Microsoft.Deployment.WindowsInstaller;
using Microsoft.Tools.WindowsInstallerXml.Bootstrapper;
using InstallMessage = Microsoft.Deployment.WindowsInstaller.InstallMessage;

namespace CoLa.BimEd.Infrastructure.Installer.UI;

public partial class SetupWindow
{
    public SetupWindow(BootstrapperApplication bootstrapperApplication)
    {
        InitializeComponent();
        DataContext = new BootstrapSetupViewModel(bootstrapperApplication);
    }
        
    public SetupWindow(Session session, ManualResetEvent installStartEvent)
    {
        InitializeComponent();
        DataContext = _applicationSetupViewModel = new ApplicationSetupViewModel(session, installStartEvent);
    }

    private readonly ApplicationSetupViewModel _applicationSetupViewModel;
        
    public MessageResult ProcessMessage(InstallMessage messageType, Record messageRecord, MessageButtons buttons, MessageIcon icon, MessageDefaultButton defaultButton)
    {
        return _applicationSetupViewModel.ProcessMessage(messageType, messageRecord, buttons, icon, defaultButton);
    }

    public void Finish()
    {
        _applicationSetupViewModel.FinishView();
    }
}