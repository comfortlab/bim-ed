using System;
using System.Windows;
using System.Windows.Input;

namespace CoLa.BimEd.Infrastructure.Installer.UI;

public class Command : ICommand
{
    public Action CommandAction { set; get; }
    public Func<bool> CanExecuteFunc { set; get; }

    public event EventHandler CanExecuteChanged
    {
        add => CommandManager.RequerySuggested += value;
        remove => CommandManager.RequerySuggested -= value;
    }

    public Command(Action commandAction) => CommandAction = commandAction;
    
    public bool CanExecute(object parameter) => CanExecuteFunc == null || CanExecuteFunc();
    
    public void Execute(object parameter)
    {
        try
        {
            if (CommandAction != null)
                CommandAction();
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}