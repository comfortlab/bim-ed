﻿using System.Diagnostics;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace CoLa.BimEd.Infrastructure.Installer.UI;

public class ExternalBrowserHyperlink : Hyperlink
{
    public ExternalBrowserHyperlink()
    {
        RequestNavigate += OnRequestNavigate;
    }

    private static void OnRequestNavigate(object sender, RequestNavigateEventArgs e)
    {
        Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
        e.Handled = true;
    }
}