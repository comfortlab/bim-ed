﻿using System;
using System.Diagnostics;
using System.Threading;
using CoLa.BimEd.Infrastructure.Installer.Projects;
using CoLa.BimEd.Infrastructure.Installer.Utils;
using Microsoft.Deployment.WindowsInstaller;
using WixSharp;

namespace CoLa.BimEd.Infrastructure.Installer.UI.ViewModels;

public class ApplicationSetupViewModel : SetupViewModel
{
    private readonly ManualResetEvent _installStartEvent;
    private readonly InstallProgressCounter _progressCounter;
    private readonly Session _session;

    public ApplicationSetupViewModel(Session session, ManualResetEvent installStartEvent)
    {
        _installStartEvent = installStartEvent;
        _progressCounter = new InstallProgressCounter(scriptPhaseWeight: 0.5);
        _session = session;
        
        SetStartView();
    }

    private void SetStartView()
    {
        if (_session == null)
        {
            InstallView();
            return;
        }
        
        Version = _session[Constants.Version];
        VersionDate = InstallUtils.ConvertDate(_session[Constants.VersionDate]);
        
        var installedVersion = _session.LookupInstalledVersion();
        var newVersion = new Version(Version);

        if (installedVersion != newVersion)
        {
            InstallView();
        }
        else
        {
            RepairUninstallView();
        }
    }

    protected override void StartInstall()
    {
        WixSharp.CommonTasks.UACRevealer.Enter();
        _installStartEvent.Set();
        WixSharp.CommonTasks.UACRevealer.Exit();
    }

    protected override void StartRepair()
    {
        StartInstall();
    }

    protected override void StartUninstall()
    {
        var id = _session[Constants.ProductId];
        
        var process = new Process
        {
            StartInfo =
            {
                FileName = "msiexec.exe",
                // Arguments = "/x " + ApplicationInstaller.ProductId + " /qn"
                Arguments = $"/x {id} /q"
            }
        };

        process.Start();
        process.WaitForExit();
    }

    protected override void RepairUninstallView()
    {
        CancelVisible = RepairVisible = true;
        SetWindowPosition(DefaultHeight);
    }

    public MessageResult ProcessMessage(InstallMessage messageType, Record messageRecord,
        MessageButtons buttons, MessageIcon icon, MessageDefaultButton defaultButton)
    {
        WixSharp.CommonTasks.UACRevealer.Exit();

        _progressCounter.ProcessMessage(messageType, messageRecord);

        Progress = (int)_progressCounter.Progress * 100;

        if (!Cancel)
            return MessageResult.OK;
            
        Message = TextResources.AppCancelled;
        MessageVisible = true;

        return MessageResult.Cancel;
    }
}