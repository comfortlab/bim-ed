﻿using System;
using System.Windows;
using CoLa.BimEd.Infrastructure.Installer.Utils;
using Microsoft.Tools.WindowsInstallerXml.Bootstrapper;

namespace CoLa.BimEd.Infrastructure.Installer.UI.ViewModels;

public class BootstrapSetupViewModel : SetupViewModel
{
    private readonly BootstrapperApplication _bootstrapperApplication;

    public BootstrapSetupViewModel(BootstrapperApplication bootstrapperApplication)
    {
        _bootstrapperApplication = bootstrapperApplication;

        if (_bootstrapperApplication == null)
        {
            License = true;
            SetWindowPosition(800);
            return;
        }
            
        _bootstrapperApplication.ApplyComplete += OnApplyComplete;
        _bootstrapperApplication.DetectPackageComplete += OnDetectPackageComplete;
        _bootstrapperApplication.Error += OnError;
        _bootstrapperApplication.PlanComplete += OnPlanComplete;
        _bootstrapperApplication.Progress += OnProgress;
            
        _bootstrapperApplication.Engine.Detect();
            
        Version = _bootstrapperApplication.Engine.VersionVariables["Version"].ToString(3);
        VersionDate = InstallUtils.ConvertDate(_bootstrapperApplication.Engine.StringVariables["VersionDate"]);
    }

    private void OnApplyComplete(object sender, ApplyCompleteEventArgs e)
    {
        FinishView();
    }

    private void OnDetectPackageComplete(object sender, DetectPackageCompleteEventArgs e)
    {
        if (e.PackageId != "Application") 
            return;
            
        switch (e.State)
        {
            case PackageState.Absent:
                InstallView();
                break;

            case PackageState.Present:
                RepairUninstallView();
                break;
        }
    }

    private void OnError(object sender, ErrorEventArgs e)
    {
        ProgressVisible = false;
        Message = e.ErrorMessage;
        MessageVisible = true;
    }

    private void OnPlanComplete(object sender, PlanCompleteEventArgs e)
    {
        if (e.Status >= 0)
            _bootstrapperApplication.Engine.Apply(IntPtr.Zero);
    }

    private void OnProgress(object sender, ProgressEventArgs e)
    {
        Progress = e.ProgressPercentage;

        if (!Cancel)
            return;
            
        e.Result = Result.Cancel;
        
        Message = TextResources.AppCancelled;
        MessageVisible = true;
    }

    protected override void StartInstall()
    {
        try
        {
            _bootstrapperApplication.Engine.Plan(LaunchAction.Install);
        }
        catch (Exception e)
        {
            MessageBox.Show($"{e.Message}\n{e.StackTrace}");
        }
    }

    protected override void StartRepair()
    {
        _bootstrapperApplication.Engine.Plan(LaunchAction.Repair);
    }

    protected override void StartUninstall()
    {
        _bootstrapperApplication.Engine.Plan(LaunchAction.Uninstall);
    }
}