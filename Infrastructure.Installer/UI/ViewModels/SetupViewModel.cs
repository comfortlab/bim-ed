﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Threading;

namespace CoLa.BimEd.Infrastructure.Installer.UI.ViewModels;

public abstract class SetupViewModel : INotifyPropertyChanged
{
    protected const double DefaultHeight = 400;
    protected const double LicenceHeight = 800;
        
    public event PropertyChangedEventHandler PropertyChanged;
    
    private bool _agreeFederalLaw;
    private bool _agreeLicence;
    private bool _allowInstall;
    private double _height;
    private double _top;
    private bool _license;
    private string _message;
    private bool _messageVisible;
    private int _progress;
    private bool _progressVisible;
    private string _version;
    private string _versionDate;
    private bool _cancelVisible;
    private bool _exitVisible;
    private bool _installVisible;
    private bool _repairVisible;
    private bool _uninstallVisible;

    protected SetupViewModel()
    {
        TextResources = new TextResources();
    }

    protected abstract void StartInstall();
    protected abstract void StartRepair();
    protected abstract void StartUninstall();
 
    protected bool Cancel;

    protected void SetWindowPosition(double height)
    {
        Height = height;
        Top = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;
    }

    protected void InstallView()
    {
        CancelVisible = InstallVisible = License = true;
        SetWindowPosition(LicenceHeight);
    }

    protected virtual void RepairUninstallView()
    {
        CancelVisible = RepairVisible = UninstallVisible = true;
        SetWindowPosition(DefaultHeight);
    }

    protected internal void FinishView()
    {
        CancelVisible = InstallVisible = RepairVisible = UninstallVisible = ProgressVisible = false;
        ExitVisible = MessageVisible = true;
    }
        
    public Command InstallCommand => new(() =>
    {
        Message = TextResources.AppInstalled;
        License = InstallVisible = false;
        ProgressVisible = true;
        SetWindowPosition(DefaultHeight);
        StartInstall();
    })
    {
        CanExecuteFunc = () => AllowInstall
    };

    public Command RepairCommand => new(() =>
    {
        Message = TextResources.AppInstalled;
        ProgressVisible = UninstallVisible = false;
        StartRepair();
    });

    public Command UninstallCommand => new(() =>
    {
        Message = TextResources.AppUninstalled;
        UninstallVisible = false;
        ProgressVisible = true;
        StartUninstall();
    });

    public Command CancelCommand => new(() =>
    {
        if (ProgressVisible)
            Cancel = true;
        else
            ExitCommand.Execute(null);
    });

    public Command ExitCommand => new(() =>
    {
        Dispatcher.CurrentDispatcher.InvokeShutdown();
    });

    public bool AgreeFederalLaw
    {
        get => _agreeFederalLaw;
        set
        {
            _agreeFederalLaw = value;
            OnPropertyChanged(nameof(AgreeFederalLaw));
            AllowInstall = AgreeFederalLaw && AgreeLicence;
        }
    }

    public TextResources TextResources { get; }
        
    public bool AgreeLicence
    {
        get => _agreeLicence;
        set
        {
            _agreeLicence = value;
            OnPropertyChanged(nameof(AgreeLicence));
            AllowInstall = AgreeFederalLaw && AgreeLicence;
        }
    }

    public bool AllowInstall
    {
        get => _allowInstall;
        set
        {
            _allowInstall = value;
            OnPropertyChanged(nameof(AllowInstall));
        }
    }
        
    public double Height
    {
        get => _height;
        set
        {
            _height = value;
            OnPropertyChanged(nameof(Height));
        }
    }

    public double Top
    {
        get => _top;
        set
        {
            _top = value;
            OnPropertyChanged(nameof(Top));
        }
    }

    public bool License
    {
        get => _license;
        set
        {
            _license = value;
            OnPropertyChanged(nameof(License));
        }
    }

    public int Progress
    {
        get => _progress;
        set
        {
            _progress = value;
            OnPropertyChanged(nameof(Progress));
        }
    }

    public bool ProgressVisible
    {
        get => _progressVisible;
        set
        {
            _progressVisible = value;
            OnPropertyChanged(nameof(ProgressVisible));
        }
    }

    public string Version
    {
        get => _version;
        set
        {
            _version = value;
            OnPropertyChanged(nameof(Version));
        }
    }

    public string VersionDate
    {
        get => _versionDate;
        set
        {
            _versionDate = value;
            OnPropertyChanged(nameof(VersionDate));
        }
    }

    public bool CancelVisible
    {
        get => _cancelVisible;
        set
        {
            _cancelVisible = value;
            OnPropertyChanged(nameof(CancelVisible));
        }
    }

    public bool ExitVisible
    {
        get => _exitVisible;
        set
        {
            _exitVisible = value;
            OnPropertyChanged(nameof(ExitVisible));
        }
    }

    public bool InstallVisible
    {
        get => _installVisible;
        set
        {
            _installVisible = value;
            OnPropertyChanged(nameof(InstallVisible));
        }
    }

    public bool RepairVisible
    {
        get => _repairVisible;
        set
        {
            _repairVisible = value;
            OnPropertyChanged(nameof(RepairVisible));
        }
    }

    public bool UninstallVisible
    {
        get => _uninstallVisible;
        set
        {
            _uninstallVisible = value;
            OnPropertyChanged(nameof(UninstallVisible));
        }
    }

    public string Message
    {
        get => _message;
        set
        {
            _message = value;
            OnPropertyChanged(nameof(Message));
        }
    }

    public bool MessageVisible
    {
        get => _messageVisible;
        set
        {
            _messageVisible = value;
            OnPropertyChanged(nameof(MessageVisible));
        }
    }

    private void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}