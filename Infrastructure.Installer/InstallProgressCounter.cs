﻿using System;
using Microsoft.Deployment.WindowsInstaller;

namespace CoLa.BimEd.Infrastructure.Installer;

/// <summary>
/// Tracks MSI progress messages and converts them to usable progress.
/// </summary>
public class InstallProgressCounter
{
    private int _total;
    private int _completed;
    private int _step;
    private bool _moveForward;
    private bool _enableActionData;
    private int _progressPhase;
    private readonly double _scriptPhaseWeight;

    public InstallProgressCounter() : this(0.3)
    {
    }

    public InstallProgressCounter(double scriptPhaseWeight)
    {
        if (scriptPhaseWeight is not (>= 0 and <= 1))
        {
            throw new ArgumentOutOfRangeException(nameof(scriptPhaseWeight));
        }

        _scriptPhaseWeight = scriptPhaseWeight;
    }

    /// <summary>
    /// Gets a number between 0 and 1 that indicates the overall installation progress.
    /// </summary>
    public double Progress { get; private set; }

    public void ProcessMessage(InstallMessage messageType, Record messageRecord)
    {
        // This MSI progress-handling code was mostly borrowed from burn and translated from C++ to C#.

        switch (messageType)
        {
            case InstallMessage.ActionStart:
                if (_enableActionData)
                {
                    _enableActionData = false;
                }
                break;

            case InstallMessage.ActionData:
                if (_enableActionData)
                {
                    if (_moveForward)
                    {
                        _completed += _step;
                    }
                    else
                    {
                        _completed -= _step;
                    }

                    UpdateProgress();
                }
                break;

            case InstallMessage.Progress:
                ProcessProgressMessage(messageRecord);
                break;
        }
    }

    private void ProcessProgressMessage(Record progressRecord)
    {
        // This MSI progress-handling code was mostly borrowed from burn and translated from C++ to C#.

        if (progressRecord == null || progressRecord.FieldCount == 0)
        {
            return;
        }

        var fieldCount = progressRecord.FieldCount;
        var progressType = progressRecord.GetInteger(1);
        var progressTypeString = string.Empty;

        switch (progressType)
        {
            case 0: // Master progress reset
                if (fieldCount < 4)
                {
                    return;
                }

                _progressPhase++;

                _total = progressRecord.GetInteger(2);
                if (_progressPhase == 1)
                {
                    // HACK!!! this is a hack courtesy of the Windows Installer team. It seems the script planning phase
                    // is always off by "about 50".  So we'll toss an extra 50 ticks on so that the standard progress
                    // doesn't go over 100%.  If there are any custom actions, they may blow the total so we'll call this
                    // "close" and deal with the rest.
                    _total += 50;
                }

                _moveForward = (progressRecord.GetInteger(3) == 0);
                _completed = (_moveForward ? 0 : _total); // if forward start at 0, if backwards start at max
                _enableActionData = false;

                UpdateProgress();
                break;

            case 1: // Action info
                if (fieldCount < 3)
                {
                    return;
                }

                if (progressRecord.GetInteger(3) == 0)
                {
                    _enableActionData = false;
                }
                else
                {
                    _enableActionData = true;
                    _step = progressRecord.GetInteger(2);
                }
                break;

            case 2: // Progress report
                if (fieldCount < 2 || _total == 0 || _progressPhase == 0)
                {
                    return;
                }

                if (_moveForward)
                {
                    _completed += progressRecord.GetInteger(2);
                }
                else
                {
                    _completed -= progressRecord.GetInteger(2);
                }

                UpdateProgress();
                break;

            case 3: // Progress total addition
                _total += progressRecord.GetInteger(2);
                break;
        }
    }

    private void UpdateProgress()
    {
        if (_progressPhase < 1 || _total == 0)
        {
            Progress = 0;
        }
        else if (_progressPhase == 1)
        {
            Progress = _scriptPhaseWeight * Math.Min(_completed, _total) / _total;
        }
        else if (_progressPhase == 2)
        {
            Progress = _scriptPhaseWeight +
                       (1 - _scriptPhaseWeight) * Math.Min(_completed, _total) / _total;
        }
        else
        {
            Progress = 1;
        }
    }
}