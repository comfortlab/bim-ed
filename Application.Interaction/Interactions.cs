﻿using System;

namespace CoLa.BimEd.Application.Interaction;

public class Interactions
{
    public CalculateInteraction Calculate { get; } = new();
    public ViewInteraction View { get; } = new();

    public class CalculateInteraction
    {
        public Action<object> CalculateOperatingMode { get; set; }
        public Action CalculateAllOperatingModes { get; set; }
    }
    
    public class ViewInteraction
    {
        public DialogInteraction Dialog { get; } = new();
        public ElectricalDiagramInteraction ElectricalDiagram { get; } = new();
        
        public class DialogInteraction
        {
            public Action ShowDistributionSystems { get; set; }
        }
        
        public class ElectricalDiagramInteraction
        {
            public Action CreateSource { get; set; }
            public Action CreateSwitchboard { get; set; }
            public Action CreateTransformer { get; set; }
            public Action ShowDistributionSystems { get; set; }
            public Action ShowLoadClassifications { get; set; }
            public Action UpdateProperties { get; set; }
            public Action<object> ConnectNode { get; set; }
            public Action<object> PickNode { get; set; }
            public Action<object> SelectNode { get; set; }
            public Action<object> ShowElectricalEquipmentDetails { get; set; }
            public Action<object> UpdateChainDiagram { get; set; }
            public Action<object> UpdateNode { get; set; }
        }
    }
}

