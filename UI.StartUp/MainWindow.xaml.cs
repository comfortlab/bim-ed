﻿using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.StartUp;

public partial class MainWindow
{
    public MainWindow()
    {
        InitializeComponent();
        this.SetPositionAndSizes(0.6);
        DataContext = new StartUpViewModel();
    }
}