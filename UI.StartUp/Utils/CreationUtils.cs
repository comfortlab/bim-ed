﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.UI.StartUp.Utils;

public class CreationUtils
{
    private readonly IServiceProvider _serviceProvider;
    public int Id => _id--;

    private int _id;

    public CreationUtils(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

    public List<DistributionSystemProxy> CreateDistributionSystems()
    {
        var distributionSystems = _serviceProvider.Get<Repository<DistributionSystemProxy>>();
        var voltageTypes = _serviceProvider.Get<Repository<VoltageTypeProxy>>();
        var voltageType230 = new VoltageTypeProxy { RevitId = Id, Name = "230 V", ActualValue = 230, MinValue = 0, MaxValue = 230 };
        var voltageType400 = new VoltageTypeProxy { RevitId = Id, Name = "400 V", ActualValue = 400, MinValue = 0, MaxValue = 400 };
        var voltageType10000 = new VoltageTypeProxy { RevitId = Id, Name = "10000 V", ActualValue = 10000, MinValue = 0, MaxValue = 10000 };
        var distributionSystem230 = new DistributionSystemProxy
        {
            RevitId = Id, Name = "230 V", PhasesNumber = PhasesNumber.One, NumWires = 2,
            LineToGroundVoltage = voltageType230
        };
        var distributionSystem400 = new DistributionSystemProxy
        {
            RevitId = Id, Name = "400 V", PhasesNumber = PhasesNumber.Three, NumWires = 4,
            ElectricalPhaseConfiguration = ElectricalPhaseConfigurationProxy.Wye, LineToGroundVoltage = voltageType230,
            LineToLineVoltage = voltageType400
        };
        var distributionSystem10000 = new DistributionSystemProxy
        {
            RevitId = Id, Name = "10000 V", PhasesNumber = PhasesNumber.Three, NumWires = 4,
            ElectricalPhaseConfiguration = ElectricalPhaseConfigurationProxy.Wye,
            LineToLineVoltage = voltageType10000
        };
        distributionSystems.Clear();
        distributionSystems.Add(distributionSystem230);
        distributionSystems.Add(distributionSystem400);
        distributionSystems.Add(distributionSystem10000);
        voltageTypes.Clear();
        voltageTypes.Add(voltageType230);
        voltageTypes.Add(voltageType400);
        voltageTypes.Add(voltageType10000);
        
        return distributionSystems;
    }
    
    public void AddSwitchboardUnits(
        ElectricalEquipmentProxy baseEquipment,
        int rows, int columns,
        Func<int> newId)
    {
        if (columns == 0)
            return;
        
        for (var i = 0; i < rows; i++)
        {
            var childSwitchboardUnit = SwitchBoardUnit.CreateSingle(newId(), $"{baseEquipment.Name}-{i + 1}", baseEquipment.DistributionSystem);
            
            childSwitchboardUnit
                .ConnectTo(new ElectricalSystemProxy(newId(), i.ToString(), baseEquipment.DistributionSystem))
                .ConnectTo(baseEquipment);
            
            AddSwitchboardUnits(childSwitchboardUnit, rows, columns - 1, newId);
        }
    }
}