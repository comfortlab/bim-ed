﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.ProgressBar;
using CoLa.BimEd.UI.Cables.Views;
using CoLa.BimEd.UI.ElectricalDiagram;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.ElectricalDiagram.Views;
using CoLa.BimEd.UI.ElectricalSetting.Windows;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.ElectricalSources;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears.Diagrams.RM6;
using CoLa.BimEd.UI.EquipmentSelector.Windows;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Framework.Utils;
using CoLa.BimEd.UI.RtmDemandFactors;
using CoLa.BimEd.UI.StartUp.SelectAndConfig;
using CoLa.BimEd.UI.StartUp.Utils;
using Localization = CoLa.BimEd.Infrastructure.Framework.Localization;

namespace CoLa.BimEd.UI.StartUp;

public class StartUpViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly CreationUtils _creationUtils;

    public StartUpViewModel()
    {
        _serviceProvider = Bootstrapper.Build();
        _serviceProvider.Get<Localization>().SetLocalization(Localization.RuRu);
        _creationUtils = new CreationUtils(_serviceProvider);
    }

    public Command ProgressBarCommand => new(() =>
    {
        var progressBar = _serviceProvider.Get<ProgressBarController>();
        progressBar.StartProcess(100);

        for (var i = 0; i < 100; i++)
        {
            Thread.Sleep(20);
            progressBar.Next();
        }
    });

    public Command CablesCommand => new(() => new CablesWindow().ShowDialog());
    public Command DiagramCommand => new(() =>
    {
        var distributionSystems = _creationUtils.CreateDistributionSystems();
        var distributionSystem400 = distributionSystems.First(x => x.LineToLineVoltage?.ActualValue.IsEqualTo(400) ?? false);
        var distributionSystem10000 = distributionSystems.First(x => x.LineToLineVoltage?.ActualValue.IsEqualTo(10000) ?? false);
        var network10000 = new ElectricalNetwork(_creationUtils.Id, "~10000V", distributionSystem10000);
        var network10000_2 = new ElectricalNetwork(_creationUtils.Id, "~10000V", distributionSystem10000);
        var transformer = new Transformer(_creationUtils.Id, "T-1", distributionSystem10000, distributionSystem400);
        var transformer_2 = new Transformer(_creationUtils.Id, "T-2", distributionSystem10000, distributionSystem400);
        transformer.ConnectTo(network10000);
        transformer_2.ConnectTo(network10000);
        
        var transformerChildSystem = new ElectricalSystemProxy(_creationUtils.Id, "1", distributionSystem400);
        transformerChildSystem.ConnectTo(transformer);
        SwitchBoardUnit.CreateSingle(_creationUtils.Id, "ЩР-1", distributionSystem400).ConnectTo(transformerChildSystem);
        SwitchBoardUnit.CreateSingle(_creationUtils.Id, "ЩР-2", distributionSystem400).ConnectTo(transformerChildSystem);
        SwitchBoardUnit.CreateSingle(_creationUtils.Id, "ЩР-3", distributionSystem400).ConnectTo(transformerChildSystem);
        
        var transformerChildSystem_2 = new ElectricalSystemProxy(_creationUtils.Id, "1", distributionSystem400);
        transformerChildSystem_2.ConnectTo(transformer_2);
        SwitchBoardUnit.CreateSingle(_creationUtils.Id, "ЩР-4", distributionSystem400).ConnectTo(transformerChildSystem_2);
        
        
        var operatingModes = _serviceProvider.Get<OperatingModes>();
        while (operatingModes.Count > 1) operatingModes.RemoveAt(operatingModes.Count - 1);
        var faultOperatingMode = new OperatingMode("Fault");
        operatingModes.Add(faultOperatingMode);
        
        _serviceProvider.Get<List<ElectricalNetwork>>().ReplaceAll(new []
        {
            network10000,
            network10000_2
        });
        _serviceProvider.Get<ElectricalDiagramWindow>().ShowDialog();
    });
    public Command DiagramGridCommand => new(() =>
    {
        var distributionSystems = _creationUtils.CreateDistributionSystems();
        var distributionSystem230 = distributionSystems.First(x => x.LineToGroundVoltage?.ActualValue.IsEqualTo(230) ?? false);
        var distributionSystem400 = distributionSystems.First(x => x.LineToLineVoltage?.ActualValue.IsEqualTo(400) ?? false);
        var distributionSystem10000 = distributionSystems.First(x => x.LineToLineVoltage?.ActualValue.IsEqualTo(10000) ?? false);
        var network230 = new ElectricalNetwork(_creationUtils.Id, "~230V", distributionSystem230);
        var network400 = new ElectricalNetwork(_creationUtils.Id, "~400V", distributionSystem400);
        var network10000 = new ElectricalNetwork(_creationUtils.Id, "~10000V", distributionSystem10000);
        var switchboard1 = new SwitchBoard(_creationUtils.Id, "DP1", distributionSystem400);
        var switchboard2 = new SwitchBoard(_creationUtils.Id, "DP2", distributionSystem400);
        var switchboard3 = new SwitchBoard(_creationUtils.Id, "DP3", distributionSystem400);
        _creationUtils.AddSwitchboardUnits(switchboard1.FirstUnit, 3, 3, () => _creationUtils.Id);
        _creationUtils.AddSwitchboardUnits(switchboard2.FirstUnit, 3, 3, () => _creationUtils.Id);
        _creationUtils.AddSwitchboardUnits(switchboard3.FirstUnit, 3, 3, () => _creationUtils.Id);
        switchboard1.FirstUnit.ConnectTo(network400);
        switchboard2.FirstUnit.ConnectTo(network400);
        switchboard3.FirstUnit.ConnectTo(network400);
        
        _serviceProvider.Get<List<ElectricalNetwork>>().ReplaceAll(new []
        {
            network230,
            network400,
            network10000,
        });
        
        ShowWindow(new NodesGridView { DataContext = _serviceProvider.Get<NodesGridViewModel>() });
    });
    public Command DistributionSystemsCommand => new(() =>
    {
        _creationUtils.CreateDistributionSystems();
        _serviceProvider.Get<DistributionSystemsWindow>().ShowDialog();
    });

    public Command LoadClassificationsCommand => new(() =>
    {
        var loadClassifications = _serviceProvider.Get<Repository<LoadClassificationProxy>>();
        var demandFactors = _serviceProvider.Get<Repository<DemandFactorProxy>>();
        var demandFactorConstant1 = new DemandFactorProxy(_creationUtils.Id, "Constant 1");
        var demandFactorConstant2 = new DemandFactorProxy(_creationUtils.Id, "Constant 2");
        var demandFactorConstant3 = new DemandFactorProxy(_creationUtils.Id, "Constant 3");
        var loadClassification1 = new LoadClassificationProxy(_creationUtils.Id, "Lighting", demandFactorConstant1);
        var loadClassification2 = new LoadClassificationProxy(_creationUtils.Id, "Power", demandFactorConstant3);
        demandFactors.Add(demandFactorConstant1);
        demandFactors.Add(demandFactorConstant2);
        demandFactors.Add(demandFactorConstant3);
        loadClassifications.Add(loadClassification1);
        loadClassifications.Add(loadClassification2);
        _serviceProvider.Get<LoadClassificationsWindow>().ShowDialog();
    });

    public Command MediumVoltageRm6Diagrams => new(() => ShowWindow(new RM6Preview()));
    public Command MediumVoltageSwitchGearConstructor => new(() =>
    {
        var switchGearSeries = _serviceProvider.Get<Repository<SwitchGearSeries>>().FirstOrDefault(x => x.Guid == Guids.Products.SwitchGears.RM6.Series);
        ShowWindow(new SwitchGearConstructor
        {
            DataContext = new SwitchGearConstructorViewModel(_serviceProvider, switchGearSeries)
        });
    });

    public Command OperatingModesCommand => new(() =>
    {
        var operatingModes = _serviceProvider.Get<OperatingModes>();
        var operatingModeElements = _serviceProvider.Get<OperatingModeElements>();
        while (operatingModes.Count > 1) operatingModes.RemoveAt(operatingModes.Count - 1);
        var faultOperatingMode = new OperatingMode("Fault");
        operatingModes.Add(faultOperatingMode);
        operatingModeElements.Elements.Add(new LoadClassificationProxy(_creationUtils.Id, "Load Classification 1"));
        operatingModeElements.Elements.Add(new LoadClassificationProxy(_creationUtils.Id, "Load Classification 2"));
        operatingModeElements.Elements.Add(new LoadClassificationProxy(_creationUtils.Id, "Load Classification 3"));
        faultOperatingMode.DisabledElements.Add(operatingModeElements.Elements[0].RevitId);
        faultOperatingMode.DisabledElements.Add(operatingModeElements.Elements[2].RevitId);
        _serviceProvider.Get<OperatingModesWindow>().ShowDialog();
    });

    public Command CreateSourceViewCommand => new(() =>
    {
        _creationUtils.CreateDistributionSystems();
        var viewModel = _serviceProvider.Get<SourceViewModel>();
        var window = new Window
        {
            Content = new CreateSourceView { DataContext = viewModel },
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
        };
        viewModel.Close = window.Close;
        window.ShowDialog();
    });

    public Command TransformerViewCommand => new(() =>
    {
        _creationUtils.CreateDistributionSystems();
        var transformers = _serviceProvider.Get<Repository<Transformer>>();
        transformers.Add(new Transformer(1, "T1"));
        transformers.Add(new Transformer(2, "T2"));
        _serviceProvider.Get<CreateTransformerWindow>().ShowDialog();
    });

    public Command RepositoryCablesCommand => new(() =>
    {
        var cableSeries = _serviceProvider.Get<Repository<CableSeries>>();
        ShowText($"Cables:{cableSeries.JoinToString()}");
    });

    public Command RepositoryMaterialsCommand => new(() =>
    {
        var materials = _serviceProvider.Get<Repository<Material>>();
        ShowText($"Materials:{materials.JoinToString()}");
    });

    public Command RtmDemandFactorsCommand => new(() => _serviceProvider.Get<RtmDemandFactorsWindow>().ShowDialog());
    public Command AuthCommand => new(() => _serviceProvider.Get<BrowserLauncher>().StartAuth());
    public Command NewSelectAndConfigCommand => new(() => _serviceProvider.Get<SelectAndConfigLauncher>().Start(0));
    public Command ConfigSelectAndConfigCommand => new(() => _serviceProvider.Get<SelectAndConfigLauncher>().Start(1));

    private static void ShowText(string text)
    {
        new Window
        {
            Content = new TextBox
            {
                Text = text,
                IsReadOnly = true,
            },
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
        }.ShowDialog();
    }

    private static void ShowWindow(FrameworkElement frameworkElement)
    {
        var window = new Window
        {
            Content = frameworkElement,
            WindowStartupLocation = WindowStartupLocation.CenterScreen,
        };
        
        window.SetPositionAndSizes(0.9);
        window.ShowDialog();
    }
}