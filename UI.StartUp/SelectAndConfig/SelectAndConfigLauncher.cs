﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.UI.StartUp.Utils;
using Newtonsoft.Json;
using Localization = CoLa.BimEd.Infrastructure.Framework.Localization;

namespace CoLa.BimEd.UI.StartUp.SelectAndConfig;

public class SelectAndConfigLauncher
{
    private readonly SeApiHttpClient _seApiHttpClient;
    private readonly SeApi.Selectors _selectors;
    private readonly CreationUtils _creationUtils;

    public SelectAndConfigLauncher(
        IServiceProvider serviceProvider,
        SeApiHttpClient seApiHttpClient,
        SeApi.Selectors selectors)
    {
        _seApiHttpClient = seApiHttpClient;
        _selectors = selectors;
        _creationUtils = new CreationUtils(serviceProvider);
    }

    private Process _process;

    private static string SelectAndConfigExePath = "../../../../UI.SelectAndConfig/bin/x64/Debug/net48/UI.SelectAndConfig.exe";

    public async void Start(int mode)
    {
        // TODO https://comfortlab.atlassian.net/browse/BED-190

        var distributionSystems = _creationUtils.CreateDistributionSystems();
        var distributionSystem = distributionSystems.First();
        var electricalSystem = new ElectricalSystemProxy(1, "1", distributionSystem)
        {
            EstimatedPowerParameters =
            {
                Current = 40
            }
        };

        var selector = _selectors.GetById(SeApi.Selectors.Acti9_iSW);
        var configBuilder = new SelectorConfigBuilder(_seApiHttpClient, new Localization(Localization.RuRu));
        var config = await configBuilder.Build(
            technology: SeApi.SelectorConfigs.TechnologyKb,
            selector.Id,
            electricalSystem,
            selector.CriteriaConverters.ToArray());

        var configJson = JsonConvert.SerializeObject(config);
        
        var fileInfo = new FileInfo(SelectAndConfigExePath);
        if (!fileInfo.Exists)
        {
            MessageBox.Show($"SelectAndConfig.Exe at path {fileInfo.FullName} not found");
            return;
        }

        _process = new Process();
        //_process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.ErrorDialog = true;
        //popupProcess.StartInfo.LoadUserProfile = true;
        _process.StartInfo.WorkingDirectory = fileInfo.DirectoryName;
        _process.StartInfo.FileName = fileInfo.FullName;
        _process.StartInfo.Arguments = mode switch
        {
            1 => string.Join(" ", mode.ToString(), configJson.Replace(' ', '·').Replace('\"', '`')),
            _ => string.Join(" ", mode.ToString(), selector.Id)
        };
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.CreateNoWindow = false;
        _process.ErrorDataReceived += Process_ErrorDataReceived;
        _process.EnableRaisingEvents = true;
        _process.Exited += Process_Exited;

        var started = _process.Start();
        if (!started)
        {
            Debug.WriteLine("Cannot start {SelectAndConfigExePath}", SelectAndConfigExePath);
        }

        var waited = _process.WaitForInputIdle(-1);
        if (!waited)
        {
            Debug.WriteLine("Cannot start {SelectAndConfigExePath}", SelectAndConfigExePath);
        }
    }

    private static Thread _serverThread = null;

    private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        var data = e.Data;
    }

    private void Process_Exited(object sender, EventArgs e)
    {
        _process.Exited -= Process_Exited;

        var errorMessage = _process.StandardError.ReadToEnd();

        if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Contains("ERROR"))
            Debug.WriteLine(errorMessage);  // ошибка
        
        switch (_process.ExitCode)
        {
            case 200:
                var reader = _process.StandardOutput;
                var output = reader.ReadToEnd();
                // добавить чистку output от начального мусора, чтобы получить валидный json - что-то вроде
                var startjson = output.IndexOf('{');
                output = output.Substring(startjson);
                MessageBox.Show(output);
                break;
            
            case 0:
                Debug.WriteLine("user closed application");
                break;
            
            default:
                Debug.WriteLine($"Exit code {_process.ExitCode}");
                break;
        }
    }
}

public class BrowserLauncher
{
    private readonly AppPaths _appPaths;
    private Process _process;

    public BrowserLauncher(AppPaths appPaths)
    {
        _appPaths = appPaths;
    }

    public void StartAuth()
    {
        var browserAppFileInfo = new FileInfo(_appPaths.BrowserApplicationPath);
        
        if (!browserAppFileInfo.Exists)
        {
            MessageBox.Show($"{browserAppFileInfo.FullName} not found");
            return;
        }
        
        _process = new Process();
        //_process.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.ErrorDialog = true;
        //popupProcess.StartInfo.LoadUserProfile = true;
        _process.StartInfo.WorkingDirectory = browserAppFileInfo.DirectoryName;
        _process.StartInfo.FileName = browserAppFileInfo.FullName;
        _process.StartInfo.Arguments = "1";
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.CreateNoWindow = false;
        _process.ErrorDataReceived += Process_ErrorDataReceived;
        _process.EnableRaisingEvents = true;
        _process.Exited += Process_Exited;

        var started = _process.Start();
        if (!started)
        {
            Debug.WriteLine("Cannot start {SelectAndConfigExePath}", _appPaths.BrowserApplicationPath);
        }

        var waited = _process.WaitForInputIdle(-1);
        if (!waited)
        {
            Debug.WriteLine("Cannot start {SelectAndConfigExePath}", _appPaths.BrowserApplicationPath);
        }
    }

    private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        var data = e.Data;
    }

    private void Process_Exited(object sender, EventArgs e)
    {
        _process.Exited -= Process_Exited;

        var errorMessage = _process.StandardError.ReadToEnd();

        if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Contains("ERROR"))
            Debug.WriteLine(errorMessage);  // ошибка
        
        switch (_process.ExitCode)
        {
            case 200:
                var reader = _process.StandardOutput;
                var output = reader.ReadToEnd();
                // добавить чистку output от начального мусора, чтобы получить валидный json - что-то вроде
                var startjson = output.IndexOf('{');
                output = output.Substring(startjson);
                MessageBox.Show(output);
                break;
            
            case 0:
                Debug.WriteLine("user closed application");
                break;
            
            default:
                Debug.WriteLine($"Exit code {_process.ExitCode}");
                break;
        }
    }
}