﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.UI.StartUp.Mocks;

internal class MockRepository<T> : Repository<T> where T : class
{
    public override void Load() { }
    public override void Save() { }
    public override void Save(T entity) { }
}
internal class MockVirtualElementsRepository : Repository<VirtualElements>
{
    public MockVirtualElementsRepository() => Add(new VirtualElements());
    public override void Load() { }
    public override void Save() { }
    public override void Save(VirtualElements entity) { }
}