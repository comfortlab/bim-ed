﻿using System;
using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.BusinessLogic.Model.Services;
using CoLa.BimEd.BusinessLogic.Model.Utils;
using CoLa.BimEd.DataAccess.Model.Mapping;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.DataAccess;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Net;
using CoLa.BimEd.Infrastructure.Framework.Net.Proxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.ProgressBar;
using CoLa.BimEd.UI.Cables.ViewModels;
using CoLa.BimEd.UI.ElectricalDiagram;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.OperatingModeSetting;
using CoLa.BimEd.UI.ElectricalSetting.Windows;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Windows;
using CoLa.BimEd.UI.RtmDemandFactors;
using CoLa.BimEd.UI.RtmDemandFactors.DataAccess;
using CoLa.BimEd.UI.RtmDemandFactors.Model;
using CoLa.BimEd.UI.RtmDemandFactors.ViewModels;
using CoLa.BimEd.UI.StartUp.Mocks;
using CoLa.BimEd.UI.StartUp.SelectAndConfig;

namespace CoLa.BimEd.UI.StartUp;

public static class Bootstrapper
{
    public static IServiceProvider Build()
    {
        var serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton(x => x);
        serviceCollection.AddSingleton<ProgressBarController>();
        serviceCollection.AddSingleton<ProgressBarWindow>();

        const string applicationName = "BIM Electrical Design";

        serviceCollection.AddSingleton(_ => new AppInfo(applicationName));
        serviceCollection.AddSingleton(_ => new AppPaths(
            applicationName,
            System.IO.Path.Combine(AssemblyUtils.GetSolutionDirectoryInfo().FullName, @"UI.BrowserApplication\bin\x64\Debug\CoLa.BimEd.UI.BrowserApplication.exe"),
            2016));
        serviceCollection.AddSingleton(_ => new Localization(Localization.RuRu));
        serviceCollection.AddSingleton<Logger>();
        serviceCollection.AddSingleton<OperatingElements>();
        serviceCollection.AddSingleton<HttpClientAccessor>();
        serviceCollection.AddSingleton<ProxyStorage>();
        serviceCollection.AddSingleton<AppHttpClient>();
        serviceCollection.AddSingleton<SeApiHttpClient>();

        // Repositories

        serviceCollection.AddSingleton<Repository<RtmData>, RtmDemandFactorRepository>();
        serviceCollection.AddSingleton<Repository<CableSeries>, CableSeriesRepository>();
        serviceCollection.AddSingleton<Repository<LoadClassificationDirectory>, LoadClassificationDirectoryRepository>();
        serviceCollection.AddSingleton<Repository<Material>, MaterialRepository>();
        serviceCollection.AddSingleton<Repository<SwitchGearFunction>, SwitchGearFunctionRepository>();
        serviceCollection.AddSingleton<Repository<SwitchGearSeries>, SwitchGearSeriesRepository>();
        serviceCollection.AddSingleton<Repository<TransformerSeries>, TransformerSeriesRepository>();
        serviceCollection.AddSingleton(_ => SwitchGearComponentsInitializer.Initialize());
        
        // Elements

        serviceCollection.AddTransient<Transformer>();
        
        // Mapping

        serviceCollection.AddSingleton(_ => new MapperConfiguration(cfg =>
        {
            // CableRouting
            // cfg.AddProfile(new CableRoutingProfile(x));

            // Model
            cfg.AddProfile<CommonProfile>();
            cfg.AddProfile<CableProfile>();
            // cfg.AddProfile<ElectricalProfile>();
            // cfg.AddProfile<ElectricalSourceProfile>();
            cfg.AddProfile<ProductProfile>();
            // cfg.AddProfile<TransformerProfile>();
        }));
        serviceCollection.AddSingleton<IMapper>(x => new Mapper(x.Get<MapperConfiguration>(), x.GetService));

        // Repositories
        
        serviceCollection.AddSingleton(_ => OperatingModes.CreateDefault());
        serviceCollection.AddSingleton<OperatingModeElements>();
        serviceCollection.AddSingleton<Repository<DemandFactorProxy>, MockRepository<DemandFactorProxy>>();
        serviceCollection.AddSingleton<Repository<DistributionSystemProxy>, MockRepository<DistributionSystemProxy>>();
        serviceCollection.AddSingleton<Repository<ElectricalSystemProxy>, MockRepository<ElectricalSystemProxy>>();
        serviceCollection.AddSingleton<Repository<LoadClassificationProxy>, MockRepository<LoadClassificationProxy>>();
        serviceCollection.AddSingleton<Repository<OperatingModes>, MockRepository<OperatingModes>>();
        serviceCollection.AddSingleton<Repository<VoltageTypeProxy>, MockRepository<VoltageTypeProxy>>();
        serviceCollection.AddSingleton<Repository<SwitchBoardUnit>, MockRepository<SwitchBoardUnit>>();
        serviceCollection.AddSingleton<Repository<SwitchGearUnit>, MockRepository<SwitchGearUnit>>();
        serviceCollection.AddSingleton<Repository<Transformer>, MockRepository<Transformer>>();
        serviceCollection.AddSingleton<Repository<VirtualElements>, MockVirtualElementsRepository>();
        serviceCollection.AddTransient(x => x.Get<Repository<VirtualElements>>().First());
        serviceCollection.AddTransient(x => x.Get<VirtualElements>().Generators);
        serviceCollection.AddTransient(x => x.Get<VirtualElements>().Networks);

        serviceCollection.AddTransient(x => x.Get<Repository<Transformer>>().Concat(x.Get<VirtualElements>().Transformers).ToArray());
        
        serviceCollection.AddSingleton<VirtualElementsUtils>();
        
        // serviceCollection.AddTransient<IRepository<Cable>, CableRepository>();
        // serviceCollection.AddTransient<IRepository<CableSeries>, CableSeriesRepository>();
        // serviceCollection.AddTransient<IRepository<DistributionSystemProxy>, OldDistributionSystemRepository>();
        // serviceCollection.AddTransient<IRepository<ElectricalSource>, ElectricalSourceRepository>();
        // serviceCollection.AddTransient<IRepository<OperatingMode>, OperatingModeRepository>();
        // serviceCollection.AddTransient<IRepository<Switchboard>, SwitchboardRepository>();
        // serviceCollection.AddTransient<IRepository<SwitchboardUnit>, SwitchboardUnitRepository>();
        // serviceCollection.AddTransient<IRepository<Transformer>, TransformerRepository>();
        // serviceCollection.AddTransient<IRepository<TransformerSeries>, TransformerSeriesRepository>();
        // serviceCollection.AddTransient<IRepository<VoltageTypeProxy>, OldVoltageTypeRepository>();

        serviceCollection.AddSingleton<Interactions>();
        serviceCollection.AddSingleton<NodeSelection>();
        serviceCollection.AddSingleton<DiagramClipboard>();
        
        serviceCollection.AddTransient<CablesViewModel>();
        serviceCollection.AddTransient<CreateTransformerWindow>();
        serviceCollection.AddTransient<ChainDiagramViewModel>();
        serviceCollection.AddTransient<NodesGridViewModel>();
        serviceCollection.AddTransient<DistributionSystemsViewModel>();
        serviceCollection.AddTransient<DistributionSystemsWindow>();
        serviceCollection.AddTransient<DiagramViewModel>();
        serviceCollection.AddTransient<ElectricalDiagramWindow>();
        serviceCollection.AddTransient<ImportLoadClassificationsViewModel>();
        serviceCollection.AddTransient<LoadClassificationsViewModel>();
        serviceCollection.AddTransient<LoadClassificationsWindow>();
        serviceCollection.AddTransient<OperatingModesViewModel>();
        serviceCollection.AddTransient<OperatingModesWindow>();
        serviceCollection.AddTransient<OperatingModesWindow>();
        serviceCollection.AddTransient<RtmDemandFactorsViewModel>();
        serviceCollection.AddTransient<RtmDemandFactorsWindow>();
        serviceCollection.AddTransient<SourceViewModel>();
        serviceCollection.AddTransient<SwitchGearViewModel>();
        serviceCollection.AddTransient<SwitchGearSeriesSelectorViewModel>();

        serviceCollection.AddSingleton<BrowserLauncher>();
        serviceCollection.AddSingleton<SeApi.Selectors>();
        serviceCollection.AddSingleton<SelectAndConfigLauncher>();

        serviceCollection.AddTransient(_ =>
        {
            return new SelectAndConfigSetting<SwitchBoardUnit>
            {
                BreakNeutralCore = false,
                SwitchSettings = new SelectorProductSettings(new[]
                {
                    SeApi.Selectors.Acti9_iSW,
                    SeApi.Selectors.ComPact_NSXm_NA,
                    SeApi.Selectors.MasterPact_MTZ1_SD,
                    SeApi.Selectors.MasterPact_MTZ2_SD,
                    SeApi.Selectors.MasterPact_MTZ3_SD,
                }),
            };
        });

        serviceCollection.AddTransient(_ =>
        {
            return new SelectAndConfigSetting<ElectricalSystemProxy>
            {
                NeutralCore = true,
                BreakNeutralCore = true,
                EquipmentSelectionMode = EquipmentSelectionMode.ByLoad,
                MinCoreSection = 1.5,
                SwitchSettings = new SelectorProductSettings(new[]
                {
                    SeApi.Selectors.Acti9_iC60,
                    SeApi.Selectors.Acti9_C120,
                    SeApi.Selectors.ComPact_NSXm,
                    SeApi.Selectors.ComPact_NSX,
                    SeApi.Selectors.MasterPact_MTZ1_CB,
                    SeApi.Selectors.MasterPact_MTZ2_CB,
                    SeApi.Selectors.MasterPact_MTZ3_CB,
                }),
            };
        });

        return DI.ServiceProvider = serviceCollection.BuildServiceProvider();
    }
}