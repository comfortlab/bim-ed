﻿namespace CoLa.BimEd.Infrastructure.Framework.Prototype;

public interface IPropertyPrototype<in T> where T : class
{
    void PullProperties(T prototype);
}