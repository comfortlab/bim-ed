﻿namespace CoLa.BimEd.Infrastructure.Framework.Prototype;

public interface IPrototype<out T>
{
    T Clone();
}