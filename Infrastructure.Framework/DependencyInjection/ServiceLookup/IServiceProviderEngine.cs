﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal interface IServiceProviderEngine : IDisposable, IServiceProvider
{
    IServiceScope RootScope { get; }
}