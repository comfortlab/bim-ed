﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal interface IServiceCallSite
{
    Type ServiceType { get; }
    Type ImplementationType { get; }
    CallSiteKind Kind { get; }
}