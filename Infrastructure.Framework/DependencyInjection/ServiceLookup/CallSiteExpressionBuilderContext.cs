﻿using System.Linq.Expressions;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal class CallSiteExpressionBuilderContext
{
    public ParameterExpression ScopeParameter { get; set; }
    public bool RequiresResolvedServices { get; set; }
}