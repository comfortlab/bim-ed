using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal class ConstantCallSite : IServiceCallSite
{
    internal object DefaultValue { get; }

    public ConstantCallSite(Type serviceType, object defaultValue)
    {
        DefaultValue = defaultValue;
    }

    public Type ServiceType => DefaultValue.GetType();
    public Type ImplementationType => DefaultValue.GetType();
    public CallSiteKind Kind { get; } = CallSiteKind.Constant;
}