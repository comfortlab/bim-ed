﻿namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal enum CallSiteKind
{
    Factory,

    Constructor,

    Constant,

    IEnumerable,

    ServiceProvider,

    Scope,

    Transient,

    CreateInstance,

    ServiceScopeFactory,

    Singleton
}