using System;
using System.Runtime.CompilerServices;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal class ThrowHelper
{
    [MethodImpl(MethodImplOptions.NoInlining)]
    internal static void ThrowObjectDisposedException()
    {
        throw new ObjectDisposedException(nameof(IServiceProvider));
    }
}