﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection.ServiceLookup;

internal interface IServiceProviderEngineCallback
{
    void OnCreate(IServiceCallSite callSite);
    void OnResolve(Type serviceType, IServiceScope scope);
}