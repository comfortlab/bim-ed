﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

public interface IServiceScope : IDisposable
{
    /// <summary>
    /// The <see cref="System.IServiceProvider"/> used to resolve dependencies from the scope.
    /// </summary>
    IServiceProvider ServiceProvider { get; }
}