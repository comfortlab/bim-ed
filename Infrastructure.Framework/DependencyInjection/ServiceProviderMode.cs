﻿namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

internal enum ServiceProviderMode
{
    Dynamic,
    Runtime,
    Expressions,
    ILEmit
}