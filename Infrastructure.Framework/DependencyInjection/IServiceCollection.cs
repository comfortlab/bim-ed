﻿using System.Collections.Generic;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

public interface IServiceCollection : IList<ServiceDescriptor>
{
}