using System;
using CoLa.BimEd.Infrastructure.Framework.Logs;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

public abstract class ServiceCommand
{
    protected IServiceProvider ServiceProvider;
    protected abstract void Execute();
    public void Execute(IServiceProvider serviceProvider)
    {
        try
        {
            ServiceProvider = serviceProvider;
            Execute();
        }
        catch (Exception exception)
        {
            ServiceProvider.Get<Logger>().Error(exception);
        }
    }
}