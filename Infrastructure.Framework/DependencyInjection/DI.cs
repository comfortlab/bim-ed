using System;

namespace CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

public static class DI
{
    public static IServiceProvider ServiceProvider { get; set; }
    public static T Get<T>() where T : class =>
        ServiceProvider?.GetService(typeof(T)) as T;
    public static T Get<T>(this IServiceProvider serviceProvider) where T : class =>
        serviceProvider?.GetService(typeof(T)) as T;
    public static object Get(Type type) => ServiceProvider.GetService(type);
}