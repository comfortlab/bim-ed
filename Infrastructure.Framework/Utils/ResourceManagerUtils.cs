﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;

namespace CoLa.BimEd.Infrastructure.Framework.Utils
{
    public static class ResourceManagerUtils
    {
        public static Dictionary<string, string> GetDictionaryValues(ResourceManager resourceManager)
        {
            // Без этого возникают ошибки при получении resourceSetInvariantCulture и resourceSetInvariantCulture
            resourceManager.GetString(string.Empty, CultureInfo.InvariantCulture);
            resourceManager.GetString(string.Empty, CultureInfo.CurrentCulture);

            var resourceSetInvariantCulture = resourceManager.GetResourceSet(CultureInfo.InvariantCulture, false, false);
            var resourceSetCurrentCulture = resourceManager.GetResourceSet(CultureInfo.CurrentCulture, false, false);
            var dictionary = new Dictionary<string, string>();

            foreach (DictionaryEntry dictionaryEntry in resourceSetInvariantCulture)
            {
                var key = dictionaryEntry.Key.ToString();
                var valueInvariantCulture = dictionaryEntry.Value.ToString();
                var valueCurrentCulture = resourceSetCurrentCulture.GetString(key);

                if (!string.IsNullOrEmpty(valueInvariantCulture) &&
                    !string.IsNullOrEmpty(valueCurrentCulture) &&
                    !string.Equals(valueInvariantCulture, valueCurrentCulture))
                    dictionary.Add(valueInvariantCulture, valueCurrentCulture);
            }

            return dictionary;
        }

        public static List<string> GetSupportedCulturesStrings(Type type, string name)
        {
            var resourceManager = new ResourceManager(type);

            return CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                .Where(n => resourceManager.GetResourceSet(n, true, false) != null)
                .Select(n => resourceManager.GetString(name, n))
                .Where(n => string.IsNullOrEmpty(n) == false).ToList();
        }
    }
}