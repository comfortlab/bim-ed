﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CoLa.BimEd.Infrastructure.Framework.Utils
{
    public static class EncryptionUtils
    {
        public static string EncryptForCurrentUser(string text) =>
            string.IsNullOrWhiteSpace(text) == false
                ? Convert.ToBase64String(ProtectedData.Protect(Encoding.Unicode.GetBytes(text), null, DataProtectionScope.CurrentUser))
                : string.Empty;

        public static string DecryptForCurrentUser(string text) =>
            string.IsNullOrWhiteSpace(text) == false
                ? Encoding.Unicode.GetString(ProtectedData.Unprotect(Convert.FromBase64String(text), null, DataProtectionScope.CurrentUser))
                : string.Empty;
    }
}