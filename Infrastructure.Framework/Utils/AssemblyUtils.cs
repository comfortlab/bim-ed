﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.Utils;

public static class AssemblyUtils
{
    public static Version GetApplicationVersion() =>
        new Version(GetApplicationFileVersionInfo().ProductVersion);

    public static FileVersionInfo GetApplicationFileVersionInfo() =>
        FileVersionInfo.GetVersionInfo(GetExecutingAssemblyPath());

    public static string GetExecutingAssemblyDirectory() =>
        Path.GetDirectoryName(GetExecutingAssemblyPath());

    public static string GetExecutingAssemblyPath()
    {
        var assembly = Assembly.GetCallingAssembly();
        var filePath = assembly.CodeBase.Replace(@"file:///", string.Empty);

        return filePath;
    }
    
    public static DirectoryInfo GetSolutionDirectoryInfo(string currentPath = null)
    {
        var directory = new DirectoryInfo(currentPath ?? Directory.GetCurrentDirectory());
            
        while (directory != null && !directory.GetFiles("*.sln").Any())
        {
            directory = directory.Parent;
        }
            
        return directory;
    }
}