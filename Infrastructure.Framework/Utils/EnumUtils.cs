﻿using System;
using System.ComponentModel;

namespace CoLa.BimEd.Infrastructure.Framework.Utils;

public static class EnumUtils
{
    public static string GetDescription(this Enum @enum)
    {
        var type = @enum.GetType();
        var memInfo = type.GetMember(@enum.ToString());

        if (memInfo.Length <= 0)
            return @enum.ToString();

        var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

        return attrs.Length > 0
            ? ((DescriptionAttribute)attrs[0]).Description
            : @enum.ToString();
    }

    public static T GetValueByName<T>(string name)
    {
        var type = typeof(T);

        if (!type.IsEnum)
            throw new InvalidOperationException();

        foreach (var fieldInfo in type.GetFields())
        {
            if (fieldInfo.Name == name)
                return (T)fieldInfo.GetValue(null);
        }

        throw new ArgumentException($"Not found: {name}", nameof(name));
    }

    public static T GetValueByDescription<T>(string description)
    {
        var type = typeof(T);

        if (!type.IsEnum)
            throw new InvalidOperationException();

        foreach (var fieldInfo in type.GetFields())
        {
            var attribute = (DescriptionAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute));

            if (attribute != null)
            {
                if (attribute.Description == description)
                    return (T)fieldInfo.GetValue(null);
            }
            else
            {
                if (fieldInfo.Name == description)
                    return (T)fieldInfo.GetValue(null);
            }
        }

        throw new ArgumentException($"Not found: {description}", nameof(description));
    }

    public static T ConvertInt32ToEnum<T>(int source) where T : Enum
    {
        var result = (T)Enum.ToObject(typeof(T), source);

        if (!Enum.IsDefined(typeof(T), result))
        {
            throw new ArgumentException($"{source} is not an underlying value of the {nameof(T)} enumeration");
        }

        return result;
    }
}