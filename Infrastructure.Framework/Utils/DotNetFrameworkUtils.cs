﻿using System;
using Microsoft.Win32;

namespace CoLa.BimEd.Infrastructure.Framework.Utils;

public class DotNetFrameworkUtils
{
    public static Version GetVersion()
    {
        using var ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
            .OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\");
        var releaseKey = Convert.ToInt32(ndpKey?.GetValue("Release"));
        return CheckFor45DotVersion(releaseKey);
    }

    private static Version CheckFor45DotVersion(int releaseKey)
    {
        return releaseKey switch
        {
            >= 528040 => new Version("4.8"),
            >= 461808 => new Version("4.7.2"),
            >= 461308 => new Version("4.7.1"),
            >= 460798 => new Version("4.7"),
            >= 394802 => new Version("4.6.2"),
            >= 394254 => new Version("4.6.1"),
            >= 393295 => new Version("4.6"),
            >= 379893 => new Version("4.5.2"),
            >= 378675 => new Version("4.5.1"),
            >= 378389 => new Version("4.5"),
            _ => new Version("4.4")
        };
    }
}