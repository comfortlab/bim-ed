﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace CoLa.BimEd.Infrastructure.Framework.Utils;

public static class BitmapUtils
{
    public static byte[] ToBytes(this Image image)
    {
        var converter = new ImageConverter();
        return (byte[])converter.ConvertTo(image, typeof(byte[]));
    }

    public static Bitmap ToImage(this byte[] bytes)
    {
        var converter = new ImageConverter();
        return (Bitmap)converter.ConvertFrom(bytes);
    }

    public static Bitmap ToBitmap(this BitmapImage bitmapImage)
    {
        using var outStream = new MemoryStream();
        BitmapEncoder enc = new BmpBitmapEncoder();
        enc.Frames.Add(BitmapFrame.Create(bitmapImage));
        enc.Save(outStream);
        var bitmap = new Bitmap(outStream);

        return new Bitmap(bitmap);
    }

    public static BitmapImage ToBitmapImage(this Bitmap bitmap)
    {
        using var memory = new MemoryStream();
        bitmap.Save(memory, ImageFormat.Png);
        memory.Position = 0;

        var bitmapImage = new BitmapImage();
        bitmapImage.BeginInit();
        bitmapImage.StreamSource = memory;
        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapImage.EndInit();
        bitmapImage.Freeze();

        return bitmapImage;
    }

    public static BitmapSource ToBitmapSource(this Bitmap bitmap)
    {
        if (bitmap == null)
            return null;

        return Imaging.CreateBitmapSourceFromHBitmap(
            bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
    }

    public static Bitmap ToBitmap(this BitmapSource source)
    {
        var bmp = new Bitmap
        (
            source.PixelWidth,
            source.PixelHeight,
            PixelFormat.Format32bppPArgb
        );

        var data = bmp.LockBits
        (
            new Rectangle(System.Drawing.Point.Empty, bmp.Size),
            ImageLockMode.WriteOnly,
            PixelFormat.Format32bppPArgb
        );

        source.CopyPixels
        (
            Int32Rect.Empty,
            data.Scan0,
            data.Height * data.Stride,
            data.Stride
        );

        bmp.UnlockBits(data);

        return bmp;
    }

    public static void SetDefaultResolution(this Bitmap bitmap) => bitmap.SetResolution(96, 96);
}