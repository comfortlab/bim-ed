﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CoLa.BimEd.Infrastructure.Framework.Comparer;

namespace CoLa.BimEd.Infrastructure.Framework.Utils;

public static class StringUtils
{
    // https://regex101.com/r/kV1dL6/1

    #region Единицы измерения

    public static string GetValue(this string text)
    {
        const string pattern = @"^\d+(\.\d+)?";
        return Regex.Matches(text, pattern)[0].Value;
    }

    public static string GetUnit(this string text)
    {
        const string pattern = @"^\d+(\.\d+)?\s?(.*)?";
        return Regex.Matches(text, pattern)[0].Groups[2].Value;
    }

    public static int GetNumberOfDigits(this string text)
    {
        const string pattern = @"\.\d+|.$";
        return Regex.Matches(text, pattern)[0].Value.Length - 1;
    }

    #endregion

    public static bool ContainsRevitProhibitedSymbols(string text, bool ifTextIsNullOrEmptyResult = false)
    {
        if (string.IsNullOrEmpty(text))
            return ifTextIsNullOrEmptyResult;
            
        var charArray = text.ToCharArray();

        foreach (var c in charArray)
        {
            switch (c)
            {
                case '\\':
                case ':':
                case '{':
                case '}':
                case '[':
                case ']':
                case '|':
                case ';':
                case '<':
                case '>':
                case '?':
                case '`':
                case '~':
                    return true;
            }
        }

        return false;
    }

    public static string DeleteMultipleSpaces(this string text)
    {
        const string pattern = @"\s+";
        const string target = " ";
        var regex = new Regex(pattern);
        return regex.Replace(text, target);
    }

    public static string GetOneLineList(this List<string> list, string separator, bool deleteRepeat)
    {
        var prefixValue = GetPrefixValue(list, deleteRepeat);
        var listByPrefix = new List<string>();

        foreach (var prefix in prefixValue.Keys)
        {
            var values = prefixValue[prefix].OrderBy(n => n, new NumbersStringComparer()).ToList();

            if (values.Count < 3)
            {
                listByPrefix.Add(string.Join(separator, values.Select(n => prefix + n)));
            }
            else
            {
                FindSequence(listByPrefix, prefix, values);
            }
        }

        return string.Join(separator, listByPrefix);
    }

    public static int GetArgumentsCount(string message)
    {
        const string pattern = @"(?<!\{)(?>\{\{)*\{\d(.*?)";
        var matches = Regex.Matches(message, pattern);
        //var totalMatchCount = matches.Count;
        var uniqueMatchCount = matches.OfType<Match>().Select(m => m.Value).Distinct().Count();
        //var parameterMatchCount = uniqueMatchCount == 0 ? 0 : matches.OfType<Match>().Select(m => m.Value).Distinct().Select(m => int.Parse(m.Replace("{", string.Empty))).Max() + 1;

        return uniqueMatchCount;
    }

    private static Dictionary<string, List<string>> GetPrefixValue(List<string> list, bool deleteRepeat)
    {
        var prefixValue = new Dictionary<string, List<string>>();

        list.ForEach(n =>
        {
            for (var i = n.Length - 1; i >= 0; i--)
            {
                if (!char.IsDigit(n, i))
                {
                    var prefix = n.Substring(0, i + 1);
                    var value = n.Substring(i + 1);
                    AddPrefixValue(prefixValue, prefix, value, deleteRepeat);
                    break;
                }
                else if (i == 0)
                {
                    var prefix = string.Empty;
                    var value = n;
                    AddPrefixValue(prefixValue, prefix, value, deleteRepeat);
                }
            }
        });

        prefixValue = prefixValue
            .OrderBy(n => n.Key, new NumbersStringComparer())
            .ToDictionary(n => n.Key, n => n.Value);
        return prefixValue;
    }

    private static void AddPrefixValue(Dictionary<string, List<string>> prefixValue, string prefix, string value, bool deleteRepeat)
    {
        if (prefixValue.ContainsKey(prefix))
        {
            if (!prefixValue[prefix].Contains(value))
                prefixValue[prefix].Add(value);
            else if (!deleteRepeat)
                prefixValue[prefix].Add(value);
        }
        else
            prefixValue.Add(prefix, new List<string>() {value});
    }

    private static void FindSequence(List<string> listByPrefix, string prefix, List<string> values)
    {
        var isPrefixDigit = false;

        if (prefix.Length > 0)
        {
            int.TryParse(prefix.Split('.', ',').Last(), out _);
            isPrefixDigit = int.TryParse(prefix.Substring(0, prefix.Length - 1), out _);
        }

        if (string.IsNullOrEmpty(values[0]))
        {
            listByPrefix.Add(prefix);
            values.RemoveAt(0);
        }

        values.Add((-1).ToString()); // для обработки последнего элемента списка
        var sequence = new List<string>() { values[0] };
        for (var i = 1; i < values.Count; i++)
        {
            int.TryParse(values[i], out var value);
            int.TryParse(values[i - 1], out var previousValue);

            if (previousValue + 1 == value)
            {
                sequence.Add(values[i]);
            }
            else
            {
                if (sequence.Count > 2)
                {
                    var secondPrefix = isPrefixDigit ? prefix : string.Empty;
                    listByPrefix.Add($"{prefix}{sequence[0]}..{secondPrefix}{sequence.Last()}");
                }
                else if (sequence.Count > 1)
                {
                    listByPrefix.Add($"{prefix}{sequence[0]}");
                    listByPrefix.Add($"{prefix}{sequence[1]}");
                }
                else
                {
                    listByPrefix.Add($"{prefix}{previousValue}");
                }

                sequence = new List<string>() { values[i] };
            }
        }
    }

    #region Текстовые функции, преобразования в строковые и обратно

    public static string GetReverse(this string text)
    {
        var result = string.Empty;
        var reverse = text.Reverse();

        return reverse.Aggregate(result, (current, c) => current + c);
    }

    public static double ToDouble(this string text)
    {
        return Convert.ToDouble(text.Replace('.', ','));
    }

    public static int FirstInt(this string text, int defaultValue = default)
    {
        if (string.IsNullOrEmpty(text))
            return 0;

        const string pattern = @"(\-?\d+)|$";
        text = Regex.Matches(text, pattern)[0].Value;
        return text.Length != 0
            ? int.Parse(text)
            : defaultValue;
    }

    public static int LastInt(this string text, int errorValue)
    {
        if (string.IsNullOrEmpty(text))
            return errorValue;
        var i = text.Length - 1;
        while (i >= 0)
        {
            if ((int)text[i] < 48 || (int)text[i] > 57)
                break;
            i--;
        }
        text = text.Substring(i + 1);
        return text != string.Empty
            ? Convert.ToInt32(text)
            : errorValue;
    }

    public static int LastInt(this string text, int errorValue, out string prefix, out int count)
    {
        var i = text.Length - 1;
        while (i >= 0)
        {
            if (text[i] < 48 || text[i] > 57)
                break;
            i--;
        }

        prefix = text.Substring(0, i + 1);
        text = text.Substring(i + 1);

        var result = 0;
        var error = false;
        if (text != string.Empty)
            result = Convert.ToInt32(text);
        else
            error = true;

        count = text.Length;

        return !error ? result : errorValue;
    }

    public static string LastIntOrLetter(this string text, out int intValue)
    {
        intValue = -1;

        if (string.IsNullOrEmpty(text))
            return text;

        var textReverse = text.Reverse();
        var intValueReverse = string.Empty;
        var result = string.Empty;
        foreach (var c in textReverse)
        {
            if (string.IsNullOrEmpty(intValueReverse))
            {
                if ((c > 64 && c < 91) || (c > 96 && c < 123))
                    return result += c;
                else if (c > 47 && c < 58)
                {
                    intValueReverse += c;
                    if (text.Length == 1)
                    {
                        result = intValueReverse;
                        intValue = int.Parse(result);
                        return result;
                    }
                }
            }
            else
            {
                if (c > 47 && c < 58)
                    intValueReverse += c;
                else
                {
                    result = string.Join(string.Empty, intValueReverse.Reverse());
                    intValue = int.Parse(result);
                    return result;
                }
            }
        }

        return string.Empty;
    }

    private static string _firstDoublePattern;
    private static string FirstDoublePattern
    {
        get
        {
            if (_firstDoublePattern != null)
                return _firstDoublePattern;

            var currentDecSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                
            return _firstDoublePattern = $@"\-?\d+(\{currentDecSeparator}\d+)?|$";
        }
    }

    public static double FirstDouble(this string text, double defaultValue)
    {
        return text.FirstDouble() ?? defaultValue;
    }
        
    public static double? FirstDouble(this string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return default;
            
        const string pattern = @"\-?\d+(\.\d+)?|$";
        text = text.Replace(",", ".");
        text = Regex.Matches(text, pattern)[0].Value;

        return text.Length != 0
            ? double.Parse(text, CultureInfo.InvariantCulture)
            : default;
    }

    public static double? FirstDoubleOrNull(this string text)
    {
        if (string.IsNullOrEmpty(text))
            return null;

        const string pattern = @"\-?\d+(\.\d+)?|$";
        text = text.Replace(",", ".");
        text = Regex.Matches(text, pattern)[0].Value;

        return text.Length == 0
            ? null
            : double.TryParse(text, NumberStyles.Any, CultureInfo.InvariantCulture, out var number)
                ? number
                : null;
    }

    public static string FirstDoubleToString(this string text)
    {
        return text.FirstDouble()?.ToString(CultureInfo.CurrentCulture);
    }

    public static string TextWithoutEndInt(this string text)
    {
        const string pattern = @"(.+)\s(\d+)$|$";
        var result = Regex.Matches(text, pattern)[0].Groups[1].Value;

        return result.Length == 0 ? text : result;
    }

    public static int EndInt(this string text)
    {
        const string pattern = @"\s(\d+)$|$";
        text = Regex.Matches(text, pattern)[0].Value;

        return text.Length == 0 ? 0 : int.Parse(text);
    }

    public static double LastDouble(this string text, char decimalSymbol = ',')
    {
        var currDecSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        var pattern = $@"\d+(\{currDecSeparator}\d+)?|$";
        text = text.Replace(".", currDecSeparator).Replace(decimalSymbol.ToString(), currDecSeparator);
        var matches = Regex.Matches(text, pattern);
        if (matches.Count > 1)
        {
            text = matches[matches.Count - 2].Value;
            return text.Length == 0 ? 0 : double.Parse(text);
        }
        else
        {
            return 0;
        }
    }

    public static string ToLastInt(this string text)
    {
        var i = text.Length - 1;
        while (i >= 0)
        {
            if (text[i] < 48 || text[i] > 57)
                break;
            i--;
        }
        return text.Substring(0, i + 1);
    }

    public static string ToComma(this string text)
    {
        if (text.IndexOf(',') >= 0)
            text = text.Substring(0, text.IndexOf(','));
        return text;
    }

    /// <summary>
    /// Удалить из текста последовательность символов text
    /// </summary>
    /// <param name="text0"></param>
    /// <param name="text">Текст, который следует удалить из исходного</param>
    /// <returns></returns>
    public static string StringDifference(this string text0, string text)
    {
        if (!text0.Contains(text))
            return text0;
        for (var i = 0; i < text0.Length; i++)
        {
            if (text0[i] == text[0])
            {
                var k = 1;
                for (var j = 1; j < text.Length; j++)
                {
                    if (text0[i + j] == text[j])
                        k++;
                }
                if (k == text.Length)
                {
                    text0 = text0.Remove(i, k);
                    break;
                }
            }
        }
        return text0;
    }

    ///// <summary>
    ///// Вычислить число строк, которое занимает текст заданного шрифта, при заданной длине строки
    ///// </summary>
    ///// <param name="text">Текст.</param>
    ///// <param name="fontName">Название шрифта.</param>
    ///// <param name="fontSize">Размер шрифта.</param>
    ///// <param name="stringLength">Длина строки.</param>
    ///// <returns></returns>
    //public static int StringCount(this string text, string fontName, float fontSize, double stringLength)
    //{
    //    const int precision = 100;
    //    var font = new Font(fontName, precision * fontSize);
    //    var stringCount = 1;
    //    var stringLength_i = 0.0;

    //    var texts = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
    //    foreach (var t in texts)
    //    {
    //        double textLength = TextRenderer.MeasureText(t, font).Width;
    //        if (stringLength_i + textLength > stringLength * precision)
    //        {
    //            stringCount++;
    //            stringLength_i = textLength;
    //        }
    //        else
    //        {
    //            stringLength_i += textLength;
    //        }
    //    }
    //    return
    //        stringCount;
    //}

    public static bool IsInt(this string text)
    {
        return
            text != string.Empty
            && text.All(t => t >= 48 && t <= 57);
    }

    public static void CombineSequentialStrings(this List<string> list)
    {
        for (var i = 0; i < list.Count - 2; i++)
        {
            var prefix = new string[3];

            var count = new int[3];

            var number = new int[3]
            {
                list[i].LastInt(int.MinValue, out prefix[0], out count[0]),
                list[i + 1].LastInt(int.MinValue, out prefix[1], out count[1]),
                list[i + 2].LastInt(int.MinValue, out prefix[2], out count[2])
            };

            var indexes = new List<int>();

            if (number[0] + 1 == number[1] &&
                number[1] + 1 == number[2] &&
                prefix[0] == prefix[1] &&
                prefix[1] == prefix[2] &&
                count[0] == count[1] &&
                count[1] == count[2])
            {
                indexes.Add(i + 1);
                indexes.Add(i + 2);

                var j = 0;
                for (j = i + 3; j < list.Count; j++)
                {
                    if (number[2] + 1 == list[j].LastInt(int.MinValue))
                    {
                        indexes.Add(j);
                        number[2]++;
                    }
                    else
                        break;
                }

                list[i] = string.Format("{0}{1:d" + count[0] + "}...{2:d" + count[2] + "}", prefix[0], number[0], number[2]);

                for (var k = indexes.Count - 1; k >= 0; k--)
                    list.RemoveAt(indexes[k]);
            }
        }
    }

    #endregion Текстовые функции, преобразования в строковые и обратно

    public static class BooleanAsString
    {
        public const string
            True = "true",
            False = "false";
    }

    public static string GetBooleanString(bool b)
    {
        return b ? BooleanAsString.True : BooleanAsString.False;
    }

    public static int GetTextWidth(this string text, Font font)
    {
        if (string.IsNullOrEmpty(text))
            return 0;

        var g = Graphics.FromImage(new Bitmap(1, 1));

        return (int)g.MeasureString(text, font).Width;
    }

    public static int GetMaxWidthPixel(List<string> items, Font font)
    {
        var itemMaxLength = "0";
        foreach (var item in items)
            if (item.Length > itemMaxLength.Length)
                itemMaxLength = item;
        var g = Graphics.FromImage(new Bitmap(1, 1));

        return (int)g.MeasureString(itemMaxLength, font).Width + 10;
    }

    public static string RemoveRevitProhibitedSymbols(string text)
    {
        if (string.IsNullOrEmpty(text))
            return text;

        text = text.Replace("\\", "-");
        text = text.Replace(":", "-");
        text = text.Replace("{", "(");
        text = text.Replace("}", ")");
        text = text.Replace("[", "(");
        text = text.Replace("]", ")");
        text = text.Replace("|", "-");
        text = text.Replace(";", ",");
        text = text.Replace("<", " ");
        text = text.Replace(">", " ");
        text = text.Replace("?", " ");
        text = text.Replace("`", " ");
        text = text.Replace("~", "-");

        while (text.Contains("  "))
            text = text.Replace("  ", " ");

        text = text.Trim();

        return text;
    }

    public static bool IsNotNullAndNotEmpty(string value) => !string.IsNullOrEmpty(value);


    public static string Join(string separator, params object[] values) =>
        string.Join(separator, values.Select(n => n?.ToString()).Where(n => !string.IsNullOrEmpty(n)));
}