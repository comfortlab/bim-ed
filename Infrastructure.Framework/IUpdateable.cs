﻿namespace CoLa.BimEd.Infrastructure.Framework
{
    public interface IUpdateable<in TEntity> : IUpdateable where TEntity : class
    {
        void Update(TEntity entity);
    }

    public interface IUpdateable
    {
        void Update<TEntity>(TEntity entity) where TEntity : class;
    }
}