﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoLa.BimEd.Infrastructure.Framework.Math
{
    public static class FindValues
    {
        public static double FindClosestValue(double number, IEnumerable<double> row)
        {
            var rowList = row.ToList();

            if (!rowList.Any())
            {
                throw new ArgumentException("Provided row is empty.");
            }

            if (number > rowList.Max() || number < 0.0)
            {
                throw new ArgumentException("Provided value is out of bounds.");
            }

            return rowList.Aggregate((x, y) => System.Math.Abs(x - number) < System.Math.Abs(y - number) && x > number ? x : y);
        }

        public static T FindClosestValue<T>(double number, IEnumerable<T> row, Func<T, double> memberToCompare) where T : class
        {
            var rowList = row.ToList();

            if (!rowList.Any())
            {
                return default;
                // throw new ArgumentException("Provided row is empty.");
            }

            // if (number > rowList.Max(memberToCompare.Invoke) || number < 0.0)
            // {
            //     throw new ArgumentException("Provided value is out of bounds.");
            // }

            return rowList.Aggregate((x, y) =>
                System.Math.Abs(memberToCompare.Invoke(x) - number) < System.Math.Abs(memberToCompare.Invoke(y) - number) &&
                memberToCompare.Invoke(x) > number ? x : y);
        }
    }
}