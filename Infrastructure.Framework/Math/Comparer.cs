﻿using System.Collections.Generic;
using System.Linq;

namespace CoLa.BimEd.Infrastructure.Framework.Math;

public static class Comparer
{
    public static double Max(params double[] values) => values.Max();
    public static double Max(params IEnumerable<double>[] values) => values.SelectMany(x => x).Max();
    public static int Max(params int[] values) => values.Max();
    public static int Max(params IEnumerable<int>[] values) => values.SelectMany(x => x).Max();
    public static double Min(params double[] values) => values.Min();
    public static double Min(params IEnumerable<double>[] values) => values.SelectMany(x => x).Min();
    public static int Min(params int[] values) => values.Min();
    public static int Min(params IEnumerable<int>[] values) => values.SelectMany(x => x).Min();
}