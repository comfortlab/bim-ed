﻿using System;
using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.Infrastructure.Framework.Math;

public enum Converting
{
    MetersFromFeet,
    MetersFromInternal,
    MetersToFeet,
    MetersToInternal,
    MillimetersFromFeet,
    MillimetersFromInternal,
    MillimetersToFeet,
    MillimetersToInternal,
    OhmMeterFromInternal,
    OhmMeterToInternal,
    SquareMetersFromFeet,
    SquareMetersToFeet,
    SquareMillimetersFromFeet,
    SquareMillimetersToFeet,
    VoltsFromInternal,
    VoltsToInternal,
    VoltAmperesFromInternal,
    VoltAmperesToInternal,
    WattsFromInternal,
    WattsToInternal,
    FromKilo,
    FromMega,
    FromMicro,
    FromMilli,
    FromSquareKilo,
    FromSquareMega,
    FromSquareMilli,
    FromSquareMicro,
    ToKilo,
    ToMega,
    ToMicro,
    ToMilli,
    ToSquareKilo,
    ToSquareMega,
    ToSquareMilli,
    ToSquareMicro,
}
    
public static class UnitConverter
{
    private const double MetersInFoot = 0.3048;
    private const double MetersInFoot2 = MetersInFoot * MetersInFoot;
    private const double MetersInFoot3 = MetersInFoot2 * MetersInFoot;
    private const double MillimetersInFoot = 304.8;
    private const double MillimetersInFoot2 = MillimetersInFoot * MillimetersInFoot;

    public static double MillimetersToInternal(this double value) => value.Convert(Converting.MillimetersToInternal);
    public static double MillimetersFromInternal(this double value) => value.Convert(Converting.MillimetersFromInternal);
    
    public static double Convert(this double value, Converting converting, bool round = false)
    {
        var result = GetFormula(converting).Invoke(value);
        return round ? System.Math.Round(result) : result;
    }

    public static double Convert(this double value, Converting converting, int significantDigits, int? digits = null)
    {
        return GetFormula(converting).Invoke(value).Round(significantDigits, digits);
    }

    private static Func<double, double> GetFormula(Converting converting) => converting switch
    {
        Converting.MillimetersFromFeet => feet => feet * MillimetersInFoot,
        Converting.MillimetersFromInternal => feet => feet * MillimetersInFoot,
        Converting.MillimetersToFeet => millimeters => millimeters / MillimetersInFoot,
        Converting.MillimetersToInternal => millimeters => millimeters / MillimetersInFoot,
        Converting.MetersFromFeet => feet => feet * MetersInFoot,
        Converting.MetersFromInternal => feet => feet * MetersInFoot,
        Converting.MetersToFeet => meters => meters / MetersInFoot,
        Converting.MetersToInternal => meters => meters / MetersInFoot,
        Converting.OhmMeterFromInternal => internalOhmMeter => internalOhmMeter * MetersInFoot3,
        Converting.OhmMeterToInternal => ohmMeters => ohmMeters / MetersInFoot3,
        Converting.SquareMetersFromFeet => value => value * MetersInFoot2,
        Converting.SquareMetersToFeet => value => value / MetersInFoot2,
        Converting.SquareMillimetersFromFeet => value => value * MillimetersInFoot2,
        Converting.SquareMillimetersToFeet => value => value / MillimetersInFoot2,
        Converting.VoltsFromInternal => value => value * MetersInFoot2,
        Converting.VoltAmperesFromInternal => value => value * MetersInFoot2,
        Converting.WattsFromInternal => value => value * MetersInFoot2,
        Converting.VoltsToInternal => value => value / MetersInFoot2,
        Converting.VoltAmperesToInternal => value => value / MetersInFoot2,
        Converting.WattsToInternal => value => value / MetersInFoot2,
        Converting.FromKilo => value => value * 1e3,
        Converting.ToMilli => value => value * 1e3,
        Converting.FromMega => value => value * 1e6,
        Converting.ToMicro => value => value * 1e6,
        Converting.FromMicro => value => value * 1e-6,
        Converting.ToMega => value => value * 1e-6,
        Converting.FromMilli => value => value * 1e-3,
        Converting.ToKilo => value => value * 1e-3,
        Converting.FromSquareKilo => value => value * 1e6,
        Converting.ToSquareMilli => value => value * 1e6,
        Converting.FromSquareMega => value => value * 1e12,
        Converting.ToSquareMicro => value => value * 1e12,
        Converting.FromSquareMicro => value => value * 1e-12,
        Converting.ToSquareMega => value => value * 1e-12,
        Converting.FromSquareMilli => value => value * 1e-6,
        Converting.ToSquareKilo => value => value * 1e-6,
        _ => throw new ArgumentOutOfRangeException(nameof(converting), converting, null)
    };
}