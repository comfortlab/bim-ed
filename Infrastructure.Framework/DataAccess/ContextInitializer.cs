﻿namespace CoLa.BimEd.Infrastructure.Framework.DataAccess
{
    public interface IContextInitializer { }

    public abstract class ContextInitializer<TContext> : IContextInitializer
        where TContext : Context
    {
        public abstract void Seed(TContext appContext);
    }
}