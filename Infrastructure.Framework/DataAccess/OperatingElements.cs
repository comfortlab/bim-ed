﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoLa.BimEd.Infrastructure.Framework.DataAccess;

/// <summary>
/// Используется для передачи объектов между несвязанными классами.
/// </summary>
public class OperatingElements
{
    private readonly List<OperatingElement> _operatingElements;

    public OperatingElements() => _operatingElements = new List<OperatingElement>();

    public void Push<T>(T element, int index = 0) where T : class
    {
        Drop<T>(index);
        _operatingElements.Add(new OperatingElement(element, index));
    }

    public T Peak<T>(int index = 0) where T : class
    {
        _operatingElements.RemoveAll(n => n == null);
        var type = typeof(T);
            
        return type.IsInterface
            ? _operatingElements.FirstOrDefault(n => n.Index == index && n.Element.GetType().GetInterface(type.Name) != null)?.Element as T
            : _operatingElements.FirstOrDefault(n => n.Index == index && n.Element is T)?.Element as T;
    }

    public T Pop<T>(int index = 0) where T : class
    {
        try { return Peak<T>(index); }
        finally { Drop<T>(index); }
    }

    public void Drop<T>(int index = 0) where T : class => _operatingElements.RemoveAll(n => n == null || (n.Index == index && n.Element is T));
    public void DropAll<T>() where T : class => _operatingElements.RemoveAll(n => n == null || n.Element is T);
    public void DropAll() => _operatingElements.Clear();

    private class OperatingElement
    {
        public OperatingElement(object element, int index = 0)
        {
            Guid = Guid.NewGuid();
            Element = element;
            Index = index;
        }
            
        public object Element { get; }
        public int Index { get; }
        public Guid Guid { get; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((OperatingElement)obj);
        }
        private bool Equals(OperatingElement other) => Guid.Equals(other.Guid);
        public override int GetHashCode() => Guid.GetHashCode();
    }
}