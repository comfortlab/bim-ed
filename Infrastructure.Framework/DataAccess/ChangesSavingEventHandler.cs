﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DataAccess;

public delegate void ChangesSavingEventHandler(object sender, ChangesSavingArgs e);
    
public class ChangesSavingArgs : EventArgs
{
    public ChangesSavingArgs(string transactionName) => TransactionName = transactionName;
    public string TransactionName { get; }
}