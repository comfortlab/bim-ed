﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.DataAccess;

public interface IUnitOfWork : IDisposable
{
    void SaveChanges();
    void SaveChanges(string transactionName);
}