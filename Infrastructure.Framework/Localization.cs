using System.Collections.Generic;
using System.Globalization;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.Infrastructure.Framework;

public class Localization
{
    public const string Global = "GLOBAL";
    public const string International = "SCHINT";

    public const string EnInt = "en";
    public const string FrInt = "fr";

    public const string EnGb = "en-GB";
    public const string EsEs = "es-ES";
    public const string FrFr = "fr-FR";
    public const string RuRu = "ru-RU";

    public const string Fra = "FRA";
    public const string Esp = "ESP";
    public const string Gbr = "GBR";
    public const string Rus = "RUS";

    public string Culture { get; private set; }
    public string LanguageCode { get; private set; }
    public string CountryCode { get; private set; }

    public Localization(string culture) => SetLocalization(culture);
    public void SetLocalization(string culture)
    {
        Culture = culture ?? EnInt;

        var twoLetterCodes = Culture.Split('-');

        if (twoLetterCodes.IsEmpty())
            return;

        LanguageCode = twoLetterCodes[0].ToUpper();
        CountryCode = GetCountryCode(culture) ?? International;
    }
        
    public string GetCountryCode(string culture)
    {
        var twoLetterCodes = culture.Split('-');

        return twoLetterCodes.Length > 1
            ? CountryDictionary.TryGetValue(twoLetterCodes[1], out var threeLetterCode)
                ? threeLetterCode
                : new RegionInfo(new CultureInfo(culture).LCID).ThreeLetterISORegionName
            : null;
    }

    private static readonly Dictionary<string, string> CountryDictionary = new()
    {
        ["FR"] = "FRA",
        ["ES"] = "ESP",
        ["GB"] = "GBR",
        ["RU"] = "RUS",
    };
}