﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework;

public abstract class Disposable : IDisposable
{
    protected bool Disposed { get; set; }
        
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing == false)
            return;

        Disposed = true;
    }

    ~Disposable() => Dispose(false);
}