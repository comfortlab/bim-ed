﻿using System;
using System.Collections.Generic;

namespace CoLa.BimEd.Infrastructure.Framework.Comparer;

public class NumbersStringComparer : IComparer<object>
{
    private readonly bool _zeroesFirst;
        
    public NumbersStringComparer(bool zeroesFirst = true) => _zeroesFirst = zeroesFirst;
        
    public int Compare(object obj1, object obj2)
    {
        //get rid of special cases
        if (obj1 == null && obj2 == null) return 0;
        if (obj1 == null) return -1;
        if (obj2 == null) return 1;

        var s1 = obj1.ToString();
        var s2 = obj2.ToString();

        if (s1.Length <= 0 && s2.Length <= 0) return 0;
        if (s1.Length <= 0) return -1;
        if (s2.Length <= 0) return 1;

        //special case
        var sp1 = char.IsLetterOrDigit(s1[0]);
        var sp2 = char.IsLetterOrDigit(s2[0]);
        if (sp1 && !sp2) return 1;
        if (!sp1 && sp2) return -1;

        int i1 = 0, i2 = 0; //current index

        while (true)
        {
            var c1 = s1[i1];
            var c2 = s2[i2];
            sp1 = char.IsDigit(c1);
            sp2 = char.IsDigit(c2);
            var r = 0; // temp result
            if (!sp1 && !sp2)
            {
                var letter1 = char.IsLetter(c1);
                var letter2 = char.IsLetter(c2);

                if (letter1 && letter2)
                {
                    r = string.Compare(char.ToUpper(c1).ToString(), char.ToUpper(c2).ToString(), StringComparison.Ordinal);
                    if (r != 0) return System.Math.Sign(r);
                }
                else if (!letter1 && !letter2)
                {
                    r = c1.CompareTo(c2);
                    if (r != 0) return System.Math.Sign(r);
                }
                else if (!letter1 && letter2)
                {
                    return -1;
                }
                else if (letter1 && !letter2)
                {
                    return 1;
                }
            }
            else if (sp1 && sp2)
            {
                r = CompareNum(s1, ref i1, s2, ref i2, _zeroesFirst);
                if (r != 0) return System.Math.Sign(r);
            }
            else if (sp1)
            {
                return -1;
            }
            else if (sp2)
            {
                return 1;
            }
            i1++;
            i2++;
            if ((i1 >= s1.Length) && (i2 >= s2.Length))
            {
                return 0;
            }
            else if (i1 >= s1.Length)
            {
                return -1;
            }
            else if (i2 >= s2.Length)
            {
                return 1;
            }
        }
    }

    private static int CompareNum(string s1, ref int i1, string s2, ref int i2, bool zeroesFirst)
    {
        var nzStart1 = i1;
        var nzStart2 = i2; // nz = non zero
        var end1 = i1;
        var end2 = i2;

        ScanNumEnd(s1, i1, ref end1, ref nzStart1);
        ScanNumEnd(s2, i2, ref end2, ref nzStart2);
        var start1 = i1; i1 = end1 - 1;
        var start2 = i2; i2 = end2 - 1;

        if (zeroesFirst)
        {
            var zl1 = nzStart1 - start1;
            var zl2 = nzStart2 - start2;
            if (zl1 > zl2) return -1;
            if (zl1 < zl2) return 1;
        }

        var nzLength1 = end1 - nzStart1;
        var nzLength2 = end2 - nzStart2;

        if (nzLength1 < nzLength2) return -1;
        else if (nzLength1 > nzLength2) return 1;

        for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++)
        {
            var r = s1[j1].CompareTo(s2[j2]);
            if (r != 0) return r;
        }
        // the nz parts are equal
        var length1 = end1 - start1;
        var length2 = end2 - start2;
        if (length1 == length2) return 0;
        if (length1 > length2) return -1;
        return 1;
    }

    //lookahead
    private static void ScanNumEnd(string s, int start, ref int end, ref int nzStart)
    {
        nzStart = start;
        end = start;
        var countZeros = true;
        while (char.IsDigit(s, end))
        {
            if (countZeros && s[end].Equals('0'))
            {
                nzStart++;
            }
            else countZeros = false;
            end++;
            if (end >= s.Length) break;
        }
    }
}