﻿using System.Collections.Generic;
using CoLa.BimEd.Infrastructure.Framework.Extensions;

namespace CoLa.BimEd.Infrastructure.Framework.Comparer;

public class DoubleComparer : IComparer<double>
{
    public int Compare(double x, double y) => x.IsEqualTo(y) ? 0 : x > y ? 1 : -1;
}