﻿using System.Collections.Generic;

namespace CoLa.BimEd.Infrastructure.Framework.Comparer;

public class IntComparer : IComparer<int>
{
    public int Compare(int x, int y) => x > y ? 1 : x < y ? -1 : 0;
}