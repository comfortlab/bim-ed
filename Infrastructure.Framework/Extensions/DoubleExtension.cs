﻿using System.Globalization;

namespace CoLa.BimEd.Infrastructure.Framework.Extensions;

public static class DoubleExtension
{
    public const double Epsilon = 1E-8;
    public const double NegativeEpsilon = -Epsilon;

    public static double Round(this double value) =>
        value.Round(significantDigits: null);

    public static double Round(this double value, int? significantDigits) =>
        value.Round(significantDigits, digits: null);

    public static double RoundTo(this double value, int? digits) =>
        value.Round(significantDigits: null, digits: null);
        
    /// <summary>
    /// Округлить число до указанного числа значащих цифр
    /// </summary>
    /// <param name="value">Округляемое число</param>
    /// <param name="significantDigits">Число значащих цифр</param>
    /// <param name="digits">Число знаков в дробной части</param>
    /// <returns></returns>
    public static double Round(this double value, int? significantDigits, int? digits)
    {
        if (significantDigits == null && digits == null)
            return System.Math.Round(value);
            
        var absValue = System.Math.Abs(value);

        if (System.Math.Abs(value) < Epsilon ||
            digits == null && significantDigits < 1 || significantDigits > 16)
            return value;

        var result = value;
        var i = 1;

        if (significantDigits is { } intSignificantDigits and > 0)
        {
            switch (absValue)
            {
                case < 1:
                {
                    while (System.Math.Abs(value) < 1)
                    {
                        value *= 10;
                        i *= 10;
                    }

                    result = System.Math.Round(value, intSignificantDigits - 1) / i;
                    break;
                }
                case > 1 when double.IsInfinity(absValue) == false:
                {
                    while (System.Math.Abs(value) > 10)
                    {
                        value /= 10;
                        i *= 10;
                    }

                    result = System.Math.Round(value, intSignificantDigits - 1) * i;
                    break;
                }
            }
        }

        if (digits is not { } intDigits)
            return result;
        
        if (intDigits >= 0)
        {
            result = System.Math.Round(result, (int)digits);
        }
        else
        {
            var m = System.Math.Pow(10, System.Math.Abs(intDigits));
            result = System.Math.Round(result / m) * m;
        }

        return result;
    }

    public static string ToStringInvariant(this double value, int significantDigits, int? digits = null) =>
        value.Round(significantDigits, digits).ToStringInvariant();
        
    public static string ToStringInvariant(this double value) =>
        value.ToString(CultureInfo.InvariantCulture);

    public static bool IsEqualTo(this double source, double value) =>
        source.IsEqualTo(value, Epsilon);

    public static bool IsEqualTo(this double source, double value, double epsilon) =>
        System.Math.Abs(source - value) < epsilon;

    public static bool IsEqualToZero(this double value) =>
        System.Math.Abs(value) < Epsilon;
}