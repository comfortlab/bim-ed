﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Используется для хранения коллекции элементов типа T, приведённых к типу TDto, в разных файлах.
/// </summary>
/// <typeparam name="T">Тип элементов коллекции</typeparam>
/// <typeparam name="TDto">Тип контракта элеметов коллекции для хранения в файле</typeparam>
/// <exception cref="InvalidOperationException">Тип T не имеет свойств <b>Id</b> или <b>Name</b>.</exception>
/// <remarks>
/// Для разделения коллекции на файлы тип T должен иметь свойства <b>Id</b> или <b>Name</b>.<br/>
/// Формат имён файлов: <b>{T}_{Id}.json</b> или <b>{T}_{Name}.json</b>
/// </remarks>
public abstract class ManyFilesRepository<T, TDto> : ManyFilesRepository<T> where T : class
{
    private readonly IMapper _mapper;

    protected ManyFilesRepository(
        IRepositoryInitializer<T> repositoryInitializer, AppPaths appPaths, IMapper mapper) :
        base(repositoryInitializer, appPaths) => _mapper = mapper;

    protected override void Read(string path = null) => this.Read<T, TDto>(path ?? Path, _mapper);
    protected override void Write(string path = null) => this.Write<T, TDto>(path ?? Path, _mapper);
}

/// <summary>
/// Используется для хранения коллекции элементов типа T в разных файлах.
/// </summary>
/// <typeparam name="T">Тип элементов коллекции</typeparam>
/// <exception cref="InvalidOperationException">Тип T не имеет свойств <b>Id</b> или <b>Name</b>.</exception>
/// <remarks>
/// Для разделения коллекции на файлы тип T должен иметь свойства <b>Id</b> или <b>Name</b>.<br/>
/// Формат имён файлов: <b>{T}_{Id}.json</b> или <b>{T}_{Name}.json</b>
/// </remarks>
public abstract class ManyFilesRepository<T> : FileRepository<T> where T : class
{
    private readonly PropertyInfo _idProperty;

    protected ManyFilesRepository(
        IRepositoryInitializer<T> repositoryInitializer, AppPaths appPaths) :
        base(repositoryInitializer, appPaths)
    {
        _idProperty =
            typeof(T).GetProperty("Id") ??
            typeof(T).GetProperty("Name") ??
            throw new InvalidOperationException($"{typeof(T).FullName} hasn't properties 'Id' or 'Name'");
    }

    public override void Load()
    {
        Clear();

        var files = GetFiles();

        if (files.Any())
        {
            foreach (var file in files)
            {
                var json = File.ReadAllText(file, Encoding.UTF8);
                var @object = JsonConvert.DeserializeObject<T>(json);
                Add(@object);
            }
        }
        else
        {
            AddRange(RepositoryInitializer.Initialize());
            Save();
        }
    }

    public override void Save()
    {
        this.Select(item => _idProperty.GetValue(item).ToString())
            .Select(GetPath).Where(path => path != null)
            .ForEach(Write);
    }

    public override void Reset()
    {
        var files = GetFiles();
        foreach (var file in files) File.Delete(file);
        Load();
    }
    
    private string[] GetFiles()
    {
        var typeName = typeof(T).Name;
        
        return new DirectoryInfo(AppPaths.Database).GetFiles($"*.{Json}")
            .Where(x =>
            {
                var split = x.Name.Split('_');
                return split.Length > 1 && split[0] == typeName;
            })
            .Select(x => x.FullName)
            .ToArray();
    }
}