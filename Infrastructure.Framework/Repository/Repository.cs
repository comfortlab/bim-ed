﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

public abstract class Repository<T> : List<T> where T : class
{
    private readonly PropertyInfo _keyProperty;
    protected internal readonly IRepositoryInitializer<T> RepositoryInitializer;

    protected Repository() { }
    protected Repository(IRepositoryInitializer<T> repositoryInitializer)
    {
        RepositoryInitializer = repositoryInitializer;
        
        var typeProperties = typeof(T).GetProperties();
        
        _keyProperty =
            typeProperties.FirstOrDefault(x => x.Name == "Guid") ??
            typeProperties.FirstOrDefault(x => x.Name == "Id") ??
            typeProperties.FirstOrDefault(x => x.Name == "Key") ??
            typeProperties.FirstOrDefault(x => x.Name == "Name");
    }

    public ChangedElements ChangedElements { get; } = new();
    public abstract void Load();
    public abstract void Save();
    public abstract void Save(T entity);
    public virtual T Get(object key)
    {
        return _keyProperty != null
            ? this.FirstOrDefault(x => Equals(_keyProperty.GetValue(x), key))
            : throw new InvalidOperationException($"Type {typeof(T)} doesn't have any key property: Guid, Id, Key or Name");
    }
}