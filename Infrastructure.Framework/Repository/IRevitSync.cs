namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Представляет логику синхронизации элеметов Revit и элементов доменной модели приложения.
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения</typeparam>
public interface IRevitSync<T> where T : class
{
    public RevitRepository<T> Repository { get; set; }
    public ChangedElements ChangedElements { get; }
    public void ApplyToRevit();
    public void ApplyToRevit(T entity);
}