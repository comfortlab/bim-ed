using System.Collections.Generic;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

public interface IRepositoryInitializer<T>
{
    public List<T> Initialize();
}