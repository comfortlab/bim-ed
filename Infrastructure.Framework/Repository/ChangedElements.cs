﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Prototype;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

public class ChangedElements : IPropertyPrototype<ChangedElements>
{
    private HashSet<int> _addedElements = new();
    private HashSet<int> _deletedElements = new();
    private HashSet<int> _modifiedElements = new();

    public void Create(int revitId) { if (revitId < 0) _addedElements.Add(revitId); }
    public void Delete(int revitId)
    {
        if (revitId > 0) _deletedElements.Add(revitId);
        else { _addedElements.Remove(revitId); _modifiedElements.Remove(revitId); }
    }
    public void Modify(int revitId) { if (revitId > 0) _modifiedElements.Add(revitId); }
    public void UpdateStorage() => IsStorageUpdated = true;

    public bool IsAnyChanged => IsAnyAdded || IsAnyDeleted || IsAnyModified || IsStorageUpdated;
    public bool IsAnyAdded => _addedElements.Any();
    public bool IsAnyDeleted => _deletedElements.Any();
    public bool IsAnyModified => _modifiedElements.Any();
    public bool IsStorageUpdated { get; private set; }
    
    public bool IsAdded(int revitId) => _addedElements.Remove(revitId);
    public bool IsDeleted(int revitId) => _deletedElements.Remove(revitId);
    public bool IsModified(int revitId) => _modifiedElements.Remove(revitId);
    
    public void ResetAll() { ResetAddedElements(); ResetDeletedElements(); ResetModifiedElements(); ResetUpdatedStorage(); }
    public void ResetAddedElements() => _addedElements = new HashSet<int>();
    public void ResetDeletedElements() => _deletedElements = new HashSet<int>();
    public void ResetModifiedElements() => _modifiedElements = new HashSet<int>();
    public void ResetUpdatedStorage() => IsStorageUpdated = false;
    public void PullProperties(ChangedElements prototype)
    {
        _addedElements = prototype._addedElements.ToHashSet();
        _deletedElements = prototype._deletedElements.ToHashSet();
        _modifiedElements = prototype._modifiedElements.ToHashSet();
        IsStorageUpdated = prototype.IsStorageUpdated;
    }
}