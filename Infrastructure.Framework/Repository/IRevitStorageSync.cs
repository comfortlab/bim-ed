namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Представляет логику синхронизации элемента хранилища Revit <b>DataStorage</b> и элемента доменной модели приложения.
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения</typeparam>
public interface IRevitStorageSync<T> : IRevitSync<T> where T : class
{
    public abstract T LoadFromRevit();
}