﻿using System;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Используется для представления элемента хранилища Revit <b>DataStorage</b> в виде элемента типа T и синхронизации этих элементов.    
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения, который соответствует типу элемента хранилища Revit.</typeparam>
/// <remarks>
/// Так как тип унаследован от <see cref="RevitRepository"/>, то он является коллекцией.
/// В работе используется только первый элемент.
/// </remarks>
public abstract class RevitStorageRepository<T> : RevitRepository<T> where T : class, new()
{
    private readonly IRevitStorageSync<T> _revitStorageSync;
    private readonly Func<T> _createDefault;

    protected RevitStorageRepository() { }
    protected RevitStorageRepository(IRevitStorageSync<T> revitSync, Func<T> createDefault = null) : base(revitSync)
    {
        _revitStorageSync = revitSync;
        _createDefault = createDefault;
        Load();
    }
    
    public sealed override void Load()
    {
        if (_revitStorageSync != null)
            this.ReplaceAll(new[] { _revitStorageSync.LoadFromRevit() ?? _createDefault?.Invoke() ?? new T() });
        else
            throw new NullReferenceException($"{nameof(RevitSync)} was not defined.");
    }
}