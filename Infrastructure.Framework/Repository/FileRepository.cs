﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Используется для хранения в файле коллекции элементов типа T приведённых к типу TDto
/// </summary>
/// <typeparam name="T">Тип элементов коллекции</typeparam>
/// <typeparam name="TDto">Тип контракта элеметов коллекции для хранения в файле</typeparam>
/// <remarks> Формат имени файла: <b>{T}.json</b> </remarks>
public abstract class FileRepository<T, TDto> : FileRepository<T> where T : class
{
    private readonly IMapper _mapper;

    protected FileRepository(
        IRepositoryInitializer<T> repositoryInitializer, AppPaths appPaths, IMapper mapper) :
        base(repositoryInitializer, appPaths) => _mapper = mapper;

    protected override void Read(string path = null) => this.Read<T, TDto>(path ?? Path, _mapper);
    protected override void Write(string path = null) => this.Write<T, TDto>(path ?? Path, _mapper);
}

/// <summary>
/// Используется для хранения в файле коллекции элементов типа T
/// </summary>
/// <typeparam name="T">Тип элементов коллекции</typeparam>
/// <remarks> Формат имени файла: <b>{T}.json</b> </remarks>
public abstract class FileRepository<T> : Repository<T> where T : class
{
    protected const string Json = "json";
    protected readonly AppPaths AppPaths;
    protected readonly string Path;

    protected FileRepository(
        IRepositoryInitializer<T> repositoryInitializer, AppPaths appPaths) :
        base(repositoryInitializer)
    {
        AppPaths = appPaths;
        Path = CreatePath(typeof(T).Name);
    }

    public override void Load() => Read();
    public override void Save() => Write();
    public override void Save(T entity) => Write(entity);
    public virtual void Reset()
    {
        this.ReplaceAll(RepositoryInitializer.Initialize());
        Write();
    }

    protected virtual void Read(string path = null)
    {
        path ??= Path;

        if (File.Exists(path))
        {
            var json = File.ReadAllText(path, Encoding.UTF8);
            this.ReplaceAll(JsonConvert.DeserializeObject<List<T>>(json));
        }
        else
        {
            this.ReplaceAll(RepositoryInitializer.Initialize());
            Write(path);
        }
    }

    protected virtual void Write(string path = null)
    {
        path ??= Path;

        var json = JsonConvert.SerializeObject(this);
        File.WriteAllText(path, json, Encoding.UTF8);
    }

    protected virtual void Write(T entity, string path = null)
    {
        path ??= Path;

        var json = JsonConvert.SerializeObject(entity);
        File.WriteAllText(path, json, Encoding.UTF8);
    }

    protected string GetPath(string name) => !string.IsNullOrWhiteSpace(name)
        ? CreatePath($"{typeof(T).Name}_{name}")
        : Path;

    private string CreatePath(string name) => System.IO.Path.ChangeExtension(
        System.IO.Path.Combine(
            AppPaths?.Database ?? throw new NullReferenceException($"{nameof(AppPaths)} was not defined."),
            name),
        Json);
}