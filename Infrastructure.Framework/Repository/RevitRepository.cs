﻿namespace CoLa.BimEd.Infrastructure.Framework.Repository;

public abstract class RevitRepository<T> : Repository<T> where T : class
{
    protected readonly IRevitSync<T> RevitSync;

    protected RevitRepository() { }
    protected RevitRepository(IRevitSync<T> revitSync)
    {
        RevitSync = revitSync;
        RevitSync.Repository = this;
    }

    public override void Save()
    {
        RevitSync.ChangedElements.PullProperties(this.ChangedElements);
        RevitSync.ApplyToRevit();
        this.ChangedElements.ResetAll();
    }

    public override void Save(T entity)
    {
        RevitSync.ChangedElements.PullProperties(this.ChangedElements);
        RevitSync.ApplyToRevit(entity);
        this.ChangedElements.ResetAll();
    }
}