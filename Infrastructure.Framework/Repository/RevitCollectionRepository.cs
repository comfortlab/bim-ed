﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Используется для представления коллекции элементов Revit одного типа в виде коллекции элементов типа T и синхронизации этих коллекций.    
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения, который соответствует типу элементов коллекции Revit.</typeparam>
public abstract class RevitCollectionRepository<T> : RevitRepository<T> where T : class
{
    private readonly IRevitCollectionSync<T> _revitCollectionSync;
    
    protected RevitCollectionRepository() { }
    protected RevitCollectionRepository(IRevitCollectionSync<T> revitSync) : base(revitSync)
    {
        _revitCollectionSync = revitSync;
        Load();
    }

    public sealed override void Load()
    {
        if (_revitCollectionSync != null)
            _revitCollectionSync.LoadFromRevit();
        else
            throw new NullReferenceException($"{nameof(RevitSync)} was not defined.");
    }
}