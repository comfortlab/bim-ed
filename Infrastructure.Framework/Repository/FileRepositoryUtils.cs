﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

public static class FileRepositoryUtils
{
    public static void Read<T, TDto>(this FileRepository<T> repository, string path, IMapper mapper) where T : class
    {
        if (File.Exists(path))
        {
            var json = File.ReadAllText(path, Encoding.UTF8);
            var dto = JsonConvert.DeserializeObject<TDto[]>(json);
            repository.ReplaceAll(mapper.Map<List<T>>(dto));
        }
        else
        {
            repository.ReplaceAll(repository.RepositoryInitializer.Initialize());
            Write<T, TDto>(repository, path, mapper);
        }
    }

    public static void Write<T, TDto>(this FileRepository<T> repository, string path, IMapper mapper) where T : class
    {
        var dto = mapper.Map<TDto[]>(repository);
        var json = JsonConvert.SerializeObject(dto);
        File.WriteAllText(path, json, Encoding.UTF8);
    }
}