﻿using System.Diagnostics;
using CoLa.BimEd.Infrastructure.Framework.Collections;

namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Используется для работы с коллекцией элементов типа T без возможности сохранения её в файл.
/// </summary>
/// <typeparam name="T">Тип элементов коллекции</typeparam>
public abstract class MemoryRepository<T> : Repository<T> where T : class
{
    protected MemoryRepository(IRepositoryInitializer<T> repositoryInitializer) :
        base(repositoryInitializer) => Load();
    public sealed override void Load() => this.ReplaceAll(RepositoryInitializer.Initialize());
    public override void Save() => Debug.WriteLine($"Repository<{typeof(T).Name}> is configured to store only in memory. It will not be written.", GetType().Namespace);
    public override void Save(T entity) => Debug.WriteLine($"Repository<{typeof(T).Name}> is configured to store only in memory. It will not be written.", GetType().Namespace);
}