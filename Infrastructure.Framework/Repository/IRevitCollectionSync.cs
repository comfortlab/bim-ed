namespace CoLa.BimEd.Infrastructure.Framework.Repository;

/// <summary>
/// Представляет логику синхронизации коллекций элеметов Revit и коллекций элементов доменной модели приложения.
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения</typeparam>
public interface IRevitCollectionSync<T> : IRevitSync<T> where T : class
{
    public abstract Repository<T> LoadFromRevit();
}