﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions.Impl;

public abstract class EnumBinder : IExpressionBinder
{
    public MemberAssignment Build(IConfigurationProvider configuration, PropertyMap propertyMap, TypeMap propertyTypeMap, ExpressionRequest request, ExpressionResolutionResult result, IDictionary<ExpressionRequest, int> typePairCount, LetPropertyMaps letPropertyMaps)
        => Expression.Bind(propertyMap.DestinationMember, Expression.Convert(result.ResolutionExpression, propertyMap.DestinationType));
    public abstract bool IsMatch(PropertyMap propertyMap, TypeMap propertyTypeMap, ExpressionResolutionResult result);
}