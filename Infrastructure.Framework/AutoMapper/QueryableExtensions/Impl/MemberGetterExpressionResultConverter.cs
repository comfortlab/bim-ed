﻿using System.ComponentModel;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions.Impl;

[EditorBrowsable(EditorBrowsableState.Never)]
public class MemberGetterExpressionResultConverter : IExpressionResultConverter
{
    public ExpressionResolutionResult GetExpressionResolutionResult(ExpressionResolutionResult expressionResolutionResult, IMemberMap propertyMap, LetPropertyMaps letPropertyMaps)
        => new(propertyMap.SourceMembers.MemberAccesses(propertyMap.CheckCustomSource(expressionResolutionResult, letPropertyMaps)));
    public bool CanGetExpressionResolutionResult(ExpressionResolutionResult expressionResolutionResult, IMemberMap propertyMap) => 
        propertyMap.SourceMembers.Count > 0;
}