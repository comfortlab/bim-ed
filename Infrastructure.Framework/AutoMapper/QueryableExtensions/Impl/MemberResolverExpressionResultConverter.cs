﻿using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions.Impl;

[EditorBrowsable(EditorBrowsableState.Never)]
public class MemberResolverExpressionResultConverter : IExpressionResultConverter
{
    public ExpressionResolutionResult GetExpressionResolutionResult(
        ExpressionResolutionResult expressionResolutionResult, IMemberMap propertyMap, LetPropertyMaps letPropertyMaps)
    {
        var mapFrom = propertyMap.CustomMapExpression;
        if (!IsSubQuery() || letPropertyMaps.ConfigurationProvider.ResolveTypeMap(propertyMap.SourceType, propertyMap.DestinationType) == null)
        {
            return new ExpressionResolutionResult(mapFrom.ReplaceParameters(propertyMap.CheckCustomSource(expressionResolutionResult, letPropertyMaps)));
        }
        var customSource = propertyMap.ProjectToCustomSource;
        if (customSource == null)
        {
            return new ExpressionResolutionResult(letPropertyMaps.GetSubQueryMarker(mapFrom), mapFrom);
        }
        var newMapFrom = IncludedMember.Chain(customSource, mapFrom);
        return new ExpressionResolutionResult(letPropertyMaps.GetSubQueryMarker(newMapFrom), newMapFrom);
        bool IsSubQuery()
        {
            if (mapFrom.Body is not MethodCallExpression methodCall)
            {
                return false;
            }
            var method = methodCall.Method;
            return method.IsStatic && method.DeclaringType == typeof(Enumerable);
        }
    }
    public bool CanGetExpressionResolutionResult(ExpressionResolutionResult expressionResolutionResult, IMemberMap propertyMap) => propertyMap.CustomMapExpression != null;
}