﻿using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions.Impl;

public class UnderlyingTypeToEnumBinder : EnumBinder
{
    public override bool IsMatch(PropertyMap propertyMap, TypeMap propertyTypeMap, ExpressionResolutionResult result) => propertyMap.Types.IsUnderlyingTypeToEnum();
}