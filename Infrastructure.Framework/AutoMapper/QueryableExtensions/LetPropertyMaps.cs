﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Execution;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions;

using static Expression;
using static ExpressionFactory;
using TypePairCount = IDictionary<ExpressionRequest, int>;

[EditorBrowsable(EditorBrowsableState.Never)]
public class LetPropertyMaps
{
    protected LetPropertyMaps(IConfigurationProvider configurationProvider) => ConfigurationProvider = configurationProvider;

    public virtual Expression GetSubQueryMarker(LambdaExpression letExpression) => null;

    public virtual void Push(PropertyExpression propertyExpression) {}

    public virtual void Pop() {}

    public virtual int Count => 0;

    public IConfigurationProvider ConfigurationProvider { get; }

    public virtual LetPropertyMaps New() => new(ConfigurationProvider);

    public virtual QueryExpressions GetSubQueryExpression(ExpressionBuilder builder, Expression projection,
        TypeMap typeMap, ExpressionRequest request, Expression instanceParameter, TypePairCount typePairCount) =>
        new(projection);

    public readonly struct PropertyPath
    {
        public PropertyPath(PropertyExpression[] properties, LambdaExpression letExpression)
        {
            Properties = properties;
            Marker = Default(letExpression.Body.Type);
            LetExpression = letExpression;
        }
        private PropertyExpression[] Properties { get; }
        public Expression Marker { get; }
        public LambdaExpression LetExpression { get; }
        public Expression GetSourceExpression(Expression parameter) => Properties.Take(Properties.Length - 1).Select(p=>p.Expression).Chain(parameter);
        public PropertyDescription GetPropertyDescription() => new("__" + string.Join("#", Properties.Select(p => p.PropertyMap.DestinationName)), LetExpression.Body.Type);
        internal bool IsEquivalentTo(PropertyPath other) => LetExpression == other.LetExpression && Properties.Length == other.Properties.Length &&
                                                            Properties.Take(Properties.Length - 1).Zip(other.Properties, (left, right) => left.PropertyMap == right.PropertyMap).All(item => item);
    }
}