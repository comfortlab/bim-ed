﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions;

public class ProjectionExpression
{
    private static readonly MethodInfo QueryableSelectMethod = FindQueryableSelectMethod();

    private readonly IQueryable _source;
    private readonly IExpressionBuilder _builder;

    public ProjectionExpression(IQueryable source, IExpressionBuilder builder)
    {
        _source = source;
        _builder = builder;
    }

    public IQueryable<TResult> To<TResult>(IDictionary<string, object> parameters, string[] membersToExpand) =>
        ToCore<TResult>(parameters, membersToExpand.Select(memberName => ReflectionHelper.GetMapMemberPath(typeof(TResult), memberName)));

    public IQueryable<TResult> To<TResult>(object parameters, Expression<Func<TResult, object>>[] membersToExpand) =>
        ToCore<TResult>(parameters, membersToExpand.Select(MapMemberVisitor.GetMemberPath));

    public IQueryable To(Type destinationType, object parameters, string[] membersToExpand) =>
        ToCore(destinationType, parameters, membersToExpand.Select(memberName => ReflectionHelper.GetMapMemberPath(destinationType, memberName)));

    private IQueryable<TResult> ToCore<TResult>(object parameters, IEnumerable<IEnumerable<MemberInfo>> memberPathsToExpand) =>
        (IQueryable<TResult>)ToCore(typeof(TResult), parameters, memberPathsToExpand);

    private IQueryable ToCore(Type destinationType, object parameters, IEnumerable<IEnumerable<MemberInfo>> memberPathsToExpand)
    {
        var members = memberPathsToExpand.SelectMany(m => m).Distinct().ToArray();
        return _builder.GetMapExpression(_source.ElementType, destinationType, parameters, members).Aggregate(_source, Select);
    }

    private static IQueryable Select(IQueryable source, LambdaExpression lambda) => source.Provider.CreateQuery(
        Expression.Call(
            null,
            QueryableSelectMethod.MakeGenericMethod(source.ElementType, lambda.ReturnType),
            new[] { source.Expression, Expression.Quote(lambda) }
        )
    );

    private static MethodInfo FindQueryableSelectMethod()
    {
        Expression<Func<IQueryable<object>>> select = () => default(IQueryable<object>).Select(default(Expression<Func<object, object>>));
        var method = ((MethodCallExpression)select.Body).Method.GetGenericMethodDefinition();
        return method;
    }
}