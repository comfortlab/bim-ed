﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions;

public interface IExpressionBuilder
{
    LambdaExpression[] GetMapExpression(
        Type sourceType,
        Type destinationType,
        object parameters,
        MemberInfo[] membersToExpand);

    LambdaExpression[] CreateMapExpression(
        ExpressionRequest request,
        IDictionary<ExpressionRequest, int> typePairCount,
        LetPropertyMaps letPropertyMaps);

    Expression CreateMapExpression(
        ExpressionRequest request,
        Expression instanceParameter,
        IDictionary<ExpressionRequest, int> typePairCount,
        LetPropertyMaps letPropertyMaps);
}