﻿using System.Linq.Expressions;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions;

public class PropertyExpression
{
    public PropertyExpression(PropertyMap propertyMap) => PropertyMap = propertyMap;
    public Expression Expression { get; set; }
    public PropertyMap PropertyMap { get; }
}