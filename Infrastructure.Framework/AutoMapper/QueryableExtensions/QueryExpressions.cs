﻿using System.Linq.Expressions;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.QueryableExtensions;

public readonly struct QueryExpressions
{
    public QueryExpressions(Expression first, Expression second = null, ParameterExpression secondParameter = null)
    {
        First = first;
        Second = second;
        SecondParameter = secondParameter;
    }
    public Expression First { get; }
    public Expression Second { get; }
    public ParameterExpression SecondParameter { get; }
}