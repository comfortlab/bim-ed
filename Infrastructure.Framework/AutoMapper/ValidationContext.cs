﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

public readonly struct ValidationContext
{
    public IObjectMapper ObjectMapper { get; }
    public IMemberMap MemberMap { get; }
    public TypeMap TypeMap { get; }
    public TypePair Types { get; }

    public ValidationContext(TypePair types, IMemberMap memberMap, IObjectMapper objectMapper) : this(types, memberMap, objectMapper, null)
    {
    }

    public ValidationContext(TypePair types, IMemberMap memberMap, TypeMap typeMap) : this(types, memberMap, null, typeMap)
    {
    }

    private ValidationContext(TypePair types, IMemberMap memberMap, IObjectMapper objectMapper, TypeMap typeMap)
    {
        ObjectMapper = objectMapper;
        TypeMap = typeMap;
        Types = types;
        MemberMap = memberMap;
    }
}