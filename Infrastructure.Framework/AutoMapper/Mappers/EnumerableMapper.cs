﻿using System.Collections.Generic;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Mappers;

public class EnumerableMapper : EnumerableMapperBase
{
    public override bool IsMatch(TypePair context) => 
        context.SourceType.IsEnumerableType() && context.DestinationType.IsInterface && context.DestinationType.IsEnumerableType();

    public override Expression MapExpression(IConfigurationProvider configurationProvider, ProfileMap profileMap,
        IMemberMap memberMap, Expression sourceExpression, Expression destExpression, Expression contextExpression)
    {
        if(destExpression.Type.IsInterface)
        {
            var listType = typeof(ICollection<>).MakeGenericType(ElementTypeHelper.GetElementType(destExpression.Type));
            destExpression = Expression.Convert(destExpression, listType);
        }
        return CollectionMapperExpressionFactory.MapCollectionExpression(configurationProvider, profileMap, memberMap, sourceExpression,
            destExpression, contextExpression, typeof(List<>), CollectionMapperExpressionFactory.MapItemExpr);
    }
}