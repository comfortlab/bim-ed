﻿using System;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Mappers;

public class EnumToEnumMapper : IObjectMapper
{
    public bool IsMatch(TypePair context) => context.IsEnumToEnum();
    public Expression MapExpression(IConfigurationProvider configurationProvider, ProfileMap profileMap,
        IMemberMap memberMap, Expression sourceExpression, Expression destExpression, Expression contextExpression)
    {
        var destinationType = destExpression.Type;
        var sourceToObject = sourceExpression.ToObject();
        var toObject = Expression.Call(typeof(Enum), "ToObject", null, Expression.Constant(destinationType), sourceToObject);
        var castToObject = Expression.Convert(toObject, destinationType);
        var isDefined = Expression.Call(typeof(Enum), "IsDefined", null, Expression.Constant(sourceExpression.Type), sourceToObject);
        var sourceToString = Expression.Call(sourceExpression, "ToString", null);
        var result = Expression.Variable(destinationType, "destinationEnumValue");
        var ignoreCase = Expression.Constant(true);
        var tryParse = Expression.Call(typeof(Enum), "TryParse", new[] { destinationType }, sourceToString, ignoreCase, result);
        return Expression.Block(new[] { result }, Expression.Condition(isDefined, Expression.Condition(tryParse, result, castToObject), castToObject));
    }
}