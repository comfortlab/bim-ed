﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Mappers;

public class FlagsEnumMapper : IObjectMapper
{
    private static readonly MethodInfo EnumParseMethod = ExpressionFactory.Method(() => Enum.Parse(null, null, true));

    public bool IsMatch(TypePair context) => 
        context.IsEnumToEnum() && context.SourceType.Has<FlagsAttribute>() && context.DestinationType.Has<FlagsAttribute>();

    public Expression MapExpression(IConfigurationProvider configurationProvider, ProfileMap profileMap,
        IMemberMap memberMap, Expression sourceExpression, Expression destExpression,
        Expression contextExpression) =>
        ExpressionFactory.ToType(
            Expression.Call(EnumParseMethod,
                Expression.Constant(destExpression.Type),
                Expression.Call(sourceExpression, sourceExpression.Type.GetRuntimeMethod("ToString", Type.EmptyTypes)),
                Expression.Constant(true)
            ),
            destExpression.Type
        );
}