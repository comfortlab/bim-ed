﻿using System.Collections.Generic;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Mappers;

public class CollectionMapper : EnumerableMapperBase
{
    public override bool IsMatch(TypePair context) => context.SourceType.IsEnumerableType() && 
                                                      (context.DestinationType.IsCollectionType() || context.DestinationType.IsListType());

    public override Expression MapExpression(IConfigurationProvider configurationProvider, ProfileMap profileMap,
        IMemberMap memberMap, Expression sourceExpression, Expression destExpression, Expression contextExpression)
        => CollectionMapperExpressionFactory.MapCollectionExpression(configurationProvider, profileMap, memberMap, sourceExpression, destExpression, contextExpression, typeof(List<>), CollectionMapperExpressionFactory.MapItemExpr);
}