﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

public enum ElementTypeFlags
{
    None = 0,
    BreakKeyValuePair = 1
}