﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

public interface IObjectMapperInfo : IObjectMapper
{
    TypePair GetAssociatedTypes(TypePair initialTypes);
}