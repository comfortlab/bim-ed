﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper.Internal;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

using static Expression;

[EditorBrowsable(EditorBrowsableState.Never)]
[DebuggerDisplay("{MemberExpression}, {TypeMap}")]
public class IncludedMember : IEquatable<IncludedMember>
{
    public IncludedMember(TypeMap typeMap, LambdaExpression memberExpression) : this(typeMap, memberExpression,
        Variable(memberExpression.Body.Type, string.Join("", memberExpression.GetMembersChain().Select(m => m.Name))), memberExpression)
    {
    }
    private IncludedMember(TypeMap typeMap, LambdaExpression memberExpression, ParameterExpression variable, LambdaExpression projectToCustomSource)
    {
        TypeMap = typeMap;
        MemberExpression = memberExpression;
        Variable = variable;
        ProjectToCustomSource = projectToCustomSource;
    }
    public IncludedMember Chain(IncludedMember other)
    {
        if (other == null)
        {
            return this;
        }
        return new IncludedMember(other.TypeMap, Chain(other.MemberExpression), other.Variable, Chain(MemberExpression, other.MemberExpression));
    }
    public static LambdaExpression Chain(LambdaExpression customSource, LambdaExpression lambda) => 
        Lambda(lambda.ReplaceParameters(customSource.Body), customSource.Parameters);
    public TypeMap TypeMap { get; }
    public LambdaExpression MemberExpression { get; }
    public ParameterExpression Variable { get; }
    public LambdaExpression ProjectToCustomSource { get; }
    public LambdaExpression Chain(LambdaExpression lambda) => Lambda(lambda.ReplaceParameters(Variable), lambda.Parameters);
    public bool Equals(IncludedMember other) => TypeMap == other?.TypeMap;
    public override int GetHashCode() => TypeMap.GetHashCode();
}