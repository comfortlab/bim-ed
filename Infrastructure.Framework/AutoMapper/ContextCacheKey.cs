﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

public readonly struct ContextCacheKey : IEquatable<ContextCacheKey>
{
    public static bool operator ==(ContextCacheKey left, ContextCacheKey right) => left.Equals(right);
    public static bool operator !=(ContextCacheKey left, ContextCacheKey right) => !left.Equals(right);
    private readonly Type _destinationType;
    public ContextCacheKey(object source, Type destinationType)
    {
        Source = source;
        _destinationType = destinationType;
    }
    public object Source { get; }
    public override int GetHashCode() => HashCodeCombiner.Combine(Source, _destinationType);
    public bool Equals(ContextCacheKey other) => Source == other.Source && _destinationType == other._destinationType;
    public override bool Equals(object other) => other is ContextCacheKey otherKey && Equals(otherKey);
}