﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration.Conventions;

public interface IGetTypeInfoMembers
{
    IEnumerable<MemberInfo> GetMemberInfos(TypeDetails typeInfo);
    IGetTypeInfoMembers AddCondition(Func<MemberInfo, bool> predicate);
}