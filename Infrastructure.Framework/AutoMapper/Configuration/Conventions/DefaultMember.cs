﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration.Conventions;

[EditorBrowsable(EditorBrowsableState.Never)]
public class DefaultMember : IChildMemberConfiguration
{
    public IParentSourceToDestinationNameMapper NameMapper { get; set; }

    public bool MapDestinationPropertyToSource(ProfileMap options, TypeDetails sourceType, Type destType, Type destMemberType, string nameToSearch, LinkedList<MemberInfo> resolvers, IMemberConfiguration parent = null, bool isReverseMap = false)
    {
        if (string.IsNullOrEmpty(nameToSearch))
            return true;
        var matchingMemberInfo = NameMapper.GetMatchingMemberInfo(sourceType, destType, destMemberType, nameToSearch);

        if (matchingMemberInfo != null)
            resolvers.AddLast(matchingMemberInfo);
        return matchingMemberInfo != null;
    }
}