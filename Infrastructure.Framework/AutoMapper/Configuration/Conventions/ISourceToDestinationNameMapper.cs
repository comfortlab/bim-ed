﻿using System;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration.Conventions;

public interface ISourceToDestinationNameMapper
{
    MemberInfo GetMatchingMemberInfo(IGetTypeInfoMembers getTypeInfoMembers, TypeDetails typeInfo, Type destType, Type destMemberType, string nameToSearch);
}