﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration;

public interface IMemberConfigurationProvider
{
    void ApplyConfiguration(IMemberConfigurationExpression memberConfigurationExpression);
}