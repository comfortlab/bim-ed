﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration;

public interface ISourceMemberConfiguration
{
    void Configure(TypeMap typeMap);
}