﻿using System;
using System.Collections.Generic;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration;

public interface IConfiguration : IProfileConfiguration
{
    Func<Type, object> ServiceCtor { get; }
    IEnumerable<IProfileConfiguration> Profiles { get; }
}