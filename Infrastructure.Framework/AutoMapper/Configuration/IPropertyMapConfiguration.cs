﻿using System.Linq.Expressions;
using System.Reflection;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Configuration;

public interface IPropertyMapConfiguration
{
    void Configure(TypeMap typeMap);
    MemberInfo DestinationMember { get; }
    LambdaExpression SourceExpression { get; }
    LambdaExpression GetDestinationExpression();
    IPropertyMapConfiguration Reverse();
}