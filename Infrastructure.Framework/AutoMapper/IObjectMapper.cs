﻿using System.Linq.Expressions;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

/// <summary>
/// Mapping execution strategy, as a chain of responsibility
/// </summary>
public interface IObjectMapper
{
    /// <summary>
    /// When true, the mapping engine will use this mapper as the strategy
    /// </summary>
    /// <param name="context">Resolution context</param>
    /// <returns>Is match</returns>
    bool IsMatch(TypePair context);

    /// <summary>
    /// Builds a mapping expression equivalent to the base Map method
    /// </summary>
    /// <param name="configurationProvider"></param>
    /// <param name="profileMap"></param>
    /// <param name="memberMap"></param>
    /// <param name="sourceExpression">Source parameter</param>
    /// <param name="destExpression">Destination parameter</param>
    /// <param name="contextExpression">ResolutionContext parameter</param>
    /// <returns>Map expression</returns>
    Expression MapExpression(
        IConfigurationProvider configurationProvider,
        ProfileMap profileMap,
        IMemberMap memberMap,
        Expression sourceExpression,
        Expression destExpression,
        Expression contextExpression);
}