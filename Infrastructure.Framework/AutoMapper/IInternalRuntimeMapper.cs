﻿using System;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

internal interface IInternalRuntimeMapper : IRuntimeMapper, IMapperBase
{
    TDestination Map<TSource, TDestination>(
        TSource source,
        TDestination destination,
        ResolutionContext context,
        Type sourceType = null,
        Type destinationType = null,
        IMemberMap memberMap = null);

    ResolutionContext DefaultContext { get; }
}