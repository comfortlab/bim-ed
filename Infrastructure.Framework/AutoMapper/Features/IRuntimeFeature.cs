﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Features;

public interface IRuntimeFeature
{
    void Seal(IConfigurationProvider configurationProvider);
}