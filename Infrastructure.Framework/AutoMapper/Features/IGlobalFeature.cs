﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper.Features;

public interface IGlobalFeature
{
    void Configure(IConfigurationProvider configurationProvider);
}