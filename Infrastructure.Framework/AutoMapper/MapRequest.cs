﻿using System;
using System.Diagnostics;

namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

[DebuggerDisplay("{RequestedTypes.SourceType.Name}, {RequestedTypes.DestinationType.Name} : {RuntimeTypes.SourceType.Name}, {RuntimeTypes.DestinationType.Name}")]
public readonly struct MapRequest : IEquatable<MapRequest>
{
    public TypePair RequestedTypes { get; }
    public TypePair RuntimeTypes { get; }
    public IMemberMap MemberMap { get; }

    public MapRequest(TypePair requestedTypes, TypePair runtimeTypes, IMemberMap memberMap = null) 
    {
        RequestedTypes = requestedTypes;
        RuntimeTypes = runtimeTypes;
        MemberMap = memberMap;
    }

    public bool Equals(MapRequest other) => 
        RequestedTypes.Equals(other.RequestedTypes) && RuntimeTypes.Equals(other.RuntimeTypes) && Equals(MemberMap, other.MemberMap);

    public override bool Equals(object obj) => obj is MapRequest other && Equals(other);

    public override int GetHashCode()
    {
        var hashCode = HashCodeCombiner.Combine(RequestedTypes, RuntimeTypes);
        if(MemberMap != null)
        {
            hashCode = HashCodeCombiner.Combine(hashCode, MemberMap.GetHashCode());
        }
        return hashCode;
    }

    public static bool operator ==(MapRequest left, MapRequest right) => left.Equals(right);

    public static bool operator !=(MapRequest left, MapRequest right) => !left.Equals(right);
}