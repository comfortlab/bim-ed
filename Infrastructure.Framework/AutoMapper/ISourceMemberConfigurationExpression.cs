﻿namespace CoLa.BimEd.Infrastructure.Framework.AutoMapper;

public interface ISourceMemberConfigurationExpression
{
    /// <summary>
    /// Ignore this member when validating source members, MemberList.Source.
    /// Does not affect validation for the default case, MemberList.Destination.
    /// </summary>
    void DoNotValidate();
}