﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using NLog;
using NLog.Config;
using NLog.Targets;

// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.Infrastructure.Framework.Logs;

public class Logger
{
    private const string DEBUG = "DEBUG";
    private const string ERROR = "ERROR";
    private const string FATAL = "FATAL";
    private const string INFO = "INFO";
    private const string WARN = "WARN";

    private readonly NLog.Logger _logger;

    public void Debug() => Log(DEBUG, string.Empty);
    public void Error() => Log(ERROR, string.Empty);
    public void Info() => Log(INFO, string.Empty);
    public void Warn() => Log(WARN, string.Empty);
    
    public void Debug(string message) => Log(DEBUG, message);
    public void Debug(string message, Type sourceType, string sourceMethodName) => Log(DEBUG, $"{message}\n\t{sourceType.FullName}.{sourceMethodName}");
    public void Error(string message) => Log(ERROR, message);
    public void Error(string message, Type sourceType, string sourceMethodName) => Log(ERROR, $"{message}\n\t{sourceType.FullName}.{sourceMethodName}");
    public void Info(string message) => Log(INFO, message);
    public void Info(string message, Type sourceType, string sourceMethodName) => Log(INFO, $"{message}\n\t{sourceType.FullName}.{sourceMethodName}");
    public void Warn(string message) => Log(WARN, message);
    public void Warn(string message, Type sourceType, string sourceMethodName) => Log(WARN, $"{message}\n\t{sourceType.FullName}.{sourceMethodName}");

    public void Debug(Exception exception) => Log(DEBUG, exception);
    public void Error(Exception exception) => Log(ERROR, exception);
    public void Info(Exception exception) => Log(INFO, exception);
    public void Warn(Exception exception) => Log(WARN, exception);

    private void Log(string level, string message)
    {
        switch (level)
        {
            case DEBUG: _logger.Debug(message); break;
            case ERROR: _logger.Error(message); break;
            case FATAL: _logger.Fatal(message); break;
            case INFO: _logger.Info(message); break;
            case WARN: _logger.Warn(message); break;
        }

        OnLog?.Invoke(_logger, new LoggerEventArgs(message, level));
    }

    private void Log(string level, Exception exception)
    {
        switch (level)
        {
            case DEBUG: _logger.Debug(exception); break;
            case ERROR: _logger.Error(exception); break;
            case FATAL: _logger.Fatal(exception); break;
            case INFO: _logger.Info(exception); break;
            case WARN: _logger.Warn(exception); break;
        }

        OnLog?.Invoke(_logger, new LoggerEventArgs($"{exception.Message}\n{exception.StackTrace}", level));
    }

    public Logger(AppInfo appInfo, AppPaths appPaths)
    {
        try
        {
            var applicationName = $"{appInfo.Name}-{appInfo.Version}";
            var logsPath = appPaths.Logs;
            var logFile = Path.Combine(logsPath, "Debug.log");
            var config = LogManager.Configuration ?? new LoggingConfiguration();

            var fileTarget = new FileTarget(applicationName)
            {
                FileName = logFile,
                Layout = "${longdate}|${uppercase:${level}}|${message} ",
                ArchiveFileName = logsPath + "/Debug.${shortdate}.log",
                ArchiveEvery = FileArchivePeriod.Day,
                MaxArchiveDays = 5,
            };

            config.AddTarget(fileTarget);
            config.AddRuleForAllLevels(fileTarget, applicationName);

            // var consoleTarget = new ConsoleTarget(name: $"{applicationName}_ConsoleTarget")
            // {
            //     Layout = "${longdate}|${uppercase:${level}}|${message} ",
            // };

            // config.AddTarget(consoleTarget);
            // config.AddRuleForAllLevels(consoleTarget);

            LogManager.Configuration = config;

            _logger = LogManager.GetLogger(applicationName);

            // var listener = new NLogTraceListener()
            var listener = new LoggerTraceListener(this)
            {
                Name = _logger.Name,
                // DisableFlush = true,
            };

            System.Diagnostics.Debug.Listeners.Add(listener);
        }
        catch (Exception exception)
        {
            Console.WriteLine($"{exception.Message}\n{exception.StackTrace}");
        }
    }

    public delegate void LoggerEventHandler(object sender, LoggerEventArgs args);

    public event LoggerEventHandler OnLog;
    public class LoggerEventArgs : EventArgs
    {
        public LoggerEventArgs(string message, string level = null)
        {
            Message = message;
            Level = level;
            LevelMessage = $"{(string.IsNullOrWhiteSpace(Level) ? string.Empty : $"{Level}|")}{Message}\n";
            TimeLevelMessage = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss.ffffff}|{LevelMessage}\n";
        }

        public string Message { get; }
        public string Level { get; }
        public string LevelMessage { get; }
        public string TimeLevelMessage { get; }
        public override string ToString() => TimeLevelMessage;
    }

    private class LoggerTraceListener : TraceListener
    {
        private readonly Logger _logger;
        public LoggerTraceListener(Logger logger) => _logger = logger;

        public override void Write(string message)
        {
            // Nothing, because message can be sent from other application
        }

        public override void WriteLine(string message)
        {
            // Nothing, because message can be sent from other application
        }

        public override void Write(string message, string category)
        {
            if (IsSentFromSameApplication(category))
                _logger.Debug(message);
        }

        public override void WriteLine(string message, string category)
        {
            if (IsSentFromSameApplication(category))
                _logger.Debug(message);
        }

        public override void Write(object obj)
        {
            if (obj is Exception exception && IsThrownFromSameApplication(exception))
                _logger.Error(exception);
        }

        public override void WriteLine(object obj)
        {
            if (obj is Exception exception && IsThrownFromSameApplication(exception))
                _logger.Error(exception);
        }

        private bool IsSentFromSameApplication(string @namespace) => GetRootNamespace(GetType()) == GetRootNamespace(@namespace);
        private bool IsThrownFromSameApplication(Exception exception) => GetRootNamespace(GetType()) == GetRootNamespace(exception.TargetSite?.DeclaringType);
        private static string GetRootNamespace(Type type, int count = 2) => GetRootNamespace(type?.Namespace, count);
        private static string GetRootNamespace(string @namespace, int count = 2) => @namespace?.Split('.').Take(count).JoinToString(".");
    }
}