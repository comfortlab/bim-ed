using System;
using System.IO;
using System.Reflection;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;

namespace CoLa.BimEd.Infrastructure.Framework;

public class AssemblyCommands
{
    private readonly Assembly _assembly;
    private readonly IServiceProvider _serviceProvider;
    
    public AssemblyCommands(string assemblyPath, IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        _assembly = AppDomain.CurrentDomain.Load(File.ReadAllBytes(assemblyPath));
    }

    public void Execute(string name)
    {
        try
        {
            var type = _assembly.GetType(name);
            var command = type.GetConstructor(Type.EmptyTypes)?.Invoke(Array.Empty<object>());
            var execute = type.GetMethod(nameof(ServiceCommand.Execute), new [] { typeof(IServiceProvider)});
            execute?.Invoke(command, new object[] { _serviceProvider });
            // AppDomain.Unload(AppDomain.CreateDomain("Assembly", _assembly.Evidence));
        }
        catch (OperationCanceledException)
        {
            _serviceProvider.Get<Logger>().Warn($"{GetType().Name} was canceled.");
        }
        catch (Exception exception)
        {
            _serviceProvider.Get<Logger>().Error(exception);
        }
    }
}