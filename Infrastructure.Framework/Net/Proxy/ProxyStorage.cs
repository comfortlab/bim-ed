﻿using System.IO;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.Infrastructure.Framework.Net.Proxy;

public class ProxyStorage
{
    private readonly string _path;
    
    public ProxyStorage(AppPaths netPath)
    {
        _path = netPath.Proxy;
    }
    
    public ProxyConfig Read()
    {
        if (!File.Exists(_path))
        {
            return new ProxyConfig();
        }
        
        var content = File.ReadAllLines(_path);

        return content.Length switch
        {
            3 or 4 => new ProxyConfig(
                proxyType: int.TryParse(content[0], out var proxyType) ? (ProxyType)proxyType : ProxyType.None,
                host: content[1],
                port: int.TryParse(content[2], out var port) ? port : 0),
            
            5 => new ProxyConfig(
                proxyType: int.TryParse(content[0], out var proxyType) ? (ProxyType)proxyType : ProxyType.None,
                host: content[1],
                port: int.TryParse(content[2], out var port) ? port : 0,
                userName: content[3],
                password: EncryptionUtils.DecryptForCurrentUser(content[4])),
            
            _ => new ProxyConfig(),
        };
    }

    public void Write(ProxyConfig proxyConfig)
    {
        File.WriteAllLines(_path, new[]
        {
            ((int)proxyConfig.ProxyType).ToString(),
            proxyConfig.Host,
            proxyConfig.Port.ToString(),
            proxyConfig.UserName,
            EncryptionUtils.EncryptForCurrentUser(proxyConfig.Password),
        });
    }
}