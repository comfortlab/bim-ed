﻿namespace CoLa.BimEd.Infrastructure.Framework.Net.Proxy
{
    public class ProxyConfig
    {
        public ProxyConfig() { }

        public ProxyConfig(ProxyType proxyType, string host, int port)
        {
            ProxyType = proxyType;
            Host = host;
            Port = port;
        }

        public ProxyConfig(ProxyType proxyType, string host, int port, string userName, string password)
        {
            ProxyType = proxyType;
            Host = host;
            Port = port;
            UserName = userName;
            Password = password;
        }

        public ProxyType ProxyType { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
