﻿namespace CoLa.BimEd.Infrastructure.Framework.Net.Proxy
{
    public enum ProxyType
    {
        None = 0,
        Http = 1,
        Socks5 = 2
    }
}