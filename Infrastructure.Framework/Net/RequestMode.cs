﻿namespace CoLa.BimEd.Infrastructure.Framework.Net;

public enum RequestMode
{
    Authenticate,
    CheckAuthentication,
    RefreshToken,
    SendStats,
}