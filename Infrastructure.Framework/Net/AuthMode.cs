namespace CoLa.BimEd.Infrastructure.Framework.Net;

public enum AuthMode
{
    LogIn,
    LogOut,
    ChangePassword,
}