﻿namespace CoLa.BimEd.Infrastructure.Framework.Net;

public enum BrowserStartupMode
{
    Login = 1,
    Logout,
    RefreshToken,
    ChangedPassword,
    SelectorEmpty,
    SelectorConfigured
}