﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Framework.Net;

public class AppHttpClient
{
    protected readonly HttpClientAccessor HttpClientAccessor;
    private readonly HttpClient _httpClient;
    
    public AppHttpClient(
        HttpClientAccessor httpClientAccessor,
        string scheme,
        string token,
        string baseAddress)
    {
        HttpClientAccessor = httpClientAccessor;
        HttpClientAccessor.SetAuthorization(scheme, token);
        HttpClientAccessor.SetBaseAddress(baseAddress);
        _httpClient = HttpClientAccessor.HttpClient;
    }
    
    public AppHttpClient(HttpClientAccessor httpClientAccessor)
    {
        HttpClientAccessor = httpClientAccessor;
        _httpClient = HttpClientAccessor.HttpClient;
    }

    public List<JsonConverter> JsonConverters { get; set; }

    public async Task<HttpResponseMessage> GetAsync(string requestUrl)
    {
        try
        {
            return await _httpClient.GetAsync(new Uri(requestUrl));
        }
        catch (HttpRequestException exception)
        {
            LogHttpRequestWarn(exception, requestUrl, nameof(GetAsync));
            return null;
        }
    }

    public async Task<string> GetStringAsync(string requestUrl)
    {
        try
        {
            return await _httpClient.GetStringAsync(requestUrl);
        }
        catch (HttpRequestException exception)
        {
            LogHttpRequestWarn(exception, requestUrl, nameof(GetStringAsync));
            return null;
        }
    }

    public async Task<HttpResponseMessage> PostAsync(string requestUrl, HttpContent httpContent)
    {
        try
        {
            return await _httpClient.PostAsync(requestUrl, httpContent);
        }
        catch (HttpRequestException exception)
        {
            LogHttpRequestWarn(exception, requestUrl, nameof(PostAsync));
            return null;
        }
    }

    public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage)
    {
        try
        {
            return await _httpClient.SendAsync(httpRequestMessage);
        }
        catch (HttpRequestException exception)
        {
            LogHttpRequestWarn(exception, httpRequestMessage.RequestUri.AbsoluteUri, nameof(SendAsync));
            return null;
        }
    }

    private void LogHttpRequestWarn(HttpRequestException exception, string requestUri, string methodName)
    {
        Debug.WriteLine(exception);
        Debug.WriteLine($"{GetType().FullName}.{methodName}");
        Debug.WriteLine(requestUri);
    }
    
    private T DeserializeJsonFromStream<T>(Stream stream)
    {
        if (stream is not { CanRead: true })
        {
            return default;
        }

        using var streamReader = new StreamReader(stream);
        using var jsonTextReader = new JsonTextReader(streamReader);
        var jsonSerializer = new JsonSerializer();
        if (JsonConverters != null)
        {
            foreach (var jsonConverter in JsonConverters)
            {
                jsonSerializer.Converters.Add(jsonConverter);
            }
        }
        var result = jsonSerializer.Deserialize<T>(jsonTextReader);
        return result;
    }

    protected async Task<T> SendRequestAsync<T>(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        using var response = await _httpClient.SendAsync(request, cancellationToken);
        return await ParseResponseAsync<T>(response);
    }

    private async Task<T> ParseResponseAsync<T>(HttpResponseMessage response)
    {
        var stream = await response.Content.ReadAsStreamAsync();

        if (response.IsSuccessStatusCode)
        {
            return DeserializeJsonFromStream<T>(stream);
        }

        var content = await StreamToStringAsync(stream);

        throw new Exception(
            $"StatusCode: {(int)response.StatusCode}\n\t" +
            $"Content: {content}");
    }

    private static async Task<string> StreamToStringAsync(Stream stream)
    {
        if (stream == null)
        {
            return null;
        }

        using var streamReader = new StreamReader(stream);
        return await streamReader.ReadToEndAsync();
    }

    public virtual async Task<bool> IsEntityPresent<T>(CancellationToken cancellationToken, string apiUriString)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get, apiUriString);
        using var response = await _httpClient.SendAsync(request, cancellationToken);
        return response.IsSuccessStatusCode;
    }

    public virtual async Task<T> GetEntityAsync<T>(CancellationToken cancellationToken, string apiUriString)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get, apiUriString);
        return await SendRequestAsync<T>(request, cancellationToken);
    }

    public virtual async Task<T> CreateEntityAsync<T>(CancellationToken cancellationToken, string apiUriString, T entity)
    {
        using var request = new HttpRequestMessage(HttpMethod.Post, apiUriString);
        request.Content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, JsonSerializerConstants.MediaTypeAppJson);
        return await SendRequestAsync<T>(request, cancellationToken);
    }

    public virtual async Task<TS> RequestEntityAsync<TR, TS>(CancellationToken cancellationToken, string apiUriString, TR entity)
    {
        using var request = new HttpRequestMessage(HttpMethod.Post, apiUriString);
        request.Content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, JsonSerializerConstants.MediaTypeAppJson);
        return await SendRequestAsync<TS>(request, cancellationToken);
    }

    public virtual async Task SendEntityAsync<T>(CancellationToken cancellationToken, string apiUriString, T entity)
    {
        using var request = new HttpRequestMessage(HttpMethod.Post, apiUriString);
        request.Content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, JsonSerializerConstants.MediaTypeAppJson);
        await SendRequestAsync<T>(request, cancellationToken);
    }

    public virtual async Task<T> UpdateEntityAsync<T>(CancellationToken cancellationToken, string apiUriString, string content)
    {
        using var request = new HttpRequestMessage(HttpMethod.Put, apiUriString);
        request.Content = new StringContent(content, Encoding.UTF8, JsonSerializerConstants.MediaTypeAppJson);
        return await SendRequestAsync<T>(request, cancellationToken);
    }

    public virtual async Task<T> PatchEntityAsync<T>(CancellationToken cancellationToken, string apiUriString,
        string content)
    {
        using var request = new HttpRequestMessage(new HttpMethod(JsonSerializerConstants.PatchMethod), apiUriString);
        request.Content = new StringContent(content, Encoding.UTF8, JsonSerializerConstants.MediaTypeAppJson);
        return await SendRequestAsync<T>(request, cancellationToken);
    }

    public virtual async Task RemoveEntityAsync<T>(CancellationToken cancellationToken, string apiUriString)
    {
        using var request = new HttpRequestMessage(HttpMethod.Delete, apiUriString);
        await SendRequestAsync<T>(request, cancellationToken);
    }
}