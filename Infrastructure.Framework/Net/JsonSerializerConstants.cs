﻿namespace CoLa.BimEd.Infrastructure.Framework.Net;

public static class JsonSerializerConstants
{
    public const string MediaTypeAppJson = "application/json";
    public const string PatchMethod = "PATCH";
}