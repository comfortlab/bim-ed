﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using CoLa.BimEd.Infrastructure.Framework.Net.Proxy;
using MihaZupan;

namespace CoLa.BimEd.Infrastructure.Framework.Net;

public class HttpClientAccessor
{
    internal readonly HttpClient HttpClient;

    public HttpClientAccessor(ProxyStorage proxyStorage)
    {
        try
        {
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Ssl3 |
                SecurityProtocolType.Tls |
                SecurityProtocolType.Tls11 |
                SecurityProtocolType.Tls12;
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
        }

        var proxyConfig = proxyStorage?.Read();
            
        var httpClientHandler = new HttpClientHandler
        {
            CookieContainer = new CookieContainer(),
            Proxy = proxyConfig != null ? GetWebProxy(proxyConfig) : null,
            // ClientCertificateOptions = ClientCertificateOption.Manual,
            ServerCertificateCustomValidationCallback = (msg, cert, chain, errors) => true,
        };
            
        HttpClient = new HttpClient(httpClientHandler);
    }

    private static IWebProxy GetWebProxy(ProxyConfig proxyConfig)
    {
        try
        {
            IWebProxy proxy = proxyConfig.ProxyType switch
            {
                ProxyType.Http => new WebProxy(new Uri($@"http://{proxyConfig.Host}:{proxyConfig.Port}")),
                ProxyType.Socks5 => new HttpToSocks5Proxy(proxyConfig.Host, proxyConfig.Port),
                _ => null,
            };

            if (proxy != null &&
                proxyConfig.UserName != null &&
                proxyConfig.Password != null)
            {
                proxy.Credentials = new NetworkCredential(proxyConfig.UserName, proxyConfig.Password);
            }

            return proxy;
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
            return null;
        }
    }

    public void SetAuthorization(string scheme, string token) =>
        HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(scheme, token);
    
    public void SetBaseAddress(string baseAddress) =>
        HttpClient.BaseAddress = new Uri(baseAddress);
}