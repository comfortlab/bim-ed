﻿using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using Newtonsoft.Json;

namespace CoLa.BimEd.Infrastructure.Framework.Converters
{
    public class DtoConverter<TDomain, TDto>
    {
        private readonly IMapper _mapper;
        public DtoConverter(IMapper mapper) => _mapper = mapper;
        public TDomain FromDto(TDto dto) => _mapper.Map<TDomain>(dto);
        public TDto ToDto(TDomain entity) => _mapper.Map<TDto>(entity);
        public string ToJson(TDomain entity) => JsonConvert.SerializeObject(ToDto(entity));
        public TDomain FromJson(string json) => FromDto(JsonConvert.DeserializeObject<TDto>(json));
    }
}