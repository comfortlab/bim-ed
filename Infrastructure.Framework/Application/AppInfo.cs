using System;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.Infrastructure.Framework.Application;

public class AppInfo
{
    public AppInfo(string name)
    {
        Name = name;
        Version = AssemblyUtils.GetApplicationVersion().ToString();
        DotNetFrameworkVersion = DotNetFrameworkUtils.GetVersion().ToString();
        CurrentSessionGuid = Guid.NewGuid();
    }

    public string Name { get; set; }
    public string Version { get; set; }
    public string DotNetFrameworkVersion { get; set; }
    public Guid CurrentSessionGuid { get; set; }
}