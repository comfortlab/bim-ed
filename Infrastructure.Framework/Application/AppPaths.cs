using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CoLa.BimEd.Infrastructure.Framework.Application;

public class AppPaths
{
    private const string ComfortLabPath = @"ComfortLab\Browser\Application";
    public string BrowserApplicationPath { get; }
    public string CefSharpPath { get; }
    public string CefSharpLocalData { get; }
    public string CookieStore { get; }
    public string Database { get; }
    public string Families { get; }
    public string LocalData { get; }
    public string Logs { get; }
    public string ProductData { get; }
    public string ProductDataCache { get; }
    public string ProductDataCacheJson { get; }
    public string ProductDataCacheImage { get; }
    public string Proxy { get; }
    public string RefreshToken { get; } 
    public string SelectorCache { get; }
    public string Stats { get; }
    public string TemporaryFiles { get; }
    public string UserRepository { get; }

    public AppPaths(
        string applicationName,
        string browserApplicationPath,
        int revitVersionNumber = 0)
    {
        if (string.IsNullOrWhiteSpace(browserApplicationPath) || !File.Exists(browserApplicationPath))
            throw new FileNotFoundException($"Browser application was not found.");
        
        var localApplicationData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        BrowserApplicationPath = browserApplicationPath;
        LocalData = CreateFolder(localApplicationData, applicationName);
        CefSharpPath = FindChromiumPath();
        CefSharpLocalData = CreateFolder(LocalData, "CefSharp");
        CookieStore = CreateFolder(CefSharpLocalData, "CookieStore");
        RefreshToken = CreateFolder(CookieStore, "Token");
        Logs = CreateFolder(LocalData, "Logs");
        Database = CreateFolder(LocalData, "Database");
        Families = CreateFolder(LocalData, "Families", $"{revitVersionNumber}");
        ProductData = CreateFolder(Database, "ProductData");
        ProductDataCache = CreateFolder(ProductData, "Cache");
        ProductDataCacheJson = CreateFolder(ProductDataCache, "Json");
        ProductDataCacheImage = CreateFolder(ProductDataCache, "Images");
        SelectorCache = CreateFolder(LocalData, "SelectorCache");
        Stats = Path.Combine(LocalData, "Stats");
        TemporaryFiles = CreateFolder(LocalData, "Temp");
        UserRepository = CreateFolder(LocalData, "User Repository");
        
        Proxy = Path.Combine(LocalData, "Proxy");
    }
        
    public string GetProductDataCacheJson(CultureInfo cultureInfo)
    {
        return CreateFolder(ProductDataCacheJson, cultureInfo.TwoLetterISOLanguageName);
    }
        
    private static string CreateFolder(string parentDirectory, params string[] folderNames)
    {
        var newDirectory = Path.Combine(new [] { parentDirectory }.Union(folderNames).ToArray());
            
        if (!Directory.Exists(newDirectory))
            Directory.CreateDirectory(newDirectory);
            
        return newDirectory;
    }

    private static string FindChromiumPath()
    {
        var programFilesPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

        return FindChromiumPath(Path.Combine(programFilesPath, ComfortLabPath));
    }

    private static string FindChromiumPath(string sourcePath)
    {
        if (!Directory.Exists(sourcePath))
            return null;

        var directoryInfo = new DirectoryInfo(sourcePath);
        
        var versionFolderNames = directoryInfo.GetDirectories()
            .Where(x =>
            {
                var split = x.Name.Split('.');
                return split.Length is >= 2 and <= 4 && split.All(s => int.TryParse(s, out _));
            })
            .Select(x => new Version(x.Name))
            .ToArray();
        
        var lastVersionFolderName = versionFolderNames.Any()
            ? versionFolderNames.Max().ToString()
            : null;

        if (lastVersionFolderName != null)
            return Path.Combine(sourcePath, lastVersionFolderName);
        
        Debug.WriteLine($"Chromium Runtime Browser was not installed.");
        return null;
    }
}