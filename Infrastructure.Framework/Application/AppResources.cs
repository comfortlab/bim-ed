namespace CoLa.BimEd.Infrastructure.Framework.Application;

public class AppResources
{
    public AppResources(byte[] appSharedParameterFile)
    {
        AppSharedParameterFile = appSharedParameterFile;
    }

    public byte[] AppSharedParameterFile { get; set; }
}