﻿namespace CoLa.BimEd.Infrastructure.Framework.Locks;

public enum Lock
{
    Calculate,
    CalculateParents,
    Delete,
    Initialize,
    Phase,
    IsElectricalLoadCalculating,
    RefreshDevices,
    RibbonItemValues,
    Save,
    SelectItem,
    Setter,
    SetParameters,
    SpecifyAdditionalDemandFactor,
    SyncProperties,
    Update,
    UpdateInstance,
    UpdateRevitModel,
}