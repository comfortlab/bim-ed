﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoLa.BimEd.Infrastructure.Framework.Collections;

public static class EnumerableExtension
{
    public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection) =>
        collection == null ? new ObservableCollection<T>() : new ObservableCollection<T>(collection);

    public static string JoinToString<T>(this IEnumerable<T> enumerable, string separator = "\n\t") =>
        $"{(separator.StartsWith("\n") ? separator : string.Empty)}{string.Join(separator, enumerable)}";

    public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
    {
        if (enumerable == null)
            return;

        foreach (var item in enumerable)
            action.Invoke(item);
    }

    public static bool IsEmpty<T>(this IEnumerable<T> enumerable) =>
        (enumerable?.Any() ?? false) == false;
}