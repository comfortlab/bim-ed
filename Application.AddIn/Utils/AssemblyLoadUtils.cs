﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.Application.AddIn.Utils;

internal static class AssemblyLoadUtils
{
    internal static void LoadProjectAssemblies(IEnumerable<Assembly> assemblies, Logger logger)
    {
        var assemblyDirectory = Path.GetDirectoryName(AssemblyUtils.GetExecutingAssemblyPath());

        if (string.IsNullOrWhiteSpace(assemblyDirectory))
        {
            logger.Warn($"{typeof(AssemblyLoadUtils).FullName}.{nameof(LoadProjectAssemblies)}: Current assembly directory was not defined.");
            return;
        }

        foreach (var assembly in assemblies)
        {
            try
            {
                var assemblyName = assembly.GetName().Name;
                var resourcesAssemblyPath = Path.Combine(assemblyDirectory, $"{assemblyName}.dll");
            
                if (File.Exists(resourcesAssemblyPath))
                    Assembly.LoadFrom(resourcesAssemblyPath);
            }
            catch (Exception exception)
            {
                logger.Error(exception);
            }
        }
    }
}