﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using Autodesk.Revit.UI.Selection;
using CoLa.BimEd.Infrastructure.Revit.Comparers;
using CoLa.BimEd.Infrastructure.Revit.RevitVersions;

namespace CoLa.BimEd.Application.AddIn.Utils;

public static class SelectionUtils
{
    public static List<ElectricalSystem> GetSelectedElectricalSystems(
        this Selection selection,
        Document document,
        RevitVersionResolver revitVersionResolver)
    {
        return selection.GetElementIds()
            .Select(document.GetElement)
            .OfType<FamilyInstance>()
            .SelectMany(revitVersionResolver.GetElectricalSystems)
            .Where(x => x != null)
            .Distinct(new ElementComparer<ElectricalSystem>())
            .ToList();
    }
}