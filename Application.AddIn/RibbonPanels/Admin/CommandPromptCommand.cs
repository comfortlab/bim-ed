﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using Autodesk.Revit.DB.ExtensibleStorage;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.BusinessLogic.Model.Settings;
using CoLa.BimEd.DataAccess.Revit.RevitParameters;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Async;
using CoLa.BimEd.Infrastructure.Revit.Families;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.Infrastructure.Revit.Utils;
using CoLa.UI.Presentation.CommandPrompt;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Admin;

[Transaction(TransactionMode.Manual)]
public class CommandPromptCommand : DefinitionExternalCommand
{
    private DebugCommandPrompt _commandPrompt;
    private IMapper _mapper;
    private Logger _logger;

    private Document _document;

    public CommandPromptCommand() : base(
        caption: Ribbon.CommandPrompt_Caption,
        largeImage: Ribbon.CommandPrompt_LargeImage)
    { }
    
    protected override Result Execute()
    {
        RegisterCommands();
        ServiceProvider.Get<CommandPromptWindow>().Show();
        return Result.Succeeded;
    }
    
    private void RegisterCommands()
    {
        // var revitContext = ServiceProvider.Get<RevitContext>();
        _document = ServiceProvider.Get<Document>();
        _commandPrompt = ServiceProvider.Get<DebugCommandPrompt>();
        _mapper = ServiceProvider.Get<IMapper>();
        _logger = ServiceProvider.Get<Logger>();

        _commandPrompt.GetObjectFunc = id => _document.GetElement(new ElementId(id));

        DataStorageCommand();
        DataStorageResetCommand();
        LoadClassificationCommand();
        SharedParametersCommand();
        SwitchCommand();
        TraceCommand();
    }

    private void DataStorageCommand() => _commandPrompt.Register(
        command: "sw",
        commandFunc: () =>
        {
            try
            {
                RevitTask.RunAsync(() =>
                {
                    _document.ExecuteTransaction(() =>
                    {
                        var electricalSettings = ServiceProvider.Get<ElectricalSettings>();
                        electricalSettings.DesignTemperature = 1000;
                        electricalSettings.VoltageDrop = 200;
                        var settingRepository = ServiceProvider.Get<Repository<ElectricalSettings>>();
                        settingRepository.Save();

                        var operatingModes = ServiceProvider.Get<OperatingModes>();
                        operatingModes.Add(new OperatingMode("Normal"));
                        operatingModes.Add(new OperatingMode("Fault"));
                        operatingModes.Current = operatingModes.First();
                        var operatingModeRepository = ServiceProvider.Get<Repository<OperatingModes>>();
                        operatingModeRepository.Save();

                    }, "Change");
                });

                return "";
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "DataStorage test command");
    
    private void DataStorageResetCommand() => _commandPrompt.Register(
        command: "reset",
        commandFunc: () =>
        {
            try
            {
                var dataStorages = _document.GetElementsOfClass<DataStorage>().Select(x => x.Id).ToList();
                
                RevitTask.RunAsync(() =>
                {
                    _document.ExecuteTransaction(() => _document.Delete(dataStorages), "Delete");
                });
                
                var result = $"Delete: {dataStorages.Select(x => x.IntegerValue).JoinToString()}";

                return result;
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "DataStorage test command");
    
    private void LoadClassificationCommand() => _commandPrompt.Register(
        command: "lc",
        commandFunc: () =>
        {
            try
            {
                var demandFactors = ServiceProvider.Get<List<ElectricalDemandFactorDefinition>>().ToArray();
                var loadClassifications = ServiceProvider.Get<List<ElectricalLoadClassification>>();
                var demandFactorProxies = _mapper.Map<DemandFactorProxy[]>(demandFactors);
                var loadClassificationProxies = _mapper.Map<LoadClassificationProxy[]>(loadClassifications, opt => opt.Items.Add("item", demandFactorProxies));

                var result = string.Empty;
                result += "Proxy Elements:\n";
                result += demandFactorProxies.Aggregate("", (current, x) => current + $"{x.AllValuesToString()}\n");
                result += loadClassificationProxies.Aggregate("", (current, x) => current + $"{x.AllValuesToString()}\n");
                
                return result;
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "Load Classification test command");

    private void SharedParametersCommand() => _commandPrompt.Register(
        command: "shared",
        commandFunc: () =>
        {
            try
            {
                var projectSharedParameters = new List<ProjectSharedParameter>
                {
                    new(SharedParameters.App.Cable_Designation, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cable_Diameter, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cable_Length, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cable_LengthMax, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cable_Length_InCableTray, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cable_Length_OutsideCableTray, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Cables_Count, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.CableTrace, BuiltInCategory.OST_CableTray, BuiltInCategory.OST_CableTrayFitting, BuiltInCategory.OST_Conduit, BuiltInCategory.OST_ConduitFitting),
                    new(SharedParameters.App.CableTray_Filling, BuiltInCategory.OST_CableTray, BuiltInCategory.OST_CableTrayFitting, BuiltInCategory.OST_Conduit, BuiltInCategory.OST_ConduitFitting),
                    new(SharedParameters.App.CableTray_FillingPercent, BuiltInCategory.OST_CableTray, BuiltInCategory.OST_CableTrayFitting, BuiltInCategory.OST_Conduit, BuiltInCategory.OST_ConduitFitting),
                    new(SharedParameters.App.Circuit_Designation, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.Circuit_Topology, BuiltInCategory.OST_ElectricalCircuit),
                    new(SharedParameters.App.ElectricalSystemIds, _electricalCategories),
                    new(SharedParameters.App.ServiceType, _electricalDevicesCategories),
                    // new(SharedParameters.App.ReferenceIds, BuiltInCategory.OST_CableTray, BuiltInCategory.OST_Conduit),
                };

                RevitTask.RunAsync(() =>
                {
                    using var sharedParameterUtils = ServiceProvider.Get<SharedParameterUtils>();
                    sharedParameterUtils.AddSharedParametersToProject(projectSharedParameters);
                });

                return $"Shared parameters are added:{projectSharedParameters.Select(x => $"{x.Guid}").JoinToString()}";
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "Show all cable trays, conduits and fittings.");

    private void TraceCommand() => _commandPrompt.Register(
        command: "trace",
        commandFunc: () =>
        {
            try
            {
                var result = string.Empty;
                
                RevitTask.RunAsync(() =>
                {
                    result = ServiceProvider.Get<NetworkCalculator>().Calculate();
                });

                return result;
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "Show all cable trays, conduits and fittings.");

    private void SwitchCommand() => _commandPrompt.Register(
        command: "switch",
        commandFunc: () =>
        {
            try
            {
                var result = string.Empty;
                
                RevitTask.RunAsync(() =>
                {
                    var document = ServiceProvider.Get<Document>();
                    
                    var lightingDevices = document.NewElementCollector()
                        .OfClass(typeof(Family))
                        .OfType<Family>()
                        .Where(x => x.IsEditable && x.FamilyCategory.Id.IntegerValue == (int)BuiltInCategory.OST_LightingDevices)
                        .ToList();

                    var count = lightingDevices.Count;
                    var i = 1;
                    
                    foreach (var lightingDevice in lightingDevices)
                    {
                        _logger.Debug($"[{i++}/{count}] {lightingDevice.Name}");

                        try
                        {
                            var familyDocument = document.EditFamily(lightingDevice);
                            var family = familyDocument.OwnerFamily;

                            familyDocument.ExecuteTransaction(
                                () => family.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)
                                    ?.Set((int)PartType.Switch),
                                "Switch");

                            familyDocument.LoadFamily(document, new FamilyLoadOptions());
                            familyDocument.Close(saveModified: false);
                        }
                        catch (Exception exception)
                        {
                            _logger.Error(exception);
                        }
                    }

                    result = string.Join("\n", lightingDevices.Select(x => $"{x.Name} - {x.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)?.AsString()}"));
                });

                return result;
            }
            catch (Exception exception)
            {
                _logger.Error(exception);
                return string.Empty;
            }
        },
        description: "Set Switch part type for all Lighting Devices.");
    
    private readonly BuiltInCategory[] _electricalDevicesCategories = {
        BuiltInCategory.OST_ElectricalEquipment,
        BuiltInCategory.OST_ElectricalFixtures,
        BuiltInCategory.OST_CommunicationDevices,
        BuiltInCategory.OST_DataDevices,
        BuiltInCategory.OST_FireAlarmDevices,
        BuiltInCategory.OST_GenericModel,
        BuiltInCategory.OST_LightingDevices,
        BuiltInCategory.OST_LightingFixtures,
        BuiltInCategory.OST_MechanicalEquipment,
        BuiltInCategory.OST_NurseCallDevices,
        BuiltInCategory.OST_SecurityDevices,
        BuiltInCategory.OST_TelephoneDevices,
    };
    
    private readonly BuiltInCategory[] _electricalCategories = {
        BuiltInCategory.OST_CableTray,
        BuiltInCategory.OST_CableTrayFitting,
        BuiltInCategory.OST_Conduit,
        BuiltInCategory.OST_ConduitFitting,
        BuiltInCategory.OST_ElectricalEquipment,
        BuiltInCategory.OST_ElectricalFixtures,
        BuiltInCategory.OST_CommunicationDevices,
        BuiltInCategory.OST_DataDevices,
        BuiltInCategory.OST_FireAlarmDevices,
        BuiltInCategory.OST_GenericModel,
        BuiltInCategory.OST_LightingDevices,
        BuiltInCategory.OST_LightingFixtures,
        BuiltInCategory.OST_MechanicalEquipment,
        BuiltInCategory.OST_NurseCallDevices,
        BuiltInCategory.OST_SecurityDevices,
        BuiltInCategory.OST_TelephoneDevices,
    };
}