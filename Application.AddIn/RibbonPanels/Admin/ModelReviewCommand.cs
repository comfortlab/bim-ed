using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ModelReview;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Admin;

[Transaction(TransactionMode.Manual)]
public class ModelReviewCommand : DefinitionExternalCommand
{
    public ModelReviewCommand() : base(
        caption: "Model\nReview",
        largeImage: Ribbon.Setting_LargeImage)
    { }
    
    protected override Result Execute()
    {
        new ModelReviewWindow().ShowDialog();
        return Result.Succeeded;
    }
}