﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Application.AddIn.Utils;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Revit;
using CoLa.BimEd.Infrastructure.Revit.RevitVersions;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.View;

[Transaction(TransactionMode.Manual)]
public class HighlightRouteCommand : DefinitionExternalCommand
{
    private Document _document;
    private NetworkDrawer _networkDrawer;
    private RevitVersionResolver _revitVersionResolver;
    private Selection _selection;
    
    public HighlightRouteCommand() : base(
        caption: Ribbon.HighlightRoute_Caption,
        largeImage: Ribbon.HighlightRoute_LargeImage,
        toolTipText:  Ribbon.HighlightRoute_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        var logger = ServiceProvider.Get<Logger>();
        var revitContext = ServiceProvider.Get<RevitContext>();
        _revitVersionResolver = ServiceProvider.Get<RevitVersionResolver>();
        _networkDrawer = ServiceProvider.Get<NetworkDrawer>();
        _document = revitContext.Document;
        _selection = revitContext.Selection;
        var selectedElectricalSystems = _selection.GetSelectedElectricalSystems(_document, _revitVersionResolver);
        
        if (selectedElectricalSystems.Any())
        {
            Draw(selectedElectricalSystems);
        }
        else
        {
            try
            {
                while (true) PickAndDraw();
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException) { /* nothing */ }
            catch (OperationCanceledException) {  /* nothing */ }
            catch (Exception exception) { logger.Error($"{exception}"); }
        }
        
        return Result.Succeeded;
    }

    private void PickAndDraw()
    {
        var pickObject = _selection.PickObject(ObjectType.Element, StatusPromptTexts.SelectElementToHighlightRoute);

        if (_document.GetElement(pickObject.ElementId) is not FamilyInstance familyInstance)
            return;
        
        var electricalSystems = _revitVersionResolver.GetElectricalSystems(familyInstance).ToList();
        
        Draw(electricalSystems);
    }

    private void Draw(List<ElectricalSystem> electricalSystems)
    {
        _document.ExecuteTransaction(() =>
            {
                _networkDrawer.ClearView();
                electricalSystems.ForEach(x => _networkDrawer.DrawBinding(x));
                _networkDrawer.DrawRoute(electricalSystems.FirstOrDefault());
            },
            TransactionNames.CableRouting);
    }
}