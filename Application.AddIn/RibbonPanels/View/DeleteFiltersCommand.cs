﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.View;

[Transaction(TransactionMode.Manual)]
public class DeleteFiltersCommand : DefinitionExternalCommand
{
    public DeleteFiltersCommand() : base(
        caption: Ribbon.LegendDelete_Caption,
        largeImage: Ribbon.LegendDelete_LargeImage)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<NetworkDrawer>().ClearDocumentFilters();
        return Result.Succeeded;
    }
}