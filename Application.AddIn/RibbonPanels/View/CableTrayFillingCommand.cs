using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.View;

[Transaction(TransactionMode.Manual)]
public class CableTrayFillingCommand : DefinitionExternalCommand
{
    public CableTrayFillingCommand() : base(
        caption: Ribbon.CableTrayFillingFilter_Caption,
        largeImage: Ribbon.CableTrayFillingFilter_LargeImage,
        toolTipText:  Ribbon.CableTrayFillingFilter_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<NetworkDrawer>().AddCableTrayFillingFilters();
        return Result.Succeeded;
    }
}