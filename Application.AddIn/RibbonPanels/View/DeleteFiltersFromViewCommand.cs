﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.View;

[Transaction(TransactionMode.Manual)]
public class DeleteFiltersFromViewCommand : DefinitionExternalCommand
{
    public DeleteFiltersFromViewCommand() : base(
        caption: Ribbon.LegendDeleteFromView_Caption,
        largeImage: Ribbon.LegendDeleteFromView_LargeImage,
        toolTipText:  Ribbon.LegendDeleteFromView_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<NetworkDrawer>().ClearView();
        return Result.Succeeded;
    }
}