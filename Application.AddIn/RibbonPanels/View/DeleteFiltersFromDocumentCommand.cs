using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.View;

[Transaction(TransactionMode.Manual)]
public class DeleteFiltersFromDocumentCommand : DefinitionExternalCommand
{
    public DeleteFiltersFromDocumentCommand() : base(
        caption: Ribbon.LegendDeleteFromDocument_Caption,
        largeImage: Ribbon.LegendDeleteFromDocument_LargeImage,
        toolTipText:  Ribbon.LegendDeleteFromDocument_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<NetworkDrawer>().ClearDocumentFilters();
        return Result.Succeeded;
    }
}