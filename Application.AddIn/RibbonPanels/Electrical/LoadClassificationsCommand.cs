﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ElectricalSetting.Windows;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Electrical;

[Transaction(TransactionMode.Manual)]
public class LoadClassificationsCommand : DefinitionExternalCommand
{
    public LoadClassificationsCommand() : base(
        caption: Ribbon.LoadClassifications_Caption,
        largeImage: Ribbon.LoadClassifications_LargeImage,
        image: Ribbon.LoadClassifications_Image,
        toolTipText: Ribbon.LoadClassifications_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<Repository<LoadClassificationProxy>>().Load();
        ServiceProvider.Get<LoadClassificationsWindow>().ShowDialog();
        return Result.Succeeded;
    }
}