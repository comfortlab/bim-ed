using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ElectricalSetting.Windows;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Electrical;

[Transaction(TransactionMode.Manual)]
public class OperatingModesCommand : DefinitionExternalCommand
{
    public OperatingModesCommand() : base(
        caption: Ribbon.OperatingModes_Caption,
        largeImage: Ribbon.OperatingModes_LargeImage,
        image: Ribbon.OperatingModes_Image,
        toolTipText: Ribbon.OperatingModes_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<Repository<OperatingModes>>().Load();
        var operatingModeElements = ServiceProvider.Get<OperatingModeElements>();
        var loadClassifications = ServiceProvider.Get<Repository<LoadClassificationProxy>>();
        operatingModeElements.Elements.Clear();
        operatingModeElements.Elements.AddRange(loadClassifications);
        ServiceProvider.Get<OperatingModesWindow>().ShowDialog();
        return Result.Succeeded;
    }
}