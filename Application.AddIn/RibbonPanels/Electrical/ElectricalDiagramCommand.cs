using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.DataAccess.Model.Services;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ElectricalDiagram;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Electrical;

[Transaction(TransactionMode.Manual)]
public class ElectricalDiagramCommand : DefinitionExternalCommand
{
    public ElectricalDiagramCommand() : base(
        caption: Ribbon.ElectricalDiagram_Caption,
        largeImage: Ribbon.ElectricalDiagram_LargeImage,
        image: Ribbon.ElectricalDiagram_Image,
        toolTipText: Ribbon.ElectricalDiagram_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<ElectricalSystemBuilder>().Build();
        ServiceProvider.Get<ElectricalDiagramWindow>().ShowDialog();
        return Result.Succeeded;
    }
}