﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ElectricalSetting.Windows;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Electrical;

[Transaction(TransactionMode.Manual)]
public class DistributionSystemsCommand : DefinitionExternalCommand
{
    public DistributionSystemsCommand() : base(
        caption: Ribbon.DistributionSystems_Caption,
        largeImage: Ribbon.DistributionSystems_LargeImage,
        image: Ribbon.DistributionSystems_Image,
        toolTipText: Ribbon.DistributionSystems_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<Repository<DistributionSystemProxy>>().Load();
        ServiceProvider.Get<DistributionSystemsWindow>().ShowDialog();
        return Result.Succeeded;
    }
}