using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.EquipmentSelector.Windows;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;

[Transaction(TransactionMode.Manual)]
public class CreateTransformerCommand : DefinitionExternalCommand
{
    public CreateTransformerCommand() : base(
        caption: Ribbon.CreateTransformer_Caption,
        largeImage: Ribbon.CreateTransformer_LargeImage,
        image: Ribbon.CreateTransformer_Image,
        toolTipText: Ribbon.CreateTransformer_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<CreateTransformerWindow>().ShowDialog();
        return Result.Succeeded;
    }
}