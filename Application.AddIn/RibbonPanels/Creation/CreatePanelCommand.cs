using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;

[Transaction(TransactionMode.Manual)]
public class CreatePanelCommand : DefinitionExternalCommand
{
    public CreatePanelCommand() : base(
        caption: Ribbon.CreatePanel_Caption,
        largeImage: Ribbon.CreatePanel_LargeImage,
        image: Ribbon.CreatePanel_Image,
        toolTipText: Ribbon.CreatePanel_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}