using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;

[Transaction(TransactionMode.Manual)]
public class CreateCubicleCommand : DefinitionExternalCommand
{
    public CreateCubicleCommand() : base(
        caption: Ribbon.CreateCubicle_Caption,
        largeImage: Ribbon.CreateCubicle_LargeImage,
        image: Ribbon.CreateCubicle_Image,
        toolTipText: Ribbon.CreateCubicle_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}