using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;

[Transaction(TransactionMode.Manual)]
public class CreateLoadCommand : DefinitionExternalCommand
{
    public CreateLoadCommand() : base(
        caption: Ribbon.ElectricalDiagram_Caption,
        largeImage: Ribbon.ElectricalDiagram_LargeImage,
        image: Ribbon.ElectricalDiagram_Image,
        toolTipText: Ribbon.ElectricalDiagram_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}