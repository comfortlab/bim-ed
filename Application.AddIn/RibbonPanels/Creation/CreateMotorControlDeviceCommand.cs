using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;

[Transaction(TransactionMode.Manual)]
public class CreateMotorControlDeviceCommand : DefinitionExternalCommand
{
    public CreateMotorControlDeviceCommand() : base(
        caption: Ribbon.CreateMotorControlDevice_Caption,
        largeImage: Ribbon.CreateMotorControlDevice_LargeImage,
        image: Ribbon.CreateMotorControlDevice_Image,
        toolTipText: Ribbon.CreateMotorControlDevice_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}