using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Tools;

[Transaction(TransactionMode.Manual)]
public class CopyRoomsAndSpacesCommand : DefinitionExternalCommand
{
    public CopyRoomsAndSpacesCommand() : base(
        caption: Ribbon.ReportsPanel_Caption,
        largeImage: Ribbon.ReportsPanel_LargeImage,
        image: Ribbon.ReportsPanel_Image,
        toolTipText: Ribbon.ReportsPanel_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}