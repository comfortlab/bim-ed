using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Tools;

[Transaction(TransactionMode.Manual)]
public class LoadFamiliesCommand : DefinitionExternalCommand
{
    public LoadFamiliesCommand() : base(
        caption: Ribbon.LoadFamilies_Caption,
        largeImage: Ribbon.LoadFamilies_LargeImage,
        image: Ribbon.LoadFamilies_Image,
        toolTipText: Ribbon.LoadFamilies_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}