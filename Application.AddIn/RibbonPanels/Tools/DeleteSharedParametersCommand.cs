using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Tools;

[Transaction(TransactionMode.Manual)]
public class DeleteSharedParametersCommand : DefinitionExternalCommand
{
    public DeleteSharedParametersCommand() : base(
        caption: Ribbon.DeleteSharedParameters_Caption,
        largeImage: Ribbon.DeleteSharedParameters_LargeImage,
        image: Ribbon.DeleteSharedParameters_Image,
        toolTipText: Ribbon.DeleteSharedParameters_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}