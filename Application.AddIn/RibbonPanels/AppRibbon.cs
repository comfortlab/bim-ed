﻿using System.Collections.Generic;
using System.Windows.Media;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Application.AddIn.RibbonPanels.About;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Admin;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Calculations;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Creation;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Electrical;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Reports;
using CoLa.BimEd.Application.AddIn.RibbonPanels.Tools;
using CoLa.BimEd.Application.AddIn.RibbonPanels.View;
using CoLa.BimEd.Infrastructure.Revit.Ribbon;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using RibbonItem = Autodesk.Revit.UI.RibbonItem;
using RibbonPanel = Autodesk.Revit.UI.RibbonPanel;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels;

public class AppRibbon
{
    private readonly RibbonFactory _ribbonFactory;

    public AppRibbon(RibbonFactory ribbonFactory)
    {
        _ribbonFactory = ribbonFactory;
    }
    
    internal bool IsBlocked { get; private set; }
    internal RibbonItem LogInButton { get; set; }
    internal RibbonItem LogOutButton { get; set; }
    internal RibbonPanel AboutRibbonPanel { get; set; }
    internal RibbonItem UpdateButton { get; set; }

    internal void AddRibbonTab(string ribbonName)
    {
        _ribbonFactory.CreateRibbonTab(ribbonName, RibbonVisible.Project);
        
        _ribbonFactory.CreateRibbonPanel("BIM Electrical Design", RibbonVisible.Project);
        _ribbonFactory.SetBackgroundImage(Ribbon.ElectricalDesign_259x94);
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.ElectricalSystems, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new ElectricalDiagramCommand());
        _ribbonFactory.AddPushButton(new DistributionSystemsCommand());
        _ribbonFactory.AddPushButton(new LoadClassificationsCommand());
        _ribbonFactory.AddPushButton(new OperatingModesCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.Creation, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new CreateTransformerCommand());
        _ribbonFactory.AddPushButton(new CreateCubicleCommand());
        _ribbonFactory.AddPushButton(new CreatePanelCommand());
        _ribbonFactory.AddPushButton(new CreateMotorControlDeviceCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.Calculations, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new CalculateCableLengthCommand());
        _ribbonFactory.AddPushButton(new CalculateLoadsCommand());
        _ribbonFactory.AddPushButton(new SelectAllEquipmentCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.Reports, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new ReportsCommand());
        _ribbonFactory.AddPushButton(new PanelReportsCommand());
        _ribbonFactory.AddSlideOut();
        _ribbonFactory.AddPushButton(new RefreshAllScheduleReportsCommand());
        _ribbonFactory.AddPushButton(new RefreshAllPanelReportsCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.Tools, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new CopyRoomsAndSpacesCommand());
        _ribbonFactory.AddPushButton(new LoadFamiliesCommand());
        _ribbonFactory.AddSlideOut();
        _ribbonFactory.AddPushButton(new DeleteSharedParametersCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.View, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new HighlightRouteCommand());
        _ribbonFactory.AddPushButton(new CableTrayFillingCommand());
        _ribbonFactory.AddPulldownButton(
            new List<DefinitionExternalCommand>
            {
                new DeleteFiltersFromViewCommand(),
                new DeleteFiltersFromDocumentCommand(),
            },
            new DeleteFiltersCommand(), RibbonVisible.Project);
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.About, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new LogInCommand());
        
        _ribbonFactory.CreateRibbonPanel(Ribbon.Admin, RibbonVisible.Project);
        _ribbonFactory.AddPushButton(new ModelReviewCommand());
        _ribbonFactory.AddPushButton(new CommandPromptCommand());

        _ribbonFactory.SetRibbonStyle(background: Colors.White,  titleBarBackground:Color.FromRgb(61, 205, 88));
    }
}