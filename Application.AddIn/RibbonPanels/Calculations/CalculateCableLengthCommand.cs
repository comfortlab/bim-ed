﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Calculations;

[Transaction(TransactionMode.Manual)]
public class CalculateCableLengthCommand : DefinitionExternalCommand
{
    public CalculateCableLengthCommand() : base(
        caption: Ribbon.CalculateCableLength_Caption,
        largeImage: Ribbon.CalculateCableLength_LargeImage,
        image: Ribbon.CalculateCableLength_Image,
        toolTipText: Ribbon.CalculateCableLength_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        ServiceProvider.Get<NetworkCalculator>().Calculate();
        return Result.Succeeded;
    }
}