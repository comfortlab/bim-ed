using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Calculations;

[Transaction(TransactionMode.Manual)]
public class CalculateLoadsCommand : DefinitionExternalCommand
{
    public CalculateLoadsCommand() : base(
        caption: Ribbon.CalculateLoads_Caption,
        largeImage: Ribbon.CalculateLoads_LargeImage,
        image: Ribbon.CalculateLoads_Image,
        toolTipText: Ribbon.CalculateLoads_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}