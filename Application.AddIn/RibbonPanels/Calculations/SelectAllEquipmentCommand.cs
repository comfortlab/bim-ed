using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Calculations;

[Transaction(TransactionMode.Manual)]
public class SelectAllEquipmentCommand : DefinitionExternalCommand
{
    public SelectAllEquipmentCommand() : base(
        caption: Ribbon.SelectAllEquipment_Caption,
        largeImage: Ribbon.SelectAllEquipment_LargeImage,
        image: Ribbon.SelectAllEquipment_Image,
        toolTipText: Ribbon.SelectAllEquipment_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}