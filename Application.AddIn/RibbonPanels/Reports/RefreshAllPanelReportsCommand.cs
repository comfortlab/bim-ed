using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Reports;

[Transaction(TransactionMode.Manual)]
public class RefreshAllPanelReportsCommand : DefinitionExternalCommand
{
    public RefreshAllPanelReportsCommand() : base(
        caption: Ribbon.ReportsRefreshAllPanels_Caption,
        largeImage: Ribbon.ReportsRefreshAllPanels_LargeImage,
        image: Ribbon.ReportsRefreshAllPanels_Image,
        toolTipText: Ribbon.ReportsRefreshAllPanels_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}