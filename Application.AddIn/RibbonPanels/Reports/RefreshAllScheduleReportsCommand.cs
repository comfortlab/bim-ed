﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.Reports;

[Transaction(TransactionMode.Manual)]
public class RefreshAllScheduleReportsCommand : DefinitionExternalCommand
{
    public RefreshAllScheduleReportsCommand() : base(
        caption: Ribbon.ReportsRefreshAllSchedules_Caption,
        largeImage: Ribbon.ReportsRefreshAllSchedules_LargeImage,
        image: Ribbon.ReportsRefreshAllSchedules_Image,
        toolTipText: Ribbon.ReportsRefreshAllSchedules_ToolTipText)
    { }
    
    protected override Result Execute()
    {
        return Result.Succeeded;
    }
}