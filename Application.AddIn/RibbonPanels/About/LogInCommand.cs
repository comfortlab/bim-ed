﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.Resources;
using CoLa.BimEd.Infrastructure.Revit.Ribbon.Definitions;
using CoLa.BimEd.UI.ModelReview;

namespace CoLa.BimEd.Application.AddIn.RibbonPanels.About;

[Transaction(TransactionMode.Manual)]
public class LogInCommand : DefinitionExternalCommand
{
    public LogInCommand() : base(
        caption: Ribbon.LogIn_Caption,
        largeImage: Ribbon.LogIn_LargeImage)
    { }
    
    protected override Result Execute()
    {
        new ModelReviewWindow().ShowDialog();
        return Result.Succeeded;
    }
}