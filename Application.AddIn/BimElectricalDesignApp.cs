﻿using System;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.DependencyInjection;
using CoLa.BimEd.Application.AddIn.RibbonPanels;
using CoLa.BimEd.Application.AddIn.Utils;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.Revit;
using CoLa.BimEd.Infrastructure.Revit.Async;
using CoLa.BimEd.UI.Model;

namespace CoLa.BimEd.Application.AddIn;

[Transaction(TransactionMode.Manual)]
public class BimElectricalDesignApp : IExternalApplication
{
    public Result OnStartup(UIControlledApplication application)
    {
        var serviceProvider = DI.ServiceProvider = Bootstrapper.Build("BIM Electrical Design", application);
        var logger = serviceProvider.Get<Logger>();
        
        try
        {
            var revitContext = serviceProvider.Get<RevitContext>();
            revitContext.AssemblyPath = AssemblyUtils.GetExecutingAssemblyPath();
            revitContext.ContextUpdating(activate: true);
        
            AssemblyLoadUtils.LoadProjectAssemblies(new []
            {
                typeof(AssemblyPointer).Assembly,
                typeof(UI.Resources.AssemblyPointer).Assembly,
            }, logger);

            serviceProvider.Get<AppRibbon>().AddRibbonTab("NEW BIM ED");

            RevitTask.Initialize(application);

            return Result.Succeeded;
        }
        catch (Exception exception)
        {
            logger.Error(exception);
            return Result.Failed;
        }
    }

    public Result OnShutdown(UIControlledApplication application)
    {
        return Result.Succeeded;
    }
}