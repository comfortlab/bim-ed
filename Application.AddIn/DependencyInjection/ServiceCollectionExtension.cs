using System;
using System.Linq;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;

namespace CoLa.BimEd.Application.AddIn.DependencyInjection;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddToScope<T>(
        this IServiceCollection services,
        Func<IServiceProvider, object> scope)
        where T : class
    {
        return services.AddToScope(
            serviceProvider =>
            {
                var type = typeof(T);
                var constructors = type.GetConstructors().OrderBy(x => x.GetParameters().Length);;
                
                foreach (var constructor in constructors)
                {
                    var parameters = constructor.GetParameters()
                        .Select(x => serviceProvider.GetService(x.ParameterType))
                        .ToArray();

                    if (parameters.Any(x => x == null))
                    {
                        continue;
                    }
                    
                    if (constructor.Invoke(parameters) is T obj)
                    {
                        return obj;
                    }
                }

                return
                    type.GetConstructor(new[] { typeof(IServiceProvider) })?.Invoke(new object[] {serviceProvider}) as T ??
                    type.GetConstructor(Type.EmptyTypes)?.Invoke(Array.Empty<object>()) as T;
            },
            scope);
    }
        
    public static IServiceCollection AddToScope<T>(
        this IServiceCollection services,
        Func<IServiceProvider, T> implementation,
        Func<IServiceProvider, object> scope)
        where T : class
    {
        return services.AddTransient(serviceProvider =>
        {
            var scopeObject = scope?.Invoke(serviceProvider);

            if (scopeObject == null)
                throw new NullReferenceException($"{nameof(scopeObject)} was not be set.");
                
            var processingScope = serviceProvider.Get<ProcessingScope>();

            if (processingScope.TryGetScoped<T>(scopeObject, out var existedInScope))
                return existedInScope;
            
            var newImplementation = implementation?.Invoke(serviceProvider);
            processingScope.Add(scopeObject, newImplementation);
            return newImplementation;
        });
    }
}