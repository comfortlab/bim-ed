﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using Autodesk.Revit.UI;
using CoLa.BimEd.Application.AddIn.RibbonPanels;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.CableRouting.Logic;
using CoLa.BimEd.BusinessLogic.CableRouting.Mapping;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.BusinessLogic.Model.Services;
using CoLa.BimEd.BusinessLogic.Model.Settings;
using CoLa.BimEd.BusinessLogic.Model.Utils;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.DataAccess.Model.Mapping;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;
using CoLa.BimEd.DataAccess.Model.Services;
using CoLa.BimEd.DataAccess.Revit.Mapping;
using CoLa.BimEd.DataAccess.Revit.Repositories;
using CoLa.BimEd.DataAccess.Revit.Repositories.Storage;
using CoLa.BimEd.DataAccess.Revit.RevitParameters.Resources;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Converters;
using CoLa.BimEd.Infrastructure.Framework.DataAccess;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Net.Proxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.ProgressBar;
using CoLa.BimEd.Infrastructure.Revit;
using CoLa.BimEd.Infrastructure.Revit.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Families;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;
using CoLa.BimEd.Infrastructure.Revit.RevitVersions;
using CoLa.BimEd.Infrastructure.Revit.Ribbon;
using CoLa.BimEd.Infrastructure.Revit.Storage;
using CoLa.BimEd.Infrastructure.Revit.Utils;
using CoLa.BimEd.UI.ElectricalDiagram;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.OperatingModeSetting;
using CoLa.BimEd.UI.ElectricalSetting.Windows;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Windows;
using CoLa.BimEd.UI.RtmDemandFactors.DataAccess;
using CoLa.BimEd.UI.RtmDemandFactors.Model;
using CoLa.BimEd.UI.RtmDemandFactors.ViewModels;
using CoLa.UI.Presentation.CommandPrompt;
using ElectricalSystemProxy = CoLa.BimEd.BusinessLogic.Model.Electrical.ElectricalSystemProxy;
using Material = CoLa.BimEd.BusinessLogic.Model.Common.Material;

namespace CoLa.BimEd.Application.AddIn.DependencyInjection;

public static class Bootstrapper
{
    private static readonly Func<IServiceProvider, object> CurrentScope =
        serviceProvider => serviceProvider.Get<ProcessingScope>().Current;
    
    public static IServiceProvider Build(string applicationName, UIControlledApplication uiControlledApplication)
    {
        var assemblyPath = AssemblyUtils.GetExecutingAssemblyPath();
        var serviceCollection = new ServiceCollection();
        
        // TODO Before release change 'applicationAssemblyPath' and 'browserApplicationPath'
        var applicationAssemblyPath = Path.Combine(AssemblyUtils.GetExecutingAssemblyDirectory(), @"CoLa.BimEd.Application.AddIn.dll");
        var browserApplicationPath = Path.Combine(AssemblyUtils.GetExecutingAssemblyDirectory(), @"CoLa.BimEd.UI.BrowserApplication.exe");
        // var applicationAssemblyPath = Path.Combine(AssemblyUtils.GetSolutionDirectoryInfo().FullName, @"Application\bin\x64\Debug\CoLa.BimEd.Application.dll");
        // var browserApplicationPath = Path.Combine(AssemblyUtils.GetSolutionDirectoryInfo().FullName, @"UI.BrowserApplication\bin\x64\Debug\CoLa.BimEd.UI.BrowserApplication.exe");

        serviceCollection.AddSingleton(x => x);
        serviceCollection.AddSingleton<ProcessingScope>();
        
        // Application
        
        serviceCollection.AddSingleton(_ => new AppInfo(applicationName));
        serviceCollection.AddSingleton(_ => new AppPaths(
            applicationName,
            browserApplicationPath,
            uiControlledApplication.ControlledApplication.VersionNumber.FirstInt()));
        serviceCollection.AddSingleton(_ => new AppResources(SharedParameterFiles.DefinitionFile));
        serviceCollection.AddSingleton(x => new AssemblyCommands(applicationAssemblyPath, x));
        serviceCollection.AddSingleton<Localization>();
        serviceCollection.AddSingleton<Logger>();
        serviceCollection.AddToScope<OperatingElements>(CurrentScope);

        // SE API

        serviceCollection.AddSingleton<ProxyStorage>();
        serviceCollection.AddSingleton<SeApiHttpClient>();
        serviceCollection.AddSingleton<SelectorConfigBuilder>();
        serviceCollection.AddSingleton<SelectorConverters>();
        serviceCollection.AddSingleton<SeApi.Converters>();
        serviceCollection.AddSingleton<SeApi.Products>();
        serviceCollection.AddSingleton<SeApi.Selectors>();
        serviceCollection.AddSingleton<SeApi.Translation>();

        serviceCollection.AddTransient(_ =>
        {
            return new SelectAndConfigSetting<SwitchBoardUnit>
            {
                BreakNeutralCore = false,
                SwitchSettings = new SelectorProductSettings(new[]
                {
                    SeApi.Selectors.Acti9_iSW,
                    SeApi.Selectors.ComPact_NSXm_NA,
                    SeApi.Selectors.MasterPact_MTZ1_SD,
                    SeApi.Selectors.MasterPact_MTZ2_SD,
                    SeApi.Selectors.MasterPact_MTZ3_SD,
                }),
            };
        });

        serviceCollection.AddTransient(_ =>
        {
            return new SelectAndConfigSetting<ElectricalSystemProxy>
            {
                NeutralCore = true,
                BreakNeutralCore = true,
                EquipmentSelectionMode = EquipmentSelectionMode.ByLoad,
                MinCoreSection = 1.5,
                SwitchSettings = new SelectorProductSettings(new[]
                {
                    SeApi.Selectors.Acti9_iC60,
                    SeApi.Selectors.Acti9_C120,
                    SeApi.Selectors.ComPact_NSXm,
                    SeApi.Selectors.ComPact_NSX,
                    SeApi.Selectors.MasterPact_MTZ1_CB,
                    SeApi.Selectors.MasterPact_MTZ2_CB,
                    SeApi.Selectors.MasterPact_MTZ3_CB,
                }),
            };
        });
        
        // Revit
        
        serviceCollection.AddSingleton(_ => uiControlledApplication);
        serviceCollection.AddSingleton<AppRibbon>();
        serviceCollection.AddSingleton<RevitContext>();
        serviceCollection.AddSingleton<RevitVersionResolver>();
        serviceCollection.AddSingleton<RibbonFactory>();
        serviceCollection.AddSingleton<RibbonVisibleUtils>();
        serviceCollection.AddSingleton<UpdateDocumentHandler>();
        
        serviceCollection.AddToScope(x => x.Get<RevitContext>().Document, CurrentScope);
        serviceCollection.AddToScope(x => x.Get<RevitContext>().UIApplication, CurrentScope);
        
        serviceCollection.AddToScope<ElectricalSystemUtils>(CurrentScope);
        serviceCollection.AddToScope<FamilyDocuments>(CurrentScope);
        serviceCollection.AddTransient<SharedParameterUtils>();
        
        // Revit Elements

        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<DistributionSysType>());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<ElectricalDemandFactorDefinition>());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<ElectricalLoadClassification>());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<ElectricalSetting>().First());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<ElectricalSystem>());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<FamilyInstance>());
        serviceCollection.AddTransient(x => x.Get<Document>().GetElementsOfClass<VoltageType>());
        serviceCollection.AddTransient<Func<BuiltInCategory, List<FamilyInstance>>>(x => category => x.Get<Document>().GetElementsOfCategory<FamilyInstance>(category));
        serviceCollection.AddTransient<Func<BuiltInCategory, ElementFilter, List<FamilyInstance>>>(x => (category, filter) => x.Get<Document>().GetElementsOfCategory<FamilyInstance>(category, filter));
        serviceCollection.AddTransient<Func<ElementFilter, List<FamilyInstance>>>(x => filter => x.Get<Document>().GetElementsOfClass<FamilyInstance>(filter));
        
        // Repositories
        
        serviceCollection.AddSingleton<Repository<RtmData>, RtmDemandFactorRepository>();
        serviceCollection.AddSingleton<Repository<CableSeries>, CableSeriesRepository>();
        serviceCollection.AddSingleton<Repository<LoadClassificationDirectory>, LoadClassificationDirectoryRepository>();
        serviceCollection.AddSingleton<Repository<Material>, MaterialRepository>();
        serviceCollection.AddSingleton<Repository<SwitchGearFunction>, SwitchGearFunctionRepository>();
        serviceCollection.AddSingleton<Repository<SwitchGearSeries>, SwitchGearSeriesRepository>();
        serviceCollection.AddSingleton<Repository<TransformerSeries>, TransformerSeriesRepository>();
        serviceCollection.AddSingleton(_ => SwitchGearComponentsInitializer.Initialize());
        
        // Revit Proxy Repositories

        serviceCollection.AddToScope<Repository<DistributionSystemProxy>>(x => new DistributionSystemRepository(x, x.Get<ElectricalSetting>()), CurrentScope);
        serviceCollection.AddToScope<Repository<VoltageTypeProxy>>(_ => new VoltageTypeRepository(), CurrentScope);
        
        serviceCollection.AddToScope<Repository<LoadClassificationProxy>>(x => new LoadClassificationRepository(x), CurrentScope);
        serviceCollection.AddToScope<Repository<DemandFactorProxy>>(_ => new DemandFactorRepository(), CurrentScope);
        
        serviceCollection.AddToScope<Repository<ElectricalSystemProxy>>(x => new ElectricalSystemRepository(x), CurrentScope);
        serviceCollection.AddToScope<Repository<SwitchBoardUnit>>(x => new SwitchBoardUnitRepository(x), CurrentScope);
        serviceCollection.AddToScope<Repository<SwitchGearUnit>>(x => new SwitchGearUnitRepository(x), CurrentScope);
        serviceCollection.AddToScope<Repository<Transformer>>(x => new TransformerRepository(x), CurrentScope);
        
        serviceCollection.AddTransient(x => x.Get<Repository<Transformer>>().Concat<ElectricalEquipmentProxy>(x.Get<Repository<SwitchBoardUnit>>()).ToArray());
        serviceCollection.AddTransient(x => x.Get<Repository<Transformer>>().Concat(x.Get<VirtualElements>().Transformers).ToArray());

        // Revit Storages

        serviceCollection.AddToScope<Repository<ElectricalSettings>>(x => new ElectricalSettingRepository(x), CurrentScope);
        serviceCollection.AddTransient(x => x.Get<Repository<ElectricalSettings>>().First());
        serviceCollection.AddTransient<DocumentStorage<ElectricalSettings>, ElectricalSettingStorage>();
        
        serviceCollection.AddToScope<Repository<VirtualElements>>(x => new VirtualElementsRepository(x), CurrentScope);
        serviceCollection.AddToScope<VirtualElementsUtils>(CurrentScope);
        serviceCollection.AddTransient(x => x.Get<Repository<VirtualElements>>().First());
        serviceCollection.AddTransient(x => x.Get<VirtualElements>().Generators);
        serviceCollection.AddTransient(x => x.Get<VirtualElements>().Networks);
        serviceCollection.AddTransient(x => x.Get<IMapper>().Map<VirtualElementsDto>(x.Get<VirtualElements>()));
        serviceCollection.AddTransient<DocumentStorage<VirtualElementsDto>, VirtualElementsStorage>();
        
        serviceCollection.AddToScope<Repository<OperatingModes>>(x => new OperatingModeRepository(x), CurrentScope);
        serviceCollection.AddToScope<OperatingModeElements>(CurrentScope);
        serviceCollection.AddTransient(x => x.Get<Repository<OperatingModes>>().First());
        serviceCollection.AddTransient(x => x.Get<IMapper>().Map<OperatingModesDto>(x.Get<OperatingModes>()));
        serviceCollection.AddTransient<DocumentStorage<OperatingModesDto>, OperatingModeStorage>();
        
        // Application Elements

        serviceCollection.AddTransient<ElectricalSystemBuilder>();
        serviceCollection.AddTransient<Transformer>();
        
        // Application Elements DTO


        #region

        // Cable Routing
        
        serviceCollection.AddTransient<NetworkCalculator>();
        serviceCollection.AddTransient<NetworkDrawer>();
        serviceCollection.AddTransient<RevitBuildingElements>();
        serviceCollection.AddToScope<CableRoutingSetting>(CurrentScope);
        serviceCollection.AddToScope<NetworkElements>(CurrentScope);
        serviceCollection.AddToScope<NetworkConverter>(CurrentScope);
        
        #endregion
        
        // Mapping

        serviceCollection.AddSingleton(_ => new MapperConfiguration(cfg =>
        {
            // CableRouting
            cfg.AddProfile(new CableRoutingProfile());

            // Model
            cfg.AddProfile<CommonProfile>();
            cfg.AddProfile<CableProfile>();
            cfg.AddProfile<DistributionSystemProfile>();
            cfg.AddProfile<ElectricalEntitiesProfile>();
            cfg.AddProfile<ElectricalProfile>();
            cfg.AddProfile<LoadClassificationProfile>();
            cfg.AddProfile<ModelProfile>();
            cfg.AddProfile<ProductProfile>();
            cfg.AddProfile<VirtualElementsProfile>();
        }));
        serviceCollection.AddSingleton<IMapper>(x => new Mapper(x.Get<MapperConfiguration>(), x.GetService));
        serviceCollection.AddSingleton<DtoConverter<Cable, CableDto>>();
        serviceCollection.AddSingleton<DtoConverter<CableSeries, CableSeriesDto>>();
        serviceCollection.AddSingleton<DtoConverter<CircuitProducts, CircuitProductsDto>>();
        serviceCollection.AddSingleton<DtoConverter<ConductorCount, ConductorCountDto>>();
        serviceCollection.AddSingleton<DtoConverter<ElectricalBase, ElectricalBaseDto>>();
        serviceCollection.AddSingleton<DtoConverter<ElectricalSystemProxy, CircuitDto>>();
        serviceCollection.AddSingleton<DtoConverter<VirtualElements, VirtualElementsDto>>();
        serviceCollection.AddSingleton<DtoConverter<LoadClassificationProxy, LoadClassificationDto>>();
        serviceCollection.AddSingleton<DtoConverter<MotorControlDevice, MotorControlDeviceDto>>();
        serviceCollection.AddSingleton<DtoConverter<OperatingModes, OperatingModesDto>>();
        serviceCollection.AddSingleton<DtoConverter<SwitchBoardUnitProducts, SwitchboardUnitProductsDto>>();
        serviceCollection.AddSingleton<DtoConverter<Transformer, TransformerDto>>();
        
        // UI
        
        serviceCollection.AddSingleton<Interactions>();
        serviceCollection.AddSingleton<DebugCommandPrompt>();
        serviceCollection.AddSingleton<DiagramClipboard>();
        serviceCollection.AddSingleton<NodeSelection>();

        serviceCollection.AddTransient<CommandPromptWindow>();
        serviceCollection.AddTransient<SourceViewModel>();
        serviceCollection.AddTransient<CreateTransformerWindow>();
        serviceCollection.AddTransient<ChainDiagramViewModel>();
        serviceCollection.AddTransient<NodesGridViewModel>();
        serviceCollection.AddTransient<DistributionSystemsViewModel>();
        serviceCollection.AddTransient<DistributionSystemsWindow>();
        serviceCollection.AddTransient<DiagramViewModel>();
        serviceCollection.AddTransient<ElectricalDiagramWindow>();
        serviceCollection.AddTransient<ImportLoadClassificationsViewModel>();
        serviceCollection.AddTransient<LoadClassificationsViewModel>();
        serviceCollection.AddTransient<LoadClassificationsWindow>();
        serviceCollection.AddTransient<OperatingModesViewModel>();
        serviceCollection.AddTransient<OperatingModesWindow>();
        serviceCollection.AddTransient<ProgressBarController>();
        serviceCollection.AddTransient<ProgressBarWindow>();
        serviceCollection.AddTransient<RtmDemandFactorsViewModel>();
        serviceCollection.AddTransient<SwitchGearViewModel>();
        serviceCollection.AddTransient<SwitchGearSeriesSelectorViewModel>();
        
        return serviceCollection.BuildServiceProvider();
    }
}