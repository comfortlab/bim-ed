using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Prototype;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Base;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels;

public class SourceViewModel : EquipmentViewModel<ElectricalSource>
{
    private readonly string _generator = ElectricalParameters.PowerSource_Generator;
    private readonly string _network = ElectricalParameters.PowerSource_Network;

    public SourceViewModel(IServiceProvider serviceProvider) :
        base(serviceProvider, null)
    {
        SourceTypesVisible = true;
    }

    public SourceViewModel(IServiceProvider serviceProvider, ElectricalSource equipmentSource) :
        base(serviceProvider, equipmentSource)
    {
        switch (EditedEquipment)
        {
            case ElectricalGenerator:
                CanBePlacedInModel = EditedEquipment.RevitId <= 0;
                ExistsInModel = EditedEquipment.RevitId > 0;
                Title = ElectricalParameters.GeneratorName;
                break;
            
            case ElectricalNetwork:
                Title = ElectricalParameters.NetworkName;
                break;
        }
    }
    
    public List<string> SourceTypes => new() { _network, _generator };
    public string Title { get; private set; }
    public string SelectedSourceType { get; set; }
    public bool SourceTypesVisible { get; set; }
    public bool SourceDefinitionVisible { get; set; }
    public bool IsSelectSourceOkEnabled { get; set; }
    
    // ReSharper disable UnusedMember.Local
    private void OnSelectedSourceTypeChanged() => IsSelectSourceOkEnabled = !string.IsNullOrWhiteSpace(SelectedSourceType);

    protected override ElectricalSource CreateDefault() => null;
    
    protected override void Create()
    {
        switch (EditedEquipment)
        {
            case ElectricalGenerator electricalGenerator:
                ServiceProvider.Get<List<ElectricalGenerator>>().Add(electricalGenerator);
                break;

            case ElectricalNetwork electricalNetwork:
                ServiceProvider.Get<List<ElectricalNetwork>>().Add(electricalNetwork);
                break;
        }

        var sources = ServiceProvider.Get<Repository<VirtualElements>>();
        sources.ChangedElements.Create(EditedEquipment.RevitId);
        sources.Save();
    }

    protected override void Delete()
    {
        switch (EditedEquipment)
        {
            case ElectricalGenerator electricalGenerator:
                ServiceProvider.Get<List<ElectricalGenerator>>().Remove(electricalGenerator);
                break;

            case ElectricalNetwork electricalNetwork:
                ServiceProvider.Get<List<ElectricalNetwork>>().Remove(electricalNetwork);
                break;
        }

        var sources = ServiceProvider.Get<Repository<VirtualElements>>();
        sources.ChangedElements.Delete(EditedEquipment.RevitId);
        sources.Save();
    }

    protected override void Modify()
    {
        OriginalEquipment.PullProperties<ElectricalSource>(EditedEquipment);
        var repository = ServiceProvider.Get<Repository<VirtualElements>>();
        repository.ChangedElements.Modify(EditedEquipment.RevitId);
        repository.Save();
    }

    public Command SelectSourceOkCommand => new(() =>
    {
        SourceTypesVisible = false;
        SourceDefinitionVisible = true;
        if (SelectedSourceType == _generator)
        {
            Title = ElectricalParameters.GeneratorName;
            EditedEquipment = new ElectricalGenerator(VirtualElements.NewId(), null, ElectricalSystemTypeProxy.PowerCircuit);
        }
        else if (SelectedSourceType == _network)
        {
            Title = ElectricalParameters.NetworkName;
            EditedEquipment = new ElectricalNetwork(VirtualElements.NewId(), null, ElectricalSystemTypeProxy.PowerCircuit);
        }
    });
}