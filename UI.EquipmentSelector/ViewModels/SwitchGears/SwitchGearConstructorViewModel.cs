﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearConstructorViewModel : ViewModel
{
    private static readonly string CombinationsMode = CommonDictionary.Combinations;
    private static readonly string FunctionsMode = CommonDictionary.Functions;

    public SwitchGearConstructorViewModel(
        IServiceProvider serviceProvider,
        SwitchGearSeries switchGearSeries)
    {
        SwitchGearSeries = switchGearSeries ?? throw new NullReferenceException(nameof(switchGearSeries));
        AddSwitchGearUnit(serviceProvider, switchGearSeries);
        SwitchGearUnits.CollectionChanged += (_, _) => AnySwitchGearUnits = SwitchGearUnits.Any();
    }

    private void AddSwitchGearUnit(IServiceProvider serviceProvider, SwitchGearSeries switchGearSeries)
    {
        MainDialogViewModel = new SwitchGearSelectorViewModel(serviceProvider, switchGearSeries);
    }

    private static string GetConnectionMode(SwitchGearUnitViewModel switchGearUnit) => GetConnectionMode(switchGearUnit.Item);
    private static string GetConnectionMode(SwitchGearUnit switchGearUnit) => switchGearUnit.LeftConnector switch
    {
        not null when switchGearUnit.RightConnector is not null => "DE",
        not null when switchGearUnit.RightConnector is null     => "LE",
        null     when switchGearUnit.RightConnector is not null => "RE",
        null     when switchGearUnit.RightConnector is null     => "NE",
        _ => throw new ArgumentOutOfRangeException()
    };

    public ViewModel  MainDialogViewModel { get; set; }
    public SwitchGearSeries SwitchGearSeries { get; }
    public ObservableCollection<SwitchGearUnitViewModel> SwitchGearUnits { get; } = new();
    public SwitchGearUnitViewModel SelectedSwitchGearUnit { get; set; }
    public bool AnySwitchGearUnits { get; set; }
    public bool IsBlockSwitchGear => SwitchGearSeries.IsBlockSwitchGear;
    public string[] ElementTypes { get; set; } = { CombinationsMode, FunctionsMode };
    public string SelectedElementType { get; set; }
    public bool IsCombinationsMode { get; set; }
    public bool CanAddLeft { get; set; }
    public bool DropToAddFirst { get; set; }
    public bool IsFunctionsMode { get; set; }
    public string[] ConnectionModes { get; set; }
    public string SelectedConnectionMode { get; set; }
    public int[] FunctionCounts { get; set; }
    public int? SelectedFunctionCount { get; set; }
    public Function[] Functions { get; set; }
    public Function? SelectedFunction { get; set; }

    public Command ResetAllCommand => new(() =>
    {
        SelectedConnectionMode = null;
        SelectedFunction = null;
        SelectedFunctionCount = null;
    });

    public void DropMode(SwitchGearUnitViewModel dragged, bool state)
    {
        DropToAddFirst = state && SwitchGearUnits.IsEmpty();
        CanAddLeft = state && dragged.Item.RightConnector != null && SwitchGearUnits.FirstOrDefault()?.Item.LeftConnector != null;
        
        foreach (var switchGearUnitViewModel in SwitchGearUnits)
            switchGearUnitViewModel.UpdateDrop(dragged, state);
    }
    
    private void AddLeftUnit(SwitchGearUnitViewModel switchGearUnitViewModel)
    {
        var switchGearUnit = switchGearUnitViewModel.Item.Clone();
        SwitchGearUnits.Insert(0, new SwitchGearUnitViewModel(switchGearUnit, this));
    }
}