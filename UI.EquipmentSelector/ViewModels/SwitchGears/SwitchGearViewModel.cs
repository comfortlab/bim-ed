﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly SwitchGearConstructorViewModel _switchGearConstructorViewModel;
    private readonly VirtualElements _virtualElements;

    public SwitchGearViewModel(
        IServiceProvider serviceProvider,
        SwitchGearConstructorViewModel switchGearConstructorViewModel,
        SwitchGear switchGear = null)
    {
        _serviceProvider = serviceProvider;
        _switchGearConstructorViewModel = switchGearConstructorViewModel;
        _virtualElements = _serviceProvider.Get<VirtualElements>();
    }

    protected SwitchGearUnit CreateDefault()
    {
        return SwitchGear.CreateSingle(_virtualElements.NewId(), "1", ElectricalSystemTypeProxy.PowerCircuit).FirstUnit;
    }

    protected void UpdateDistributionSystems()
    {
        
    }
}