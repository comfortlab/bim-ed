﻿using System.Linq;
using System.Windows;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.UI.Framework.Behaviors;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearFunctionViewModel : ItemViewModel<SwitchGearFunction>, IDropped
{
    public SwitchGearFunctionViewModel(SwitchGearFunction switchGearFunction) :
        base(switchGearFunction)
    {
        SwitchGearFunction = switchGearFunction;
    }

    public SwitchGearFunction SwitchGearFunction { get; }
    public bool ExternalConnection { get; set; }
    public bool LeftConnection { get; set; }
    public bool RightConnection { get; set; }
    public bool NonInternalConnection { get; set; }
    public bool SectionConnection { get; set; }
    public bool FaultPassageIndicatorInstalled { get; set; }
    public bool ProtectionRelayInstalled { get; set; }
    public bool ShuntTripReleaseInstalled { get; set; }
    public bool SwitchControllerInstalled { get; set; }
    public bool UndervoltageReleaseInstalled { get; set; }
    public string ShuntTripReleaseDescription { get; set; }
    public string ShuntTripReleaseType { get; set; }

    private void OnSwitchGearFunctionChanged()
    {
        LeftConnection = SwitchGearFunction?.LeftConnector != null;
        RightConnection = SwitchGearFunction?.RightConnector != null;
        SectionConnection = SwitchGearFunction?.Functions.Contains(Function.SectionSwitch) ?? false;
        ExternalConnection = !SectionConnection;
    }

    public void OnDropped(IDataObject dataObject)
    {
        MessageBox.Show($"{dataObject.GetData(typeof(object))}");
    }
}