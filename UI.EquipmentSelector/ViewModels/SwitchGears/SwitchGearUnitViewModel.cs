﻿using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Prototype;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.Behaviors;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearUnitViewModel :
    ItemViewModel<SwitchGearUnit>,
    IPrototype<SwitchGearUnitViewModel>,
    IDragged
{
    public SwitchGearUnitViewModel(
        SwitchGearUnit switchGearUnit,
        SwitchGearConstructorViewModel switchGearConstructorViewModel,
        bool isCatalog = false) :
        base(switchGearUnit)
    {
        SwitchGearConstructorViewModel = switchGearConstructorViewModel;
        SwitchGearUnit = switchGearUnit;
        SwitchGearFunctions = switchGearUnit.SwitchGearFunctions
            .Select(x => new SwitchGearFunctionViewModel(x))
            .ToObservableCollection();
        IsCatalog = isCatalog;
        AddRightUnitDropped = new Dropped<SwitchGearUnitViewModel>(AddRightUnit);
        ReplaceUnitDropped = new Dropped<SwitchGearUnitViewModel>(ReplaceUnit);
    }
    public SwitchGearUnitViewModel(
        SwitchGearUnit switchGearUnit,
        bool isCatalog = false) :
        base(switchGearUnit)
    {
        SwitchGearUnit = switchGearUnit;
        SwitchGearFunctions = switchGearUnit.SwitchGearFunctions
            .Select(x => new SwitchGearFunctionViewModel(x))
            .ToObservableCollection();
        IsCatalog = isCatalog;
    }

    public SwitchGearConstructorViewModel SwitchGearConstructorViewModel { get; }
    public SwitchGearUnit SwitchGearUnit { get; }
    public ObservableCollection<SwitchGearFunctionViewModel> SwitchGearFunctions { get; private set; }
    public bool IsCatalog { get; set; }
    public bool CanAddRight { get; set; }
    public bool CanReplace { get; set; }
    public IDropped AddRightUnitDropped { get; }
    public IDropped ReplaceUnitDropped { get; }

    public Command SelectCommand => new(() =>
    {
        
    });

    internal void UpdateDrop(SwitchGearUnitViewModel switchGearUnitViewModel, bool state)
    {
        CanAddRight = state && Item.RightConnector != null && switchGearUnitViewModel.Item.LeftConnector != null;
    }
    
    private void AddRightUnit(SwitchGearUnitViewModel switchGearUnitViewModel)
    {
        var switchGearUnit = switchGearUnitViewModel.Item.Clone();
        var index = SwitchGearConstructorViewModel.SwitchGearUnits.IndexOf(this); 
        SwitchGearConstructorViewModel.SwitchGearUnits.Insert(index + 1, new SwitchGearUnitViewModel(switchGearUnit, SwitchGearConstructorViewModel));
    }

    private void ReplaceUnit(SwitchGearUnitViewModel replaced)
    {
        if (IsCatalog && replaced.IsCatalog)
        {
            if (Item.LeftConnector is null == replaced.Item.LeftConnector is null &&
                Item.RightConnector is null == replaced.Item.RightConnector is null)
            {
                
            }
        }
        else
        {
            var switchGearUnit = replaced.Item.Clone();
            var index = SwitchGearConstructorViewModel.SwitchGearUnits.IndexOf(this); 
            SwitchGearConstructorViewModel.SwitchGearUnits.Insert(index, new SwitchGearUnitViewModel(switchGearUnit, SwitchGearConstructorViewModel));
        }
    }

    public SwitchGearUnitViewModel Clone() => new(SwitchGearUnit, SwitchGearConstructorViewModel)
    {
        SwitchGearFunctions = SwitchGearFunctions.ToObservableCollection(),
    };

    public void DragCallback() => SwitchGearConstructorViewModel?.DropMode(this, true);
    public void DropCallback() => SwitchGearConstructorViewModel?.DropMode(this, false);
}