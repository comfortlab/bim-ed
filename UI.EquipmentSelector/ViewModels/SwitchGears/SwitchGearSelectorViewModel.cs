﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearSelectorViewModel : ViewModel
{
    private static readonly string CombinationsMode = CommonDictionary.Combinations;
    private static readonly string FunctionsMode = CommonDictionary.Functions;

    public SwitchGearSelectorViewModel(
        IServiceProvider serviceProvider,
        SwitchGearSeries switchGearSeries)
    {
        SwitchGearSeries = switchGearSeries ?? throw new NullReferenceException(nameof(switchGearSeries));
        if (IsBlockSwitchGear) ElementTypes = new[] { CombinationsMode, FunctionsMode };
        Filter();
    }

    public void Filter()
    {
        if (IsBlockSwitchGear)
        {
            var switchGearUnits = SwitchGearSeries.SwitchGearUnits
                .Where(x => SelectedFunctionCount == null || x.SwitchGearFunctions.Count == SelectedFunctionCount)
                .Where(x => SelectedConnectionMode == null || GetConnectionMode(x) == SelectedConnectionMode)
                .Where(x => SelectedFunction == null || x.SwitchGearFunctions.Any(f => f?.Functions.Contains((Function)SelectedFunction) ?? false))
                .ToArray();

            ConnectionModes = switchGearUnits.Select(GetConnectionMode).Distinct().ToArray();
            Functions = switchGearUnits.SelectMany(x => x.SwitchGearFunctions.SelectMany(f => f?.Functions ?? Array.Empty<Function>())).Distinct().OrderBy(x => x).ToArray();
            FunctionCounts = switchGearUnits.Select(x => x.SwitchGearFunctions.Count).Distinct().OrderBy(x => x).ToArray();
            CatalogSwitchGearUnits = switchGearUnits.Select(x => new SwitchGearUnitViewModel(x, isCatalog: true)).ToArray();
        }

        CatalogSwitchGearFunctions = SwitchGearSeries.SwitchGearFunctions
            .Where(x => SelectedFunction == null || x.Functions.Contains((Function)SelectedFunction))
            .ToArray();
    }

    private static string GetConnectionMode(SwitchGearUnitViewModel switchGearUnit) => GetConnectionMode(switchGearUnit.Item);
    private static string GetConnectionMode(SwitchGearUnit switchGearUnit) => switchGearUnit.LeftConnector switch
    {
        not null when switchGearUnit.RightConnector is not null => "DE",
        not null when switchGearUnit.RightConnector is null     => "LE",
        null     when switchGearUnit.RightConnector is not null => "RE",
        null     when switchGearUnit.RightConnector is null     => "NE",
        _ => throw new ArgumentOutOfRangeException()
    };

    public SwitchGearSeries SwitchGearSeries { get; }
    public SwitchGearFunction[] CatalogSwitchGearFunctions { get; set; }
    public SwitchGearUnitViewModel[] CatalogSwitchGearUnits { get; set; }
    public ObservableCollection<SwitchGearUnitViewModel> SwitchGearUnits { get; } = new();
    public SwitchGearUnitViewModel SelectedSwitchGearUnit { get; set; }
    public bool AnySwitchGearUnits { get; set; }
    public bool IsBlockSwitchGear => SwitchGearSeries.IsBlockSwitchGear;
    public string[] ElementTypes { get; set; } = { CombinationsMode, FunctionsMode };
    public string SelectedElementType { get; set; }
    public bool IsCombinationsMode { get; set; }
    public bool IsFunctionsMode { get; set; }
    public string[] ConnectionModes { get; set; }
    public string SelectedConnectionMode { get; set; }
    public int[] FunctionCounts { get; set; }
    public int? SelectedFunctionCount { get; set; }
    public Function[] Functions { get; set; }
    public Function? SelectedFunction { get; set; }

    public Command ResetAllCommand => new(() =>
    {
        SelectedConnectionMode = null;
        SelectedFunction = null;
        SelectedFunctionCount = null;
    });

    private void OnElementTypesChanged() => SelectedElementType = ElementTypes.FirstOrDefault();
    private void OnSelectedElementTypeChanged()
    {
        IsCombinationsMode = SelectedElementType == CombinationsMode;
        IsFunctionsMode = SelectedElementType == FunctionsMode;
    }
    private void OnSelectedConnectionModeChanged() => Filter();
    private void OnSelectedFunctionChanged() => Filter();
    private void OnSelectedFunctionCountChanged() => Filter();
}