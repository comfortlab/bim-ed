﻿using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;

public class SwitchGearSeriesSelectorViewModel : ViewModel
{
    private readonly Repository<DistributionSystemProxy> _distributionSystemRepository;
    private readonly Interactions.ViewInteraction _viewInteraction;
    private bool _isUpdating;
    
    public SwitchGearSeriesSelectorViewModel(
        Repository<SwitchGearSeries> switchGearSeries,
        Repository<DistributionSystemProxy> distributionSystems,
        DistributionSystemsViewModel distributionSystemsViewModel,
        Interactions interactions)
    {
        AllSwitchGearSeries = switchGearSeries.ToArray();
        _distributionSystemRepository = distributionSystems;
        _viewInteraction = interactions.View;
        _viewInteraction.Dialog.ShowDistributionSystems = () =>
        {
            DistributionSystemsViewModel = distributionSystemsViewModel;
            DistributionSystemsViewModel.Close = () => DistributionSystemsViewModel = null;
            DistributionSystemsViewModel.OkCallback = _ => UpdateDistributionSystems();
        };

        Filter();
        UpdateDistributionSystems();
        DistributionSystemCanBeChanged = true;
    }
    
    protected override void OkAction() => OkCallback?.Invoke(SelectedSwitchGearSeries);
    
    private void Filter()
    {
        if (_isUpdating)
            return;

        SwitchGearSeries = AllSwitchGearSeries
            .Where(x => SelectedGasExhaust == null || x.GasExhausts.Contains((GasExhaust)SelectedGasExhaust))
            .Where(x => SelectedBetweenCompartmentsProtectionIndex == null || x.BetweenCompartmentsProtectionIndexes.Contains((ProtectionIndex)SelectedBetweenCompartmentsProtectionIndex))
            .Where(x => SelectedExternalFacesProtectionIndex == null || x.ExternalFaceProtectionIndexes.Contains((ProtectionIndex)SelectedExternalFacesProtectionIndex))
            .Where(x => SelectedHighVoltagePartsProtectionIndex == null || x.HighVoltagePartsProtectionIndexes.Contains((ProtectionIndex)SelectedHighVoltagePartsProtectionIndex))
            .Where(x => SelectedSwitchMountType == null || x.SwitchMountTypes.Contains((SwitchMountType)SelectedSwitchMountType))
            .ToArray();
        
        GasExhausts = SwitchGearSeries.SelectMany(x => x.GasExhausts).Distinct().ToArray();
        BetweenCompartmentsProtectionIndexes = SwitchGearSeries.SelectMany(x => x.BetweenCompartmentsProtectionIndexes).Distinct().ToArray();
        ExternalFacesProtectionIndexes = SwitchGearSeries.SelectMany(x => x.ExternalFaceProtectionIndexes).Distinct().ToArray();
        HighVoltagePartsProtectionIndexes = SwitchGearSeries.SelectMany(x => x.HighVoltagePartsProtectionIndexes).Distinct().ToArray();
        SwitchMountTypes = SwitchGearSeries.SelectMany(x => x.SwitchMountTypes).Distinct().ToArray();
    }

    private void UpdateDistributionSystems()
    {
        DistributionSystems = _distributionSystemRepository
            .Where(x => x.IsMediumVoltage())
            .OrderBy(n => n.LineToLineVoltage?.ActualValue ?? n.LineToGroundVoltage?.ActualValue)
            .ToArray();
        
        DistributionSystemsOperation = DistributionSystems.Any() ? CommandNames.Change : CommandNames.Create;
    }

    public SwitchGearSeries[] AllSwitchGearSeries { get; set; }
    public SwitchGearSeries[] SwitchGearSeries { get; set; }
    public SwitchGearSeries SelectedSwitchGearSeries { get; set; }

    public GasExhaust[] GasExhausts { get; set; } 
    public GasExhaust? SelectedGasExhaust { get; set; } 
    public ProtectionIndex[] BetweenCompartmentsProtectionIndexes { get; set; }
    public ProtectionIndex[] ExternalFacesProtectionIndexes { get; set; }
    public ProtectionIndex[] HighVoltagePartsProtectionIndexes { get; set; }
    public ProtectionIndex? SelectedBetweenCompartmentsProtectionIndex { get; set; }
    public ProtectionIndex? SelectedExternalFacesProtectionIndex { get; set; }
    public ProtectionIndex? SelectedHighVoltagePartsProtectionIndex { get; set; }
    public SwitchMountType[] SwitchMountTypes { get; set; }  
    public SwitchMountType? SelectedSwitchMountType { get; set; }
    
    public DistributionSystemsViewModel DistributionSystemsViewModel { get; set; }
    public DistributionSystemProxy[] DistributionSystems { get; set; }
    public DistributionSystemProxy SelectedDistributionSystem { get; set; }
    public string DistributionSystemsOperation { get; set; }
    public Command ChangeDistributionSystemsCommand => new(() => { _viewInteraction.Dialog.ShowDistributionSystems(); });
    public bool DistributionSystemCanBeChanged { get; set; }
    public bool IsMainContentEnabled { get; set; }
    public bool OkEnabled { get; set; }

    public Command ResetAllCommand => new(() =>
    {
        _isUpdating = true;
        
        SelectedGasExhaust = null;
        SelectedBetweenCompartmentsProtectionIndex = null;
        SelectedExternalFacesProtectionIndex = null;
        SelectedHighVoltagePartsProtectionIndex = null;
        SelectedDistributionSystem = null;
        
        _isUpdating = false;
        
        Filter();
    });

    private void OnSelectedGasExhaustChanged() => Filter();
    private void OnSelectedBetweenCompartmentsProtectionIndexChanged() => Filter();
    private void OnSelectedExternalFacesProtectionIndexChanged() => Filter();
    private void OnSelectedHighVoltagePartsProtectionIndexChanged() => Filter();
    private void OnSelectedSwitchMountTypeChanged() => Filter();
    private void OnDistributionSystemsViewModelChanged() => IsMainContentEnabled = DistributionSystemsViewModel == null;
    private void OnSelectedSwitchGearSeriesChanged() => OkEnabled = SelectedSwitchGearSeries != null;
}