using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Prototype;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

// ReSharper disable VirtualMemberCallInConstructor

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Base;

public abstract class EquipmentViewModel<T> : ViewModel where T : ElectricalEquipmentProxy
{
    protected readonly IServiceProvider ServiceProvider;
    protected readonly Repository<VirtualElements> VirtualElementsRepository;
    protected readonly VirtualElements VirtualElements;
    protected readonly Interactions.ViewInteraction ViewInteraction;
    protected List<DistributionSystemProxy> AllDistributionSystems;
    private readonly bool _isInitialized;
    private bool _selectedDistributionSystemChanging;
    protected string[] ExistNames;

    protected EquipmentViewModel(IServiceProvider serviceProvider, T equipment)
    {
        ServiceProvider = serviceProvider;
        VirtualElementsRepository = ServiceProvider.Get<Repository<VirtualElements>>();
        VirtualElements = VirtualElementsRepository.First();
        OriginalEquipment = equipment;
        EditedEquipment = equipment?.CloneAs<T>() ?? equipment ?? CreateDefault();
        Name = EditedEquipment?.Name;
        CanBeChanged = EditedEquipment?.ConsumerConnectors.IsEmpty() ?? true;
        IsCreateMode = equipment == null;
        IsLoadConnected = equipment?.Consumers.Any() ?? false;
        
        ViewInteraction = ServiceProvider.Get<Interactions>().View;
        ViewInteraction.Dialog.ShowDistributionSystems = () =>
        {
            DistributionSystemsViewModel = ServiceProvider.Get<DistributionSystemsViewModel>();
            DistributionSystemsViewModel.Close = () => DistributionSystemsViewModel = null;
            DistributionSystemsViewModel.OkCallback = _ => UpdateDistributionSystems();
        };
        
        UpdateDistributionSystems();

        _isInitialized = true;
    }

    public string Name { get; set; }
    public T OriginalEquipment { get; set; }
    public T EditedEquipment { get; set; }
    public DistributionSystemsViewModel DistributionSystemsViewModel { get; set; }
    public List<DistributionSystemProxy> DistributionSystems { get; set; }
    public DistributionSystemProxy SelectedDistributionSystem { get; set; }
    public string DistributionSystemsOperation { get; set; }
    public List<PhasesNumber> PhasesNumbers { get; set; }
    public PhasesNumber? SelectedPhasesNumber { get; set; }
    public List<ElectricalEquipmentProxy> AllCompatibles { get; set; }
    public List<ElectricalEquipmentProxy> Compatibles { get; set; }
    public ElectricalEquipmentProxy Connected { get; set; }
    public Action DeleteCallback { get; set; }
    public bool CanBeChanged { get; set; }
    public bool CanBePlacedInModel { get; set; }
    public bool ExistsInModel { get; set; }
    public bool IsDeleteVisible { get; set; }
    // public bool IsDistributionSystemsVisible { get; set; }
    public bool IsChanged { get; set; }
    public bool IsCreateMode { get; set; }
    public bool IsLoadConnected { get; set; }
    public bool IsMainContentEnabled { get; set; } = true;
    public bool IsNameExists { get; set; }
    public bool IsOkEnabled { get; set; }

    protected abstract T CreateDefault();
    protected virtual void Create() { }

    protected virtual void Delete()
    {
        var repository = ServiceProvider.Get<Repository<T>>();
        repository.Remove(OriginalEquipment);
        
        if (OriginalEquipment.IsVirtual)
        {
            VirtualElements.Remove(OriginalEquipment);
            VirtualElementsRepository.ChangedElements.UpdateStorage();
            VirtualElementsRepository.Save();
        }
        else
        {
            repository.ChangedElements.Delete(OriginalEquipment.RevitId);
            repository.Save();
        }
    }

    protected virtual void Modify()
    {
        OriginalEquipment.PullProperties<T>(EditedEquipment);
        
        if (OriginalEquipment.IsVirtual)
        {
            VirtualElementsRepository.ChangedElements.UpdateStorage();
            VirtualElementsRepository.Save();
        }
        else
        {
            var repository = ServiceProvider.Get<Repository<T>>();
            repository.ChangedElements.Modify(EditedEquipment.RevitId);
            repository.Save();
        }
    }
    
    // ReSharper disable UnusedMember.Local UnusedMember.Global
    protected void OnDistributionSystemsViewModelChanged() => IsMainContentEnabled = DistributionSystemsViewModel == null;
    private void OnIsDeleteVisibleChanged() => IsMainContentEnabled = !IsDeleteVisible;

    private void OnNameChanged()
    {
        if (!_isInitialized)
            return;
        
        EditedEquipment.Name = Name;
        IsNameExists = ExistNames?.Contains(Name) ?? false; 
        UpdateOkEnabled();
        Change();
    }

    protected virtual void OnSelectedPhasesNumberChanged()
    {
        if (_selectedDistributionSystemChanging)
            return;

        UpdateDistributionSystems();
        
        DistributionSystems = SelectedPhasesNumber != null
            ? DistributionSystems.Where(n => n.PhasesNumber == SelectedPhasesNumber).ToList()
            : DistributionSystems;

        if (DistributionSystems.Count == 1)
            SelectedDistributionSystem = DistributionSystems.First();
            
        CheckForName();
        UpdateCompatibles();
    }

    protected virtual void OnSelectedDistributionSystemChanged()
    {
        if (SelectedDistributionSystem != null)
        {
            _selectedDistributionSystemChanging = true;
                
            EditedEquipment.SetDistributionSystem(SelectedDistributionSystem);
                
            EditedEquipment.PowerParameters.LineToGroundVoltage =
                SelectedDistributionSystem.LineToGroundVoltage?.ActualValue ??
                SelectedDistributionSystem.LineToLineVoltage?.ActualValue ?? 0 / Math.Sqrt(3);
            SelectedPhasesNumber = SelectedDistributionSystem.PhasesNumber;

            CheckForName();
            
            _selectedDistributionSystemChanging = false;
        }

        UpdateCompatibles();
        UpdateOkEnabled();
        Change();
    }
    
    protected override void OkAction()
    {
        try
        {
            if (IsCreateMode)
            {
                Create();
                OkCallback?.Invoke(EditedEquipment);
            }
            else if (IsChanged)
            {
                Modify();
                OkCallback?.Invoke(EditedEquipment);
            }
        }
        finally
        {
            Close?.Invoke();
        }
    }

    public Command DeleteCommand => new(() => IsDeleteVisible = true);
    public Command DeleteCancelCommand => new(() => IsDeleteVisible = false);
    public Command DeleteOkCommand => new(() =>
    {
        try
        {
            Delete();
            DeleteCallback?.Invoke();
        }
        finally
        {
            Close?.Invoke();
        }
    });
    
    public Command ChangeDistributionSystemsCommand => new(() => { ViewInteraction.Dialog.ShowDistributionSystems(); });
    public Command ConnectToCommand => new(Connect);
    public Command PlaceInModelCommand => new(() => { });
    public Command SelectInModelCommand => new(() => { });
    
    private void CheckForName()
    {
        if (SelectedDistributionSystem == null)
            return;
        
        if (string.IsNullOrWhiteSpace(Name) ||
            (Name != SelectedDistributionSystem.Name &&
             AllDistributionSystems.Select(x => x.Name).Contains(Name)))
            Name = SelectedDistributionSystem.Name;
    }

    protected virtual void Connect() => ViewInteraction.ElectricalDiagram.ConnectNode(EditedEquipment);

    protected virtual void UpdateCompatibles() { }
    
    protected virtual void UpdateDistributionSystems()
    {
        AllDistributionSystems = ServiceProvider.Get<Repository<DistributionSystemProxy>>()
            .OrderBy(n => n.LineToLineVoltage?.ActualValue ?? n.LineToGroundVoltage?.ActualValue)
            .ToList();
            
        DistributionSystems = AllDistributionSystems;
        PhasesNumbers = AllDistributionSystems.Select(n => n.PhasesNumber).Distinct().ToList();
        DistributionSystemsOperation = AllDistributionSystems.Any() ? CommandNames.Change : CommandNames.Create;
        SelectedDistributionSystem = EditedEquipment?.DistributionSystem;
    }

    protected virtual void UpdateOkEnabled()
    {
        IsOkEnabled =
            !string.IsNullOrWhiteSpace(Name) &&
            SelectedDistributionSystem != null;
    }

    protected void Change() { if (_isInitialized) IsChanged = true; }
}