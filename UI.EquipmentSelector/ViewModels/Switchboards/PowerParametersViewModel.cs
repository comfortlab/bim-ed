using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class PowerParametersViewModel : ElectricalParametersViewModel
{
    public PowerParametersViewModel(ElectricalBase electrical, OperatingMode operatingMode) :
        base(electrical, operatingMode) { }

    public double ApparentLoad { get; set; }
    public double TrueLoad { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double EstimateCurrent { get; set; }
    public double ShortCurrent1 { get; set; }
    public double ShortCurrent3 { get; set; }
    public double SurgeShortCurrent3 { get; set; }
    public double VoltageDrop { get; set; }
    public double VoltageDropPercent { get; set; }
    public double PowerFactor { get; set; }
    public double DemandFactor { get; set; }
    public double AdditionalDemandFactor { get; set; }
    public double TotalDemandFactor { get; set; }
    public bool ShowParameters { get; set; }
        
    protected override void UpdateProperties()
    {
        ApparentLoad = EstimatedPowerParameters.ApparentLoad;
        TrueLoad = EstimatedPowerParameters.TrueLoad;
        EstimateTrueLoad = EstimatedPowerParameters.EstimateTrueLoad;
        EstimateCurrent = EstimatedPowerParameters.Current;
        ShortCurrent1 = EstimatedPowerParameters.ShortCurrent1;
        ShortCurrent3 = EstimatedPowerParameters.ShortCurrent3;
        SurgeShortCurrent3 = EstimatedPowerParameters.SurgeShortCurrent3;
        VoltageDrop = EstimatedPowerParameters.VoltageDrop;
        VoltageDropPercent = EstimatedPowerParameters.VoltageDropPercent;
        DemandFactor = EstimatedPowerParameters.DemandFactor;
        AdditionalDemandFactor = EstimatedPowerParameters.AdditionalDemandFactor;
        TotalDemandFactor = EstimatedPowerParameters.TotalDemandFactor;
        PowerFactor = EstimatedPowerParameters.PowerFactor;
        ShowParameters =
            ShortCurrent1 > 0 ||
            ShortCurrent3 > 0 ||
            SurgeShortCurrent3 > 0 ||
            VoltageDrop > 0;
    }
}