﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public abstract class SwitchViewModel : SelectorProductViewModel
{
    protected SwitchViewModel(IServiceProvider serviceProvider, ElectricalBase electrical, ElectricalProducts products) :
        base(serviceProvider, electrical, products)
    {
        MainProduct = Products.MainSwitch?.Product != null ? Products.MainSwitch : null;
        Designation = Products.MainSwitch?.Designation;
    }

    protected override void SaveChanges()
    {
            
    }

    public override void OnMainProductChanged()
    {
        Products.MainSwitch = MainProduct;
        base.OnMainProductChanged();
    }

    protected override void OnDesignationChanged()
    {
        if (Products.MainSwitch != null)
            Products.MainSwitch.Designation = Designation;
    }

    internal virtual void Update()
    {
        MainProduct = Products.MainSwitch;
    }
}