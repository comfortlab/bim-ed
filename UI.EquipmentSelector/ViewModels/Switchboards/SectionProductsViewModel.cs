﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SectionProductsViewModel
{
    public string PanelDesignation { get; set; }
    public ProductWithAccessoriesViewModel MainSwitch { get; set; }
    public ProductWithAccessoriesViewModel EnergyMeter { get; set; }
    public List<ProductWithAccessoriesViewModel> Products { get; set; }
    public List<CircuitProductsViewModel> Circuits { get; set; }

    public SectionProductsViewModel(SwitchBoardUnit panel, string panelName)
    {
        PanelDesignation = panelName;

        if (panel.Products?.MainSwitch is { } mainSwitch)
        {
            MainSwitch = new ProductWithAccessoriesViewModel(mainSwitch);
        }

        if (panel.SwitchBoardUnitProducts?.EnergyMeter is { } energyMeter)
        {
            EnergyMeter = new ProductWithAccessoriesViewModel(energyMeter);
        }

        Products = panel.Products?.Products
            .Select(p => new ProductWithAccessoriesViewModel(p))
            .GetGrouped();

        var circuits = panel.GetAllConsumersOf<ElectricalSystemProxy>();

        Circuits = circuits
            .Select(c => new CircuitProductsViewModel(c))
            .ToList();
    }

    public void SortProductsByReference()
    {
        Products = Products.GetSortedByReference();

        Circuits.ForEach(c => c.SortProductsByReference());
    }

    public void SortProductsByDescription()
    {
        Products = Products.GetSortedByDescription();

        Circuits.ForEach(c => c.SortProductsByDescription());
    }

    public List<ProductWithAccessoriesViewModel> GetProductsWithAccessories()
    {
        var products = new List<ProductWithAccessoriesViewModel>();

        if (MainSwitch != null)
        {
            products.Add(MainSwitch.Clone());
        }

        if (EnergyMeter != null)
        {
            products.Add(EnergyMeter.Clone());
        }

        products.AddRange(Products.Select(p => p.Clone()));

        Circuits.ForEach(c => products.AddRange(c.GetProductsWithAccessories()));

        return products;
    }

    public List<ProductViewModel> GetProducts()
    {
        var products = new List<ProductViewModel>();

        if (MainSwitch != null)
        {
            products.AddRange(MainSwitch.GetProducts());
        }

        if (EnergyMeter != null)
        {
            products.AddRange(EnergyMeter.GetProducts());
        }

        Products.ForEach(p => products.AddRange(p.GetProducts()));
        Circuits.ForEach(c => products.AddRange(c.GetProducts()));

        return products;
    }
}