﻿using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.UI.Framework;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class EnclosureViewModel
{
    public SelectorProduct Product { get; set; }
    public SwitchBoardDiagramViewModel SwitchBoardDiagramViewModel { get; set; }
    public bool HasAccessories { get; set; }
    public bool HasAccessoriesBothTypes { get; set; }

    public EnclosureViewModel(SelectorProduct product, SwitchBoardDiagramViewModel switchBoardDiagramViewModel)
    {
        Product = product;
        SwitchBoardDiagramViewModel = switchBoardDiagramViewModel;
        HasAccessories = Product.Accessories.Any() || Product.Accessories.Any();
        HasAccessoriesBothTypes = Product.Accessories.Any() && Product.Accessories.Any();
    }

    public Command<string> GoToLinkCommand => new(commandAction: url => Process.Start(new ProcessStartInfo(url)));

    public Command OpenEnclosureCommand => new(() =>
    {
        SwitchBoardDiagramViewModel.EditedEnclosure = Product;
        SwitchBoardDiagramViewModel.EnclosuresPopupOpen = false;

        try
        {
            // DialogStackView.PushView(
            //     viewContent: new SelectorsView(
            //         selectors: SeApi.Selectors.GetSelectorsById(Product.SelectorId),
            //         electricalItem: SwitchboardViewModel.Switchboard.FirstUnit,
            //         callback: SwitchboardViewModel.ApplyEnclosure,
            //         configuration: SwitchboardViewModel?.EditedEnclosure?.SerializedConfiguration),
            //     cancelCallback: () => SwitchboardViewModel.EditedEnclosure = null, buttons: DialogButtons.CancelClose);
        }
        finally
        {
            SwitchBoardDiagramViewModel.EnclosuresPopupOpen = true;
        }
    });

    public Command DuplicateCommand => new(() =>
    {
        var index = SwitchBoardDiagramViewModel.SwitchBoardUnitProducts.Enclosures.IndexOf(Product);

        var productClone = Product.Clone();
        SwitchBoardDiagramViewModel.SwitchBoardUnitProducts.Enclosures.Insert(index + 1, productClone);
        SwitchBoardDiagramViewModel.EnclosureViewModels.Insert(index + 1, new EnclosureViewModel(productClone, SwitchBoardDiagramViewModel));

        SwitchBoardDiagramViewModel.UpdateEnclosuresInfo();
    });

    public Command DeleteCommand => new(() =>
    {
        SwitchBoardDiagramViewModel.EnclosureViewModels.Remove(this);
        SwitchBoardDiagramViewModel.SwitchBoardUnitProducts.Enclosures.Remove(Product);

        SwitchBoardDiagramViewModel.UpdateEnclosuresInfo();
    });
}