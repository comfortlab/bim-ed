﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class DeleteViewModel : ViewModel
{
    private readonly Repository<ElectricalSystemProxy> _electricalSystemRepository;
    private readonly Repository<SwitchBoardUnit> _switchboardUnitRepository;
    private bool _checkChanging;
    private List<ElectricalSystemProxy> _electricalCircuits;
    private List<SwitchBoard> _electricalPanels;
    private IReadOnlyCollection<ElectricalBase> _electricals;

    public DeleteViewModel(IServiceProvider serviceProvider, SwitchBoardDiagramViewModel switchBoardDiagramViewModel)
    {
        _electricalSystemRepository = serviceProvider.Get<Repository<ElectricalSystemProxy>>();
        _switchboardUnitRepository = serviceProvider.Get<Repository<SwitchBoardUnit>>();
        SwitchBoardDiagramViewModel = switchBoardDiagramViewModel;
    }

    public SwitchBoardDiagramViewModel SwitchBoardDiagramViewModel { get; set; }

    public bool IsAllChecked { get; set; }
    public bool IsCableChecked { get; set; }
    public bool IsEnergyMeterChecked { get; set; }
    public bool IsSwitchChecked { get; set; }

    public bool IsAnyProductExists { get; set; }
    public bool IsCablesExist { get; set; }
    public bool IsEnergyMetersExist { get; set; }
    public bool IsSwitchesExist { get; set; }

    public Command DeleteCommand => new(() =>
        {
            if (IsCableChecked)
            {
                _electricalCircuits.ForEach(n =>
                {
                    n.Products.SetCable(null);
                    _electricalSystemRepository.ChangedElements.Modify(n.RevitId);
                });
            }

            if (IsSwitchChecked)
            {
                _electricalCircuits.ForEach(n =>
                {
                    n.Products.MainSwitch = null;
                    _electricalSystemRepository.ChangedElements.Modify(n.RevitId);
                });

                _electricalPanels.ForEach(n =>
                {
                    n.Products.MainSwitch = null;
                    _switchboardUnitRepository.ChangedElements.Modify(n.RevitId);
                });
            }

            SwitchBoardDiagramViewModel.UpdateProducts(_electricals);
            SwitchBoardDiagramViewModel.UpdateSelections();

            _electricalSystemRepository.Save();
            _switchboardUnitRepository.Save();
        },
        canExecute: () =>
            IsCableChecked ||
            IsEnergyMeterChecked ||
            IsSwitchChecked);

    private void OnIsAllCheckedChanged()
    {
        if (_checkChanging)
            return;

        _checkChanging = true;

        var isAllChecked =
            (!IsCablesExist || IsCableChecked) &&
            (!IsEnergyMetersExist || IsEnergyMeterChecked) &&
            (!IsSwitchesExist || IsSwitchChecked);

        IsCableChecked = !isAllChecked;
        IsEnergyMeterChecked = !isAllChecked;
        IsSwitchChecked = !isAllChecked;

        _checkChanging = false;
    }

    private void OnIsCableCheckedChanged() => UpdateAllChecked();
    private void OnIsEnergyMeterCheckedChanged() => UpdateAllChecked();
    private void OnIsSwitchCheckedChanged() => UpdateAllChecked();

    private void UpdateAllChecked()
    {
        if (_checkChanging)
            return;

        _checkChanging = true;

        IsAllChecked =
            (!IsCablesExist || IsCableChecked) &&
            (!IsEnergyMetersExist || IsEnergyMeterChecked) &&
            (!IsSwitchesExist || IsSwitchChecked);

        _checkChanging = false;
    }

    internal void Update(IReadOnlyCollection<ElectricalBase> electricals)
    {
        if (electricals == null)
            return;
            
        _electricals = electricals;
        _electricalCircuits = _electricals.OfType<ElectricalSystemProxy>().ToList();
        _electricalPanels = _electricals.OfType<SwitchBoard>().ToList();

        IsCablesExist = _electricalCircuits.Any(n => n.Products.Cable != null);
        IsEnergyMetersExist = _electricalPanels.Any(n => n.SwitchBoardUnitProducts.EnergyMeter!= null);
        IsSwitchesExist =
            _electricalCircuits.Any(n => n.Products.MainSwitch != null) ||
            _electricalPanels.Any(n => n.Products.MainSwitch != null);

        IsAnyProductExists =
            IsCablesExist ||
            IsEnergyMetersExist ||
            IsSwitchesExist;
    }
}