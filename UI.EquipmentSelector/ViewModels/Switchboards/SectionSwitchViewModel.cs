﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SectionSwitchViewModel : SwitchViewModel
{
    public SectionSwitchViewModel(IServiceProvider serviceProvider, SwitchBoardDiagramViewModel switchBoardDiagramViewModel, ElectricalProducts electricalProducts) :
        base(serviceProvider, null, electricalProducts)
    // TODO # 2021.10.19
    // base(switchboardViewModel.Switchboard, electricalProducts)
    {

    }

    protected override void AddProduct()
    {
        // DialogStackView.PushView(
        //     viewContent: new SelectorGroupsView(
        //         selectorGroups: new[]
        //         {
        //             SelectorGroup.CircuitBreakers,
        //             SelectorGroup.SwitchDisconnectors,
        //         },
        //         selectGroupCallback: selectorGroup =>
        //         {
        //             DialogStackView.CloseLastView();
        //
        //             DialogStackView.PushView(
        //                 viewContent: new SelectorsView(
        //                     selectors: SeApi.Selectors.GetApplicability<SwitchboardUnit>(selectorGroup),
        //                     electricalItem: Electrical,
        //                     callback: config =>
        //                     {
        //                         ApplyProduct(config, selectorGroup);
        //                     }),
        //                 buttons: DialogButtons.CancelClose);
        //         }),
        //     buttons: DialogButtons.CancelClose,
        //     alignment: DialogAlignment.Center);
    }

    protected override void OpenProduct(string configuration)
    {
        var selectorGroup = (SelectorGroup)MainProduct.SelectorGroup;

        // DialogStackView.PushView(
        //     viewContent: new SelectorsView(
        //         selectors: SeApi.Selectors.GetApplicability<SwitchboardUnit>(selectorGroup),
        //         electricalItem: Electrical,
        //         callback: config => ApplyProduct(config, selectorGroup),
        //         configuration: configuration),
        //     buttons: DialogButtons.CancelClose);
    }

    protected override void SaveChanges()
    {
        // using var unitOfWork = DomainUnitOfWork.NewInstance();
        // var repository = new ElectricalPanelRepository(unitOfWork);
        // repository.Update(_electricalPanel);
        // unitOfWork.SaveChanges("Apply product");
    }
}