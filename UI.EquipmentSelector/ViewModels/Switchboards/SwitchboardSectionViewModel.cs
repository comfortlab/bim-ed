﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SwitchboardSectionViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly bool _initialized;
    private bool _selectionUpdating;
    private bool _connectorStatesUpdating;
    private bool _propertiesUpdating;

    #region Properties

    private OperatingMode _operatingMode;
    public SwitchBoardDiagramViewModel SwitchBoardDiagramViewModel { get; set; }
    public SwitchBoardUnit Unit { get; set; }
    public List<CircuitGroupViewModel> CircuitGroups { get; set; }
    public ObservableCollection<CircuitViewModel> CircuitViewModels { get; set; }
    public List<CircuitViewModel> SelectedElectricalCircuits { get; set; }
    public EnergyMeterViewModel EnergyMeterViewModel { get; set; }
    public InputSwitchViewModel InputSwitchViewModel { get; set; }
    public SectionSwitchViewModel SectionSwitchViewModel { get; set; }
    public CurrentsViewModel CurrentsViewModel { get; set; }
    public PowerParametersViewModel PowerParametersViewModel { get; set; }
    public DistributionWiresViewModel DistributionWiresViewModel { get; set; }
    public string Name { get; set; }
    public bool IsBaseConnectorActive { get; set; }
    public bool IsReserveSourceActive { get; set; }
    public bool IsSectionConnectorActive { get; set; }
    public bool IsSinglePanel { get; set; }
    public bool IsPanelSelected { get; set; }
    public bool IsAnyCircuitSelected { get; set; }
    public bool IsOneCircuitSelected { get; set; }
    public bool IsManyCircuitsSelected { get; set; }
    public bool IsThreePhases { get; set; }
    public double HorizontalOffset { get; set; }
    public double VerticalOffset { get; set; }
    public double MaxVerticalOffset { get; set; }
    public bool Wire2 { get; set; }
    public bool Wire3 { get; set; }
    public bool BottomParametersSeparateLine { get; set; }
    public bool TopParametersSeparateLine { get; set; }

    #endregion

    public SwitchboardSectionViewModel(IServiceProvider serviceProvider, SwitchBoardDiagramViewModel switchBoardDiagramViewModel, SwitchBoardUnit switchBoardUnit)
    {
        _serviceProvider = serviceProvider;
        SwitchBoardDiagramViewModel = switchBoardDiagramViewModel;
        Unit = switchBoardUnit ?? throw new NullReferenceException(nameof(Unit));
        Name = Unit.Name;

        if (!Unit.IsPower)
            throw new InvalidOperationException($"{nameof(Unit)} must be Power");

        SetElectricalCircuits();
            
        IsSinglePanel = Unit.SwitchBoard.Units.Count == 1;
        IsThreePhases = Unit.PowerParameters.PhasesNumber > PhasesNumber.One;

        SectionSwitchViewModel = IsSinglePanel || Equals(switchBoardUnit.SwitchBoard.FirstUnit, switchBoardUnit)
            ? null
            : new SectionSwitchViewModel(_serviceProvider, switchBoardDiagramViewModel, switchBoardUnit.Products);

        CircuitGroups = new List<CircuitGroupViewModel>();
        EnergyMeterViewModel = new EnergyMeterViewModel(_serviceProvider, this);
        InputSwitchViewModel = new InputSwitchViewModel(_serviceProvider, this);
        DistributionWiresViewModel = new DistributionWiresViewModel(Unit.PowerParameters);
            
        Wire2 = Unit.PowerParameters.PhasesNumber > PhasesNumber.One;
        Wire3 = Unit.PowerParameters.PhasesNumber > PhasesNumber.Two;

        _operatingMode = _serviceProvider.Get<OperatingModes>().Current;
            
        CurrentsViewModel = IsThreePhases ? new CurrentsViewModel(Unit, _operatingMode) : null;
        PowerParametersViewModel = new PowerParametersViewModel(Unit, _operatingMode);

        UpdateProperties();
        OnVerticalOffsetChanged();

        _initialized = true;
    }

    internal void SetElectricalCircuits()
    {
        CircuitViewModels = Unit.GetConsumersOf<ElectricalSystemProxy>()
            .Select(n => new CircuitViewModel(_serviceProvider, this, n)).ToObservableCollection();
    }

    private void OnIsPanelSelectedChanged()
    {
        if (_selectionUpdating)
            return;

        SelectedElectricalCircuits?.ForEach(n => n.IsSelected = false);
        UpdateSelections();
    }

    private void OnNameChanged()
    {
        Unit.Name = Name;
    }

    private void OnIsBaseConnectorActiveChanged()
    {
        if (!_initialized || _connectorStatesUpdating)
            return;

        var connectingSwitchboardUnits = new List<SwitchBoardUnit> { Unit };

        if (IsBaseConnectorActive)
            DisconnectSuppliedUnits(connectingSwitchboardUnits);

        Unit.BaseConnector.SetState(IsBaseConnectorActive, _operatingMode);

        RecalculateAndUpdateProperties(connectingSwitchboardUnits);
    }

    #region OnIsBaseConnectorActiveChanged

    private void DisconnectSuppliedUnits(List<SwitchBoardUnit> connectingSwitchboardUnits)
    {
        var leftConnected = Unit.GetLeftConnected(includeOwner: false, _operatingMode).ToList();
        var rightConnected = Unit.GetRightConnected(includeOwner: false, _operatingMode).ToList();

        if (leftConnected.Any(unit => unit.BaseConnector.GetState(_operatingMode)))
        {
            connectingSwitchboardUnits.AddRange(leftConnected.OfType<SwitchBoardUnit>());
            Unit.LeftConnector.SetState(false, _operatingMode);
            UpdateConnectorStates();
        }

        if (rightConnected.Any(unit => unit.BaseConnector.GetState(_operatingMode)))
        {
            connectingSwitchboardUnits.AddRange(rightConnected.OfType<SwitchBoardUnit>());
            var index = SwitchBoardDiagramViewModel.SectionViewModels.IndexOf(this);
            var rightSection = SwitchBoardDiagramViewModel.SectionViewModels.ElementAtOrDefault(index + 1);
            rightSection?.Unit.LeftConnector?.SetState(false, _operatingMode);
            rightSection?.UpdateConnectorStates();
        }
    }

    #endregion

    private void OnIsSectionConnectorActiveChanged()
    {
        if (!_initialized || _connectorStatesUpdating)
            return;
            
        if (!IsSectionConnectorActive)
        {
            Unit.LeftConnector.SetState(false, _operatingMode);
            RecalculateAndUpdateProperties(GetConnectingSwitchboardUnits());
            return;
        }
            
        var allConnectingSwitchboardUnits = GetAllConnectingSwitchboardUnits();

        if (OnlyOneActiveInput(allConnectingSwitchboardUnits))
        {
            Unit.LeftConnector.SetState(true, _operatingMode);
            RecalculateAndUpdateProperties(allConnectingSwitchboardUnits);
            return;
        }
            
        // DialogStackView.PushView(
        //     viewContent: new SectionSwitchVerificationView(Unit),
        //     okCallback: () =>
        //     {
        //         SetConnectorStates(allConnectingSwitchboardUnits);
        //         return true;
        //     },
        //     okEnabled: false,
        //     cancelCallback: () =>
        //     {
        //         IsSectionConnectorActive = false;
        //     },
        //     buttons: DialogButtons.OkCancel, alignment: DialogAlignment.Center);
    }

    #region OnIsSectionConnectorActiveChanged

    private List<SwitchBoardUnit> GetConnectingSwitchboardUnits()
    {
        var units = new List<SwitchBoardUnit> { Unit };
        var index = Unit.SwitchBoard.Units.IndexOf(Unit);

        if (index > 0)
            units.Insert(0, Unit.SwitchBoard.Units.ElementAtOrDefault(index - 1));

        return units;
    }

    private List<SwitchBoardUnit> GetAllConnectingSwitchboardUnits()
    {
        var index = Unit.SwitchBoard.Units.IndexOf(Unit);
        var leftSwitchboardUnit = index > 0
            ? Unit.SwitchBoard.Units.ElementAtOrDefault(index - 1)
            : null;
        var leftSwitchboardUnits =
            leftSwitchboardUnit?.GetLeftConnected(includeOwner: true, _operatingMode).Reverse().OfType<SwitchBoardUnit>() ??
            new List<SwitchBoardUnit>();
        var rightSwitchboardUnits = Unit.GetLeftConnected(includeOwner: true, _operatingMode).OfType<SwitchBoardUnit>();
            
        return leftSwitchboardUnits.Concat(rightSwitchboardUnits).ToList();
    }

    private bool OnlyOneActiveInput(IEnumerable<SwitchBoardUnit> connectedSwitchboardUnits)
    {
        return connectedSwitchboardUnits.Sum(n =>
            (n.BaseConnector.GetState(_operatingMode) ? 1 : 0) +
            (n.ReserveSourceConnector?.GetState(_operatingMode) ?? false ? 1 : 0)) < 2;
    }

    private void SetConnectorStates(List<SwitchBoardUnit> allConnectingSwitchboardUnits)
    {
        Unit.LeftConnector.SetState(true);

        // using var unitOfWork = UnitOfWork.NewInstance();
        // var repository = new SwitchboardUnitRepository(unitOfWork);
        // var switchedConnector = Stash.Instance.Pop<ConnectorProxy>();
        // var changedSwitchboardUnits = GetConnectingSwitchboardUnits();
        //
        // foreach (var unit in allConnectingSwitchboardUnits)
        // {
        //     var oldBaseState = unit.BaseConnector.GetState(_operatingMode);
        //     var oldReserveState = unit.ReserveSourceConnector?.GetState(_operatingMode) ?? false;
        //     var newBaseState = unit.BaseConnector == switchedConnector;
        //     var newReserveState = unit.ReserveSourceConnector == switchedConnector;
        //
        //     if (newBaseState != oldBaseState)
        //     {
        //         if (!changedSwitchboardUnits.Contains(unit))
        //             changedSwitchboardUnits.Add(unit);
        //
        //         unit.BaseConnector.SetState(newBaseState, _operatingMode);
        //             
        //         repository.Update(unit);
        //     }
        //
        //     if (newReserveState != oldReserveState)
        //     {
        //         if (!changedSwitchboardUnits.Contains(unit))
        //             changedSwitchboardUnits.Add(unit);
        //             
        //         unit.ReserveSourceConnector?.SetState(newReserveState, _operatingMode);
        //
        //         repository.Update(unit);
        //     }
        // }
        //
        // RecalculateAndUpdateProperties(changedSwitchboardUnits);
    }

    private void RecalculateAndUpdateProperties(IEnumerable<SwitchBoardUnit> electricals)
    {
        foreach (var unit in electricals)
        {
            // ElectricalCalculator.Loads.Calculate(unit, _operatingMode, true);

            var source = unit.GetConnectedSource(_operatingMode);
            var baseSource = unit.BaseConnector.Source;
            var reserveSource = unit.ReserveSourceConnector?.Source;
                
            // if (baseSource != null && !Equals(baseSource, source))
            //     ElectricalCalculator.Loads.Calculate(baseSource, _operatingMode, true);
            //         
            // if (reserveSource != null && !Equals(reserveSource, unit))
            //     ElectricalCalculator.Loads.Calculate(reserveSource, _operatingMode, true);
        }

        SwitchBoardDiagramViewModel.UpdateProperties(_operatingMode);
    }

    #endregion

    private void OnVerticalOffsetChanged()
    {
        BottomParametersSeparateLine = VerticalOffset < MaxVerticalOffset;
        TopParametersSeparateLine = VerticalOffset > 0;
    }

    internal void UpdateProducts()
    {
        InputSwitchViewModel.Update();
    }

    internal void UpdateProperties(OperatingMode operatingMode = null)
    {
        if (_propertiesUpdating)
            return;

        _propertiesUpdating = true;
            
        if (operatingMode != null)
            _operatingMode = operatingMode;

        UpdateConnectorStates();
        CircuitViewModels.ForEach(n => n.UpdateProperties(_operatingMode));
        CurrentsViewModel?.UpdateProperties(_operatingMode);
        PowerParametersViewModel?.UpdateProperties(_operatingMode);
            
        _propertiesUpdating = false;
    }

    private void UpdateConnectorStates()
    {
        _connectorStatesUpdating = true;
            
        IsBaseConnectorActive = Unit.BaseConnector.GetState(_operatingMode);
        IsSectionConnectorActive = Unit.LeftConnector.GetState(_operatingMode);

        _connectorStatesUpdating = false;
    }

    internal void UpdateSelections()
    {
        if (_selectionUpdating)
            return;

        _selectionUpdating = true;

        SwitchBoardDiagramViewModel.ResetSelections(except: this);
        SelectedElectricalCircuits = CircuitViewModels
            .Where(n => n.IsSelected)
            .Concat(CircuitViewModels
                .Where(n => n.GroupViewModel != null)
                .SelectMany(n => n.GroupViewModel.ElectricalCircuits)
                .Where(n => n.IsSelected))
            .ToList();
        IsOneCircuitSelected = SelectedElectricalCircuits.Count == 1;
        IsManyCircuitsSelected = SelectedElectricalCircuits.Count > 1;
        IsAnyCircuitSelected = IsOneCircuitSelected || IsManyCircuitsSelected;

        if (IsAnyCircuitSelected)
            IsPanelSelected = false;

        _selectionUpdating = false;

        SwitchBoardDiagramViewModel.SelectedSwitchboardSection = IsPanelSelected ? this : null;
        SwitchBoardDiagramViewModel.SelectedCircuits = SelectedElectricalCircuits.ToList();
    }

    internal void UpdateCircuitPhaseMarks()
    {
        CircuitViewModels.ForEach(n => n.UpdatePhases());
    }
}