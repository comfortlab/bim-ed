using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class DistributionWiresViewModel : ViewModel
{
    public PowerParameters PowerParameters { get; set; }
    public bool L1 { get; set; }
    public bool L2 { get; set; }
    public bool L3 { get; set; }
    public bool N { get; set; }
    public bool Pe { get; set; }
        
    public DistributionWiresViewModel(PowerParameters powerParameters, bool neutral = true, bool earth = true)
    {
        PowerParameters = powerParameters;
            
        if (PowerParameters == null)
            throw new NullReferenceException(nameof(PowerParameters));

        N = neutral;
        Pe = earth;
            
        SetPhaseMarks();
    }

    public void SetPhaseMarks()
    {
        var phase = PowerParameters.Phase;

        switch (PowerParameters.PhasesNumber)
        {
            case PhasesNumber.One:
                L1 = phase is Phase.L1;
                L2 = phase is Phase.L2;
                L3 = phase is Phase.L3;
                break;

            case PhasesNumber.Two:
                L1 = phase is Phase.L12 or Phase.L13;
                L2 = phase is Phase.L12 or Phase.L23;
                L3 = phase is Phase.L13 or Phase.L23;
                break;

            case PhasesNumber.Three:
                L1 = true;
                L2 = true;
                L3 = true;
                break;
        }
    }
}