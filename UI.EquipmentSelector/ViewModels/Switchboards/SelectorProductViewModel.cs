﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public abstract class SelectorProductViewModel : ViewModel
{
    private readonly SeApi.Converters _seApiConverter;
    public ElectricalBase Electrical { get; set; }
    public ElectricalProducts Products { get; set; }
    public SelectorProduct MainProduct { get; set; }
    public List<ProductViewModel> Accessories { get; set; }
    public string Designation { get; set; }
    public string MainProductType { get; set; }
    public string AccessoriesNames { get; set; }
        
    protected SelectorProductViewModel(IServiceProvider serviceProvider, ElectricalBase electrical, ElectricalProducts products)
    {
        _seApiConverter = serviceProvider.Get<SeApi.Converters>();
        Electrical = electrical;
        Products = products;
    }

    protected abstract void AddProduct();
    protected abstract void OpenProduct(string configuration);
    protected abstract void SaveChanges();

    public Command AddProductCommand => new(AddProduct);
    public Command OpenProductCommand => new(OpenProduct);
        
    private void OpenProduct()
    {
        if (Products.MainSwitch != null)
        {
            OpenProduct(Products.MainSwitch.SerializedConfiguration);
        }
        else
        {
            AddProductCommand?.Execute(null);
        }
    }

    protected async void ApplyProduct(SelectorConfig config, SelectorGroup? selectorGroup = null)
    {
        if (config != null)
        {
            var product = await _seApiConverter.ConvertToProduct(config);
            MainProduct = product;
            MainProduct.SelectorGroup = (int?)selectorGroup ?? -1;
            SaveChanges();
        }
        else
        {
            Debug.WriteLine($"<{GetType().Name}.{nameof(ApplyProduct)}> {nameof(config)} is NULL");
        }

        // DialogStackView.CloseLastView();
    }

    public Command<string> GoToLinkCommand => new(commandAction: url => Process.Start(new ProcessStartInfo(url)));

    public virtual void OnMainProductChanged()
    {
        MainProductType = MainProduct?.Product?.ShortName;
        AccessoriesNames = MainProduct?.AccessoriesNames;
        Accessories = MainProduct?.Accessories.Select(n => new ProductViewModel(n)).ToList();
    }

    protected virtual void OnDesignationChanged()
    {
            
    }
}