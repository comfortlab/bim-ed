﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CircuitProductsViewModel
{
    public string CircuitDesignation { get; set; }
    public ProductWithAccessoriesViewModel MainSwitch { get; set; }
    public List<ProductWithAccessoriesViewModel> Products { get; set; }

    public CircuitProductsViewModel(ElectricalSystemProxy circuit)
    {
        CircuitDesignation = circuit.CircuitDesignation;

        if (circuit.Products?.MainSwitch != null)
        {
            MainSwitch = new ProductWithAccessoriesViewModel(circuit.Products.MainSwitch);
        }

        Products = circuit.Products?.Products
            .Select(p => new ProductWithAccessoriesViewModel(p))
            .GetGrouped() ?? new List<ProductWithAccessoriesViewModel>();
    }

    public void SortProductsByReference()
    {
        Products = Products.GetSortedByReference();
    }

    public void SortProductsByDescription()
    {
        Products = Products.GetSortedByDescription();
    }

    public List<ProductWithAccessoriesViewModel> GetProductsWithAccessories()
    {
        var products = new List<ProductWithAccessoriesViewModel>();

        if (MainSwitch != null)
        {
            products.Add(MainSwitch.Clone());
        }

        products.AddRange(Products.Select(p => p.Clone()));

        return products;
    }

    public List<ProductViewModel> GetProducts()
    {
        var products = new List<ProductViewModel>();

        if (MainSwitch != null)
        {
            products.AddRange(MainSwitch.GetProducts());
        }

        Products.ForEach(p => products.AddRange(p.GetProducts()));

        return products;
    }
}