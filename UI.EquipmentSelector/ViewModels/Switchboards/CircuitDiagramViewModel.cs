﻿using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CircuitDiagramViewModel : ViewModel
{
    public CircuitDiagramViewModel(CircuitViewModel circuitViewModel)
    {
        CircuitViewModel = circuitViewModel;
    }

    public CircuitViewModel CircuitViewModel { get; set; }

    public double Width { get; set; }
}