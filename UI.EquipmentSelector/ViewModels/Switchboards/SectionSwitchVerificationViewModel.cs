﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SectionSwitchVerificationViewModel : ViewModel
{
    // private readonly Stash _stash;
        
    public List<SwitchStateViewModel> SwitchStateViewModels { get; set; } = new();
        
    public SectionSwitchVerificationViewModel(IServiceProvider serviceProvider, SwitchBoardUnit switchBoardUnit)
    {
        // _stash = Stash.Instance;

        var operatingMode = serviceProvider.Get<OperatingModes>().Current;
        var index = switchBoardUnit.SwitchBoard.Units.IndexOf(switchBoardUnit);
        var leftSwitchboardUnit = index > 0
            ? switchBoardUnit.SwitchBoard.Units.ElementAtOrDefault(index - 1)
            : null;

        var leftSwitchboardUnits = leftSwitchboardUnit?.GetLeftConnected(includeOwner: true, operatingMode)
            .Reverse() ?? new List<SwitchBoardUnit>();

        var rightSwitchboardUnits = switchBoardUnit.GetLeftConnected(includeOwner: true, operatingMode);
            
        var switchboardUnits = leftSwitchboardUnits.Concat(rightSwitchboardUnits);
            
        foreach (var unit in switchboardUnits)
        {
            if (unit.BaseConnector != null)
                SwitchStateViewModels.Add(new SwitchStateViewModel(this, unit.BaseConnector, operatingMode));
                
            if (unit.ReserveSourceConnector != null)
                SwitchStateViewModels.Add(new SwitchStateViewModel(this, unit.BaseConnector, operatingMode));
        }
    }

    internal void Verification()
    {
        var switched = SwitchStateViewModels.Where(n => n.State).ToArray();
        var isOneSelected = switched.Length == 1;
            
        // DialogStackView.SetOkEnabled(isOneSelected);
        //
        // if (isOneSelected)
        //     _stash.Push(switched[0].Connector);
        // else
        //     _stash.Drop<ConnectorProxy>();
    }
}

public class SwitchStateViewModel : ViewModel
{
    public SwitchStateViewModel(SectionSwitchVerificationViewModel verificationViewMode,
        ConnectorProxy connector, OperatingMode operatingMode)
    {
        VerificationViewModel = verificationViewMode;
        Connector = connector;
        Name = Connector.Source?.GetFirstSourceOf<ElectricalEquipmentProxy>()?.Name ??
               Connector.Owner?.Name;
        State = Connector.GetState(operatingMode);
    }
        
    public SectionSwitchVerificationViewModel VerificationViewModel { get; set; }
    public string Name { get; set; }
    public ConnectorProxy Connector { get; set; }
    public bool State { get; set; }

    private void OnStateChanged()
    {
        VerificationViewModel.Verification();
    }
}