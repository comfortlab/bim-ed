﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CircuitGroupViewModel : ViewModel
{
    public CircuitGroupViewModel(IServiceProvider serviceProvider, CircuitViewModel mainCircuitViewModel)
    {
        MainCircuitViewModel = mainCircuitViewModel;

        var groupCircuit = mainCircuitViewModel.ElectricalSystem;

        if (groupCircuit.IsGroupCircuit == false)
            throw new ArgumentException($"{nameof(mainCircuitViewModel)} must be '{nameof(ElectricalSystemProxy.IsGroupCircuit)}'.");

        MainCircuitViewModel.GroupViewModel = this;
        GroupCircuit = groupCircuit;
        ElectricalCircuitGroup = GroupCircuit.GetConsumerAt<ElectricalCircuitGroup>(0);
            
        if (ElectricalCircuitGroup == null)
            throw new InvalidCastException($"{nameof(GroupCircuit)} child is not a {nameof(ElectricalCircuitGroup)}");

        SectionViewModel = mainCircuitViewModel.SectionViewModel;
        SectionViewModel.CircuitGroups.Add(this);
        SwitchViewModel = new CircuitSwitchViewModel(serviceProvider, GroupCircuit);
        WiresViewModel = new DistributionWiresViewModel(ElectricalCircuitGroup.PowerParameters);

        ElectricalCircuits = ElectricalCircuitGroup.GetConsumersOf<ElectricalSystemProxy>()
            .Select(n => new CircuitViewModel(serviceProvider, SectionViewModel, n)).ToObservableCollection();

        mainCircuitViewModel.PropertyChanged += (sender, args) =>
        {
            if (args.PropertyName == nameof(mainCircuitViewModel.SelectedPhase))
            {
                WiresViewModel.SetPhaseMarks();
                ElectricalCircuits.ForEach(n => n.SelectedPhase = mainCircuitViewModel.SelectedPhase);
            }
        };
    }

    public ElectricalSystemProxy GroupCircuit { get; set; }
    public CircuitViewModel MainCircuitViewModel { get; set; }
    public ElectricalCircuitGroup ElectricalCircuitGroup { get; set; }
    public SwitchboardSectionViewModel SectionViewModel { get; set; }
    public SwitchViewModel SwitchViewModel { get; set; }
    public ObservableCollection<CircuitViewModel> ElectricalCircuits { get; set; }
    public DistributionWiresViewModel WiresViewModel { get; set; }
    public double SwitchHeight { get; set; }
    public double Width { get; set; }

    private void OnWidthChanged()
    {
        MainCircuitViewModel.Width = Width;
    }

    public void UpdateProducts()
    {
        SwitchViewModel.Update();
    }
}