﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Comparers;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;
using CoLa.BimEd.UI.Framework;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SwitchBoardDiagramViewModel : SwitchBoardViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly SeApi.Converters _seApiConverters;
    private OperatingMode _operatingMode;
    private bool _isSelectionUpdating;
    
    public SwitchBoard SwitchBoard { get; set; }
    public ObservableCollection<SwitchboardSectionViewModel> SectionViewModels { get; set; }
    public SwitchBoardUnitProducts SwitchBoardUnitProducts { get; set; }
    public DistributionWiresViewModel DistributionWiresViewModel { get; set; }
    public SwitchboardSectionViewModel SelectedSwitchboardSection { get; set; }
    public List<CircuitViewModel> SelectedCircuits { get; set; } = new();
    public ObservableCollection<EnclosureViewModel> EnclosureViewModels { get; set; }
    public ClipboardViewModel ClipboardViewModel { get; set; }
    public DeleteViewModel DeleteViewModel { get; set; }
    public SelectorProduct EditedEnclosure { get; set; }
    public bool HasEnclosures { get; set; }
    public bool HasMountingModeIds { get; set; }
    public string EnclosureName { get; set; }
    public bool EnclosuresPopupOpen { get; set; } = true;
    public bool IsThreePhases { get; set; }
    public bool IsAnyCircuitSelected { get; set; }
    public bool IsOneCircuitSelected { get; set; }
    public bool IsManyCircuitsSelected { get; set; }
    public bool CanAddToCircuitGroup { get; set; }
    public bool CanRemoveCircuitsFromGroup { get; set; }
    public bool CanCreateCircuitGroup { get; set; }

    public SwitchBoardDiagramViewModel(IServiceProvider serviceProvider, SwitchBoardUnit switchBoardUnit) :
        base(serviceProvider, switchBoardUnit)
    {
        _serviceProvider = serviceProvider;
        _operatingMode = serviceProvider.Get<OperatingModes>().Current;
        _seApiConverters = serviceProvider.Get<SeApi.Converters>();
        
        SwitchBoard = switchBoardUnit.SwitchBoard;

        Name = SwitchBoard.Name;
        SectionViewModels = SwitchBoard.Units.Select(n => new SwitchboardSectionViewModel(serviceProvider, this, n)).ToObservableCollection();
        SelectedSwitchboardSection = SectionViewModels.FirstOrDefault(n => Equals(n.Unit, switchBoardUnit));
        IsThreePhases = SwitchBoard.FirstUnit.PowerParameters.IsThreePhases;
        ClipboardViewModel = new ClipboardViewModel(serviceProvider, this);
        DeleteViewModel = new DeleteViewModel(serviceProvider, this);
            
        UpdateSectionSwitches();
            
        DistributionWiresViewModel = new DistributionWiresViewModel(SwitchBoard.FirstUnit.PowerParameters);
        EnclosureViewModels = new ObservableCollection<EnclosureViewModel>();

        SwitchBoardUnitProducts = SwitchBoard.SwitchBoardUnitProducts;
        SwitchBoardUnitProducts?.Enclosures?.ForEach(e =>
            EnclosureViewModels.Add(new EnclosureViewModel(e as SelectorProduct, this))
        );

        UpdateEnclosuresInfo();

        RegisterMessages();
    }
    
    public Command AddFirstEnclosureCommand => new(() =>
    {
        EditedEnclosure = null;

        // DialogStackView.PushView(
        //     viewContent: new SelectorsView(
        //         selectors: SeApi.Selectors.GetApplicability<SwitchboardUnit>(SelectorGroup.Enclosures),
        //         electricalItem: Switchboard.FirstUnit ?? SectionViewModels.FirstOrDefault()?.Unit as ElectricalBase,
        //         callback: ApplyEnclosure),
        //     buttons: DialogButtons.CancelClose);
    });

    public Command AddNextEnclosureCommand => new(() =>
    {
        EditedEnclosure = null;

        // DialogStackView.PushView(
        //     viewContent: new SelectorsView(
        //         selectors: SeApi.Selectors.GetSelectorsById(EnclosureViewModels.First().Product.SelectorId),
        //         electricalItem: Switchboard.FirstUnit ?? SectionViewModels.FirstOrDefault()?.Unit as ElectricalBase,
        //         callback: ApplyEnclosure),
        //     buttons: DialogButtons.CancelClose);
    });

    public Command CreateElectricalSystemCommand => new(() =>
    {

    });

    public Command CreateElectricalSystemGroupCommand => new(() =>
    {
        var currentPanelViewModel = SelectedCircuits.FirstOrDefault()?.SectionViewModel;
        var existThreePhasesCircuits = SelectedCircuits.Any(n => n.PowerParameters.IsThreePhases);

        if (existThreePhasesCircuits || SelectedCircuits.Count < 3)
        {
            // DialogStackView.PushMessage(
            //     message: CoLa.BimEd.UI.Resources.Messages.Question_CombineSelectedCircuitsIntoGroup,
            //     buttons: DialogButtons.YesNo,
            //     okCallback: () =>
            //     {
            //         var factory = OldElectricalFactory.Instance;
            //         using var unitOfWork = UnitOfWork.NewInstance();
            //         var groupCircuit = factory.CreateElectricalCircuitGroup(SelectedCircuits.Select(n => n.ElectricalSystem).ToList());
            //
            //         ElectricalCalculator.Loads.Calculate(groupCircuit);
            //         ElectricalCalculator.Parameters.CalculateResistance(groupCircuit);
            //
            //         unitOfWork.SaveChanges("Create Electrical Group");
            //
            //         currentPanelViewModel?.SetElectricalCircuits();
            //
            //         SelectedCircuits = new List<CircuitViewModel>();
            //
            //         return true;
            //     });
        }
        else
        {
            // var items = new List<MessageItem>
            // {
            //     new(PhasesNumber.One, ElectricalParameters.PhasesNumber1),
            //     new(PhasesNumber.Three, ElectricalParameters.PhasesNumber3),
            // };
            //
            // var selectedItem = items.FirstOrDefault();
            //
            // DialogStackView.PushSelectItemView(
            //     message: CoLa.BimEd.UI.Resources.Messages.Question_CombineSelectedCircuitsIntoGroup,
            //     items: items,
            //     selectedItem: selectedItem,
            //     buttons: DialogButtons.YesNo,
            //     okCallback: newItem =>
            //     {
            //         var factory = OldElectricalFactory.Instance;
            //         using var unitOfWork = UnitOfWork.NewInstance();
            //         var phasesNumber = newItem?.Item is PhasesNumber p ? p : PhasesNumber.Undefined;
            //         var groupCircuit = factory.CreateElectricalCircuitGroup(SelectedCircuits.Select(n => n.ElectricalSystem).ToList(), phasesNumber);
            //
            //         ElectricalCalculator.Loads.Calculate(groupCircuit);
            //         ElectricalCalculator.Parameters.CalculateResistance(groupCircuit);
            //
            //         unitOfWork.SaveChanges("Create Electrical Group");
            //
            //         currentPanelViewModel?.SetElectricalCircuits();
            //
            //         SelectedCircuits = new List<CircuitViewModel>();
            //
            //         return true;
            //     });
        }
    });

    public Command AddToElectricalSystemGroupCommand => new(() =>
    {
        var selectedGroup = SelectedCircuits.FirstOrDefault(n => n.ElectricalSystem.IsGroupCircuit);
        var otherCircuits = SelectedCircuits.Where(n => n.ElectricalSystem.IsGroupCircuit == false).Select(n => n.ElectricalSystem).ToList();

        if (selectedGroup == null)
            return;

        // using var unitOfWork = UnitOfWork.NewInstance();

        selectedGroup.GroupViewModel.ElectricalCircuitGroup.AddRange(otherCircuits);
        // ElectricalCalculator.Loads.Calculate(selectedGroup.ElectricalSystem);
        // ElectricalCalculator.Parameters.CalculateResistance(selectedGroup.ElectricalSystem);

        // unitOfWork.SaveChanges("Add to Electrical Group");

        var currentPanelViewModel = SelectedCircuits.FirstOrDefault()?.SectionViewModel;
        currentPanelViewModel?.SetElectricalCircuits();

        SelectedCircuits = new List<CircuitViewModel>();
    });

    public Command RemoveFromElectricalSystemGroupCommand => new(() =>
    {
        var currentSectionViewModel = SelectedCircuits.FirstOrDefault()?.SectionViewModel;
        var section = currentSectionViewModel?.Unit;
        var groups = SelectedCircuits
            .Select(n => n.ElectricalSystem.Source as ElectricalCircuitGroup)
            .Where(n => n != null)
            .Distinct(new RevitProxyComparer<ElectricalCircuitGroup>())
            .ToList();
        var circuits = SelectedCircuits
            .Where(n => n.ElectricalSystem.IsGroupCircuit == false)
            .Select(n => n.ElectricalSystem)
            .ToList();

        if (section == null)
            return;

        // using var unitOfWork = UnitOfWork.NewInstance();

        foreach (var group in groups)
        {
            foreach (var otherCircuit in circuits)
            {
                if (!group.ConsumerConnectors.Contains(otherCircuit.BaseConnector))
                    continue;

                group.Remove(otherCircuit);
                otherCircuit.ConnectTo(section);

                var groupCircuit = group.Source as ElectricalSystemProxy;

                if (group.ConsumerConnectors.IsEmpty())
                    groupCircuit.Disconnect();

                // ElectricalCalculator.Loads.Calculate(groupCircuit);
                // ElectricalCalculator.Parameters.CalculateResistance(groupCircuit);
            }
        }

        // ElectricalCalculator.Loads.Calculate(section);

        // unitOfWork.SaveChanges("Remove from Electrical Group");

        currentSectionViewModel.SetElectricalCircuits();

        SelectedCircuits = new List<CircuitViewModel>();
    });

    public Command CreateSectionOrInputCommand => new(() =>
    {
        // DialogStackView.PushView(
        //     viewContent: new CreateSectionView()
        //     {
        //         DataContext = new CreateSectionViewModel(this)
        //     },
        //     okCallback: () =>
        //     {
        //         CollectionExtensions.ForEach(SectionViewModels, n => n.IsSinglePanel = false);
        //         return true;
        //     },
        //     buttons: DialogButtons.Cancel, alignment: DialogAlignment.Center);
    });

    public Command DeleteElectricalSystemCommand => new(() =>
    {
        // DialogStackView.PushMessage(
        //     message: CoLa.BimEd.UI.Resources.Messages.Question_DeleteSelectedCircuits,
        //     buttons: DialogButtons.DeleteCancel,
        //     okCallback: () =>
        //     {
        //         using var unitOfWork = UnitOfWork.NewInstance();
        //         var electricalCircuitRepository = new ElectricalCircuitRepository(unitOfWork);
        //         while (SelectedCircuits.Any())
        //         {
        //             var circuitViewModel = SelectedCircuits.First();
        //             var panelViewMode = circuitViewModel.SectionViewModel;
        //             panelViewMode.CircuitViewModels.Remove(circuitViewModel);
        //             SelectedCircuits.Remove(circuitViewModel);
        //             electricalCircuitRepository.DeleteElectricalCircuit(circuitViewModel.ElectricalSystem, saveChanges: false);
        //         }
        //
        //         unitOfWork.SaveChanges(CommandNames.Delete);
        //
        //         MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraph>();
        //
        //         return true;
        //     });
    });

    public Command RebalanceLoadCommand => new(() =>
    {
        SectionViewModels.ForEach(n =>
        {
            // ElectricalCalculator.Loads.Rebalance(n.Unit, RebalanceLoadMode.ByLoad, 0);
            n.UpdateProperties();
            n.UpdateCircuitPhaseMarks();
        });

        UpdateElectricalChain();
    });

    public Command OrderPhasesCommand => new(() =>
    {
        SectionViewModels.ForEach(n =>
        {
            // ElectricalCalculator.Loads.Rebalance(n.Unit, RebalanceLoadMode.ByOrder, 0);
            n.CircuitViewModels.ForEach(vm => vm.UpdateProperties());
            n.UpdateCircuitPhaseMarks();
        });

        UpdateElectricalChain();
    });

    public Command ProductCartCommand => new(() =>
    {
        var productsListPanelView = SwitchBoard != null
            ? new ProductsListPanelView(SwitchBoard)
            : new ProductsListPanelView(SectionViewModels.First().Unit);

        // DialogStackView.PushView(
        //     viewContent: productsListPanelView,
        //     buttons: DialogButtons.CancelClose,
        //     alignment: DialogAlignment.Center);
    });

    public Command SelectCircuitsEquipmentCommand => new(SelectCircuitsEquipment);

    private async void SelectCircuitsEquipment()
    {
        // ModalDialogStackView.LockLastView();

        // using var unitOfWork = UnitOfWork.NewInstance();
        // var circuitRepository = new ElectricalCircuitRepository(unitOfWork);
        // var equipmentSelector = BusinessLogic.Services.Electrical.Selectors.EquipmentSelector.Instance;
        // var selectedCircuits = SelectedCircuits.Select(n => n.ElectricalSystem).ToList();
        // await equipmentSelector.SelectEquipment(selectedCircuits);
        // SelectedCircuits.ForEach(viewModel => viewModel.UpdateProducts());
        // selectedCircuits.ForEach(circuitRepository.Update);
        // unitOfWork.SaveChanges("Select Equipment");
    }

    public Command SelectPanelEquipmentCommand => new(SelectPanelEquipment);

    private async void SelectPanelEquipment()
    {
        // ModalDialogStackView.LockLastView();

        // using var unitOfWork = UnitOfWork.NewInstance();
        // var panelRepository = new SwitchboardRepository(unitOfWork);
        // var circuitRepository = new ElectricalCircuitRepository(unitOfWork);
        // var equipmentSelector = BusinessLogic.Services.Electrical.Selectors.EquipmentSelector.Instance;

        foreach (var panelViewModel in SectionViewModels)
        {
            // await equipmentSelector.SelectEquipment(panelViewModel.Unit);
            panelViewModel.CircuitViewModels.ForEach(viewModel => viewModel.UpdateProducts());
            panelViewModel.UpdateProducts();

            // panelRepository.Update(panelViewModel.Unit.Switchboard);

            foreach (var electricalCircuit in panelViewModel.CircuitViewModels.Select(n => n.ElectricalSystem))
            {
                // circuitRepository.Update(electricalCircuit);
            }
        }

        // unitOfWork.SaveChanges(TransactionNames.SelectElectricalEquipment);
    }

    public Command SelectorSettingCommand => new(SelectorSetting);

    private static async void SelectorSetting()
    {
        // using var unitOfWork = UnitOfWork.NewInstance();
        // var loadClassificationRepository = new LoadClassificationRepository(unitOfWork);
        // var loadClassifications = loadClassificationRepository.GetAll().ToList();

        // await SeApi.Selectors.VerifyProducts(loadClassifications);

        // DialogStackView.PushView(
        //     viewContent: new SelectorSettingView(loadClassifications),
        //     okCallback: () =>
        //     {
        //         using var callbackUnitOfWork = UnitOfWork.NewInstance();
        //         var callbackLoadClassificationRepository = new LoadClassificationRepository(callbackUnitOfWork);
        //         var editedLoadClassifications = Stash.Instance.Pop<List<LoadClassificationProxy>>();
        //
        //         foreach (var editedLoadClassification in editedLoadClassifications ?? new List<LoadClassificationProxy>())
        //         {
        //             var loadClassification = loadClassifications.FirstOrDefault(n => n.RevitId == editedLoadClassification.RevitId);
        //
        //             loadClassification?.CircuitSelectAndConfigSetting.PullProperties(editedLoadClassification.CircuitSelectAndConfigSetting);
        //             loadClassification?.PanelSelectAndConfigSetting.PullProperties(editedLoadClassification.PanelSelectAndConfigSetting);
        //                 
        //             callbackLoadClassificationRepository.Update(loadClassification);
        //         }
        //
        //         callbackUnitOfWork.SaveChanges("Save Load Classification");
        //
        //         return true;
        //     }, buttons: DialogButtons.OkCancel);
    }

    private void OnNameChanged()
    {
        if (SwitchBoard != null)
            SwitchBoard.Name = Name;
    }

    private void OnSelectedCircuitsChanged()
    {
        UpdateSelections();
    }

    private void OnSelectedPanelChanged()
    {
        UpdateSelections();
    }

    internal void UpdateSelections()
    {
        IsOneCircuitSelected = SelectedCircuits.Count == 1;
        IsManyCircuitsSelected = SelectedCircuits.Count > 1;
        IsAnyCircuitSelected = IsOneCircuitSelected || IsManyCircuitsSelected;
        CanCreateCircuitGroup = IsManyCircuitsSelected && SelectedCircuits.All(n => n.ElectricalSystem.Source is not ElectricalCircuitGroup && !n.ElectricalSystem.IsGroupCircuit);
        CanAddToCircuitGroup = IsManyCircuitsSelected && SelectedCircuits.Count(n => n.ElectricalSystem.IsGroupCircuit) == 1;
        CanRemoveCircuitsFromGroup = IsAnyCircuitSelected && SelectedCircuits.All(n => n.ElectricalSystem.Source is ElectricalCircuitGroup);

        ClipboardViewModel?.Update();

        DeleteViewModel?.Update(SelectedSwitchboardSection?.Unit != null
            ? new[] { SelectedSwitchboardSection.Unit }
            : SelectedCircuits?.Select(n => n.ElectricalSystem).ToList());
    }

    public async void ApplyEnclosure(SelectorConfig config)
    {
        try
        {
            var product = await _seApiConverters.ConvertToProduct(config);

            if (product == null)
            {
                return;
            }

            if (EditedEnclosure == null)
            {
                SwitchBoardUnitProducts.Enclosures.Add(product);
                EnclosureViewModels.Add(new EnclosureViewModel(product, this));
            }
            else
            {
                var index = SwitchBoardUnitProducts.Enclosures.IndexOf(EditedEnclosure);
                SwitchBoardUnitProducts.Enclosures[index] = product;
                EnclosureViewModels[index] = new EnclosureViewModel(product, this);
            }

            UpdateEnclosuresInfo();
        }
        finally
        {
            EditedEnclosure = null;
            // DialogStackView.CloseLastView();
        }
    }

    internal void UpdateProperties(OperatingMode operatingMode = null)
    {
        if (operatingMode != null)
            _operatingMode = operatingMode;
            
        SectionViewModels.ForEach(n => n.UpdateProperties(_operatingMode));
    }
        
    internal void UpdateElectricalChain()
    {
        // MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraphChain>();
    }

    public void UpdateEnclosuresInfo()
    {
        HasEnclosures = EnclosureViewModels.Any();

        HasMountingModeIds = EnclosureViewModels.Any(e => !string.IsNullOrWhiteSpace(e.Product.MountingModeId));

        EnclosureName = EnclosureViewModels
            .FirstOrDefault()
            ?.Product
            ?.Product
            .RangeOfProduct;
    }

    internal void UpdateProducts(IReadOnlyCollection<SwitchboardSectionViewModel> panelViewModels)
    {
        foreach (var panelViewModel in panelViewModels)
        {
            panelViewModel.UpdateProducts();
        }
    }

    internal void UpdateProducts(IReadOnlyCollection<CircuitViewModel> circuitViewModels)
    {
        foreach (var circuitViewModel in circuitViewModels)
        {
            circuitViewModel.UpdateProducts();
            circuitViewModel.GroupViewModel?.UpdateProducts();
        }
    }

    internal void UpdateProducts(IReadOnlyCollection<ElectricalBase> electricals)
    {
        foreach (var panelViewModel in SectionViewModels.Where(n => electricals.Contains(n.Unit)))
        {
            panelViewModel.UpdateProducts();
        }

        foreach (var circuitViewModel in SectionViewModels.SelectMany(n => n.CircuitViewModels).Where(n => electricals.Contains(n.ElectricalSystem)))
        {
            circuitViewModel.UpdateProducts();
            circuitViewModel.GroupViewModel?.UpdateProducts();
        }
    }

    internal void UpdateSwitch(ElectricalSystemProxy electricalCircuit)
    {
        var circuitViewModel = SectionViewModels.SelectMany(n => n.CircuitViewModels)
            .FirstOrDefault(n => Equals(n.ElectricalSystem, electricalCircuit));

        circuitViewModel?.SwitchViewModel.Update();
    }

    internal void UpdateSectionSwitches()
    {
        for (var i = 0; i < SectionViewModels.Count; i++)
        {
            var unitViewModel = SectionViewModels.ElementAt(i);
            var products = SwitchBoard?.SectionSwitches.ElementAtOrDefault(i);
                
            if (products != null)
            {
                unitViewModel.SectionSwitchViewModel = new SectionSwitchViewModel(_serviceProvider, this, products);
            }
        }
    }

    public void ResetSelections(SwitchboardSectionViewModel except = null)
    {
        if (_isSelectionUpdating)
            return;

        _isSelectionUpdating = true;

        foreach (var diagramViewModel in SectionViewModels.Where(n => n != except))
        {
            diagramViewModel.IsPanelSelected = false;
            diagramViewModel.CircuitViewModels.ForEach(n => n.IsSelected = false);
        }

        _isSelectionUpdating = false;
    }

    public void ResetCircuitSelections()
    {
        SectionViewModels.ForEach(n => n.CircuitViewModels.ForEach(c => c.IsSelected = false));
    }

    public void RegisterMessages()
    {
        // MessageMediator.Unregister<ViewMessage.Update.SwitchboardDiagram>();
        //     
        // MessageMediator.Register<ViewMessage.Update.SwitchboardDiagram>(GetType(), message =>
        // {
        //     _operatingMode = DI.Get<ICurrentOperatingMode>().Value;
        //     UpdateProperties();
        // });
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (disposing == false)
            return;

        // MessageMediator.Unregister<ViewMessage.Update.SwitchboardDiagram>();
    }

    ~SwitchBoardDiagramViewModel() => Dispose(false);
}