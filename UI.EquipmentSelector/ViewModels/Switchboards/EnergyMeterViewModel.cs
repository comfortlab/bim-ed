using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class EnergyMeterViewModel : SelectorProductViewModel
{
    private readonly SwitchBoardUnit _unit;
        
    public SwitchBoardUnitProducts SectionProducts { get; set; }
        
    public EnergyMeterViewModel(IServiceProvider serviceProvider, SwitchboardSectionViewModel switchboardSectionViewModel) :
        base(serviceProvider, switchboardSectionViewModel.Unit, switchboardSectionViewModel.Unit.Products)
    {
        _unit = switchboardSectionViewModel.Unit;
        SectionProducts = _unit.SwitchBoardUnitProducts;
        MainProduct = SectionProducts.EnergyMeter as SelectorProduct;
        Designation = SectionProducts.EnergyMeter?.Designation;
    }
        
    protected override void AddProduct()
    {
        // DialogStackView.PushView(
        //     viewContent: new SelectorsView(
        //         selectors: SeApi.Selectors.GetApplicability<SwitchboardUnit>(SelectorGroup.EnergyMetering),
        //         electricalItem: Electrical,
        //         callback: config => ApplyProduct(config)),
        //     buttons: DialogButtons.CancelClose);
    }

    protected override void OpenProduct(string configuration)
    {
        // ModalDialogStackView.PushView(
        //     viewContent: new SelectorsView(
        //         selectors: SeApi.Selectors.GetApplicability<ElectricalCircuit>(SelectorGroup.CircuitBreakers),
        //         electricalItem: Electrical,
        //         callback: config => ApplyProduct(config),
        //         configuration: configuration),
        //     buttons: DialogButtons.CancelClose);
    }

    protected override void SaveChanges()
    {
        // using var unitOfWork = UnitOfWork.NewInstance();
        // var repository = new SwitchboardUnitRepository(unitOfWork);
        // repository.Update(_unit);
        // unitOfWork.SaveChanges(TransactionNames.SaveChanges);
    }

    public override void OnMainProductChanged()
    {
        SectionProducts.EnergyMeter = MainProduct;
        base.OnMainProductChanged();
    }

    protected override void OnDesignationChanged()
    {
        SectionProducts.EnergyMeter.Designation = Designation;
    }
}