using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CurrentsViewModel : ElectricalParametersViewModel
{
    public bool IsThreePhases { get; set; }
    public double EstimateCurrent1 { get; set; }
    public double EstimateCurrent2 { get; set; }
    public double EstimateCurrent3 { get; set; }
    public double Asymmetry { get; set; }
        
    public CurrentsViewModel(ElectricalBase electrical, OperatingMode operatingMode) :
        base(electrical, operatingMode) { }
        
    protected override void UpdateProperties()
    {
        IsThreePhases = EstimatedPowerParameters.ThreePhases != null;

        if (Electrical.PowerParameters.IsOnePhase)
        {
            EstimateCurrent1 = EstimatedPowerParameters.Current;
        }
        else if (EstimatedPowerParameters.ThreePhases != null)
        {
            EstimateCurrent1 = EstimatedPowerParameters.ThreePhases.EstimateCurrent1;
            EstimateCurrent2 = EstimatedPowerParameters.ThreePhases.EstimateCurrent2;
            EstimateCurrent3 = EstimatedPowerParameters.ThreePhases.EstimateCurrent3;
            Asymmetry = EstimatedPowerParameters.ThreePhases.Asymmetry;
        }
    }
}