﻿using System;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CircuitSwitchViewModel : SwitchViewModel
{
    private readonly ElectricalSystemProxy _electricalCircuit;
        
    public CircuitSwitchViewModel(IServiceProvider serviceProvider, ElectricalSystemProxy electricalCircuit) :
        base(serviceProvider, electricalCircuit, electricalCircuit.Products)
    {
        _electricalCircuit = electricalCircuit;
    }

    protected override void AddProduct()
    {
        // MessageMediator.SendMessage(
        //     new ViewMessage.ProductSelector.AddCircuitSwitch(
        //         _electricalCircuit,
        //         callback: config => ApplyProduct(config)));
    }

    protected override void OpenProduct(string configuration)
    {
        // MessageMediator.SendMessage(
        //     new ViewMessage.ProductSelector.OpenCircuitSwitch(
        //         _electricalCircuit,
        //         callback: config => ApplyProduct(config),
        //         configuration: configuration));
    }

    protected override void SaveChanges()
    {
        // using var unitOfWork = UnitOfWork.NewInstance();
        // var repository = new ElectricalCircuitRepository(unitOfWork);
        // repository.Update(_electricalCircuit);
        // unitOfWork.SaveChanges("Apply product");
    }

    public override void OnMainProductChanged()
    {
        base.OnMainProductChanged();

        if (Designation != null ||
            Electrical is not ElectricalSystemProxy electricalCircuit)
            return;
            
        var circuitIndex = electricalCircuit.ParentAs<ElectricalSystemProxy>()
            ?.Consumers.ToList()
            .IndexOf(electricalCircuit);

        if (circuitIndex >= 0)
            Designation = $"QF{circuitIndex + 1}";
    }
}