using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Factories;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CreateSectionViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    public string GroupName { get; set; }
    public SwitchBoardDiagramViewModel SwitchBoardDiagramViewModel { get; set; }
    public List<SectionViewModel> Sections { get; set; }

    public CreateSectionViewModel(IServiceProvider serviceProvider, SwitchBoardDiagramViewModel switchBoardDiagramViewModel)
    {
        _serviceProvider = serviceProvider;
        SwitchBoardDiagramViewModel = switchBoardDiagramViewModel;
        GroupName = SwitchBoardDiagramViewModel.Name;
        Sections = SwitchBoardDiagramViewModel.SectionViewModels.Select(n => new SectionViewModel(_serviceProvider, n)).ToList();
    }

    public Command CreateLeftSectionCommand => new(() =>
    {
        var section = SwitchBoardDiagramViewModel.SectionViewModels.First().Unit;
        var switchboard =
            SwitchBoardDiagramViewModel.SwitchBoard ??
            ElectricalFactory.Switchboards.Create(section.Name, section);
        var index = switchboard.Units.IndexOf(section);
        var newSection = ElectricalFactory.Switchboards.CreateUnit("New", section.DistributionSystem);

        switchboard.InsertUnit(index, newSection);

        var diagramViewModel = new SwitchboardSectionViewModel(_serviceProvider, SwitchBoardDiagramViewModel, newSection);

        SwitchBoardDiagramViewModel.SectionViewModels.Insert(0, diagramViewModel);
        SwitchBoardDiagramViewModel.UpdateSectionSwitches();

        // MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraph>();

        // DialogStackView.OkLastView();
    });
}

public class SectionViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    public string SectionName { get; set; }
    public SwitchboardSectionViewModel SwitchboardSectionViewModel { get; set; }
    public List<PanelInput> PanelInputs { get; set; }

    public SectionViewModel(IServiceProvider serviceProvider, SwitchboardSectionViewModel switchboardSectionViewModel)
    {
        _serviceProvider = serviceProvider;
        SwitchboardSectionViewModel = switchboardSectionViewModel;
        SectionName = SwitchboardSectionViewModel.Unit.Name;
        // TODO # 2021.10.18
        // PanelInputs = PanelSectionViewModel.ElectricalPanel.PanelInputs;
    }

    public Command CreateSectionCommand => new(() =>
    {
        var section = SwitchboardSectionViewModel.Unit;
        var switchboard =
            SwitchboardSectionViewModel.Unit.SwitchBoard ??
            ElectricalFactory.Switchboards.Create(section.Name, section);
        var index = switchboard.Units.IndexOf(SwitchboardSectionViewModel.Unit);
        var newSection = ElectricalFactory.Switchboards.CreateUnit("New", section.DistributionSystem);
        var switchboardViewModel = SwitchboardSectionViewModel.SwitchBoardDiagramViewModel;

        switchboard.InsertUnit(index + 1, newSection);

        var diagramViewModel = new SwitchboardSectionViewModel(_serviceProvider, switchboardViewModel, newSection);

        switchboardViewModel.SectionViewModels.Insert(index + 1, diagramViewModel);
        switchboardViewModel.UpdateSectionSwitches();

        // MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraph>();

        // DialogStackView.OkLastView();
    });

    public Command CreateInputCommand => new(() => { /*DialogStackView.OkLastView();*/ });
}