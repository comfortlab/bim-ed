﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class ElementsViewModel : ViewModel
{
    public IEnumerable<ElementViewModel> Switchboards { get; }
    public List<LevelViewModel> Levels { get; set; }
    public bool SeparateLine { get; set; }

    public ElementsViewModel(ElectricalSystemProxy electricalCircuit)
    {
        var circuitChildren = electricalCircuit.Consumers.ToList();
        var elements = circuitChildren.Where(n => n is not SwitchBoardUnit).ToList();

        Switchboards = circuitChildren.Where(n => n is SwitchBoardUnit).Select(n => new ElementViewModel(n));

        // TODO # 2021.10.19
        // var elementRooms = elements.Select(n => n.Room).Where(n => n != null)
        //     .Distinct(new RevitProxyComparer<RoomProxy>())
        //     .Select(room => new RoomViewModel(
        //         room,
        //         circuitChildren.Where(e => Equals(e.Room, room))
        //             .OrderBy(n => n.Category)
        //             .ThenBy(e => e.Name)
        //             .Select(n => new ElementViewModel(n)).ToList()))
        //     .ToList();
        //
        // Levels = circuitChildren.Select(n => n.Level)
        //     .Distinct(new RevitProxyComparer<LevelProxy>())
        //     .Where(n => n != null)
        //     .Select(level => new LevelViewModel(
        //         level,
        //         elementRooms.Where(r => Equals(r.Room?.Level, level)).OrderBy(n => n.Room?.Number).ToList()))
        //     .OrderBy(n => n.Level?.Elevation).ToList();
        //
        // var elementsOutRooms = elements.Where(n => n.Room == null).ToList();
        //
        // Levels.ForEach(levelViewModel =>
        // {
        //     var elementViewModels = elementsOutRooms
        //         .Where(element => Equals(element.Level, levelViewModel.Level))
        //         .Select(element => new ElementViewModel(element)).ToList();
        //
        //     if (elementViewModels.IsEmpty())
        //         return;
        //
        //     levelViewModel.Rooms.Add(new RoomViewModel(null, elementViewModels));
        // });

        SeparateLine = Switchboards.Any() && (Levels?.Any() ?? false);
    }
}

public class LevelViewModel : ViewModel
{
    public LevelViewModel(LevelProxy level, List<RoomViewModel> rooms = null)
    {
        Level = level;
        Rooms = rooms ?? new List<RoomViewModel>();
    }

    public LevelProxy Level { get; set; }
    public List<RoomViewModel> Rooms { get; set; }
}

public class RoomViewModel : ViewModel
{
    public RoomViewModel(RoomProxy room, List<ElementViewModel> electricalElements = null)
    {
        Room = room;
        // Name = Room?.Name ?? Building.OutsideRooms;
        ElectricalElements = electricalElements ?? new List<ElementViewModel>();
    }

    public RoomProxy Room { get; set; }
    public string Name { get; set; }
    public List<ElementViewModel> ElectricalElements { get; set; }
}

public sealed class ElementViewModel : ViewModel
{
    public string Name { get; set; }
    public ElectricalBase ElectricalElement { get; set; }
    public double ApparentLoad { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double EstimateCurrent { get; set; }
    public double PowerFactor { get; set; }

    public ElementViewModel(ElectricalBase electricalElement)
    {
        ElectricalElement = electricalElement;
        Name = ElectricalElement.Name;

        if (!ElectricalElement.IsPower)
            return;


        ApparentLoad = ElectricalElement.PowerParameters.OwnApparentLoad;
        EstimateTrueLoad = ElectricalElement.EstimatedPowerParameters.EstimateTrueLoad;
        EstimateCurrent = ElectricalElement.EstimatedPowerParameters.Current;
        PowerFactor = ElectricalElement.PowerParameters.OwnPowerFactor;
    }

    public Command GoToElectricalPanelCommand => new(() =>
        {
            // if (ElectricalElement is SwitchboardUnit switchboardUnit)
            //     MessageMediator.SendMessage(
            //         new ViewMessage.DialogStack.PushSwitchboardView(switchboardUnit?.Switchboard));
        },
        canExecute: () => ElectricalElement is SwitchBoardUnit);
}