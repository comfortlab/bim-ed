﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.BusinessLogic.Model.Utils;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class CircuitViewModel : ViewModel
{
    private readonly Repository<ElectricalSystemProxy> _electricalSystemRepository;
    private readonly bool _isInitialized;
    private bool _selectedPhaseChanging;
    private bool _demandFactorChanging;
    private bool _propertiesUpdating;
    private double _previousReducingFactor;

    #region Properties

    private OperatingMode _operatingMode;
    private ChangedElements ChangedElements => _electricalSystemRepository.ChangedElements;
    public SwitchboardSectionViewModel SectionViewModel { get; set; }
    public CircuitGroupViewModel GroupViewModel { get; set; }
    public ElectricalSystemProxy ElectricalSystem { get; set; }
    public ElementsViewModel ElementsViewModel { get; set; }
    public CircuitSwitchViewModel SwitchViewModel { get; set; }
    public PowerParameters PowerParameters { get; }
    public CircuitPowerParameters CircuitPowerParameters { get; }
    public EstimatedPowerParameters EstimatedPowerParameters { get; set; }
    public Cable Cable { get; set; }
    public double EstimateCurrent { get; set; }
    public double EstimatePermissibleCableCurrent { get; set; }
    public double CablePermissibleCurrent { get; set; }
    public double ShortCurrent1 { get; set; }
    public double ShortCurrent3 { get; set; }
    public double SurgeShortCurrent3 { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double TrueLoad { get; set; }
    public double VoltageDrop { get; set; }
    public double VoltageDropPercent { get; set; }
    public List<ProductViewModel> Accessories { get; set; }
    public string CircuitBreakerDesignation { get; set; }
    public string CircuitBreakerType { get; set; }
    public string CableDesignation { get; set; }
    public double CableLength { get; set; }
    public double CableLengthInCableTray { get; set; }
    public double CableLengthMax { get; set; }
    public string CableMounting { get; set; }
    public string CableConductors { get; set; }
    public int CablesCount { get; set; }
    public int EstimateCablesCount { get; set; }
    public bool CablesCountAboveOne { get; set; }
    public bool IsSelected { get; set; }
    public bool CanSwitchPhases { get; set; }
    public List<Phase> Phases { get; set; }
    public Phase SelectedPhase { get; set; }
    public bool L1 { get; set; }
    public bool L2 { get; set; }
    public bool L3 { get; set; }
    public bool LockCableLength { get; set; }
    public double AdditionalDemandFactor { get; set; }
    public double DemandFactor { get; set; }
    public double TotalDemandFactor { get; set; }
    public double ReducingFactor { get; set; }
    public double Width { get; set; }
    public bool ShowVoltageDrop { get; set; }
    public bool ShowShortCurrents { get; set; }
    public bool ShowShortCurrent1 { get; set; }
    public bool ShowShortCurrent3 { get; set; }

    #endregion

    public CircuitViewModel(IServiceProvider serviceProvider, SwitchboardSectionViewModel sectionViewModel, ElectricalSystemProxy electricalSystem)
    {
        _electricalSystemRepository = serviceProvider.Get<Repository<ElectricalSystemProxy>>();
        _operatingMode = serviceProvider.Get<OperatingModes>().Current;
        SectionViewModel = sectionViewModel;
        ElectricalSystem = electricalSystem;
        ElementsViewModel = new ElementsViewModel(ElectricalSystem);

        PowerParameters = ElectricalSystem.PowerParameters;
        CircuitPowerParameters = ElectricalSystem.CircuitPowerParameters;
        EstimatedPowerParameters = ElectricalSystem.BaseConnector.GetEstimatedPowerParameters(_operatingMode);

        if (EstimatedPowerParameters == null)
            throw new InvalidOperationException($"{nameof(ElectricalSystem)} must be Power.");
            
        SwitchViewModel = new CircuitSwitchViewModel(serviceProvider, ElectricalSystem);

        AdditionalDemandFactor = EstimatedPowerParameters.AdditionalDemandFactor;
        DemandFactor = EstimatedPowerParameters.DemandFactor;
        TotalDemandFactor = EstimatedPowerParameters.TotalDemandFactor;
        Cable = ElectricalSystem.Products.Cable;
        CableDesignation = ElectricalSystem.CableDesignation;
        CablesCount = ElectricalSystem.Products.CablesCount;
        EstimateCablesCount = ElectricalSystem.Products.EstimateCablesCount;
        CableLength = ElectricalSystem.CableLength;
        CableLengthInCableTray = ElectricalSystem.CableLengthInCableTray;
        CableLengthMax = ElectricalSystem.CableLengthMax;
        CableMounting = ElectricalSystem.CableMounting;
        CircuitBreakerDesignation = ElectricalSystem.Products.MainSwitch?.Designation;
        LockCableLength = ElectricalSystem.LockCableLength;

        SetPhases();

        _isInitialized = true;

        UpdateProperties();
        UpdateCircuitParameters();
    }

    private void OnCableChanged()
    {
        if (!_isInitialized)
            return;

        if (!Equals(ElectricalSystem.Products.Cable, Cable))
        {
            ElectricalSystem.Products.SetCable(Cable);
            ChangedElements.Modify(ElectricalSystem.RevitId);
        }

        CableConductors = Cable != null ? string.Join("+", Cable.Conductors) : null;
        UpdateCablePermissibleCurrent();
        ReducingFactor = ElectricalSystem.CircuitPowerParameters.ReducingFactor;

        // ElectricalCalculator.Parameters.CalculateResistance(ElectricalSystem);
        UpdateCircuitParameters();
    }

    private void OnCableLengthChanged()
    {
        if (!_isInitialized)
            return;

        ElectricalSystem.CableLength = CableLength;
        // ElectricalCalculator.Parameters.CalculateResistance(ElectricalSystem);
        ChangedElements.Modify(ElectricalSystem.RevitId);
        UpdateCircuitParameters();
    }

    private void OnCableLengthInCableTrayChanged()
    {
        if (!_isInitialized)
            return;

        ElectricalSystem.CableLengthInCableTray = CableLengthInCableTray;
        ChangedElements.Modify(ElectricalSystem.RevitId);
    }

    private void OnCableLengthMaxChanged()
    {
        if (!_isInitialized)
            return;

        ElectricalSystem.CableLengthMax = CableLengthMax;

        // ElectricalCalculator.Parameters.CalculateResistance(ElectricalSystem);
        ChangedElements.Modify(ElectricalSystem.RevitId);
        UpdateCircuitParameters();
    }

    private void OnCircuitBreakerDesignationChanged()
    {
        if (!_isInitialized)
            return;
            
        ElectricalSystem.Products.MainSwitch.Designation = CircuitBreakerDesignation;
        ChangedElements.Modify(ElectricalSystem.RevitId);
    }

    private void OnEstimateCablesCountChanged()
    {
        if (!_isInitialized)
            return;
            
        CablesCountAboveOne = EstimateCablesCount > 1;
        ChangedElements.Modify(ElectricalSystem.RevitId);
    }

    private void OnReducingFactorChanged()
    {
        if (!_isInitialized)
            return;
            
        ReducingFactor = ReducingFactor switch
        {
            < 1e-6 when _previousReducingFactor > 0 => _previousReducingFactor,
            < 1e-6 => CircuitPowerParameters.DefaultReducingFactor,
            > 1 => 1,
            _ => ReducingFactor
        };

        _previousReducingFactor = ReducingFactor;
            
        CircuitPowerParameters.SetReducingFactor(ReducingFactor);
        ChangedElements.Modify(ElectricalSystem.RevitId);
        UpdateCablePermissibleCurrent();
    }

    private void OnIsSelectedChanged()
    {
        if (!_isInitialized)
            return;
            
        SectionViewModel?.UpdateSelections();
    }

    private void OnSelectedPhaseChanged()
    {
        if (!_isInitialized)
            return;
            
        if (!_selectedPhaseChanging)
        {
            var phaseUtils = new PhaseUtils();
            phaseUtils.SetPhaseAndCalculate(ElectricalSystem, SelectedPhase);
            // ElectricalCalculator.Loads.CalculateParents(ElectricalSystem);
            SetPhaseMarks();
        }
            
        ChangedElements.Modify(ElectricalSystem.RevitId);
        UpdateProperties();
    }

    private void OnAdditionalDemandFactorChanged()
    {
        if (!_isInitialized || _demandFactorChanging || _propertiesUpdating)
            return;
            
        try
        {
            _demandFactorChanging = true;
                
            // ElectricalCalculator.Loads.UpdateAdditionalDemandFactor(ElectricalSystem, AdditionalDemandFactor);
            TotalDemandFactor = EstimatedPowerParameters.TotalDemandFactor;

            ChangedElements.Modify(ElectricalSystem.RevitId);
            UpdateProperties();
        }
        finally
        {
            _demandFactorChanging = false;
        }
    }

    private void OnTotalDemandFactorChanged()
    {
        if (!_isInitialized || _demandFactorChanging || _propertiesUpdating)
            return;
            
        try
        {
            _demandFactorChanging = true;
                
            // ElectricalCalculator.Loads.UpdateTotalDemandFactor(ElectricalSystem, TotalDemandFactor);
            AdditionalDemandFactor = EstimatedPowerParameters.AdditionalDemandFactor;

            ChangedElements.Modify(ElectricalSystem.RevitId);
            UpdateProperties();
        }
        finally
        {
            _demandFactorChanging = false;
        }
    }

    private void SetPhases()
    {
        var parent = ElectricalSystem.Source;
        var parentPhasesNumber = parent?.PowerParameters.PhasesNumber;
        var parentPhase = parent?.PowerParameters.Phase;

        CanSwitchPhases = ElectricalSystem.PowerParameters.PhasesNumber < parentPhasesNumber;

        Phases = ElectricalSystem.PowerParameters.PhasesNumber switch
        {
            PhasesNumber.One
                when parentPhasesNumber == PhasesNumber.Two &&
                     parentPhase == Phase.L12 =>
                new List<Phase> { Phase.L1, Phase.L2 },

            PhasesNumber.One
                when parentPhasesNumber == PhasesNumber.Two &&
                     parentPhase == Phase.L23 =>
                new List<Phase> { Phase.L2, Phase.L3 },

            PhasesNumber.One
                when parentPhasesNumber == PhasesNumber.Two &&
                     parentPhase == Phase.L13 =>
                new List<Phase> { Phase.L1, Phase.L3 },

            PhasesNumber.One
                when parentPhasesNumber == PhasesNumber.Three =>
                new List<Phase> { Phase.L1, Phase.L2, Phase.L3 },

            PhasesNumber.Two
                when parentPhasesNumber == PhasesNumber.Three =>
                new List<Phase> { Phase.L12, Phase.L23, Phase.L13 },

            _ => null,
        };

        ChangedElements.Modify(ElectricalSystem.RevitId);
        UpdatePhases();
    }

    private void UpdateCable()
    {
        Cable = ElectricalSystem.Products.Cable;
        CablesCount = ElectricalSystem.Products.CablesCount;
        EstimateCablesCount = ElectricalSystem.Products.EstimateCablesCount;
            
        UpdateCablePermissibleCurrent();
    }

    public void UpdateCablePermissibleCurrent()
    {
        CablePermissibleCurrent = CircuitPowerParameters.CablePermissibleCurrent;
        EstimatePermissibleCableCurrent = CircuitPowerParameters.EstimateCablePermissibleCurrent;
    }

    public void UpdatePhases()
    {
        _selectedPhaseChanging = true;
            
        SelectedPhase = PowerParameters.Phase;
        SetPhaseMarks();

        _selectedPhaseChanging = false;
    }

    public void SetPhaseMarks()
    {
        L1 = PowerParameters.IsThreePhases || SelectedPhase is Phase.L1 or Phase.L12 or Phase.L13;
        L2 = PowerParameters.IsThreePhases || SelectedPhase is Phase.L2 or Phase.L12 or Phase.L23;
        L3 = PowerParameters.IsThreePhases || SelectedPhase is Phase.L3 or Phase.L13 or Phase.L23;
    }

    internal void UpdateProducts()
    {
        UpdateCable();
        SwitchViewModel.Update();
    }

    internal void UpdateProperties(OperatingMode operatingMode = null)
    {
        if (!_isInitialized || _propertiesUpdating)
            return;

        _propertiesUpdating = true;
            
        if (operatingMode != null)
        {
            _operatingMode = operatingMode;
            EstimatedPowerParameters = EstimatedPowerParameters = ElectricalSystem.BaseConnector.GetEstimatedPowerParameters(_operatingMode);
        }

        TrueLoad = EstimatedPowerParameters.TrueLoad;
        EstimateTrueLoad = EstimatedPowerParameters.EstimateTrueLoad;
        EstimateCurrent = EstimatedPowerParameters.Current;

        AdditionalDemandFactor = EstimatedPowerParameters.AdditionalDemandFactor;
        DemandFactor = EstimatedPowerParameters.DemandFactor;
        TotalDemandFactor = EstimatedPowerParameters.TotalDemandFactor;
            
        SectionViewModel.UpdateProperties(operatingMode);
        SectionViewModel.SwitchBoardDiagramViewModel.UpdateElectricalChain();

        _propertiesUpdating = false;
    }

    private void UpdateCircuitParameters()
    {
        ShortCurrent1 = EstimatedPowerParameters.ShortCurrent1;
        ShortCurrent3 = EstimatedPowerParameters.ShortCurrent3;
        SurgeShortCurrent3 = EstimatedPowerParameters.SurgeShortCurrent3;
        VoltageDrop = EstimatedPowerParameters.VoltageDrop;
        VoltageDropPercent = EstimatedPowerParameters.VoltageDropPercent;

        ShowVoltageDrop = VoltageDrop > 0;
        ShowShortCurrent1 = ShortCurrent1 > 0;
        ShowShortCurrent3 = ShortCurrent3 > 0;
        ShowShortCurrents = ShowShortCurrent1 || ShowShortCurrent3;
    }

    public Command CableCommand => new(() =>
    {
        var clone = ElectricalSystem.Clone();
                
        // DialogStackView.PushView(
        //     viewContent: new CableSelectorView(clone),
        //     okCallback: () =>
        //     {
        //         ElectricalSystem.PullProperties(clone);
        //         UpdateCable();
        //         SectionViewModel.SwitchboardViewModel.ClipboardViewModel.Update();
        //         return true;
        //     },
        //     okEnabled: ElectricalSystem.Products.Cable != null,
        //     cancelCallback: () =>
        //     {
        //                 
        //     },
        //     buttons: DialogButtons.OkCancel, alignment: DialogAlignment.Stretch);
    });

    // public Command GoToLinkCommand => new Command<string>
    // {
    //     CommandAction = url =>
    //     {
    //         Process.Start(new ProcessStartInfo(url));
    //     }
    // };
}