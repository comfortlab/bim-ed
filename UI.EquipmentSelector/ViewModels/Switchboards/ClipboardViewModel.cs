﻿// ReSharper disable UnusedMember.Local

using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.BusinessLogic.Model.Services;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class ClipboardViewModel : ViewModel
{
    private readonly Repository<ElectricalSystemProxy> _electricalSystemRepository;
    private readonly Repository<SwitchBoardUnit> _switchboardUnitRepository;
    private bool _checkChanging;

    public ClipboardViewModel(IServiceProvider serviceProvider, SwitchBoardDiagramViewModel switchBoardDiagramViewModel)
    {
        Clipboard = serviceProvider.Get<DiagramClipboard>();
        _electricalSystemRepository = serviceProvider.Get<Repository<ElectricalSystemProxy>>();
        _switchboardUnitRepository = serviceProvider.Get<Repository<SwitchBoardUnit>>();
        SwitchBoardDiagramViewModel = switchBoardDiagramViewModel;

        CableToPaste = Clipboard.Cable;
        EnergyMeterToPaste = Clipboard.EnergyMeter;
        ReducingFactorToPaste = Clipboard.ReducingFactor;
        SwitchToPaste = Clipboard.Switch;

        IsCableClipboardChecked = true;
        IsSwitchClipboardChecked = true;
    }
        
    public DiagramClipboard Clipboard { get; set; }
    public SwitchBoardDiagramViewModel SwitchBoardDiagramViewModel { get; set; }
    public SwitchboardSectionViewModel SelectedSwitchboardSection { get; set; }
    public List<CircuitViewModel> SelectedCircuits { get; set; } = new();
    public Cable CableToCopy { get; set; }
    public Cable CableToPaste { get; set; }
    public SelectorProduct SwitchToCopy { get; set; }
    public SelectorProduct SwitchToPaste { get; set; }
    public SelectorProduct EnergyMeterToCopy { get; set; }
    public SelectorProduct EnergyMeterToPaste { get; set; }
    public double ReducingFactorToCopy { get; set; }
    public double ReducingFactorToPaste { get; set; }
        
    public bool CanCopy { get; set; }
    public bool CanEnergyMeterCopy { get; set; }
    public bool CanCableCopy { get; set; }
    public bool CanReducingFactorCopy { get; set; }
    public bool CanSwitchCopy { get; set; }
    public bool CanPaste { get; set; }
    public bool CanEnergyMeterPaste { get; set; }
    public bool CanCablePaste { get; set; }
    public bool CanReducingFactorPaste { get; set; }
    public bool CanSwitchPaste { get; set; }

    public bool IsAllClipboardChecked { get; set; }
    public bool IsSwitchClipboardChecked { get; set; }
    public bool IsEnergyMeterClipboardChecked { get; set; }
    public bool IsCableClipboardChecked { get; set; }
    public bool IsReducingFactorClipboardChecked { get; set; }

    public Command CopyCommand => new(() =>
        {
            if (CanCableCopy && IsCableClipboardChecked)
            {
                Clipboard.Cable = SelectedCircuits?.FirstOrDefault()?.Cable;
                CableToPaste = Clipboard.Cable;
            }

            if (CanEnergyMeterCopy && IsEnergyMeterClipboardChecked)
            {
                Clipboard.EnergyMeter = SelectedSwitchboardSection?.EnergyMeterViewModel.MainProduct;
                EnergyMeterToPaste = Clipboard.EnergyMeter;
            }

            if (CanReducingFactorCopy && IsReducingFactorClipboardChecked)
            {
                Clipboard.ReducingFactor = SelectedCircuits?.FirstOrDefault()?.ReducingFactor ?? default;
                ReducingFactorToPaste = Clipboard.ReducingFactor;
            }

            if (CanSwitchCopy && IsSwitchClipboardChecked)
            {
                Clipboard.Switch =
                    SelectedSwitchboardSection?.InputSwitchViewModel.MainProduct ??
                    SelectedCircuits?.FirstOrDefault()?.SwitchViewModel.MainProduct ??
                    SelectedCircuits?.FirstOrDefault()?.GroupViewModel?.SwitchViewModel.MainProduct;
                SwitchToPaste = Clipboard.Switch;
            }

            SwitchBoardDiagramViewModel.UpdateSelections();
        },
        canExecute: IsAnyChecked);

    public Command PasteCommand => new(() =>
        {
            if (SelectedSwitchboardSection != null)
            {
                if (CanEnergyMeterPaste && IsEnergyMeterClipboardChecked)
                {
                    SelectedSwitchboardSection.EnergyMeterViewModel.MainProduct = Clipboard.EnergyMeter.Clone();
                    _switchboardUnitRepository.ChangedElements.Modify(SelectedSwitchboardSection.Unit.RevitId);
                }

                if (CanSwitchPaste && IsSwitchClipboardChecked)
                {
                    SelectedSwitchboardSection.InputSwitchViewModel.MainProduct = Clipboard.Switch.Clone();
                    _switchboardUnitRepository.ChangedElements.Modify(SelectedSwitchboardSection.Unit.RevitId);
                }
                    
                SwitchBoardDiagramViewModel.UpdateProducts(new [] {SelectedSwitchboardSection});
            }

            else if (SelectedCircuits?.Any() ?? false)
            {
                foreach (var circuitViewModel in SelectedCircuits)
                {
                    if (CanCablePaste && IsCableClipboardChecked)
                    {
                        circuitViewModel.Cable = Clipboard.Cable;
                        _electricalSystemRepository.ChangedElements.Modify(circuitViewModel.ElectricalSystem.RevitId);
                    }

                    if (CanReducingFactorPaste && IsReducingFactorClipboardChecked)
                    {
                        circuitViewModel.ReducingFactor = Clipboard.ReducingFactor;
                        _electricalSystemRepository.ChangedElements.Modify(circuitViewModel.ElectricalSystem.RevitId);
                    }

                    if (CanSwitchPaste && IsSwitchClipboardChecked)
                    {
                        circuitViewModel.SwitchViewModel.MainProduct = Clipboard.Switch.Clone();
                        _electricalSystemRepository.ChangedElements.Modify(circuitViewModel.ElectricalSystem.RevitId);
                    }
                }

                SwitchBoardDiagramViewModel.UpdateProducts(SelectedCircuits);
            }

            SwitchBoardDiagramViewModel.UpdateSelections();

            _switchboardUnitRepository.Save();
            _electricalSystemRepository.Save();
        },
        canExecute: () => IsAnyChecked() && (Clipboard?.IsAnyCopied() ?? false));

    private bool IsAnyChecked()
    {
        return IsCableClipboardChecked ||
               IsEnergyMeterClipboardChecked ||
               IsSwitchClipboardChecked ||
               IsReducingFactorClipboardChecked;
    }

    private void OnCableToPasteChanged() => CanCablePaste = SelectedCircuits.Any() && CableToPaste != null;
    private void OnEnergyMeterToPasteChanged() => CanEnergyMeterPaste = SelectedSwitchboardSection != null && EnergyMeterToPaste != null;
    private void OnReducingFactorToPasteChanged() => CanReducingFactorPaste = SelectedCircuits.Any() && ReducingFactorToPaste > 0;
    private void OnSwitchToPasteChanged() => CanSwitchPaste = SwitchToPaste != null;

    private void OnIsAllClipboardCheckedChanged()
    {
        if (_checkChanging)
            return;

        _checkChanging = true;

        var isAllChecked = IsCableClipboardChecked &&
                           IsEnergyMeterClipboardChecked &&
                           IsSwitchClipboardChecked &&
                           IsReducingFactorClipboardChecked;

        IsCableClipboardChecked = !isAllChecked;
        IsEnergyMeterClipboardChecked = !isAllChecked;
        IsSwitchClipboardChecked = !isAllChecked;
        IsReducingFactorClipboardChecked = !isAllChecked;

        _checkChanging = false;
    }

    private void OnIsCableClipboardCheckedChanged() => UpdateAllChecked();
    private void OnIsEnergyMeterClipboardCheckedChanged() => UpdateAllChecked();
    private void OnIsSwitchClipboardCheckedChanged() => UpdateAllChecked();
    private void OnIsReducingFactorClipboardCheckedChanged() => UpdateAllChecked();

    private void UpdateAllChecked()
    {
        if (_checkChanging)
            return;

        _checkChanging = true;

        IsAllClipboardChecked =
            IsCableClipboardChecked &&
            IsEnergyMeterClipboardChecked &&
            IsSwitchClipboardChecked &&
            IsReducingFactorClipboardChecked;

        _checkChanging = false;
    }

    internal void Update()
    {
        SelectedSwitchboardSection = SwitchBoardDiagramViewModel.SelectedSwitchboardSection;
        SelectedCircuits = SwitchBoardDiagramViewModel.SelectedCircuits;
            
        var isAnyCircuitSelected = SwitchBoardDiagramViewModel.IsAnyCircuitSelected;
        var isOneCircuitSelected = SwitchBoardDiagramViewModel.IsOneCircuitSelected;
        var oneSelectedCircuit = isOneCircuitSelected ? SelectedCircuits.First() : null;
            
        CableToCopy = oneSelectedCircuit?.Cable;
        EnergyMeterToCopy = SelectedSwitchboardSection?.EnergyMeterViewModel.MainProduct;
        ReducingFactorToCopy = oneSelectedCircuit?.ReducingFactor ?? default;
        SwitchToCopy = oneSelectedCircuit?.SwitchViewModel.MainProduct ??
                       oneSelectedCircuit?.GroupViewModel?.SwitchViewModel.MainProduct ??
                       SelectedSwitchboardSection?.InputSwitchViewModel.MainProduct;

        CanCableCopy = CableToCopy != null;
        CanEnergyMeterCopy = EnergyMeterToCopy != null;
        CanReducingFactorCopy = ReducingFactorToCopy > 0;
        CanSwitchCopy = SwitchToCopy != null;

        CanCopy =
            CanCableCopy ||
            CanSwitchCopy ||
            CanEnergyMeterCopy;

        CanCablePaste = isAnyCircuitSelected && CableToPaste != null;
        CanEnergyMeterPaste = SelectedSwitchboardSection != null && EnergyMeterToPaste != null;
        CanReducingFactorPaste = isAnyCircuitSelected && ReducingFactorToPaste > 0;
        CanSwitchPaste = (SelectedSwitchboardSection != null || isAnyCircuitSelected) && SwitchToPaste != null;

        CanPaste =
            CanCablePaste ||
            CanEnergyMeterPaste ||
            CanReducingFactorPaste ||
            CanSwitchPaste;
    }
}