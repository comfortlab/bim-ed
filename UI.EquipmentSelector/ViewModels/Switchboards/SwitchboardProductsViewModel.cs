﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class SwitchboardProductsViewModel
{
    public string SwitchboardDesignation { get; set; }
    public List<ProductWithAccessoriesViewModel> Enclosures { get; set; }
    public List<SectionProductsViewModel> SectionProducts { get; set; }
    public bool HasSingleSection { get; set; }

    public SwitchboardProductsViewModel(SwitchBoard switchBoard)
    {
        SwitchboardDesignation = switchBoard.Name;
            
        Enclosures = switchBoard.SwitchBoardUnitProducts?.Enclosures
                         ?.Select(e => new ProductWithAccessoriesViewModel(e))
                         .GetGrouped()
                     ?? new List<ProductWithAccessoriesViewModel>();
            
        SectionProducts = switchBoard.Units
            .Select(p => new SectionProductsViewModel(p, p.Name))
            .ToList();
    }

    public SwitchboardProductsViewModel(SwitchBoardUnit unit)
    {
        SwitchboardDesignation = unit.Name;
        HasSingleSection = true;

        Enclosures = unit.SwitchBoardUnitProducts?.Enclosures
                         ?.Select(e => new ProductWithAccessoriesViewModel(e))
                         .GetGrouped()
                     ?? new List<ProductWithAccessoriesViewModel>();

        SectionProducts = new List<SectionProductsViewModel>
        {
            new(unit, null)
        };
    }

    public void SortProductsByReference()
    {
        Enclosures = Enclosures.GetSortedByReference();

        SectionProducts.ForEach(p => p.SortProductsByReference());
    }

    public void SortProductsByDescription()
    {
        Enclosures = Enclosures.GetSortedByDescription();

        SectionProducts.ForEach(p => p.SortProductsByDescription());
    }

    public List<ProductWithAccessoriesViewModel> GetProductsWithAccessories()
    {
        var products = new List<ProductWithAccessoriesViewModel>();

        products.AddRange(Enclosures.Select(e => e.Clone()));

        SectionProducts.ForEach(p => products.AddRange(p.GetProductsWithAccessories()));

        return products;
    }

    public List<ProductViewModel> GetProducts()
    {
        var products = new List<ProductViewModel>();

        Enclosures.ForEach(p => products.AddRange(p.GetProducts()));
        SectionProducts.ForEach(c => products.AddRange(c.GetProducts()));

        return products;
    }
}