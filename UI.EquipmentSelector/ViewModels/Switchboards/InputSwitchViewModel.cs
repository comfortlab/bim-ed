﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class InputSwitchViewModel : SwitchViewModel
{
    private readonly SwitchBoardUnit _unit;
        
    public InputSwitchViewModel(IServiceProvider serviceProvider, SwitchboardSectionViewModel switchboardSectionViewModel) :
        base(serviceProvider, switchboardSectionViewModel.Unit, switchboardSectionViewModel.Unit.Products)
    {
        _unit = switchboardSectionViewModel.Unit;
    }

    protected override void AddProduct()
    {
        // MessageMediator.SendMessage(new ViewMessage.ProductSelector.AddInputSectionSwitch(
        //     _unit,
        //     (config, selectorGroup) => ApplyProduct(config, selectorGroup)));
    }

    protected override void OpenProduct(string configuration)
    {
        // MessageMediator.SendMessage(new ViewMessage.ProductSelector.OpenInputSectionSwitch(
        //     _unit,
        //     config => ApplyProduct(config),
        //     (SelectorGroup)MainProduct.SelectorGroup,
        //     configuration));
    }

    protected override void SaveChanges()
    {
        // using var unitOfWork = UnitOfWork.NewInstance();
        // var repository = new SwitchboardUnitRepository(unitOfWork);
        // repository.Update(_unit);
        // unitOfWork.SaveChanges("Apply product");
    }
}