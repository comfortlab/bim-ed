﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.UI.Framework;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class ProductViewModel
{
    public Product Product { get; set; }
    public int Count { get; set; }

    public ProductViewModel(Product product)
    {
        Product = product;
        Count = product.Count;
    }

    public ProductViewModel(ProductViewModel productViewModel)
    {
        Product = productViewModel.Product;
        Count = productViewModel.Count;
    }

    public ProductViewModel Clone()
    {
        return new ProductViewModel(this);
    }

    public Command<string> GoToLinkCommand => new(commandAction: url => Process.Start(new ProcessStartInfo(url)));
}

public static class ProductViewModelExtensions
{
    public static List<ProductViewModel> GetGrouped(this IEnumerable<ProductViewModel> list)
    {
        return list
            .GroupBy(p => p.Product.Reference)
            .Select(g =>
            {
                var product = new ProductViewModel(g.First().Product);
                product.Count = g.Sum(p => p.Count);
                return product;
            })
            .OrderBy(p => p.Product.Reference)
            .ToList();
    }

    public static List<ProductViewModel> GetSortedByReference(this IEnumerable<ProductViewModel> list)
    {
        return list
            .OrderBy(p => p.Product.Reference)
            .ToList();
    }

    public static List<ProductViewModel> GetSortedByDescription(this IEnumerable<ProductViewModel> list)
    {
        return list
            .OrderBy(p => p.Product.Description)
            .ToList();
    }
}