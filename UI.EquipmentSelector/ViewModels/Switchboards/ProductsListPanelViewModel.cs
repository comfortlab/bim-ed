﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class ProductsListPanelViewModel : ViewModel
{
    public bool SortByDescription { get; set; }
    public bool SortByReference { get; set; }
    public bool GroupByPosition { get; set; }
    public bool GroupByMainProduct { get; set; }
    public bool GroupByReference { get; set; }

    public SwitchboardProductsViewModel SwitchboardProducts { get; set; }
    public List<ProductViewModel> Products { get; set; }
    public List<ProductWithAccessoriesViewModel> ProductsWithAccessories { get; set; }

    public ProductsListPanelViewModel(SwitchBoard switchBoard)
    {
        SwitchboardProducts = new SwitchboardProductsViewModel(switchBoard);

        SetProducts();
    }

    public ProductsListPanelViewModel(SwitchBoardUnit unit)
    {
        SwitchboardProducts = new SwitchboardProductsViewModel(unit);

        SetProducts();
    }

    private void SetProducts()
    {
        ProductsWithAccessories = SwitchboardProducts
            .GetProductsWithAccessories()
            .GetGrouped();

        Products = SwitchboardProducts
            .GetProducts()
            .GetGrouped();

        GroupByReference = true;
        SortByReference = true;
    }

    private void SortProductsByReference()
    {
        SwitchboardProducts.SortProductsByReference();

        Products = Products.GetSortedByReference();

        ProductsWithAccessories = ProductsWithAccessories.GetSortedByReference();
    }

    private void SortProductsByDescription()
    {
        SwitchboardProducts.SortProductsByDescription();

        Products = Products.GetSortedByDescription();

        ProductsWithAccessories = ProductsWithAccessories.GetSortedByDescription();
    }

    private void OnGroupByPositionChanged()
    {
        if (GroupByPosition)
        {
            GroupByMainProduct = false;
            GroupByReference = false;
        }
        else
        {
            SetDefaultGroupBy();
        }
    }

    private void OnGroupByMainProductChanged()
    {
        if (GroupByMainProduct)
        {
            GroupByPosition = false;
            GroupByReference = false;
        }
        else
        {
            SetDefaultGroupBy();
        }
    }

    private void OnGroupByReferenceChanged()
    {
        if (GroupByReference)
        {
            GroupByPosition = false;
            GroupByMainProduct = false;
        }
        else
        {
            SetDefaultGroupBy();
        }
    }

    private void SetDefaultGroupBy()
    {
        if (!GroupByReference && !GroupByMainProduct && !GroupByPosition)
        {
            GroupByReference = true;
        }
    }

    private void OnSortByReferenceChanged()
    {
        if (SortByReference)
        {
            SortProductsByReference();
        }

        SortByDescription = !SortByReference;
    }

    private void OnSortByDescriptionChanged()
    {
        if (SortByDescription)
        {
            SortProductsByDescription();
        }

        SortByReference = !SortByDescription;
    }
}