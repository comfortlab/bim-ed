﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public abstract class ElectricalParametersViewModel : ViewModel
{
    private OperatingMode _operatingMode;
        
    protected readonly ElectricalBase Electrical;
    protected EstimatedPowerParameters EstimatedPowerParameters;

    protected ElectricalParametersViewModel(ElectricalBase electrical, OperatingMode operatingMode)
    {
        Electrical = electrical;
        SetEstimatedPowerParameters(operatingMode);
    }

    private void SetEstimatedPowerParameters(OperatingMode operatingMode)
    {
        if (Equals(_operatingMode, operatingMode))
            return;
                
        _operatingMode = operatingMode;
        EstimatedPowerParameters = Electrical?.GetEstimatedPowerParameters(_operatingMode);
    }
        
    internal void UpdateProperties(OperatingMode operatingMode)
    {
        SetEstimatedPowerParameters(operatingMode);
        UpdateProperties();
    }

    protected abstract void UpdateProperties();
}