﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Products;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

public class ProductWithAccessoriesViewModel
{
    public ProductViewModel MainProduct { get; set; }
    public List<ProductViewModel> Accessories { get; set; }
    public int Count { get; set; }

    public ProductWithAccessoriesViewModel(SelectorProduct selectorProduct)
    {
        Count = 1;
        MainProduct = new ProductViewModel(selectorProduct.Product);
        Accessories = selectorProduct.Accessories
            .Select(a => new ProductViewModel(a))
            .ToList();
    }

    public ProductWithAccessoriesViewModel(ProductWithAccessoriesViewModel productViewModel)
    {
        Count = productViewModel.Count;
        MainProduct = productViewModel.MainProduct.Clone();
        Accessories = productViewModel.Accessories
            .Select(a => a.Clone())
            .ToList();
    }

    public ProductWithAccessoriesViewModel Clone()
    {
        return new ProductWithAccessoriesViewModel(this);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(obj, this))
        {
            return true;
        }

        if (ReferenceEquals(obj, null) || obj.GetType() != GetType())
        {
            return false;
        }

        var productViewModel = (ProductWithAccessoriesViewModel)obj;

        if (MainProduct.Product.Reference != productViewModel.MainProduct.Product.Reference)
        {
            return false;
        }

        if (Accessories.Count != productViewModel.Accessories.Count)
        {
            return false;
        }

        for (var i = 0; i < Accessories.Count; i++)
        {
            if (Accessories[i].Product.Reference != productViewModel.Accessories[i].Product.Reference
                || Accessories[i].Count != productViewModel.Accessories[i].Count)
            {
                return false;
            }
        }

        return true;
    }

    public override int GetHashCode()
    {
        var prime = 37;
        var hash = 1;

        hash = prime * hash + MainProduct.Product.Reference.GetHashCode();
        hash = prime * hash + MainProduct.Product.Count.GetHashCode();

        foreach (var a in Accessories)
        {
            hash = prime * hash + a.Product.Reference.GetHashCode();
            hash = prime * hash + a.Product.Count.GetHashCode();
        }

        return hash;
    }

    public void SortProductsByReference()
    {
        Accessories = Accessories.GetSortedByReference();
    }

    public void SortProductsByDescription()
    {
        Accessories = Accessories.GetSortedByDescription();
    }

    public List<ProductViewModel> GetProducts()
    {
        var products = new List<ProductViewModel>();

        var mainProduct = MainProduct.Clone();
        mainProduct.Count *= Count;
        products.Add(mainProduct);

        products.AddRange(Accessories.Select(a =>
        {
            var product = a.Clone();
            product.Count *= Count;
            return product;
        }));

        return products;
    }
}

public static class ProductWithAccessoriesViewModelExtensions
{
    public static List<ProductWithAccessoriesViewModel> GetGrouped(this IEnumerable<ProductWithAccessoriesViewModel> list)
    {
        return list
            .GroupBy(p => p)
            .Select(g =>
            {
                var product = new ProductWithAccessoriesViewModel(g.First())
                {
                    Count = g.Sum(p => p.Count)
                };
                return product;
            })
            .OrderBy(p => p.MainProduct.Product.Reference)
            .ToList();
    }

    public static List<ProductWithAccessoriesViewModel> GetSortedByReference(this IEnumerable<ProductWithAccessoriesViewModel> list)
    {
        var sortedList = list
            .OrderBy(p => p.MainProduct.Product.Reference)
            .ToList();

        sortedList.ForEach(pa => pa.SortProductsByReference());

        return sortedList;
    }

    public static List<ProductWithAccessoriesViewModel> GetSortedByDescription(this IEnumerable<ProductWithAccessoriesViewModel> list)
    {
        var sortedList = list
            .OrderBy(p => p.MainProduct.Product.Description)
            .ToList();

        sortedList.ForEach(pa => pa.SortProductsByDescription());

        return sortedList;
    }
}