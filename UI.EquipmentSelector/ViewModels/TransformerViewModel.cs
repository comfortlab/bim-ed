﻿using System;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.Comparer;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Base;
using CoLa.BimEd.UI.Framework;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels;

public sealed class TransformerViewModel : EquipmentViewModel<Transformer>
{
    private readonly bool _isInitialized;
    private readonly double[] _primaryVoltages;
    private readonly double[] _secondaryVoltages;
    private readonly DoubleEqualityComparer _primaryVoltageComparer = new(501);
    private readonly DoubleEqualityComparer _secondaryVoltageComparer = new(11);

    public TransformerViewModel(IServiceProvider serviceProvider, Transformer transformer = null, bool showInstanceButtons = false) :
        base(serviceProvider, transformer)
    {
        var allTransformerProducts = ServiceProvider.Get<Repository<TransformerSeries>>().SelectMany(x => x.Products).ToArray();
        _primaryVoltages = allTransformerProducts.Select(x => x.PrimaryVoltage).Distinct(new DoubleEqualityComparer()).ToArray();
        _secondaryVoltages = allTransformerProducts.Select(x => x.SecondaryVoltage).Distinct(new DoubleEqualityComparer()).ToArray();
        SelectedDistributionSystem = EditedEquipment.DistributionSystem;
        SelectedSecondaryDistributionSystem = EditedEquipment.SecondaryDistributionSystem;
        IsProductSelected = EditedEquipment.TransformerProduct.Product != null;
        IsReactanceManualInput = EditedEquipment.TransformerReactanceMode == TransformerReactanceMode.ManualInput;
        IsReactanceByPrimaryBreakingCapacity = EditedEquipment.TransformerReactanceMode == TransformerReactanceMode.ByPrimaryBreakingCapacity;
        IsReactanceByPrimaryShortCircuitPower = EditedEquipment.TransformerReactanceMode == TransformerReactanceMode.ByPrimaryShortCircuitPower;
        NetworkInductiveReactance = EditedEquipment.NetworkInductiveReactance;
        PrimaryBreakingCapacity = EditedEquipment.PrimaryBreakingCapacity;
        PrimaryShortCircuitPower = EditedEquipment.PrimaryShortCircuitPower;
        IsPlacedInModel = EditedEquipment.IsPlacedInModel;
        ShowInstanceButtons = showInstanceButtons;

        UpdateTransformerParameters();

        _isInitialized = true;
    }

    public string TransformerSeriesName { get; set; }
    public DistributionSystemProxy SelectedSecondaryDistributionSystem { get; set; }
    public TransformerProductViewModel TransformerProductViewModel { get; set; }
    public double PrimaryShortCircuitPower { get; set; }
    public double PrimaryBreakingCapacity { get; set; }
    public double NetworkInductiveReactance { get; set; }
    public double RatedPower { get; set; }
    public double PrimaryVoltage { get; set; }
    public double SecondaryVoltage { get; set; }
    public ProtectionIndex ProtectionIndex { get; set; }
    public VectorGroup VectorGroup { get; set; }
    public bool CanSelectTransformerProduct { get; set; }
    public bool IsDistributionSystemsSelected { get; set; }
    public bool IsPlacedInModel { get; set; }
    public bool IsProductSelected { get; set; }
    public bool IsReactanceManualInput { get; set; } = true;
    public bool IsReactanceByPrimaryBreakingCapacity { get; set; }
    public bool IsReactanceByPrimaryShortCircuitPower { get; set; }
    public bool ShowInstanceButtons { get; set; }
    
    // ReSharper disable UnusedMember.Local
    private void OnSelectedSecondaryDistributionSystemChanged()
    {
        EditedEquipment.SecondaryDistributionSystem = SelectedSecondaryDistributionSystem;

        IsDistributionSystemsSelected =
            SelectedDistributionSystem != null &&
            SelectedSecondaryDistributionSystem != null; 
            
        UpdateOkEnabled();
    }
    
    private void OnTransformerProductViewModelChanged() => IsMainContentEnabled = TransformerProductViewModel == null;

    private void OnNetworkInductiveReactanceChanged()
    {
        EditedEquipment.NetworkInductiveReactance = NetworkInductiveReactance;
    }
    
    private bool _isPropertyChanging;
    private void OnPrimaryShortCircuitPowerChanged()
    {
        if (!_isInitialized || _isPropertyChanging)
            return;

        try
        {
            _isPropertyChanging = true;

            PrimaryBreakingCapacity = default;
            EditedEquipment.PrimaryShortCircuitPower = PrimaryShortCircuitPower;
            NetworkInductiveReactance = EditedEquipment.NetworkInductiveReactance;
        }
        finally
        {
            _isPropertyChanging = false;
        }
    }

    private void OnPrimaryBreakingCapacityChanged()
    {
        if (!_isInitialized || _isPropertyChanging)
            return;

        try
        {
            _isPropertyChanging = true;
                
            PrimaryShortCircuitPower = default;
            EditedEquipment.PrimaryBreakingCapacity = PrimaryBreakingCapacity;
            NetworkInductiveReactance = EditedEquipment.NetworkInductiveReactance;
        }
        finally
        {
            _isPropertyChanging = false;
        }
    }

    private void OnIsReactanceManualInputChanged()
    {
        if (!_isInitialized) return;
        if (IsReactanceManualInput) IsReactanceByPrimaryBreakingCapacity = IsReactanceByPrimaryShortCircuitPower = false;
        else IsReactanceManualInput = !(IsReactanceByPrimaryBreakingCapacity || IsReactanceByPrimaryShortCircuitPower);
        EditedEquipment.TransformerReactanceMode = TransformerReactanceMode.ManualInput;
    }

    private void OnIsReactanceByPrimaryBreakingCapacityChanged()
    {
        if (!_isInitialized) return;
        if (IsReactanceByPrimaryBreakingCapacity) IsReactanceManualInput = IsReactanceByPrimaryShortCircuitPower = false;
        else IsReactanceManualInput = !(IsReactanceByPrimaryBreakingCapacity || IsReactanceByPrimaryShortCircuitPower);
        EditedEquipment.TransformerReactanceMode = TransformerReactanceMode.ByPrimaryBreakingCapacity;
    }

    private void OnIsReactanceByPrimaryShortCircuitPowerChanged()
    {
        if (!_isInitialized) return;
        if (IsReactanceByPrimaryShortCircuitPower) IsReactanceManualInput = IsReactanceByPrimaryBreakingCapacity = false;
        else IsReactanceManualInput = !(IsReactanceByPrimaryBreakingCapacity || IsReactanceByPrimaryShortCircuitPower);
        EditedEquipment.TransformerReactanceMode = TransformerReactanceMode.ByPrimaryShortCircuitPower;
    }
    
    //     
    // protected override void UpdateCompatibles()
    // {
    //     var connected = Connected;
    //         
    //     using var unitOfWork = UnitOfWork.NewInstance();
    //     var sourceRepository = new ElectricalSourceRepository(unitOfWork);
    //     var switchboardUnitRepository = new SwitchboardUnitRepository(unitOfWork);
    //
    //     var compatibleSources = sourceRepository.GetAll()
    //         .Where(n =>
    //             n.IsPower && 
    //             n.DistributionSystem.IsCompatible(SelectedDistributionSystem))
    //         .ToList();
    //         
    //     var compatibleSwitchboardUnits = switchboardUnitRepository.GetAll()
    //         .Where(n =>
    //             n.IsPower &&
    //             n.Consumers.IsEmpty() &&
    //             n.DistributionSystem.IsCompatible(SelectedDistributionSystem))
    //         .ToList();
    //         
    //     AllCompatibles = compatibleSources.Concat<ElectricalEquipment>(compatibleSwitchboardUnits).ToList();
    //
    //     var takeCompatibleSources = compatibleSources.Take(5).ToList();
    //     var takeCompatibleSwitchboardUnits = compatibleSwitchboardUnits.Take(10 - takeCompatibleSources.Count);
    //         
    //     Compatibles = takeCompatibleSources.Concat<ElectricalEquipment>(takeCompatibleSwitchboardUnits).ToList();
    //
    //     if (!Compatibles.Contains(Connected))
    //         Connected = connected ?? Compatibles.FirstOrDefault();
    // }
    //
    // protected override void UpdateOkEnabled()
    // {
    //     DialogStackView.SetOkEnabled(
    //         !string.IsNullOrEmpty(Name) &&
    //         SelectedDistributionSystem != null &&
    //         SelectedSecondaryDistributionSystem != null);
    // }

    public Command SelectTransformerProductCommand => new(() =>
        {
            var cloneTransformer = EditedEquipment.Clone();
            TransformerProductViewModel = new TransformerProductViewModel(ServiceProvider, cloneTransformer);
            TransformerProductViewModel.OkCallback = _ =>
            {
                EditedEquipment.PullProperties(cloneTransformer);
                IsProductSelected = EditedEquipment.TransformerProduct.Product != null;
                UpdateTransformerParameters();
            };
            TransformerProductViewModel.Close = () => TransformerProductViewModel = null;
        },
        canExecute: () =>
            SelectedDistributionSystem != null &&
            SelectedSecondaryDistributionSystem != null &&
            (_primaryVoltages?.Contains(SelectedDistributionSystem.LineToLineVoltage.ActualValue, _primaryVoltageComparer) ?? false) &&
            (_secondaryVoltages?.Contains(SelectedSecondaryDistributionSystem.LineToLineVoltage.ActualValue, _secondaryVoltageComparer) ?? false));

    public Command DeleteTransformerProductCommand => new(() =>
    {
        EditedEquipment.TransformerProduct.Reset();
        IsProductSelected = false;
        UpdateTransformerParameters();
    });

    public Command CreateFamilyInstanceCommand => new (() =>
    {
    //     WindowService.CloseWindow();
    //     RevitServices.FamilyInstanceCreator.CreateTransformerInstance(EditedEquipment, MainDiagramWindow.ShowWindow);
    //     // using var uow = UnitOfWork.NewInstance();
    //     // var transformerRepository = new TransformerRepository(uow);
    //     // transformerRepository.Insert(EditedEquipment);
    //     // uow.SaveChanges(TransactionNames.CreateTransformer);
    });

    public Command EditFamilyInstanceCommand => new(() =>
    {
        
    });

    public Command DeleteFamilyInstanceCommand => new(() =>
    {
        // DialogStackView.PushMessage(
        //     Messages.Question_DeleteFamilyInstance,
        //     DialogButtons.DeleteCancel,
        //     okCallback: () =>
        //     {
        //         var sync = DocumentContext.Instance.SyncContextWithDocument;
        //         DocumentContext.Instance.SyncContextWithDocument = true;
        //         using var uow = UnitOfWork.NewInstance();
        //         var transformerRepository = new TransformerRepository(uow);
        //         transformerRepository.Delete(EditedEquipment);
        //         uow.SaveChanges(TransactionNames.Delete);
        //         DocumentContext.Instance.SyncContextWithDocument = sync;
        //         return true;
        //     });
    });

    // public Command ConnectToCommand => new(() => { });
    public Command ShowFamilyInstanceCommand => new(() => { });

    protected override Transformer CreateDefault()
    {
        ExistNames = ServiceProvider.Get<Transformer[]>().Select(x => x.Name).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        var newName = NameUtils.GetNextName($"{Names.PrefixTransformer}1", ExistNames);
        return new Transformer(VirtualElements.NewId(), newName);
    }

    protected override void Create()
    {
        VirtualElements.Transformers.Add(EditedEquipment);
        VirtualElementsRepository.ChangedElements.UpdateStorage();
    }

    private void UpdateTransformerParameters()
    {
        TransformerSeriesName = EditedEquipment.TransformerProduct.TransformerSeries?.Name;
        RatedPower = EditedEquipment.TransformerProduct.RatedPower;
        PrimaryVoltage = EditedEquipment.TransformerProduct.PrimaryVoltage;
        SecondaryVoltage = EditedEquipment.TransformerProduct.SecondaryVoltage;
        ProtectionIndex = EditedEquipment.TransformerProduct.ProtectionIndex;
        VectorGroup = EditedEquipment.TransformerProduct.VectorGroup;
    }

    protected override void UpdateDistributionSystems()
    {
        base.UpdateDistributionSystems();
        
        DistributionSystems = DistributionSystems
            .Where(n => n.PhasesNumber == PhasesNumber.Three)
            .ToList();
    }

    protected override void UpdateOkEnabled()
    {
        IsOkEnabled =
            !string.IsNullOrWhiteSpace(Name) &&
            SelectedDistributionSystem != null &&
            SelectedSecondaryDistributionSystem != null;
    }
}