﻿using System;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Base;

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels;

public class SwitchBoardViewModel : EquipmentViewModel<SwitchBoardUnit>
{
    public SwitchBoardViewModel(IServiceProvider serviceProvider, SwitchBoardUnit switchBoardUnit = null) :
        base(serviceProvider, switchBoardUnit)
    {
        
    }

    protected override SwitchBoardUnit CreateDefault()
    {
        var forbiddenNames = ServiceProvider.Get<Repository<SwitchBoardUnit>>().Select(x => x.Name).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        var newName = NameUtils.GetNextName($"{Names.PrefixSwitchboard}1", forbiddenNames);
        return SwitchBoardUnit.CreateSingle(VirtualElements.NewId(), newName, ElectricalSystemTypeProxy.PowerCircuit);
    }

    protected override void Create()
    {
        VirtualElements.SwitchboardUnits.Add(EditedEquipment);
        VirtualElementsRepository.ChangedElements.UpdateStorage();
    }

    protected override void UpdateDistributionSystems()
    {
        base.UpdateDistributionSystems();
        
        DistributionSystems = DistributionSystems
            .Where(n => n.IsLowVoltage())
            .ToList();
    }
}