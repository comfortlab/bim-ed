﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.EquipmentSelector.ViewModels;

public class TransformerProductViewModel : ViewModel
{
    private readonly bool _isInitialized;
    private readonly TransformerSeries _transformerSeries;
        
    public Transformer Transformer { get; set; }
    public TransformerProduct TransformerProduct { get; set; }

    public TransformerProductViewModel(IServiceProvider serviceProvider, Transformer transformer)
    {
        Transformer = transformer ?? throw new ArgumentNullException(nameof(transformer));
        TransformerProduct = Transformer.TransformerProduct;
             
        var transformerSeriesRepository = (TransformerSeriesRepository)serviceProvider.Get<Repository<TransformerSeries>>();
        var estimatedPowerParameters = Transformer.EstimatedPowerParameters;
             
        EstimateApparentLoad = Transformer.IsPower ? estimatedPowerParameters.EstimateApparentLoad : 0;
        
        _transformerSeries = transformerSeriesRepository
            .GetTrihalEasyTransformerSeries()?
            .FirstOrDefault();
             
        if (_transformerSeries == null)
            return;
            
        Name = Transformer.Name;
        TransformerSeriesName = _transformerSeries.Name;
        TransformerProduct.TransformerSeries = _transformerSeries;
            
        var primaryVoltage = transformer.DistributionSystem?.LineToLineVoltage?.ActualValue ?? 0;
        var secondaryVoltage = transformer.SecondaryDistributionSystem?.LineToLineVoltage?.ActualValue ?? 0;
        var primaryVoltageTolerance = 0.1 * primaryVoltage;
        var secondaryVoltageTolerance = 0.1 * secondaryVoltage;
            
        RatedPowers = _transformerSeries.RatedPowers.ToList();
            
        PrimaryVoltages = _transformerSeries
            .PrimaryVoltages
            .Where(n => primaryVoltage.IsEqualTo(n, primaryVoltageTolerance)).ToList();
            
        SecondaryVoltages = _transformerSeries
            .SecondaryVoltages
            .Where(n => secondaryVoltage.IsEqualTo(n, secondaryVoltageTolerance)).ToList();
            
        InsulationVoltages = _transformerSeries
            .Products
            .Where(n => PrimaryVoltages.Contains(n.PrimaryVoltage))
            .Select(n => n.InsulationVoltage)
            .Distinct().ToList();
            
        VectorGroups = _transformerSeries.VectorGroups;
        ProtectionIndices = _transformerSeries.ProtectionIndices;
        Materials = _transformerSeries.WindingMaterials;
        CoolingModes = _transformerSeries.CoolingModes;
        TransformerProperties = _transformerSeries.Properties;
        SelectedPower = GetSelectedPower(estimatedPowerParameters);
        SelectedPrimaryVoltage = TransformerProduct.PrimaryVoltage > 0 && PrimaryVoltages.Contains(TransformerProduct.PrimaryVoltage)
            ? TransformerProduct.PrimaryVoltage
            : primaryVoltage;
        SelectedSecondaryVoltage = secondaryVoltage;
        SelectedVectorGroup = VectorGroups.Contains(TransformerProduct.VectorGroup)
            ? TransformerProduct.VectorGroup
            : VectorGroups.FirstOrDefault();
        SelectedProtectionIndex = ProtectionIndices.Contains(TransformerProduct.ProtectionIndex)
            ? TransformerProduct.ProtectionIndex
            : ProtectionIndices.FirstOrDefault();
        SelectedMaterial = TransformerProduct.WindingMaterial ?? Materials.FirstOrDefault();
        SelectedCoolingMode = CoolingModes.Contains(TransformerProduct.CoolingMode)
            ? TransformerProduct.CoolingMode
            : CoolingModes.FirstOrDefault();
            
        SelectedTransformerProperties = TransformerProduct.Properties?.ToList();
        // SelectedTransformerProperties.CollectionChanged += OnSelectedTransformerPropertiesChanged;
        
        _isInitialized = true;
            
        SetTransformer();
    }

    public string Name { get; set; }
    public string TransformerSeriesName { get; }
    public double EstimateApparentLoad { get; set; }
    public List<double> RatedPowers { get; set; }
    public List<double> InsulationVoltages { get; set; }
    public List<double> PrimaryVoltages { get; set; }
    public IEnumerable<double> SecondaryVoltages { get; set; }
    public IEnumerable<VectorGroup> VectorGroups { get; set; }
    public IEnumerable<ProtectionIndex> ProtectionIndices { get; set; }
    public IEnumerable<Material> Materials { get; set; }
    public IEnumerable<CoolingMode> CoolingModes { get; set; }
    public IEnumerable<TransformerProperty> TransformerProperties { get; set; }

    public double SelectedPower { get; set; }
    public double InsulationVoltage { get; set; }
    public double SelectedPrimaryVoltage { get; set; }
    public double SelectedSecondaryVoltage { get; set; }
    public double PrimaryCurrent { get; set; }
    public double SecondaryCurrent { get; set; }
    public double Resistance { get; set; }
    public double InductiveReactance { get; set; }
    public double NoLoadLosses{ get; set; }
    public double LoadLosses120 { get; set; }
    public ProtectionIndex SelectedProtectionIndex { get; set; }
    public CoolingMode SelectedCoolingMode { get; set; }
    public Material SelectedMaterial { get; set; }
    public VectorGroup SelectedVectorGroup { get; set; }
    public List<TransformerProperty> SelectedTransformerProperties { get; set; }
    public double Height { get; set; }
    public double Length { get; set; }
    public double Width { get; set; }
    public double Weight { get; set; }
    public bool IsMainContentEnabled { get; set; } = true;
    public bool IsOkEnabled { get; set; } = true;

    private void OnNameChanged() => Transformer.Name = Name;
    private void OnSelectedPrimaryVoltageChanged() => SetTransformer();
    private void OnSelectedPowerChanged() => SetTransformer();
    private void OnSelectedProtectionIndexChanged()
    {
        TransformerProduct.ProtectionIndex = SelectedProtectionIndex;
        SetTransformer();
    }
    private void OnSelectedCoolingModeChanged() => TransformerProduct.CoolingMode = SelectedCoolingMode;
    private void OnSelectedMaterialChanged() => TransformerProduct.WindingMaterial = SelectedMaterial;
    private void OnSelectedVectorGroupChanged() => TransformerProduct.VectorGroup = SelectedVectorGroup;
        
    private void OnSelectedTransformerPropertiesChanged()
    {
        TransformerProduct.Properties = SelectedTransformerProperties?.ToList() ?? new List<TransformerProperty>();
    }

    private double GetSelectedPower(EstimatedPowerParameters estimatedPowerParameters)
    {
        var selectedPower = TransformerProduct.RatedPower > 0
            ? TransformerProduct.RatedPower
            : RatedPowers.FirstOrDefault(x => x > (estimatedPowerParameters?.EstimateApparentLoad ?? 0));

        if (selectedPower.IsEqualToZero())
            selectedPower = RatedPowers.LastOrDefault();
        return selectedPower;
    }

    private void SetTransformer()
    {
        if (!_isInitialized || _transformerSeries == null)
            return;

        var prototype = _transformerSeries.GetProduct(
            SelectedPower,
            SelectedPrimaryVoltage,
            SelectedSecondaryVoltage,
            SelectedProtectionIndex);

        if (prototype == null)
            throw new NullReferenceException(nameof(prototype));

        TransformerProduct.PullProductProperties(prototype);

        InsulationVoltage = TransformerProduct.InsulationVoltage;
        PrimaryCurrent = TransformerProduct.PrimaryCurrent;
        SecondaryCurrent = TransformerProduct.SecondaryCurrent;
        Resistance = TransformerProduct.Resistance;
        InductiveReactance = TransformerProduct.InductiveReactance;
        NoLoadLosses = TransformerProduct.NoLoadLosses;
        LoadLosses120 = TransformerProduct.LoadLosses120;
        Height = TransformerProduct.Product.Dimensions.Height;
        Length = TransformerProduct.Product.Dimensions.Width;
        Width = TransformerProduct.Product.Dimensions.Depth;
        Weight = TransformerProduct.Product.Weight;
    }
}