﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Windows;

public partial class CreateTransformerWindow
{
    public CreateTransformerWindow(IServiceProvider serviceProvider)
    {
        InitializeComponent();
        this.SetPositionAndSizes(0.9);
        var virtualElements = serviceProvider.Get<VirtualElements>();
        DataContext = new TransformerViewModel(serviceProvider, new Transformer(virtualElements.NewId()), showInstanceButtons: false)
        {
            Close = Close
        };
    }
}