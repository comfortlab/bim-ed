﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Transformers;

public partial class TransformerProductView
{
    public TransformerProductView()
    {
        InitializeComponent();
        // Loaded += (_, _) => { TransformerName.Focus(); };
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.ScrollMouseWheel(sender, e);
}