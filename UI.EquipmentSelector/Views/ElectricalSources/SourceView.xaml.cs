﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.ElectricalSources;

public partial class SourceView
{
    public SourceView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(SourceName);
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.MultiScrollMouseWheel(sender, e);
}