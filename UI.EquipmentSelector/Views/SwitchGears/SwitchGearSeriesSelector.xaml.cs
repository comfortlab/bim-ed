﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;

public partial class SwitchGearSeriesSelector
{
    public SwitchGearSeriesSelector() => InitializeComponent();

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.ScrollMouseWheel(sender, e);
}