﻿// ReSharper disable InconsistentNaming
namespace CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears.Diagrams;

public static class DiagramKeys
{
    public const string RM6_Empty_Preview                       = "RM6_Empty_Preview";
    public const string RM6_Cable                               = "RM6_Cable";
    public const string RM6_Cable_Preview                       = "RM6_Cable_Preview";
    public const string RM6_CircuitBreaker                      = "RM6_CircuitBreaker";
    public const string RM6_CircuitBreaker_Preview              = "RM6_CircuitBreaker_Preview";
    public const string RM6_CircuitBreaker_Section              = "RM6_CircuitBreaker_Section";
    public const string RM6_CircuitBreaker_Section_Preview      = "RM6_CircuitBreaker_Section_Preview";
    public const string RM6_DisconnectingSwitch                 = "RM6_DisconnectingSwitch";
    public const string RM6_DisconnectingSwitch_Preview         = "RM6_DisconnectingSwitch_Preview";
    public const string RM6_DisconnectingSwitch_Section         = "RM6_DisconnectingSwitch_Section";
    public const string RM6_DisconnectingSwitch_Section_Preview = "RM6_DisconnectingSwitch_Section_Preview";
    public const string RM6_DisconnectingSwitchFuse             = "RM6_DisconnectingSwitchFuse";
    public const string RM6_DisconnectingSwitchFuse_Preview     = "RM6_DisconnectingSwitchFuse_Preview";
    public const string RM6_Metering                            = "RM6_Metering";
    public const string RM6_Metering_Preview                    = "RM6_Metering_Preview";
}