﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;

public partial class SwitchGearConstructor
{
    public SwitchGearConstructor() => InitializeComponent();
    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.MultiScrollMouseWheel(sender, e);
}