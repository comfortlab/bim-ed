﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;

public partial class CreateSwitchGearView
{
    public CreateSwitchGearView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(SwitchGearName);
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) => ScrollingUtils.ScrollMouseWheel(sender, e);
}