﻿using System.Windows.Input;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class ProductsListPanelView
{
    public ProductsListPanelView(SwitchBoard switchBoard)
    {       
        InitializeComponent();
        Loaded += (o, e) => DataContext = new ProductsListPanelViewModel(switchBoard);
    }

    public ProductsListPanelView(SwitchBoardUnit unit)
    {
        InitializeComponent();
        Loaded += (o, e) => DataContext = new ProductsListPanelViewModel(unit);
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.ScrollMouseWheel(sender, e);
}