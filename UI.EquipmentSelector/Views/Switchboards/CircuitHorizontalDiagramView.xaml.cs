﻿using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class CircuitHorizontalDiagramView
{
    public CircuitHorizontalDiagramView() => InitializeComponent();

    public CircuitHorizontalDiagramView(CircuitViewModel circuitViewModel)
        : this() => DataContext = circuitViewModel;
}