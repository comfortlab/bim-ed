﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class ElementsView
{
    public ElementsView() => InitializeComponent();

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.ScrollMouseWheel(sender, e);
}