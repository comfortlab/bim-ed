﻿using System.Windows;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards.Wires;

public partial class InputWiresView
{
    public static readonly DependencyProperty IsConnectedProperty = DependencyProperty.Register(
        "IsConnected", typeof(bool), typeof(InputWiresView), new PropertyMetadata(default(bool)));
        
    public InputWiresView() => InitializeComponent();

    public bool IsConnected
    {
        get => (bool)GetValue(IsConnectedProperty);
        set => SetValue(IsConnectedProperty, value);
    }
}