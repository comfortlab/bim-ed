﻿using System.Windows;
using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class SwitchboardUnitDiagramView
{
    public SwitchboardUnitDiagramView()
    {
        InitializeComponent();
    }

    private void VerticalScroll(object sender, MouseWheelEventArgs e)
    {
        if ((e.OriginalSource as FrameworkElement)?.FindVisualAncestor<ElementsView>() != null)
            e.Handled = false;
        else
            ScrollingUtils.ScrollMouseWheel(sender, e);
    }

    private void ParentHorizontalScroll(object sender, MouseWheelEventArgs e)
    {
        this.FindVisualAncestor<SwitchboardDiagramView>()?.PanelsHorizontalScroll(sender, e);
    }
}