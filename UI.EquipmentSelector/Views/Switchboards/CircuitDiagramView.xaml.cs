﻿using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class CircuitDiagramView
{
    public CircuitDiagramView() => InitializeComponent();

    public CircuitDiagramView(CircuitViewModel circuitViewModel)
        : this()
    {
        DataContext = circuitViewModel;
    }
}