﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class CreateSwitchBoardView
{
    public CreateSwitchBoardView()
    {
        InitializeComponent();
        FocusUtils.AutoFocus(SwitchboardName);
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) => ScrollingUtils.ScrollMouseWheel(sender, e);
}