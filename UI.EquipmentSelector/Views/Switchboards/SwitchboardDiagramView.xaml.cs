﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class SwitchboardDiagramView
{
    public SwitchboardDiagramView() => InitializeComponent();

    private void HorizontalScroll(object sender, MouseWheelEventArgs e)
    {
        if (Keyboard.IsKeyDown(Key.LeftShift) ||
            Keyboard.IsKeyDown(Key.RightShift))
            ScrollingUtils.HorizontalScrollMouseWheel(sender, e);
        else
            e.Handled = false;
    }

    internal void PanelsHorizontalScroll(object sender, MouseWheelEventArgs e)
    {
        ScrollingUtils.HorizontalScrollMouseWheel(SectionItems, e);
    }

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
    {
        ScrollingUtils.ScrollMouseWheel(sender, e);
    }
}