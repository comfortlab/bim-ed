﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;

namespace CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

public partial class SectionSwitchVerificationView
{
    public SectionSwitchVerificationView(IServiceProvider serviceProvider, SwitchBoardUnit switchBoardUnit)
    {
        InitializeComponent();
        DataContext = new SectionSwitchVerificationViewModel(serviceProvider, switchBoardUnit);
    }
}