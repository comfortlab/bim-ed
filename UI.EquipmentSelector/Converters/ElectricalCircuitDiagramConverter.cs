using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;
using CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class ElectricalCircuitDiagramConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        value is CircuitViewModel electricalCircuitViewModel
            ? new CircuitHorizontalDiagramView(electricalCircuitViewModel)
            : null;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}