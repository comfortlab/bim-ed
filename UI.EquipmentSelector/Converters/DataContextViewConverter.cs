using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class DataContextViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        null => null,
        SwitchGearSelectorViewModel => new SwitchGearSelector(),
        _ => value
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}