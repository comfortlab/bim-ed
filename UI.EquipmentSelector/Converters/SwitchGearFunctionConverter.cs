﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears.Diagrams;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class SwitchGearFunctionConverter : IMultiValueConverter
{
    public bool IsPreview { get; set; }
    
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values.Length < 2 ||
            values[0] is not FrameworkElement frameworkElement ||
            values[1] is not SwitchGearFunction switchGearFunction)
            return null;

        var key = GetResourceKey(switchGearFunction);
        
        return frameworkElement.FindResource(key);
    }

    private string GetResourceKey(SwitchGearFunction switchGearFunction)
    {
        if (IsPreview)
        {
            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_B ||
                switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_D)
                return DiagramKeys.RM6_CircuitBreaker_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_BC)
                return DiagramKeys.RM6_CircuitBreaker_Section_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_I)
                return DiagramKeys.RM6_DisconnectingSwitch_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_IC)
                return DiagramKeys.RM6_DisconnectingSwitch_Section_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_Mt)
                return DiagramKeys.RM6_Metering_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_O)
                return DiagramKeys.RM6_Cable_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_Q)
                return DiagramKeys.RM6_DisconnectingSwitchFuse_Preview;

            if (switchGearFunction.Guid == Guid.Empty)
                return DiagramKeys.RM6_Empty_Preview;

            return DiagramKeys.RM6_Empty_Preview;
        }
        else
        {
            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_B ||
                switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_D)
                return DiagramKeys.RM6_CircuitBreaker;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_BC)
                return DiagramKeys.RM6_CircuitBreaker_Section_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_I)
                return DiagramKeys.RM6_DisconnectingSwitch_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_IC)
                return DiagramKeys.RM6_DisconnectingSwitch_Section_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_Mt)
                return DiagramKeys.RM6_Metering_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_O)
                return DiagramKeys.RM6_Cable_Preview;

            if (switchGearFunction.Guid == Guids.Products.SwitchGears.RM6.RM6_Q)
                return DiagramKeys.RM6_DisconnectingSwitchFuse_Preview;

            if (switchGearFunction.Guid == Guid.Empty)
                return DiagramKeys.RM6_Empty_Preview;
        }

        return DiagramKeys.RM6_CircuitBreaker_Preview;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}