﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class ProductsCountConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value is int ? $"×{value}" : null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}