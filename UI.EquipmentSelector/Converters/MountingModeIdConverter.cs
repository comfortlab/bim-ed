﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class MountingModeIdConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not string mountingModeId) return null;
        return string.IsNullOrWhiteSpace(mountingModeId) ? null : ElectricalPanelParameters.ResourceManager.GetString(mountingModeId);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}