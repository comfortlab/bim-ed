﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;
using CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class CircuitDiagramConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not CircuitViewModel circuitViewModel)
            return null;

        var circuit = circuitViewModel.ElectricalSystem;

        return circuit.IsGroupCircuit
            ? new CircuitGroupDiagramView
            {
                DataContext = new CircuitGroupViewModel(DI.Get<IServiceProvider>(), circuitViewModel)
            }
            : new CircuitDiagramView
            {
                DataContext = circuitViewModel
            };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}