﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class MarginConverter : IValueConverter
{
    public enum MarginSide
    {
        Left,
        Top,
        Right,
        Bottom,
    }

    public MarginSide Side { get; set; } = MarginSide.Left;

    public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        switch (value)
        {
            case double doubleValue:
                return GetThickness(doubleValue);
                
            case decimal decimalValue:
                return GetThickness((double) decimalValue);
                
            case int intValue:
                return GetThickness(intValue);

            default:
                return new Thickness(0);
        }
    }

    public Thickness GetThickness(double value)
    {
        switch (Side)
        {
            case MarginSide.Left:
                return new Thickness(value, 0, 0, 0);

            case MarginSide.Top:
                return new Thickness(0, value, 0, 0);

            case MarginSide.Right:
                return new Thickness(0, 0, value, 0);

            case MarginSide.Bottom:
                return new Thickness(0, 0, 0, value);

            default:
                return new Thickness(0);
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

public class PhaseMarginConverter : MarginConverter
{
    public double StartOffset { get; set; } = 0;
    public double BetweenOffset { get; set; } = 15;

    public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not Phase phase)
            return GetThickness(StartOffset);

        return phase switch
        {
            Phase.L1 => GetThickness(StartOffset),
            Phase.L2 => GetThickness(StartOffset + BetweenOffset),
            Phase.L3 => GetThickness(StartOffset + BetweenOffset * 2),
            _ => GetThickness(StartOffset)
        };
    }
}

public class PhasesNumberMarginConverter : MarginConverter
{
    public double StartOffset { get; set; } = 15;
    public double BetweenOffset { get; set; } = 15;

    public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not PhasesNumber phasesNumber)
            return GetThickness(StartOffset);

        return phasesNumber switch
        {
            PhasesNumber.One => GetThickness(StartOffset),
            PhasesNumber.Two => GetThickness(StartOffset + BetweenOffset),
            PhasesNumber.Three => GetThickness(StartOffset + BetweenOffset * 2),
            _ => GetThickness(StartOffset)
        };
    }
}