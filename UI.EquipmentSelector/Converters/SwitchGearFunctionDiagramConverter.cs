﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.DataAccess.Model.Repositories;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears.Diagrams.RM6;

namespace CoLa.BimEd.UI.EquipmentSelector.Converters;

public class SwitchGearFunctionDiagramConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not SwitchGearFunctionViewModel switchGearFunctionViewModel)
            return null;

        var seriesGuid = switchGearFunctionViewModel.Item.SwitchGearSeriesGuid; 
        
        if (seriesGuid == Guids.Products.SwitchGears.PremSet.Series)
            return null; // new PremSetDiagram { DataContext = switchGearFunctionViewModel };

        if (seriesGuid == Guids.Products.SwitchGears.RM6.Series)
            return new RM6Diagram { DataContext = switchGearFunctionViewModel };

        if (seriesGuid == Guids.Products.SwitchGears.SM6.Series)
            return null; // new SM6Diagram { DataContext = switchGearFunctionViewModel };

        else
            return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}