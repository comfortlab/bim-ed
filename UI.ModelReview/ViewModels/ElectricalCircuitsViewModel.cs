﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ModelReview.ViewModels;

public class ElectricalCircuitsViewModel : ViewModel
{
    public ElectricalCircuitsViewModel()
    {
        var electricalCircuits = DI.Get<List<ElectricalSystemProxy>>();
        ElectricalCircuits = electricalCircuits;
    }
    
    public List<ElectricalSystemProxy> ElectricalCircuits { get; set; }
}