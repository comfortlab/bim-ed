using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ModelReview.ViewModels;

public class LoadClassificationsViewModel : ViewModel
{
    public LoadClassificationsViewModel()
    {
        var electricalCircuits = DI.Get<Repository<LoadClassificationProxy>>();
        LoadClassifications = electricalCircuits;
    }
    
    public Repository<LoadClassificationProxy> LoadClassifications { get; set; }
}