﻿using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ModelReview;

public partial class ModelReviewWindow
{
    public ModelReviewWindow()
    {
        InitializeComponent();
        this.SetPositionAndSizes(0.9, 0.9);
    }
}