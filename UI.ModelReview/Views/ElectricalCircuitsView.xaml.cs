﻿using CoLa.BimEd.UI.ModelReview.ViewModels;

namespace CoLa.BimEd.UI.ModelReview.Views;

public partial class ElectricalCircuitsView
{
    public ElectricalCircuitsView()
    {
        InitializeComponent();
        DataContext = new ElectricalCircuitsViewModel();
    }
}