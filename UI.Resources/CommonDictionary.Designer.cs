﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoLa.BimEd.UI.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CommonDictionary {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CommonDictionary() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CoLa.BimEd.UI.Resources.CommonDictionary", typeof(CommonDictionary).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calculation options.
        /// </summary>
        public static string CalculationOptions {
            get {
                return ResourceManager.GetString("CalculationOptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Combinations.
        /// </summary>
        public static string Combinations {
            get {
                return ResourceManager.GetString("Combinations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Components.
        /// </summary>
        public static string Components {
            get {
                return ResourceManager.GetString("Components", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configuration.
        /// </summary>
        public static string Configuration {
            get {
                return ResourceManager.GetString("Configuration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection.
        /// </summary>
        public static string Connection {
            get {
                return ResourceManager.GetString("Connection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dimensions.
        /// </summary>
        public static string Dimensions {
            get {
                return ResourceManager.GetString("Dimensions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Element.
        /// </summary>
        public static string Element {
            get {
                return ResourceManager.GetString("Element", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Equal to.
        /// </summary>
        public static string EqualTo {
            get {
                return ResourceManager.GetString("EqualTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Equipment.
        /// </summary>
        public static string Equipment {
            get {
                return ResourceManager.GetString("Equipment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Equipment with the same name already exists.
        /// </summary>
        public static string EquipmentWithNameExists {
            get {
                return ResourceManager.GetString("EquipmentWithNameExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filter by.
        /// </summary>
        public static string FilterBy {
            get {
                return ResourceManager.GetString("FilterBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Functions.
        /// </summary>
        public static string Functions {
            get {
                return ResourceManager.GetString("Functions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grater than.
        /// </summary>
        public static string GraterThan {
            get {
                return ResourceManager.GetString("GraterThan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grater than or Equal to.
        /// </summary>
        public static string GraterThanOrEqualTo {
            get {
                return ResourceManager.GetString("GraterThanOrEqualTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Less than.
        /// </summary>
        public static string LessThan {
            get {
                return ResourceManager.GetString("LessThan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Less than or Equal to.
        /// </summary>
        public static string LessThanOrEqualTo {
            get {
                return ResourceManager.GetString("LessThanOrEqualTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maintenance.
        /// </summary>
        public static string Maintenance {
            get {
                return ResourceManager.GetString("Maintenance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manual input.
        /// </summary>
        public static string ManualInput {
            get {
                return ResourceManager.GetString("ManualInput", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximum.
        /// </summary>
        public static string Maximum {
            get {
                return ResourceManager.GetString("Maximum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimum.
        /// </summary>
        public static string Minimum {
            get {
                return ResourceManager.GetString("Minimum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New name.
        /// </summary>
        public static string NewName {
            get {
                return ResourceManager.GetString("NewName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to None.
        /// </summary>
        public static string None {
            get {
                return ResourceManager.GetString("None", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of functions.
        /// </summary>
        public static string NumberOfFunctions {
            get {
                return ResourceManager.GetString("NumberOfFunctions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old name.
        /// </summary>
        public static string OldName {
            get {
                return ResourceManager.GetString("OldName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options.
        /// </summary>
        public static string Options {
            get {
                return ResourceManager.GetString("Options", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset all.
        /// </summary>
        public static string ResetAll {
            get {
                return ResourceManager.GetString("ResetAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select directory.
        /// </summary>
        public static string SelectDirectory {
            get {
                return ResourceManager.GetString("SelectDirectory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Series.
        /// </summary>
        public static string Series {
            get {
                return ResourceManager.GetString("Series", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Types.
        /// </summary>
        public static string Types {
            get {
                return ResourceManager.GetString("Types", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unlimited.
        /// </summary>
        public static string Unlimited {
            get {
                return ResourceManager.GetString("Unlimited", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value.
        /// </summary>
        public static string Value {
            get {
                return ResourceManager.GetString("Value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Weight.
        /// </summary>
        public static string Weight {
            get {
                return ResourceManager.GetString("Weight", resourceCulture);
            }
        }
    }
}
