﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoLa.BimEd.UI.Resources.Electrical {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Functions {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Functions() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CoLa.BimEd.UI.Resources.Electrical.Functions", typeof(Functions).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bus Riser.
        /// </summary>
        public static string BusRiser {
            get {
                return ResourceManager.GetString("BusRiser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cable Connection.
        /// </summary>
        public static string CableConnection {
            get {
                return ResourceManager.GetString("CableConnection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Protection.
        /// </summary>
        public static string GeneralProtection {
            get {
                return ResourceManager.GetString("GeneralProtection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generator Incomer.
        /// </summary>
        public static string GeneratorIncomer {
            get {
                return ResourceManager.GetString("GeneratorIncomer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Line Feeder.
        /// </summary>
        public static string LineFeeder {
            get {
                return ResourceManager.GetString("LineFeeder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Line Protection.
        /// </summary>
        public static string LineProtection {
            get {
                return ResourceManager.GetString("LineProtection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Metering.
        /// </summary>
        public static string Metering {
            get {
                return ResourceManager.GetString("Metering", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Motor Protection.
        /// </summary>
        public static string MotorProtection {
            get {
                return ResourceManager.GetString("MotorProtection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Section Switch.
        /// </summary>
        public static string SectionSwitch {
            get {
                return ResourceManager.GetString("SectionSwitch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Special.
        /// </summary>
        public static string Special {
            get {
                return ResourceManager.GetString("Special", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transformer Protection.
        /// </summary>
        public static string TransformerProtection {
            get {
                return ResourceManager.GetString("TransformerProtection", resourceCulture);
            }
        }
    }
}
