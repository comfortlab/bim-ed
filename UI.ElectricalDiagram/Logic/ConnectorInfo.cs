﻿using System.Windows;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;

namespace CoLa.BimEd.UI.ElectricalDiagram.Logic;

internal class ConnectorInfo
{
    public double NodeLeft { get; set; }
    public double NodeTop { get; set; }
    public Size NodeSize { get; set; }
    public Point Position { get; set; }
    public ConnectorOrientation Orientation { get; set; }
}