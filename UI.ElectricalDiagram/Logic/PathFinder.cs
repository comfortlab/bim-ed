﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Logic;
// Note: I couldn't find a useful open source library that does
// orthogonal routing so started to write something on my own.
// Categorize this as a quick and dirty short term solution.
// I will keep on searching.

// Helper class to provide an orthogonal connection path
/*
 * TASK BED-35
 * https://comfortlab.atlassian.net/browse/BED-35
 */

internal class PathFinder
{
    private const int Margin = 10;
    private const int MarginForward = 35;
    private const int MarginBackward = 10;

    internal static List<Point> GetConnectionLine(ConnectorInfo source, ConnectorInfo target, bool showLastLine)
    {
        var rectSource = GetRectWithMargin(source, Margin);
        var rectSink = GetRectWithMargin(target, Margin);

        var startPoint = GetOffsetPoint(source, rectSource);
        var endPoint = GetOffsetPoint(target, rectSink);

        return GetLinePoints(source, target, rectSource, rectSink, startPoint, endPoint, showLastLine);
    }

    internal static List<Point> GetConnectionLine(ConnectorInfo source, ConnectorInfo target, bool showLastLine, NodeViewModel sourceNode, NodeViewModel targetNode)
    {
        if (targetNode.GroupViewModel == null
            || source.Orientation != ConnectorOrientation.Right
            || target.Orientation != ConnectorOrientation.Left
            || source.NodeLeft < target.NodeLeft)
        {
            return GetConnectionLine(source, target, showLastLine);
        }

        var rectSource = GetRectWithMargin(sourceNode);
        var rectSink = GetRectWithMargin(targetNode);

        var startPoint = GetOffsetPoint(source, rectSource);
        var endPoint = GetOffsetPoint(target, rectSink);

        return GetLinePoints(source, target, rectSource, rectSink, startPoint, endPoint, showLastLine);
    }

    private static List<Point> GetLinePoints(ConnectorInfo source, ConnectorInfo target, Rect rectSource, Rect rectSink, Point startPoint, Point endPoint, bool showLastLine)
    {
        var linePoints = new List<Point> { startPoint };

        var currentPoint = startPoint;

        if (!rectSink.Contains(currentPoint) && !rectSource.Contains(endPoint))
        {
            while (true)
            {
                #region source node

                if (IsPointVisible(currentPoint, endPoint, new[] { rectSource, rectSink }))
                {
                    linePoints.Add(endPoint);
                    currentPoint = endPoint;
                    break;
                }

                var neighbor = GetNearestVisibleNeighborSink(currentPoint, endPoint, target, rectSource, rectSink);
                if (!double.IsNaN(neighbor.X))
                {
                    linePoints.Add(neighbor);
                    linePoints.Add(endPoint);
                    currentPoint = endPoint;
                    break;
                }

                if (currentPoint == startPoint)
                {
                    var n = GetNearestNeighborSource(source, endPoint, rectSource, rectSink, out var flag);
                    linePoints.Add(n);
                    currentPoint = n;

                    if (IsRectVisible(currentPoint, rectSink, new[] { rectSource }))
                        continue;

                    GetOppositeCorners(source.Orientation, rectSource, out var oppositeCorner1, out var oppositeCorner2);

                    if (flag)
                    {
                        linePoints.Add(oppositeCorner1);
                        currentPoint = oppositeCorner1;
                    }
                    else
                    {
                        linePoints.Add(oppositeCorner2);
                        currentPoint = oppositeCorner2;
                    }

                    if (IsRectVisible(currentPoint, rectSink, new[] { rectSource }))
                        continue;

                    if (flag)
                    {
                        linePoints.Add(oppositeCorner2);
                        currentPoint = oppositeCorner2;
                    }
                    else
                    {
                        linePoints.Add(oppositeCorner1);
                        currentPoint = oppositeCorner1;
                    }
                }
                #endregion

                #region target node

                else // from here on we jump to the target node
                {
                    GetNeighborCorners(target.Orientation, rectSink, out var neighborCorner1, out var neighborCorner2);
                    GetOppositeCorners(target.Orientation, rectSink, out var oppositeCorner1, out var oppositeCorner2);

                    var n1Visible = IsPointVisible(currentPoint, oppositeCorner1, new[] { rectSource, rectSink });
                    var n2Visible = IsPointVisible(currentPoint, oppositeCorner2, new[] { rectSource, rectSink });

                    if (n1Visible && n2Visible)
                    {
                        if (rectSource.Contains(oppositeCorner1))
                        {
                            linePoints.Add(oppositeCorner2);
                            if (rectSource.Contains(neighborCorner2))
                            {
                                linePoints.Add(oppositeCorner1);
                                linePoints.Add(neighborCorner1);
                            }
                            else
                                linePoints.Add(neighborCorner2);

                            linePoints.Add(endPoint);
                            currentPoint = endPoint;
                            break;
                        }

                        if (rectSource.Contains(oppositeCorner2))
                        {
                            linePoints.Add(oppositeCorner1);
                            if (rectSource.Contains(neighborCorner1))
                            {
                                linePoints.Add(oppositeCorner2);
                                linePoints.Add(neighborCorner2);
                            }
                            else
                                linePoints.Add(neighborCorner1);

                            linePoints.Add(endPoint);
                            currentPoint = endPoint;
                            break;
                        }

                        if ((Distance(oppositeCorner1, endPoint) <= Distance(oppositeCorner2, endPoint)))
                        {
                            linePoints.Add(oppositeCorner1);
                            if (rectSource.Contains(neighborCorner1))
                            {
                                linePoints.Add(oppositeCorner2);
                                linePoints.Add(neighborCorner2);
                            }
                            else
                                linePoints.Add(neighborCorner1);
                            linePoints.Add(endPoint);
                            currentPoint = endPoint;
                            break;
                        }
                        else
                        {
                            linePoints.Add(oppositeCorner2);
                            if (rectSource.Contains(neighborCorner2))
                            {
                                linePoints.Add(oppositeCorner1);
                                linePoints.Add(neighborCorner1);
                            }
                            else
                                linePoints.Add(neighborCorner2);
                            linePoints.Add(endPoint);
                            currentPoint = endPoint;
                            break;
                        }
                    }
                    else if (n1Visible)
                    {
                        linePoints.Add(oppositeCorner1);
                        if (rectSource.Contains(neighborCorner1))
                        {
                            linePoints.Add(oppositeCorner2);
                            linePoints.Add(neighborCorner2);
                        }
                        else
                            linePoints.Add(neighborCorner1);
                        linePoints.Add(endPoint);
                        currentPoint = endPoint;
                        break;
                    }
                    else
                    {
                        linePoints.Add(oppositeCorner2);
                        if (rectSource.Contains(neighborCorner2))
                        {
                            linePoints.Add(oppositeCorner1);
                            linePoints.Add(neighborCorner1);
                        }
                        else
                            linePoints.Add(neighborCorner2);
                        linePoints.Add(endPoint);
                        currentPoint = endPoint;
                        break;
                    }
                }
                #endregion
            }
        }
        else
        {
            linePoints.Add(endPoint);
        }

        linePoints = OptimizeLinePoints(linePoints, new[] { rectSource, rectSink }, source.Orientation, target.Orientation);

        CheckPathEnd(source, target, showLastLine, linePoints);

        return linePoints;
    }

    internal static List<Point> GetConnectionLine(ConnectorInfo source, Point sinkPoint, ConnectorOrientation preferredOrientation)
    {
        var linePoints = new List<Point>();
        var rectSource = GetRectWithMargin(source, Margin);
        var startPoint = GetOffsetPoint(source, rectSource);
        var endPoint = sinkPoint;

        linePoints.Add(startPoint);
        var currentPoint = startPoint;

        if (!rectSource.Contains(endPoint))
        {
            while (true)
            {
                if (IsPointVisible(currentPoint, endPoint, new[] { rectSource }))
                {
                    linePoints.Add(endPoint);
                    break;
                }

                var n = GetNearestNeighborSource(source, endPoint, rectSource, out var sideFlag);
                linePoints.Add(n);
                currentPoint = n;

                if (IsPointVisible(currentPoint, endPoint, new[] { rectSource }))
                {
                    linePoints.Add(endPoint);
                    break;
                }
                else
                {
                    GetOppositeCorners(source.Orientation, rectSource, out var oppositeCorner1, out var oppositeCorner2);
                    linePoints.Add(sideFlag ? oppositeCorner1 : oppositeCorner2);
                    linePoints.Add(endPoint);
                    break;
                }
            }
        }
        else
        {
            linePoints.Add(endPoint);
        }

        linePoints = OptimizeLinePoints(linePoints, new[] { rectSource }, source.Orientation, preferredOrientation != ConnectorOrientation.None
            ? preferredOrientation
            : GetOppositeOrientation(source.Orientation));

        return linePoints;
    }

    private static List<Point> OptimizeLinePoints(List<Point> linePoints, Rect[] rectangles, ConnectorOrientation sourceOrientation, ConnectorOrientation sinkOrientation)
    {
        var points = new List<Point>();
        var cut = 0;
        var startPoint = linePoints.First();
        var endPoint = linePoints.Last();
        var idForward =
            (sourceOrientation == ConnectorOrientation.Left ||
             sourceOrientation == ConnectorOrientation.Right) &&
            startPoint.Y < endPoint.Y ||
            (sourceOrientation == ConnectorOrientation.Top ||
             sourceOrientation == ConnectorOrientation.Bottom) &&
            startPoint.X < endPoint.X;

        for (var i = 0; i < linePoints.Count; i++)
        {
            if (i < cut)
                continue;

            for (var k = linePoints.Count - 1; k > i; k--)
            {
                if (!IsPointVisible(linePoints[i], linePoints[k], rectangles))
                    continue;

                cut = k;
                break;
            }

            points.Add(linePoints[i]);
        }

        #region Line
        for (var j = 0; j < points.Count - 1; j++)
        {
            if (points[j].X != points[j + 1].X && points[j].Y != points[j + 1].Y)
            {
                // orientation from point
                var orientationFrom = j == 0 ? sourceOrientation : GetOrientation(points[j], points[j - 1]);

                // orientation to pint 
                var orientationTo = j == points.Count - 2 ? sinkOrientation : GetOrientation(points[j + 1], points[j + 2]);


                if ((orientationFrom == ConnectorOrientation.Left || orientationFrom == ConnectorOrientation.Right) &&
                    (orientationTo == ConnectorOrientation.Left || orientationTo == ConnectorOrientation.Right))
                {
                    var centerX = Math.Min(points[j].X, points[j + 1].X) + (idForward ? MarginForward : MarginBackward); // Math.Abs(points[j].X - points[j + 1].X) / 2;
                    points.Insert(j + 1, new Point(centerX, points[j].Y));
                    points.Insert(j + 2, new Point(centerX, points[j + 2].Y));
                    if (points.Count - 1 > j + 3)
                        points.RemoveAt(j + 3);
                    return points;
                }

                if ((orientationFrom == ConnectorOrientation.Top || orientationFrom == ConnectorOrientation.Bottom) &&
                    (orientationTo == ConnectorOrientation.Top || orientationTo == ConnectorOrientation.Bottom))
                {
                    var centerY = Math.Min(points[j].Y, points[j + 1].Y) + (idForward ? MarginForward : MarginBackward); //Math.Abs(points[j].Y - points[j + 1].Y) / 2;
                    points.Insert(j + 1, new Point(points[j].X, centerY));
                    points.Insert(j + 2, new Point(points[j + 2].X, centerY));
                    if (points.Count - 1 > j + 3)
                        points.RemoveAt(j + 3);
                    return points;
                }

                if ((orientationFrom == ConnectorOrientation.Left || orientationFrom == ConnectorOrientation.Right) &&
                    (orientationTo == ConnectorOrientation.Top || orientationTo == ConnectorOrientation.Bottom))
                {
                    points.Insert(j + 1, new Point(points[j + 1].X, points[j].Y));
                    return points;
                }

                if ((orientationFrom == ConnectorOrientation.Top || orientationFrom == ConnectorOrientation.Bottom) &&
                    (orientationTo == ConnectorOrientation.Left || orientationTo == ConnectorOrientation.Right))
                {
                    points.Insert(j + 1, new Point(points[j].X, points[j + 1].Y));
                    return points;
                }
            }
        }
        #endregion

        return points;
    }

    private static ConnectorOrientation GetOrientation(Point p1, Point p2)
    {
        if (Math.Abs(p1.X - p2.X) < 1e-6)
            return p1.Y >= p2.Y ? ConnectorOrientation.Bottom : ConnectorOrientation.Top;

        if (Math.Abs(p1.Y - p2.Y) < 1e-6)
            return p1.X >= p2.X ? ConnectorOrientation.Right : ConnectorOrientation.Left;

        throw new Exception("Failed to retrieve orientation");
    }

    private static Orientation GetOrientation(ConnectorOrientation sourceOrientation)
    {
        switch (sourceOrientation)
        {
            case ConnectorOrientation.Left:
                return Orientation.Horizontal;

            case ConnectorOrientation.Top:
                return Orientation.Vertical;

            case ConnectorOrientation.Right:
                return Orientation.Horizontal;

            case ConnectorOrientation.Bottom:
                return Orientation.Vertical;

            default:
                throw new Exception("Unknown ConnectorOrientation");
        }
    }

    private static Point GetNearestNeighborSource(ConnectorInfo source, Point endPoint, Rect rectSource, Rect rectSink, out bool flag)
    {
        GetNeighborCorners(source.Orientation, rectSource, out var neighborCorner1, out var neighborCorner2);

        if (rectSink.Contains(neighborCorner1))
        {
            flag = false;
            return neighborCorner2;
        }

        if (rectSink.Contains(neighborCorner2))
        {
            flag = true;
            return neighborCorner1;
        }

        if (Distance(neighborCorner1, endPoint) <= Distance(neighborCorner2, endPoint))
        {
            flag = true;
            return neighborCorner1;
        }
        else
        {
            flag = false;
            return neighborCorner2;
        }
    }

    private static Point GetNearestNeighborSource(ConnectorInfo source, Point endPoint, Rect rectSource, out bool flag)
    {
        GetNeighborCorners(source.Orientation, rectSource, out var neighborCorner1, out var neighborCorner2);

        if (Distance(neighborCorner1, endPoint) <= Distance(neighborCorner2, endPoint))
        {
            flag = true;
            return neighborCorner1;
        }
        else
        {
            flag = false;
            return neighborCorner2;
        }
    }

    private static Point GetNearestVisibleNeighborSink(Point currentPoint, Point endPoint, ConnectorInfo sink, Rect sourceRect, Rect targetRect)
    {
        GetNeighborCorners(sink.Orientation, targetRect, out var neighborCorner1, out var neighborCorner2);

        var neighborCornerVisible1 = IsPointVisible(currentPoint, neighborCorner1, new[] { sourceRect, targetRect });
        var neighborCornerVisible2 = IsPointVisible(currentPoint, neighborCorner2, new[] { sourceRect, targetRect });

        if (neighborCornerVisible1)
        {
            if (neighborCornerVisible2)
            {
                if (targetRect.Contains(neighborCorner1))
                    return neighborCorner2;

                if (targetRect.Contains(neighborCorner2))
                    return neighborCorner1;

                return Distance(neighborCorner1, endPoint) <= Distance(neighborCorner2, endPoint) ? neighborCorner1 : neighborCorner2;

            }
            else
            {
                return neighborCorner1;
            }
        }
        else
        {
            if (neighborCornerVisible2)
            {
                return neighborCorner2;
            }
            else
            {
                return new Point(double.NaN, double.NaN);
            }
        }
    }

    private static bool IsPointVisible(Point fromPoint, Point targetPoint, Rect[] rectangles)
    {
        return rectangles.All(rect => !RectangleIntersectsLine(rect, fromPoint, targetPoint));
    }

    private static bool IsRectVisible(Point fromPoint, Rect targetRect, Rect[] rectangles)
    {
        if (IsPointVisible(fromPoint, targetRect.TopLeft, rectangles))
            return true;

        else if (IsPointVisible(fromPoint, targetRect.TopRight, rectangles))
            return true;

        else if (IsPointVisible(fromPoint, targetRect.BottomLeft, rectangles))
            return true;

        else if (IsPointVisible(fromPoint, targetRect.BottomRight, rectangles))
            return true;

        else
            return false;
    }

    private static bool RectangleIntersectsLine(Rect rect, Point startPoint, Point endPoint)
    {
        rect.Inflate(-1, -1);
        return rect.IntersectsWith(new Rect(startPoint, endPoint));
    }

    private static void GetOppositeCorners(ConnectorOrientation orientation, Rect rect, out Point oppositeCorner1, out Point oppositeCorner2)
    {
        switch (orientation)
        {
            case ConnectorOrientation.Left:
                oppositeCorner1 = rect.TopRight; oppositeCorner2 = rect.BottomRight;
                return;

            case ConnectorOrientation.Top:
                oppositeCorner1 = rect.BottomLeft; oppositeCorner2 = rect.BottomRight;
                return;

            case ConnectorOrientation.Right:
                oppositeCorner1 = rect.TopLeft; oppositeCorner2 = rect.BottomLeft;
                return;

            case ConnectorOrientation.Bottom:
                oppositeCorner1 = rect.TopLeft; oppositeCorner2 = rect.TopRight;
                return;

            default:
                throw new Exception("No opposite corners found!");
        }
    }

    private static void GetNeighborCorners(ConnectorOrientation orientation, Rect rect, out Point neighborCorner1, out Point neighborCorner2)
    {
        switch (orientation)
        {
            case ConnectorOrientation.Left:
                neighborCorner1 = rect.TopLeft;
                neighborCorner2 = rect.BottomLeft;
                return;

            case ConnectorOrientation.Top:
                neighborCorner1 = rect.TopLeft;
                neighborCorner2 = rect.TopRight;
                return;

            case ConnectorOrientation.Right:
                neighborCorner1 = rect.TopRight;
                neighborCorner2 = rect.BottomRight;
                return;

            case ConnectorOrientation.Bottom:
                neighborCorner1 = rect.BottomLeft;
                neighborCorner2 = rect.BottomRight;
                return;

            default:
                throw new Exception("No neighour corners found!");
        }
    }

    private static double Distance(Point p1, Point p2)
    {
        return Point.Subtract(p1, p2).Length;
    }

    private static Rect GetRectWithMargin(ConnectorInfo connectorThumb, double margin)
    {
        var rect = new Rect(
            connectorThumb.NodeLeft,
            connectorThumb.NodeTop,
            connectorThumb.NodeSize.Width,
            connectorThumb.NodeSize.Height);

        rect.Inflate(margin, margin);

        return rect;
    }

    private static Rect GetRectWithMargin(NodeViewModel node)
    {
        var rect = new Rect();

        var groupTopMargin = 3;

        if (node.GroupViewModel != null)
        {
            rect = new Rect(
                node.GroupViewModel.Left - Margin,
                node.GroupViewModel.Top - groupTopMargin,
                node.GroupViewModel.Width + 2 * Margin,
                node.GroupViewModel.Height
            );
        }
        // else
        // {
        //     var parentNodeWithGroup = node.GetParentNodeWithGroup();
        //
        //     if (parentNodeWithGroup != null)
        //     {
        //         var group = parentNodeWithGroup.GroupViewModel;
        //
        //         rect = new Rect(
        //             group.Left - Margin,
        //             group.Top - groupTopMargin,
        //             node.Left - group.Left + Math.Max(node.Width, node.ActualSize.Width) + 2 * Margin,
        //             (node.Top + node.Height > group.Top + group.Height)
        //                 ? node.Top - group.Top + node.Height
        //                 : group.Height
        //         );
        //     }
        //     else
        //     {
        //         rect = new Rect(
        //             node.Left,
        //             node.Top,
        //             Math.Max(node.Width, node.ActualSize.Width),
        //             node.Height
        //         );
        //
        //         rect.Inflate(Margin, Margin);
        //     }
        // }

        return rect;
    }

    private static Point GetOffsetPoint(ConnectorInfo connector, Rect rect)
    {
        var offsetPoint = new Point();

        switch (connector.Orientation)
        {
            case ConnectorOrientation.Left:
                offsetPoint = new Point(rect.Left, connector.Position.Y);
                break;

            case ConnectorOrientation.Top:
                offsetPoint = new Point(connector.Position.X, rect.Top);
                break;

            case ConnectorOrientation.Right:
                offsetPoint = new Point(rect.Right, connector.Position.Y);
                break;

            case ConnectorOrientation.Bottom:
                offsetPoint = new Point(connector.Position.X, rect.Bottom);
                break;

            default:
                break;
        }

        return offsetPoint;
    }

    private static void CheckPathEnd(ConnectorInfo source, ConnectorInfo sink, bool showLastLine, List<Point> linePoints)
    {
        if (showLastLine)
        {
            var startPoint = new Point(0, 0);
            var endPoint = new Point(0, 0);
            const double marginPath = 1;

            switch (source.Orientation)
            {
                case ConnectorOrientation.Left:
                    startPoint = new Point(source.Position.X - marginPath, source.Position.Y);
                    break;

                case ConnectorOrientation.Top:
                    startPoint = new Point(source.Position.X, source.Position.Y - marginPath);
                    break;

                case ConnectorOrientation.Right:
                    startPoint = new Point(source.Position.X + marginPath, source.Position.Y);
                    break;

                case ConnectorOrientation.Bottom:
                    startPoint = new Point(source.Position.X, source.Position.Y + marginPath);
                    break;

                default:
                    break;
            }

            switch (sink.Orientation)
            {
                case ConnectorOrientation.Left:
                    endPoint = new Point(sink.Position.X - marginPath, sink.Position.Y);
                    break;

                case ConnectorOrientation.Top:
                    endPoint = new Point(sink.Position.X, sink.Position.Y - marginPath);
                    break;

                case ConnectorOrientation.Right:
                    endPoint = new Point(sink.Position.X + marginPath, sink.Position.Y);
                    break;

                case ConnectorOrientation.Bottom:
                    endPoint = new Point(sink.Position.X, sink.Position.Y + marginPath);
                    break;

                default:
                    break;
            }

            linePoints.Insert(0, startPoint);
            linePoints.Add(endPoint);
        }
        else
        {
            linePoints.Insert(0, source.Position);
            linePoints.Add(sink.Position);
        }
    }

    private static ConnectorOrientation GetOppositeOrientation(ConnectorOrientation connectorOrientation)
    {
        switch (connectorOrientation)
        {
            case ConnectorOrientation.Left:
                return ConnectorOrientation.Right;

            case ConnectorOrientation.Top:
                return ConnectorOrientation.Bottom;

            case ConnectorOrientation.Right:
                return ConnectorOrientation.Left;

            case ConnectorOrientation.Bottom:
                return ConnectorOrientation.Top;

            default:
                return ConnectorOrientation.Top;
        }
    }
}