﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Logic;

public class NodeSelection
{
    private readonly Interactions _interactions;
    public NodeSelection(Interactions interactions) => _interactions = interactions;
    public Action<ISelectable> NodeSelectionChanged { get; set; }
    public List<ISelectable> AllSelected { get; } = new();
    public ISelectable Selected => AllSelected?.LastOrDefault();
    public List<ISelectable> Highlighted { get; } = new();
    public bool IsPickMode { get; private set; }

    public void PickModeOn() => IsPickMode = true;
    public void PickModeOff() => IsPickMode = false;

    public void Select(ISelectable newSelected)
    {
        try
        {
            if (newSelected == null)
            {
                ResetSelection();
                return;
            }
            
            if (IsPickMode)
            {
                _Select(newSelected);
            }
            else
            {
                _Select(newSelected);
                Highlight(newSelected);
            }
        }
        finally
        {
            NodeSelectionChanged?.Invoke(newSelected);
            Expand(newSelected);
        }
    }

    private void _Select(ISelectable newSelected)
    {
        if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl))
            ResetSelection();

        newSelected.IsSelected = true;
        newSelected.ZIndex = 1;
        AllSelected.Add(newSelected);
    }
    
    private static void Expand(ISelectable newSelected)
    {
        if (newSelected is not NodeViewModel nodeViewModel)
            return;

        var parent = nodeViewModel.ParentNode;

        while (parent != null)
        {
            parent.IsExpanded = true;
            parent = parent.ParentNode;
        }
    }
    
    public void ResetSelection()
    {
        foreach (var highlighted in Highlighted)
        {
            highlighted.IsStrongHighlight = false;
            highlighted.IsWeekHighlight = false;
            highlighted.ZIndex = -1;
        }
            
        foreach (var selected in AllSelected)
        {
            selected.IsSelected = false;
            selected.ZIndex = -1;
        }

        Highlighted.Clear();
        AllSelected.Clear();
    }

    private void Highlight(ISelectable selected)
    {
        switch (selected)
        {
            case ConnectionViewModel connectionViewModel:
                HighlightConnectors(connectionViewModel);
                break;

            case NodeViewModel nodeViewModel:
                HighlightPathToSource(nodeViewModel);
                break;
        }
    }

    private void HighlightConnectors(ConnectionViewModel connectionViewModel)
    {
        SetStrongHighlight(connectionViewModel.SourceNode);
        SetStrongHighlight(connectionViewModel.TargetNode);
    }

    private void HighlightPathToSource(NodeViewModel nodeViewModel)
    {
        var connectedSources = nodeViewModel.Reference.GetAllConnectedSources();
        var pathNodes = new List<NodeViewModel>();
        var nextNode = GetNextNode(nodeViewModel, connectedSources, out var connectionViewModel);

        while (nextNode != null)
        {
            if (pathNodes.Contains(nextNode))
                break;

            pathNodes.Add(nextNode);
                
            SetWeekHighlight(nextNode);
            SetWeekHighlight(connectionViewModel);
                
            nextNode = GetNextNode(nextNode, connectedSources, out connectionViewModel);
        }
        
        var chain = pathNodes.Inverse().Select(x => x.Reference).Concat(new [] { nodeViewModel.Reference }).ToArray();
        _interactions.View.ElectricalDiagram.UpdateChainDiagram?.Invoke(chain);
    }

    private static NodeViewModel GetNextNode(
        NodeViewModel nodeViewModel,
        ICollection<ElectricalBase> sources,
        out ConnectionViewModel nextConnectionViewModel)
    {
        nextConnectionViewModel = null;
            
        foreach (var connectionViewModel in nodeViewModel.Connectors.SelectMany(n => n.Connections))
        {
            if (TryGetNextNode(connectionViewModel.TargetNode, sources, out var nextNode) ||
                TryGetNextNode(connectionViewModel.SourceNode, sources, out nextNode))
            {
                nextConnectionViewModel = connectionViewModel;
                return nextNode;
            }
            else if (sources.Contains(connectionViewModel.Reference))
            {
                nextConnectionViewModel = connectionViewModel;
                return connectionViewModel.SourceNode;
            }
        }

        return null;
    }
            
    private static bool TryGetNextNode(
        NodeViewModel candidateNodeViewModel,
        ICollection<ElectricalBase> sources,
        out NodeViewModel nodeViewModel)
    {
        var gotNextNode =
            candidateNodeViewModel != null &&
            sources.Contains(candidateNodeViewModel.Reference);

        nodeViewModel = gotNextNode ? candidateNodeViewModel : null;

        if (gotNextNode)
            sources.Remove(candidateNodeViewModel.Reference);

        return gotNextNode;
    }

    private void SetStrongHighlight(ISelectable selectable)
    {
        if (selectable == null)
            return;

        selectable.IsStrongHighlight = true;
        selectable.ZIndex = 1;
        Highlighted.Add(selectable);
    }

    private void SetWeekHighlight(ISelectable selectable)
    {
        if (selectable == null)
            return;
            
        selectable.IsWeekHighlight = true;
        selectable.ZIndex = 1;
        Highlighted.Add(selectable);
    }
}