﻿namespace CoLa.BimEd.UI.ElectricalDiagram.Logic;

public interface ISelectable
{
    object Reference { get; set; }
    bool IsEnabled { get; set; }
    bool IsSelected { get; set; }
    bool IsStrongHighlight { get; set; }
    bool IsWeekHighlight { get; set; }
    int ZIndex { get; set; }
}