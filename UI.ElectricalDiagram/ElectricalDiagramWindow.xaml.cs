﻿using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalDiagram;

public partial class ElectricalDiagramWindow
{
    public ElectricalDiagramWindow(DiagramViewModel viewModel)
    {
        InitializeComponent();
        this.SetPositionAndSizes(0.9, 0.9);
        viewModel.Close = Close;
        DataContext = viewModel;
    }
}