using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.OperatingModeSetting;
using CoLa.BimEd.UI.ElectricalSetting.Views.DistributionSystems;
using CoLa.BimEd.UI.ElectricalSetting.Views.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.Views.OperatingModeSetting;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.ElectricalSources;
using CoLa.BimEd.UI.EquipmentSelector.Views.Switchboards;
using CoLa.BimEd.UI.EquipmentSelector.Views.SwitchGears;
using CoLa.BimEd.UI.EquipmentSelector.Views.Transformers;
using CoLa.BimEd.UI.Framework.Dialogs;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class DataContextViewConverter : IValueConverter
{
    public const string Create = "Create";
    
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        null => null,
        DistributionSystemsViewModel => new DistributionSystemsView(),
        LoadClassificationsViewModel => new LoadClassificationsView(),
        OperatingModesViewModel => new OperatingModesView(),
        SelectItemDialogViewModel => new SelectItemDialogView(),
        SourceViewModel when parameter is Create => new CreateSourceView(),
        SourceViewModel => new SourceView(),
        SwitchBoardViewModel when parameter is Create => new CreateSwitchBoardView(),
        SwitchBoardDiagramViewModel => new SwitchboardDiagramView(),
        SwitchGearConstructorViewModel => new SwitchGearConstructor(),
        SwitchGearSeriesSelectorViewModel => new SwitchGearSeriesSelector(),
        TransformerViewModel => new TransformerView(),
        _ => value
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}