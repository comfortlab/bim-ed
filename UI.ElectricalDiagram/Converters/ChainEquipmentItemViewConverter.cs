﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.UI.ElectricalDiagram.Views.NodeDecorators.Chain;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class ChainEquipmentItemViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        ElectricalSource electricalNetwork => new ChainSourceItem(electricalNetwork),
        SwitchBoardUnit switchboardUnit => new ChainSwitchboardUnitItem(switchboardUnit),
        Transformer transformer => new ChainTransformerItem(transformer),
        _ => null
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}