using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.UI.ElectricalDiagram.Controls.Chain;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Chain;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class ChainConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not ChainElement chainNodeViewModel)
            return value;
        
        return chainNodeViewModel.Reference switch
        {
            ElectricalSystemProxy circuit => new ChainConnection(circuit)
            {
                // DataContext = new CircuitItemViewModel(circuit),
            },
            ElectricalEquipmentProxy when chainNodeViewModel.ChainLevel == ChainLevel.Secondary => new ChainSecondaryItem
            {
                DataContext = chainNodeViewModel,
            },
            ElectricalEquipmentProxy => new ChainMainItem
            {
                DataContext = chainNodeViewModel,
            },
            _ => null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}