﻿using System;
using System.Data;
using System.Globalization;
using System.Windows.Data;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class CellDataContextConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not DataRowView dataRowView ||
            parameter is not string @string || !int.TryParse(@string, out var index) ||
            index < 0 || dataRowView.Row.ItemArray.Length <= index)
            return null;
        
        var item = dataRowView.Row.ItemArray[index];
        
        return item is not null && item is not DBNull
            ? item
            : null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}