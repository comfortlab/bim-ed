using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Views.NodeDecorators.Compact;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class NodeViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            // Cubicle cubicle => new CompactCubicleNode();
            ElectricalSystemProxy electricalCircuit => new Connection(),
            ElectricalSource electricalNetwork => new CompactSourceNode(),
            SwitchBoard electricalPanel => new CompactSwitchboardNode(),
            SwitchBoardUnit switchboardUnit => new CompactSwitchboardNode(),
            Transformer transformer => new CompactTransformerNode(),
            _ => new CompactNode()
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}