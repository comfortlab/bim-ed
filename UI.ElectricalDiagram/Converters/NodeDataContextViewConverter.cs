﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Converters;

public class NodeDataContextViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        value is NodeViewModel nodeViewModel
            ? new Node { DataContext = nodeViewModel } :
            new Node();

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => value;
}