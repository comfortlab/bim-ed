﻿using System.Windows.Input;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Controls;

public partial class Node
{
    public Node()
    {
        InitializeComponent();
        this.PreviewMouseDown += OnPreviewMouseDown;
    }

    private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        base.OnMouseDown(e);

        if (DataContext is ISelectable { IsEnabled: true } selected)
        {
            DI.Get<NodeSelection>().Select(selected);
        }

        e.Handled = false;
    }

    private void OnPreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        if (DataContext is NodeViewModel { IsEnabled: true } nodeViewModel)
        {
            if (DI.Get<NodeSelection>().IsPickMode)
                DI.Get<Interactions>().View.ElectricalDiagram.PickNode?.Invoke(nodeViewModel);
            else
                nodeViewModel.Open();
        }

        e.Handled = false;
    }
}