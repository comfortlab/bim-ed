﻿using System.Windows.Input;

namespace CoLa.BimEd.UI.ElectricalDiagram.Controls.Chain;

public partial class ChainMainItem
{
    public ChainMainItem()
    {
        InitializeComponent();
        this.PreviewMouseDown += OnPreviewMouseDown;
        this.PreviewMouseDoubleClick += OnPreviewMouseDoubleClick;
    }

    private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        // base.OnMouseDown(e);
        //
        // if (DataContext is ISelectable { IsEnabled: true } selected)
        // {
        //     GraphSelection.GetInstance().Select(selected);
        // }
        //
        // e.Handled = false;
    }

    private void OnPreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        // if (DataContext is NodeViewModel { IsEnabled: true } nodeViewModel)
        // {
        //     if (nodeViewModel.IsPicking)
        //     {
        //         // MessageMediator.SendMessage<ViewMessage.Command.SelectNode>();
        //     }
        //     else
        //     {
        //         nodeViewModel.OpenNode();
        //     }
        // }
        //
        // e.Handled = false;
    }
}