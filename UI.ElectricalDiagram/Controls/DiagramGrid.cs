﻿using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Converters;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Controls;

public class DiagramGrid : Grid
{
    public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
        "ItemsSource", typeof(IEnumerable), typeof(DiagramGrid), new PropertyMetadata(default(IEnumerable), ItemsSourceChanged));

    public IEnumerable ItemsSource
    {
        get => (IEnumerable)GetValue(ItemsSourceProperty);
        set => SetValue(ItemsSourceProperty, value);
    }

    public DiagramGrid()
    {
        DataContextChanged += (_, args) =>
        {
            if (args.NewValue is not NodesGridViewModel diagramGridViewModel)
                return;

            diagramGridViewModel.Nodes.UpdatePositions = UpdatePositions;
        };

        Loaded += (_, _) =>
        {
            UpdatePositions();
        };
    }

    private void UpdatePositions()
    {
        UpdateLayout();
        
        foreach (NodeViewModel nodeViewModel in this.ItemsSource)
        {
            UpdatePosition(nodeViewModel);
        }
    }

    private void UpdatePosition(NodeViewModel nodeViewModel)
    {
        var columns = ColumnDefinitions.Take(nodeViewModel.ColumnIndex).ToArray();
        var rows = RowDefinitions.Take(nodeViewModel.RowIndex).ToArray();

        nodeViewModel.Left = columns.Sum(x => x.ActualWidth);
        nodeViewModel.Top = rows.Sum(x => x.ActualHeight);
        nodeViewModel.UpdateConnectors();
    }

    private static void ItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not DiagramGrid diagramGrid)
            return;

        if (e.NewValue is not IEnumerable enumerable)
            return;

        foreach (var element in enumerable)
            diagramGrid.AddNode(element);

        if (e.NewValue is not INotifyCollectionChanged notifyCollectionChanged)
            return;

        notifyCollectionChanged.CollectionChanged += (_, args) =>
        {
            foreach (var newItem in args.NewItems ?? new ArrayList())
                diagramGrid.AddNode(newItem);
            
            foreach (var oldItem in args.OldItems ?? new ArrayList())
                diagramGrid.DeleteNode(oldItem);
        };
    }

    private void AddNode(object element)
    {
        if (element is not NodeViewModel nodeViewModel)
            return;

        if (new NodeDataContextViewConverter().Convert(nodeViewModel, typeof(Node), null, CultureInfo.InvariantCulture) is
            not FrameworkElement node)
            return;

        nodeViewModel.PositionChanged += (_, _) => SetPosition(node, nodeViewModel);
        Children.Add(node);
        SetPosition(node, nodeViewModel);
    }

    private void SetPosition(FrameworkElement node, NodeViewModel nodeViewModel)
    {
        UpdateGridSize(nodeViewModel.ColumnIndex, nodeViewModel.RowIndex);
        SetColumn(node, nodeViewModel.ColumnIndex);
        SetRow(node, nodeViewModel.RowIndex);
    }

    private void UpdateGridSize(int columnIndex, int rowIndex)
    {
        while (ColumnDefinitions.Count <= columnIndex)
            ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Auto) });

        while (RowDefinitions.Count <= rowIndex)
            RowDefinitions.Add(new RowDefinition { Height = new GridLength(0, GridUnitType.Auto) });
    }

    private void DeleteNode(object element)
    {
        if (element is not NodeViewModel nodeViewModel)
            return;

        var node = Children.OfType<FrameworkElement>().FirstOrDefault(x => x.DataContext == nodeViewModel);
        
        if (node != null)
            Children.Remove(node);
    }
}