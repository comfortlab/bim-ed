﻿namespace CoLa.BimEd.UI.ElectricalDiagram.Controls;

public enum ConnectorOrientation
{
    None,
    Left,
    Top,
    Right,
    Bottom
}