﻿using System.Windows.Input;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;

namespace CoLa.BimEd.UI.ElectricalDiagram.Controls;

public partial class Connection
{
    public Connection()
    {
        InitializeComponent();
        this.MouseDown += OnMouseDown;
    }

    private void OnMouseDown(object sender, MouseButtonEventArgs e)
    {
        base.OnMouseDown(e);

        if (DataContext is ISelectable selected)
            DI.Get<NodeSelection>().Select(selected);

        e.Handled = false;
    }
}