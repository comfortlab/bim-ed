﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CoLa.BimEd.UI.ElectricalDiagram.Controls;

public class Connector : Control
{
    public static readonly DependencyProperty PositionProperty = DependencyProperty
        .Register("Position", typeof(Point), typeof(Connector),
            new FrameworkPropertyMetadata(default(Point), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    public Point Position
    {
        get => (Point)GetValue(PositionProperty);
        set => SetValue(PositionProperty, value);
    }

    public Connector()
    {
        LayoutUpdated += Connector_LayoutUpdated;
        Focusable = false;
    }

    private void Connector_LayoutUpdated(object sender, EventArgs e)
    {
        var graphCanvas = GetCanvas(this);
            
        if (graphCanvas != null)
            this.Position = this.TransformToAncestor(graphCanvas)
                .Transform(new Point(this.Width / 2, this.Height / 2));
    }

    private static Canvas GetCanvas(DependencyObject element)
    {
        while (element != null && element is not Canvas)
            element = VisualTreeHelper.GetParent(element);

        return element as Canvas;
    }
}