﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Chain;

public class ChainElement
{
    public ChainElement(ElectricalBase reference, ChainLevel chainLevel)
    {
        Reference = reference;
        ChainLevel = chainLevel;
    }
    
    public ChainElement(ElectricalBase reference)
    {
        Reference = reference;
        ChainLevel = Reference switch
        {
            ElectricalSystemProxy => ChainLevel.Circuit,
            _ => ChainLevel.Main,
        };
    }
    
    public ElectricalBase Reference { get; }
    public ChainLevel ChainLevel { get; set; }
    public bool IsFirst { get; set; }
    public bool IsLast { get; set; }
    public bool IsSelected => IsLast; // Applied in the DataTrigger of NodeBorderTemplate.
}