namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Chain;

public enum ChainLevel
{
    Circuit,
    Main,
    Secondary
}