﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class ConnectionViewModel : ViewModel, ISelectable
{
    private const double Radius = 10;

    public ElectricalSystemProxy ElectricalSystem { get; set; }
    public NodeViewModel SourceNode => SourceConnector?.Owner;
    public NodeViewModel TargetNode => TargetConnector?.Owner;
    public ConnectorViewModel SourceConnector { get; set; }
    public ConnectorViewModel TargetConnector { get; set; }
    public PathGeometry PathGeometry { get; set; }
    public object Reference { get; set; }
    public bool IsEnabled { get; set; } = true;
    public bool IsHalftone { get; set; }
    public bool IsSelected { get; set; }
    public bool IsStrongHighlight { get; set; }
    public bool IsWeekHighlight { get; set; }
    public bool Visible { get; set; } = true;
    public int ZIndex { get; set; }

    public static ConnectionViewModel CreateFromRightToLeft(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode)
    {
        var sourceConnector = sourceNode.RightConnector;
        var targetConnector = targetNode.LeftConnector;

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel CreateFromBottomToTop(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode)
    {
        var sourceConnector = sourceNode.BottomConnector;
        var targetConnector = targetNode.TopConnector;

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel CreateFromTopToBottom(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode)
    {
        var sourceConnector = sourceNode.TopConnector;
        var targetConnector = targetNode.BottomConnector;

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel CreateHorizontal(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode)
    {
        var sourceConnector = sourceNode.Left < targetNode.Left ? sourceNode.RightConnector : sourceNode.LeftConnector;
        var targetConnector = sourceNode.Left < targetNode.Left ? targetNode.LeftConnector : targetNode.RightConnector;

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel CreateVertical(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode)
    {
        var sourceConnector = sourceNode.Top < targetNode.Top ? sourceNode.BottomConnector : sourceNode.TopConnector;
        var targetConnector = sourceNode.Top < targetNode.Top ? targetNode.TopConnector : targetNode.BottomConnector;

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel CreateOneSide(
        ElectricalSystemProxy electricalSystem,
        NodeViewModel sourceNode,
        NodeViewModel targetNode,
        ConnectorOrientation connectorOrientation)
    {
        ConnectorViewModel sourceConnector;
        ConnectorViewModel targetConnector;

        switch (connectorOrientation)
        {
            case ConnectorOrientation.Left:
                sourceConnector = sourceNode.LeftConnector;
                targetConnector = targetNode.LeftConnector;
                break;

            case ConnectorOrientation.Top:
                sourceConnector = sourceNode.TopConnector;
                targetConnector = targetNode.TopConnector;
                break;

            case ConnectorOrientation.Right:
                sourceConnector = sourceNode.RightConnector;
                targetConnector = targetNode.RightConnector;
                break;

            case ConnectorOrientation.Bottom:
                sourceConnector = sourceNode.BottomConnector;
                targetConnector = targetNode.BottomConnector;
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(connectorOrientation), connectorOrientation, null);
        }

        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    public static ConnectionViewModel Create(
        ElectricalSystemProxy electricalSystem,
        ConnectorViewModel sourceConnector,
        ConnectorViewModel targetConnector)
    {
        return new ConnectionViewModel(electricalSystem, sourceConnector, targetConnector);
    }

    private ConnectionViewModel(
        ElectricalSystemProxy electricalSystem,
        ConnectorViewModel sourceConnector,
        ConnectorViewModel targetConnector)
    {
        Reference = ElectricalSystem = electricalSystem;
        SourceConnector = sourceConnector;
        TargetConnector = targetConnector;

        SourceConnector.Connections.Add(this);
        TargetConnector.Connections.Add(this);

        SourceConnector.CheckConnection();
        TargetConnector.CheckConnection();

        UpdatePathGeometry();
    }

    private void OnIsSelectedChanged()
    {
        if (SourceConnector != null)
        {
            SourceConnector.IsSelected = IsSelected;
            SelectReferenceConnections(SourceConnector);
        }

        if (TargetConnector != null)
        {
            TargetConnector.IsSelected = IsSelected;
            SelectReferenceConnections(TargetConnector);
        }
    }

    private void SelectReferenceConnections(ConnectorViewModel connector)
    {
        foreach (var connection in connector.Connections.Where(x => x != this && x.Reference == this.Reference))
        {
            connection.IsSelected = IsSelected;
            connection.SourceNode.IsStrongHighlight = IsSelected;
            connection.TargetNode.IsStrongHighlight = IsSelected;
        }
    }

    private void OnIsStrongHighlightChanged()
    {
        if (SourceConnector != null) SourceConnector.IsStrongHighlight = IsStrongHighlight;
        if (TargetConnector != null) TargetConnector.IsStrongHighlight = IsStrongHighlight;
    }

    private void OnIsWeekHighlightChanged()
    {
        if (SourceConnector != null) SourceConnector.IsWeekHighlight = IsWeekHighlight;
        if (TargetConnector != null) TargetConnector.IsWeekHighlight = IsWeekHighlight;
    }

    public void ConnectSource(ConnectorViewModel sourceConnector)
    {
        SourceConnector?.Connections.Remove(this);
        SourceConnector = sourceConnector;
        SourceConnector.Connections.Add(this);
        SourceConnector.CheckConnection();

        UpdatePathGeometry();
    }

    public void ConnectTarget(ConnectorViewModel targetConnector)
    {
        TargetConnector?.Connections.Remove(this);
        TargetConnector = targetConnector;
        TargetConnector.Connections.Add(this);
        TargetConnector.CheckConnection();

        UpdatePathGeometry();
    }
    
    internal void UpdatePathGeometry()
    {
        if (SourceConnector == null || TargetConnector == null)
            return;

        var linePoints = PathFinder.GetConnectionLine(SourceConnector.GetInfo(), TargetConnector.GetInfo(), true, SourceConnector.Owner, TargetConnector.Owner);

        if (linePoints.Count < 2)
            return;

        var simplifiedLinePoints = GetSimplifiedLine(linePoints);

        if (simplifiedLinePoints.Count < 2)
            return;

        PathGeometry = new PathGeometry { Figures = { CreatePathFigure(simplifiedLinePoints) } };
    }
        
    private static List<Point> GetSimplifiedLine(List<Point> linePoints)
    {
        var simplifiedLinePoints = new List<Point>();

        var previousPoint = linePoints[0];
        var currentPoint = linePoints[1];

        for (var i = 2; i < linePoints.Count; i++)
        {
            var nextPoint = linePoints[i];

            if (Math.Abs(previousPoint.X - currentPoint.X) < 1e-3 &&
                Math.Abs(currentPoint.X - nextPoint.X) < 1e-3)
            {
                currentPoint = nextPoint;
            }
            else if (Math.Abs(previousPoint.Y - currentPoint.Y) < 1e-3 &&
                     Math.Abs(currentPoint.Y - nextPoint.Y) < 1e-3)
            {
                currentPoint = nextPoint;
            }
            else
            {
                simplifiedLinePoints.Add(previousPoint);
                previousPoint = currentPoint;
                currentPoint = nextPoint;
            }
        }

        simplifiedLinePoints.Add(previousPoint);
        simplifiedLinePoints.Add(currentPoint);

        return simplifiedLinePoints;
    }

    private static PathFigure CreatePathFigure(List<Point> linePoints)
    {
        var figure = new PathFigure { StartPoint = linePoints[0] };

        for (var i = 1; i < linePoints.Count - 1; i++)
        {
            var v1 = linePoints[i] - linePoints[i - 1];
            var v2 = linePoints[i + 1] - linePoints[i];
            var length1 = v1.Length;

            double radius;

            if (i == 1)
            {
                radius = Math.Min(Math.Min(v1.Length * 0.8, v2.Length / 2), Radius);
            }
            else if (i + 2 == linePoints.Count)
            {
                radius = Math.Min(Math.Min(v1.Length / 2, v2.Length * 0.8), Radius);
            }
            else
            {
                radius = Math.Min(Math.Min(v1.Length, v2.Length) / 2, Radius);
            }

            v1.Normalize();
            v2.Normalize();

            if (v1 == v2)
            {
                figure.Segments.Add(new LineSegment(linePoints[i], true));
            }
            else
            {
                v1 *= length1 - radius;
                v2 *= radius;

                figure.Segments.Add(new LineSegment(linePoints[i - 1] + v1, true));

                var arcPoint = linePoints[i] + v2;
                var arcSize = new Size(radius, radius);
                var arcDirection = Vector.AngleBetween(v1, v2) > 0
                    ? SweepDirection.Clockwise
                    : SweepDirection.Counterclockwise;

                figure.Segments.Add(new ArcSegment(arcPoint, arcSize, 0, false, arcDirection, true));
            }
        }

        figure.Segments.Add(new LineSegment(linePoints[linePoints.Count - 1], true));

        return figure;
    }
}