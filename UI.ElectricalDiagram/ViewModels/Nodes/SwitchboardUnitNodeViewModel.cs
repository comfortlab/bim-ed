﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

public sealed class SwitchboardUnitNodeViewModel : NodeViewModel
{
    public SwitchboardUnitNodeViewModel(SwitchBoardUnit switchBoardUnit, NodeViewModel parentNode = null) :
        base(switchBoardUnit, parentNode)
    {
        UpdateProperties();
    }
    
    public double EstimateCurrent { get; set; }
    public double ShortCurrent1 { get; set; }
    public double ShortCurrent3 { get; set; }
    public double SurgeShortCurrent3 { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double VoltageDrop { get; set; }
    public double VoltageDropPercent { get; set; }

    public override void UpdateProperties()
    {
        var estimatedPowerParameters = Reference.EstimatedPowerParameters;
            
        EstimateCurrent = estimatedPowerParameters.Current;
        EstimateTrueLoad = estimatedPowerParameters.EstimateTrueLoad;
        ShortCurrent1 = estimatedPowerParameters.ShortCurrent1;
        ShortCurrent3 = estimatedPowerParameters.ShortCurrent3;
        SurgeShortCurrent3 = estimatedPowerParameters.SurgeShortCurrent3;
        VoltageDrop = estimatedPowerParameters.VoltageDrop;
        VoltageDropPercent = estimatedPowerParameters.VoltageDropPercent;
        IsHalftone = EstimateTrueLoad < 1e-3;
        base.UpdateProperties();
    }
}