﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

public sealed class SourceNodeViewModel : NodeViewModel
{
    public SourceNodeViewModel(ElectricalSource electricalSource) :
        base(electricalSource)
    {
        ElectricalSource = electricalSource;
        UpdateProperties();
    }
    
    public ElectricalSource ElectricalSource { get; set; }
    public bool IsGenerator { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double Voltage { get; set; }
    
    public override void UpdateProperties()
    {
        IsGenerator = ElectricalSource is ElectricalGenerator;
            
        if (Reference.IsPower == false)
            return;
            
        EstimateTrueLoad = Reference.EstimatedPowerParameters.EstimateTrueLoad;
        Voltage = ElectricalSource.DistributionSystem.LineToLineVoltage?.ActualValue ??
                  ElectricalSource.DistributionSystem.LineToGroundVoltage?.ActualValue ?? 0;
        
        IsHalftone = EstimateTrueLoad < 1e-3;
        
        base.UpdateProperties();
    }
}