﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

public sealed class TransformerNodeViewModel : NodeViewModel
{
    public TransformerNodeViewModel(Transformer transformer, NodeViewModel parentNode) :
        base(transformer, parentNode)
    {
        Transformer = transformer;
        UpdateProperties();
    }
    
    public Transformer Transformer { get; set; }
    public double PrimaryVoltage { get; set; }
    public double SecondaryVoltage { get; set; }
    public double RatedPower { get; set; }
    public double EstimateApparentLoad { get; set; }

    public override void UpdateProperties()
    {
        PrimaryVoltage = Transformer.DistributionSystem?.LineToLineVoltage?.ActualValue ?? 0;
        SecondaryVoltage = Transformer.SecondaryDistributionSystem?.LineToLineVoltage?.ActualValue ?? 0;
        RatedPower = Transformer.TransformerProduct.RatedPower;
        EstimateApparentLoad = Reference.EstimatedPowerParameters.EstimateApparentLoad;
        IsHalftone = EstimateApparentLoad < 1e-3;
        base.UpdateProperties();
    }
}