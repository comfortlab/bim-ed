﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

public class NodeViewModel : ViewModel, ISelectable
{
    public event EventHandler PositionChanged;

    public const double DefaultHeight = 42;
    public const double DefaultWidth = 120;
    public const double DefaultVerticalConnectorMargin = 15;

    public readonly ConnectorViewModel[] Connectors;
    // internal readonly GraphDesigner GraphDesigner;
    private int _columnIndex;
    private int _rowIndex;

    public NodeViewModel(ElectricalBase reference, NodeViewModel parentNode = null)
    {
        Reference = reference;
        Guid = Reference.Guid;
        Name = Reference.Name;

        Height = DefaultHeight;
        Width = DefaultWidth;
            
        LeftConnector = new ConnectorViewModel(this, ConnectorOrientation.Left);
        TopConnector = new ConnectorViewModel(this, ConnectorOrientation.Top);
        RightConnector = new ConnectorViewModel(this, ConnectorOrientation.Right);
        BottomConnector = new ConnectorViewModel(this, ConnectorOrientation.Bottom);

        Connectors = new[]
        {
            LeftConnector,
            TopConnector,
            RightConnector,
            BottomConnector
        };

        // UpdateProperties();
        // UpdateConnectors();

        UpdateExpandable();
        // Reference.GetAllConsumersOf<ElectricalEquipment>().All(n => n.EquipmentGroup == null);

        parentNode?.AddChildNode(this);
    }

    public static NodeViewModel Create(ElectricalEquipmentProxy electricalEquipment, NodeViewModel parentNode)
    {
        return electricalEquipment switch
        {
            // Cubicle cubicle => new CubicleNodeViewModel(this, cubicle, parentNode),
            SwitchBoardUnit switchboardUnit => new SwitchboardUnitNodeViewModel(switchboardUnit, parentNode),
            Transformer transformer => new TransformerNodeViewModel(transformer, parentNode),
            _ => new NodeViewModel(electricalEquipment, parentNode)
        };
    }

    public ElectricalBase Reference { get; set; }

    object ISelectable.Reference
    {
        get => Reference;
        set => Reference = value as ElectricalBase;
    }

    public Guid Guid { get; private set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public Size ActualSize { get; set; }
    public double Height { get; set; }
    public double Width { get; set; }
    public double Left { get; set; }
    public double Top { get; set; }
    public bool IsUpdated { get; set; }
    public double TreeHeight { get; set; } = DefaultHeight;
    public double TreeWidth { get; set; } = DefaultWidth;
    public NodeGroupViewModel GroupViewModel { get; set; }
    public NodeViewModel ParentNode { get; set; }
    public ConnectionViewModel ParentConnection { get; set; }
    public List<NodeViewModel> ChildNodes { get; set; } = new();
    public ConnectorViewModel LeftConnector { get; set; }
    public ConnectorViewModel TopConnector { get; set; }
    public ConnectorViewModel RightConnector { get; set; }
    public ConnectorViewModel BottomConnector { get; set; }
    public bool Expandable { get; set; }
    public bool IsEnabled { get; set; } = true;
    public bool IsHalftone { get; set; }
    public bool IsExpanded { get; set; }
    public bool IsPickMode { get; set; }
    public bool IsSelected { get; set; }
    public bool IsStrongHighlight { get; set; }
    public bool IsWeekHighlight { get; set; }
    public bool Visible { get; set; } = true;

    public int ColumnIndex
    {
        get => _columnIndex;
        set
        {
            _columnIndex = value;
            PositionChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public int RowIndex
    {
        get => _rowIndex;
        set
        {
            _rowIndex = value;
            PositionChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public int ZIndex { get; set; }
    
    public Action<NodeViewModel> Expand { get; set; }
    public Action<NodeViewModel> Collapse { get; set; }
    public Command OpenCommand => new(Open);
    public Command ExpandCommand => new(_Expand);
    public Command CollapseCommand => new(_Collapse);

    private void _Expand()
    {
        Expand?.Invoke(this);
        IsExpanded = true;
    }

    private void _Collapse()
    {
        Collapse?.Invoke(this);
        IsExpanded = false;
    }

    private void OnNameChanged()
    {
        if (Reference != null)
            Reference.Name = Name;
    }

    private void OnIsSelectedChanged()
    {
        if (GroupViewModel != null)
            GroupViewModel.IsSelected = IsSelected;
    }

    private void OnIsStrongHighlightChanged()
    {
        if (GroupViewModel != null)
            GroupViewModel.IsStrongHighlight = IsStrongHighlight;
    }

    private void OnIsWeekHighlightChanged()
    {
        if (GroupViewModel != null)
            GroupViewModel.IsWeekHighlight = IsWeekHighlight;
    }

    private void OnActualSizeChanged() => UpdateConnectors();

    public void AddChildNode(NodeViewModel childNode)
    {
        if (!ChildNodes.Contains(childNode))
            ChildNodes.Add(childNode);
        
        childNode.ParentNode = this;
        Expandable = true;
    }

    public void RemoveChildNode(NodeViewModel childNode)
    {
        ChildNodes.Remove(childNode);
        childNode.ParentNode = null;
        Expandable = ChildNodes.Any();
    }
    
    public void Open() => DI.Get<Interactions>().View.ElectricalDiagram.ShowElectricalEquipmentDetails(Reference);

    public virtual void UpdateProperties()
    {
        Name = Reference?.Name;
            
        foreach (var connector in Connectors)
        {
            foreach (var connection in connector.Connections
                         .Where(c => c.SourceConnector == connector))
            {
                connection.IsHalftone = IsHalftone;
            }
        }
    }

    internal void UpdateConnectors()
    {
        LeftConnector.Position = new Point(Left, Top + ActualSize.Height / 2);
        TopConnector.Position = new Point(Left + DefaultVerticalConnectorMargin /*ActualSize.Width / 2*/, Top);
        RightConnector.Position = new Point(Left + ActualSize.Width, Top + ActualSize.Height / 2);
        BottomConnector.Position = new Point(Left + DefaultVerticalConnectorMargin /*ActualSize.Width / 2*/, Top + ActualSize.Height);

        foreach (var connector in Connectors)
        {
            foreach (var connection in connector.Connections)
            {
                connection.UpdatePathGeometry();
            }
        }
    }

    public void UpdateExpandable()
    {
        Expandable =
            Reference is not ElectricalSource &&
            Reference.GetFirstConsumersOf<ElectricalEquipmentProxy>().Any() &&
            Reference.GetAllConsumersOf<SwitchBoardUnit>().All(n => n.SwitchBoard == null || n.SwitchBoard.Units.Count < 2);
    }
    
    public bool IsBelowThenBranchOf(NodeViewModel otherNode)
    {
        if (this.ColumnIndex == otherNode.ColumnIndex)
            return this.RowIndex > otherNode.RowIndex;

        if (this.ColumnIndex < otherNode.ColumnIndex &&
            this.RowIndex > otherNode.RowIndex)
            return true;
        
        if (this.ColumnIndex > otherNode.ColumnIndex &&
            this.RowIndex < otherNode.RowIndex)
            return false;

        if (this.ColumnIndex < otherNode.ColumnIndex)
        {
            var parentOtherNode = otherNode.ParentNode;
            
            while (this.ColumnIndex < parentOtherNode.ColumnIndex && parentOtherNode.ParentNode != null)
                parentOtherNode = parentOtherNode.ParentNode;

            return this.RowIndex > parentOtherNode.RowIndex;
        }
        else
        {
            var parentNode = this.ParentNode;
            
            while (otherNode.ColumnIndex < parentNode.ColumnIndex && parentNode.ParentNode != null)
                parentNode = parentNode.ParentNode;

            return parentNode.RowIndex > otherNode.RowIndex;
        }
    }
}