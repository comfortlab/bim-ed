﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

public class NodeGroupViewModel : ViewModel
{
    public const double LeftOffset = 21;
    public const double RightOffset = 20;
    public const double TopOffset = 26;
    public const double BottomOffset = 10;

    // public IEquipmentGroup EquipmentGroup { get; }
    public SwitchBoard SwitchBoard { get; }
    public string Name { get; set; }
    public int MaxLevel { get; set; }
    public List<NodeViewModel> NodeViewModels { get; set; }
    public List<NodeViewModel> CyclicInputNodes { get; set; }
    public List<NodeGroupViewModel> CyclicGroups { get; set; }
    public double Height { get; set; }
    public double Width { get; set; }
    public double Left { get; set; }
    public double Top { get; set; }
    public double TreeHeight { get; set; }
    public double TreeWidth { get; set; }
    public bool IsUpdated { get; set; }
    public bool IsSelected { get; set; }
    public bool IsStrongHighlight { get; set; }
    public bool IsWeekHighlight { get; set; }

    public void Add(NodeViewModel nodeViewModel)
    {
        NodeViewModels.Add(nodeViewModel);
        nodeViewModel.GroupViewModel = this;
    }
}