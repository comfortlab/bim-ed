using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Utils;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class NodesGridViewModel : ViewModel
{
    public NodesGridViewModel(
        List<ElectricalNetwork> networks,
        NodeSelection nodeSelection,
        VirtualElementsUtils virtualElementsUtils,
        Interactions interactions)
    {
        NodeSelection = nodeSelection;
        NodeSelection.NodeSelectionChanged = NodeOnSelectionChanged;
        Nodes = new NodesCollection(virtualElementsUtils);

        foreach (var network in networks.OrderByDescending(x =>
                     x.DistributionSystem?.LineToLineVoltage?.ActualValue ??
                     x.DistributionSystem?.LineToGroundVoltage?.ActualValue ?? 0))
            Nodes.CreateRoot(network);
        
        interactions.View.ElectricalDiagram.ShowElectricalEquipmentDetails = _ => { };
        interactions.View.ElectricalDiagram.PickNode = _ => OkPickCommand.Execute(null);
    }

    public NodesCollection Nodes { get; set; }
    public NodeSelection NodeSelection { get; set; }
    public bool CanConnect { get; set; }
    public bool CanDisconnect { get; set; }

    private void NodeOnSelectionChanged(ISelectable selectable)
    {
        var reference = selectable.Reference as ElectricalBase;
        var source = reference?.GetFirstSourceOf<ElectricalEquipmentProxy>();
        
        CanConnect = reference != null && reference is not ElectricalSource;
        CanDisconnect = source != null && source is not ElectricalSource;
    }

    public void Select(ElectricalBase electrical)
    {
        if (electrical is ElectricalEquipmentProxy electricalEquipment &&
            Nodes.TryGetNode(electricalEquipment, out var node))
            NodeSelection.Select(node);
    }

    #region Pick Mode
    
    public Action<ElectricalBase> OkPickCallback { get; set; }
    public bool IsPickMode { get; set; }
    public string Message { get; set; }
    public Command OkPickCommand => new(() =>
    {
        try { OkPickCallback?.Invoke(NodeSelection.Selected?.Reference as ElectricalBase); }
        finally { PickModeOff(); }
    });
    public Command CancelPickCommand => new(PickModeOff);

    internal void PickModeOn(string message) => PickModeOn(message, Array.Empty<NodeViewModel>());
    internal void PickModeOn(
        string message,
        ElectricalEquipmentProxy compatibleEquipment)
    {
        try
        {
            var allowedNodes = Nodes.GetAllowedNodes(compatibleEquipment);
            PickModeOn(message, allowedNodes);
        }
        catch (Exception exception)
        {
            IsPickMode = false;
            Debug.WriteLine(exception);
        }
    }
    internal void PickModeOn(
        string message,
        IReadOnlyCollection<NodeViewModel> allowedNodes)
    {
        try
        {
            Message = message;
            
            var startSelected = GetStartSelected(allowedNodes);
        
            IsPickMode = true;
            NodeSelection.PickModeOn();
            NodeSelection.ResetSelection();
            
            if (startSelected != null)
                NodeSelection.Select(startSelected);

            Nodes.ForEach(n =>
            {
                n.IsEnabled = allowedNodes.Contains(n);
                n.IsPickMode = true;
            });
        }
        catch (Exception exception)
        {
            IsPickMode = false;
            Debug.WriteLine(exception);
        }
    }

    private void PickModeOff()
    {
        try
        {
            Nodes.ForEach(n =>
            {
                n.IsEnabled = true;
                n.IsPickMode = false;
            });
        }
        finally
        {
            NodeSelection.PickModeOff();
            IsPickMode = false;
        }
    }

    private NodeViewModel GetStartSelected(IReadOnlyCollection<NodeViewModel> allowedNodes)
    {
        var startSelected = NodeSelection.Selected as NodeViewModel;

        if (!allowedNodes.Contains(startSelected))
            startSelected = allowedNodes.Count == 1 ? allowedNodes.ElementAt(0) : null;
        
        return startSelected;
    }
    
    #endregion Pick Mode
}