using System;
using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.DistributionSystems;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.LoadClassifications;
using CoLa.BimEd.UI.ElectricalSetting.ViewModels.OperatingModeSetting;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.Switchboards;
using CoLa.BimEd.UI.EquipmentSelector.ViewModels.SwitchGears;
using CoLa.BimEd.UI.Framework;
using CoLa.BimEd.UI.Framework.Dialogs;
using CoLa.BimEd.UI.Framework.MVVM;
using CoLa.BimEd.UI.Resources;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class DiagramViewModel : ViewModel
{
    private readonly IServiceProvider _serviceProvider;
    private readonly Repository<OperatingModes> _operatingModes;
    private readonly Repository<ElectricalSystemProxy> _electricalSystems;
    private readonly Repository<VirtualElements> _virtualElementsRepository;
    private readonly VirtualElements _virtualElements;
    private readonly Interactions.ViewInteraction _viewInteraction;

    public DiagramViewModel(
        IServiceProvider serviceProvider,
        Repository<ElectricalSystemProxy> electricalSystems,
        Repository<OperatingModes> operatingModes,
        Repository<VirtualElements> virtualElementsRepository,
        ChainDiagramViewModel chainDiagramViewModel,
        NodesGridViewModel nodesGridViewModel,
        Interactions interactions)
    {
        _serviceProvider = serviceProvider;
        _electricalSystems = electricalSystems;
        _operatingModes = operatingModes;
        _virtualElementsRepository = virtualElementsRepository;
        _virtualElements = _virtualElementsRepository.First();
        ChainDiagramViewModel = chainDiagramViewModel;
        NodesGridViewModel = nodesGridViewModel;
        
        UpdateOperatingModes();
        
        _viewInteraction = interactions.View;
        _viewInteraction.ElectricalDiagram.ConnectNode = Connect;
        _viewInteraction.ElectricalDiagram.ShowElectricalEquipmentDetails = EditElectricalEquipment;
        _viewInteraction.ElectricalDiagram.UpdateNode = NodesGridViewModel.Nodes.UpdateNode;
    }

    public ViewModel MainDialogViewModel { get; set; }
    public ViewModel DiagramDialogViewModel { get; set; }
    public ChainDiagramViewModel ChainDiagramViewModel { get; set; }
    public NodesGridViewModel NodesGridViewModel { get; set; }
    public OperatingModes OperatingModes { get; set; }
    public OperatingMode SelectedOperatingMode { get; set; }

    public Command ConnectCommand => new(Connect);
    public Command CreateSourceCommand => new(CreateSource);
    public Command CreateSwitchBoardCommand => new(CreateSwitchBoard);
    public Command CreateSwitchGearCommand => new(CreateSwitchGear);
    public Command CreateTransformerCommand => new(CreateTransformer);
    public Command DisconnectCommand => new(Disconnect);
    public Command DistributionSystemSettingCommand => new(EditDistributionSystems);
    public Command LoadClassificationsCommand => new(EditLoadClassifications);
    public Command OperatingModesCommand => new(EditOperatingModes);
    public Command SelectAllEquipmentCommand => new();

    // ReSharper disable UnusedMember.Local
    private void OnSelectedOperatingModeChanged()
    {
        if (OperatingModes == null)
            return;
        
        OperatingModes.Current = SelectedOperatingMode;
        _operatingModes.Save();

        // if (SelectedOperatingMode != null)
        //     DI.Get<ICurrentOperatingMode>().SetValue(SelectedOperatingMode);

        // MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraphProperties>();
        // MessageMediator.SendMessage<ViewMessage.Update.ElectricalGraphChain>();
        // MessageMediator.SendMessage<ViewMessage.Update.SwitchboardDiagram>();
        // // CurrentOperatingMode.Instance.SetValue(SelectedOperatingMode);
    }

    private void CreateSource()
    {
        MainDialogViewModel = _serviceProvider.Get<SourceViewModel>();
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
        MainDialogViewModel.OkCallback = @object =>
        {
            if (@object is ElectricalSource electricalSource)
                NodesGridViewModel.Nodes.CreateRoot(electricalSource);
        };
    }

    private void CreateSwitchBoard()
    {
        MainDialogViewModel = new SwitchBoardViewModel(_serviceProvider);
        MainDialogViewModel.OkCallback = CreateEquipment;
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void CreateSwitchGear()
    {
        MainDialogViewModel = _serviceProvider.Get<SwitchGearSeriesSelectorViewModel>();
        MainDialogViewModel.OkCallback = @object =>
        {
            if (@object is not SwitchGearSeries switchGearSeries)
                return;
            
            MainDialogViewModel = new SwitchGearConstructorViewModel(_serviceProvider, switchGearSeries);
            MainDialogViewModel.Close = () => MainDialogViewModel = null;
        };
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void CreateTransformer()
    {
        MainDialogViewModel = new TransformerViewModel(_serviceProvider);
        MainDialogViewModel.OkCallback = CreateEquipment;
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void EditDistributionSystems()
    {
        MainDialogViewModel = _serviceProvider.Get<DistributionSystemsViewModel>();
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void EditElectricalEquipment(object @object)
    {
        switch (@object)
        {
            case ElectricalSource electricalSource: EditElectricalSource(electricalSource); break;
            case SwitchBoardUnit switchboardUnit: EditSwitchboardUnit(switchboardUnit); break;
            case Transformer transformer: EditTransformer(transformer); break;
        }
    }

    private void EditElectricalSource(ElectricalSource electricalSource)
    {
        DiagramDialogViewModel = new SourceViewModel(_serviceProvider, electricalSource)
        {
            OkCallback = _ => _viewInteraction.ElectricalDiagram.UpdateNode(electricalSource),
            DeleteCallback = () => NodesGridViewModel.Nodes.DeleteNode(electricalSource),
            Close = () => DiagramDialogViewModel = null,
        };
    }

    private void EditSwitchboardUnit(SwitchBoardUnit switchBoardUnit)
    {
        DiagramDialogViewModel = new SwitchBoardDiagramViewModel(_serviceProvider, switchBoardUnit)
        {
            OkCallback = EditEquipment,
            // DeleteCallback = () => NodesGridViewModel.Nodes.DeleteNode(switchboardUnit),
            Close = () => DiagramDialogViewModel = null,
        };
    }

    private void EditTransformer(Transformer transformer)
    {
        DiagramDialogViewModel = new TransformerViewModel(_serviceProvider, transformer)
        {
            OkCallback = EditEquipment,
            DeleteCallback = () => NodesGridViewModel.Nodes.DeleteNode(transformer),
            Close = () => DiagramDialogViewModel = null,
        };
    }

    private void EditLoadClassifications()
    {
        MainDialogViewModel = _serviceProvider.Get<LoadClassificationsViewModel>();
        MainDialogViewModel.OkCallback = _ => UpdateOperatingModes();
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void EditOperatingModes()
    {
        var operatingModeElements = _serviceProvider.Get<OperatingModeElements>();
        operatingModeElements.Elements.ReplaceAll(_serviceProvider.Get<Repository<LoadClassificationProxy>>());
        MainDialogViewModel = _serviceProvider.Get<OperatingModesViewModel>();
        MainDialogViewModel.OkCallback = _ => UpdateOperatingModes();
        MainDialogViewModel.Close = () => MainDialogViewModel = null;
    }

    private void CreateEquipment(object @object)
    {
        if (@object is not ElectricalEquipmentProxy electricalEquipment)
            return;

        var allowedNodes = NodesGridViewModel.Nodes.GetAllowedNodes(electricalEquipment);

        NodesGridViewModel.OkPickCallback = reference =>
        {
            if (reference is not ElectricalEquipmentProxy targetEquipment)
                return;

            electricalEquipment
                .ConnectTo(new ElectricalSystemProxy(_virtualElements.NewId(), null, electricalEquipment.DistributionSystem))
                .ConnectTo(targetEquipment);
            
            NodesGridViewModel.Nodes.CreateNode(electricalEquipment);
            NodesGridViewModel.Select(electricalEquipment);
            _virtualElementsRepository.Save();
        };

        NodesGridViewModel.PickModeOn(
            message: $"{Messages.Instruction_SelectItemToConnect} {electricalEquipment.Name}",
            allowedNodes);
    }

    private void EditEquipment(object @object)
    {
        _viewInteraction.ElectricalDiagram.UpdateNode(@object);
        
        // if (@object is not ElectricalEquipmentProxy electricalEquipment)
        //     return;
        //
        // var source = electricalEquipment.GetFirstSourceOf<ElectricalEquipmentProxy>();
        //
        // if (!source.IsCompatible(electricalEquipment))
        // {
        //     var network =
        //         _virtualElementsRepository.First().Networks.FirstOrDefault(x => x.IsCompatible(electricalEquipment)) ??
        //         _virtualElementsUtils.CreateCompatibleNetwork(electricalEquipment);
        //     
        //     electricalEquipment.ConnectTo(network);
        // }
        //
        // NodesGridViewModel.Nodes.UpdateParentNode(electricalEquipment);
        _virtualElementsRepository.Save();
    }

    private void UpdateOperatingModes()
    {
        OperatingModes = null;
        OperatingModes = _serviceProvider.Get<OperatingModes>();
        SelectedOperatingMode = OperatingModes.Current ?? OperatingModes.FirstOrDefault();
    }
    
    public void Connect()
    {
        // _selected = Selected.LastOrDefault();
        //
        // if (_selected?.Reference is not ElectricalEquipmentProxy selectedEquipment ||
        //     selectedEquipment.DistributionSystem == null)
        //     return;

        // Connect(selectedEquipment);
    }
    
    private void Connect(object @object)
    {
        if (@object is ElectricalEquipmentProxy electricalEquipment)
            Connect(electricalEquipment);
    }
    
    public void Connect(ElectricalEquipmentProxy electricalEquipment)
    {
        // NodesGridViewModel.OkPickCallback = reference =>
        // {
        //     if (reference is not ElectricalEquipmentProxy targetEquipment)
        //         return;
        //     
        //     electricalEquipment.ConnectTo(targetEquipment);
        //
        //     if (!NodesGridViewModel.Nodes.TryGetNode(electricalEquipment, out var _))
        //         NodesGridViewModel.Nodes.AddNode(electricalEquipment);
        //
        //     NodesGridViewModel.Select(electricalEquipment);
        //     _virtualElementsRepository.Save();
        // };
        //
        // NodesGridViewModel.PickModeOn(
        //     message: $"{Resources.Messages.Instruction_SelectItemToConnect} {electricalEquipment.Name}",
        //     compatibleEquipment: electricalEquipment);
    }

    private void Disconnect()
    {
        switch (NodesGridViewModel.NodeSelection.Selected)
        {
            case ConnectionViewModel connectionViewModel:
                DisconnectConnection(connectionViewModel);
                break;
            
            case NodeViewModel nodeViewModel:
                DisconnectNode(nodeViewModel);
                break;
        }
    }

    private void DisconnectConnection(ConnectionViewModel connectionViewModel)
    {
        var networkNode = NodesGridViewModel.Nodes
            .GetAllowedRoots(connectionViewModel.ElectricalSystem)
            .FirstOrDefault(x => x.Reference is ElectricalNetwork);
        
        var newConnection = NodesGridViewModel.Nodes.ConnectToNode(connectionViewModel, networkNode);
        NodesGridViewModel.NodeSelection.Select(newConnection);
        _electricalSystems.ChangedElements.Modify(connectionViewModel.ElectricalSystem.RevitId);
        _electricalSystems.Save();
    }

    private void DisconnectNode(NodeViewModel node)
    {
        var parentConnection = node.ParentConnection;
        var equipmentConsumers = parentConnection.ElectricalSystem.GetConsumersOf<ElectricalEquipmentProxy>().ToArray();

        if (equipmentConsumers.IsEmpty())
            return;
        if (equipmentConsumers.Length == 1)
            DisconnectConnection(parentConnection);
        else
            DisconnectOrRemoveFromCircuit(node);
    }

    private void DisconnectOrRemoveFromCircuit(NodeViewModel node)
    {
        var electricalSystem = node.ParentConnection.ElectricalSystem;
        var variant1 = string.Format(Messages.DialogFormat_DisconnectCircuit, electricalSystem.Name, electricalSystem.Source.Name);
        var variant2 = string.Format(Messages.DialogFormat_RemoveFromCircuit, node.Reference.Name, electricalSystem.Name);

        DiagramDialogViewModel = new SelectItemDialogViewModel(new[] { variant1, variant2 })
        {
            OkCallback = item =>
            {
                if (item is not string value)
                    return;
                if (value == variant1)
                    DisconnectConnection(node.ParentConnection);
                else if (value == variant2)
                    RemoveElementFromCircuit(node);
                
                _electricalSystems.ChangedElements.Modify(electricalSystem.RevitId);
                _electricalSystems.Save();
            },
            Close = () => DiagramDialogViewModel = null,
            ItemsWrapping = false
        };
    }

    private void RemoveElementFromCircuit(NodeViewModel node)
    {
        var element = node.Reference;
        
        node.ParentConnection.ElectricalSystem.Remove(element);

        var networkNode = NodesGridViewModel.Nodes
            .GetAllowedRoots(element)
            .First(x => x.Reference is ElectricalNetwork);

        element.ConnectTo(networkNode.Reference);

        NodesGridViewModel.Nodes.ConnectToNode(node, networkNode);
        NodesGridViewModel.NodeSelection.Select(node.ParentConnection);
        _electricalSystems.ChangedElements.Modify(element.RevitId);
        _electricalSystems.Save();
    }
}