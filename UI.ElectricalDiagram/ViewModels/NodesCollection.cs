﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.BusinessLogic.Model.Utils;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

// ReSharper disable UnusedMember.Local

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class NodesCollection : ObservableCollection<NodeViewModel>
{
    private readonly VirtualElementsUtils _virtualElementsUtils;
    private readonly Dictionary<ElectricalBase, NodeViewModel> _nodes = new();
    private int _isExpanding;

    public NodesCollection(VirtualElementsUtils virtualElementsUtils)
    {
        _virtualElementsUtils = virtualElementsUtils;
    }
    
    public ConnectionCollections Connections { get; set; } = new();
    public Action UpdatePositions { get; set; }

    public NodeViewModel CreateNode(ElectricalEquipmentProxy electricalEquipment)
    {
        if (electricalEquipment is ElectricalSource electricalSource)
            return CreateRoot(electricalSource);
        
        var parentNode = GetParentNode(electricalEquipment);
        var node = NodeViewModel.Create(electricalEquipment, parentNode);
        AddToParent(node);
        UpdatePositions?.Invoke();
        return node;
    }

    public NodeViewModel CreateRoot(ElectricalSource electricalSource)
    {
        var node = new SourceNodeViewModel(electricalSource)
        {
            ColumnIndex = 0,
            RowIndex = GetRoots().OrderBy(x => x.RowIndex).LastOrDefault()?.RowIndex + 1 ?? 0,
        };
        
        _nodes.Add(electricalSource, node);
        Add(node);
        AddChildren(node);
        return node;
    }

    private void AddToParent(NodeViewModel childNode)
    {
        var parentNode = childNode.ParentNode;
        
        MoveSelf(parentNode);
        MoveOtherNodes(parentNode, parentNode.ChildNodes.Count);
        
        childNode.ColumnIndex = parentNode.ColumnIndex + 1;
        childNode.RowIndex = parentNode.RowIndex + parentNode.ChildNodes.Count - 1;
        
        AddNode(childNode);
        parentNode.UpdateExpandable();
        SetExpanding(parentNode);
        UpdatePositions?.Invoke();
    }
    
    private void AddChildren(NodeViewModel node)
    {
        try
        {
            var electricalEquipmentProxy = node.Reference;
            var consumers = electricalEquipmentProxy.GetFirstConsumersOf<ElectricalEquipmentProxy>();
        
            if (consumers.IsEmpty())
                return;
        
            MoveSelf(node);
            MoveOtherNodes(node, consumers.Count);
        
            var columnIndex = node.ColumnIndex + 1;
            var rowIndex = node.RowIndex;
        
            foreach (var newNode in consumers.Select(consumer => NodeViewModel.Create(consumer, node)))
            {
                newNode.ColumnIndex = columnIndex;
                newNode.RowIndex = rowIndex++;
                AddNode(newNode);
            }
        }
        finally
        {
            UpdatePositions?.Invoke();
        }
    }

    private void AddNode(NodeViewModel newNode)
    {
        _nodes.Add(newNode.Reference, newNode);
        Add(newNode);
        SetExpanding(newNode);
        Connections.Create(newNode.ParentNode, newNode);
    }

    public void UpdateNode(object obj)
    {
        if (obj is ElectricalBase electrical &&
            TryGetNode(electrical, out var node))
            node.UpdateProperties();
    }

    public ConnectionViewModel ConnectToNode(ConnectionViewModel connection, NodeViewModel targetNode)
    {
        connection.ElectricalSystem.ConnectTo(targetNode.Reference);
        
        var nodes = _nodes
            .Where(x => connection.ElectricalSystem.Consumers.Contains(x.Key))
            .OrderBy(x => x.Value.RowIndex)
            .Select(x => x.Value)
            .ToArray();

        Connections.RemoveConnection(connection.ElectricalSystem);
        
        foreach (var node in nodes) ConnectToNode(node, targetNode);

        UpdatePositions?.Invoke();

        return nodes.LastOrDefault()?.ParentConnection;
    }

    public void ConnectToNode(NodeViewModel node, NodeViewModel targetNode)
    {
        var electricalEquipment = (ElectricalEquipmentProxy)node.Reference;
        DeleteNode(node);
        var oldParent = node.ParentNode;
        
        if (oldParent != null)
        {
            var belowNodes = oldParent.ChildNodes.Where(x => x.RowIndex > node.RowIndex).OrderBy(x => x.RowIndex).ToArray();
            var belowReferences = belowNodes.Select(x => x.Reference).OfType<ElectricalEquipmentProxy>();
            foreach (var belowNode in belowNodes) DeleteNode(belowNode);
            foreach (var reference in belowReferences) CreateNode(reference);
        }

        if (electricalEquipment.Source is ElectricalSystemProxy electricalSystem)
            electricalSystem.ConnectTo(targetNode.Reference);
        else if (targetNode.Reference is ElectricalNetwork network)
            electricalEquipment.ConnectTo(network);
        else
            throw new InvalidOperationException($"TODO: Create electrical system to connect {electricalEquipment} to {targetNode.Reference}");
        
        CreateNode(electricalEquipment);
    }

    public void DeleteNode(ElectricalBase electrical, bool includeChildren = true)
    {
        if (!TryGetNode(electrical, out var node))
            return;

        DeleteNode(node, includeChildren);
    }

    private void DeleteNode(NodeViewModel node, bool includeChildren = true)
    {
        if (includeChildren)
        {
            while (node.ChildNodes.Any())
            {
                var childNode = node.ChildNodes[0];
                DeleteNode(childNode, includeChildren: true);
                node.RemoveChildNode(childNode);
            }
        }

        node.ParentNode.RemoveChildNode(node);
        Connections.RemoveConnection(node.ParentConnection);
        _nodes.Remove(node.Reference);
        this.Remove(node);
        UpdatePositions?.Invoke();
    }

    private void SetExpanding(NodeViewModel nodeViewModel)
    {
        if (!nodeViewModel.Expandable) return;
        nodeViewModel.Expand = Expand;
        nodeViewModel.Collapse = Collapse;
    }

    private void Expand(NodeViewModel nodeViewModel)
    {
        _isExpanding++;
        
        if (nodeViewModel.ChildNodes.IsEmpty())
        {
            AddChildren(nodeViewModel);
        }
        else
        {
            foreach (var childNode in nodeViewModel.ChildNodes)
            {
                childNode.Visible = true;
                childNode.ParentConnection.Visible = true;
                if (childNode.IsExpanded) Expand(childNode);
            }
        }
        
        _isExpanding--;
        
        if (_isExpanding == 0)
            UpdatePositions?.Invoke();
    }

    private void Collapse(NodeViewModel nodeViewModel)
    {
        _isExpanding++;
        
        foreach (var childNode in nodeViewModel.ChildNodes)
        {
            Collapse(childNode);
            childNode.ParentConnection.Visible = false;
            childNode.Visible = false;
        }
        
        _isExpanding--;
        
        if (_isExpanding == 0)
            UpdatePositions?.Invoke();
    }

    private void MoveSelf(NodeViewModel node)
    {
        var columnIndex = node.ColumnIndex + 1;
        var hinderNodes = this.Where(x => x.ColumnIndex == columnIndex && x.RowIndex >= node.RowIndex && node.IsBelowThenBranchOf(x));
        var lastHinderNode = hinderNodes.OrderBy(x => x.RowIndex).LastOrDefault();

        if (lastHinderNode == null)
            return;
        
        MoveDown(node, lastHinderNode.RowIndex - node.RowIndex + 1);
    }

    private void MoveOtherNodes(NodeViewModel node, int childNodesCount)
    {
        var columnIndex = node.ColumnIndex + 1;
        var rowIndex = node.RowIndex;
        var lastRowIndex = node.RowIndex + childNodesCount;
        var firstBelowNode = this
            .Where(x =>
                x.ColumnIndex == columnIndex &&
                x.RowIndex > rowIndex &&
                x.RowIndex <= lastRowIndex &&
                x.ParentNode != node)
            .OrderBy(x => x.RowIndex)
            .FirstOrDefault();

        if (firstBelowNode == null)
            return;
        
        MoveDown(firstBelowNode, lastRowIndex - firstBelowNode.RowIndex);
    }
    
    private void MoveDown(NodeViewModel node, int count)
    {
        if (node == null)
            return;
        
        var belowColumnNodes = GetBelowBranchesNodes(node)
            .Concat(GetThisRowParentNodes(node))
            .Concat(GetAllChildNodes(node));

        foreach (var belowColumnNode in belowColumnNodes)
            belowColumnNode.RowIndex += count;

        node.RowIndex += count;
    }

    private NodeViewModel Get(int columnIndex, int rowIndex) =>
        this.FirstOrDefault(x => x.ColumnIndex == columnIndex && x.RowIndex == rowIndex);

    private IEnumerable<NodeViewModel> GetRoots() =>
        this.Where(x => x.ColumnIndex == 0);

    private static IEnumerable<NodeViewModel> GetAllChildNodes(NodeViewModel node) =>
        node.ChildNodes.Concat(node.ChildNodes.SelectMany(GetAllChildNodes)).ToArray();

    private static NodeViewModel[] GetFirstChildNodes(NodeViewModel nodeViewModel)
    {
        var childNode = nodeViewModel.ChildNodes.FirstOrDefault();
        return childNode is not null
            ? new[] { childNode }.Concat(childNode.IsExpanded ? GetFirstChildNodes(childNode) : Array.Empty<NodeViewModel>()).ToArray()
            : Array.Empty<NodeViewModel>();
    }

    private static IEnumerable<NodeViewModel> GetThisRowParentNodes(NodeViewModel nodeViewModel)
    {
        var parentNode = nodeViewModel.ParentNode;
        return parentNode?.RowIndex == nodeViewModel.RowIndex
            ? new[] { parentNode }.Concat(GetThisRowParentNodes(parentNode))
            : Array.Empty<NodeViewModel>();
    }

    private IEnumerable<NodeViewModel> GetBelowBranchesNodes(NodeViewModel node)
    {
        return this.Where(x => x.IsBelowThenBranchOf(node));
    }

    private NodeViewModel GetParentNode(ElectricalBase electrical)
    {
        var source = electrical.GetFirstSourceOf<ElectricalEquipmentProxy>();

        return source != null
            ? TryGetNode(source, out var parentNode) ? parentNode : CreateNode(source)
            : GetRoots().FirstOrDefault(x => x.Reference.IsCompatible(electrical)) ??
              CreateRoot(_virtualElementsUtils.CreateCompatibleNetwork(electrical));
    }

    public bool TryGetNode(ElectricalBase key, out NodeViewModel node)
    {
        node = null;
        return key != null && _nodes.TryGetValue(key, out node);
    }

    internal NodeViewModel[] GetAllowedNodes(ElectricalBase electrical)
    {
        if (electrical == null)
            return Array.Empty<NodeViewModel>();
        
        var allowedNodes = (electrical.IsPower
                ? this.Where(x => IsPowerCompatible(electrical, x.Reference))
                : this.Where(x => electrical.IsCompatible(x.Reference)))
            .ToArray();

        if (allowedNodes.Any())
            return allowedNodes;
        
        var newNetwork = _virtualElementsUtils.CreateCompatibleNetwork(electrical);

        return new[] { CreateRoot(newNetwork) };
    }

    internal NodeViewModel[] GetAllowedRoots(ElectricalBase electrical)
    {
        if (electrical == null)
            return Array.Empty<NodeViewModel>();
        
        var allowedNodes = (electrical.IsPower
                ? this.Where(x => x.ColumnIndex == 0 && IsPowerCompatible(electrical, x.Reference))
                : this.Where(x => x.ColumnIndex == 0 && electrical.IsCompatible(x.Reference)))
            .ToArray();

        if (allowedNodes.Any())
            return allowedNodes;
        
        var newNetwork = _virtualElementsUtils.CreateCompatibleNetwork(electrical);

        return new[] { CreateRoot(newNetwork) };
    }

    private static bool IsPowerCompatible(ElectricalBase electrical, ElectricalBase target)
    {
        return target switch
        {
            Transformer transformer =>
                transformer.Consumers.IsEmpty() &&
                transformer.SecondaryDistributionSystem.IsCompatible(electrical),
            _ => electrical.IsCompatible(target)
        };
    }
}