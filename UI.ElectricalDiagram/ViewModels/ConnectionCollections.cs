﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class ConnectionCollections : ObservableCollection<ConnectionViewModel>
{
    private readonly Dictionary<ElectricalSystemProxy, List<NodeViewModel>> _createdConnections = new();
    
    public ConnectionViewModel Create(NodeViewModel parentNode, NodeViewModel childNode)
    {
        var child = childNode.Reference;
        var circuit = child.GetFirstSourceOf<ElectricalSystemProxy>();
        ConnectionViewModel connectionViewModel;

        if (circuit == null)
        {
            connectionViewModel = ConnectionViewModel.CreateFromRightToLeft(null, parentNode, childNode);
        }
        else if (_createdConnections.TryGetValue(circuit, out var circuitNodes) && circuitNodes.Any())
        {
            var previousNode = circuitNodes.LastOrDefault();
            connectionViewModel = ConnectionViewModel.CreateOneSide(circuit, previousNode, childNode, ConnectorOrientation.Left);
            circuitNodes.Add(childNode);
        }
        else
        {
            connectionViewModel = ConnectionViewModel.CreateFromRightToLeft(circuit, parentNode, childNode);
            _createdConnections.Add(circuit, new List<NodeViewModel>{childNode});
        }
            
        childNode.ParentConnection = connectionViewModel;
        Add(connectionViewModel);

        return connectionViewModel;
    }

    public void RemoveConnection(ElectricalSystemProxy electricalSystem)
    {
        if (!_createdConnections.TryGetValue(electricalSystem, out var allCircuitNodes))
            return;

        foreach (var connection in allCircuitNodes.Select(node => node.ParentConnection))
            _RemoveConnection(connection);

        _createdConnections.Remove(electricalSystem);
    }

    public void RemoveConnection(ConnectionViewModel connection)
    {
        if (_createdConnections.TryGetValue(connection.ElectricalSystem, out var nodes))
        {
            nodes.Remove(connection.TargetNode);
                
            if (nodes.IsEmpty())
                _createdConnections.Remove(connection.ElectricalSystem);
        }

        _RemoveConnection(connection);
    }
    
    private void _RemoveConnection(ConnectionViewModel connection)
    {
        if (connection.SourceNode != null)
        {
            connection.SourceNode.IsStrongHighlight = false;
            connection.SourceConnector.Disconnect(connection);
        }

        if (connection.TargetNode != null)
        {
            connection.TargetNode.IsStrongHighlight = false;
            connection.TargetConnector.Disconnect(connection);
        }
        
        this.Remove(connection);
    }
}