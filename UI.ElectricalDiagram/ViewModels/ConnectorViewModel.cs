﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CoLa.BimEd.UI.ElectricalDiagram.Controls;
using CoLa.BimEd.UI.ElectricalDiagram.Logic;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class ConnectorViewModel : ViewModel
{
    public NodeViewModel Owner { get; set; }
    public List<ConnectionViewModel> Connections { get; set; }
    public ConnectorOrientation Orientation { get; set; }
    public Point Position { get; set; }
    public bool IsConnected { get; set; }
    public bool IsSelected { get; set; }
    public bool IsStrongHighlight { get; set; }
    public bool IsWeekHighlight { get; set; }

    public ConnectorViewModel(NodeViewModel owner, ConnectorOrientation connectorOrientation)
    {
        Owner = owner;
        Orientation = connectorOrientation;
        Connections = new List<ConnectionViewModel>();
    }

    internal void CheckConnection()
    {
        IsConnected = Connections.Any();
    }

    public void Disconnect(ConnectionViewModel connection)
    {
        Connections.Remove(connection);
        
        if (connection.SourceConnector == this)
            connection.SourceConnector = null;
        
        if (connection.TargetConnector == this)
            connection.TargetConnector = null;
        
        CheckConnection();
    }
    
    internal ConnectorInfo GetInfo()
    {
        return new ConnectorInfo
        {
            NodeLeft = Owner.Left,
            NodeTop = Owner.Top,
            NodeSize = Owner.ActualSize,
            Orientation = this.Orientation,
            Position = this.Position
        };
    }
    public override string ToString() => $"{Orientation}: {Position.X}, {Position.Y}";
}