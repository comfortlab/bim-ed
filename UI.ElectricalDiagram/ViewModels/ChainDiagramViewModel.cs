using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.Application.Interaction;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Chain;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.ElectricalDiagram.ViewModels;

public class ChainDiagramViewModel : ViewModel
{
    public ChainElement[] Chain { get; set; }
    public bool ChainEnabled { get; set; } = true;

    public ChainDiagramViewModel(
        Interactions interactions)
    {
        interactions.View.ElectricalDiagram.UpdateChainDiagram = obj => Chain = obj switch
        {
            IEnumerable<ElectricalBase> chain => CreateChain(chain),
            _ => Chain
        };
    }

    private static ChainElement[] CreateChain(IEnumerable<ElectricalBase> sourceChain)
    {
        var collection = sourceChain.ToArray();
        var chain = new List<ChainElement>();

        for (var i = 0; i < collection.Length; i++)
        {
            var reference = collection[i];
            var source = reference.Source;
            
            if (source is ElectricalSystemProxy electricalSystem)
                chain.Add(new ChainElement(electricalSystem));
            if (i + 1 < collection.Length && Equals(collection[i + 1].Source, source))
                chain.Add(new ChainElement(reference, ChainLevel.Secondary));
            else
                chain.Add(new ChainElement(reference));
        }

        if (chain.Count > 0)
        {
            chain[0].IsFirst = true;
            chain[chain.Count - 1].IsLast = true;
        }
        
        return chain.ToArray();
    }
    
    private void OnChainChanged() => UpdateScrollPosition();

    private void UpdateScrollPosition()
    {
        ChainEnabled = false;
        ChainEnabled = true;
    }
}