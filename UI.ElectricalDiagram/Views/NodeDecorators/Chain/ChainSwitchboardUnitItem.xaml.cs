﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Views.NodeDecorators.Chain;

public partial class ChainSwitchboardUnitItem
{
    public ChainSwitchboardUnitItem(SwitchBoardUnit switchBoardUnit)
    {
        InitializeComponent();
        DataContext = new SwitchboardUnitNodeViewModel(switchBoardUnit);
    }
}