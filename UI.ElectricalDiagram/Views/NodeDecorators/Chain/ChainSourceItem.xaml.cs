﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Views.NodeDecorators.Chain;

public partial class ChainSourceItem
{
    public ChainSourceItem(ElectricalSource source)
    {
        InitializeComponent();
        DataContext = new SourceNodeViewModel(source);
    }
}