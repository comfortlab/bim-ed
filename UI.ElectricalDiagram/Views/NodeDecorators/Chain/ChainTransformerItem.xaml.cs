﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.UI.ElectricalDiagram.ViewModels.Nodes;

namespace CoLa.BimEd.UI.ElectricalDiagram.Views.NodeDecorators.Chain;

public partial class ChainTransformerItem
{
    public ChainTransformerItem(Transformer transformer)
    {
        InitializeComponent();
        DataContext = new TransformerNodeViewModel(transformer, null);
    }
}