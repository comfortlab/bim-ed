﻿using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalDiagram.Views;

public partial class NodesGridView
{
    public NodesGridView() => InitializeComponent();

    private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.MultiScrollMouseWheel(sender, e);
}