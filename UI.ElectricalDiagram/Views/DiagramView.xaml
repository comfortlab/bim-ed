﻿<UserControl x:Class="CoLa.BimEd.UI.ElectricalDiagram.Views.DiagramView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:resources="clr-namespace:CoLa.BimEd.UI.Resources;assembly=CoLa.BimEd.UI.Resources"
             xmlns:converters="clr-namespace:CoLa.BimEd.UI.ElectricalDiagram.Converters"
             xmlns:views="clr-namespace:CoLa.BimEd.UI.ElectricalDiagram.Views"
             xmlns:viewModels="clr-namespace:CoLa.BimEd.UI.ElectricalDiagram.ViewModels"
             mc:Ignorable="d"
             d:DesignHeight="450" d:DesignWidth="800"
             d:DataContext="{d:DesignInstance viewModels:DiagramViewModel}"
             UseLayoutRounding="True">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/CoLa.BimEd.UI.Framework;component/ResourceDictionaries/Converters/Converters.xaml" />
                <ResourceDictionary Source="/CoLa.BimEd.UI.Framework;component/ResourceDictionaries/Styles/Borders.xaml" />
                <ResourceDictionary Source="/CoLa.BimEd.UI.Framework;component/ResourceDictionaries/Styles/Buttons.xaml" />
                <ResourceDictionary Source="/CoLa.BimEd.UI.Framework;component/ResourceDictionaries/Styles/Layout.xaml" />
                <ResourceDictionary Source="/CoLa.BimEd.UI.Framework;component/ResourceDictionaries/Styles/ToolTips.xaml" />
            </ResourceDictionary.MergedDictionaries>
            <converters:DataContextViewConverter x:Key="DataContextViewConverter" />
            <x:Static x:Key="Create" Member="converters:DataContextViewConverter.Create" />
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid>

        <!-- Main Dialog View -->
        
        <Grid
            ZIndex="1"
            Background="{StaticResource TranslucentDarkGrayBackgroundBrush}"
            Visibility="{Binding MainDialogViewModel, Converter={StaticResource NullVisibilityConverter}}">
            <ContentControl
                Content="{Binding Converter={StaticResource DataContextViewConverter}, ConverterParameter={StaticResource Create}}"
                DataContext="{Binding MainDialogViewModel}"
                Margin="20" />
        </Grid>

        <!-- Main View -->

        <Grid
            Background="{StaticResource BackgroundBrush}"
            IsEnabled="{Binding MainDialogViewModel, Converter={StaticResource InvertNullBoolConverter}}">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="4*" />
            </Grid.ColumnDefinitions>

            <!-- Left side -->

            <Grid
                Grid.Column="0">
                <Grid
                    IsEnabled="{Binding NodesGridViewModel.IsPickMode, Converter={StaticResource InvertBoolConverter}}">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition />
                    </Grid.RowDefinitions>

                    <views:SelectOperatingModeView
                        Grid.Row="0" />

                    <views:ChainDiagramView
                        Grid.Row="1"
                        DataContext="{Binding ChainDiagramViewModel}"/>
                </Grid>
                
                <Grid
                    Background="{StaticResource TranslucentDarkGrayBackgroundBrush}"
                    Visibility="{Binding NodesGridViewModel.IsPickMode, Converter={StaticResource BoolVisibilityConverter}}"/>

            </Grid>
            
            <Grid
                Grid.Column="1">
                
                <!-- Diagram Dialog View -->
                
                <Grid
                    ZIndex="1"
                    Background="{StaticResource TranslucentDarkGrayBackgroundBrush}"
                    Margin="1 0 0 0"
                    Visibility="{Binding DiagramDialogViewModel, Converter={StaticResource NullVisibilityConverter}}">
                    <ContentControl
                        Content="{Binding Converter={StaticResource DataContextViewConverter}}"
                        DataContext="{Binding DiagramDialogViewModel}"
                        Margin="20"/>
                </Grid>

                <Grid
                    IsEnabled="{Binding DiagramDialogViewModel, Converter={StaticResource InvertNullBoolConverter}}">
                    
                    <!-- Graph -->

                    <views:NodesGridView
                        DataContext="{Binding NodesGridViewModel}" />

                    <!-- Buttons -->

                    <ContentControl
                        HorizontalAlignment="Left"
                        VerticalAlignment="Center"
                        Template="{StaticResource DarkGrayRounderBorderTemplate}"
                        Margin="10"
                        Visibility="{Binding NodesGridViewModel.IsPickMode, Converter={StaticResource InvertBoolVisibilityConverter}}">
                        <StackPanel>
                            <StackPanel.Resources>
                                <Style
                                    TargetType="{x:Type ToolTip}"
                                    BasedOn="{StaticResource DarkRightCenteredToolTip}" />
                            </StackPanel.Resources>
                            <Button
                                Command="{Binding DistributionSystemSettingCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.DistributionSystemsSetting}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Common/Setting_32x32.png"
                                    Stretch="None" />
                            </Button>
                            <Button
                                Command="{Binding LoadClassificationsCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.LoadClassifications}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/LoadClassification/LoadClassification_32x32.png"
                                    Stretch="None" />
                            </Button>

                            <Line Style="{StaticResource HorizontalSeparatingLine34}" />

                            <Button
                                Command="{Binding CreateSourceCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.CreateElectricitySource}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/SourceCreate_32x32.png"
                                    Stretch="None" />
                            </Button>
                            <Button
                                Command="{Binding CreateTransformerCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.CreateTransformer}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Equipment/TransformerCreate_32x32.png"
                                    Stretch="None" />
                            </Button>
                            <Button
                                Command="{Binding CreateSwitchGearCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.CreateMediumVoltageCubicle}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Equipment/PanelMediumVoltageCreate_32x32.png"
                                    Stretch="None" />
                            </Button>
                            <Button
                                Command="{Binding CreateSwitchBoardCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.CreateLowVoltagePanel}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Equipment/PanelLowVoltageCreate_32x32.png"
                                    Stretch="None" />
                            </Button>

                            <Line Style="{StaticResource HorizontalSeparatingLine34}" />

                            <Button
                                Command="{Binding ConnectCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.ConnectTo}"
                                Visibility="{Binding NodesGridViewModel.CanConnect, Converter={StaticResource BoolVisibilityConverter}}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Equipment/PanelConnect_32x32.png"
                                    Stretch="None" />
                            </Button>
                            <Button
                                Command="{Binding DisconnectCommand}"
                                Style="{StaticResource SquareButton40}"
                                ToolTip="{x:Static resources:CommandNames.Disconnect}"
                                Visibility="{Binding NodesGridViewModel.CanDisconnect, Converter={StaticResource BoolVisibilityConverter}}">
                                <Image
                                    Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Equipment/PanelDisconnect_32x32.png"
                                    Stretch="None" />
                            </Button>

                            <Line Style="{StaticResource HorizontalSeparatingLine34}" />

                            <ContentControl>
                                <!-- Template="{StaticResource SelectorSettingPopupTemplate}"> -->
                                <StackPanel>
                                    <Button
                                        Command="{Binding SelectAllEquipmentCommand}"
                                        Style="{StaticResource SquareButton40}"
                                        ToolTip="{x:Static resources:CommandNames.EquipmentSelection}">
                                        <Image
                                            Source="/CoLa.BimEd.UI.Resources;component/Images/Electrical/Circuits/CircuitsComponentCreate_32x32.png"
                                            Stretch="None" />
                                    </Button>
                                </StackPanel>
                            </ContentControl>
                        </StackPanel>
                    </ContentControl>
                </Grid>

                <!-- PickNodes -->

                <Grid
                    Visibility="{Binding NodesGridViewModel.IsPickMode, Converter={StaticResource BoolVisibilityConverter}}">
                    <views:PickNodeWrapper
                        DataContext="{Binding NodesGridViewModel}" />
                </Grid>
            </Grid>

            <GridSplitter
                Grid.Column="1"
                Style="{StaticResource VerticalSplitter}" />

        </Grid>
    </Grid>
</UserControl>