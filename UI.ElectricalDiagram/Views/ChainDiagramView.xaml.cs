﻿using System.Windows;
using System.Windows.Input;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.ElectricalDiagram.Views;

public partial class ChainDiagramView
{
    public ChainDiagramView() => InitializeComponent();

    private void ScrollViewer_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) =>
        ScrollingUtils.ScrollMouseWheel(sender, e);

    private void UIElement_OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e) =>
        ScrollViewer.ScrollToEnd();
}