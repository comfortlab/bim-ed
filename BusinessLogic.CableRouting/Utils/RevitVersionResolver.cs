﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.Infrastructure.Revit2016;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Utils;

internal static class RevitVersionResolver
{
    internal static IEnumerable<ElectricalSystem> GetElectricalSystems(
        int revitVersion,
        FamilyInstance familyInstance)
    {
        return revitVersion < 2021
            ? MEPModelRevit2016.GetElectricalSystems(familyInstance)
            : Revit2021.GetElectricalSystems(familyInstance);
    }

    internal static ParameterFilterElement CreateParameterFilterElement(
        int revitVersion,
        Document document,
        string name,
        IList<ElementId> categoryIds,
        IList<FilterRule> rules)
    {
        return revitVersion < 2019
            ? ParameterFilterElementRevit2016.Create(document, name, categoryIds, rules)
            : Revit2019.CreateParameterFilterElement(document, name, categoryIds, rules);
    }

    internal static void SetProjectionLinePatternId(
        int revitVersion,
        Category category,
        ElementId linePatternId)
    {
        if (revitVersion > 2016)
            Revit2017.SetProjectionLinePatternId(category, linePatternId);
    }
    
    internal static void SetSurfaceForegroundPatternVisible(
        int revitVersion,
        OverrideGraphicSettings graphicSettings, bool fillPatternVisible)
    {
        if (revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternVisible(graphicSettings, fillPatternVisible);
        else
            Revit2019.SetSurfaceForegroundPatternVisible(graphicSettings, fillPatternVisible);
    }

    internal static void SetSurfaceForegroundPatternId(
        int revitVersion,
        OverrideGraphicSettings graphicSettings,
        FillPatternElement solidPattern)
    {
        if (revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternId(graphicSettings, solidPattern.Id);
        else
            Revit2019.SetSurfaceForegroundPatternId(graphicSettings, solidPattern);
    }

    internal static void SetSurfaceForegroundPatternColor(
        int revitVersion,
        OverrideGraphicSettings graphicSettings,
        Color color)
    {
        if (revitVersion < 2019)
            OverrideGraphicSettingsRevit2016.SetSurfaceForegroundPatternColor(graphicSettings, color);
        else
            Revit2019.SetSurfaceForegroundPatternColor(graphicSettings, color);
    }
}

internal static class Revit2017
{
    internal static void SetProjectionLinePatternId(
        Category category, ElementId linePatternId)
    {
        category.SetLinePatternId(linePatternId, GraphicsStyleType.Projection);
    }
}

internal static class Revit2019
{
    internal static ParameterFilterElement CreateParameterFilterElement(
        Document document, string name, ICollection<ElementId> categoryIds, IList<FilterRule> rules)
    {
        return ParameterFilterElement.Create(document, name, categoryIds, new ElementParameterFilter(rules));
    }

    internal static void SetSurfaceForegroundPatternColor(
        OverrideGraphicSettings graphicSettings, Color color)
    {
        graphicSettings.SetSurfaceForegroundPatternColor(color);
    }

    internal static void SetSurfaceForegroundPatternId(
        OverrideGraphicSettings graphicSettings, FillPatternElement solidPattern)
    {
        graphicSettings.SetSurfaceForegroundPatternId(solidPattern.Id);
    }

    internal static void SetSurfaceForegroundPatternVisible(
        OverrideGraphicSettings graphicSettings, bool fillPatternVisible)
    {
        graphicSettings.SetSurfaceForegroundPatternVisible(fillPatternVisible);
    }
}

internal static class Revit2021
{
    internal static IEnumerable<ElectricalSystem> GetElectricalSystems(
        FamilyInstance familyInstance)
    {
        return familyInstance.MEPModel?.GetElectricalSystems() as IEnumerable<ElectricalSystem> ?? new List<ElectricalSystem>();
    }
}