﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Utils;

internal class RevitElementComparer<T> : IEqualityComparer<T> where T : Element
{
    public bool Equals(T x, T y)
    {
        if (ReferenceEquals(x, y))
            return true;

        if (ReferenceEquals(x, null) ||
            ReferenceEquals(y, null))
            return false;

        return x.Id.IntegerValue == y.Id.IntegerValue;
    }

    public int GetHashCode(T element) => element.Id.IntegerValue;
}