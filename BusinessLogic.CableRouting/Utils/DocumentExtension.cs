﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Utils;

internal static class DocumentExtension
{
    internal static void ExecuteTransactionGroup(this Document document, Action action, string transactionGroupName)
    {
        if (TransactionIsOpened(document))
        {
            action?.Invoke();
            return;
        }

        using var transactionGroup = new TransactionGroup(document, $"BIM ED: {transactionGroupName}");

        try
        {
            transactionGroup.Start();
            action();
            transactionGroup.Assimilate();
        }
        catch
        {
            if (transactionGroup.HasStarted()) transactionGroup.RollBack();
            throw;
        }
    }

    internal static void ExecuteTransaction(this Document document, Action action, string transactionName, bool? showWarnings = null)
    {
        if (TransactionIsOpened(document))
        {
            action?.Invoke();
            return;
        }

        using var transaction = new Transaction(document, $"BIM ED: {transactionName}");

        try
        {
            if (transaction.Start() == TransactionStatus.Started)
            {
                // FailureController.SetWarningDisplay(transaction, showWarnings ?? AppInfo.Revit.ShowWarnings);
                action?.Invoke();
            }

            if (transaction.Commit() != TransactionStatus.Committed)
            {
                transaction.RollBack();
            }
        }
        catch (Exception)
        {
            if (transaction.HasStarted())
            {
                transaction.RollBack();
            }

            throw;
        }
    }

    internal static void ExecuteSubTransaction(this Document document, Action action)
    {
        using var subTransaction = new SubTransaction(document);

        try
        {
            if (subTransaction.Start() == TransactionStatus.Started)
            {
                action?.Invoke();
            }

            if (subTransaction.Commit() != TransactionStatus.Committed)
            {
                subTransaction.RollBack();
            }
        }
        catch (Exception)
        {
            if (subTransaction.HasStarted())
            {
                subTransaction.RollBack();
            }

            throw;
        }
    }

    private static bool TransactionIsOpened(this Document document) => document.IsReadOnly || document.IsModifiable;
    
    internal static List<T> GetElementsOf<T>(this Document document)
        where T : Element
    {
        using var filteredElementCollector = document.NewElementCollector();
        return filteredElementCollector.OfClass(typeof(T)).OfType<T>().ToList();
    }
    
    internal static FilteredElementCollector NewElementCollector(this Document document) => new(document);
    
    internal static int GetRevitVersion(this Document document)
    {
        return int.TryParse(document.Application.VersionNumber.Split('.').FirstOrDefault(), out var number)
            ? number
            : 2022;
    }
}