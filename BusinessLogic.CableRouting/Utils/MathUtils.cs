﻿namespace CoLa.BimEd.BusinessLogic.CableRouting.Utils;

internal static class MathUtils
{
    private const double DefaultTolerance = 1E-6;
    
    internal static double FeetToMillimeters(this double feet, bool round = false)
    {
        var value = feet * 304.8;
        return round ? System.Math.Round(value) : value;
    }

    internal static double FeetToMillimeters(this double feet, int digits)
    {
        return (feet * 304.8).Round(digits);
    }
    
    internal static double MillimetersToFeet(this double millimeters, bool round = false)
    {
        var value = millimeters / 304.8;
        return round ? System.Math.Round(value) : value;
    }

    internal static double MillimetersToFeet(this double millimeters, int digits)
    {
        return (millimeters / 304.8).Round(digits);
    }

    internal static double Round(this double value, int significantDigits, int? digits = null)
    {
        var absValue = System.Math.Abs(value);
            
        if (System.Math.Abs(value) < DefaultTolerance ||
            System.Math.Abs(absValue - 1) < DefaultTolerance ||
            significantDigits is < 1 or > 16)
            return value;

        var result = value;
        var i = 1;

        switch (absValue)
        {
            case < 1:
            {
                while (System.Math.Abs(value) < 1)
                {
                    value *= 10;
                    i *= 10;
                }

                result = System.Math.Round(value, significantDigits - 1) / i;
                break;
            }
            
            case > 1 when double.IsInfinity(absValue) == false:
            {
                while (System.Math.Abs(value) > 10)
                {
                    value /= 10;
                    i *= 10;
                }

                result = System.Math.Round(value, significantDigits - 1) * i;
                break;
            }
        }

        if (digits != null)
            result = System.Math.Round(result, (int)digits);

        return result;
    }
}