﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Utils;

internal class RevitIdEqualityComparer<T> : IEqualityComparer<T> where T : ElementProxy
{
    public bool Equals(T x, T y)
    {
        if (ReferenceEquals(x, y)) return true;
        if (ReferenceEquals(x, null)) return false;
        if (ReferenceEquals(y, null)) return false;
        if (x.GetType() != y.GetType()) return false;
        return x.RevitId.Equals(y.RevitId);
    }

    public int GetHashCode(T obj)
    {
        return obj.RevitId;
    }
}