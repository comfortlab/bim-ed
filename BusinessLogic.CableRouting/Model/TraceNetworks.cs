﻿using System.Collections.Generic;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public class TraceNetworks
{
    public List<TraceNetwork> All { get; set; }
}