namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public enum TracePathType
{
    ByCableTrayConduit,
    ByElement,
}