namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public enum Topology
{
    Tree = 0,
    Star = 1,
    // Bus = 2,
    // Ring = 3
}