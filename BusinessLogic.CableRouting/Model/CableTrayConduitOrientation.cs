namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public enum CableTrayConduitOrientation
{
    Undefined = -1,
    Horizontal,
    Vertical,
    Inclined
}