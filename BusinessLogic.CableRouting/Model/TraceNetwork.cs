using System.Collections.Generic;
using System.Linq;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public class TraceNetwork
{
    public TraceNetwork(int id)
    {
        Id = id;
    }

    public int Id { get; }
    public List<ConnectorProxy> Connectors { get; } = new();
    public List<TraceElement> TraceElements { get; } = new();
    public List<ElectricalElementProxy> ElectricalElements { get; } = new();

    public TraceElement AddElement(TraceElement traceElement)
    {
        traceElement.TraceNetwork = this;

        switch (traceElement)
        {
            case ElectricalElementProxy electricalElement:
                ElectricalElements.Add(electricalElement);
                break;

            default:
                TraceElements.Add(traceElement);
                break;
        }

        return traceElement;
    }
    
    public TraceElement GetElement(int revitId) =>
        (TraceElement) ElectricalElements.FirstOrDefault(n => n.RevitId == revitId) ??
        TraceElements.FirstOrDefault(n => n.RevitId == revitId);

    public bool ElementInNetwork(int revitId) =>
        ElectricalElements.Any(n => n.RevitId == revitId) ||
        TraceElements.Any(n => n.RevitId == revitId);

    public bool ElementInNetwork(TraceElement traceElement) =>
        traceElement.TraceNetwork?.Equals(this) ?? false;
    
    public bool Equals(TraceNetwork other)
    {
        if (ReferenceEquals(other, null))
            return false;

        return ReferenceEquals(this, other) || Id.Equals(other.Id);
    }

    public override string ToString()
    {
        return $"{nameof(TraceNetwork)} [{Id}] TraceElements: {TraceElements.Count}"; // TraceElements: {TraceElements.EnumerableToString()}";
    }
}