﻿namespace CoLa.BimEd.BusinessLogic.CableRouting.Model;

public class CableRoutingSetting
{
    public double CableReservePercent { get; set; }
}