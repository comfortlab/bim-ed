﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;
using CoLa.BimEd.BusinessLogic.CableRouting.Utils;
using Profile = CoLa.BimEd.Infrastructure.Framework.AutoMapper.Profile;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Mapping;

using Topology = CoLa.BimEd.BusinessLogic.CableRouting.Model.Topology;

public class CableRoutingProfile : Profile
{
    public CableRoutingProfile()
    {
        CreateMap<Connector, ConnectorProxy>()
            .ForMember(dest => dest.Point, opt => opt.MapFrom(src => src.Origin));
            // .ForMember(dest => dest.Element, opt => opt.MapFrom(src => src.Owner));

        CreateMap<CableTray, CableTrayProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.Point1, opt => opt.MapFrom(src => (src.Location as LocationCurve).Curve.GetEndPoint(0)))
            .ForMember(dest => dest.Point2, opt => opt.MapFrom(src => (src.Location as LocationCurve).Curve.GetEndPoint(1)))
            .ForMember(dest => dest.ServiceType, opt => opt.MapFrom(src => src.get_Parameter(BuiltInParameter.RBS_CTC_SERVICE_TYPE).AsString()));

        CreateMap<Conduit, ConduitProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.Point1, opt => opt.MapFrom(src => (src.Location as LocationCurve).Curve.GetEndPoint(0)))
            .ForMember(dest => dest.Point2, opt => opt.MapFrom(src => (src.Location as LocationCurve).Curve.GetEndPoint(1)))
            .ForMember(dest => dest.ServiceType, opt => opt.MapFrom(src => src.get_Parameter(BuiltInParameter.RBS_CTC_SERVICE_TYPE).AsString()));

        CreateMap<CableTrayConduitBaseProxy, CableTrayConduitBase>()
            .ForMember(dest => dest.Name, opt => opt.Ignore())
            .AfterMap((src, dest) =>
            {
                dest.get_Parameter(SharedParameters.ElectricalSystemIds)?.Set($"#{string.Join("#", src.ElectricalSystems.Select(x => x.RevitId))}#");
                dest.get_Parameter(SharedParameters.CableTrace)?.Set(src.CableTrace);
                
                if (src is CableTrayProxy cableTray)
                    dest.get_Parameter(SharedParameters.CableTray_Filling)?.Set(cableTray.Filling);
            });
        
        CreateMap<Curve, SegmentProxy>()
            .ForMember(dest => dest.Point1, opt => opt.MapFrom(src => src.GetEndPoint(0)))
            .ForMember(dest => dest.Point2, opt => opt.MapFrom(src => src.GetEndPoint(1)));

        CreateMap<FamilyInstance, CableTrayConduitFittingProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue));

        CreateMap<FamilyInstance, ElectricalElementProxy>()
            .ForMember(dest => dest.CableReserve, opt => opt.MapFrom(src => AsDouble(src, SharedParameters.Cable_Reserve, 0)))
            .ForMember(dest => dest.LevelId, opt => opt.Ignore())
            .ForMember(dest => dest.LocationPoint, opt => opt.MapFrom(src => (src.Location as LocationPoint).Point))
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.Room, opt => opt.Ignore())
            .ForMember(dest => dest.RoomId, opt => opt.Ignore())
            .ForMember(dest => dest.ServiceType, opt => opt.MapFrom(src => AsString(src, SharedParameters.ServiceType, string.Empty)))
            .AfterMap(SetLevelAndRoomId);

        CreateMap<ElectricalSystem, ElectricalSystemProxy>()
            .ForMember(dest => dest.BaseEquipment, opt => opt.Ignore())
            .ForMember(dest => dest.CableDesignation, opt => opt.MapFrom(src => AsString(src, SharedParameters.Cable_Designation, string.Empty)))
            .ForMember(dest => dest.CableDiameter, opt => opt.MapFrom(src => AsDouble(src, SharedParameters.Cable_Diameter, 0d)))
            .ForMember(dest => dest.CablesCount, opt => opt.MapFrom(src => AsInteger(src, SharedParameters.Cables_Count, 1)))
            .ForMember(dest => dest.CircuitDesignation, opt => opt.MapFrom(src => AsString(src, SharedParameters.Circuit_Designation, string.Empty)))
            .ForMember(dest => dest.Elements, opt => opt.MapFrom(src => src.Elements.OfType<FamilyInstance>()))
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.Topology, opt => opt.MapFrom(src => (Topology)AsInteger(src, SharedParameters.Topology, 0)))
            .ReverseMap()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
            .AfterMap((src, dest) =>
            {
                dest.get_Parameter(SharedParameters.Cables_Count)?.Set(src.CablesCount);
                dest.get_Parameter(SharedParameters.Cable_Length)?.Set(src.CableLength.MillimetersToFeet());
                dest.get_Parameter(SharedParameters.Cable_Length_InCableTray)?.Set(src.CableLengthInCableTray.MillimetersToFeet());
                dest.get_Parameter(SharedParameters.Cable_Length_OutsideCableTray)?.Set(src.CableLengthOutsideCableTray.MillimetersToFeet());
                dest.get_Parameter(SharedParameters.Cable_LengthMax)?.Set(src.CableLengthMax.MillimetersToFeet());
                dest.get_Parameter(SharedParameters.Topology)?.Set((int)src.Topology);
            });

        CreateMap<Level, LevelProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue));

        CreateMap<Room, RoomProxy>()
            .ForMember(dest => dest.LevelId, opt => opt.MapFrom(src => src.LevelId.IntegerValue))
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.Segments, opt => opt.MapFrom(src => GetSegments(src)));
        
        CreateMap<Line, SegmentProxy>()
            .ForMember(dest => dest.Point1, opt => opt.MapFrom(src => src.GetEndPoint(0)))
            .ForMember(dest => dest.Point2, opt => opt.MapFrom(src => src.GetEndPoint(1)));
        
        CreateMap<XYZ, XYZProxy>()
            .ReverseMap();
    }
    
    private static void SetLevelAndRoomId(FamilyInstance src, ElectricalElementProxy dest)
    {
        var levelId = src.GetLevelId();

        dest.LevelId = levelId.IntegerValue;
        dest.RoomId = src.Room?.Id.IntegerValue ?? -1;
        
        if (dest.RoomId > 0 || dest.LevelId == -1 || src.Location is not LocationPoint locationPoint)
            return;

        var document = src.Document;
        var level = (Level)document.GetElement(levelId);
        var point = locationPoint.Point;
        var projectPoint = new XYZ(point.X, point.Y, level.Elevation + 0.01);
        var roomAtPoint = document.GetRoomAtPoint(projectPoint);

        dest.RoomId = roomAtPoint?.Id.IntegerValue ?? -1;
    }

    private static List<List<SegmentProxy>> GetSegments(SpatialElement spatialElement)
    {
        var segments = new List<List<SegmentProxy>>();
        var boundarySegments = spatialElement.GetBoundarySegments(new SpatialElementBoundaryOptions());

        foreach (var area in boundarySegments)
        {
            var areaSegments = new List<SegmentProxy>();
            segments.Add(areaSegments);
            
            foreach (var boundarySegment in area)
            {
                var tessellate = boundarySegment.GetCurve().Tessellate();
                
                for (var i = 1; i < tessellate.Count; i++)
                {
                    var xyz1 = tessellate[i - 1];
                    var xyz2 = tessellate[i];
                    var segment = new SegmentProxy(new XYZProxy(xyz1.X, xyz1.Y, xyz1.Z), new XYZProxy(xyz2.X, xyz2.Y, xyz2.Z));
                    areaSegments.Add(segment);
                }
            }
        }

        return segments;
    }

    private static double AsDouble(Element element, Guid guid, double defaultValue) => element.get_Parameter(guid)?.AsDouble() ?? defaultValue;
    private static int AsInteger(Element element, Guid guid, int defaultValue) => element.get_Parameter(guid)?.AsInteger() ?? defaultValue;
    private static string AsString(Element element, Guid guid, string defaultValue) => element.get_Parameter(guid)?.AsString() ?? defaultValue;
}