using System.Collections.Generic;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic;

public abstract class ProxyConverter<T> where T : class
{
    protected Document Document { get; }
    protected IMapper Mapper { get; }

    protected ProxyConverter(Document document, IMapper mapper)
    {
        Document = document;
        Mapper = mapper;
    }
    
    public abstract List<T> Convert();
}