﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic;

public class ElectricalSystemConverter
{
    private readonly Document _document;
    private readonly IMapper _mapper;
    private readonly BuildingProxy _buildingProxy;
    private Dictionary<int, ElectricalElementProxy> _baseEquipments;

    public ElectricalSystemConverter(Document document, IMapper mapper, BuildingProxy buildingProxy)
    {
        _document = document;
        _mapper = mapper;
        _buildingProxy = buildingProxy;
    }

    public List<ElectricalSystemProxy> Convert(params int[] electricalSystemIds)
    {
        _baseEquipments = new Dictionary<int, ElectricalElementProxy>();

        var electricalSystems = new FilteredElementCollector(_document)
            .OfClass(typeof(ElectricalSystem))
            .OfType<ElectricalSystem>()
            .Where(x => !electricalSystemIds.Any() || electricalSystemIds.Contains(x.Id.IntegerValue))
            .Select(GetElectricalSystemProxy)
            .ToList();

        return electricalSystems;
    }

    public ElectricalSystemProxy Convert(int electricalSystemId)
    {
        _baseEquipments = new Dictionary<int, ElectricalElementProxy>();

        return GetElectricalSystemProxy(new FilteredElementCollector(_document)
            .OfClass(typeof(ElectricalSystem))
            .OfType<ElectricalSystem>()
            .FirstOrDefault(x => electricalSystemId == x.Id.IntegerValue));
    }

    public ElectricalSystemProxy Convert(ElectricalSystem electricalSystem)
    {
        _baseEquipments = new Dictionary<int, ElectricalElementProxy>();

        return GetElectricalSystemProxy(electricalSystem);
    }

    private ElectricalSystemProxy GetElectricalSystemProxy(ElectricalSystem electricalSystem)
    {
        try
        {
            var proxy = _mapper.Map<ElectricalSystemProxy>(electricalSystem);
            AddBaseEquipment(electricalSystem, proxy);
            SetProperties(proxy);
            return proxy;
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
            return new ElectricalSystemProxy();
        }
    }

    private void AddBaseEquipment(MEPSystem x, ElectricalSystemProxy proxy)
    {
        if (x.BaseEquipment == null)
            return;
        
        if (_baseEquipments.TryGetValue(x.BaseEquipment.Id.IntegerValue, out var existBaseEquipment))
        {
            Connect(proxy, existBaseEquipment);
        }
        else
        {
            var baseEquipment = _mapper.Map<ElectricalElementProxy>(x.BaseEquipment);
            _baseEquipments.Add(baseEquipment.RevitId, baseEquipment);
            Connect(proxy, baseEquipment);
        }
    }

    private static void Connect(ElectricalSystemProxy electricalSystem, ElectricalElementProxy baseEquipment)
    {
        electricalSystem.BaseEquipment = baseEquipment;
        baseEquipment.ElectricalSystems.Add(electricalSystem);
    }

    private void SetProperties(ElectricalSystemProxy electricalSystem)
    {
        var baseEquipment = electricalSystem.BaseEquipment;

        if (baseEquipment != null)
        {
            baseEquipment.Level = _buildingProxy.Levels.FirstOrDefault(level => level.RevitId == baseEquipment.LevelId);
            baseEquipment.Room = _buildingProxy.GetRoom(baseEquipment);
        }

        foreach (var element in electricalSystem.Elements)
        {
            element.ElectricalSystems.Add(electricalSystem);
            element.Level = _buildingProxy.Levels.FirstOrDefault(level => level.RevitId == element.LevelId);
            element.Room = _buildingProxy.GetRoom(element);
        }

        if (electricalSystem.CablesCount < 1)
        {
            electricalSystem.CablesCount = 1;
        }
    }
}