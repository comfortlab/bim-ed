namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic.ViewFilters;

internal enum FilterRuleMode
{
    Contains,
    Equals,
    Greater,
    GreaterOrEqual,
    Less,
    LessOrEqual,
    NotContains,
    NotEquals,
}