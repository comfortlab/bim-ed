﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.CableRouting.Utils;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic.ViewFilters;

internal class FilterManager
{
    private const string FilterPrefix = "ED";
    private readonly Document _document;
    private readonly int _revitVersion;

    internal FilterManager(Document document, int revitVersion)
    {
        _document = document;
        _revitVersion = revitVersion;
    }

    internal void ClearFilters(string filterKey, out Dictionary<ElementId, OverrideGraphicSettings> existFilterGraphicSettings)
    {
        var view = _document.ActiveView;
        
        RemoveFilters(view, filterKey);

        existFilterGraphicSettings = view.GetFilters().ToDictionary(n => n, view.GetFilterOverrides);

        foreach (var elementId in existFilterGraphicSettings.Keys)
            view.RemoveFilter(elementId);
    }

    internal void RemoveFilters(View view, params string[] filterKeys)
    {
        var filterIds = _document.GetElementsOf<ParameterFilterElement>()
            .Where(filter => filter.Name.StartsWith(FilterPrefix) && filterKeys.Any(key => filter.Name.EndsWith(key)))
            .Select(filter => filter.Id);

        foreach (var elementId in filterIds.Where(view.IsFilterApplied))
            view.RemoveFilter(elementId);
    }

    internal void RemoveFilters(params string[] filterKeys)
    {
        var filterIds = _document.GetElementsOf<ParameterFilterElement>()
            .Where(filter => filter.Name.StartsWith(FilterPrefix) && filterKeys.Any(key => filter.Name.EndsWith(key)))
            .Select(filter => filter.Id)
            .ToList();

        _document.Delete(filterIds);
    }

    internal void AddExistFilters(Dictionary<ElementId, OverrideGraphicSettings> existFilterGraphicSettings)
    {
        if (existFilterGraphicSettings == null)
            return;

        var view = _document.ActiveView;
        
        foreach (var filterGraphicSetting in existFilterGraphicSettings)
        {
            var idFilter = filterGraphicSetting.Key;
            view.AddFilter(idFilter);
            view.SetFilterOverrides(idFilter, filterGraphicSetting.Value);
        }
    }

    public void SetFilters(string name,
        string filterKey,
        IReadOnlyCollection<FilterRuleParameter> filterRuleParameters,
        FilterGraphicSettings filterGraphicSettings,
        params BuiltInCategory[] categories)
    {
        var categoryIds = categories.Select(n => new ElementId(n)).ToList();

        if (!SetParameterIds(filterRuleParameters, categoryIds))
            return;

        var filterName = $"{FilterPrefix}_{name}_{filterKey}";
        var rules = filterRuleParameters.Where(x => x.ParameterId != null).Select(x => x.CreateFilterRule()).ToList();

        SetFilter(filterName, categoryIds, rules, filterGraphicSettings);
    }

    private bool SetParameterIds(IReadOnlyCollection<FilterRuleParameter> filterRuleParameters, ICollection<ElementId> categoryIds)
    {
        var guids = filterRuleParameters.Select(n => n.SharedParameterGuid);
        var parameterIds = _document.GetElementsOf<SharedParameterElement>()
            .Where(n => guids.Contains(n.GuidValue))
            .ToDictionary(n => n.GuidValue, n => n.Id);
        var parametersOfCategories = ParameterFilterUtilities
            .GetFilterableParametersInCommon(_document, categoryIds);

        if (!parameterIds.Any() ||
            !parametersOfCategories.Intersect(parameterIds.Values).Any())
            return false;

        foreach (var filterRuleParameter in filterRuleParameters)
        {
            var guid = filterRuleParameter.SharedParameterGuid;
            if (parameterIds.ContainsKey(guid))
                filterRuleParameter.ParameterId = parameterIds[guid];
        }

        return true;
    }
    
    private void SetFilter(
        string name,
        IList<ElementId> categoryIds,
        IList<FilterRule> rules,
        FilterGraphicSettings filterGraphicSettings)
    {
        try
        {
            var filter = GetFilter(name, categoryIds, rules);
            var graphicSettings = new OverrideGraphicSettings();
            SetGraphicSettings(graphicSettings, filterGraphicSettings);

            _document.ActiveView.SetFilterOverrides(filter.Id, graphicSettings);
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
            Debug.WriteLine(
                $"Categories:\n\t{string.Join("\n\t", categoryIds.Select(n => (BuiltInCategory)n.IntegerValue).ToList())}",
                GetType().Namespace);
        }
    }
    
    private ParameterFilterElement GetFilter(string name, IList<ElementId> categoryIds, IList<FilterRule> rules)
    {
        return _document.GetElementsOf<ParameterFilterElement>().FirstOrDefault(n => n.Name == name) ??
               RevitVersionResolver.CreateParameterFilterElement(_revitVersion, _document, name, categoryIds, rules);
    }

    private void SetGraphicSettings(
        OverrideGraphicSettings graphicSettings,
        FilterGraphicSettings filterGraphicSettings)
    {
        var solidPattern = _document.GetElementsOf<FillPatternElement>().FirstOrDefault(n => n?.GetFillPattern()?.IsSolidFill ?? false);

        switch (filterGraphicSettings)
        {
            case FilterGraphicSettings.Default:
                break;

            case FilterGraphicSettings.Halftone:
                graphicSettings.SetHalftone(true);
                break;

            case FilterGraphicSettings.Invisible:
                RevitVersionResolver.SetSurfaceForegroundPatternVisible(_revitVersion, graphicSettings, false);
                break;

            case FilterGraphicSettings.SolidBlueLight:
                graphicSettings.SetProjectionFill(solidPattern, new Color(050, 200, 255), _revitVersion);
                break;

            case FilterGraphicSettings.SolidGreen:
                graphicSettings.SetProjectionFill(solidPattern, new Color(000, 255, 000), _revitVersion);
                break;

            case FilterGraphicSettings.SolidGreenDark:
                graphicSettings.SetProjectionFill(solidPattern, new Color(000, 128, 000), _revitVersion);
                break;

            case FilterGraphicSettings.SolidGreenLight:
                graphicSettings.SetProjectionFill(solidPattern, new Color(192, 255, 192), _revitVersion);
                break;

            case FilterGraphicSettings.SolidOrange:
                graphicSettings.SetProjectionFill(solidPattern, new Color(255, 128, 000), _revitVersion);
                break;

            case FilterGraphicSettings.SolidRed:
                graphicSettings.SetProjectionFill(solidPattern, new Color(255, 000, 000), _revitVersion);
                break;

            case FilterGraphicSettings.SolidRedDark:
                graphicSettings.SetProjectionFill(solidPattern, new Color(128, 000, 000), _revitVersion);
                break;

            case FilterGraphicSettings.SolidWhite:
                graphicSettings.SetProjectionFill(solidPattern, new Color(255, 255, 255), _revitVersion);
                break;

            case FilterGraphicSettings.SolidYellow:
                graphicSettings.SetProjectionFill(solidPattern, new Color(255, 255, 000), _revitVersion);
                break;

            case FilterGraphicSettings.LinesRed:
                graphicSettings.SetProjectionLineColor(new Color(255, 0, 0));
                break;
            
            default:
                throw new ArgumentOutOfRangeException(nameof(filterGraphicSettings), filterGraphicSettings, null);
        }
    }
}