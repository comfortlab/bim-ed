namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic.ViewFilters;

internal enum FilterGraphicSettings
{
    Default,
    Halftone,
    Invisible,
    LinesRed,
    SolidBlueLight,
    SolidGreen,
    SolidGreenLight,
    SolidOrange,
    SolidRed,
    SolidRedDark,
    SolidWhite,
    SolidYellow,
    SolidGreenDark
}