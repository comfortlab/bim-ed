using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic;

public class BuildingConverter
{
    private readonly Document _document;
    private readonly IMapper _mapper;

    public BuildingConverter(Document document, IMapper mapper)
    {
        _document = document;
        _mapper = mapper;
    }

    public BuildingProxy Convert()
    {
        var levels = new FilteredElementCollector(_document)
            .OfCategory(BuiltInCategory.OST_Levels)
            .WhereElementIsNotElementType()
            .OfType<Level>()
            .OrderByDescending(n => n.Elevation)
            .Select(level => _mapper.Map<LevelProxy>(level))
            .ToList();
        
        var rooms = new FilteredElementCollector(_document)
            .OfCategory(BuiltInCategory.OST_Rooms)
            .WhereElementIsNotElementType()
            .OfType<Room>()
            .Where(room => room.Location != null)
            .Select(room => _mapper.Map<RoomProxy>(room))
            .ToList();

        foreach (var room in rooms)
        {
            var level = levels.FirstOrDefault(level => level.RevitId == room.LevelId);
            
            if (level == null) continue;

            room.Level = level;
            level.Rooms.Add(room);
        }
        
        return new BuildingProxy(levels, rooms);
    }
}