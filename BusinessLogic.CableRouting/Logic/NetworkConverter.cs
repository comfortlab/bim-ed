﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.CableRouting.Model;
using CoLa.BimEd.BusinessLogic.CableRouting.Utils;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.BusinessLogic.CableRouting.Logic;

public class NetworkConverter : ProxyConverter<TraceNetwork>
{
    private readonly BuildingProxy _building;
    private readonly Stack<Element> _stackElements = new();
    private List<CableTrayConduitBase> _cableTrayConduits;
    
    public NetworkConverter(Document document, IMapper mapper, BuildingProxy building) : base(document, mapper)
    {
        _building = building;
    }

    public override List<TraceNetwork> Convert()
    {
        var cableTrays = new FilteredElementCollector(Document).OfClass(typeof(CableTray)).WhereElementIsNotElementType().OfType<CableTray>();
        var conduits = new FilteredElementCollector(Document).OfClass(typeof(Conduit)).WhereElementIsNotElementType().OfType<Conduit>();
        _cableTrayConduits = new List<CableTrayConduitBase>(cableTrays.OfType<CableTrayConduitBase>().Concat(conduits));
        
        var traceNetworks = new List<TraceNetwork>();
        var idTraceNetwork = 0;
        
        while (_cableTrayConduits.Any())
        {
            var firstCableTrayConduit = _cableTrayConduits.ElementAt(0);
            _cableTrayConduits.RemoveAt(0);

            traceNetworks.Add(Build(idTraceNetwork++, firstCableTrayConduit));
        }

        return traceNetworks;
    }

    private TraceNetwork Build(int idTraceNetwork, CableTrayConduitBase firstCableTrayConduit)
    {
        var traceNetwork = new TraceNetwork(idTraceNetwork);
        _stackElements.Push(firstCableTrayConduit);

        while (_stackElements.Any())
        {
            var currentElement = _stackElements.Pop();

            var elementToRemove = _cableTrayConduits.FirstOrDefault(n => n.Id == currentElement.Id);

            if (elementToRemove != null)
                _cableTrayConduits.Remove(elementToRemove);

            var traceElement = GetTraceElement(traceNetwork, currentElement);

            foreach (var connector in RevitConnectorUtils.GetConnectors(currentElement, Domain.DomainCableTrayConduit).OrderBy(n => n.Id))
                AddTraceElement(traceNetwork, traceElement, connector);
        }

        return traceNetwork;
    }

    private void AddTraceElement(TraceNetwork traceNetwork, TraceElement traceElement, Connector connector)
    {
        if (connector.ConnectorType == ConnectorType.MasterSurface ||
            !connector.IsConnected ||
            traceElement.ContainsConnector(connector.Id))
            return;

        var traceConnector = Mapper.Map<ConnectorProxy>(connector);
        traceElement.AddConnector(traceConnector);

        var refConnector = connector.GetRefConnector();

        if (refConnector == null)
            return;

        var refElement = refConnector.Owner;
        var refTraceElement = GetTraceElement(traceNetwork, refElement);

        switch (traceElement)
        {
            case CableTrayConduitBaseProxy cableTrayConduitProxy when refTraceElement is ElectricalElementProxy electricalElement:
                electricalElement.AddTraceElement(cableTrayConduitProxy);
                break;

            case ElectricalElementProxy electricalElement when refTraceElement is CableTrayConduitBaseProxy cableTrayConduitProxy:
                electricalElement.AddTraceElement(cableTrayConduitProxy);
                break;

            case ElectricalElementProxy electricalElement when refTraceElement is { } fitting:
                electricalElement.AddTraceElement(fitting);
                break;

            case { } fitting when refTraceElement is ElectricalElementProxy electricalElement:
                electricalElement.AddTraceElement(fitting);
                break;
        }

        if (refTraceElement == null ||
            refTraceElement.ContainsConnector(refConnector.Id))
            return;

        var refConnectorProxy = Mapper.Map<ConnectorProxy>(refConnector);

        traceConnector.AddRefConnector(refConnectorProxy);
        refTraceElement.AddConnector(refConnectorProxy);

        if (traceNetwork.ElementInNetwork(refTraceElement) == false)
            traceNetwork.AddElement(refTraceElement);

        if (_stackElements.Contains(refElement) == false)
            _stackElements.Push(refElement);
    }

    private TraceElement GetTraceElement(TraceNetwork traceNetwork, Element currentElement)
    {
        var traceElement = traceNetwork.GetElement(currentElement.Id.IntegerValue);

        if (traceElement != null)
            return traceElement;
        
        traceElement = currentElement switch
        {
            CableTray cableTray => Mapper.Map<CableTrayProxy>(cableTray),
            Conduit conduit => Mapper.Map<ConduitProxy>(conduit),
            FamilyInstance familyInstance
                when (BuiltInCategory)familyInstance.Category.Id.IntegerValue
                is BuiltInCategory.OST_CableTray
                or BuiltInCategory.OST_ConduitFitting =>
                Mapper.Map<CableTrayConduitFittingProxy>(familyInstance),
            FamilyInstance familyInstance => Mapper.Map<ElectricalElementProxy>(familyInstance),
            _ => throw new ArgumentOutOfRangeException(nameof(currentElement), currentElement, null)
        };

        if (traceElement is CableTrayConduitBaseProxy cableTrayConduit)
        {
            AddLevels(cableTrayConduit);
            AddRooms(cableTrayConduit);
        }
        
        return traceNetwork.AddElement(traceElement);
    }

    private void AddLevels(CableTrayConduitBaseProxy cableTrayConduit)
    {
        var levels = _building.GetLevels(cableTrayConduit.Point1, cableTrayConduit.Point2);

        foreach (var level in levels.Where(level => !cableTrayConduit.Levels.Contains(level)))
        {
            cableTrayConduit.Levels.Add(level);
        }
    }

    private void AddRooms(CableTrayConduitBaseProxy cableTrayConduit)
    {
        cableTrayConduit.Rooms.AddRange(_building.GetRooms(cableTrayConduit));
    }
}