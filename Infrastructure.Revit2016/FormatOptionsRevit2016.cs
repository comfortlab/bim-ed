﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class FormatOptionsRevit2016
    {
        public static void SetMetersAsDisplayUnitType(
            FormatOptions formatOptions)
        {
            formatOptions.DisplayUnits = DisplayUnitType.DUT_METERS;
        }
    }
}