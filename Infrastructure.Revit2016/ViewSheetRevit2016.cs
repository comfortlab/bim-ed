﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class ViewSheetRevit2016
    {
        public static List<ElementId> GetGenericAnnotationElements(ViewSheet viewSheet) =>
            new FilteredElementCollector(viewSheet.Document)
                .OfCategory(BuiltInCategory.OST_GenericAnnotation)
                .Where(n => n.OwnerViewId == viewSheet.Id).Select(n => n.Id).ToList();
    }
}