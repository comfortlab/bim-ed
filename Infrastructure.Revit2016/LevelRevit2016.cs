﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class LevelRevit2016
    {
        public static IList<ElementId> GetViewPlans(Level level) =>
            new FilteredElementCollector(level.Document)
                .OfClass(typeof(ViewPlan)).OfType<ViewPlan>()
                .Where(n => n.GenLevel.Id.IntegerValue == level.Id.IntegerValue)
                .Select(n => n.Id).ToList();
    }
}