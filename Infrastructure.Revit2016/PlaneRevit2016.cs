﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class PlaneRevit2016
    {
        public static Plane CreateByNormalAndOrigin(
            XYZ normal,
            XYZ origin)
        {
            return new Plane(normal, origin);
        }
    }
}