﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class InternalDefinitionRevit2016
    {
        public static bool Equals(
            InternalDefinition internalDefinition1,
            InternalDefinition internalDefinition2)
        {
            return internalDefinition1.Name == internalDefinition2.Name;
        }
    }
}