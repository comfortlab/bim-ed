﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class ElectricalSystemRevit2016
    {
        public static ElectricalSystem Create(
            Document document,
            Connector connector,
            ElectricalSystemType electricalSystemType)
        {
            return document.Create.NewElectricalSystem(
                connector,
                electricalSystemType);
        }
        
        public static ElectricalSystem Create(
            Document document,
            ICollection<ElementId> elementIds,
            ElectricalSystemType electricalSystemType)
        {
            return document.Create.NewElectricalSystem(
                elementIds,
                electricalSystemType);
        }
    }
}