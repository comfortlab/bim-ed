﻿using Autodesk.Revit.DB;

namespace CoLa.BimEd.Infrastructure.Revit2016
{
    public class UnitUtilsRevit2016
    {
        public static double ConvertFromInternalUnitsToMillimeters(double value) =>
            UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);

        public static double ConvertFromInternalUnitsToOhmMeters(double value) =>
            UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_OHM_METERS);

        public static double ConvertFromInternalUnitsToVoltAmperes(double value) =>
            UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_VOLT_AMPERES);

        public static double ConvertFromInternalUnitsToVolts(double value) =>
            UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_VOLTS);

        public static double ConvertFromInternalUnitsToWatts(double value) =>
            UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_WATTS);

        public static double ConvertToInternalUnits(double value, Parameter targetParameter) =>
            UnitUtils.ConvertToInternalUnits(value, targetParameter.DisplayUnitType);

        public static double ConvertToInternalUnitsFromMillimeters(double value) =>
            UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);

        public static double ConvertToInternalUnitsFromVolts(double value) =>
            UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_VOLTS);

        public static double ConvertToInternalUnitsFromVoltAmperes(double value) =>
            UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_VOLT_AMPERES);

        public static double ConvertToInternalUnitsFromWatts(double value) =>
            UnitUtils.ConvertToInternalUnits(value, DisplayUnitType.DUT_WATTS);
    }
}