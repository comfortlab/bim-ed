﻿using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Services;

public class ElectricalSystemBuilder
{
    private readonly Repository<VirtualElements> _virtualElementsRepository;
    private readonly Repository<DistributionSystemProxy> _distributionSystems;
    private readonly Repository<LoadClassificationProxy> _loadClassifications;
    private readonly Repository<ElectricalSystemProxy> _electricalSystems;
    private readonly Repository<SwitchBoardUnit> _switchboardUnits;
    private readonly Repository<Transformer> _transformers;
    private VirtualElements VirtualElements => _virtualElementsRepository.First();

    public ElectricalSystemBuilder(
        Repository<VirtualElements> virtualElementsRepository,
        Repository<DistributionSystemProxy> distributionSystems,
        Repository<LoadClassificationProxy> loadClassifications,
        Repository<ElectricalSystemProxy> electricalSystems,
        Repository<SwitchBoardUnit> switchboardUnits,
        Repository<Transformer> transformers)
    {
        _distributionSystems = distributionSystems;
        _electricalSystems = electricalSystems;
        _loadClassifications = loadClassifications;
        _switchboardUnits = switchboardUnits;
        _transformers = transformers;
        _virtualElementsRepository = virtualElementsRepository;
    }

    public void Build()
    {
        _distributionSystems.Load();
        _loadClassifications.Load();
        _virtualElementsRepository.Load();
        _switchboardUnits.Load();
        _transformers.Load();
        _electricalSystems.Load();
        _switchboardUnits.AddRange(VirtualElements.SwitchboardUnits);
        _transformers.AddRange(VirtualElements.Transformers);
        RestoreVirtualElements();
        ConnectToSources();
    }

    private void RestoreVirtualElements()
    {
        foreach (var electricalEquipment in VirtualElements.GetElectricalEquipments())
        {
            var distributionSystemId = electricalEquipment.DistributionSystem?.RevitId ?? -1;
            var distributionSystem = _distributionSystems.FirstOrDefault(x => x.RevitId == distributionSystemId);
            electricalEquipment.SetDistributionSystem(distributionSystem);

            if (electricalEquipment is not Transformer transformer)
                continue;

            var secondaryDistributionSystemId = transformer.SecondaryDistributionSystem?.RevitId ?? -1;
            var secondaryDistributionSystem = _distributionSystems.FirstOrDefault(x => x.RevitId == secondaryDistributionSystemId);
            transformer.SecondaryDistributionSystem = secondaryDistributionSystem;
        }
    }

    private void ConnectToSources()
    {
        var equipments = _transformers.OfType<ElectricalEquipmentProxy>().Concat(_switchboardUnits).ToArray();
        var notConnected = equipments.Where(x => x.Source is null or ElectricalSystemProxy { Source: null }).ToArray();
        var networks = VirtualElements.Networks; 
        
        foreach (var equipment in notConnected)
        {
            var distributionSystem = equipment.DistributionSystem;
            var network = networks.FirstOrDefault(n => distributionSystem.IsCompatible(n.DistributionSystem));

            if (network == null)
            {
                network = new ElectricalNetwork(VirtualElements.NewId(), distributionSystem?.Name, distributionSystem);
                networks.Add(network);
                _virtualElementsRepository.ChangedElements.UpdateStorage();
            }

            equipment.ConnectTo(network);
        }

        _virtualElementsRepository.Save();
    }
}