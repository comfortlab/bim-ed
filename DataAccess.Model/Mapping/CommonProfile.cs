﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Contracts;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Model.Mapping;

public class CommonProfile : Profile
{
    public CommonProfile()
    {
        DisableConstructorMapping();

        CreateMap<ElementProxy, ElementProxyDto>()
            .ReverseMap();
    }
}