﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Model.Mapping;

public class CableProfile : Profile
{
    public CableProfile()
    {
        DisableConstructorMapping();

        CreateMap<ConductorCount, ConductorCountDto>()
            .ForMember(dest => dest.Section, opt => opt.MapFrom(src => src.Section.Round(4)))
            .ReverseMap();

        CreateMap<Cable, CableDto>()
            .ForMember(dest => dest.Diameter, opt => opt.MapFrom(src => src.Diameter))
            .ForMember(dest => dest.PermissibleCurrent, opt => opt.MapFrom(src => (int)src.PermissibleCurrent))
            .ForMember(dest => dest.GroundPermissibleCurrent,
                opt => opt.MapFrom(src => (int)src.GroundPermissibleCurrent))
            .ForMember(dest => dest.R0, opt => opt.MapFrom(src => src.R0.Round(4)))
            .ForMember(dest => dest.R1, opt => opt.MapFrom(src => src.R1.Round(4)))
            .ForMember(dest => dest.X0, opt => opt.MapFrom(src => src.X0.Round(4)))
            .ForMember(dest => dest.X1, opt => opt.MapFrom(src => src.X1.Round(4)))
            .ForMember(dest => dest.WeightPerMeter, opt => opt.MapFrom(src => src.WeightPerMeter.Round(4, null)))
            .ReverseMap();

        CreateMap<CableSeries, CableSeriesDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.ConductorMaterialId, opt => opt.MapFrom(src => src.ConductorMaterial.Id))
            .ForMember(dest => dest.FireResistant, opt => opt.MapFrom(src => src.Insulation.FireResistant))
            .ForMember(dest => dest.LowSmoke, opt => opt.MapFrom(src => src.Insulation.LowSmoke))
            .ForMember(dest => dest.LowToxic, opt => opt.MapFrom(src => src.Insulation.LowToxic))
            .ForMember(dest => dest.ZeroHalogen, opt => opt.MapFrom(src => src.Insulation.ZeroHalogen))
            .ForMember(dest => dest.CableProperties, opt => opt.MapFrom(src => src.CableProperties != null ? string.Join(";", src.CableProperties.Select(n => (int)n)) : string.Empty))
            .ReverseMap()
            .ForPath(src => src.Id, opt => opt.MapFrom(dest => dest.Id))
            .ForPath(src => src.ConductorMaterial.Id, opt => opt.MapFrom(dest => dest.ConductorMaterialId))
            .ForPath(src => src.Insulation.FireResistant, opt => opt.MapFrom(dest => dest.FireResistant))
            .ForPath(src => src.Insulation.LowSmoke, opt => opt.MapFrom(dest => dest.LowSmoke))
            .ForPath(src => src.Insulation.LowToxic, opt => opt.MapFrom(dest => dest.LowToxic))
            .ForPath(src => src.Insulation.ZeroHalogen, opt => opt.MapFrom(dest => dest.ZeroHalogen))
            .ForPath(src => src.CableProperties, opt => opt.MapFrom(dest => !string.IsNullOrWhiteSpace(dest.CableProperties) ? dest.CableProperties.Split(';').Select(n => (CableProperty)int.Parse(n)) : new List<CableProperty>()));
    }
}