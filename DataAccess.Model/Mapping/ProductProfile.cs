﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Model.Mapping;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        DisableConstructorMapping();
        
        CreateMap<Product, ProductDto>()
            .ReverseMap();
        
        CreateMap<SelectorProduct, SelectorProductDto>()
            .ReverseMap();
        
        CreateMap<CircuitProducts, CircuitProductsDto>()
            .ForMember(
                dest => dest.CableSeriesId,
                opt => opt.MapFrom(src => src.Cable.CableSeries.Id))
            .ForMember(
                dest => dest.CableId,
                opt => opt.MapFrom(src => src.Cable.Id))
            .ReverseMap()
            .ForPath(
                dest => dest.Cable.CableSeries.Id,
                opt => opt.MapFrom(src => src.CableSeriesId))
            .ForPath(
                dest => dest.Cable.Id,
                opt => opt.MapFrom(src => src.CableId));
        
        CreateMap<SwitchBoardUnitProducts, SwitchboardUnitProductsDto>()
            .ReverseMap();
        
        CreateMap<TransformerProduct, TransformerProductDto>()
            .ForMember(dest => dest.TransformerSeriesGuid, opt => opt.MapFrom(src => src.TransformerSeries != null ? src.TransformerSeries.Guid : Guid.Empty))
            .ReverseMap()
            .ForPath(src => src.TransformerSeries.Guid, opt => opt.MapFrom(dest => dest.TransformerSeriesGuid));
    }
}