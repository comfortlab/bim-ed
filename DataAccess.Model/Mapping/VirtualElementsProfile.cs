using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Model.Mapping;

public class VirtualElementsProfile : Profile
{
    public VirtualElementsProfile()
    {
        DisableConstructorMapping();

        CreateMap<ElectricalEquipmentProxy, ElectricalEquipmentDto>()
            .ForMember(dest => dest.DistributionSystemId, opt => opt.MapFrom(src => src.DistributionSystem != null ? src.DistributionSystem.RevitId : -1))
            .ReverseMap()
            .ForPath(src => src.DistributionSystem.RevitId, opt => opt.MapFrom(dest => dest.DistributionSystemId));

        CreateMap<ElectricalGenerator, ElectricalEquipmentDto>()
            .IncludeBase<ElectricalEquipmentProxy, ElectricalEquipmentDto>()
            .ReverseMap();

        CreateMap<ElectricalNetwork, ElectricalEquipmentDto>()
            .IncludeBase<ElectricalEquipmentProxy, ElectricalEquipmentDto>()
            .ReverseMap()
            .ForPath(src => src.BaseConnector.SystemType, opt => opt.MapFrom(dest => dest.SystemType));
        
        CreateMap<Transformer, TransformerDto>()
            .IncludeBase<ElectricalEquipmentProxy, ElectricalEquipmentDto>()
            .ForMember(dest => dest.SecondaryDistributionSystemId, opt => opt.MapFrom(src => src.SecondaryDistributionSystem != null ? src.SecondaryDistributionSystem.RevitId : -1))
            .ReverseMap()
            .ForPath(src => src.SecondaryDistributionSystem.RevitId, opt => opt.MapFrom(dest => dest.SecondaryDistributionSystemId));
        
        CreateMap<SwitchBoardUnit, ElectricalEquipmentDto>()
            .IncludeBase<ElectricalEquipmentProxy, ElectricalEquipmentDto>()
            .ReverseMap();
        
        CreateMap<VirtualElements, VirtualElementsDto>()
            .ReverseMap();
    }
}