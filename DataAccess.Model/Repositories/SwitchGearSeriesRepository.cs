﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public sealed class SwitchGearSeriesRepository : MemoryRepository<SwitchGearSeries>
{
    public SwitchGearSeriesRepository(
        Repository<SwitchGearFunction> switchGearFunctions) :
        base(new SwitchGearSeriesInitializer(switchGearFunctions)) { }
}