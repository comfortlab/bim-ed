﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.Repository;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public class LoadClassificationDirectoryRepository : MemoryRepository<LoadClassificationDirectory>
{
    public LoadClassificationDirectoryRepository() :
        base(new LoadClassificationDirectoryInitializer()) { }

    private class LoadClassificationDirectoryInitializer : IRepositoryInitializer<LoadClassificationDirectory>
    {
        private readonly LoadClassificationDirectory _directory = new();

        public List<LoadClassificationDirectory> Initialize()
        {
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_1, 1);
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_2_1, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(1,        0,    5),
                new DemandFactorValueProxy(0.622,    5,    6),
                new DemandFactorValueProxy(0.511,    6,    9),
                new DemandFactorValueProxy(0.444,    9,   12),
                new DemandFactorValueProxy(0.4,     12,   15),
                new DemandFactorValueProxy(0.377,   15,   18),
                new DemandFactorValueProxy(0.311,   18,   24),
                new DemandFactorValueProxy(0.267,   24,   40),
                new DemandFactorValueProxy(0.233,   40,   60),
                new DemandFactorValueProxy(0.189,   60,  100),
                new DemandFactorValueProxy(0.171,  100,  200),
                new DemandFactorValueProxy(0.158,  200,  400),
                new DemandFactorValueProxy(0.153,  400,  600),
                new DemandFactorValueProxy(0.149,  600, 1000),
                new DemandFactorValueProxy(0.149, 1000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_2_2, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(1,        0,    5),
                new DemandFactorValueProxy(0.567,    5,    6),
                new DemandFactorValueProxy(0.483,    6,    9),
                new DemandFactorValueProxy(0.417,    9,   12),
                new DemandFactorValueProxy(0.367,   12,   15),
                new DemandFactorValueProxy(0.333,   15,   18),
                new DemandFactorValueProxy(0.3,     18,   24),
                new DemandFactorValueProxy(0.233,   24,   40),
                new DemandFactorValueProxy(0.217,   40,   60),
                new DemandFactorValueProxy(0.18,    60,  100),
                new DemandFactorValueProxy(0.167,  100,  200),
                new DemandFactorValueProxy(0.153,  200,  400),
                new DemandFactorValueProxy(0.14,   400,  600),
                new DemandFactorValueProxy(0.127,  600, 1000),
                new DemandFactorValueProxy(0.127, 1000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_2_3, DemandFactorRuleProxy.QuantityTable, 
                new DemandFactorValueProxy(1,        0,    5),
                new DemandFactorValueProxy(0.51,     5,    6),
                new DemandFactorValueProxy(0.38,     6,    9),
                new DemandFactorValueProxy(0.32,     9,   12),
                new DemandFactorValueProxy(0.28,    12,   15),
                new DemandFactorValueProxy(0.26,    15,   18),
                new DemandFactorValueProxy(0.22,    18,   24),
                new DemandFactorValueProxy(0.195,   24,   40),
                new DemandFactorValueProxy(0.17,    40,   60),
                new DemandFactorValueProxy(0.15,    60,  100),
                new DemandFactorValueProxy(0.136,  100,  200),
                new DemandFactorValueProxy(0.127,  200,  400),
                new DemandFactorValueProxy(0.123,  400,  600),
                new DemandFactorValueProxy(0.119,  600, 1000),
                new DemandFactorValueProxy(0.119, 1000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_2_4, DemandFactorRuleProxy.QuantityTable, 
                new DemandFactorValueProxy(1,        0,    5),
                new DemandFactorValueProxy(0.575,    5,    6),
                new DemandFactorValueProxy(0.425,    6,    9),
                new DemandFactorValueProxy(0.35,     9,   12),
                new DemandFactorValueProxy(0.3,     12,   15),
                new DemandFactorValueProxy(0.275,   15,   18),
                new DemandFactorValueProxy(0.225,   18,   24),
                new DemandFactorValueProxy(0.19,    24,   40),
                new DemandFactorValueProxy(0.1725,  40,   60),
                new DemandFactorValueProxy(0.1525,  60,  100),
                new DemandFactorValueProxy(0.145,  100,  200),
                new DemandFactorValueProxy(0.135,  200,  400),
                new DemandFactorValueProxy(0.1275, 400,  600),
                new DemandFactorValueProxy(0.115,  600, 1000),
                new DemandFactorValueProxy(0.115, 1000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_2_5, DemandFactorRuleProxy.QuantityTable, 
                new DemandFactorValueProxy(1,      0,   5),
                new DemandFactorValueProxy(0.51,   5,   6),
                new DemandFactorValueProxy(0.38,   6,   9),
                new DemandFactorValueProxy(0.32,   9,  12),
                new DemandFactorValueProxy(0.29,  12,  15),
                new DemandFactorValueProxy(0.26,  15,  18),
                new DemandFactorValueProxy(0.24,  18,  24),
                new DemandFactorValueProxy(0.2,   24,  40),
                new DemandFactorValueProxy(0.18,  40,  60),
                new DemandFactorValueProxy(0.16,  60, 100),
                new DemandFactorValueProxy(0.14, 100, 200),
                new DemandFactorValueProxy(0.13, 200, 400),
                new DemandFactorValueProxy(0.11, 400, 600),
                new DemandFactorValueProxy(0.11, 600));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_3, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(1,      0,   5),
                new DemandFactorValueProxy(0.9,    5,  10),
                new DemandFactorValueProxy(0.85,  10,  15),
                new DemandFactorValueProxy(0.8,   15,  25),
                new DemandFactorValueProxy(0.7,   25,  50),
                new DemandFactorValueProxy(0.65,  50, 100),
                new DemandFactorValueProxy(0.6,  100, 200),
                new DemandFactorValueProxy(0.55, 200, 250),
                new DemandFactorValueProxy(0.55, 250));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_4, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,      0,  10),
                new DemandFactorValueProxy(0.9,   10,  20),
                new DemandFactorValueProxy(0.8,   20,  50),
                new DemandFactorValueProxy(0.7,   50, 100),
                new DemandFactorValueProxy(0.36, 100, 200),
                new DemandFactorValueProxy(0.3,  200, 400),
                new DemandFactorValueProxy(0.24, 400, 600),
                new DemandFactorValueProxy(0.21, 600, 650),
                new DemandFactorValueProxy(0.21, 650));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_5, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(1,      0,   1),
                new DemandFactorValueProxy(0.9,    1,   2),
                new DemandFactorValueProxy(0.4,    2,  20),
                new DemandFactorValueProxy(0.2,   20, 100),
                new DemandFactorValueProxy(0.15, 100, 200),
                new DemandFactorValueProxy(0.15, 200));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_7_1, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.8,   0,  3),
                new DemandFactorValueProxy(0.7,   3,  5),
                new DemandFactorValueProxy(0.65,  5,  6),
                new DemandFactorValueProxy(0.5,   6, 10),
                new DemandFactorValueProxy(0.4,  10, 20),
                new DemandFactorValueProxy(0.35, 20, 25),
                new DemandFactorValueProxy(0.35, 25));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_7_2, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.9,   0,  3),
                new DemandFactorValueProxy(0.8,   3,  5),
                new DemandFactorValueProxy(0.75,  5,  6),
                new DemandFactorValueProxy(0.6,   6, 10),
                new DemandFactorValueProxy(0.5,  10, 20),
                new DemandFactorValueProxy(0.4,  20, 25),
                new DemandFactorValueProxy(0.4,  25));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_8_1, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(1,      0,   2),
                new DemandFactorValueProxy(0.9,    2,   3),
                new DemandFactorValueProxy(0.8,    3,   5),
                new DemandFactorValueProxy(0.75,   5,   8),
                new DemandFactorValueProxy(0.7,    8,  10),
                new DemandFactorValueProxy(0.65,  10,  20),
                new DemandFactorValueProxy(0.6,   20,  30),
                new DemandFactorValueProxy(0.55,  30, 100),
                new DemandFactorValueProxy(0.5,  100, 200),
                new DemandFactorValueProxy(0.5,  200));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_8_2, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.75,   0,   5),
                new DemandFactorValueProxy(0.7,    5,   8),
                new DemandFactorValueProxy(0.65,   8,  10),
                new DemandFactorValueProxy(0.6,   10,  30),
                new DemandFactorValueProxy(0.55,  30, 100),
                new DemandFactorValueProxy(0.5,  100, 200),
                new DemandFactorValueProxy(0.5,  200));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_8_3, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.7,    0,   5),
                new DemandFactorValueProxy(0.65,   5,  10),
                new DemandFactorValueProxy(0.6,   10,  20),
                new DemandFactorValueProxy(0.55,  20,  30),
                new DemandFactorValueProxy(0.5,   30, 100),
                new DemandFactorValueProxy(0.45, 100, 200),
                new DemandFactorValueProxy(0.45, 200));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_8_4, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.65,  0,  5),
                new DemandFactorValueProxy(0.6,   5, 10),
                new DemandFactorValueProxy(0.55, 10, 15),
                new DemandFactorValueProxy(0.5,  15, 50),
                new DemandFactorValueProxy(0.45, 50));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_1_8_5, DemandFactorRuleProxy.QuantityTable,
                new DemandFactorValueProxy(0.6,    0,   8),
                new DemandFactorValueProxy(0.55,   8,  10),
                new DemandFactorValueProxy(0.5,   10,  30),
                new DemandFactorValueProxy(0.45,  30, 100),
                new DemandFactorValueProxy(0.4,  100, 200),
                new DemandFactorValueProxy(0.4,  200));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_1, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,         0,   5000),
                new DemandFactorValueProxy(0.8,    5000,  10000),
                new DemandFactorValueProxy(0.7,   10000,  15000),
                new DemandFactorValueProxy(0.6,   15000,  25000),
                new DemandFactorValueProxy(0.5,   25000,  50000),
                new DemandFactorValueProxy(0.4,   50000, 100000),
                new DemandFactorValueProxy(0.35, 100000, 200000),
                new DemandFactorValueProxy(0.3,  200000, 400000),
                new DemandFactorValueProxy(0.3, 400000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_2, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,         0,   5000),
                new DemandFactorValueProxy(0.9,    5000,  10000),
                new DemandFactorValueProxy(0.85,  10000,  15000),
                new DemandFactorValueProxy(0.8,   15000,  25000),
                new DemandFactorValueProxy(0.75,  25000,  50000),
                new DemandFactorValueProxy(0.7,   50000, 100000),
                new DemandFactorValueProxy(0.65, 100000, 200000),
                new DemandFactorValueProxy(0.6,  200000, 400000),
                new DemandFactorValueProxy(0.5,  400000, 500000),
                new DemandFactorValueProxy(0.5,  500000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_3, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,         0,   5000),
                new DemandFactorValueProxy(0.95,   5000,  10000),
                new DemandFactorValueProxy(0.9,   10000,  15000),
                new DemandFactorValueProxy(0.85,  15000,  25000),
                new DemandFactorValueProxy(0.8,   25000,  50000),
                new DemandFactorValueProxy(0.75,  50000, 100000),
                new DemandFactorValueProxy(0.7,  100000, 200000),
                new DemandFactorValueProxy(0.65, 200000, 400000),
                new DemandFactorValueProxy(0.6,  400000, 500000),
                new DemandFactorValueProxy(0.6,  500000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_4, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,         0,   5000),
                new DemandFactorValueProxy(1,      5000,  10000),
                new DemandFactorValueProxy(0.95,  10000,  15000),
                new DemandFactorValueProxy(0.9,   15000,  25000),
                new DemandFactorValueProxy(0.85,  25000,  50000),
                new DemandFactorValueProxy(0.8,   50000, 100000),
                new DemandFactorValueProxy(0.75, 100000, 200000),
                new DemandFactorValueProxy(0.7,  200000, 400000),
                new DemandFactorValueProxy(0.65, 400000, 500000),
                new DemandFactorValueProxy(0.65, 500000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_5, 1d);
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_6, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,         0,   5000),
                new DemandFactorValueProxy(0.9,    5000,  10000),
                new DemandFactorValueProxy(0.8,   10000,  15000),
                new DemandFactorValueProxy(0.75,  15000,  25000),
                new DemandFactorValueProxy(0.7,   25000,  50000),
                new DemandFactorValueProxy(0.65,  50000, 100000),
                new DemandFactorValueProxy(0.55, 100000, 200000),
                new DemandFactorValueProxy(0.55, 200000));
            Add(LoadClassificationDirectorySp256.Sp256_1325800_2016_7_2_1_7, DemandFactorRuleProxy.LoadTable,
                new DemandFactorValueProxy(1,        0,   5000),
                new DemandFactorValueProxy(0.9,   5000,  10000),
                new DemandFactorValueProxy(0.8,  10000,  15000),
                new DemandFactorValueProxy(0.7,  15000,  25000),
                new DemandFactorValueProxy(0.65, 25000,  50000),
                new DemandFactorValueProxy(0.6,  50000, 100000),
                new DemandFactorValueProxy(0.5, 100000, 200000),
                new DemandFactorValueProxy(0.5, 200000));
            
            return new List<LoadClassificationDirectory> { _directory };
        }

        private void Add(string name, double constantValue) =>
            Add(new DemandFactorProxy(0, name, constantValue));

        private void Add(string name, DemandFactorRuleProxy demandFactorRuleProxy, params DemandFactorValueProxy[] values) =>
            Add(new DemandFactorProxy(0, name, demandFactorRuleProxy, values));

        private void Add(DemandFactorProxy demandFactor)
        {
            var loadClassification = new LoadClassificationProxy(0, demandFactor.Name, demandFactor);
            _directory.DemandFactors.Add(demandFactor);
            _directory.LoadClassifications.Add(loadClassification);
        }
    }
}