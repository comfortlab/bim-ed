﻿// ReSharper disable InconsistentNaming

using System;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public static class Guids
{
    public static class Products
    {
        public static class SwitchGears
        {
            public class RM6
            {
                public static readonly Guid Series    = new("675B4D8D-3987-4BB9-B28D-E19551A12BEE");
                public static readonly Guid RM6_Empty = new("4E5D0939-8ECD-459D-ADDB-F0337B8E4B7C");
                public static readonly Guid RM6_B     = new("F8AC526C-C6F2-47C6-B991-9D3E35AB79B9");
                public static readonly Guid RM6_BC    = new("E39A96A6-228A-4DAC-B2FC-9DAE3663D56D");
                public static readonly Guid RM6_D     = new("430173BD-1AE2-45D6-816B-27ECE2B816E5");
                public static readonly Guid RM6_I     = new("576FF7EC-A260-4E25-87E8-2E2B27B7413B");
                public static readonly Guid RM6_IC    = new("9392ABF9-1E11-470E-9FA3-7C308F43C8CE");
                public static readonly Guid RM6_Mt    = new("E9F51D9E-96C9-498D-8EEC-DE6AF3CD9CC5");
                public static readonly Guid RM6_O     = new("54B614DA-0602-45F5-914E-08BF0A0A684D");
                public static readonly Guid RM6_Q     = new("304F867E-F40B-48BA-B171-17E6E7A52864");
            }
            public static class PIX
            {
                public static readonly Guid Series     = new("34A011FB-F2B9-4960-BF82-72B7F59DC511");
            }

            public static class PremSet
            {
                public static readonly Guid Series = new("DE89753C-89F8-4757-921F-A9B201D06D22");
            }

            public static class SM6
            {
                public static readonly Guid Series = new("1C262C34-2306-4A0E-88E1-6E543CBE88F0");
            }
            
            
            public static class AuxiliaryContacts
            {
                public static class RM6
                {
                    public static readonly Guid CircuitBreakerPositionAuxContact         = new("7CF53EF2-799E-4C33-B891-11A7269FD148");
                    public static readonly Guid ForbiddenClosingUnderFaultAuxContact     = new("4CE7EA58-A88B-4CF3-8DEC-B831FA34A499");
                    public static readonly Guid FuseBlownAuxContact                      = new("E0BDD8A9-6676-40AF-8DC8-41615560EDE9");
                    public static readonly Guid FuseSwitchCombinationsPositionAuxContact = new("37A043A2-73C5-46A6-9592-FB97414DA458");
                    public static readonly Guid MainSwitchPositionAuxContact             = new("277D5B10-3C0A-4429-BC40-E68EAF10950F");
                    public static readonly Guid TrippingIndicationAuxContact             = new("E19980A6-F7E4-41EE-9A20-7B31A40FA78B");
                }
            }

            public static class CurrentTransformers
            {
                public static readonly Guid CUa                      = new("64EA049E-871F-457B-98B1-1D8AA7402B0A");
                public static readonly Guid CUb                      = new("5602FBAB-D54A-4337-AE3D-5FECDC1C2C86");
                public static readonly Guid CTR2200                  = new("5AD1E3D5-2E67-4B69-8AC1-60E9AF5C0CDD");
                public static readonly Guid CTRH2200                 = new("D92F58B7-7E02-4D29-86C5-7B69D34EBEB6");
                public static readonly Guid EasergySCTR500_1_class1  = new("AC840DFE-C3D6-4759-A517-DD0C9B9FB7AC");
                public static readonly Guid EasergySCTR500_1_class3  = new("89292C51-B36B-42D9-A18E-D1AAC5C687CB");
                public static readonly Guid EasergySCTRH500_1_class3 = new("7801AE61-784A-44AC-AF44-75607897270A");
            }

            public static class FaultPassageIndicators
            {
                public static readonly Guid Flair21D  = new("370C22C3-AA5E-4856-843C-1210C4E61131");
                public static readonly Guid Flair22D  = new("6C92B427-871F-4214-9839-D44EF39A6F2C");
                public static readonly Guid Flair23D  = new("5EE98ADC-4F8B-4140-A42D-580BBEB53AEE");
                public static readonly Guid Flair23DM = new("BA7D433D-10E8-4941-A42D-B544E4CC5A63");
            }

            public static class MeasuringVoltageTransformers
            {
                public static readonly Guid LPVT = new("7EDB6890-B6C2-42D5-9491-58C9379B5477");
            }

            public static class MotorMechanisms
            {
                public static class RM6
                {
                    public static readonly Guid DC24  = new("EBF827AC-B0A6-4261-AABE-91341A1719FC");
                    public static readonly Guid DC48  = new("ABC5A0A0-54FE-40BD-B5E9-91A2EBA9EF41");
                    public static readonly Guid DC60  = new("F8D05DD7-A950-4EF8-88DF-30A7772A9D88");
                    public static readonly Guid DC110 = new("C5B8304F-A9EE-449F-A864-7358AF0FF3B6");
                    public static readonly Guid DC125 = new("47F8BE7B-1182-4432-990A-F42FC31E7D77");
                    public static readonly Guid DC220 = new("1B33D88C-E306-4BFB-AE57-83627229D2E7");
                    public static readonly Guid AC120 = new("9C52C515-F05B-40C9-9887-D3BA1755F827");
                    public static readonly Guid AC230 = new("731E2B51-1AB5-478C-9DA9-829E3DD2547F");
                }
            }

            public static class ProtectionRelays
            {
                public static readonly Guid VIP40_100 = new("DF2F7B81-6AE3-4EB8-B7C5-2246572CFB3B");
                public static readonly Guid VIP40_200 = new("3276601E-7A51-4812-876C-45E82EC33646");
                public static readonly Guid VIP45_100 = new("685DD921-97B4-4FE2-ABAE-ADDAA6B119ED");
                public static readonly Guid VIP45_200 = new("21594C1E-7FF9-4BF3-861C-06DF5860039B");
                public static readonly Guid VIP400    = new("4580E6E0-1E05-491F-99B5-2AC688521A4C");
                public static readonly Guid VIP410A   = new("8914585B-C73D-428D-9FD2-BA6A6CA41CD6");
                public static readonly Guid VIP410E   = new("C7620880-85C1-4786-8163-398C76B866F1");
            }

            public static class SwitchController
            {
                public static readonly Guid EasergySC150 = new("BD124B12-B841-47DA-B00A-6DAF3E996A16");
            }

            public static class ShuntTripReleases
            {
                public static class RM6
                {
                    public static readonly Guid DC24  = new("717799FD-A49B-4E08-8555-BF004C83B0F1");
                    public static readonly Guid DC48  = new("0BB330C8-DDBE-4613-A5C6-3B7721043017");
                    public static readonly Guid DC60  = new("97123304-9015-42DB-B6A4-AE2BE4051170");
                    public static readonly Guid DC110 = new("216DE4A1-7E36-47A7-9885-03E8433FE8AA");
                    public static readonly Guid DC125 = new("77722ACF-F82B-4608-8FD9-0B98BBF7AC2B");
                    public static readonly Guid DC220 = new("4293E2F6-2792-4743-9A2C-70746E6E0966");
                    public static readonly Guid AC120 = new("01404A29-C517-4DF4-B5E9-75E7F051299F");
                    public static readonly Guid AC230 = new("0D8F4541-34F6-4201-849C-1390A60FE726");
                }
            }

            public static class UndervoltageReleases
            {
                public static class RM6
                {
                    public static readonly Guid DC24  = new("221586A9-23ED-4F17-99A5-E5F851563AAA");
                    public static readonly Guid DC48  = new("69A1B232-8994-4081-AD47-99A55A83695A");
                    public static readonly Guid DC60  = new("04E85DB2-6F76-4171-B364-DAA89F7006A4");
                    public static readonly Guid DC110 = new("9DD52B45-67F9-41A4-9BB8-2F96B08A3A5E");
                    public static readonly Guid DC125 = new("6FA35CEA-0E31-4FAD-9D9D-307411D27FA7");
                    public static readonly Guid DC220 = new("75AA90D5-2050-4F43-9EBA-6FCF302BE4A1");
                    public static readonly Guid AC120 = new("9CB1B085-D9E9-41EF-A341-3C5FD52CAE45");
                    public static readonly Guid AC230 = new("889F9080-0D35-41ED-AD22-822CD140F8B9");
                }
            }
        }

        public static class Transformers
        {
            public static readonly Guid TrihalEasy = new("E38AC0C3-ADCA-4DEF-9FAB-F4B39A75E788");
        }
    }
    
    public static class RelayFunctions
    {
        public static readonly Guid Control_A_B_Group                          = new("942103e7-6986-4e09-8c17-9e6b9ebdd8fc");
        public static readonly Guid Control_ANSI_30                            = new("86412134-b663-43a3-a771-8f4aaa5860a1");
        public static readonly Guid Control_ANSI_68                            = new("c808022c-ff00-45f8-af56-98aff5bed3f2");
        public static readonly Guid Control_ANSI_74                            = new("d451f42d-5a7f-4b06-855c-c342ba0f4012");
        public static readonly Guid Control_ANSI_74TC                          = new("818e18c9-3d28-42cf-b3e4-1db3e04a2f6d");
        public static readonly Guid Control_ANSI_86                            = new("1d1be739-1cb7-452b-8507-703c2d5986d5");
        public static readonly Guid Control_ANSI_94_69                         = new("e23cca38-cbb3-4465-971b-481d1360a817");
        public static readonly Guid Control_BreakingCurrentHistory             = new("f53dc614-f246-4aa1-a461-7dc01da2017d");
        public static readonly Guid Control_CircuitBreakerMonitoring           = new("3d12b34f-3495-426c-a7de-54746aa77532");
        public static readonly Guid Control_CircuitBreakerRemote               = new("41d24d5d-7136-4f4f-b4bc-0d0b6890819b");
        public static readonly Guid Control_Communication_Modbus               = new("48fe1d07-4192-4267-8dfc-55fe2061566b");
        public static readonly Guid Control_CountingTripsNumber                = new("9450b16f-355c-475b-b521-9b0735603a67");
        public static readonly Guid Control_CustomizationOfAnnunciationRelays  = new("e6a5f207-dd36-41d0-aa98-6df3a2f1c904");
        public static readonly Guid Control_CustomizedMitopOperation           = new("1bdd0b65-be78-4940-9378-33d98df86940");
        public static readonly Guid Control_CustomLogic                        = new("014bcb15-97dd-4c78-9834-5456964356d2");
        public static readonly Guid Control_DisplayLastFault                   = new("0c3d2b28-2190-4cdf-aaa8-c25129f7d3bd");
        public static readonly Guid Control_ExternalTrippingInput              = new("8e28a6e5-8f2f-45f0-ad81-6c9d5616f814");
        public static readonly Guid Control_FunctionKeys                       = new("df028bab-a442-43cf-a2dc-cf51b83f2c44");
        public static readonly Guid Control_LedPanel                           = new("df01123b-1b03-46dd-93cc-540596ea3371");
        public static readonly Guid Control_LoadCurrentHistory                 = new("dbe897ff-6961-4ee5-be68-d84fa6c6d5a3");
        public static readonly Guid Control_LocalControlOnSingleLineDiagram    = new("9f98d18b-bf1e-43f6-bf8b-a0f332dac1fe");
        public static readonly Guid Control_LocalControlWithIOKeys             = new("5ac57dd4-81bc-4a86-977e-64899e6cccd1");
        public static readonly Guid Control_LocalRemoteControl                 = new("285cdc81-bf86-4dad-ac33-3ef42ef04eff");
        public static readonly Guid Control_LogicEquationEditor                = new("f3275d2e-854c-4f19-8591-90cd47b5b2ee");
        public static readonly Guid Control_MitopSupervision                   = new("fe814dde-55f9-42d9-8b0b-1b9e3efcbdc6");
        public static readonly Guid Control_MitopTripping                      = new("413cd4cd-1a31-4bb7-94a7-9972563254ef");
        public static readonly Guid Control_OvercurrentAndBreakingProfile      = new("ea1f7f5f-d69e-48cb-a5a8-58afddf405f4");
        public static readonly Guid Control_ProgrammableSwitchgearInterlocking = new("d8d3a44b-f81c-432d-8b73-8922e50b3341");
        public static readonly Guid Control_RelayMonitoring                    = new("85e19a9d-7399-4e89-8afb-6da319a99410");
        public static readonly Guid Control_SmartApp                           = new("dadc214c-0b08-440b-9041-04a52b9fc6c4");
        public static readonly Guid Control_SwitchgearControlAndMonitoring     = new("15acf742-7f84-47f8-876e-20e97f7c8f34");
        public static readonly Guid Control_SwitchgearMonitoringOnly           = new("3b3bd614-0235-4969-8545-00300cec8bd0");
        public static readonly Guid Control_SwitchingSettingsGroups            = new("fdd27f84-905d-4dc2-ab74-42ef97a4fc65");
        public static readonly Guid Control_ThreeAnnunciationRelay             = new("b797fc58-0713-413a-a642-7f453548aa2f");
        public static readonly Guid Control_TimeTaggedEvents_Local             = new("a3d8dd9d-e80c-4dae-8a3b-4701f872ad94");
        public static readonly Guid Control_TimeTaggedEvents_Remote            = new("57fe11f9-75f8-46f8-a73a-778b29754485");
        public static readonly Guid Control_TimeTaggedRecord                   = new("213aee2a-1346-4d3b-acb1-8c31a15159fc");
        public static readonly Guid Control_TripIndication_Local               = new("64ba3e0f-1f98-48e4-9590-c3a2a4b2aefc");
        public static readonly Guid Control_TripIndication_Remote              = new("c1d01087-d191-456f-9682-9644d7a47ccb");
        public static readonly Guid Control_TripIndication_ThreeOutputRelays   = new("82841760-40db-4717-8a86-e1dda7a97219");
        public static readonly Guid Control_Watchdog                           = new("64b9729e-372a-40ce-811d-598be52747df");
        
        public static readonly Guid LogsAndRecords_DisturbanceRecord       = new("2a1dff34-1a95-4ae3-bfd6-9535ab7f1b35");
        public static readonly Guid LogsAndRecords_RelayMaintenanceDataLog = new("5b4f315e-f3a7-4886-a6a7-d64d90093d64");
        public static readonly Guid LogsAndRecords_SecurityDataLog         = new("505d3db8-36d4-4645-bbb3-ae59a29e8638");
        public static readonly Guid LogsAndRecords_SequenceOfEventRecord   = new("75aef460-e8fb-4ef1-b1d5-7b2a084d1ce6");
        public static readonly Guid LogsAndRecords_TrippingContextRecord   = new("5ae5dce2-63fb-456f-a59d-1e2dfb210f79");

        public static readonly Guid Measurement_Demand_PhaseCurrent                             = new("281d921e-9dc8-42df-8ae0-c5b9016d1b7f");
        public static readonly Guid Measurement_Demand_PowerAndPowerFactor                      = new("b9b115ff-154b-45c2-add6-b73a29346934");
        public static readonly Guid Measurement_EarthCurrent                                    = new("a45da4ef-21c3-4578-a186-0847c0ab6ab7");
        public static readonly Guid Measurement_EnergyActiveReactive                            = new("ad0755b0-7ed1-4d53-b4f3-f641251c6717");
        public static readonly Guid Measurement_EnergyTransmittedPulseOutputs                   = new("bb842c20-0f49-4d5e-bdd5-a4ea8cd0e746");
        public static readonly Guid Measurement_Frequency                                       = new("717e5dd7-00c1-4a38-a6d3-93fa9d9a3857");
        public static readonly Guid Measurement_FundamentalFrequencyCurrent                     = new("74defa93-dc6e-494d-ba57-743bec9bb662");
        public static readonly Guid Measurement_FundamentalFrequencyPower                       = new("51ab60c9-32b4-4379-9130-da2706bde873");
        public static readonly Guid Measurement_FundamentalFrequencyVoltage                     = new("e2116397-307d-4692-aabc-8c35571076fb");
        public static readonly Guid Measurement_HarmonicPhaseCurrentTHD                         = new("f09f980d-530b-417f-a335-04dfc7061689");
        public static readonly Guid Measurement_HarmonicVoltageTHD                              = new("83158f52-dca5-4d97-b97d-e3c74a52fca1");
        public static readonly Guid Measurement_LoadHistory_CumulativeTime                      = new("cf5a58e3-ff1f-4576-a5e3-d190771e8cce");
        public static readonly Guid Measurement_MaxAndMin_Currents                              = new("d8224132-41f0-4863-b5f1-c38b0545a76c");
        public static readonly Guid Measurement_MaxAndMin_Frequency                             = new("3ed1be7c-cd28-410f-9641-96a18049333f");
        public static readonly Guid Measurement_MaxAndMin_PowerAndPowerFactor                   = new("c2c2ef34-c186-47da-bb4b-22ae49ed0de5");
        public static readonly Guid Measurement_MaxAndMin_Voltages                              = new("f96d1518-ca76-4dd7-b7d4-9ede94ba4dbf");
        public static readonly Guid Measurement_MaxDemand_Over31Days12Months_Power              = new("1fe3bf37-54cd-4c47-a6fb-d5a225843110");
        public static readonly Guid Measurement_MaxDemand_PhaseCurrents                         = new("e968188f-51b1-4ead-ac0e-b6f2c43355f6");
        public static readonly Guid Measurement_MinAndMaxDemand_PhaseCurrents                   = new("244e0f69-bbd0-4135-84f5-f368d9d5573c");
        public static readonly Guid Measurement_MinAndMaxDemand_PowerAndPowerFactor             = new("635234af-d092-45b5-aec4-4cca4d89b2ee");
        public static readonly Guid Measurement_MinAndMaxDemand_RMS_PhaseCurrents               = new("69c0231b-a29d-4781-a39a-910e905d5a44");
        public static readonly Guid Measurement_MinDemand_Over31Days12MonthsActiveReactivePower = new("841b288c-cda4-45c7-bdaf-3aa6b6e29cf0");
        public static readonly Guid Measurement_MotorSpeedDetection                             = new("2f59513a-030f-4375-99bc-315ece30b020");
        public static readonly Guid Measurement_PhaseCurrent                                    = new("0b80f33f-fd84-4050-b4f9-d862ff7273f6");
        public static readonly Guid Measurement_PhasePeakDemandCurrent                          = new("e61e0024-132b-461e-9948-ca7c14816634");
        public static readonly Guid Measurement_PowerFactor                                     = new("be9ba3a7-441b-4743-9208-414c543777e3");
        public static readonly Guid Measurement_RMS_Current                                     = new("85f620b5-7054-4338-9bc7-e531f9676ec3");
        public static readonly Guid Measurement_RMS_Power                                       = new("9a18ffc3-1c0a-4049-a34f-daae4c009d2b");
        public static readonly Guid Measurement_RMS_Voltage                                     = new("9b157f40-c602-46af-a2f3-46280804ca57");
        public static readonly Guid Measurement_VoltageSagsSwells                               = new("5442f24a-e036-4c96-83cf-2f6a0f02a6fd");
        public static readonly Guid Measurement_Active_and_reactive_energy_by_pulse_counting    = new("decb26dc-6584-4a3e-ba58-1cca07918067");
        public static readonly Guid Measurement_Calculated_Wh_varh                              = new("0c96deb5-36be-4f1e-8905-d02e774b5f69");
        public static readonly Guid Measurement_I123_IM123                                      = new("62fbe0e2-c2ff-489a-9e14-13fe5b9ccb1f");
        public static readonly Guid Measurement_I123_RMS_I0                                     = new("dbbda122-9ae1-4e0f-9fbe-49600d565970");
        public static readonly Guid Measurement_PQS                                             = new("bb1b2bca-e82c-4f35-8da7-261fa081fa56");
        public static readonly Guid Measurement_Temperature                                     = new("1edec196-520f-4ec7-ba16-efc2ef45e880");
        public static readonly Guid Measurement_U21_32_13_V123_V0                               = new("bde3d98c-2d81-4626-95bd-cd4607d62e90");
        public static readonly Guid Measurement_Vd                                              = new("ea81c71c-b661-452e-8a1b-5e8f24961ba6");
        
        public static readonly Guid Network_CableArcingFaultDetection                    = new("313c54d7-baf6-44f1-860f-8f6f3ecbffed");
        public static readonly Guid Network_DisturbanceRecording                         = new("834a0cc4-ce27-47ca-b887-2a5127be8e1b");
        public static readonly Guid Network_NumberStarts                                 = new("ecd8bbc6-9ee9-431b-a25b-0fcb942745c3");
        public static readonly Guid Network_PhaseDisplacement                            = new("ae1d2916-08f3-47d9-ae20-c4406aa2c241");
        public static readonly Guid Network_RemainingOperatingTimeBeforeOverloadTripping = new("716ca7d3-2b0b-4d7b-b1d3-a8e42fac0f07");
        public static readonly Guid Network_RunningHoursCounterPeratingTime              = new("20c02a98-59f8-4e67-9769-5d34373f3517");
        public static readonly Guid Network_StartingCurrentAndTime                       = new("db492b00-c3aa-41ad-abba-6a2f75c199bd");
        public static readonly Guid Network_ThermalCapacityUsed                          = new("0b79b71c-f9dd-4acf-9348-42f346f38aae");
        public static readonly Guid Network_TripI1230                                    = new("12d2c267-e8b0-47b5-83d5-4733810db1e7");
        public static readonly Guid Network_TrippingContext                              = new("49047ead-6e2c-4ff5-908f-1202050a82aa");
        public static readonly Guid Network_Unbalance                                    = new("4b0e1301-fc07-4d57-820c-afa9d97de337");
        public static readonly Guid Network_WaitingTimeAfterOverloadTripping             = new("102804f9-75e4-42f5-8a4d-a556fbe87226");
        
        public static readonly Guid Protection_ANSI_12                  = new("26f5072e-1118-4f2e-be16-72fc1f944286");
        public static readonly Guid Protection_ANSI_14                  = new("f8771cee-960c-442a-bd71-e22bfb54e680");
        public static readonly Guid Protection_ANSI_21                  = new("3e8159ca-403c-404e-bc88-e83eee58cad9");
        public static readonly Guid Protection_ANSI_21FL                = new("58b41447-03c1-451a-bbad-6a24b464a6fe");
        public static readonly Guid Protection_ANSI_21G                 = new("5945526d-8cbf-4134-9f9d-00b5e059f078");
        public static readonly Guid Protection_ANSI_21YN                = new("af112ebd-6189-48a2-837c-056708c04d9f");
        public static readonly Guid Protection_ANSI_24                  = new("02455c4d-e997-437a-a426-7d472b814fd8");
        public static readonly Guid Protection_ANSI_25                  = new("f9689af1-c623-4f1f-9f21-4345c6879f84");
        public static readonly Guid Protection_ANSI_26_63               = new("1eefe73a-0838-436f-83a4-409483142cdf");
        public static readonly Guid Protection_ANSI_27                  = new("59af376f-36b3-4c3d-8f71-665b53515ffe");
        public static readonly Guid Protection_ANSI_27_27S              = new("959769d2-b241-4af2-9856-2346175257cb");
        public static readonly Guid Protection_ANSI_27D                 = new("2c027d08-c092-470c-9a91-1eb993508912");
        public static readonly Guid Protection_ANSI_27P                 = new("ec8526b4-8582-41e4-9e2e-e1759ea34792");
        public static readonly Guid Protection_ANSI_27R                 = new("61ad5901-e1f6-4982-8f40-7b21ba4ea0b5");
        public static readonly Guid Protection_ANSI_27S                 = new("aac0735b-972b-4bbd-96b0-7d08c5294169");
        public static readonly Guid Protection_ANSI_32                  = new("870943bc-be6f-4e61-9690-5101d845cdb5");
        public static readonly Guid Protection_ANSI_32_37N              = new("58f33897-98fb-4791-bb4c-80cefa56e92e");
        public static readonly Guid Protection_ANSI_32N                 = new("b1d85096-ebb3-443c-ae1d-209358f763d4");
        public static readonly Guid Protection_ANSI_32P                 = new("cc6f0eb2-9bcd-4a7c-bff8-14afed0fad40");
        public static readonly Guid Protection_ANSI_32Q                 = new("0d2ed428-6f83-461e-b96c-2dabd3de553f");
        public static readonly Guid Protection_ANSI_32Q_40              = new("577ea8a0-9c7d-4615-bcbc-658e56984236");
        public static readonly Guid Protection_ANSI_37                  = new("052220c5-dfed-4af0-848b-5fe8e98ce33c");
        public static readonly Guid Protection_ANSI_38                  = new("e34e3d0d-2def-48b2-8a0f-dfdc818cc079");
        public static readonly Guid Protection_ANSI_38_49T              = new("be859c95-23db-4e9a-85f6-0959eb560b2f");
        public static readonly Guid Protection_ANSI_40                  = new("6ef94981-a5fc-4b0d-a405-b03cd8ccc641");
        public static readonly Guid Protection_ANSI_46                  = new("16e9a33b-a7b6-448a-9b94-7df98643c00e");
        public static readonly Guid Protection_ANSI_46BC                = new("0f74a41d-119a-47ba-ab0b-dd56b134f3bb");
        public static readonly Guid Protection_ANSI_47                  = new("42922dbf-877d-4e67-bd10-2e3df074486c");
        public static readonly Guid Protection_ANSI_48_51LR             = new("abab3de2-a33e-47d0-bd22-d428f85d4137");
        public static readonly Guid Protection_ANSI_48_51LR_14          = new("0ea75a96-d62b-4703-81d3-908a3598ba40");
        public static readonly Guid Protection_ANSI_49                  = new("12d2e10c-effc-4201-91cd-8572d1d89ca6");
        public static readonly Guid Protection_ANSI_49RMS               = new("897c6819-f723-4db3-8736-12d1cf9b0c53");
        public static readonly Guid Protection_ANSI_50_51               = new("b1718649-cf1d-477e-a520-99cbfc0d0704");
        public static readonly Guid Protection_ANSI_50ARC               = new("57a0fa63-40c5-4b0b-940b-92f023359ea9");
        public static readonly Guid Protection_ANSI_50BF                = new("d6c4dc61-eca2-46a2-8249-b5c16cd8a524");
        public static readonly Guid Protection_ANSI_50HS                = new("b75c2bb0-c2ff-4af9-bc79-df4beafd54ed");
        public static readonly Guid Protection_ANSI_50N_51N             = new("241c30ad-5065-4cdc-86b8-0d2eb0d87d8e");
        public static readonly Guid Protection_ANSI_50N_51N_50G_51G     = new("a7732133-7c5a-4b8f-a87a-37ab27ae0e93");
        public static readonly Guid Protection_ANSI_51C                 = new("1f491ff7-40e7-4055-afcc-6629afc40421");
        public static readonly Guid Protection_ANSI_51N                 = new("aa17daac-3f1b-4d80-adf1-04030a534dd1");
        public static readonly Guid Protection_ANSI_51N_HighSensitivity = new("da9f93b3-4667-49e3-a1cd-a9facea9ebdc");
        public static readonly Guid Protection_ANSI_51V                 = new("7d42e375-7029-4f9e-90d2-cc182bd60bb5");
        public static readonly Guid Protection_ANSI_59                  = new("e920a175-227a-4215-9c21-dddb42aeea6a");
        public static readonly Guid Protection_ANSI_59C                 = new("bfafd1c6-e68e-4673-96f9-46f32c49ab1c");
        public static readonly Guid Protection_ANSI_59N                 = new("d48c7389-6e06-497e-945e-84f9c64607ed");
        public static readonly Guid Protection_ANSI_60                  = new("d481836f-f803-4dcd-93fc-6dd06cc767b4");
        public static readonly Guid Protection_ANSI_60CT                = new("1750dd68-2835-440d-a896-7ee4f0b06d14");
        public static readonly Guid Protection_ANSI_60FL                = new("1be84dc4-4500-4045-a563-cf49eccfe534");
        public static readonly Guid Protection_ANSI_60VT                = new("9e128ef7-62cf-436c-a42e-05f4306cf1ca");
        public static readonly Guid Protection_ANSI_64REF               = new("735f5c63-f0ef-475f-aba2-bd3cfb882346");
        public static readonly Guid Protection_ANSI_64S                 = new("eaa6b396-0ca9-44ba-949e-c45ca42cd282");
        public static readonly Guid Protection_ANSI_66                  = new("9ad6a034-2889-4828-b6da-08e5c5731568");
        public static readonly Guid Protection_ANSI_67                  = new("786e4209-4bed-4194-84f3-573792c97e79");
        public static readonly Guid Protection_ANSI_67_67N              = new("C80AFB37-8263-4C32-9795-2AFF3F49EE2F");
        public static readonly Guid Protection_ANSI_67N                 = new("2399f040-1bca-48c2-97fe-9d74c57b08a0");
        public static readonly Guid Protection_ANSI_67N_67NC            = new("893f99eb-8a5e-478e-911d-12d17b8f737d");
        public static readonly Guid Protection_ANSI_67NI                = new("425c43c7-447a-4ffc-a0aa-03e15192c077");
        public static readonly Guid Protection_ANSI_68F2                = new("dbf1280b-57a7-431c-9ea6-38383a89e0f8");
        public static readonly Guid Protection_ANSI_68H2                = new("d81b7e6b-64f9-46e1-8b19-da2f13b026f0");
        public static readonly Guid Protection_ANSI_68H5                = new("51b387c9-0e07-4a72-acef-60496046477d");
        public static readonly Guid Protection_ANSI_78PS                = new("1e929468-78e2-4601-8cd1-5c8775fb0291");
        public static readonly Guid Protection_ANSI_79                  = new("8587ae2c-20d6-46b7-ad2a-b219a0c0d9fd");
        public static readonly Guid Protection_ANSI_81                  = new("9bb44e05-9563-4d70-812a-8e1fe9f34beb");
        public static readonly Guid Protection_ANSI_81H                 = new("1b1d800c-039d-439b-8559-bc206ff5d103");
        public static readonly Guid Protection_ANSI_81L                 = new("b3890bd5-ccff-4fdb-9328-a0f093043030");
        public static readonly Guid Protection_ANSI_81R                 = new("5a049e0b-f17a-4865-8523-5ee3acc87dca");
        public static readonly Guid Protection_ANSI_81U                 = new("5c5920be-44ce-4ada-840e-a1734ed1f8bc");
        public static readonly Guid Protection_ANSI_86                  = new("11704704-1472-4de0-83ec-d9b0f8e75118");
        public static readonly Guid Protection_ANSI_87L                 = new("e4ed2335-c100-4f2b-929d-cd7afeea56f9");
        public static readonly Guid Protection_ANSI_87M                 = new("823b27fb-5479-4bad-a232-abbb60ca4a5c");
        public static readonly Guid Protection_ANSI_87T                 = new("be569114-7b38-4422-b107-9e1cce74d228");
        public static readonly Guid Protection_ANSI_99                  = new("d05c0e3e-33b3-4689-9b75-7cef08a5564a");
        public static readonly Guid Protection_ANSI_ABS                 = new("a13b1422-d83a-4367-8b0a-8a0b203ab41a");
        public static readonly Guid Protection_ANSI_CLPU_50_51          = new("2b559518-7027-43b8-bed7-28ca5d8f1b31");
        public static readonly Guid Protection_ANSI_CLPU_50_51N         = new("6211a84c-83f5-428f-bf6b-8a4bb0adca27");
        public static readonly Guid Protection_ArcFlashDetectionStages  = new("efd9dc72-70e4-4f94-a316-7ba98cd069d3");
        public static readonly Guid Protection_ColdLoadPickUp           = new("be2f2f5a-cdc9-4d76-a50a-c6ce10edfd6e");
        public static readonly Guid Protection_ProgrammableCurves       = new("1cf06807-b550-4fd6-b294-68affabc8dfd");
        public static readonly Guid Protection_SettingGroups            = new("b0f3397c-0ed7-4c61-a797-c8152d34ea72");

        public static readonly Guid Switchgear_CT_VT_Supervision_60FL                      = new("3dc0c82c-fd2d-4d85-ae34-1b781b7413fe");
        public static readonly Guid Switchgear_CumulativeBreakingCurrent                   = new("c516ab63-f052-48d2-af98-bb9fb2ee0b19");
        public static readonly Guid Switchgear_NumberOperations_OperatingTime_ChargingTime = new("780d50b3-89a4-4cae-a3de-2576e4e4c42d");
        public static readonly Guid Switchgear_TripCircuitSupervision                      = new("b1683a51-373a-4151-ab2f-a6bcb18a3ce1");
    }
}