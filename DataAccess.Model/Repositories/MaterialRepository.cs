﻿using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public sealed class MaterialRepository : FileRepository<Material>
{
    public MaterialRepository(AppPaths appPaths) :
        base(new MaterialInitializer(), appPaths) => Load();
    
    private class MaterialInitializer : IRepositoryInitializer<Material>
    {
        public List<Material> Initialize() => new()
        {
            new Material("Al")
            {
                Density = 2700,
                Resistivity = 2.8e-8,
                TemperatureCoefficient = 3.9e-3,
            },
            new Material("Cu")
            {
                Density = 8950,
                Resistivity = 1.75e-8,
                TemperatureCoefficient = 4.04e-3,
            }
        };
    }
}