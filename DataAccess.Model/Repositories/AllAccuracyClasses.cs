﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public static class AllAccuracyClasses
{
    private static MeasuringAccuracyClass _m01;
    private static MeasuringAccuracyClass _m02;
    private static MeasuringAccuracyClass _m02S;
    private static MeasuringAccuracyClass _m05;
    private static MeasuringAccuracyClass _m05S;
    private static MeasuringAccuracyClass _m1;
    private static MeasuringAccuracyClass _m3;
    private static ProtectionAccuracyClass _p5;
    private static ProtectionAccuracyClass _p5_10;
    private static ProtectionAccuracyClass _p5_15;
    private static ProtectionAccuracyClass _p5_20;
    private static ProtectionAccuracyClass _p5_30;
    private static ProtectionAccuracyClass _p10;
    private static ProtectionAccuracyClass _p10_10;
    private static ProtectionAccuracyClass _p10_15;
    private static ProtectionAccuracyClass _p10_20;
    private static ProtectionAccuracyClass _p10_30;
    
    public static MeasuringAccuracyClass M01 => _m01 ??= new MeasuringAccuracyClass(0.1);
    public static MeasuringAccuracyClass M02 => _m02 ??= new MeasuringAccuracyClass(0.2);
    public static MeasuringAccuracyClass M02S => _m02S ??= new MeasuringAccuracyClass(0.2, specialApplication: true);
    public static MeasuringAccuracyClass M05 => _m05 ??= new MeasuringAccuracyClass(0.5);
    public static MeasuringAccuracyClass M05S => _m05S ??= new MeasuringAccuracyClass(0.5, specialApplication: true);
    public static MeasuringAccuracyClass M1 => _m1 ??= new MeasuringAccuracyClass(1.0);
    public static MeasuringAccuracyClass M3 => _m3 ??= new MeasuringAccuracyClass(3.0);
    public static ProtectionAccuracyClass P5 => _p5 ??= new ProtectionAccuracyClass(5.0);
    public static ProtectionAccuracyClass P5_10 => _p5_10 ??= new ProtectionAccuracyClass(5.0, 10);
    public static ProtectionAccuracyClass P5_15 => _p5_15 ??= new ProtectionAccuracyClass(5.0, 15);
    public static ProtectionAccuracyClass P5_20 => _p5_20 ??= new ProtectionAccuracyClass(5.0, 20);
    public static ProtectionAccuracyClass P5_30 => _p5_30 ??= new ProtectionAccuracyClass(5.0, 30);
    public static ProtectionAccuracyClass P10 => _p10 ??= new ProtectionAccuracyClass(10.0);
    public static ProtectionAccuracyClass P10_10 => _p10_10 ??= new ProtectionAccuracyClass(10.0, 10);
    public static ProtectionAccuracyClass P10_15 => _p10_15 ??= new ProtectionAccuracyClass(10.0, 15);
    public static ProtectionAccuracyClass P10_20 => _p10_20 ??= new ProtectionAccuracyClass(10.0, 20);
    public static ProtectionAccuracyClass P10_30 => _p10_30 ??= new ProtectionAccuracyClass(10.0, 30);
}