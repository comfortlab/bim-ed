﻿using CoLa.BimEd.BusinessLogic.Model.Common;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public static class SwitchGearDimensions
{
    public static Dimensions PremSet_D02N  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_D06H  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_D06N  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_D12H  = Dimensions.CreateFromMillimeters(1550, 750, 910);
    public static Dimensions PremSet_ES_B  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_G06   = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_G12   = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_I06H  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_I06T  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_I12H  = Dimensions.CreateFromMillimeters(1550, 750, 910);
    public static Dimensions PremSet_M06A  = Dimensions.CreateFromMillimeters(1550, 750, 910);
    public static Dimensions PremSet_M06S  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_M12A  = Dimensions.CreateFromMillimeters(1550, 750, 910);
    public static Dimensions PremSet_M12S  = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_VTM   = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_VTM_D = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_VTP   = Dimensions.CreateFromMillimeters(1550, 375, 910);
    public static Dimensions PremSet_VTP_D = Dimensions.CreateFromMillimeters(1550, 375, 910);

    public static Dimensions RM6_B     = Dimensions.CreateFromMillimeters(1142,  572, 670);
    public static Dimensions RM6_BC    = Dimensions.CreateFromMillimeters(1142,  632, 670);
    public static Dimensions RM6_D     = Dimensions.CreateFromMillimeters(1142,  572, 670);
    public static Dimensions RM6_I     = Dimensions.CreateFromMillimeters(1142,  472, 670);
    public static Dimensions RM6_IC    = Dimensions.CreateFromMillimeters(1142,  532, 670);
    public static Dimensions RM6_Mt    = Dimensions.CreateFromMillimeters(1142, 1106, 670);
    public static Dimensions RM6_O     = Dimensions.CreateFromMillimeters(1142,  532, 670);
    public static Dimensions RM6_O_LE  = Dimensions.CreateFromMillimeters(1142,  502, 670);
    public static Dimensions RM6_O_RE  = Dimensions.CreateFromMillimeters(1142,  502, 670);
    public static Dimensions RM6_Q     = Dimensions.CreateFromMillimeters(1142,  632, 670);

    public static Dimensions SM6_CM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_CM2         = Dimensions.CreateFromMillimeters(1600, 500,  940);
    public static Dimensions SM6_DM1_A       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM1_D       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM1_W       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM1_Z       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM2         = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DMVL_A      = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DMVL_D      = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM1_M       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DM1_S       = Dimensions.CreateFromMillimeters(1600, 750, 1220);
    public static Dimensions SM6_DMV_A       = Dimensions.CreateFromMillimeters(1600, 625,  940);
    public static Dimensions SM6_DMV_D       = Dimensions.CreateFromMillimeters(1600, 625,  940);
    public static Dimensions SM6_GAM         = Dimensions.CreateFromMillimeters(1600, 500, 1020);
    public static Dimensions SM6_GAM2        = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_GBC_A       = Dimensions.CreateFromMillimeters(1600, 750, 1020);
    public static Dimensions SM6_GBC_B       = Dimensions.CreateFromMillimeters(1600, 750, 1020);
    public static Dimensions SM6_GBM         = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_GEM         = Dimensions.CreateFromMillimeters(1600, 125,  920);
    public static Dimensions SM6_GIM         = Dimensions.CreateFromMillimeters(1600, 125,  840);
    public static Dimensions SM6_IM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_IMB         = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_IMC         = Dimensions.CreateFromMillimeters(1600, 500,  940);
    public static Dimensions SM6_IMM         = Dimensions.CreateFromMillimeters(1600, 750,  940);
    public static Dimensions SM6_NSM_cables  = Dimensions.CreateFromMillimeters(2050, 750,  940);
    public static Dimensions SM6_NSM_busbars = Dimensions.CreateFromMillimeters(2050, 750,  940);
    public static Dimensions SM6_PM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_QM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_QMB         = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_QMC         = Dimensions.CreateFromMillimeters(1600, 625,  940);
    public static Dimensions SM6_SM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
    public static Dimensions SM6_TM          = Dimensions.CreateFromMillimeters(1600, 375,  940);
}