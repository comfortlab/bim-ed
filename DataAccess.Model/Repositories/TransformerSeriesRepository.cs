using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public sealed class TransformerSeriesRepository : MemoryRepository<TransformerSeries>
{
    private readonly Localization _localization;
    public const string Trihal = "Trihal";
    public const string TrihalEasy = "Trihal EASY";
        
    public TransformerSeriesRepository(Repository<Material> materials, Localization localization) :
        base(new TransformerSeriesInitializer(materials))
    {
        _localization = localization;
        Load();
    }

    public List<TransformerSeries> GetTrihalTransformerSeries(string countryCode = null) => GetTransformerSeries(Trihal, countryCode);
    public List<TransformerSeries> GetTrihalEasyTransformerSeries(string countryCode = null) => GetTransformerSeries(TrihalEasy, countryCode);
    public List<TransformerSeries> GetTransformerSeries(string name, string countryCode = null)
    {
        return this.Where(n =>
                n.Name == name &&
                n.Countries.Contains(countryCode ?? _localization.CountryCode))
            .ToList();
    }
    
    private class TransformerSeriesInitializer : IRepositoryInitializer<TransformerSeries>
    {
        private readonly List<Material> _materials;

        public TransformerSeriesInitializer(List<Material> materials)
        {
            _materials = materials;
        }
        
        public List<TransformerSeries> Initialize() => new()
        {
            TrihalEasy(),
        };

        private TransformerSeries TrihalEasy()
        {
            const string name = "Trihal EASY";
            var guid = Guids.Products.Transformers.TrihalEasy;
            var ip00 = new List<ProtectionIndex> { ProtectionIndex.IP00 };
            var ip2131 = new List<ProtectionIndex> { ProtectionIndex.IP21, ProtectionIndex.IP31 };
                
            return new TransformerSeries(guid, name, TransformerNoLoadLossesLevel.C0, TransformerLoadLossesLevel.Bk)
            {
                Countries = new List<string>
                {
                    Localization.Rus,  
                },
                CoolingModes = new List<CoolingMode> {CoolingMode.AN, CoolingMode.AF25, CoolingMode.AF40,},
                Properties = new List<TransformerProperty>
                {
                    TransformerProperty.ExtendedWarranty,
                    TransformerProperty.InstallationSupervision,
                    TransformerProperty.SupplyOfSpareParts,
                    TransformerProperty.MaintenanceContract,
                },
                ProtectionIndices = new List<ProtectionIndex> {ProtectionIndex.IP00, ProtectionIndex.IP21, ProtectionIndex.IP31,},
                VectorGroups = new List<VectorGroup> {VectorGroup.Dyn11, VectorGroup.Yyn0,},
                WindingMaterials = new List<Material>
                {
                    _materials.FirstOrDefault(x => x.Id == Material.Al),
                },
                Products = new List<TransformerProduct>
                {
                    new( 250000,  6000, 400,  7200,  680,  2510,  2887, 6, ip00,   1195,  620, 1425,  805, 65, 53),
                    new( 250000,  6300, 400,  7200,  680,  2510,  2887, 6, ip00,   1195,  620, 1425,  805, 65, 53),
                    new( 250000, 10000, 400, 12000,  680,  2510,  2887, 6, ip00,   1195,  620, 1425,  805, 65, 53),
                    new( 250000, 10500, 400, 12000,  680,  2510,  2887, 6, ip00,   1195,  620, 1425,  805, 65, 53),
                    new( 400000,  6000, 400,  7200,  980,  3750,  4313, 6, ip00,   1290,  770, 1460, 1300, 68, 56),
                    new( 400000,  6300, 400,  7200,  980,  3750,  4313, 6, ip00,   1290,  770, 1460, 1300, 68, 56),
                    new( 400000, 10000, 400, 12000,  980,  3750,  4313, 6, ip00,   1290,  770, 1460, 1300, 68, 56),
                    new( 400000, 10500, 400, 12000,  980,  3750,  4313, 6, ip00,   1290,  770, 1460, 1300, 68, 56),
                    new( 630000,  6000, 400,  7200, 1100,  5912,  6799, 6, ip00,   1450,  770, 1510, 2050, 70, 57),
                    new( 630000,  6300, 400,  7200, 1100,  5912,  6799, 6, ip00,   1450,  770, 1510, 2050, 70, 57),
                    new( 630000, 10000, 400, 12000, 1100,  5912,  6799, 6, ip00,   1450,  770, 1510, 2050, 70, 57),
                    new( 630000, 10500, 400, 12000, 1100,  5912,  6799, 6, ip00,   1450,  770, 1510, 2050, 70, 57),
                    new(1000000,  6000, 400,  7200, 1550,  8500,  9775, 6, ip00,   1570,  940, 1787, 2800, 73, 59),
                    new(1000000,  6300, 400,  7200, 1550,  8500,  9775, 6, ip00,   1570,  940, 1787, 2800, 73, 59),
                    new(1000000, 10000, 400, 12000, 1550,  8500,  9775, 6, ip00,   1570,  940, 1787, 2800, 73, 59),
                    new(1000000, 10500, 400, 12000, 1550,  8500,  9775, 6, ip00,   1570,  940, 1787, 2800, 73, 59),
                    new(1250000,  6000, 400,  7200, 1900, 10100, 11615, 6, ip00,   1660,  940, 1900, 3100, 75, 61),
                    new(1250000,  6300, 400,  7200, 1900, 10100, 11615, 6, ip00,   1660,  940, 1900, 3100, 75, 61),
                    new(1250000, 10000, 400, 12000, 1900, 10100, 11615, 6, ip00,   1660,  940, 1900, 3100, 75, 61),
                    new(1250000, 10500, 400, 12000, 1900, 10100, 11615, 6, ip00,   1660,  940, 1900, 3100, 75, 61),
                    new(1600000,  6000, 400,  7200, 2300, 11850, 13628, 6, ip00,   1716,  940, 2142, 3500, 75, 61),
                    new(1600000,  6300, 400,  7200, 2300, 11850, 13628, 6, ip00,   1716,  940, 2142, 3500, 75, 61),
                    new(1600000, 10000, 400, 12000, 2300, 11850, 13628, 6, ip00,   1716,  940, 2142, 3500, 75, 61),
                    new(1600000, 10500, 400, 12000, 2300, 11850, 13628, 6, ip00,   1716,  940, 2142, 3500, 75, 61),
                    new(2000000,  6000, 400,  7200, 2690, 13637, 15500, 6, ip00,   1820, 1190, 2250, 4480, 75, 63),
                    new(2000000,  6300, 400,  7200, 2690, 13637, 15500, 6, ip00,   1820, 1190, 2250, 4480, 75, 63),
                    new(2000000, 10000, 400, 12000, 2690, 13637, 15500, 6, ip00,   1820, 1190, 2250, 4480, 75, 63),
                    new(2000000, 10500, 400, 12000, 2690, 13637, 15500, 6, ip00,   1820, 1190, 2250, 4480, 75, 63),
                    new(2500000,  6000, 400,  7200, 3200, 16700, 19205, 6, ip00,   2000, 1180, 2225, 5350, 76, 65),
                    new(2500000,  6300, 400,  7200, 3200, 16700, 19205, 6, ip00,   2000, 1180, 2225, 5350, 76, 65),
                    new(2500000, 10000, 400, 12000, 3200, 16700, 19205, 6, ip00,   2000, 1180, 2225, 5350, 76, 65),
                    new(2500000, 10500, 400, 12000, 3200, 16700, 19205, 6, ip00,   2000, 1180, 2225, 5350, 76, 65),
                    new( 250000,  6000, 400,  7200,  680,  2510,  2887, 6, ip2131, 1885,  880, 1750,  965, 65, 53),
                    new( 250000,  6300, 400,  7200,  680,  2510,  2887, 6, ip2131, 1885,  880, 1750,  965, 65, 53),
                    new( 250000, 10000, 400, 12000,  680,  2510,  2887, 6, ip2131, 1885,  880, 1750,  965, 65, 53),
                    new( 250000, 10500, 400, 12000,  680,  2510,  2887, 6, ip2131, 1885,  880, 1750,  965, 65, 53),
                    new( 400000,  6000, 400,  7200,  980,  3750,  4313, 6, ip2131, 1990,  980, 1770, 1530, 68, 56),
                    new( 400000,  6300, 400,  7200,  980,  3750,  4313, 6, ip2131, 1990,  980, 1770, 1530, 68, 56),
                    new( 400000, 10000, 400, 12000,  980,  3750,  4313, 6, ip2131, 1990,  980, 1770, 1530, 68, 56),
                    new( 400000, 10500, 400, 12000,  980,  3750,  4313, 6, ip2131, 1990,  980, 1770, 1530, 68, 56),
                    new( 630000,  6000, 400,  7200, 1100,  5912,  6799, 6, ip2131, 2075,  980, 2000, 2320, 70, 57),
                    new( 630000,  6300, 400,  7200, 1100,  5912,  6799, 6, ip2131, 2075,  980, 2000, 2320, 70, 57),
                    new( 630000, 10000, 400, 12000, 1100,  5912,  6799, 6, ip2131, 2075,  980, 2000, 2320, 70, 57),
                    new( 630000, 10500, 400, 12000, 1100,  5912,  6799, 6, ip2131, 2075,  980, 2000, 2320, 70, 57),
                    new(1000000,  6000, 400,  7200, 1550,  8500,  9775, 6, ip2131, 2225, 1055, 2140, 3080, 73, 59),
                    new(1000000,  6300, 400,  7200, 1550,  8500,  9775, 6, ip2131, 2225, 1055, 2140, 3080, 73, 59),
                    new(1000000, 10000, 400, 12000, 1550,  8500,  9775, 6, ip2131, 2225, 1055, 2140, 3080, 73, 59),
                    new(1000000, 10500, 400, 12000, 1550,  8500,  9775, 6, ip2131, 2225, 1055, 2140, 3080, 73, 59),
                    new(1250000,  6000, 400,  7200, 1900, 10100, 11615, 6, ip2131, 2585, 1080, 2295, 3415, 75, 61),
                    new(1250000,  6300, 400,  7200, 1900, 10100, 11615, 6, ip2131, 2585, 1080, 2295, 3415, 75, 61),
                    new(1250000, 10000, 400, 12000, 1900, 10100, 11615, 6, ip2131, 2585, 1080, 2295, 3415, 75, 61),
                    new(1250000, 10500, 400, 12000, 1900, 10100, 11615, 6, ip2131, 2585, 1080, 2295, 3415, 75, 61),
                    new(1600000,  6000, 400,  7200, 2300, 11850, 13628, 6, ip2131, 2635, 1180, 2660, 3950, 75, 61),
                    new(1600000,  6300, 400,  7200, 2300, 11850, 13628, 6, ip2131, 2635, 1180, 2660, 3950, 75, 61),
                    new(1600000, 10000, 400, 12000, 2300, 11850, 13628, 6, ip2131, 2635, 1180, 2660, 3950, 75, 61),
                    new(1600000, 10500, 400, 12000, 2300, 11850, 13628, 6, ip2131, 2635, 1180, 2660, 3950, 75, 61),
                    new(2000000,  6000, 400,  7200, 2690, 13637, 15500, 6, ip2131, 2840, 1270, 2720, 4870, 75, 63),
                    new(2000000,  6300, 400,  7200, 2690, 13637, 15500, 6, ip2131, 2840, 1270, 2720, 4870, 75, 63),
                    new(2000000, 10000, 400, 12000, 2690, 13637, 15500, 6, ip2131, 2840, 1270, 2720, 4870, 75, 63),
                    new(2000000, 10500, 400, 12000, 2690, 13637, 15500, 6, ip2131, 2840, 1270, 2720, 4870, 75, 63),
                    new(2500000,  6000, 400,  7200, 3200, 16700, 19205, 6, ip2131, 2980, 1270, 2800, 5780, 76, 65),
                    new(2500000,  6300, 400,  7200, 3200, 16700, 19205, 6, ip2131, 2980, 1270, 2800, 5780, 76, 65),
                    new(2500000, 10000, 400, 12000, 3200, 16700, 19205, 6, ip2131, 2980, 1270, 2800, 5780, 76, 65),
                    new(2500000, 10500, 400, 12000, 3200, 16700, 19205, 6, ip2131, 2980, 1270, 2800, 5780, 76, 65),
                },
            };
        }
    }
}