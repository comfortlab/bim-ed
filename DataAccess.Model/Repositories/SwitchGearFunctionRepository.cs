﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;
using CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public sealed class SwitchGearFunctionRepository : MemoryRepository<SwitchGearFunction>
{
    public SwitchGearFunctionRepository(
        SwitchGearComponentCollections switchGearComponentCollections) :
        base(new SwitchGearFunctionInitializer(switchGearComponentCollections)) { }
}