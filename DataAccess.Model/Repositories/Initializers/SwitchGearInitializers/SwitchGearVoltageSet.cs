﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage.Properties;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public static  class SwitchGearVoltageSet
{
    public static readonly SwitchGearVoltage kV_06_7p2_20_60    = new( 6_000,  7_200, 20_000,  60_000);
    public static readonly SwitchGearVoltage kV_06_7_28_75      = new( 6_000,  7_200, 28_000,  75_000);
    public static readonly SwitchGearVoltage kV_06_7p2_32_60    = new( 6_000,  7_200, 32_000,  60_000);
    public static readonly SwitchGearVoltage kV_10_12_28_75     = new(10_000, 12_000, 28_000,  75_000);
    public static readonly SwitchGearVoltage kV_10_12_42_75     = new(10_000, 12_000, 42_000,  75_000);
    public static readonly SwitchGearVoltage kV_13p8_17p5_38_95 = new(13_800, 17_500, 38_000,  95_000);
    public static readonly SwitchGearVoltage kV_15_17p5_38_95   = new(15_000, 17_500, 38_000,  95_000);
    public static readonly SwitchGearVoltage kV_20_24_65_125    = new(20_000, 24_000, 65_000, 125_000);
    public static readonly SwitchGearVoltage kV_20_24_50_125    = new(20_000, 24_000, 50_000, 125_000);
    public static readonly SwitchGearVoltage kV_33_36_70_170    = new(33_000, 36_000, 70_000, 170_000);
}