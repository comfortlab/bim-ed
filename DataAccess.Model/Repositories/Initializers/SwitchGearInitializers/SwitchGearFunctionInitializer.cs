﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.DataAccess.Model.Resources.Images;
using CoLa.BimEd.DataAccess.Model.Resources.Products;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using SG = CoLa.BimEd.DataAccess.Model.Repositories.Guids.Products.SwitchGears;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public class SwitchGearFunctionInitializer : IRepositoryInitializer<SwitchGearFunction>
{
    #region Fields

    private readonly Rm6ElectricalCharacteristicsFactory _rm6_ElectricalCharacteristicsFactory = new();
    
    private readonly AuxiliaryContact _rm6_CircuitBreakerPositionAuxContact;
    private readonly AuxiliaryContact _rm6_ForbiddenClosingUnderFaultAuxContact;
    private readonly AuxiliaryContact _rm6_FuseBlownAuxContact;
    private readonly AuxiliaryContact _rm6_FuseSwitchCombinationsPositionAuxContact;
    private readonly AuxiliaryContact _rm6_MainSwitchPositionAuxContact;
    private readonly AuxiliaryContact _rm6_TrippingIndicationAuxContact;
    
    private readonly CurrentTransformer _cUa;
    private readonly CurrentTransformer _cUb;
    private readonly CurrentTransformer _easergySctr500_1_class1;
    private readonly CurrentTransformer _easergySctr500_1_class3;
    private readonly CurrentTransformer _easergySctrh500_1_class3;
    private readonly CurrentTransformer _ctr2200;
    private readonly CurrentTransformer _ctrh2200;
    
    private readonly FaultPassageIndicator _flair21d;
    private readonly FaultPassageIndicator _flair22d;
    private readonly FaultPassageIndicator _flair23d;
    private readonly FaultPassageIndicator _flair23dm;
    
    private readonly MeasuringVoltageTransformer _lpvt;
    
    private readonly ProtectionRelay _vip40_100;
    private readonly ProtectionRelay _vip40_200;
    private readonly ProtectionRelay _vip45_100;
    private readonly ProtectionRelay _vip45_200;
    private readonly ProtectionRelay _vip400;
    private readonly ProtectionRelay _vip410A;
    private readonly ProtectionRelay _vip410E;

    private readonly SwitchController _easergySC150;
    
    private readonly UniqueCollection<Fuse> _allFuses;
    private readonly UniqueCollection<MotorMechanism> _rm6_MotorMechanisms;
    private readonly UniqueCollection<ShuntTripRelease> _rm6_ShuntTripReleases;
    private readonly UniqueCollection<UndervoltageRelease> _rm6_UndervoltageReleases;

    #endregion

    public SwitchGearFunctionInitializer(
        SwitchGearComponentCollections componentCollections)
    {
        _rm6_CircuitBreakerPositionAuxContact         = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.CircuitBreakerPositionAuxContact);
        _rm6_ForbiddenClosingUnderFaultAuxContact     = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.ForbiddenClosingUnderFaultAuxContact);
        _rm6_FuseBlownAuxContact                      = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.FuseBlownAuxContact);
        _rm6_FuseSwitchCombinationsPositionAuxContact = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.FuseSwitchCombinationsPositionAuxContact);
        _rm6_MainSwitchPositionAuxContact             = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.MainSwitchPositionAuxContact);
        _rm6_TrippingIndicationAuxContact             = componentCollections.Get<AuxiliaryContact>(SG.AuxiliaryContacts.RM6.TrippingIndicationAuxContact);

        _cUa                      = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.CUa);
        _cUb                      = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.CUb);
        _easergySctr500_1_class1  = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.EasergySCTR500_1_class1);
        _easergySctr500_1_class3  = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.EasergySCTR500_1_class3);
        _easergySctrh500_1_class3 = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.EasergySCTRH500_1_class3);
        _ctr2200                  = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.CTR2200);
        _ctrh2200                 = componentCollections.Get<CurrentTransformer>(SG.CurrentTransformers.CTRH2200);

        _flair21d  = componentCollections.Get<FaultPassageIndicator>(SG.FaultPassageIndicators.Flair21D);
        _flair22d  = componentCollections.Get<FaultPassageIndicator>(SG.FaultPassageIndicators.Flair22D);
        _flair23d  = componentCollections.Get<FaultPassageIndicator>(SG.FaultPassageIndicators.Flair23D);
        _flair23dm = componentCollections.Get<FaultPassageIndicator>(SG.FaultPassageIndicators.Flair23DM);

        _allFuses = componentCollections.GetComponents<Fuse>();
        
        _lpvt = componentCollections.Get<MeasuringVoltageTransformer>(SG.MeasuringVoltageTransformers.LPVT);

        _vip40_100  = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP40_100);
        _vip40_200  = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP40_200);
        _vip45_100  = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP45_100);
        _vip45_200  = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP45_200);
        _vip400     = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP400);
        _vip410A    = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP410A);
        _vip410E    = componentCollections.Get<ProtectionRelay>(SG.ProtectionRelays.VIP410E);

        _easergySC150 = componentCollections.Get<SwitchController>(SG.SwitchController.EasergySC150);

        _rm6_MotorMechanisms = new UniqueCollection<MotorMechanism>
        {
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC24),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC48),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC60),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC110),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC125),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.DC220),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.AC120),
            componentCollections.Get<MotorMechanism>(SG.MotorMechanisms.RM6.AC230),
        };
        
        _rm6_ShuntTripReleases = new UniqueCollection<ShuntTripRelease>
        {
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC24),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC48),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC60),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC110),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC125),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.DC220),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.AC120),
            componentCollections.Get<ShuntTripRelease>(SG.ShuntTripReleases.RM6.AC230),
        };
        
        _rm6_UndervoltageReleases = new UniqueCollection<UndervoltageRelease>
        {
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC24),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC48),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC60),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC110),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC125),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.DC220),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.AC120),
            componentCollections.Get<UndervoltageRelease>(SG.UndervoltageReleases.RM6.AC230),
        };
    }

    public List<SwitchGearFunction> Initialize()
    {
        var functions = new List<SwitchGearFunction>();

        functions.AddRange(Rm6Functions());
        
        return functions;
    }

    private IEnumerable<SwitchGearFunction> Rm6Functions() => new []
    {
        RM6_Empty(),
        RM6_B(),
        RM6_BC(),
        RM6_D(),
        RM6_I(),
        RM6_IC(),
        RM6_Mt(),
        RM6_O(),
        RM6_Q(),
    };

    #region RM6

    private SwitchGearFunction RM6_Empty()
    {
        return new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_Empty, new Product())
        {
            SwitchMountType = SwitchMountType.Fixed,
            Functions = Array.Empty<Function>(),
        };
    }

    private SwitchGearFunction RM6_B()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_B, new Product
        {
            Name = "B",
            Description = SwitchGearDescriptions.RM6_B,
            LongDescription = SwitchGearDescriptions.RM6_B_Long,
            Image = SwitchGearImages.RM6_B_150x300,
            Dimensions = SwitchGearDimensions.RM6_B,
        })
        {
            SwitchMountType = SwitchMountType.Fixed,
            Components = new SwitchGearComponents
            {
                Switch = new SwitchGearSwitch { RatedCurrent = 630 },
            },
            Functions = new []
            {
                Function.GeneralProtection,
                Function.GeneratorIncomer,
                Function.LineProtection,
                Function.MotorProtection,
                Function.SectionSwitch,
                Function.TransformerProtection, // Решено при обсуждении
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                    SwitchGearOption.Telemechanics_Control_OffExternalSignal,
                    SwitchGearOption.Telemechanics_Control_OffUndervoltage,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerPosition,
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerTripping,
                    SwitchGearOption.Telemechanics_Signalization_EarthSwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_ForbiddenClosing,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_CircuitBreakerPositionAuxContact,
                    _rm6_ForbiddenClosingUnderFaultAuxContact,
                    _rm6_TrippingIndicationAuxContact,
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _ctr2200,
                    _ctrh2200,
                    _cUa,
                    _cUb,
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                ProtectionRelays = new UniqueCollection<ProtectionRelay>
                {
                    _vip400,
                    _vip410A,
                    _vip410E,
                },
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.ProtectionRelay = switchGearFunction.CompatibleComponents.GetDefaultComponent<ProtectionRelay>();

        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    private SwitchGearFunction RM6_BC()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_BC, new Product
        {
            Name = "Bc",
            Description = SwitchGearDescriptions.RM6_BC,
            LongDescription = SwitchGearDescriptions.RM6_BC_Long,
            Image = SwitchGearImages.RM6_BC_150x300,
            Dimensions = SwitchGearDimensions.RM6_BC,
        })
        {
            IsOnlyInternalConnections = true,
            SwitchMountType = SwitchMountType.Fixed,
            Components = new SwitchGearComponents
            {
                Switch = new SwitchGearSwitch { RatedCurrent = 630 },
            },
            Functions = new []
            {
                Function.SectionSwitch,
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                    SwitchGearOption.Telemechanics_Control_OffExternalSignal,
                    SwitchGearOption.Telemechanics_Control_OffUndervoltage,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerPosition,
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerTripping,
                    SwitchGearOption.Telemechanics_Signalization_EarthSwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_ForbiddenClosing,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_CircuitBreakerPositionAuxContact,
                    _rm6_ForbiddenClosingUnderFaultAuxContact,
                    _rm6_TrippingIndicationAuxContact,
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _cUa,
                    _cUb,
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                ProtectionRelays = new UniqueCollection<ProtectionRelay>
                {
                    _vip400,
                    _vip410A,
                    _vip410E,
                },
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.ProtectionRelay = switchGearFunction.CompatibleComponents.GetDefaultComponent<ProtectionRelay>();
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    private SwitchGearFunction RM6_D()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_D, new Product
        {
            Name = "D",
            Description = SwitchGearDescriptions.RM6_D,
            LongDescription = SwitchGearDescriptions.RM6_D_Long,
            Image = SwitchGearImages.RM6_D_150x300,
            Dimensions = SwitchGearDimensions.RM6_D,
        })
        {
            SwitchMountType = SwitchMountType.Fixed,
            Components = new SwitchGearComponents
            {
                Switch = new SwitchGearSwitch { RatedCurrent = 200 },
            },
            Functions = new []
            {
                Function.GeneralProtection,
                Function.MotorProtection,
                Function.SectionSwitch,
                Function.TransformerProtection,
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                    SwitchGearOption.Telemechanics_Control_OffExternalSignal,
                    SwitchGearOption.Telemechanics_Control_OffUndervoltage,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerPosition,
                    SwitchGearOption.Telemechanics_Signalization_CircuitBreakerTripping,
                    SwitchGearOption.Telemechanics_Signalization_EarthSwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_ForbiddenClosing,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_CircuitBreakerPositionAuxContact,
                    _rm6_ForbiddenClosingUnderFaultAuxContact,
                    _rm6_TrippingIndicationAuxContact,
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _cUa,
                    _cUb,
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                ProtectionRelays = new UniqueCollection<ProtectionRelay>
                {
                    _vip40_100,
                    _vip40_200,
                    _vip45_100,
                    _vip45_200,
                    _vip400,
                    _vip410A,
                    _vip410E,
                },
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.ProtectionRelay = switchGearFunction.CompatibleComponents.GetDefaultComponent<ProtectionRelay>();
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    private SwitchGearFunction RM6_I()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_I, new Product
        {
            Name = "I",
            Description = SwitchGearDescriptions.RM6_I,
            LongDescription = SwitchGearDescriptions.RM6_I_Long,
            Image = SwitchGearImages.RM6_I_150x300,
            Dimensions = SwitchGearDimensions.RM6_I,
        })
        {
            SwitchMountType = SwitchMountType.Fixed,
            Functions = new []
            {
                Function.LineFeeder,
                Function.SectionSwitch,
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_SwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_EarthSwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_FaultPassageIndicators,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                DefaultComponents = new UniqueCollection<Manufactured>
                {
                    _flair22d,
                },
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_MainSwitchPositionAuxContact
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _ctr2200,
                    _ctrh2200,
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                FaultPassageIndicators = new UniqueCollection<FaultPassageIndicator>
                {
                    _flair21d,
                    _flair22d,
                    _flair23d,
                    _flair23dm,
                },
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.FaultPassageIndicator = switchGearFunction.CompatibleComponents.GetDefaultComponent<FaultPassageIndicator>();
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    private SwitchGearFunction RM6_IC()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_IC, new Product
        {
            Name = "Ic",
            Description = SwitchGearDescriptions.RM6_IC,
            LongDescription = SwitchGearDescriptions.RM6_IC_Long,
            Image = SwitchGearImages.RM6_IC_150x300,
            Dimensions = SwitchGearDimensions.RM6_IC,
        })
        {
            IsOnlyInternalConnections = true,
            SwitchMountType = SwitchMountType.Fixed,
            Functions = new []
            {
                Function.SectionSwitch,
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_SwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_EarthSwitchPosition,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                DefaultComponents = new UniqueCollection<Manufactured>
                {
                    _flair22d,
                },
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_MainSwitchPositionAuxContact
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _ctr2200,
                    _ctrh2200,
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                FaultPassageIndicators = new UniqueCollection<FaultPassageIndicator>
                {
                    _flair21d,
                    _flair22d,
                    _flair23d,
                    _flair23dm,
                },
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.FaultPassageIndicator = switchGearFunction.CompatibleComponents.GetDefaultComponent<FaultPassageIndicator>();
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    private SwitchGearFunction RM6_Mt()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_Mt, new Product
        {
            Name = "Mt",
            Description = SwitchGearDescriptions.RM6_Mt,
            LongDescription = SwitchGearDescriptions.RM6_Mt_Long,
            Image = SwitchGearImages.RM6_Mt_150x300,
            Dimensions = SwitchGearDimensions.RM6_Mt,
        })
        {
            IsOnlyInternalConnections = true,
            SwitchMountType = SwitchMountType.Fixed,
            Functions = new[]
            {
                Function.Metering,
            },
        };
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);

        return switchGearFunction;
    }

    private SwitchGearFunction RM6_O()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_O, new Product
        {
            Name = "O",
            Description = SwitchGearDescriptions.RM6_O,
            LongDescription = SwitchGearDescriptions.RM6_O_Long,
            Image = SwitchGearImages.RM6_O_150x300,
            Dimensions = SwitchGearDimensions.RM6_O,
        })
        {
            SwitchMountType = SwitchMountType.Fixed,
            Functions = new[]
            {
                Function.CableConnection,
            },
        };
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);

        return switchGearFunction;
    }

    private SwitchGearFunction RM6_Q()
    {
        var switchGearFunction = new SwitchGearFunction(SG.RM6.Series, SG.RM6.RM6_Q, new Product
        {
            Name = "Q",
            Description = SwitchGearDescriptions.RM6_Q,
            LongDescription = SwitchGearDescriptions.RM6_Q_Long,
            Image = SwitchGearImages.RM6_Q_150x300,
            Dimensions = SwitchGearDimensions.RM6_Q,
        })
        {
            SwitchMountType = SwitchMountType.Fixed,
            Components = new SwitchGearComponents
            {
                Switch = new SwitchGearSwitch { RatedCurrent = 200 },
            },
            Functions = new []
            {
                Function.TransformerProtection,
            },
            AvailableOptions = new SwitchGearOptions()
            {
                Controls =
                {
                    SwitchGearOption.Telemechanics_Control_On,
                    SwitchGearOption.Telemechanics_Control_Off,
                    SwitchGearOption.Telemechanics_Control_OffExternalSignal,
                    SwitchGearOption.Telemechanics_Control_OffUndervoltage,
                },
                Signalization =
                {
                    SwitchGearOption.Telemechanics_Signalization_FuseSwitchPosition,
                    SwitchGearOption.Telemechanics_Signalization_FuseBlown,
                },
                Telemetry =
                {
                    SwitchGearOption.Telemechanics_Telemetry_Current,
                    SwitchGearOption.Telemechanics_Telemetry_Voltage,
                    SwitchGearOption.Telemechanics_Telemetry_ActivePower,
                    SwitchGearOption.Telemechanics_Telemetry_ReactivePower,
                }
            },
            CompatibleComponents = new SwitchGearComponentCollections
            {
                DefaultComponents =
                {
                    _allFuses.FirstOrDefault(x => x.Current >= 50),
                },
                AuxiliaryContacts = new UniqueCollection<AuxiliaryContact>
                {
                    _rm6_FuseBlownAuxContact,
                    _rm6_FuseSwitchCombinationsPositionAuxContact,
                },
                CurrentTransformers = new UniqueCollection<CurrentTransformer>
                {
                    _easergySctr500_1_class1,
                    _easergySctr500_1_class3,
                    _easergySctrh500_1_class3,
                },
                MeasuringVoltageTransformers = new UniqueCollection<MeasuringVoltageTransformer>
                {
                    _lpvt,
                },
                Fuses = _allFuses,
                RemoteControls = _rm6_MotorMechanisms,
                ShuntTripReleases = _rm6_ShuntTripReleases,
                SwitchControllers = new UniqueCollection<SwitchController>
                {
                    _easergySC150,
                },
                UndervoltageReleases = _rm6_UndervoltageReleases,
            },
        };

        switchGearFunction.Components.SetFuse(switchGearFunction.CompatibleComponents.GetDefaultComponent<Fuse>());
        
        _rm6_ElectricalCharacteristicsFactory.InitializeElectricalCharacteristics(switchGearFunction);
        
        return switchGearFunction;
    }

    #endregion
}