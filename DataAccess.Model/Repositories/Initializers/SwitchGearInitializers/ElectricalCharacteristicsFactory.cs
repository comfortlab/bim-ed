﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage.Properties;

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public abstract class ElectricalCharacteristicsFactory
{
    protected SwitchGearFunction SwitchGearFunction;

    public void InitializeElectricalCharacteristics(SwitchGearFunction switchGearFunction)
    {
        SwitchGearFunction = switchGearFunction;
        SwitchGearFunction.ElectricalCharacteristicsSource.BusbarCurrentVoltageBinding.Merge(GetBusbarCurrentVoltageBinding());
        SwitchGearFunction.ElectricalCharacteristicsSource.RatedCurrentVoltageBinding.Merge(GetRatedCurrentVoltageBinding());
        SwitchGearFunction.ElectricalCharacteristicsSource.CubicleVoltages.UnionWith(SwitchGearFunction.ElectricalCharacteristicsSource.RatedCurrentVoltageBinding.GetAllValues2());
        SwitchGearFunction.ElectricalCharacteristicsSource.ThermalCurrentVoltageBinding.Merge(GetThermalCurrentVoltageBinding());
        SwitchGearFunction.ElectricalCharacteristicsSource.ThermalCurrentBusbarCurrentBinding.Merge(GetThermalCurrentBusbarCurrentBinding());
        SwitchGearFunction.ElectricalCharacteristicsSource.ArcInsulations.Add(GetArcInsulations());
        SwitchGearFunction.ElectricalCharacteristicsSource.CubicleInsulations.Add(GetCubicleInsulations());
    }

    protected abstract BindingValues<int, SwitchGearVoltage> GetBusbarCurrentVoltageBinding();
    protected abstract BindingValues<int, SwitchGearVoltage> GetRatedCurrentVoltageBinding();
    protected abstract BindingValues<SwitchGearVoltage, CurrentTimePoint> GetThermalCurrentVoltageBinding();
    protected abstract BindingValues<int, CurrentTimePoint> GetThermalCurrentBusbarCurrentBinding();
    protected abstract InsulationType GetArcInsulations();
    protected abstract InsulationType GetCubicleInsulations();
}