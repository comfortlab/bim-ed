﻿using CoLa.BimEd.BusinessLogic.Model.Electrical;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public static class CurrentTimePointSet
{
    public static readonly CurrentTimePoint kA_12_5_s_1 = new(12_500, 1);
    public static readonly CurrentTimePoint kA_16_s_1   = new(16_000, 1);
    public static readonly CurrentTimePoint kA_16_s_3   = new(16_000, 3);
    public static readonly CurrentTimePoint kA_17_5_s_1 = new(17_500, 1);
    public static readonly CurrentTimePoint kA_17_5_s_3 = new(17_500, 3);
    public static readonly CurrentTimePoint kA_20_s_1   = new(20_000, 4);
    public static readonly CurrentTimePoint kA_20_s_3   = new(20_000, 4);
    public static readonly CurrentTimePoint kA_20_s_4   = new(20_000, 4);
    public static readonly CurrentTimePoint kA_21_s_1   = new(21_000, 1);
    public static readonly CurrentTimePoint kA_21_s_3   = new(21_000, 3);
    public static readonly CurrentTimePoint kA_25_s_1   = new(25_000, 1);
    public static readonly CurrentTimePoint kA_25_s_3   = new(25_000, 3);
}