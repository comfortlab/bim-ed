﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products.SwitchGears;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.DataAccess.Model.Resources.Products;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Collections;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public static class SwitchGearComponentsInitializer
{
    #region ElectricalQuantities

    private static readonly ElectricalQuantity V24DC  = ElectricalQuantity.CreateDC(24, Units.V);
    private static readonly ElectricalQuantity V48DC  = ElectricalQuantity.CreateDC(48, Units.V);
    private static readonly ElectricalQuantity V60DC  = ElectricalQuantity.CreateDC(60, Units.V);
    private static readonly ElectricalQuantity V110DC = ElectricalQuantity.CreateDC(110, Units.V);
    private static readonly ElectricalQuantity V125DC = ElectricalQuantity.CreateDC(125, Units.V);
    private static readonly ElectricalQuantity V220DC = ElectricalQuantity.CreateDC(220, Units.V);
    private static readonly ElectricalQuantity V120AC = ElectricalQuantity.CreateAC(120, Units.V);
    private static readonly ElectricalQuantity V230AC = ElectricalQuantity.CreateAC(230, Units.V);
    private static readonly ElectricalQuantity VA4_5  = ElectricalQuantity.Create(4.5, Units.VA);
    private static readonly ElectricalQuantity VA200  = ElectricalQuantity.Create(200, Units.VA);
    private static readonly ElectricalQuantity VA280  = ElectricalQuantity.Create(280, Units.VA);
    private static readonly ElectricalQuantity VA400  = ElectricalQuantity.Create(400, Units.VA);
    private static readonly ElectricalQuantity VA750  = ElectricalQuantity.Create(750, Units.VA);
    private static readonly ElectricalQuantity W4_5   = ElectricalQuantity.Create(4.5, Units.W);
    private static readonly ElectricalQuantity W200   = ElectricalQuantity.Create(200, Units.W);
    private static readonly ElectricalQuantity W240   = ElectricalQuantity.Create(240, Units.W);
    private static readonly ElectricalQuantity W250   = ElectricalQuantity.Create(250, Units.W);
    private static readonly ElectricalQuantity W300   = ElectricalQuantity.Create(300, Units.W);

    #endregion
    
    public static SwitchGearComponentCollections Initialize()
    {
        var components = new SwitchGearComponentCollections();
        
        components.AuxiliaryContacts = InitializeAuxiliaryContacts();
        components.CurrentTransformers = InitializeCurrentTransformers();
        components.FaultPassageIndicators = InitializeFaultPassageIndicators(components.CurrentTransformers);
        components.Fuses = InitializeFuses();
        components.MeasuringVoltageTransformers = InitializeMeasuringVoltageTransformers();
        components.ProtectionRelays = InitializeProtectionRelays(components.CurrentTransformers);
        components.RemoteControls = InitializeMotorMechanisms();
        components.ShuntTripReleases = InitializeShuntTripReleases();
        components.SwitchControllers = InitializeSwitchControllers(components.CurrentTransformers, components.MeasuringVoltageTransformers);
        components.UndervoltageReleases = InitializeUndervoltageReleases();

        return components;
    }

    private static UniqueCollection<AuxiliaryContact> InitializeAuxiliaryContacts() => new()
    {
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.MainSwitchPositionAuxContact)
        {
            Product =
            {
                Name = "LSBw 2NO+2NC",
                Description = SwitchGearComponentDescriptions.AuxContact_MainSwitchPositionIndication,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            },
        },
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.CircuitBreakerPositionAuxContact)
        {
            Product =
            {
                Name = "CB 2NO+2NC",
                Description = SwitchGearComponentDescriptions.AuxContact_CircuitBreakerPositionIndication,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            }
        },
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.FuseBlownAuxContact)
        {
            Product =
            {
                Name = "1NO",
                Description = SwitchGearComponentDescriptions.AuxContact_FuseBlown,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            }
        },
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.FuseSwitchCombinationsPositionAuxContact)
        {
            Product =
            {
                Name = "LSBw 2NO+2NC",
                Description = SwitchGearComponentDescriptions.AuxContact_FuseSwitchCombinationsPositionIndication,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            }
        },
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.ForbiddenClosingUnderFaultAuxContact)
        {
            Product =
            {
                Name = "1NC",
                Description = SwitchGearComponentDescriptions.AuxContact_ForbiddenClosingUnderFault,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            }
        },
        new AuxiliaryContact(Guids.Products.SwitchGears.AuxiliaryContacts.RM6.TrippingIndicationAuxContact)
        {
            Product =
            {
                Name = "1NO",
                Description = SwitchGearComponentDescriptions.AuxContact_TrippingIndication,
                Image = SwitchGearComponentImages.AuxiliaryContacts_RM6,
            }
        },
    };

    private static UniqueCollection<CurrentTransformer> InitializeCurrentTransformers() => new()
    {
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.CUa)
        {
            Product =
            {
                RangeOfProduct = "VIP",
                Name = "CUa",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_CUa_Description,
                LongDescription = SwitchGearComponentDescriptions.CurrentTransformer_CUa_LongDescription,
                Image = SwitchGearComponentImages.ProtectionRelay_CUa,
            },
            PrimaryCurrent = 200,
            MeasuringAccuracyClass = AllAccuracyClasses.M1,
            ProtectionAccuracyClass = AllAccuracyClasses.P5_30,
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.CUb)
        {
            Product =
            {
                RangeOfProduct = "VIP",
                Name = "CUb",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_CUb_Description,
                LongDescription = SwitchGearComponentDescriptions.CurrentTransformer_CUb_LongDescription,
                Image = SwitchGearComponentImages.ProtectionRelay_CUb,
            },
            PrimaryCurrent = 630,
            MeasuringAccuracyClass = AllAccuracyClasses.M1,
            ProtectionAccuracyClass = AllAccuracyClasses.P5_30,
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.CTR2200)
        {
            Product =
            {
                Reference = "59925",
                RangeOfProduct = "Easergy",
                Name = "CTR2200",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_CTR2200_Description,
                Image = SwitchGearComponentImages.Measuring_CTR2200,
            },
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.CTRH2200)
        {
            Product =
            {
                Reference = "59926",
                RangeOfProduct = "Easergy",
                Name = "CTRH2200",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_CTRH2200_Description,
                Image = SwitchGearComponentImages.Measuring_CTRH2200,
            },
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.EasergySCTR500_1_class1)
        {
            Product =
            {
                Reference = "EMS85182",
                RangeOfProduct = "Easergy T300",
                Name = "SCTR500-1 class 1",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_EasergySCTR500_1_class1_Description,
                Image = SwitchGearComponentImages.Measuring_EMS58182_288x288,
            },
            PrimaryCurrent = 500,
            MeasuringAccuracyClass = AllAccuracyClasses.M1,
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.EasergySCTR500_1_class3)
        {
            Product =
            {
                Reference = "EMS58171",
                RangeOfProduct = "Easergy T300",
                Name = "SCTR500-1 class 3",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_EasergySCTR500_1_class3_Description,
                Image = SwitchGearComponentImages.Measuring_EMS58182_288x288,
            },
            PrimaryCurrent = 500,
            MeasuringAccuracyClass = AllAccuracyClasses.M3,
        },
        new CurrentTransformer(Guids.Products.SwitchGears.CurrentTransformers.EasergySCTRH500_1_class3)
        {
            Product =
            {
                Reference = "EMS58111",
                RangeOfProduct = "Easergy T300",
                Name = "SCTRH500-1 class 3",
                Description = SwitchGearComponentDescriptions.CurrentTransformer_EasergySCTRH500_1_class3_Description,
                Image = SwitchGearComponentImages.Measuring_EMS58111_288x288,
            },
            PrimaryCurrent = 500,
            MeasuringAccuracyClass = AllAccuracyClasses.M3,
        },
    };

    private static UniqueCollection<FaultPassageIndicator> InitializeFaultPassageIndicators(ICollection<CurrentTransformer> currentTransformers)
    {
        var ctr2200 = Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.CTR2200);
        var ctrh2200 = Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.CTRH2200);
        
        return new UniqueCollection<FaultPassageIndicator>
        {
            new(Guids.Products.SwitchGears.FaultPassageIndicators.Flair21D)
            {
                Product =
                {
                    Reference = "EMS58351",
                    RangeOfProduct = "Easergy Flair",
                    Name = "Flair 21D",
                    Description = SwitchGearComponentDescriptions.FaultPassageIndicator_Flair21D_Description,
                    ProductAdvantages = new ProductAdvantages[]
                    {
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair21D_Advantages1),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair21D_Advantages2),
                    },
                    Image = SwitchGearComponentImages.Controller_Flair21D_288x288,
                },
                CompatibleComponents = new UniqueCollection<CurrentTransformer>()
                {
                    ctr2200,
                    ctrh2200
                },
            },
            new(Guids.Products.SwitchGears.FaultPassageIndicators.Flair22D)
            {
                Product =
                {
                    Reference = "EMS58352",
                    RangeOfProduct = "Easergy Flair",
                    Name = "Flair 22D",
                    Description = SwitchGearComponentDescriptions.FaultPassageIndicator_Flair22D_Description,
                    ProductAdvantages = new ProductAdvantages[]
                    {
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair22D_Advantages1),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair22D_Advantages2),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair22D_Advantages3),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair22D_Advantages4),
                    },
                    Image = SwitchGearComponentImages.Controller_Flair22D_288x288,
                },
                CompatibleComponents = new UniqueCollection<CurrentTransformer>()
                {
                    ctr2200,
                    ctrh2200
                },
            },
            new(Guids.Products.SwitchGears.FaultPassageIndicators.Flair23D)
            {
                Product =
                {
                    Reference = "EMS58354",
                    RangeOfProduct = "Easergy Flair",
                    Name = "Flair 23D",
                    Description = SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23D_Description,
                    ProductAdvantages = new ProductAdvantages[]
                    {
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23D_Advantages1),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23D_Advantages2),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23D_Advantages3),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23D_Advantages4),
                    },
                    Image = SwitchGearComponentImages.Controller_Flair23D_288x288,
                },
                CompatibleComponents = new UniqueCollection<CurrentTransformer>()
                {
                    ctr2200,
                    ctrh2200
                },
            },
            new(Guids.Products.SwitchGears.FaultPassageIndicators.Flair23DM)
            {
                Product =
                {
                    Reference = "EMS58355",
                    RangeOfProduct = "Easergy Flair",
                    Name = "Flair 23DM",
                    Description = SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Description,
                    ProductAdvantages = new ProductAdvantages[]
                    {
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages1),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages2),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages3),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages4),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages5),
                        new(SwitchGearComponentDescriptions.FaultPassageIndicator_Flair23DM_Advantages6),
                    },
                    Image = SwitchGearComponentImages.Controller_Flair23DM_288x288,
                },
                CompatibleComponents = new UniqueCollection<CurrentTransformer>()
                {
                    ctr2200,
                    ctrh2200
                },
            },
        };
    }
    
    private const string FusarcCf = "Fusarc CF";
    private const string Siba = "SIBA";
    
    private static UniqueCollection<Fuse> InitializeFuses() => new()
    {
        new Fuse(FusarcCf, current:  10,   voltage: 12_000, length: 0.292, diameter: 0.0505, weight: 1.2),
        new Fuse(FusarcCf, current:  16,   voltage: 12_000, length: 0.292, diameter: 0.0505, weight: 1.2),
        new Fuse(FusarcCf, current:  20,   voltage: 12_000, length: 0.292, diameter: 0.0505, weight: 1.2),
        new Fuse(FusarcCf, current:  25,   voltage: 12_000, length: 0.292, diameter: 0.0505, weight: 1.2),
        new Fuse(FusarcCf, current:  31.5, voltage: 12_000, length: 0.292, diameter: 0.055,  weight: 1.8),
        new Fuse(FusarcCf, current:  40,   voltage: 12_000, length: 0.292, diameter: 0.055,  weight: 1.8),
        new Fuse(FusarcCf, current:  50,   voltage: 12_000, length: 0.292, diameter: 0.076,  weight: 3.2),
        new Fuse(FusarcCf, current:  63,   voltage: 12_000, length: 0.292, diameter: 0.076,  weight: 3.2),
        new Fuse(FusarcCf, current:  80,   voltage: 12_000, length: 0.292, diameter: 0.076,  weight: 3.2),
        new Fuse(FusarcCf, current: 100,   voltage: 12_000, length: 0.292, diameter: 0.076,  weight: 3.2),
        new Fuse(FusarcCf, current: 125,   voltage: 12_000, length: 0.442, diameter: 0.086,  weight: 5),
        new Fuse(FusarcCf, current:  10,   voltage: 24_000, length: 0.442, diameter: 0.0505, weight: 1.7),
        new Fuse(FusarcCf, current:  16,   voltage: 24_000, length: 0.442, diameter: 0.0505, weight: 1.7),
        new Fuse(FusarcCf, current:  20,   voltage: 24_000, length: 0.442, diameter: 0.0505, weight: 1.7),
        new Fuse(FusarcCf, current:  25,   voltage: 24_000, length: 0.442, diameter: 0.0505, weight: 1.7),
        new Fuse(FusarcCf, current:  31.5, voltage: 24_000, length: 0.442, diameter: 0.055,  weight: 2.6),
        new Fuse(FusarcCf, current:  40,   voltage: 24_000, length: 0.442, diameter: 0.055,  weight: 2.6),
        new Fuse(FusarcCf, current:  50,   voltage: 24_000, length: 0.442, diameter: 0.076,  weight: 4.5),
        new Fuse(FusarcCf, current:  63,   voltage: 24_000, length: 0.442, diameter: 0.076,  weight: 4.5),
        new Fuse(FusarcCf, current:  80,   voltage: 24_000, length: 0.442, diameter: 0.076,  weight: 4.5),
        new Fuse(FusarcCf, current: 100,   voltage: 24_000, length: 0.442, diameter: 0.086,  weight: 5.7),
        new Fuse(Siba, "30-020-13.160", current: 160, voltage: 12_000, length: 0.292, diameter: 0.085,  weight: 3.8),
    };
    
    private static UniqueCollection<MeasuringVoltageTransformer> InitializeMeasuringVoltageTransformers() => new()
    {
        new MeasuringVoltageTransformer(Guids.Products.SwitchGears.MeasuringVoltageTransformers.LPVT)
        {
            Product =
            {
                Name = "LPVT",
                Description = SwitchGearComponentDescriptions.MotorMechanism,
                Image = SwitchGearComponentImages.Measuring_LPVT,
            },
            MeasuringAccuracyClass = AllAccuracyClasses.M05,
        },
    };
    
    private static UniqueCollection<ProtectionRelay> InitializeProtectionRelays(ICollection<CurrentTransformer> currentTransformers)
    {
        var CUa = Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.CUa); 
        var CUb = Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.CUb); 
        
        return new UniqueCollection<ProtectionRelay>
        {
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP40_100)
            {
                Product =
                {
                    Reference = "REL 59910",
                    Name = "VIP 40 100A",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP40_100_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP40_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP40,
                },
                Functions =
                {
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                },
                CompatibleComponents =
                {
                    CUa
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP40_200)
            {
                Product =
                {
                    Reference = "REL 59911",
                    Name = "VIP 40 200A",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP40_200_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP40_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP40,
                },
                Functions =
                {
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                },
                CompatibleComponents =
                {
                    CUa
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP45_100)
            {
                Product =
                {
                    Reference = "REL 59912",
                    Name = "VIP 45 100A",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP45_100_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP45_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP40,
                },
                Functions =
                {
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Protection_ANSI_51N,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_EarthCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                },
                CompatibleComponents =
                {
                    CUa
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP45_200)
            {
                Product =
                {
                    Reference = "REL 59913",
                    Name = "VIP 45 200A",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP45_200_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP45_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP40,
                },
                Functions =
                {
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Protection_ANSI_51N,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_EarthCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                },
                CompatibleComponents =
                {
                    CUa
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP400)
            {
                Product =
                {
                    Reference = "REL 59915",
                    Name = "VIP 400",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP400_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP400_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP400,
                },
                Functions =
                {
                    Function.BusRiser,
                    Function.GeneralProtection,
                    Function.GeneratorIncomer,
                    Function.LineFeeder,
                    Function.LineProtection,
                    Function.MotorProtection,
                    Function.SectionSwitch,
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_49,
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Protection_ANSI_51N,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_OvercurrentAndBreakingProfile,
                    AllRelayFunctions.Control_TimeTaggedEvents_Local,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_EarthCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                    AllRelayFunctions.Measurement_LoadHistory,
                },
                CompatibleComponents =
                {
                    CUa,
                    CUb,
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP410A)
            {
                Product =
                {
                    Reference = "REL 59916",
                    Name = "VIP 410 A",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP410_A_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP410_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP400,
                },
                Functions =
                {
                    Function.BusRiser,
                    Function.GeneralProtection,
                    Function.GeneratorIncomer,
                    Function.LineFeeder,
                    Function.LineProtection,
                    Function.MotorProtection,
                    Function.SectionSwitch,
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_49,
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Protection_ANSI_51N,
                    AllRelayFunctions.Protection_ANSI_51N_HighSensitivity,
                    AllRelayFunctions.Protection_ColdLoadPickUp,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_Communication_Modbus,
                    AllRelayFunctions.Control_ExternalTrippingInput,
                    AllRelayFunctions.Control_OvercurrentAndBreakingProfile,
                    AllRelayFunctions.Control_TimeTaggedEvents_Local,
                    AllRelayFunctions.Control_TimeTaggedEvents_Remote,
                    AllRelayFunctions.Control_TripIndication_3_OutputRelays,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_EarthCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                    AllRelayFunctions.Measurement_LoadHistory,
                },
                CompatibleComponents =
                {
                    CUa,
                    CUb,
                },
            },
            new(Guids.Products.SwitchGears.ProtectionRelays.VIP410E)
            {
                Product =
                {
                    Reference = "REL 59917",
                    Name = "VIP 410 E",
                    Description = SwitchGearComponentDescriptions.ProtectionRelay_VIP410_E_Name,
                    LongDescription = SwitchGearComponentDescriptions.ProtectionRelay_VIP410_Function,
                    Image = SwitchGearComponentImages.ProtectionRelay_VIP400,
                },
                Functions =
                {
                    Function.BusRiser,
                    Function.GeneralProtection,
                    Function.GeneratorIncomer,
                    Function.LineFeeder,
                    Function.LineProtection,
                    Function.MotorProtection,
                    Function.SectionSwitch,
                    Function.TransformerProtection,
                },
                RelayFunctions =
                {
                    AllRelayFunctions.Protection_ANSI_49,
                    AllRelayFunctions.Protection_ANSI_50_51,
                    AllRelayFunctions.Protection_ANSI_51N,
                    AllRelayFunctions.Protection_ANSI_51N_HighSensitivity,
                    AllRelayFunctions.Protection_ColdLoadPickUp,
                    AllRelayFunctions.Control_ANSI_74TC,
                    AllRelayFunctions.Control_Communication_Modbus,
                    AllRelayFunctions.Control_ExternalTrippingInput,
                    AllRelayFunctions.Control_OvercurrentAndBreakingProfile,
                    AllRelayFunctions.Control_TimeTaggedEvents_Local,
                    AllRelayFunctions.Control_TimeTaggedEvents_Remote,
                    AllRelayFunctions.Control_TripIndication_3_OutputRelays,
                    AllRelayFunctions.Control_TripIndication_Local,
                    AllRelayFunctions.Control_TripIndication_Remote,
                    AllRelayFunctions.Measurement_PhaseCurrent,
                    AllRelayFunctions.Measurement_EarthCurrent,
                    AllRelayFunctions.Measurement_PhasePeakDemandCurrent,
                    AllRelayFunctions.Measurement_LoadHistory,
                },
                CompatibleComponents =
                {
                    CUa,
                    CUb,
                },
            },
        };
    }

    private static UniqueCollection<SwitchController> InitializeSwitchControllers(
        ICollection<CurrentTransformer> currentTransformers,
        ICollection<MeasuringVoltageTransformer> voltageTransformers) => new()
    {
        new SwitchController(Guids.Products.SwitchGears.SwitchController.EasergySC150)
        {
            Product =
            {
                Reference = "EMS59201",
                RangeOfProduct = "Easergy T300",
                Name = "SC150",
                Description = SwitchGearComponentDescriptions.SwitchController_SC150_Description,
                Image = SwitchGearComponentImages.Controller_SC150_288x288,
            },
            RelayFunctions =
            {
                AllRelayFunctions.Protection_ANSI_50_51,
                AllRelayFunctions.Protection_ANSI_50N_51N,
                AllRelayFunctions.Protection_ANSI_67,
                AllRelayFunctions.Protection_ANSI_67N,
                AllRelayFunctions.Protection_ANSI_47,
                AllRelayFunctions.Protection_ANSI_27,
                AllRelayFunctions.Protection_ANSI_59,
                AllRelayFunctions.Protection_ANSI_59N,
                AllRelayFunctions.Protection_ANSI_37,
            },
            CompatibleComponents =
            {
                Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.EasergySCTR500_1_class1),
                Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.EasergySCTR500_1_class3),
                Get(currentTransformers, Guids.Products.SwitchGears.CurrentTransformers.EasergySCTRH500_1_class3),
                Get(voltageTransformers, Guids.Products.SwitchGears.MeasuringVoltageTransformers.LPVT),
            },
        },
    };
    
    private static UniqueCollection<MotorMechanism> InitializeMotorMechanisms() => new()
    {
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC24,  V24DC,  W200),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC48,  V48DC,  W240),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC60,  V60DC,  W240),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC110, V110DC, W240),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC125, V125DC, W240),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.DC220, V220DC, W240),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.AC120, V120AC, VA280),
        CreateMotorMechanism(Guids.Products.SwitchGears.MotorMechanisms.RM6.AC230, V230AC, VA280),
    };

    private static UniqueCollection<ShuntTripRelease> InitializeShuntTripReleases() => new()
    {
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC24, V24DC, W200, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC48, V48DC, W250, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC60, V60DC, W250, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC110, V110DC, W300, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC125, V125DC, W300, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.DC220, V220DC, W300, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.AC120, V120AC, VA400, responseTime: 0.035),
        CreateShuntTripRelease(Guids.Products.SwitchGears.ShuntTripReleases.RM6.AC230, V230AC, VA750, responseTime: 0.035),
    };

    private static UniqueCollection<UndervoltageRelease> InitializeUndervoltageReleases() => new()
    {
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC24, V24DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC48, V48DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC60, V60DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC110, V110DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC125, V125DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.DC220, V220DC, W200, W4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.AC120, V120AC, VA200, VA4_5),
        CreateUndervoltageRelease(Guids.Products.SwitchGears.UndervoltageReleases.RM6.AC230, V230AC, VA200, VA4_5),
    };

    private static MotorMechanism CreateMotorMechanism(Guid guid, ElectricalQuantity voltage, ElectricalQuantity power) => new(guid)
    {
        Product =
        {
            Name = GetName("M", voltage, power),
            Description = GetMotorMechanismDescription(voltage, power),
            Image = SwitchGearComponentImages.MotorMechanism_MCH,
        },
        Voltage = voltage,
        Power = power,
    };

    private static ShuntTripRelease CreateShuntTripRelease(Guid guid, ElectricalQuantity voltage, ElectricalQuantity power, double responseTime) => new(guid)
    {
        Product =
        {
            Name = GetName("MX", voltage, power),
            Description = GetShuntTripReleaseDescription(voltage, power, responseTime),
            Image = SwitchGearComponentImages.Release_MX,
        },
        Voltage = voltage,
        Power = power,
        ResponseTime = responseTime,
    };

    private static readonly Range<double> MnDelayTime = new(0.5, 3);
    private static readonly Range<double> MnThresholdClosing = new(0.85);
    private static readonly Range<double> MnThresholdOpening = new(0.35, 0.7);
    
    private static UndervoltageRelease CreateUndervoltageRelease(
        Guid guid, ElectricalQuantity voltage, ElectricalQuantity power, ElectricalQuantity latchedPower) => new(guid)
    {
        Product =
        {
            Name = GetName("MN", voltage, power),
            Description = GetUndervoltageReleaseDescription(voltage, power, MnThresholdOpening, MnThresholdClosing, MnDelayTime),
            Image = SwitchGearComponentImages.Release_MN,
        },
        Voltage = voltage,
        Power = power,
        LatchedPower = latchedPower,
        DelayTime = MnDelayTime,
        ThresholdOpening = MnThresholdOpening,
        ThresholdClosing = MnThresholdClosing,
    };

    private static string GetName(string prefix, ElectricalQuantity voltage, ElectricalQuantity power) => $"{prefix} {voltage} {power}";
    private static string GetMotorMechanismDescription(ElectricalQuantity voltage, ElectricalQuantity power) => 
        $"{SwitchGearComponentDescriptions.MotorMechanism} {voltage} {power}";
    private static string GetShuntTripReleaseDescription(ElectricalQuantity voltage, ElectricalQuantity power, double time) =>
        $"{SwitchGearComponentDescriptions.TripUnit_ShuntTripRelease} {voltage} {power} {time * 1000} {Units.ms}";
    private static string GetUndervoltageReleaseDescription(
        ElectricalQuantity voltage, ElectricalQuantity power,
        Range<double> thresholdOpening, Range<double> thresholdClosing, Range<double> delayTime) =>
        $"{SwitchGearComponentDescriptions.TripUnit_UndervoltageRelease} {voltage} {power}, " +
        $"{SwitchGearComponentDescriptions.TripUnit_opening} {thresholdOpening} Un, " +
        $"{SwitchGearComponentDescriptions.TripUnit_closing} {thresholdClosing} Un, " +
        $"{delayTime} {Units.s}";

    private static T Get<T>(IEnumerable<T> items, Guid guid) where T : Manufactured => items.FirstOrDefault(x => x.Product.Guid == guid) ?? throw new KeyNotFoundException($"{typeof(T)} was not found.");
}