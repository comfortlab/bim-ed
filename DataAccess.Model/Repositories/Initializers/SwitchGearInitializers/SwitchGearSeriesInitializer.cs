﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.DataAccess.Model.Resources.Images;
using CoLa.BimEd.DataAccess.Model.Resources.Products;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using SG = CoLa.BimEd.DataAccess.Model.Repositories.Guids.Products.SwitchGears;

// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public class SwitchGearSeriesInitializer : IRepositoryInitializer<SwitchGearSeries>
{
    private readonly Repository<SwitchGearFunction> _switchGearFunctions;

    public SwitchGearSeriesInitializer(Repository<SwitchGearFunction> switchGearFunctions)
    {
        _switchGearFunctions = switchGearFunctions;
    }

    public List<SwitchGearSeries> Initialize() => new()
    {
        RM6().InitializeElectricalCharacteristics(),
        PremSet().InitializeElectricalCharacteristics(),
        SM6().InitializeElectricalCharacteristics(),
    };

    private SwitchGearSeries PremSet() => new(SG.PremSet.Series, "PremSet")
    {
        SwitchGearFunctions = _switchGearFunctions.Where(x => x.SwitchGearSeriesGuid == SG.PremSet.Series).ToArray(),
        Description = SwitchGearSeriesDescriptions.PremSet_Description,
        LongDescription = SwitchGearSeriesDescriptions.PremSet_LongDescription,
        Image = SwitchGearImages.PremSet,
        ProductAdvantages = new ProductAdvantages[]
        {
            
        },
        ElectricalCharacteristicsSource =
        {
            Frequencies =
            {
                ElectricalConstants.Frequency.Hz_50,
                ElectricalConstants.Frequency.Hz_60,
            },

            OperatingVoltages =
            {
                // ElectricalQuantity.CreateDC( 24),
                // ElectricalQuantity.CreateDC( 48),
                // ElectricalQuantity.CreateDC( 60),
                // ElectricalQuantity.CreateDC(110),
                // ElectricalQuantity.CreateDC(125),
                // ElectricalQuantity.CreateDC(220),
                // ElectricalQuantity.CreateAC(120),
                // ElectricalQuantity.CreateAC(230),
            }
        },
        GasExhausts =
        {
            GasExhaust.Downwards,
            GasExhaust.Upwards,
        },
        NeutralSystems =
        {
            NeutralSystem.Earthed,
            NeutralSystem.Isolated,
        },
        BetweenCompartmentsProtectionIndexes =
        {
            ProtectionIndex.IP2X,
        },
        ExternalFaceProtectionIndexes =
        {
            ProtectionIndex.IP3X,
            ProtectionIndex.IP41,
        },
        HighVoltagePartsProtectionIndexes =
        {
            ProtectionIndex.IP67,
        },
    };

    private SwitchGearSeries RM6() => new(SG.RM6.Series, "RM6")
    {
        IsBlockSwitchGear = true,
        SwitchGearFunctions = _switchGearFunctions.Where(x => x.SwitchGearSeriesGuid == SG.RM6.Series).ToArray(),
        SwitchGearUnits = new Rm6SwitchGearUnitInitializer(_switchGearFunctions).Initialize(),
        Description = SwitchGearSeriesDescriptions.RM6_Description,
        LongDescription = SwitchGearSeriesDescriptions.RM6_LongDescription,
        Image = SwitchGearImages.RM6,
        ProductAdvantages = new ProductAdvantages[]
        {
            new(SwitchGearSeriesDescriptions.RM6_Advantages_Safety_1_Title,
                new List<string>
                {
                    SwitchGearSeriesDescriptions.RM6_Advantages_Safety_1_1,
                    SwitchGearSeriesDescriptions.RM6_Advantages_Safety_1_2,
                    SwitchGearSeriesDescriptions.RM6_Advantages_Safety_1_3,
                    SwitchGearSeriesDescriptions.RM6_Advantages_Safety_1_4,
                }),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Safety_2_Title,
                SwitchGearSeriesDescriptions.RM6_Advantages_Safety_2_Description),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Efficiency_1_Title,
                SwitchGearSeriesDescriptions.RM6_Advantages_Efficiency_1_Description),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Efficiency_2_Title,
                SwitchGearSeriesDescriptions.RM6_Advantages_Efficiency_2_Description),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Connected_1_Title,
                SwitchGearSeriesDescriptions.RM6_Advantages_Connected_1_Description),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Connected_2_Title,
                SwitchGearSeriesDescriptions.RM6_Advantages_Connected_2_Description),

            new(SwitchGearSeriesDescriptions.RM6_Advantages_Connected_3_Title),
        },
        ElectricalCharacteristicsSource =
        {
            Frequencies =
            {
                ElectricalConstants.Frequency.Hz_50,
                ElectricalConstants.Frequency.Hz_60,
            },

            OperatingVoltages =
            {
                ElectricalQuantity.CreateDC( 24),
                ElectricalQuantity.CreateDC( 48),
                ElectricalQuantity.CreateDC( 60),
                ElectricalQuantity.CreateDC(110),
                ElectricalQuantity.CreateDC(125),
                ElectricalQuantity.CreateDC(220),
                ElectricalQuantity.CreateAC(120),
                ElectricalQuantity.CreateAC(230),
            }
        },
        GasExhausts =
        {
            GasExhaust.Downwards,
            GasExhaust.Rear,
        },
        NeutralSystems =
        {
            NeutralSystem.Earthed,
            NeutralSystem.Isolated,
        },
        BetweenCompartmentsProtectionIndexes =
        {
            ProtectionIndex.IP67,
        },
        ExternalFaceProtectionIndexes =
        {
            ProtectionIndex.IP3X,
        },
        HighVoltagePartsProtectionIndexes =
        {
            ProtectionIndex.IP3X,
        },
    };

    private SwitchGearSeries SM6() => new(SG.SM6.Series, "SM6")
    {
        Description = SwitchGearSeriesDescriptions.SM6_Description,
        // LongDescription = SwitchGearSeriesDescriptions.SM6_LongDescription,
        Image = SwitchGearImages.SM6,
    };
}