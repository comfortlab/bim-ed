﻿using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using SG = CoLa.BimEd.DataAccess.Model.Repositories.Guids.Products.SwitchGears;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public class Rm6SwitchGearUnitInitializer
{
    private readonly SwitchGearFunction _empty;
    private readonly SwitchGearFunction _b;
    private readonly SwitchGearFunction _bc;
    private readonly SwitchGearFunction _d;
    private readonly SwitchGearFunction _i;
    private readonly SwitchGearFunction _ic;
    private readonly SwitchGearFunction _mt;
    private readonly SwitchGearFunction _o;
    private readonly SwitchGearFunction _q;

    private SwitchGearFunction Empty => _empty.Clone();
    private SwitchGearFunction B     => _b.Clone();
    private SwitchGearFunction BC    => _bc.Clone();
    private SwitchGearFunction D     => _d.Clone();
    private SwitchGearFunction I     => _i.Clone();
    private SwitchGearFunction IC    => _ic.Clone();
    private SwitchGearFunction Mt    => _mt.Clone();
    private SwitchGearFunction O     => _o.Clone();
    private SwitchGearFunction Q     => _q.Clone();
    
    public Rm6SwitchGearUnitInitializer(Repository<SwitchGearFunction> switchGearFunctions)
    {
        _empty = switchGearFunctions.Get(SG.RM6.RM6_Empty);
        _b     = switchGearFunctions.Get(SG.RM6.RM6_B);
        _bc    = switchGearFunctions.Get(SG.RM6.RM6_BC);
        _d     = switchGearFunctions.Get(SG.RM6.RM6_D);
        _i     = switchGearFunctions.Get(SG.RM6.RM6_I);
        _ic    = switchGearFunctions.Get(SG.RM6.RM6_IC);
        _mt    = switchGearFunctions.Get(SG.RM6.RM6_Mt);
        _o     = switchGearFunctions.Get(SG.RM6.RM6_O);
        _q     = switchGearFunctions.Get(SG.RM6.RM6_Q);
    }

    public SwitchGearUnit[] Initialize() => new[]
    {
        // 1 complete NE
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-I",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 472, 670),
                Weight = 135,
            },
            switchGearFunctions: I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-B",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 572, 670),
                Weight = 135,
            },
            switchGearFunctions: B),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-D",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 572, 670),
                Weight = 135,
            },
            switchGearFunctions: D),
        
        // 1 complete DE
        
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-I",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 532, 670),
                Weight = 135,
            },
            switchGearFunctions: I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-B",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 632, 670),
                Weight = 135,
            },
            switchGearFunctions: B),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-D",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 632, 670),
                Weight = 135,
            },
            switchGearFunctions: D),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-Q",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 632, 670),
                Weight = 185,
            },
            switchGearFunctions: Q),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-Ic",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 632, 670),
                Weight = 145,
            },
            switchGearFunctions: IC),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-Bc",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 632, 670),
                Weight = 145,
            },
            switchGearFunctions: BC),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-Mt",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1106, 840),
                Weight = 420,
            },
            switchGearFunctions: Mt),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-O",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 532, 670),
                Weight = 135,
            },
            switchGearFunctions: O),
        
        // 1 complete LE
        
        SwitchGearUnit.CreateLE(SG.RM6.Series,
            new Product
            {
                Name = "LE-O",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 502, 670),
                Weight = 135,
            },
            switchGearFunctions: O),
        
        // 1 complete RE
        
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-O",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 502, 670),
                Weight = 135,
            },
            switchGearFunctions: O),
        
        // 2 complete NE
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-II",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 829, 670),
                Weight = 155,
            },
            I, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-BI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 829, 670),
                Weight = 180,
            },
            B, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-DI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 829, 670),
                Weight = 180,
            },
            D, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-QI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 829, 670),
                Weight = 180,
            },
            Q, I),
        
        // 2 complete RE
        
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-II",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 859, 670),
                Weight = 155,
            },
            I, I),
        
        // 3 complete NE
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-III",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1186, 670),
                Weight = 240,
            },
            I, I, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1186, 670),
                Weight = 250,
            },
            I, B, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1186, 670),
                Weight = 240,
            },
            I, D, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1186, 670),
                Weight = 275,
            },
            I, Q, I),
        
        // 3 complete RE
        
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-III",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1216, 670),
                Weight = 240,
            },
            I, I, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1216, 670),
                Weight = 250,
            },
            I, B, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1216, 670),
                Weight = 240,
            },
            I, D, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1216, 670),
                Weight = 275,
            },
            I, Q, I),
        
        // 3 complete DE
        
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-III",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1246, 670),
                Weight = 240,
            },
            I, I, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1246, 670),
                Weight = 250,
            },
            I, B, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1246, 670),
                Weight = 240,
            },
            I, D, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1246, 670),
                Weight = 275,
            },
            I, Q, I),
        
        // 4 complete NE
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IIII",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 320,
            },
            I, I, I, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IIBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 330,
            },
            I, I, B, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-BIBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 340,
            },
            B, I, B, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IIDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 330,
            },
            I, I, D, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-DIDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 340,
            },
            D, I, D, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-IIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 355,
            },
            I, I, Q, I),
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product
            {
                Name = "NE-QIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1619, 670),
                Weight = 390,
            },
            Q, I, Q, I),
        
        // 4 complete RE
        
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IIII",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 320,
            },
            I, I, I, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IIBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 330,
            },
            I, I, B, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-BIBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 340,
            },
            B, I, B, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IIDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 330,
            },
            I, I, D, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-DIDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 340,
            },
            D, I, D, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-IIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 355,
            },
            I, I, Q, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product
            {
                Name = "RE-QIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1649, 670),
                Weight = 390,
            },
            Q, I, Q, I),
        
        // 4 complete DE
        
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IIII",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1679, 670),
                Weight = 320,
            },
            I, I, I, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IIBI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1679, 670),
                Weight = 330,
            },
            I, I, B, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IIDI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1679, 670),
                Weight = 330,
            },
            I, I, D, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1679, 670),
                Weight = 355,
            },
            I, I, Q, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product
            {
                Name = "DE-IIQI",
                Dimensions = Dimensions.CreateFromMillimeters(1142, 1679, 670),
                Weight = 355,
            },
            I, I, Q, I),
        
        // 2 free
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product { Name = "NE-_ _" },
            Empty, Empty),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product { Name = "DE-_ _" },
            Empty, Empty),
        SwitchGearUnit.CreateLE(SG.RM6.Series,
            new Product { Name = "LE-_ _" },
            Empty, Empty),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product { Name = "RE-_ _" },
            Empty, Empty),
        
        // 3 free
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product { Name = "NE-_ _ _" },
            Empty, Empty, Empty),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product { Name = "DE-_ _ _" },
            Empty, Empty, Empty),
        SwitchGearUnit.CreateLE(SG.RM6.Series,
            new Product { Name = "LE-_ _ _" },
            Empty, Empty, Empty),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product { Name = "RE-_ _ _" },
            Empty, Empty, Empty),
        
        // 5 free
        
        SwitchGearUnit.CreateNE(SG.RM6.Series,
            new Product { Name = "NE-I_I_I" },
            I, Empty, I, Empty, I),
        SwitchGearUnit.CreateDE(SG.RM6.Series,
            new Product { Name = "DE-I_I_I" },
            I, Empty, I, Empty, I),
        SwitchGearUnit.CreateLE(SG.RM6.Series,
            new Product { Name = "LE-I_I_I" },
            I, Empty, I, Empty, I),
        SwitchGearUnit.CreateRE(SG.RM6.Series,
            new Product { Name = "RE-I_I_I" },
            I, Empty, I, Empty, I),
    };
}