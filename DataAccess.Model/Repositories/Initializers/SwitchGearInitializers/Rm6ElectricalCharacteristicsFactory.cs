﻿using System.Collections.Generic;
using System.Globalization;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage.Properties;
using SG = CoLa.BimEd.DataAccess.Model.Repositories.Guids.Products.SwitchGears;
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Model.Repositories.Initializers.SwitchGearInitializers;

public class Rm6ElectricalCharacteristicsFactory : ElectricalCharacteristicsFactory
{
    private readonly string _languageName;

    public Rm6ElectricalCharacteristicsFactory()
    {
        _languageName = CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower();
    }
        
    protected override BindingValues<int, SwitchGearVoltage> GetBusbarCurrentVoltageBinding()
    {
        if (SwitchGearFunction.Guid == SG.RM6.RM6_B ||
            SwitchGearFunction.Guid == SG.RM6.RM6_BC ||
            SwitchGearFunction.Guid == SG.RM6.RM6_D ||
            SwitchGearFunction.Guid == SG.RM6.RM6_I ||
            SwitchGearFunction.Guid == SG.RM6.RM6_IC ||
            SwitchGearFunction.Guid == SG.RM6.RM6_Q)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV_ru,
                _    => Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_Mt ||
            SwitchGearFunction.Guid == SG.RM6.RM6_O)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_630_ru,
                _    => Rm6BindingVoltageCurrent_630,
            };

        return new BindingValues<int, SwitchGearVoltage>();
    }

    protected override BindingValues<int, SwitchGearVoltage> GetRatedCurrentVoltageBinding()
    {
        if (SwitchGearFunction.Guid == SG.RM6.RM6_B ||
            SwitchGearFunction.Guid == SG.RM6.RM6_BC ||
            SwitchGearFunction.Guid == SG.RM6.RM6_D ||
            SwitchGearFunction.Guid == SG.RM6.RM6_O)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_200_630_ru,
                _    => Rm6BindingVoltageCurrent_200_630,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_I ||
            SwitchGearFunction.Guid == SG.RM6.RM6_IC)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV_ru,
                _    => Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_Mt)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_630_ru,
                _    => Rm6BindingVoltageCurrent_630,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_Q)
            return _languageName switch
            {
                "ru" => Rm6BindingVoltageCurrent_200_ru,
                _    => Rm6BindingVoltageCurrent_200,
            };

        return new BindingValues<int, SwitchGearVoltage>();
    }

    protected override BindingValues<SwitchGearVoltage, CurrentTimePoint> GetThermalCurrentVoltageBinding()
    {
        if (SwitchGearFunction.Guid == SG.RM6.RM6_B ||
            SwitchGearFunction.Guid == SG.RM6.RM6_D ||
            SwitchGearFunction.Guid == SG.RM6.RM6_I ||
            SwitchGearFunction.Guid == SG.RM6.RM6_IC)
            return _languageName switch
            {
                "ru" => Rm6BindingThermalCurrentVoltage_B_D_I_ru,
                _    => Rm6BindingThermalCurrentVoltage_B_D_I,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_BC)
            return _languageName switch
            {
                "ru" => Rm6BindingThermalCurrentVoltage_Bc_ru,
                _    => Rm6BindingThermalCurrentVoltage_Bc,
            };
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_O)
            return _languageName switch
            {
                "ru" => Rm6BindingThermalCurrentVoltage_O_ru,
                _    => Rm6BindingThermalCurrentVoltage_O,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_Mt)
            return _languageName switch
            {
                "ru" => Rm6BindingThermalCurrentVoltage_M_ru,
                _    => Rm6BindingThermalCurrentVoltage_M,
            };

        if (SwitchGearFunction.Guid == SG.RM6.RM6_Q)
            return _languageName switch
            {
                "ru" => Rm6BindingThermalCurrentVoltage_Q_ru,
                _    => Rm6BindingThermalCurrentVoltage_Q,
            };
        
        return new BindingValues<SwitchGearVoltage, CurrentTimePoint>();
    }

    protected override BindingValues<int, CurrentTimePoint> GetThermalCurrentBusbarCurrentBinding()
    {
        if (SwitchGearFunction.Guid == SG.RM6.RM6_B ||
            SwitchGearFunction.Guid == SG.RM6.RM6_D)
            return Rm6BindingThermalCurrentBusbarCurrent_B_D;
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_I ||
            SwitchGearFunction.Guid == SG.RM6.RM6_IC)
            return Rm6BindingThermalCurrentBusbarCurrent_I_Q;
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_BC)
            return Rm6BindingThermalCurrentBusbarCurrent_Bc;
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_O)
            return Rm6BindingThermalCurrentBusbarCurrent_O;
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_Mt)
            return Rm6BindingThermalCurrentBusbarCurrent_M;
        
        if (SwitchGearFunction.Guid == SG.RM6.RM6_Q)
            return Rm6BindingThermalCurrentBusbarCurrent_I_Q;
        
        return new BindingValues<int, CurrentTimePoint>();
    }

    protected override InsulationType GetArcInsulations() => InsulationType.Gas;

    protected override InsulationType GetCubicleInsulations() => InsulationType.Gas;

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_200 = new BindingValues<int, SwitchGearVoltage>(
            200,
            new[]
            {
                SwitchGearVoltageSet.kV_10_12_28_75,
                SwitchGearVoltageSet.kV_15_17p5_38_95,
                SwitchGearVoltageSet.kV_20_24_50_125,
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_200_ru = new BindingValues<int, SwitchGearVoltage>(
            200,
            new[]
            {
                SwitchGearVoltageSet.kV_06_7p2_32_60,
                SwitchGearVoltageSet.kV_10_12_42_75,
                SwitchGearVoltageSet.kV_20_24_65_125,
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_200_630 = new BindingValues<int, SwitchGearVoltage>(
            new[]
            {
                200,
                630,
            },
            new[]
            {
                SwitchGearVoltageSet.kV_10_12_28_75,
                SwitchGearVoltageSet.kV_15_17p5_38_95,
                SwitchGearVoltageSet.kV_20_24_50_125,
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_200_630_ru = new BindingValues<int, SwitchGearVoltage>(
            new[]
            {
                200,
                630,
            },
            new[]
            {
                SwitchGearVoltageSet.kV_06_7p2_32_60,
                SwitchGearVoltageSet.kV_10_12_42_75,
                SwitchGearVoltageSet.kV_20_24_65_125,
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV = new BindingValues<int, SwitchGearVoltage>(
            new[]
            {
                (400, new List<SwitchGearVoltage>
                {
                    SwitchGearVoltageSet.kV_20_24_50_125,
                }),
                (630, new List<SwitchGearVoltage>
                {
                    SwitchGearVoltageSet.kV_10_12_28_75,
                    SwitchGearVoltageSet.kV_15_17p5_38_95,
                    SwitchGearVoltageSet.kV_20_24_50_125,
                }),
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_400A_24kV_630A_12_24kV_ru = new BindingValues<int, SwitchGearVoltage>(
            new[]
            {
                (400, new List<SwitchGearVoltage>
                {
                    SwitchGearVoltageSet.kV_20_24_65_125,
                }),
                (630, new List<SwitchGearVoltage>
                {
                    SwitchGearVoltageSet.kV_06_7p2_32_60,
                    SwitchGearVoltageSet.kV_10_12_42_75,
                    SwitchGearVoltageSet.kV_20_24_65_125,
                }),
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_630 = new BindingValues<int, SwitchGearVoltage>(
            630,
            new[]
            {
                SwitchGearVoltageSet.kV_10_12_28_75,
                SwitchGearVoltageSet.kV_15_17p5_38_95,
                SwitchGearVoltageSet.kV_20_24_50_125,
            });

    private static readonly BindingValues<int, SwitchGearVoltage>
        Rm6BindingVoltageCurrent_630_ru = new BindingValues<int, SwitchGearVoltage>(
            630,
            new[]
            {
                SwitchGearVoltageSet.kV_06_7p2_32_60,
                SwitchGearVoltageSet.kV_10_12_42_75,
                SwitchGearVoltageSet.kV_20_24_65_125,
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_B_D_I = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_10_12_28_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_15_17p5_38_95, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_50_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_B_D_I_ru = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_06_7p2_32_60, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_10_12_42_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_65_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_Bc = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_10_12_28_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_15_17p5_38_95, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_17_5_s_1,
                    CurrentTimePointSet.kA_17_5_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_50_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_Bc_ru = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_06_7p2_32_60, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_10_12_42_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_17_5_s_1,
                    CurrentTimePointSet.kA_17_5_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_65_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_M = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_10_12_28_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_15_17p5_38_95, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_50_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_16_s_3,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_M_ru = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_06_7p2_32_60, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_10_12_42_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_65_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_16_s_3,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_O = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_10_12_28_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_15_17p5_38_95, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_50_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_O_ru = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_06_7p2_32_60, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_10_12_42_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_65_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_Q = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_10_12_28_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_15_17p5_38_95, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_50_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<SwitchGearVoltage, CurrentTimePoint>
        Rm6BindingThermalCurrentVoltage_Q_ru = new BindingValues<SwitchGearVoltage, CurrentTimePoint>(
            new[]
            {
                (SwitchGearVoltageSet.kV_06_7p2_32_60, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_25_s_1,
                }),
                (SwitchGearVoltageSet.kV_10_12_42_75, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                }),
                (SwitchGearVoltageSet.kV_20_24_65_125, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                }),
            });

    private static readonly BindingValues<int, CurrentTimePoint>
        Rm6BindingThermalCurrentBusbarCurrent_B_D = new BindingValues<int, CurrentTimePoint>(
            new[]
            {
                (400, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                }),
                (630, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                    CurrentTimePointSet.kA_25_s_1,
                }),
            });

    private static readonly BindingValues<int, CurrentTimePoint>
        Rm6BindingThermalCurrentBusbarCurrent_Bc = new BindingValues<int, CurrentTimePoint>(
            new[]
            {
                (400, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                }),
                (630, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                    CurrentTimePointSet.kA_17_5_s_1,
                    CurrentTimePointSet.kA_17_5_s_3,
                    CurrentTimePointSet.kA_25_s_1,
                }),
            });

    private static readonly BindingValues<int, CurrentTimePoint>
        Rm6BindingThermalCurrentBusbarCurrent_I_Q = new BindingValues<int, CurrentTimePoint>(
            new[]
            {
                (400, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_12_5_s_1,
                    CurrentTimePointSet.kA_16_s_1,
                }),
                (630, new List<CurrentTimePoint>
                {
                    CurrentTimePointSet.kA_16_s_1,
                    CurrentTimePointSet.kA_20_s_1,
                    CurrentTimePointSet.kA_20_s_3,
                    CurrentTimePointSet.kA_21_s_1,
                    CurrentTimePointSet.kA_21_s_3,
                    CurrentTimePointSet.kA_25_s_1,
                }),
            });

    private static readonly BindingValues<int, CurrentTimePoint>
        Rm6BindingThermalCurrentBusbarCurrent_M = new BindingValues<int, CurrentTimePoint>(
            630,
            new[]
            {
                CurrentTimePointSet.kA_16_s_1,
                CurrentTimePointSet.kA_16_s_3,
                CurrentTimePointSet.kA_20_s_1,
                CurrentTimePointSet.kA_20_s_3,
                CurrentTimePointSet.kA_21_s_1,
                CurrentTimePointSet.kA_21_s_3,
                CurrentTimePointSet.kA_25_s_1,
            }
        );

    private static readonly BindingValues<int, CurrentTimePoint>
        Rm6BindingThermalCurrentBusbarCurrent_O = new BindingValues<int, CurrentTimePoint>(
            630,
            new[]
            {
                CurrentTimePointSet.kA_16_s_1,
                CurrentTimePointSet.kA_20_s_1,
                CurrentTimePointSet.kA_20_s_3,
                CurrentTimePointSet.kA_21_s_3,
                CurrentTimePointSet.kA_25_s_1,
            }
        );
}