﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Model.Repositories;

public sealed class CableSeriesRepository : ManyFilesRepository<CableSeries, CableSeriesDto>
{
    public CableSeriesRepository(Repository<Material> materials, AppPaths appPaths, IMapper mapper) :
        base(new CableSeriesInitializer(materials), appPaths, mapper) => Load();

    // public override void Insert(CableSeries cableSeries)
    // {
    //     CheckMaterial(cableSeries);
    //     base.Insert(cableSeries);
    // }
    //
    // public override void Update(CableSeries cableSeries)
    // {
    //     CheckMaterial(cableSeries);
    //     base.Update(cableSeries);
    // }
    //
    // public void Restore(Cable cable)
    // {
    //     var cableSeries = GetAll()
    //         .FirstOrDefault(n => n.Id == cable?.CableSeries?.Id);
    //
    //     var prototype = cableSeries?
    //         .FirstOrDefault(n => n.Id == cable?.Id);
    //
    //     if (prototype != null)
    //         cable.PullProperties(prototype);
    //     else
    //         _logger.Warn($"<{GetType()}.{nameof(Restore)}> {nameof(prototype)} is NULL\n\t{nameof(cable)}: {cable}");
    // }
    //
    // private void CheckMaterial(CableSeries cableSeries)
    // {
    //     if (UnitOfWork.GetContext() is not ProductContext context)
    //         return;
    //
    //     var material = context.MaterialSet.Find(cableSeries.ConductorMaterial.Id);
    //
    //     if (material == null)
    //         context.MaterialSet.Add(cableSeries.ConductorMaterial);
    // }
    
    private class CableSeriesInitializer : IRepositoryInitializer<CableSeries>
    {
        private readonly List<Material> _materials;

        public CableSeriesInitializer(List<Material> materials)
        {
            _materials = materials;
        }

        public List<CableSeries> Initialize() => new()
        {
            Initialize_BBГнг_A_LS(),
            Initialize_BBГнг_A_FRLS(),
        };

        private CableSeries Initialize_BBГнг_A_LS()
        {
            var cooper = _materials.FirstOrDefault(n => n.Id == Material.Cu);

            var cableSeries = new CableSeries("ВВГнг(A)-LS", cooper, new CableInsulation { LowSmoke = true, })
            {
                CableProperties = new List<CableProperty>
                {
                    CableProperty.LowSmoke,
                },
            };

            cableSeries.Cables.Add(Cable(cableSeries, cooper, 5.4, 0.05, ConductorType.Solid, new ConductorCount(1, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 5.8, 0.06, ConductorType.Solid, new ConductorCount(1, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 7.0, 0.09, ConductorType.Solid, new ConductorCount(1, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 7.5, 0.11, ConductorType.Solid, new ConductorCount(1, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 8.3, 0.16, ConductorType.Solid, new ConductorCount(1, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.8, 0.24, ConductorType.Stranded, new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.5, 0.23, ConductorType.Solid, new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.3, 0.33, ConductorType.Stranded, new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.1, 0.34, ConductorType.Solid, new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.3, 0.43, ConductorType.Stranded, new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.9, 0.42, ConductorType.Solid, new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.9, 0.59, ConductorType.Stranded, new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.4, 0.56, ConductorType.Solid, new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 15.6, 0.79, ConductorType.Stranded, new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 17.6, 1.04, ConductorType.Stranded, new ConductorCount(1, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 19.4, 1.30, ConductorType.Stranded, new ConductorCount(1, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.3, 1.60, ConductorType.Stranded, new ConductorCount(1, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.4, 1.96, ConductorType.Stranded, new ConductorCount(1, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.4, 2.52, ConductorType.Stranded, new ConductorCount(1, 240)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.3, 0.11, ConductorType.Solid, new ConductorCount(2, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.1, 0.16, ConductorType.Solid, new ConductorCount(2, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.8, 0.24, ConductorType.Solid, new ConductorCount(2, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.8, 0.29, ConductorType.Solid, new ConductorCount(2, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.4, 0.41, ConductorType.Solid, new ConductorCount(2, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.6, 0.69, ConductorType.Stranded, new ConductorCount(2, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.2, 0.56, ConductorType.Solid, new ConductorCount(2, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.0, 1.03, ConductorType.Stranded, new ConductorCount(2, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.1, 0.96, ConductorType.Solid, new ConductorCount(2, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.2, 1.31, ConductorType.Stranded, new ConductorCount(2, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.0, 1.21, ConductorType.Solid, new ConductorCount(2, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.4, 1.72, ConductorType.Stranded, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.2, 1.22, ConductorType.SectionStranded, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.4, 1.66, ConductorType.Solid, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.4, 1.72, ConductorType.Solid, new ConductorCount(2, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.4, 1.62, ConductorType.SectionStranded, new ConductorCount(2, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.4, 2.17, ConductorType.SectionStranded, new ConductorCount(2, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.9, 2.80, ConductorType.SectionStranded, new ConductorCount(2, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 30.5, 3.41, ConductorType.SectionStranded, new ConductorCount(2, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.8, 0.15, ConductorType.Solid, new ConductorCount(3, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.6, 0.20, ConductorType.Solid, new ConductorCount(3, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.4, 0.28, ConductorType.Solid, new ConductorCount(3, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.5, 0.36, ConductorType.Solid, new ConductorCount(3, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 15.2, 0.51, ConductorType.Solid, new ConductorCount(3, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.4, 0.74, ConductorType.Stranded, new ConductorCount(3, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 17.2, 0.71, ConductorType.Solid, new ConductorCount(3, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.2, 1.26, ConductorType.Stranded, new ConductorCount(3, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.0, 1.51, ConductorType.Stranded, new ConductorCount(3, 25), new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.3, 1.24, ConductorType.Solid, new ConductorCount(3, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.6, 1.63, ConductorType.Stranded, new ConductorCount(3, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.5, 1.86, ConductorType.Stranded, new ConductorCount(3, 35), new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.8, 1.56, ConductorType.Solid, new ConductorCount(3, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 29.0, 2.22, ConductorType.Stranded, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.2, 2.46, ConductorType.Stranded, new ConductorCount(3, 50), new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.5, 1.80, ConductorType.SectionStranded, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.4, 2.11, ConductorType.Solid, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.9, 2.39, ConductorType.SectionStranded, new ConductorCount(3, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 29.1, 2.37, ConductorType.SectionStranded, new ConductorCount(3, 70), new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 33.0, 3.38, ConductorType.SectionStranded, new ConductorCount(3, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.5, 4.00, ConductorType.SectionStranded, new ConductorCount(3, 95), new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.0, 4.06, ConductorType.SectionStranded, new ConductorCount(3, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 39.0, 4.97, ConductorType.SectionStranded, new ConductorCount(3, 120), new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 40.3, 5.00, ConductorType.SectionStranded, new ConductorCount(3, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 43.8, 5.95, ConductorType.SectionStranded, new ConductorCount(3, 150), new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 44.6, 6.30, ConductorType.SectionStranded, new ConductorCount(3, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 48.1, 7.38, ConductorType.SectionStranded, new ConductorCount(3, 185), new ConductorCount(1, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.5, 0.18, ConductorType.Solid, new ConductorCount(4, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.4, 0.23, ConductorType.Solid, new ConductorCount(4, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.5, 0.36, ConductorType.Solid, new ConductorCount(4, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.7, 0.44, ConductorType.Solid, new ConductorCount(4, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.7, 0.64, ConductorType.Solid, new ConductorCount(4, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 19.8, 0.94, ConductorType.Stranded, new ConductorCount(4, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.7, 0.90, ConductorType.Solid, new ConductorCount(4, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.4, 1.55, ConductorType.Stranded, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.9, 1.43, ConductorType.SectionStranded, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.8, 1.53, ConductorType.Solid, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.0, 2.03, ConductorType.Stranded, new ConductorCount(4, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.0, 1.94, ConductorType.Solid, new ConductorCount(4, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 35.8, 2.84, ConductorType.Stranded, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.3, 2.42, ConductorType.SectionStranded, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.1, 2.54, ConductorType.Solid, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 32.2, 3.25, ConductorType.SectionStranded, new ConductorCount(4, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.8, 4.32, ConductorType.SectionStranded, new ConductorCount(4, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 40.2, 5.45, ConductorType.SectionStranded, new ConductorCount(4, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 45.0, 6.68, ConductorType.SectionStranded, new ConductorCount(4, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 49.8, 8.06, ConductorType.SectionStranded, new ConductorCount(4, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.4, 0.20, ConductorType.Solid, new ConductorCount(5, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.4, 0.27, ConductorType.Solid, new ConductorCount(5, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.7, 0.40, ConductorType.Solid, new ConductorCount(5, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.1, 0.52, ConductorType.Solid, new ConductorCount(5, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.3, 0.77, ConductorType.Solid, new ConductorCount(5, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.8, 1.13, ConductorType.Stranded, new ConductorCount(5, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.7, 1.09, ConductorType.Solid, new ConductorCount(5, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.0, 1.90, ConductorType.Stranded, new ConductorCount(5, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.0, 1.85, ConductorType.Solid, new ConductorCount(5, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.1, 2.48, ConductorType.Stranded, new ConductorCount(5, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 30.0, 2.45, ConductorType.Solid, new ConductorCount(5, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 35.8, 3.43, ConductorType.Stranded, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 33.5, 3.12, ConductorType.SectionStranded, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 34.5, 3.28, ConductorType.Solid, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 37.9, 4.00, ConductorType.SectionStranded, new ConductorCount(5, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 44.1, 5.35, ConductorType.SectionStranded, new ConductorCount(5, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 48.5, 6.71, ConductorType.SectionStranded, new ConductorCount(5, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 54.5, 8.23, ConductorType.SectionStranded, new ConductorCount(5, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 59.8, 10.20, ConductorType.SectionStranded, new ConductorCount(5, 185)));

            return cableSeries;
        }

        private CableSeries Initialize_BBГнг_A_FRLS()
        {
            var cooper = _materials.FirstOrDefault(n => n.Id == Material.Cu);

            var cableSeries = new CableSeries("ВВГнг(A)-FRLS", cooper, new CableInsulation { FireResistant = true, LowSmoke = true, })
            {
                CableProperties = new List<CableProperty>()
                {
                    CableProperty.FireResistivity,
                    CableProperty.LowSmoke,
                }
            };

            cableSeries.Cables.Add(Cable(cableSeries, cooper, 5.4, 0.05, ConductorType.Solid, new ConductorCount(1, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 5.8, 0.06, ConductorType.Solid, new ConductorCount(1, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 7.0, 0.09, ConductorType.Solid, new ConductorCount(1, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 7.5, 0.11, ConductorType.Solid, new ConductorCount(1, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 8.3, 0.16, ConductorType.Solid, new ConductorCount(1, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.8, 0.24, ConductorType.Stranded, new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.5, 0.23, ConductorType.Solid, new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.3, 0.33, ConductorType.Stranded, new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.1, 0.34, ConductorType.Solid, new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.3, 0.43, ConductorType.Stranded, new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.9, 0.42, ConductorType.Solid, new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.9, 0.59, ConductorType.Stranded, new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.4, 0.56, ConductorType.Solid, new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 15.6, 0.79, ConductorType.Stranded, new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 17.6, 1.04, ConductorType.Stranded, new ConductorCount(1, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 19.4, 1.30, ConductorType.Stranded, new ConductorCount(1, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.3, 1.60, ConductorType.Stranded, new ConductorCount(1, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.4, 1.96, ConductorType.Stranded, new ConductorCount(1, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.4, 2.52, ConductorType.Stranded, new ConductorCount(1, 240)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.3, 0.11, ConductorType.Solid, new ConductorCount(2, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.1, 0.16, ConductorType.Solid, new ConductorCount(2, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.8, 0.24, ConductorType.Solid, new ConductorCount(2, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.8, 0.29, ConductorType.Solid, new ConductorCount(2, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.4, 0.41, ConductorType.Solid, new ConductorCount(2, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.6, 0.69, ConductorType.Stranded, new ConductorCount(2, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.2, 0.56, ConductorType.Solid, new ConductorCount(2, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.0, 1.03, ConductorType.Stranded, new ConductorCount(2, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.1, 0.96, ConductorType.Solid, new ConductorCount(2, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.2, 1.31, ConductorType.Stranded, new ConductorCount(2, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.0, 1.21, ConductorType.Solid, new ConductorCount(2, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.4, 1.72, ConductorType.Stranded, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.2, 1.22, ConductorType.SectionStranded, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.4, 1.66, ConductorType.Solid, new ConductorCount(2, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.4, 1.72, ConductorType.Solid, new ConductorCount(2, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.4, 1.62, ConductorType.SectionStranded, new ConductorCount(2, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.4, 2.17, ConductorType.SectionStranded, new ConductorCount(2, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.9, 2.80, ConductorType.SectionStranded, new ConductorCount(2, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 30.5, 3.41, ConductorType.SectionStranded, new ConductorCount(2, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 9.8, 0.15, ConductorType.Solid, new ConductorCount(3, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.6, 0.20, ConductorType.Solid, new ConductorCount(3, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.4, 0.28, ConductorType.Solid, new ConductorCount(3, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.5, 0.36, ConductorType.Solid, new ConductorCount(3, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 15.2, 0.51, ConductorType.Solid, new ConductorCount(3, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.4, 0.74, ConductorType.Stranded, new ConductorCount(3, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 17.2, 0.71, ConductorType.Solid, new ConductorCount(3, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 23.2, 1.26, ConductorType.Stranded, new ConductorCount(3, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.0, 1.51, ConductorType.Stranded, new ConductorCount(3, 25), new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 22.3, 1.24, ConductorType.Solid, new ConductorCount(3, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.6, 1.63, ConductorType.Stranded, new ConductorCount(3, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.5, 1.86, ConductorType.Stranded, new ConductorCount(3, 35), new ConductorCount(1, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.8, 1.56, ConductorType.Solid, new ConductorCount(3, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 29.0, 2.22, ConductorType.Stranded, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.2, 2.46, ConductorType.Stranded, new ConductorCount(3, 50), new ConductorCount(1, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.5, 1.80, ConductorType.SectionStranded, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.4, 2.11, ConductorType.Solid, new ConductorCount(3, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 26.9, 2.39, ConductorType.SectionStranded, new ConductorCount(3, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 29.1, 2.37, ConductorType.SectionStranded, new ConductorCount(3, 70), new ConductorCount(1, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 33.0, 3.38, ConductorType.SectionStranded, new ConductorCount(3, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.5, 4.00, ConductorType.SectionStranded, new ConductorCount(3, 95), new ConductorCount(1, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.0, 4.06, ConductorType.SectionStranded, new ConductorCount(3, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 39.0, 4.97, ConductorType.SectionStranded, new ConductorCount(3, 120), new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 40.3, 5.00, ConductorType.SectionStranded, new ConductorCount(3, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 43.8, 5.95, ConductorType.SectionStranded, new ConductorCount(3, 150), new ConductorCount(1, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 44.6, 6.30, ConductorType.SectionStranded, new ConductorCount(3, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 48.1, 7.38, ConductorType.SectionStranded, new ConductorCount(3, 185), new ConductorCount(1, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 10.5, 0.18, ConductorType.Solid, new ConductorCount(4, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 11.4, 0.23, ConductorType.Solid, new ConductorCount(4, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 13.5, 0.36, ConductorType.Solid, new ConductorCount(4, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.7, 0.44, ConductorType.Solid, new ConductorCount(4, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.7, 0.64, ConductorType.Solid, new ConductorCount(4, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 19.8, 0.94, ConductorType.Stranded, new ConductorCount(4, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.7, 0.90, ConductorType.Solid, new ConductorCount(4, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 25.4, 1.55, ConductorType.Stranded, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.9, 1.43, ConductorType.SectionStranded, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 24.8, 1.53, ConductorType.Solid, new ConductorCount(4, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.0, 2.03, ConductorType.Stranded, new ConductorCount(4, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.0, 1.94, ConductorType.Solid, new ConductorCount(4, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 35.8, 2.84, ConductorType.Stranded, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.3, 2.42, ConductorType.SectionStranded, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.1, 2.54, ConductorType.Solid, new ConductorCount(4, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 32.2, 3.25, ConductorType.SectionStranded, new ConductorCount(4, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 36.8, 4.32, ConductorType.SectionStranded, new ConductorCount(4, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 40.2, 5.45, ConductorType.SectionStranded, new ConductorCount(4, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 45.0, 6.68, ConductorType.SectionStranded, new ConductorCount(4, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 49.8, 8.06, ConductorType.SectionStranded, new ConductorCount(4, 185)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.4, 0.20, ConductorType.Solid, new ConductorCount(5, 1.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 12.4, 0.27, ConductorType.Solid, new ConductorCount(5, 2.5)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 14.7, 0.40, ConductorType.Solid, new ConductorCount(5, 4)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 16.1, 0.52, ConductorType.Solid, new ConductorCount(5, 6)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 18.3, 0.77, ConductorType.Solid, new ConductorCount(5, 10)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 21.8, 1.13, ConductorType.Stranded, new ConductorCount(5, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 20.7, 1.09, ConductorType.Solid, new ConductorCount(5, 16)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 28.0, 1.90, ConductorType.Stranded, new ConductorCount(5, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 27.0, 1.85, ConductorType.Solid, new ConductorCount(5, 25)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 31.1, 2.48, ConductorType.Stranded, new ConductorCount(5, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 30.0, 2.45, ConductorType.Solid, new ConductorCount(5, 35)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 35.8, 3.43, ConductorType.Stranded, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 33.5, 3.12, ConductorType.SectionStranded, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 34.5, 3.28, ConductorType.Solid, new ConductorCount(5, 50)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 37.9, 4.00, ConductorType.SectionStranded, new ConductorCount(5, 70)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 44.1, 5.35, ConductorType.SectionStranded, new ConductorCount(5, 95)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 48.5, 6.71, ConductorType.SectionStranded, new ConductorCount(5, 120)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 54.5, 8.23, ConductorType.SectionStranded, new ConductorCount(5, 150)));
            cableSeries.Cables.Add(Cable(cableSeries, cooper, 59.8, 10.20, ConductorType.SectionStranded, new ConductorCount(5, 185)));

            return cableSeries;
        }

        private CableSeries Initialize_BBГнг_A_LSLTx()
        {
            var cooper = _materials.FirstOrDefault(n => n.Id == Material.Cu);

            var cableSeries = new CableSeries("ВВГнг(A)-LSLTx", cooper, new CableInsulation
            {
                LowSmoke = true,
                LowToxic = true,
            });

            return cableSeries;
        }

        private static Cable Cable(
            CableSeries cableSeries,
            Material conductorMaterial,
            double diameter, double weight,
            ConductorType conductorType,
            params ConductorCount[] conductors)
        {
            var conductorTypeString = GetConductorType(conductorType);
            var cable = new Cable(cableSeries, conductors)
            {
                Id = $"{cableSeries.Id} {string.Join("+", conductors.Select(n => $"{n.Count}x{n.Section.Round(significantDigits:4).ToString(CultureInfo.InvariantCulture)}{conductorTypeString}"))}",
                ConductorType = conductorType,
                Diameter = diameter,
                WeightPerMeter = weight,
            };

            var cores = conductors.Sum(n => n.Count);
            var section = conductors.First().Section.Round(4);
            var k = cores > 3 || cores == 3 && section > 10 ? 1.12 : 1;
            var l = 0.1 * (2 * Math.Log(2 * k * diameter * 0.75 / Math.Sqrt(4 * section / Math.PI)) + 0.5) * 1e-6;
            var r1 = conductorMaterial.Resistivity / (section * 1e-6);
            var r0 = r1 * (1 + 3 * Math.Pow(0.0006 / r1, 2) / (1 + Math.Pow(0.0006 / r1, 2)));
            var x1 = 2 * Math.PI * 50 * l;
            var x0 = 3 * (x1 + 0.0006 / (1 + Math.Pow(0.0006 / r1, 2)));

            cable.R0 = r0;
            cable.R1 = r1;
            cable.X0 = x0;
            cable.X1 = x1;

            cable.PermissibleCurrent = GetCurrent(conductorMaterial, cores, section, inGround: false);
            cable.GroundPermissibleCurrent = GetCurrent(conductorMaterial, cores, section, inGround: true);

            return cable;
        }

        private static string GetConductorType(ConductorType conductorType) =>
            conductorType switch
            {
                ConductorType.Solid => "ок",
                ConductorType.Stranded => "мк",
                ConductorType.SectionStranded => "мс",
                _ => throw new ArgumentOutOfRangeException(nameof(conductorType), conductorType, null)
            };

        private static int GetCurrent(Material material, int count, double section, bool inGround)
        {
            return material.Id switch
            {
                Material.Cu => count switch
                {
                    1 => CurrentsPueCooperAirOneCores.TryGetValue(section, out var current) ? current : 0,
                    2 or 3 when inGround => CurrentsPueCooperGroundTwoCores.TryGetValue(section, out var current) ? current : 0,
                    2 or 3 => CurrentsPueCooperAirTwoCores.TryGetValue(section, out var current) ? current : 0,
                    4 or 5 when inGround => CurrentsPueCooperGroundThreeCores.TryGetValue(section, out var current) ? current : 0,
                    4 or 5 => CurrentsPueCooperAirThreeCores.TryGetValue(section, out var current) ? current : 0,
                    _ => 0,
                },
                _ => 0,
            };
        }

        private static readonly IDictionary<double, int> CurrentsPueCooperAirOneCores = new Dictionary<double, int>
        {
            [1.5] = 23,
            [2.5] = 30,
            [4] = 41,
            [6] = 50,
            [10] = 80,
            [16] = 100,
            [25] = 140,
            [35] = 170,
            [50] = 215,
            [70] = 270,
            [95] = 325,
            [120] = 385,
            [150] = 440,
            [185] = 510,
            [240] = 605,
        };

        private static readonly IDictionary<double, int> CurrentsPueCooperAirTwoCores = new Dictionary<double, int>
        {
            [1.5] = 19,
            [2.5] = 27,
            [4] = 38,
            [6] = 50,
            [10] = 70,
            [16] = 90,
            [25] = 115,
            [35] = 140,
            [50] = 175,
            [70] = 215,
            [95] = 260,
            [120] = 300,
            [150] = 350,
            [185] = 405,
            // [240] = 500,
        };

        private static readonly IDictionary<double, int> CurrentsPueCooperGroundTwoCores = new Dictionary<double, int>
        {
            [1.5] = 33,
            [2.5] = 44,
            [4] = 55,
            [6] = 70,
            [10] = 105,
            [16] = 135,
            [25] = 175,
            [35] = 210,
            [50] = 265,
            [70] = 320,
            [95] = 385,
            [120] = 445,
            [150] = 505,
            [185] = 570,
            // [240] = 650,
        };

        private static readonly IDictionary<double, int> CurrentsPueCooperAirThreeCores = new Dictionary<double, int>
        {
            [1.5] = 19,
            [2.5] = 25,
            [4] = 35,
            [6] = 42,
            [10] = 55,
            [16] = 75,
            [25] = 95,
            [35] = 120,
            [50] = 145,
            [70] = 180,
            [95] = 220,
            [120] = 260,
            [150] = 305,
            [185] = 350,
            // [240] = 450,
        };

        private static readonly IDictionary<double, int> CurrentsPueCooperGroundThreeCores = new Dictionary<double, int>
        {
            [1.5] = 27,
            [2.5] = 38,
            [4] = 49,
            [6] = 60,
            [10] = 90,
            [16] = 115,
            [25] = 150,
            [35] = 180,
            [50] = 225,
            [70] = 275,
            [95] = 330,
            [120] = 385,
            [150] = 435,
            [185] = 500,
            // [240] = 650,
        };
    }
}