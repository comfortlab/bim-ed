using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using Newtonsoft.Json;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record SelectAndConfigSettingDto
{
    [JsonProperty("ESM")]
    public EquipmentSelectionMode EquipmentSelectionMode { get; set; }
    
    [JsonProperty("BNC")]
    public bool BreakNeutralCore { get; set; }
    
    [JsonProperty("NC")]
    public bool NeutralCore { get; set; }
    
    [JsonProperty("CSI")]
    public string CableSeriesId { get; set; }
    
    [JsonProperty("MCS")]
    public double MinCoreSection { get; set; }
    
    [JsonProperty("SS")]
    public List<SelectorProductSettingDto> SwitchSettings { get; set; }
    
    [JsonProperty("CS")]
    public List<SelectorProductSettingDto> ContactorSettings { get; set; }
    
    [JsonProperty("EMS")]
    public List<SelectorProductSettingDto> EnergyMeterSettings { get; set; }
    
    [JsonProperty("OPS")]
    public List<SelectorProductSettingDto> OtherProductSettings { get; set; }
}