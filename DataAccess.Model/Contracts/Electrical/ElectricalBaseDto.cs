namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record ElectricalBaseDto
{
    public ConnectorDto BaseConnector { get; set; }
}