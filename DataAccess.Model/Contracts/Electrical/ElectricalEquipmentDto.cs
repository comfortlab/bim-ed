﻿using System;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using Newtonsoft.Json;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record ElectricalEquipmentDto
{
    [JsonProperty("G")]
    public Guid Guid { get; set; }
    
    [JsonProperty("Id")]
    public int RevitId { get; set; }
    
    [JsonProperty("N")]
    public string Name { get; set; }
    public bool ShouldSerializeName() => !string.IsNullOrWhiteSpace(Name);
    
    [JsonProperty("DSI")]
    public int DistributionSystemId { get; set; }
    public bool ShouldSerializeDistributionSystemId() => DistributionSystemId > 0;
    
    [JsonProperty("ST")]
    public ElectricalSystemTypeProxy SystemType { get; set; }
}