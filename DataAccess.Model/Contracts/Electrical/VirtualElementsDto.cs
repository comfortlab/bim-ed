﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

// ReSharper disable CollectionNeverUpdated.Global

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record VirtualElementsDto
{
    public int Id { get; set; }
    
    [JsonProperty("G")]
    public List<ElectricalEquipmentDto> Generators { get; set; }
    public bool ShouldSerializeGenerators() => Generators?.Any() ?? false;
    
    [JsonProperty("N")]
    public List<ElectricalEquipmentDto> Networks { get; set; }
    public bool ShouldSerializeNetworks() => Networks?.Any() ?? false;
    
    [JsonProperty("SU")]
    public List<ElectricalEquipmentDto> SwitchboardUnits { get; set; }
    public bool ShouldSerializeSwitchboardUnits() => SwitchboardUnits?.Any() ?? false;
    
    [JsonProperty("T")]
    public List<TransformerDto> Transformers { get; set; }
    public bool ShouldSerializeTransformers() => Transformers?.Any() ?? false;
}