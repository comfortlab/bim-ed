using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using Newtonsoft.Json;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record TransformerDto : ElectricalEquipmentDto
{
    [JsonProperty("SDSI")]
    public int SecondaryDistributionSystemId { get; set; }
    public bool ShouldSerializeSecondaryDistributionSystemId() => SecondaryDistributionSystemId > 0;
    
    [JsonProperty("TP")]
    public TransformerProductDto TransformerProduct { get; set; }
    public bool ShouldSerializeTransformerProduct() => TransformerProduct != default;
    
    [JsonProperty("NIR")]
    public double NetworkInductiveReactance { get; set; }
    public bool ShouldSerializeNetworkInductiveReactance() => NetworkInductiveReactance > 0;
    
    [JsonProperty("PSCP")]
    public double PrimaryShortCircuitPower { get; set; }
    public bool ShouldSerializePrimaryShortCircuitPower() => NetworkInductiveReactance > 0;
    
    [JsonProperty("PBC")]
    public double PrimaryBreakingCapacity { get; set; }
    public bool ShouldSerializePrimaryBreakingCapacity() => NetworkInductiveReactance > 0;
}

public record TransformerProductDto
{
    [JsonProperty("TSG")]
    public Guid TransformerSeriesGuid { get; set; }
    public bool ShouldSerializeTransformerSeriesGuid() => TransformerSeriesGuid != Guid.Empty;

    [JsonProperty("RP")]
    public double RatedPower { get; set; }
    public bool ShouldSerializeRatedPower() => RatedPower > 0;
    
    [JsonProperty("PV")]
    public double PrimaryVoltage { get; set; }
    public bool ShouldSerializePrimaryVoltage() => PrimaryVoltage > 0;
    
    [JsonProperty("SV")]
    public double SecondaryVoltage { get; set; }
    public bool ShouldSerializeSecondaryVoltage() => SecondaryVoltage > 0;
    
    [JsonProperty("PI")]
    public ProtectionIndex ProtectionIndex { get; set; }
    public bool ShouldSerializeProtectionIndex() => ProtectionIndex > 0;
    
    [JsonProperty("CM")]
    public CoolingMode CoolingMode { get; set; }
    public bool ShouldSerializeCoolingMode() => CoolingMode > 0;
    
    [JsonProperty("VG")]
    public VectorGroup VectorGroup { get; set; }
    public bool ShouldSerializeVectorGroup() => VectorGroup > 0;
    
    [JsonProperty("P")]
    public List<TransformerProperty> Properties { get; set; }
    public bool ShouldSerializeProperties() => Properties?.Any() ?? false;
}