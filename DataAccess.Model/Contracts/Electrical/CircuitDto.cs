﻿namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record CircuitDto
{
    public CircuitProductsDto ElectricalProducts { get; set; }
}