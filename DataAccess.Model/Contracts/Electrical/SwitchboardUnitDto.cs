﻿namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record SwitchboardUnitDto
{
    public SwitchboardUnitProductsDto ElectricalProducts { get; set; }
}