﻿// using Newtonsoft.Json;
//
// namespace CoLa.BimEd.BusinessLogic.Model.Contracts.Electrical;
//
// public class SpareCircuitDto : ElementDto
// {
//     /// <summary>
//     /// Констуктор по умолчанию не удалять. Необходим для десериализации.
//     /// </summary>
//     public SpareCircuitDto()
//     {
//         // AdditionalDevices = new ObservableCollection<DeviceInstance>();
//         // AuxiliaryDevices = new ObservableCollection<DeviceInstance>();
//     }
//
//     // [JsonProperty("CB")]
//     // public SwitchDisconnectorInstance CircuitBreakerInstance { get; set; }
//     // public bool ShouldSerializeCircuitBreakerInstance() => CircuitBreakerInstance != null;
//
//     // [JsonProperty("AdD")]
//     // public ObservableCollection<DeviceInstance> AdditionalDevices { get; set; }
//     // public bool ShouldSerializeAdditionalDevices() => AdditionalDevices?.Any() ?? false;
//     //
//     // [JsonProperty("AuD")]
//     // public ObservableCollection<DeviceInstance> AuxiliaryDevices { get; set; }
//     // public bool ShouldSerializeAuxiliaryDevices() => AuxiliaryDevices?.Any() ?? false;
//
//     [JsonProperty("D")]
//     public string CircuitDesignation { get; set; }
//     public bool ShouldSerializeCircuitDesignation() => string.IsNullOrEmpty(CircuitDesignation) == false;
//
//     [JsonProperty("CBD")]
//     public string CircuitBreakerDesignation { get; set; }
//     public bool ShouldSerializeCircuitBreakerDesignation() => string.IsNullOrEmpty(CircuitBreakerDesignation) == false;
//
//     [JsonProperty("Ph")]
//     public Phase Phase { get; set; }
//     public bool ShouldSerializePhase() => Phase != Phase.Undefined;
//
//     [JsonProperty("B")]
//     public bool IsBalancedLoad { get; set; }
//     public bool ShouldSerializeIsBalancedLoad() => IsBalancedLoad;
// }