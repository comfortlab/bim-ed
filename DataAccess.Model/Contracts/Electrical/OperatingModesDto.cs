﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

// ReSharper disable CollectionNeverUpdated.Global

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record OperatingModesDto
{
    [JsonProperty("C")]
    public Guid CurrentGuid { get; set; }
    
    [JsonProperty("I")]
    public OperatingModeDto[] Items { get; set; }
}

public record OperatingModeDto
{
    [JsonProperty("G")]
    public Guid Guid { get; set; }
    
    [JsonProperty("N")]
    public string Name { get; set; }
    
    [JsonProperty("DE")]
    public int[] DisabledElements { get; set; }
    public bool ShouldSerializeDisabledElements() => DisabledElements?.Any() ?? false;

    // public Dictionary<ElementProxyDto, bool> ElementSwitches { get; set; }
}

public record ConnectorDto
{
    public Dictionary<Guid, ConnectorOperatingModeDto> ConnectorOperatingModes { get; set; }
}
    
public record ConnectorOperatingModeDto
{
    public bool State { get; set; }
    public double Current { get; set; }
    public double EstimateCurrent1 { get; set; }
    public double EstimateCurrent2 { get; set; }
    public double EstimateCurrent3 { get; set; }
    public double ShortCurrent1 { get; set; }
    public double ShortCurrent3 { get; set; }
    public double SurgeShortCurrent3 { get; set; }
    public double TrueLoad { get; set; }
    public double EstimateTrueLoad { get; set; }
    public double ApparentLoad { get; set; }
    public double EstimateApparentLoad { get; set; }
    public double PowerFactor { get; set; }
    public double VoltageDrop { get; set; }
    public double DemandFactor { get; set; }
    public double AdditionalDemandFactor { get; set; }
    public double TotalDemandFactor { get; set; }
}