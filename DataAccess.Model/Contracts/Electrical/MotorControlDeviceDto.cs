using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record MotorControlDeviceDto
{
    public ProductDto Product { get; set; }
    public List<ProductDto> Accessories { get; set; }
}