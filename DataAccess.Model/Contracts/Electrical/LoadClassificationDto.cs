﻿using Newtonsoft.Json;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record LoadClassificationDto
{
    [JsonProperty("CS")]
    public SelectAndConfigSettingDto CircuitSelectAndConfigSetting { get; set; }
    
    [JsonProperty("PS")]
    public SelectAndConfigSettingDto PanelSelectAndConfigSetting { get; set; }
    
    [JsonProperty("ST")]
    public string ServiceType { get; set; }
}