using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;

namespace CoLa.BimEd.DataAccess.Model.Contracts.Electrical;

public record ElectricalProductsDto
{
    public string MainSwitchDesignation { get; set; }
    public string MainSwitchSelectorId { get; set; }
    public SelectorProductDto MainSwitch { get; set; }
    public List<SelectorProductDto> Products { get; set; }
}

public record SwitchboardUnitProductsDto : ElectricalProductsDto
{
    public string EnergyMeterDesignation { get; set; }
    public SelectorProductDto EnergyMeter { get; set; }
}

public record CircuitProductsDto : ElectricalProductsDto
{
    public SelectorProductDto MainSwitch { get; set; }
    public string CableId { get; set; }
    public string CableSeriesId { get; set; }
    public int CablesCount { get; set; }
    public int EstimateCablesCount { get; set; }
}