﻿using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.Cables.ViewModels;

namespace CoLa.BimEd.UI.Cables.Views;

public partial class CablesView
{
    public CablesView()
    {
        InitializeComponent();
        DataContext = DI.Get<CablesViewModel>();
    }
}