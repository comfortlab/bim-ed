﻿using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.Cables.Views;

public partial class CablesWindow
{
    public CablesWindow()
    {
        InitializeComponent();
        this.SetPositionAndSizes(heightScaleFactor: 0.9, widthScaleFactor: 0.8);
    }
}