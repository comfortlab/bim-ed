﻿using System.Collections.ObjectModel;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.UI.Framework.MVVM;

namespace CoLa.BimEd.UI.Cables.ViewModels;

public class CablesViewModel : ViewModel
{
    public CablesViewModel()
    {
        
    }
    
    public CablesViewModel(Repository<CableSeries> cableSeriesRepository)
    {
        CableSeries = cableSeriesRepository.Select(x => new CableSeriesViewModel(x)).ToObservableCollection();
    }

    public ObservableCollection<CableSeriesViewModel> CableSeries { get; set; }
    public ObservableCollection<CableViewModel> Cables { get; set; }
    public CableSeriesViewModel SelectedCableSeries { get; set; }
    public CableViewModel SelectedCable { get; set; }
    public string CableName { get; set; }
    
    private void OnSelectedCableSeriesChanged()
    {
        Cables = SelectedCableSeries?.CableSeries.Cables.Select(x => new CableViewModel(x)).ToObservableCollection();
    }
    
    private void OnSelectedCableChanged()
    {
        CableName = SelectedCable?.Cable.Id;
    }
}

public class CableSeriesViewModel : ViewModel
{
    public CableSeriesViewModel(CableSeries cableSeries)
    {
        CableSeries = cableSeries;
        Name = CableSeries.Id;
    }

    public CableSeries CableSeries { get; set; }
    public string Name { get; set; }
}

public class CableViewModel : ViewModel
{
    public CableViewModel(Cable cable)
    {
        Cable = cable;
    }

    public Cable Cable { get; set; }
    public string Id { get; set; }
    public string Description { get; set; }
    public string Diameter { get; set; }

    private void OnCableChanged()
    {
        Id = Cable.Id;
        Description = Cable.Description;
        Diameter = Cable.Diameter.ToStringInvariant();
    }
}