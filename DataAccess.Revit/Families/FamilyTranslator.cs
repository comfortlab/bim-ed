﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.DataAccess.Revit.Resources;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.Revit.Families;

namespace CoLa.BimEd.DataAccess.Revit.Families;

public class FamilyTranslator
{
    public static Family TranslateFamily(Family family)
    {
        if (family == null)
            return null;

        var dictionary = ResourceManagerUtils.GetDictionaryValues(ParametersAnnotation.ResourceManager);

        return TranslateFamilyParameters(family, dictionary);
    }

    public static Family TranslateFamilyParameters(Family family, IDictionary<string, string> dictionary)
    {
        if (family == null)
            return null;

        var doc = family.Document;
        var familyDocument = doc.EditFamily(family);

        using var transaction = new Transaction(familyDocument, TransactionNames.EditFamily);
        
        transaction.Start();

        var isFamilyEdited = TranslateParameters(familyDocument, dictionary) |
                             TranslateTypes(familyDocument, dictionary);

        if (isFamilyEdited)
        {
            transaction.Commit();
        }
        else
        {
            transaction.RollBack();
        }

        var translatedFamily = familyDocument.LoadFamily(doc, new FamilyLoadOptions());
        familyDocument.Close(saveModified: false);

        return translatedFamily;
    }

    private static bool TranslateParameters(Document familyDoc, IDictionary<string, string> dictionary)
    {
        var isFamilyEdited = false;

        foreach (FamilyParameter familyParameter in familyDoc.FamilyManager.Parameters)
        {
            var name = familyParameter.Definition.Name;

            if (familyParameter.IsShared ||
                !dictionary.ContainsKey(name))
                continue;

            var newName = dictionary[name];

            familyDoc.FamilyManager.RenameParameter(familyParameter, newName);
            isFamilyEdited = true;
        }

        return isFamilyEdited;
    }

    private static bool TranslateTypes(Document familyDoc, IDictionary<string, string> dictionary)
    {
        var isFamilyEdited = false;

        foreach (FamilyType familyType in familyDoc.FamilyManager.Types)
        {
            var name = familyType.Name;

            if (!dictionary.ContainsKey(name))
                continue;

            var newName = dictionary[name];

            familyDoc.FamilyManager.CurrentType = familyType;
            familyDoc.FamilyManager.RenameCurrentType(newName);
            isFamilyEdited = true;
        }

        return isFamilyEdited;
    }
}