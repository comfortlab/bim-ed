﻿using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Geometry;

namespace CoLa.BimEd.DataAccess.Revit.Converters;

public class SegmentConverter
{
    public static SegmentProxy CreateSegment(Curve curve)
    {
        var point0 = PointConverter.CreatePoint(curve.GetEndPoint(0));
        var point1 = PointConverter.CreatePoint(curve.GetEndPoint(1));
        return new SegmentProxy(point0, point1);
    }

    public static SegmentProxy CreateSegment(Line line)
    {
        var point0 = PointConverter.CreatePoint(line.GetEndPoint(0));
        var point1 = PointConverter.CreatePoint(line.GetEndPoint(1));
        return new SegmentProxy(point0, point1);
    }

    public static Line CreateLine(SegmentProxy segment)
    {
        var xyz0 = PointConverter.CreateXYZ(segment.Point1);
        var xyz1 = PointConverter.CreateXYZ(segment.Point2);

        return Line.CreateBound(xyz0, xyz1);
    }
}