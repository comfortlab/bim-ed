﻿using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Geometry;

namespace CoLa.BimEd.DataAccess.Revit.Converters;

public class PointConverter
{
    public static XYZProxy CreatePoint(XYZ xyz) => new XYZProxy(xyz.X, xyz.Y, xyz.Z);

    public static XYZ CreateXYZ(XYZProxy point) => new XYZ(point.X, point.Y, point.Z);
}