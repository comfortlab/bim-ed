﻿using System;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class ElectricalSystemSync : RevitCollectionSync<ElectricalSystemProxy, ElectricalSystem>
{
    public ElectricalSystemSync(IServiceProvider serviceProvider) : base(
        serviceProvider,
        mappingContext: SyncUtils.ElectricalSystemMappingContext(serviceProvider)) { }
    
    protected override ElectricalSystem CreateRevitElement(ElectricalSystemProxy electricalSystem) => null;
}