﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class DistributionSystemSync : RevitCollectionSync<DistributionSystemProxy, DistributionSysType>
{
    private readonly ElectricalSetting _electricalSetting;
    private readonly VoltageTypeSync _voltageTypeSync;
    
    public DistributionSystemSync(
        IServiceProvider serviceProvider,
        ElectricalSetting electricalSetting) : base(serviceProvider)
    {
        _electricalSetting = electricalSetting;
        _voltageTypeSync = new VoltageTypeSync(serviceProvider, electricalSetting);
    }

    protected override DistributionSysType CreateRevitElement(DistributionSystemProxy proxy) =>
        _electricalSetting.AddDistributionSysType(proxy.Name, ElectricalPhase.SinglePhase, ElectricalPhaseConfiguration.Undefined, 3, null, null);

    public override Repository<DistributionSystemProxy> LoadFromRevit()
    {
        var voltageTypeProxies = _voltageTypeSync.LoadFromRevit();
        var distributionSysTypes = ServiceProvider.Get<List<DistributionSysType>>().ToArray();
        Repository.ReplaceAll(Mapper.Map<List<DistributionSystemProxy>>(distributionSysTypes, opt => opt.Items.Add("item", voltageTypeProxies)));
        return Repository;
    }

    public override void ApplyToRevit()
    {
        var voltageTypes = ServiceProvider.Get<List<VoltageType>>();
        var distributionSysTypes = ServiceProvider.Get<List<DistributionSysType>>();

        if (!ChangedElements.IsAnyChanged)
            return;

        _voltageTypeSync.ChangedElements.PullProperties(ChangedElements);
        
        if (ChangedElements.IsAnyAdded)
        {
            Document.ExecuteTransaction(() =>
            {
                _voltageTypeSync.Add(voltageTypes);
            }, TransactionNames.CreateVoltageType);
        }
        
        if (!ChangedElements.IsAnyChanged)
            return;
        
        Document.ExecuteTransaction(() =>
        {
            Add(distributionSysTypes, contextItem: voltageTypes);
            UpdateOrDelete(distributionSysTypes, contextItem: voltageTypes);
            _voltageTypeSync.UpdateOrDelete(voltageTypes);
        }, TransactionNames.Update);
    }
}