﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

/// <summary>
/// Представляет логику синхронизации коллекций элеметов Revit и коллекций элементов доменной модели приложения.
/// </summary>
/// <typeparam name="TElementProxy">Тип элемента доменной модели приложения</typeparam>
/// <typeparam name="TRevitElement">Тип элемента Revit</typeparam>
public abstract class RevitCollectionSync<TElementProxy, TRevitElement> : IRevitCollectionSync<TElementProxy>
    where TElementProxy : ElementProxy
    where TRevitElement : Element
{
    private readonly BuiltInCategory _filterCategory;
    private readonly ElementFilter _elementFilter;
    private readonly Func<TRevitElement, bool> _extraFilter;
    private readonly Func<Dictionary<string, object>> _mappingContext;
    protected readonly IServiceProvider ServiceProvider;
    protected readonly Document Document;
    protected readonly IMapper Mapper;
    
    protected RevitCollectionSync(
        IServiceProvider serviceProvider,
        BuiltInCategory filterCategory = BuiltInCategory.INVALID,
        ElementFilter elementFilter = null,
        Func<TRevitElement, bool> extraFilter = null,
        Func<Dictionary<string, object>> mappingContext = null)
    {
        ServiceProvider = serviceProvider;
        _filterCategory = filterCategory;
        _elementFilter = elementFilter;
        _extraFilter = extraFilter;
        _mappingContext = mappingContext;
        Document = ServiceProvider.Get<Document>();
        Mapper = ServiceProvider.Get<IMapper>();
    }

    protected abstract TRevitElement CreateRevitElement(TElementProxy elementProxy);
    public RevitRepository<TElementProxy> Repository { get; set; }
    public ChangedElements ChangedElements { get; set; } = new();

    public virtual Repository<TElementProxy> LoadFromRevit()
    {
        Repository ??= (RevitRepository<TElementProxy>)ServiceProvider.Get<Repository<TElementProxy>>();
        
        var revitElements = (_filterCategory switch
        {
            BuiltInCategory.INVALID when _elementFilter != null => ServiceProvider.Get<Func<ElementFilter, List<TRevitElement>>>().Invoke(_elementFilter),
            BuiltInCategory.INVALID => ServiceProvider.Get<List<TRevitElement>>(),
            _ when _elementFilter != null => ServiceProvider.Get<Func<BuiltInCategory, ElementFilter, List<TRevitElement>>>()?.Invoke(_filterCategory, _elementFilter),
            _ => ServiceProvider.Get<Func<BuiltInCategory, List<TRevitElement>>>()?.Invoke(_filterCategory),
        })?.Where(x => _extraFilter == null || _extraFilter(x)).ToArray() ?? Array.Empty<TRevitElement>();

        var newItems = _mappingContext != null
            ? Mapper.Map<IEnumerable<TElementProxy>>(revitElements, opt => opt.Items.AddRange(_mappingContext?.Invoke()))
            : Mapper.Map<IEnumerable<TElementProxy>>(revitElements);
        
        Repository.ReplaceAll(newItems);

        return Repository;
    }

    public virtual void ApplyToRevit()
    {
        Repository ??= (RevitRepository<TElementProxy>)ServiceProvider.Get<Repository<TElementProxy>>();

        var revitElements = ServiceProvider.Get<List<TRevitElement>>() ?? throw new NullReferenceException($"Service of 'List<{typeof(TRevitElement).FullName}>' was not found.");

        if (!ChangedElements.IsAnyChanged)
            return;
        
        Document.ExecuteTransaction(() =>
        {
            Add(revitElements);
            UpdateOrDelete(revitElements);
        }, TransactionNames.Update);
        
        ChangedElements.ResetAll();
    }

    public void ApplyToRevit(TElementProxy entity)
    {
        throw new NotImplementedException($"RevitCollectionSync is used to synchronize collections. Use {nameof(ApplyToRevit)} without arguments.");
    }

    protected internal void Add(
        List<TRevitElement> revitElements,
        object contextItem = null)
    {
        if (!ChangedElements.IsAnyAdded)
            return;
        
        foreach (var proxy in Repository.Where(x => ChangedElements.IsAdded(x.RevitId) && !ChangedElements.IsDeleted(x.RevitId)))
        {
            var revitElement = revitElements.FirstOrDefault(x => x.Id.IntegerValue == proxy.RevitId);

            if (revitElement != null)
                continue;

            revitElement = CreateRevitElement(proxy);

            if (revitElement == null)
            {
                Debug.WriteLine($"Revit Element {nameof(TRevitElement)} was not created: {GetType()}.{nameof(Add)}");
                continue;
            }

            revitElements.Add(revitElement);
            proxy.RevitId = revitElement.Id.IntegerValue;
            
            if (contextItem != null)
                Mapper.Map(proxy, revitElement, opt => opt.Items.Add("item", contextItem));
            else
                Mapper.Map(proxy, revitElement);
        }
    }

    protected internal void UpdateOrDelete(
        List<TRevitElement> revitElements,
        object contextItem = null)
    {
        if (!ChangedElements.IsAnyDeleted && !ChangedElements.IsAnyChanged)
            return;

        var deletedElements = new List<TRevitElement>();
        
        foreach (var revitElement in revitElements)
        {
            var revitId = revitElement.Id.IntegerValue;
            
            if (ChangedElements.IsModified(revitId))
            {
                var proxy = Repository.FirstOrDefault(x => x.RevitId == revitElement.Id.IntegerValue);

                if (proxy == null)
                    continue;
                
                if (contextItem != null)
                    Mapper.Map(proxy, revitElement, opt => opt.Items.Add("item", contextItem));
                else
                    Mapper.Map(proxy, revitElement);
            }
            else if (ChangedElements.IsDeleted(revitId))
            {
                deletedElements.Add(revitElement);
                Document.Delete(revitElement.Id);
            }
        }

        foreach (var revitElement in deletedElements)
            revitElements.Remove(revitElement);
    }
}