using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Storage;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

/// <summary>
/// Представляет логику синхронизации элемента хранилища Revit <b>DataStorage</b> и элемента доменной модели приложения.
/// </summary>
/// <typeparam name="T">Тип элемента доменной модели приложения</typeparam>
public abstract class RevitStorageSync<T> : IRevitStorageSync<T>
    where T : class
{
    private readonly IServiceProvider _serviceProvider;
    private readonly StorageBase _storage;
    private readonly Document _document;

    protected RevitStorageSync(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        _storage = _serviceProvider.Get<DocumentStorage<T>>();
        _document = _serviceProvider.Get<Document>();
    }

    public RevitRepository<T> Repository { get; set; }
    public virtual ChangedElements ChangedElements { get; } = new();
    public virtual T LoadFromRevit() => _storage.GetAs<T>();

    public virtual void ApplyToRevit()
    {
        try
        {
            if (!ChangedElements.IsAnyChanged)
                return;
        
            _document.ExecuteTransaction(() =>
            {
                var value = _serviceProvider.Get<T>() ?? throw new NullReferenceException($"Service of '{typeof(T).FullName}' was not found.");
                _storage.SetAsJson(value);
            }, TransactionNames.DataStorage);
        }
        finally
        {
            ChangedElements.ResetAll();
        }
    }

    public virtual void ApplyToRevit(T entity)
    {
        try
        {
            if (!ChangedElements.IsAnyChanged)
                return;
        
            _document.ExecuteTransaction(() => _storage.SetAsJson(entity), TransactionNames.DataStorage);
        }
        finally
        {
            ChangedElements.ResetAll();
        }
    }
}