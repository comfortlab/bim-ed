using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class OperatingModeSync : RevitStorageSync<OperatingModes>
{
    private readonly IMapper _mapper;
    private readonly OperatingModeDtoSync _dtoSync;
    
    public OperatingModeSync(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _mapper = serviceProvider.Get<IMapper>();
        _dtoSync = new OperatingModeDtoSync(serviceProvider);
    }

    public override ChangedElements ChangedElements => _dtoSync.ChangedElements; 
    public override OperatingModes LoadFromRevit()
    {
        var dto = _dtoSync.LoadFromRevit();
        return dto != null ? _mapper.Map<OperatingModes>(dto) : null;
    }
    public override void ApplyToRevit() => _dtoSync.ApplyToRevit();
    
    private class OperatingModeDtoSync : RevitStorageSync<OperatingModesDto>
    {
        public OperatingModeDtoSync(IServiceProvider serviceProvider) : base(serviceProvider) { }
    }
}