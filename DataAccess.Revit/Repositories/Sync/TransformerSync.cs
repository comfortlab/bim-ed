﻿using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.DataAccess.Revit.Filters;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class TransformerSync : RevitCollectionSync<Transformer, FamilyInstance>
{
    public TransformerSync(IServiceProvider serviceProvider) : base(
        serviceProvider,
        BuiltInCategory.OST_ElectricalEquipment,
        extraFilter: ElectricalElementFilters.TransformerFilter,
        mappingContext: SyncUtils.ElectricalEquipmentMappingContext(serviceProvider)) { }

    protected override FamilyInstance CreateRevitElement(Transformer transformer) => null;
}