﻿using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.DataAccess.Revit.Filters;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class SwitchGearUnitSync : RevitCollectionSync<SwitchGearUnit, FamilyInstance>
{
    public SwitchGearUnitSync(IServiceProvider serviceProvider) : base(
        serviceProvider,
        BuiltInCategory.OST_ElectricalEquipment,
        extraFilter: ElectricalElementFilters.MediumVoltageSwitchGearFilter,
        mappingContext: SyncUtils.ElectricalEquipmentMappingContext(serviceProvider)) { }

    protected override FamilyInstance CreateRevitElement(SwitchGearUnit switchBoardUnit) => null;
}