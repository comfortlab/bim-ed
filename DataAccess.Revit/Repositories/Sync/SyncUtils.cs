﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public static class SyncUtils
{
    public static Func<Dictionary<string, object>> ElectricalEquipmentMappingContext(IServiceProvider serviceProvider) => () => new Dictionary<string, object>
    {
        ["0"] = serviceProvider.Get<Repository<DistributionSystemProxy>>(),
        ["1"] = serviceProvider.Get<Repository<LoadClassificationProxy>>(),
    };
    
    public static Func<Dictionary<string, object>> ElectricalSystemMappingContext(IServiceProvider serviceProvider) => () => new Dictionary<string, object>
    {
        ["0"] = serviceProvider.Get<ElectricalEquipmentProxy[]>(),
    };
}