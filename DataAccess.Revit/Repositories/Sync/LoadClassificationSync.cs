﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Resources;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class LoadClassificationSync : RevitCollectionSync<LoadClassificationProxy, ElectricalLoadClassification>
{
    private readonly DemandFactorSync _demandFactorSync;
    
    public LoadClassificationSync(
        IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _demandFactorSync = new DemandFactorSync(serviceProvider);
    }

    protected override ElectricalLoadClassification CreateRevitElement(LoadClassificationProxy proxy) =>
        ElectricalLoadClassification.Create(Document, proxy.Name);

    public override Repository<LoadClassificationProxy> LoadFromRevit()
    {
        var demandFactorProxies = _demandFactorSync.LoadFromRevit();
        var loadClassifications = ServiceProvider.Get<List<ElectricalLoadClassification>>().ToArray();
        Repository.ReplaceAll(Mapper.Map<IEnumerable<LoadClassificationProxy>>(loadClassifications, opt => opt.Items.Add("item", demandFactorProxies)));
        return Repository;
    }

    public override void ApplyToRevit()
    {
        var demandFactors = ServiceProvider.Get<List<ElectricalDemandFactorDefinition>>();
        var loadClassifications = ServiceProvider.Get<List<ElectricalLoadClassification>>();

        if (!ChangedElements.IsAnyChanged)
            return;

        _demandFactorSync.ChangedElements.PullProperties(ChangedElements);
        
        if (ChangedElements.IsAnyAdded)
        {
            Document.ExecuteTransaction(() =>
            {
                _demandFactorSync.Add(demandFactors);
            }, TransactionNames.CreateDemandFactor);
        }
        
        if (!ChangedElements.IsAnyChanged)
            return;
        
        Document.ExecuteTransaction(() =>
        {
            Add(loadClassifications, contextItem: demandFactors);
            UpdateOrDelete(loadClassifications, contextItem: demandFactors);
            _demandFactorSync.UpdateOrDelete(demandFactors);
        }, TransactionNames.Update);
    }
}