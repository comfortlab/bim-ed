using System;
using CoLa.BimEd.BusinessLogic.Model.Settings;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class ElectricalSettingSync : RevitStorageSync<ElectricalSettings>
{
    public ElectricalSettingSync(IServiceProvider serviceProvider) : base(serviceProvider) { }
}