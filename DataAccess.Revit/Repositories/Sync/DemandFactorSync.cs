using System;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class DemandFactorSync : RevitCollectionSync<DemandFactorProxy, ElectricalDemandFactorDefinition>
{
    public DemandFactorSync(IServiceProvider serviceProvider) : base(serviceProvider) { }
    protected override ElectricalDemandFactorDefinition CreateRevitElement(DemandFactorProxy proxy) =>
        ElectricalDemandFactorDefinition.Create(Document, proxy.Name);
}