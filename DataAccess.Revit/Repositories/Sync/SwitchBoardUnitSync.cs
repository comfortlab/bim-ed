﻿using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Revit.Filters;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class SwitchBoardUnitSync : RevitCollectionSync<SwitchBoardUnit, FamilyInstance>
{
    public SwitchBoardUnitSync(IServiceProvider serviceProvider) : base(
        serviceProvider,
        BuiltInCategory.OST_ElectricalEquipment,
        extraFilter: ElectricalElementFilters.LowVoltageSwitchBoardFilter,
        mappingContext: SyncUtils.ElectricalEquipmentMappingContext(serviceProvider)) { }

    protected override FamilyInstance CreateRevitElement(SwitchBoardUnit switchBoardUnit) => null;
}