﻿using System;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class VoltageTypeSync : RevitCollectionSync<VoltageTypeProxy, VoltageType>
{
    private readonly ElectricalSetting _electricalSetting;
    public VoltageTypeSync(IServiceProvider serviceProvider, ElectricalSetting electricalSetting) :
        base(serviceProvider) => _electricalSetting = electricalSetting;
    protected override VoltageType CreateRevitElement(VoltageTypeProxy proxy) =>
        _electricalSetting.AddVoltageType(proxy.Name, 0, 0, 0);
}