﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync.Base;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Sync;

public class VirtualElementsSync : RevitStorageSync<VirtualElements>
{
    private readonly IMapper _mapper;
    private readonly VirtualElementsDtoSync _dtoSync;
    
    public VirtualElementsSync(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _mapper = serviceProvider.Get<IMapper>();
        _dtoSync = new VirtualElementsDtoSync(serviceProvider);
    }

    public override ChangedElements ChangedElements => _dtoSync.ChangedElements; 
    public override VirtualElements LoadFromRevit() => _mapper.Map<VirtualElements>(_dtoSync.LoadFromRevit());
    public override void ApplyToRevit() => _dtoSync.ApplyToRevit();
    
    private class VirtualElementsDtoSync : RevitStorageSync<VirtualElementsDto>
    {
        public VirtualElementsDtoSync(IServiceProvider serviceProvider) : base(serviceProvider) { }
    }
}