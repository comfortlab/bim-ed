﻿using System;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class DistributionSystemRepository : RevitCollectionRepository<DistributionSystemProxy>
{
    public DistributionSystemRepository(
        IServiceProvider serviceProvider, ElectricalSetting electricalSetting) :
        base(new DistributionSystemSync(serviceProvider, electricalSetting)) { }
}