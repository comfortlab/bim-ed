﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Settings;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class ElectricalSettingRepository : RevitStorageRepository<ElectricalSettings>
{
    public ElectricalSettingRepository(IServiceProvider serviceProvider) :
        base(new ElectricalSettingSync(serviceProvider)) { }
}