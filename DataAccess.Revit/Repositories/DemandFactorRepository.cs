﻿using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class DemandFactorRepository : RevitCollectionRepository<DemandFactorProxy> { }