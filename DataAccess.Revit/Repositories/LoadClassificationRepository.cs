﻿using System;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class LoadClassificationRepository : RevitCollectionRepository<LoadClassificationProxy>
{
    public LoadClassificationRepository(IServiceProvider serviceProvider) :
        base(new LoadClassificationSync(serviceProvider)) { }
}