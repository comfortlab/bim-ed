﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class SwitchBoardUnitRepository : RevitCollectionRepository<SwitchBoardUnit>
{
    public SwitchBoardUnitRepository(IServiceProvider serviceProvider) :
        base(new SwitchBoardUnitSync(serviceProvider)) { }
}