﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class TransformerRepository : RevitCollectionRepository<Transformer>
{
    public TransformerRepository(IServiceProvider serviceProvider) :
        base(new TransformerSync(serviceProvider)) { }
}