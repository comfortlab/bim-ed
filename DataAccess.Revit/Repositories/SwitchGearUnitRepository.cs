﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class SwitchGearUnitRepository : RevitCollectionRepository<SwitchGearUnit>
{
    public SwitchGearUnitRepository(IServiceProvider serviceProvider) :
        base(new SwitchGearUnitSync(serviceProvider)) { }
}