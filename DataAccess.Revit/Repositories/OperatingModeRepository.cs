﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class OperatingModeRepository : RevitStorageRepository<OperatingModes>
{
    public OperatingModeRepository(IServiceProvider serviceProvider) :
        base(new OperatingModeSync(serviceProvider), createDefault: OperatingModes.CreateDefault) { }
}