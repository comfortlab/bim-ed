﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.Infrastructure.Framework.Repository;

namespace CoLa.BimEd.DataAccess.Revit.Repositories;

public class ElectricalSystemRepository : RevitCollectionRepository<ElectricalSystemProxy>
{
    public ElectricalSystemRepository(IServiceProvider serviceProvider) :
        base(new ElectricalSystemSync(serviceProvider)) { }
}