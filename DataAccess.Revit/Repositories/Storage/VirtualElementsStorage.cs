﻿using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Storage;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Storage;

public class VirtualElementsStorage : DocumentStorage<VirtualElementsDto>
{
    public VirtualElementsStorage(Document document) : base(document,
        new Guid("37DE7035-5E68-4CA6-9AD7-9AF136130884"),
        "BIM_ED_VirtualElements") { }
}