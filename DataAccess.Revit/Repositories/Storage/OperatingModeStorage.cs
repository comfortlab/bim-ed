using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Storage;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Storage;

public class OperatingModeStorage : DocumentStorage<OperatingModesDto>
{
    public OperatingModeStorage(Document document) : base(document,
        new Guid("06ADD6EA-A732-41BE-8486-B913EF2486B0"),
        "BIM_ED_OperatingModes") { }
}