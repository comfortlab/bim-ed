using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Settings;
using CoLa.BimEd.Infrastructure.Revit.Storage;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Storage;

public class ElectricalSettingStorage : DocumentStorage<ElectricalSettings>
{
    public ElectricalSettingStorage(Document document) : base(document,
        new Guid("4A9A2D93-E143-483E-BD0A-FECFD2AD6A5B"),
        "BIM_ED_ElectricalSettings") { }
}