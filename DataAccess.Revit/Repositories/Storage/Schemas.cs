﻿using System;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Storage;

public abstract class DataSchema
{
    protected DataSchema(Guid guid) => Guid = guid;
    public Guid Guid { get; set; }
    public string Data { get; set; }
}

public class DocumentSettingSchema : DataSchema
{
    public DocumentSettingSchema() : base(new Guid("1F75DB6F-6374-4834-995C-AE9D5BF43E5A")) { }
}

public class CircuitSchema : DataSchema
{
    public CircuitSchema() : base(new Guid("6E85621F-B472-4FAF-B577-ABA951579535")) { }
}

public class CircuitProductsSchema : DataSchema
{
    public CircuitProductsSchema() : base(new Guid("{DF9AABF3-E764-44B5-9B12-60DAC53E11AF}")) { }
}

public class ElectricalBaseSchema : DataSchema
{
    public ElectricalBaseSchema() : base(new Guid("00474631-58D9-4E66-985F-4B39B3780A41")) { }
}

public class ElectricalPanelProductsSchema : DataSchema
{
    public ElectricalPanelProductsSchema() : base(new Guid("{D7CFF8D8-D9F9-45A7-A19C-A0D26473E701}")) { }
}

public class ElectricalSourceSchema : DataSchema
{
    public ElectricalSourceSchema() : base(new Guid("8F3409A4-4BA7-4197-BDC3-D80FBF0CA923")) { }
}

public class ElectricalSettingSchema : DataSchema
{
    public ElectricalSettingSchema() : base(new Guid("9D96B0A8-E1C1-40A4-BA9C-96C823A84539")) { }
}

public class FunctionBlockSchema : DataSchema
{
    public FunctionBlockSchema() : base(new Guid("E5E25C9E-13C2-4972-A69A-595C3DD8BC55")) { }
}

public class LoadClassificationSchema : DataSchema
{
    public LoadClassificationSchema() : base(new Guid("{0777D86D-DC83-4387-9740-43FA22E349DB}")) { }
}

public class MotorControlDeviceSchema : DataSchema
{
    public MotorControlDeviceSchema() : base(new Guid("B946A56C-6D73-4040-9D4F-CCBBEC7FABB6")) { }
}

public class OperatingModeSchema : DataSchema
{
    public OperatingModeSchema() : base(new Guid("{43CCB62C-FF27-49C8-87F1-90DD9132FBFE}")) { }
}

public class ReportSchema : DataSchema
{
    public ReportSchema() : base(new Guid("D4225669-5588-4FDD-98F7-DB2CFB78C9AD")) {}
}

public class QuestionnaireSchema : DataSchema
{
    public QuestionnaireSchema() : base(new Guid("C4B2B5E8-3996-44DD-BB97-E3888C06AE6F")) { }
}

public class SubstationSchema : DataSchema
{
    public SubstationSchema() : base(new Guid("A0009A51-E1BE-484B-8DB1-F52C5A6288F4")) { }
}

public class SwitchBoardGroupSchema : DataSchema
{
    public SwitchBoardGroupSchema() : base(new Guid("FCEDEB5F-5774-41D8-A3D4-B6451E66156C")) { }
}

public class TransformerSchema : DataSchema
{
    public TransformerSchema() : base(new Guid("77089B6A-B0A8-42D6-8CDD-D15BB94F4807")) { }
}

public class ViewSettingSchema : DataSchema
{
    public ViewSettingSchema() : base(new Guid("F02F5120-17D1-43B7-A364-002506CBF075")) { }
}