using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Revit.Storage;

namespace CoLa.BimEd.DataAccess.Revit.Repositories.Storage;

public class ElementStorage : ElementStorageBase
{
    public ElementStorage(Element element) : base(element,
        new Guid("7FD929D9-4DA9-4747-A16D-D79D522A58DC"),
        "BIM_ED_Storage") { }
}