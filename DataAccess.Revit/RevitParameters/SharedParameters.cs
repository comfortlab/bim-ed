﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using CoLa.BimEd.Infrastructure.Revit.Parameters.Shared;

// ReSharper disable MemberHidesStaticFromOuterClass
// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.DataAccess.Revit.RevitParameters;

public static class SharedParameters
{
    public static readonly BuiltInCategory[] CableTrayCategories =
    {
        BuiltInCategory.OST_CableTray,
        BuiltInCategory.OST_CableTrayFitting,
    };

    public static readonly BuiltInCategory[] CableTrayConduitCategories =
    {
        BuiltInCategory.OST_CableTray,
        BuiltInCategory.OST_CableTrayFitting,
        BuiltInCategory.OST_Conduit,
        BuiltInCategory.OST_ConduitFitting,
    };

    public static readonly BuiltInCategory[] ElectricalCommonCategories =
    {
        BuiltInCategory.OST_ElectricalCircuit,
        BuiltInCategory.OST_ElectricalEquipment,
    };

    public static readonly BuiltInCategory[] ElectricalDeviceCategories =
    {
        BuiltInCategory.OST_ElectricalEquipment,
        BuiltInCategory.OST_ElectricalFixtures,
        BuiltInCategory.OST_CommunicationDevices,
        BuiltInCategory.OST_DataDevices,
        BuiltInCategory.OST_FireAlarmDevices,
        BuiltInCategory.OST_GenericModel,
        BuiltInCategory.OST_LightingDevices,
        BuiltInCategory.OST_LightingFixtures,
        BuiltInCategory.OST_MechanicalEquipment,
        BuiltInCategory.OST_NurseCallDevices,
        BuiltInCategory.OST_SecurityDevices,
        BuiltInCategory.OST_TelephoneDevices,
    };

    public static class Adsk
    {
        public static List<ProjectSharedParameter> ProjectSharedParameters => new()
        {
            new ProjectSharedParameter(Common.ADSK_Designation, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_Name, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_Mark, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_PartNr, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_Manufacturer, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_Unit, ElectricalDeviceCategories),
            new ProjectSharedParameter(Common.ADSK_Mass_Text, ElectricalDeviceCategories),
        };

        public static class Common
        {
            public class Guids
            {
                public static Guid ADSK_Count => new("8D057BB3-6CCD-4655-9165-55526691FE3A");
                public static Guid ADSK_Designation => new("9C98831B-9450-412D-B072-7D69B39F4029");
                public static Guid ADSK_Family_Version => new("85CD0032-C9EE-4CD3-8FFA-B2F1A05328E3");
                public static Guid ADSK_Fire_Resistance_Limit => new("4D902DAD-86E1-4713-841A-A196798DBDF9");
                public static Guid ADSK_Floor => new("9EABF56C-A6CD-4B5C-A9D0-E9223E19EA3F");
                public static Guid ADSK_Manufacturer => new("A8CDBF7B-D60A-485E-A520-447D2055F351");
                public static Guid ADSK_Mark => new("2204049C-D557-4DFC-8D70-13F19715E46D");
                public static Guid ADSK_Mass => new("32989501-0D17-4916-8777-DA950841C6D7");
                public static Guid ADSK_Mass_Text => new("A8832DF7-0302-4A63-A6E1-47A01632B987");
                public static Guid ADSK_Material_Designation => new("DBE7F282-3606-44CF-AC51-0F274C34C07B");
                public static Guid ADSK_Material_Name => new("87CE1509-068E-400F-AFAB-75DF889463C7");
                public static Guid ADSK_Name => new("E6E0F5CD-3E26-485B-9342-23882B20EB43");
                public static Guid ADSK_Name_Short => new("F194BF60-B880-4217-B793-1E0C30DDA5E9");
                public static Guid ADSK_Note => new("A85B7661-26B0-412F-979C-66AF80B4B2C3");
                public static Guid ADSK_PartNr => new("2FD9E8CB-84F3-4297-B8B8-75F444E124ED");
                public static Guid ADSK_Position => new("AE8FF999-1F22-4ED7-AD33-61503D85F0F4");
                public static Guid ADSK_Unit => new("4289CB19-9517-45DE-9C02-5A74EBF5C86D");
                public static Guid ADSK_URL_Device_Page => new("C5F25D97-91AD-48C8-96E2-99EAC6FBB3F1");
                public static Guid ADSK_URL_Manual => new("BDDC75F4-8E27-4AFC-ADD0-DE7358BBF6D3");
                public static Guid ADSK_Zone => new("C78F0A7D-B68B-4D21-A247-1C8C6CED8BC5");

            }

            public static SharedParameterDefinition ADSK_Count => new(
                Guids.ADSK_Count,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Designation => new(
                Guids.ADSK_Designation,
                builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
                isInstance: true);

            public static SharedParameterDefinition ADSK_Family_Version => new(
                Guids.ADSK_Family_Version,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Fire_Resistance_Limit => new(
                Guids.ADSK_Fire_Resistance_Limit,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Floor => new(
                Guids.ADSK_Floor,
                builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
                isInstance: true);

            public static SharedParameterDefinition ADSK_Manufacturer => new(
                Guids.ADSK_Manufacturer,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Mark => new(
                Guids.ADSK_Mark,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Mass => new(
                Guids.ADSK_Mass,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Mass_Text => new(
                Guids.ADSK_Mass_Text,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Material_Designation => new(
                Guids.ADSK_Material_Designation,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Material_Name => new(
                Guids.ADSK_Material_Name,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Name => new(
                Guids.ADSK_Name,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Name_Short => new(
                Guids.ADSK_Name_Short,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Note => new(
                Guids.ADSK_Note,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA,
                isInstance: true);

            public static SharedParameterDefinition ADSK_PartNr => new(
                Guids.ADSK_PartNr,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Position => new(
                Guids.ADSK_Position,
                builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
                isInstance: true);

            public static SharedParameterDefinition ADSK_Unit => new(
                Guids.ADSK_Unit,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_URL_Device_Page => new(
                Guids.ADSK_URL_Device_Page,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_URL_Manual => new(
                Guids.ADSK_URL_Manual,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);

            public static SharedParameterDefinition ADSK_Zone => new(
                Guids.ADSK_Zone,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA,
                isInstance: true);
        }

        public static class MetalStructures
        {
            public static class Guids
            {
                public static Guid ADSK_Element_Mass => new("5913A1F9-0B38-4364-96FE-A6F3CB7FCC68");
            }
                
            public static SharedParameterDefinition ADSK_Element_Mass => new(
                Guids.ADSK_Element_Mass,
                builtInParameterGroup: BuiltInParameterGroup.PG_DATA);
        }
            
        public static class Engineering
        {
            public class Guids
            {
                public static Guid ADSK_Electrical_NumberOfPhases => new("D182B385-9E45-4E8B-B8DA-725396848493");
                public static Guid ADSK_Electrical_LoadClassification => new("A3413727-1213-401F-8FF2-880522A42B91");
                public static Guid ADSK_Electrical_PowerFactor => new("E3C1A4B0-78C8-49F5-B3C7-01869252C30E");
                public static Guid ADSK_Electrical_PowerRated => new("B4D13AAD-0763-4481-B015-63137342D077");
                public static Guid ADSK_Electrical_PowerApparent => new("7BAE0E4A-A125-4818-973A-00FD56BF853D");
                public static Guid ADSK_Electrical_Voltage => new("BE29221E-5B74-4A61-A253-4EB5F3B532D9");
            }

            public static SharedParameterDefinition ADSK_Electrical_NumberOfPhases => new(
                Guids.ADSK_Electrical_NumberOfPhases,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS
            );

            public static SharedParameterDefinition ADSK_Electrical_LoadClassification => new(
                Guids.ADSK_Electrical_LoadClassification,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
                isInstance: true
            );

            public static SharedParameterDefinition ADSK_Electrical_PowerFactor => new(
                Guids.ADSK_Electrical_PowerFactor,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
                isInstance: true
            );

            public static SharedParameterDefinition ADSK_Electrical_PowerRated => new(
                Guids.ADSK_Electrical_PowerRated,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
                isInstance: true
            );

            public static SharedParameterDefinition ADSK_Electrical_PowerApparent => new(
                Guids.ADSK_Electrical_PowerApparent,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
                isInstance: true
            );

            public static SharedParameterDefinition ADSK_Electrical_Voltage => new(
                Guids.ADSK_Electrical_Voltage,
                builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
                isInstance: true
            );
        }

        public static class Size
        {
            public static class Guids
            {
                public static Guid ADSK_Size_Angle => new("A7397D18-200B-4659-B34C-3D8AE1C54317");
                public static Guid ADSK_Size_Base_Thickness => new("398EA174-E0EF-4619-8DB9-0B2C598D5206");
                public static Guid ADSK_Size_Depth => new("14E630A8-BC4F-4556-9094-647E8F323F08");
                public static Guid ADSK_Size_Diameter => new("9B679AB7-EA2E-49CE-90AB-0549D5AA36FF");
                public static Guid ADSK_Size_Element_Side_Thickness => new("DE129AA9-DAE8-430D-8DE8-F7F08388FCAA");
                public static Guid ADSK_Size_Height => new("DA753FE3-ECFA-465B-9A2C-02F55D0C2FF1");
                public static Guid ADSK_Size_Offset => new("515DC061-93CE-40E4-859A-E29224D80A10");
                public static Guid ADSK_Size_Item_Diameter => new("D6888DC7-7A03-40C3-9AC2-AB300C4E2C0A");
                public static Guid ADSK_Size_Length => new("748A2515-4CC9-4B74-9A69-339A8D65A212");
                public static Guid ADSK_Size_Radius => new("F49BF488-E8A4-4517-AC59-E5EA2093350D");
                public static Guid ADSK_Size_Shelf_Thickness => new("31881D64-B1C3-45D2-AB34-BAF2EE3E74E4");
                public static Guid ADSK_Size_Thickness => new("293F055D-6939-4611-87B7-9A50D0C1F50E");
                public static Guid ADSK_Size_Width => new("8F2E4F93-9472-4941-A65D-0AC468FD6A5D");
            }
                
            public static SharedParameterDefinition ADSK_Size_Angle => new(
                Guids.ADSK_Size_Angle,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Base_Thickness => new(
                Guids.ADSK_Size_Base_Thickness,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Depth => new(
                Guids.ADSK_Size_Depth,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Diameter => new(
                Guids.ADSK_Size_Diameter,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Element_Side_Thickness => new(
                Guids.ADSK_Size_Element_Side_Thickness,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Height => new(
                Guids.ADSK_Size_Height,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Item_Diameter => new(
                Guids.ADSK_Size_Item_Diameter,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Length => new(
                Guids.ADSK_Size_Length,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Offset => new(
                Guids.ADSK_Size_Offset,
                builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS);

            public static SharedParameterDefinition ADSK_Size_Radius => new(
                Guids.ADSK_Size_Radius,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Shelf_Thickness => new(
                Guids.ADSK_Size_Shelf_Thickness,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Thickness => new(
                Guids.ADSK_Size_Thickness,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);

            public static SharedParameterDefinition ADSK_Size_Width => new(
                Guids.ADSK_Size_Width,
                builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY);
        }
    }

    public static class App
    {
        public static List<ProjectSharedParameter> ProjectSharedParameters => new()
        {
            // new ProjectSharedParameter(Id_Ref,
            //     BuiltInCategory.OST_ElectricalCircuit,
            //     BuiltInCategory.OST_ElectricalEquipment),

            new ProjectSharedParameter(Id, ElectricalDeviceCategories),
            new ProjectSharedParameter(OwnerId, BuiltInCategory.OST_GenericAnnotation, BuiltInCategory.OST_Sheets),
            new ProjectSharedParameter(Panel_Name, BuiltInCategory.OST_ElectricalEquipment),
            new ProjectSharedParameter(Circuit_Panel, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Circuit_Designation, ElectricalDeviceCategories.Union(new[] { BuiltInCategory.OST_ElectricalCircuit }).ToArray()),
            new ProjectSharedParameter(Circuit_Topology, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Circuit_Phase, ElectricalCommonCategories),
            new ProjectSharedParameter(LoadApparent_Estimate, ElectricalCommonCategories),
            new ProjectSharedParameter(LoadTrue, ElectricalCommonCategories),
            new ProjectSharedParameter(LoadTrue_Estimate, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Estimate, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Estimate_L1, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Estimate_L2, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Estimate_L3, ElectricalCommonCategories),
            new ProjectSharedParameter(DemandFactor_Additional, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(DemandFactor_Total, ElectricalCommonCategories),
            new ProjectSharedParameter(PowerFactor, ElectricalCommonCategories),
            new ProjectSharedParameter(VoltageDrop, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Short1, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Short3, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Short3_Shock, ElectricalCommonCategories),
            new ProjectSharedParameter(Current_Start, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(LockCalculateCircuit, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Designation, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cables_Count, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Type, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_CoresSection, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Name, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Diameter, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Length, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Length_InCableTray, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_Length_OutsideCableTray, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_LengthMax, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Cable_LengthReserve, ElectricalDeviceCategories),
            new ProjectSharedParameter(Cable_MountingMethod, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(CircuitBreaker_Designation, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(LockCableLength, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(ReducingFactor, BuiltInCategory.OST_ElectricalCircuit),
            new ProjectSharedParameter(Rooms, BuiltInCategory.OST_ElectricalCircuit),

            new ProjectSharedParameter(Scheme_Notes, BuiltInCategory.OST_ElectricalEquipment),

            new ProjectSharedParameter(CableTray_Filling, CableTrayCategories),
            new ProjectSharedParameter(CableTray_FillingPercent, CableTrayCategories),

            new ProjectSharedParameter(CableTrace, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_01, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_02, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_03, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_04, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_05, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_06, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_07, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_08, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_09, CableTrayConduitCategories),
            new ProjectSharedParameter(CableTrace_10, CableTrayConduitCategories),
            new ProjectSharedParameter(ReferenceIds, CableTrayConduitCategories),

            new ProjectSharedParameter(ServiceType, ElectricalDeviceCategories),
            new ProjectSharedParameter(Adsk.Size.ADSK_Size_Offset, ElectricalDeviceCategories),
            // new ProjectSharedParameter(ElectricalSystemId, ElectricalDeviceCategories),
            // new ProjectSharedParameter(Id, ElectricalDeviceCategories),
        };

        public class Guids
        {
            public static Guid ClassName => new("2DBFAB3F-03D5-4F5F-B572-3E0B92003CD0");
            public static Guid BimEdKey => new("4A4EE2D6-A464-485D-AE25-D5B62BD2E353");
            public static Guid BimEdBinding => new("47A2CD17-18D7-496A-9AF0-517A666A09DF");
            public static Guid ElementKey => new("84115BB9-393B-4854-8D59-8FF91BB14F6D");
            public static Guid SectionKey => new("35A7209E-E91B-4FE8-B0E9-BFB46E619D58");
            public static Guid SectionName => new("DA613CD9-5830-49CE-8660-3AA4D6AB2073");
            public static Guid SubstationKey => new("2C274BFE-08E8-42AE-867D-11D327AD4142");
            public static Guid SubstationName => new("314456E1-0223-46C5-8187-6CC133CDD588");
            public static Guid VoltageInsulation => new("54DB8C26-B50A-4060-BDFD-4A54F076E711");
            public static Guid VoltagePrimary => new("21491E25-B9B4-43E8-9ABB-35C37DB33528");
            public static Guid VoltageSecondary => new("4E7A6D7A-9DAF-479E-92A3-B308D2CC009D");
            public static Guid PowerApparentRated => new("D2BE0163-A110-4BD1-9A64-A5B84C3BB5F6");
            public static Guid LossesLevelLoad => new("44755956-15EB-435B-A2D8-E9481EB72325");
            public static Guid LossesLevelNoLoad => new("4390C47E-0BB4-47E5-9E69-37392583AC2A");
            public static Guid Cable_Cores => new("876EDCA7-ABBC-47BF-B22C-BABE604109AE");
            public static Guid Cable_CoresSection => new("35426566-3309-40CD-A1BC-9637D848ECD9");
            public static Guid Cable_Designation => new("0B7106D7-795F-4981-9AEC-AE1EC60D3F9B");
            public static Guid Cable_Diameter => new("331FF9B2-57ED-4933-BB32-34D218EF81DD");
            public static Guid Cable_Length => new("315BC87E-941A-489B-ACFC-77C6E591EB6F");
            public static Guid Cable_Length_InCableTray => new("2C69AE7F-76DD-4BA8-840A-F48A28D33EF4");
            public static Guid Cable_Length_OutsideCableTray => new("158A1C82-C5D6-427E-B828-C0DD51E4B405");
            public static Guid Cable_LengthMax => new("C5305554-0DA1-4E1D-96F6-338C91528CEA");
            public static Guid Cable_LengthReserve => new("9A093637-EA68-4920-91D1-4ED6C4B1C9F3");
            public static Guid Cable_LengthReservePercent => new("978D8559-BD88-47CD-85B0-C7F53C50B1A7");
            public static Guid Cable_MountingMethod => new("E94E30C1-461F-4633-824D-3FBC527D3A27");
            public static Guid Cable_Name => new("B76065C9-36B2-4950-AFDE-30CA04E02DFA");
            public static Guid Cable_Section => new("1626DBA5-E5C0-4D28-8600-771B9C4A285C");
            public static Guid Cable_Type => new("7BFF9560-8094-47C0-963C-3184AA295C7A");
            public static Guid Cables_Count => new("02605D3C-419C-4860-8D2C-04D6A9D4AF2F");
            public static Guid CableTrace => new("3C465675-D0FA-4D5F-A997-77E21D61CA4D");
            public static Guid CableTrace_01 => new("1E03B7EF-E1B9-47B8-BB0D-6967457F8C66");
            public static Guid CableTrace_02 => new("6CDC2052-114E-4D3B-BC53-D40D6ED1D1ED");
            public static Guid CableTrace_03 => new("AF7D50B3-388E-4A6C-A3EE-40A741DBB72C");
            public static Guid CableTrace_04 => new("3992FF6D-EEBD-4280-9CF3-4173876A14E1");
            public static Guid CableTrace_05 => new("6218B8DD-ED55-412C-9C39-BF4244C8D9B1");
            public static Guid CableTrace_06 => new("D71F9BF4-6A32-48E0-BBB0-4E0D30B6A047");
            public static Guid CableTrace_07 => new("C93E4633-5478-4505-81C8-062FC8FA053C");
            public static Guid CableTrace_08 => new("B06D76D2-2496-4D50-8D67-D1D4EDEC0E47");
            public static Guid CableTrace_09 => new("CC52E1A1-A720-4E9B-B080-C3D409CAEFD3");
            public static Guid CableTrace_10 => new("95C99E28-65A1-4736-BA70-269596D2EF1E");
            public static Guid CableTray_Filling => new("B387334B-95F4-475A-8FD5-3FFD56556F2F");
            public static Guid CableTray_FillingPercent => new("8E6494C3-AAC9-4FAA-B3CA-7950322CA990");
            public static Guid Current_Estimate => new("A1CEBD9A-F55B-4141-A1E7-2AF13C681C15");
            public static Guid Current_Estimate_L1 => new("F9C62016-90EB-49A2-9351-5CE195083D70");
            public static Guid Current_Estimate_L2 => new("024CAACA-A8EA-48A1-A2C6-D2AEEDE5F247");
            public static Guid Current_Estimate_L3 => new("E33AEFAF-1D43-4120-837F-CEBC1F7F3043");
            public static Guid Current_Short1 => new("208FA501-216C-4B3A-9EF7-E23F093B0E57");
            public static Guid Current_Short3 => new("5C50A487-D0F2-42A4-8322-40DE1035B7F4");
            public static Guid Current_Short3_Shock => new("F8979053-FAB5-4D04-9EF6-443793A1FF0B");
            public static Guid Current_Start => new("C66489B8-48C6-415B-9766-A320EDDE4E2C");
            public static Guid DemandFactor_Additional => new("69196E64-03FA-4944-95F5-F9419DEDEEB3");
            public static Guid DemandFactor_Total => new("67AA0C5D-4ABE-4540-9170-D61410E42200");
            public static Guid Circuit_Designation => new("20B6E044-AA6D-42BB-A30B-6916EC42CA51");
            public static Guid LoadApparent => new("C3E6BF98-6AEC-4804-AC38-31265AB36737");
            public static Guid LoadApparent_Estimate => new("36E10116-9BAF-4A6F-A570-581CB901CCA8");
            public static Guid LoadApparent_Estimate_L1 => new("1F0E0C04-479C-4173-BA97-3A390A6A274A");
            public static Guid LoadApparent_Estimate_L2 => new("E1B7C0E0-09EF-4798-B44F-C3B033DBA396");
            public static Guid LoadApparent_Estimate_L3 => new("055D55E9-B6D6-4E45-BEBA-20ADC76D6BF1");
            public static Guid LoadApparent_L1 => new("A86EA6DF-7BF8-4319-A744-8C238B75679D");
            public static Guid LoadApparent_L2 => new("3C581AA4-0014-4C54-9783-EE4D68AE8DBA");
            public static Guid LoadApparent_L3 => new("95B0A2BF-E054-4299-9BB3-D34E579E235E");
            public static Guid LoadTrue => new("1C8C39F4-6337-47B7-BD7E-A439F5B607B7");
            public static Guid LoadTrue_Estimate => new("483CA9AE-0A89-4022-98B3-FC4F5203F63D");
            public static Guid LoadTrue_Estimate_L1 => new("C86CE3A0-8237-42E5-B4B6-53E56ADAE445");
            public static Guid LoadTrue_Estimate_L2 => new("BA59ECE2-20DF-4139-A71C-3A5F3F0E6497");
            public static Guid LoadTrue_Estimate_L3 => new("AE18D59F-20D3-4966-AAE0-C832CF52C487");
            public static Guid Circuit_Panel => new("33DBFC05-D624-43CB-B527-9B1547DCF424");
            public static Guid Circuit_Phase => new("3391E33B-EFF4-45D1-B2EC-8F084AE733E1");
            public static Guid Circuit_Topology => new("60F4E268-499F-4451-A6E2-0193F19523A0");
            public static Guid PowerFactor => new("2B9E03A2-5C9F-4E37-96D6-D4D31BC959F0");
            public static Guid ReducingFactor => new("B6E29EB6-C952-40F9-9433-93FF2E65239A");
            public static Guid VoltageDrop => new("36AA3B97-ECD5-43F7-9D9C-5114C425A4B7");
            public static Guid CircuitBreaker_Designation => new("DE1A677A-7F44-4DC5-A0D6-B0C3F35125BE");
            public static Guid Code_Equipment => new("CDAE7CEB-7F6B-4CFF-AF2B-4423C6082CE2");
            public static Guid Code_Function => new("55B396A2-2614-41D1-902F-633C362D4403");
            public static Guid Code_Mounting => new("7B11E476-59CB-4BB6-A49C-75ABC9F50D48");
            public static Guid General_DegreeOfProtection => new("28C66C8E-8924-4E75-8925-F9C619225E9C");
            public static Guid Common_Designation => new("9C98831B-9450-412D-B072-7D69B39F4029");
            public static Guid Common_FilterParameter => new("7054538B-113C-4550-851D-C3D87AAE9765");
            public static Guid Common_NotCount => new("9CAE56D5-E5D2-4C7E-825B-0084DDC2809D");
            public static Guid Common_SystemName => new("279140C7-1070-480C-B9D8-228564BDF635");
            public static Guid Id => new("A64EB211-369D-4303-A462-435D65D54B92");
            public static Guid ElectricalSystemId => new("C712B63A-51A6-4ED9-A0D1-9CE7346101A1");
            public static Guid OwnerId => new("5E1A2B5B-4211-4AA8-8042-8AFB1CC5FCA1");
            public static Guid ElectricalSystemIds => new("A5A1274E-4CDE-49FA-86EB-9EEF2221D7D6");
            public static Guid ReferenceIds => new("D966BD92-5784-4C0F-AD27-B4BE7D1EA1F0");
            public static Guid ReportIds => new("FAAEA63A-1CD6-4E10-9FF4-DE74A39D6ED2");
            public static Guid ImageFront => new("5B29FA50-383F-41A1-8B35-51B24DCD84FC");
            public static Guid Index => new("F7FF1408-02BC-4D19-978D-0FDC3E0E556C");
            public static Guid IP_Address => new("B108F278-33A3-4B58-A82C-4221FB35FBD1");
            public static Guid KNX_Area => new("3E4CACEA-C73C-4DFD-9D19-EAD61AFAD08A");
            public static Guid KNX_Device => new("FBBE3206-D17E-43B2-9C89-051E6911A0C2");
            public static Guid KNX_Line => new("56BF241C-501B-4289-85CA-41DEFC0EBFEB");
            public static Guid Language => new("4A4E4746-DBCF-4500-87C0-043FF3B0BB7A");
            public static Guid LockCableLength => new("E542F319-DF45-42DC-8538-75C99CB3F32B");
            public static Guid LockCircuitCalculate => new("235A43D6-BFB0-4A6E-A4B1-53214261CF7E");
            public static Guid Modbus_Address => new("75AF79C7-AAE8-4D85-A7B4-EB4D84550694");
            public static Guid Offset => new("515DC061-93CE-40E4-859A-E29224D80A10");
            public static Guid Panel_Name => new("8F893933-482F-4280-8F3F-81D11B17FB98");
            public static Guid R_Resistance => new("E706F701-9787-4728-8D00-F0EFA2820AE9");
            public static Guid R0_Resistance_ZeroSequence => new("0881AA75-8022-4AAF-97AA-6723B27C3EC3");
            public static Guid Rooms => new("18B691AF-551E-4DD7-8B62-C8BF4477F4DB");
            public static Guid Scheme_Notes => new("336B5C85-7F54-4D37-9D3F-A6FB930B267D");
            public static Guid SelectById => new("1FD22462-2179-4BC5-92EB-7568C6E6C6C5");
            public static Guid ServiceType => new("38A75B2D-9F32-41C8-86D6-B84D9C8BAE0B");
            public static Guid Size_CenterDistance => new("6475D5F8-DD51-4B6A-B2AE-518BE8EE4409");
            public static Guid SortingParameter => new("A0945274-995F-4BFA-A413-7696BA9C5366");
            public static Guid WithEnclosure => new("50EAA72F-C4AD-424F-A0AE-40A025C3BE10");
            public static Guid X_Reactance => new("8EB9E55B-EF41-4CA1-8659-6B7EC38892C3");
            public static Guid X0_Reactance_ZeroSequence => new("E090E232-E9C3-4178-9013-D84DA5BEC3AD");
            public static Guid Z_Impedance => new("3D1305E5-72A6-4720-9805-8075D90C7572");
        }

        public static SharedParameterDefinition BimEdKey => new(
            Guids.BimEdKey,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA);

        public static SharedParameterDefinition BimEdBinding => new(
            Guids.BimEdBinding,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition ElementKey => new(
            Guids.ElementKey,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition SectionKey => new(
            Guids.SectionKey,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition SectionName => new(
            Guids.SectionName,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition SubstationKey => new(
            Guids.SubstationKey,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition SubstationName => new(
            Guids.SubstationName,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition VoltageInsulation => new(
            Guids.VoltageInsulation,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL);

        public static SharedParameterDefinition VoltagePrimary => new(
            Guids.VoltagePrimary,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL);

        public static SharedParameterDefinition VoltageSecondary => new(
            Guids.VoltageSecondary,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL);

        public static SharedParameterDefinition PowerApparentRated => new(
            Guids.PowerApparentRated,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS);

        public static SharedParameterDefinition LossesLevelLoad => new(
            Guids.LossesLevelLoad,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS);

        public static SharedParameterDefinition LossesLevelNoLoad => new(
            Guids.LossesLevelNoLoad,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS);

        public static SharedParameterDefinition Code_Equipment => new(
            Guids.Code_Equipment,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA);

        public static SharedParameterDefinition Code_Function => new(
            Guids.Code_Function,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA);

        public static SharedParameterDefinition Code_Mounting => new(
            Guids.Code_Mounting,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA);

        public static SharedParameterDefinition Offset => new(
            Guids.Offset,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Cores => new(
            Guids.Cable_Cores,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_CoresSection => new(
            Guids.Cable_CoresSection,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Designation => new(
            Guids.Cable_Designation,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Diameter => new(
            Guids.Cable_Diameter,
            builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY,
            isInstance: true);

        public static SharedParameterDefinition Cable_Length => new(
            Guids.Cable_Length,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Length_InCableTray => new(
            Guids.Cable_Length_InCableTray,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Length_OutsideCableTray => new(
            Guids.Cable_Length_OutsideCableTray,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_LengthMax => new(
            Guids.Cable_LengthMax,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_LengthReserve => new(
            Guids.Cable_LengthReserve,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_LengthReservePercent => new(
            Guids.Cable_LengthReservePercent,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition Cable_MountingMethod => new(
            Guids.Cable_MountingMethod,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Name => new(
            Guids.Cable_Name,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Section => new(
            Guids.Cable_Section,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cable_Type => new(
            Guids.Cable_Type,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Cables_Count => new(
            Guids.Cables_Count,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition CircuitBreaker_Designation => new(
            Guids.CircuitBreaker_Designation,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition CableTrace => new(
            Guids.CableTrace,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_01 => new(
            Guids.CableTrace_01,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_02 => new(
            Guids.CableTrace_02,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_03 => new(
            Guids.CableTrace_03,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_04 => new(
            Guids.CableTrace_04,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_05 => new(
            Guids.CableTrace_05,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_06 => new(
            Guids.CableTrace_06,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_07 => new(
            Guids.CableTrace_07,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_08 => new(
            Guids.CableTrace_08,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_09 => new(
            Guids.CableTrace_09,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTrace_10 => new(
            Guids.CableTrace_10,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTray_Filling => new(
            Guids.CableTray_Filling,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition CableTray_FillingPercent => new(
            Guids.CableTray_FillingPercent,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        //public static SharedParameterDefinition CableTray_Section => new SharedParameterDefinition(
        //
        //    GuidValues.CableTray_Section,
        //    SharedParameters.CableTray_Section,
        //    GroupGroups.BIMElectricalDesign,
        //    ParameterTypeProxy.Area,
        //    builtInParameterGroup: BuiltInParameterGroup.PG_AELECTRICAL
        //;

        public static SharedParameterDefinition Current_Estimate => new(
            Guids.Current_Estimate,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Estimate_L1 => new(
            Guids.Current_Estimate_L1,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Estimate_L2 => new(
            Guids.Current_Estimate_L2,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Estimate_L3 => new(
            Guids.Current_Estimate_L3,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Short1 => new(
            Guids.Current_Short1,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Short3 => new(
            Guids.Current_Short3,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Short3_Shock => new(
            Guids.Current_Short3_Shock,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Current_Start => new(
            Guids.Current_Start,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition DemandFactor_Additional => new(
            Guids.DemandFactor_Additional,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition DemandFactor_Total => new(
            Guids.DemandFactor_Total,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Circuit_Designation => new(
            Guids.Circuit_Designation,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent => new(
            Guids.LoadApparent,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_L1 => new(
            Guids.LoadApparent_L1,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_L2 => new(
            Guids.LoadApparent_L2,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_L3 => new(
            Guids.LoadApparent_L3,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_Estimate => new(
            Guids.LoadApparent_Estimate,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_Estimate_L1 => new(
            Guids.LoadApparent_Estimate_L1,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_Estimate_L2 => new(
            Guids.LoadApparent_Estimate_L2,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadApparent_Estimate_L3 => new(
            Guids.LoadApparent_Estimate_L3,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadTrue => new(
            Guids.LoadTrue,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadTrue_Estimate => new(
            Guids.LoadTrue_Estimate,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadTrue_Estimate_L1 => new(
            Guids.LoadTrue_Estimate_L1,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadTrue_Estimate_L2 => new(
            Guids.LoadTrue_Estimate_L2,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LoadTrue_Estimate_L3 => new(
            Guids.LoadTrue_Estimate_L3,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition Circuit_Panel => new(
            Guids.Circuit_Panel,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Circuit_Phase => new(
            Guids.Circuit_Phase,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Circuit_Topology => new(
            Guids.Circuit_Topology,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition PowerFactor => new(
            Guids.PowerFactor,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition VoltageDrop => new(
            Guids.VoltageDrop,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition ReducingFactor => new(
            Guids.ReducingFactor,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition LockCableLength => new(
            Guids.LockCableLength,
            builtInParameterGroup: BuiltInParameterGroup.INVALID,
            isInstance: true);

        public static SharedParameterDefinition LockCalculateCircuit => new(
            Guids.LockCircuitCalculate,
            builtInParameterGroup: BuiltInParameterGroup.INVALID,
            isInstance: true);

        public static SharedParameterDefinition Common_DegreeOfProtection => new(
            Guids.General_DegreeOfProtection,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: false);

        public static SharedParameterDefinition Common_Designation => new(
            Guids.Common_Designation,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Common_FilterParameter => new(
            Guids.Common_FilterParameter,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Common_NotCount => new(
            Guids.Common_NotCount,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Common_SystemName => new(
            Guids.Common_SystemName,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Id => new(
            Guids.Id,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition OwnerId => new(
            Guids.OwnerId,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition ElectricalSystemIds => new(
            Guids.ElectricalSystemIds,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition ReferenceIds => new(
            Guids.ReferenceIds,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition ReportIds => new(
            Guids.ReportIds,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition Panel_Name => new(
            Guids.Panel_Name,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRAINTS,
            isInstance: true);

        public static SharedParameterDefinition Rooms => new(
            Guids.Rooms,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition Scheme_Notes => new(
            Guids.Scheme_Notes,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL,
            isInstance: true);

        public static SharedParameterDefinition ServiceType => new(
            Guids.ServiceType,
            builtInParameterGroup: BuiltInParameterGroup.PG_IDENTITY_DATA,
            isInstance: true);

        public static SharedParameterDefinition Size_CenterDistance => new(
            Guids.Size_CenterDistance,
            builtInParameterGroup: BuiltInParameterGroup.PG_GEOMETRY,
            isInstance: true);

        public static SharedParameterDefinition SortingParameter => new(
            Guids.SortingParameter,
            builtInParameterGroup: BuiltInParameterGroup.INVALID,
            isInstance: true);

        public static SharedParameterDefinition WithEnclosure => new(
            Guids.WithEnclosure,
            builtInParameterGroup: BuiltInParameterGroup.PG_CONSTRUCTION,
            isInstance: true);

        public static SharedParameterDefinition R_Resistance => new(
            Guids.R_Resistance,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition X_Reactance => new(
            Guids.X_Reactance,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition R0_Resistance_ZeroSequence => new(
            Guids.R0_Resistance_ZeroSequence,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);

        public static SharedParameterDefinition X0_Reactance_ZeroSequence => new(
            Guids.X0_Reactance_ZeroSequence,
            builtInParameterGroup: BuiltInParameterGroup.PG_ELECTRICAL_LOADS,
            isInstance: true);
    }
}