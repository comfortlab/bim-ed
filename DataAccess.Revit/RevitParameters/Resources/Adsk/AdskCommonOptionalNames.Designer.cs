﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoLa.BimEd.DataAccess.Revit.RevitParameters.Resources.Adsk {
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AdskCommonOptionalNames {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AdskCommonOptionalNames() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CoLa.BimEd.DataAccess.Revit.RevitParameters.Resources.Adsk.AdskCommonOptionalNames" +
                            "", typeof(AdskCommonOptionalNames).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Family Version.
        /// </summary>
        public static string ADSK_Family_Version {
            get {
                return ResourceManager.GetString("ADSK_Family_Version", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Grouping.
        /// </summary>
        public static string ADSK_Grouping {
            get {
                return ResourceManager.GetString("ADSK_Grouping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Material.
        /// </summary>
        public static string ADSK_Material {
            get {
                return ResourceManager.GetString("ADSK_Material", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Material Count Type.
        /// </summary>
        public static string ADSK_Material_Count_Type {
            get {
                return ResourceManager.GetString("ADSK_Material_Count_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Revit Version.
        /// </summary>
        public static string ADSK_Revit_Version {
            get {
                return ResourceManager.GetString("ADSK_Revit_Version", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Set.
        /// </summary>
        public static string ADSK_Set {
            get {
                return ResourceManager.GetString("ADSK_Set", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_View_Notes.
        /// </summary>
        public static string ADSK_View_Notes {
            get {
                return ResourceManager.GetString("ADSK_View_Notes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_View_Owner.
        /// </summary>
        public static string ADSK_View_Owner {
            get {
                return ResourceManager.GetString("ADSK_View_Owner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_View_Purpose.
        /// </summary>
        public static string ADSK_View_Purpose {
            get {
                return ResourceManager.GetString("ADSK_View_Purpose", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Норма расхода.
        /// </summary>
        public static string ADSK_Норма_расхода {
            get {
                return ResourceManager.GetString("ADSK_Норма_расхода", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ADSK_Предел огнестойкости экземпляра.
        /// </summary>
        public static string ADSK_Предел_огнестойкости_экземпляра {
            get {
                return ResourceManager.GetString("ADSK_Предел_огнестойкости_экземпляра", resourceCulture);
            }
        }
    }
}
