﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoLa.BimEd.DataAccess.Revit.RevitParameters.Resources.Adsk {
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AdskEngeneeringObligatoryTypes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AdskEngeneeringObligatoryTypes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CoLa.BimEd.DataAccess.Revit.RevitParameters.Resources.Adsk.AdskEngeneeringObligato" +
                            "ryTypes", typeof(AdskEngeneeringObligatoryTypes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ELECTRICAL_CURRENT.
        /// </summary>
        public static string ADSK_Electrical_Current {
            get {
                return ResourceManager.GetString("ADSK_Electrical_Current", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ELECTRICAL_POWER.
        /// </summary>
        public static string ADSK_Electrical_Power_Rated {
            get {
                return ResourceManager.GetString("ADSK_Electrical_Power_Rated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ELECTRICAL_POTENTIAL.
        /// </summary>
        public static string ADSK_Electrical_Voltage {
            get {
                return ResourceManager.GetString("ADSK_Electrical_Voltage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LOADCLASSIFICATION.
        /// </summary>
        public static string ADSK_Load_Classification {
            get {
                return ResourceManager.GetString("ADSK_Load_Classification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INTEGER.
        /// </summary>
        public static string ADSK_Number_of_Phase_Integer {
            get {
                return ResourceManager.GetString("ADSK_Number_of_Phase_Integer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NOOFPOLES.
        /// </summary>
        public static string ADSK_Number_of_Phases {
            get {
                return ResourceManager.GetString("ADSK_Number_of_Phases", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NUMBER.
        /// </summary>
        public static string ADSK_Power_Factor {
            get {
                return ResourceManager.GetString("ADSK_Power_Factor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LENGTH.
        /// </summary>
        public static string ADSK_Side_Thickness {
            get {
                return ResourceManager.GetString("ADSK_Side_Thickness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ELECTRICAL_APPARENT_POWER.
        /// </summary>
        public static string ADSK_Полная_мощность {
            get {
                return ResourceManager.GetString("ADSK_Полная_мощность", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_PRESSURE.
        /// </summary>
        public static string ADSK_Потеря_давления_воздуха {
            get {
                return ResourceManager.GetString("ADSK_Потеря_давления_воздуха", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_PRESSURE.
        /// </summary>
        public static string ADSK_Потеря_давления_жидкости {
            get {
                return ResourceManager.GetString("ADSK_Потеря_давления_жидкости", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_AIR_FLOW.
        /// </summary>
        public static string ADSK_Расход_воздуха {
            get {
                return ResourceManager.GetString("ADSK_Расход_воздуха", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_AIR_FLOW.
        /// </summary>
        public static string ADSK_Расход_воздуха_вытяжной {
            get {
                return ResourceManager.GetString("ADSK_Расход_воздуха_вытяжной", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_AIR_FLOW.
        /// </summary>
        public static string ADSK_Расход_воздуха_приточный {
            get {
                return ResourceManager.GetString("ADSK_Расход_воздуха_приточный", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_ГВ {
            get {
                return ResourceManager.GetString("ADSK_Расход_ГВ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_жидкости {
            get {
                return ResourceManager.GetString("ADSK_Расход_жидкости", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_канализационных_стоков {
            get {
                return ResourceManager.GetString("ADSK_Расход_канализационных_стоков", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_теплоносителя {
            get {
                return ResourceManager.GetString("ADSK_Расход_теплоносителя", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_ХВ {
            get {
                return ResourceManager.GetString("ADSK_Расход_ХВ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_FLOW.
        /// </summary>
        public static string ADSK_Расход_холодоносителя {
            get {
                return ResourceManager.GetString("ADSK_Расход_холодоносителя", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_PRESSURE.
        /// </summary>
        public static string ADSK_Свободный_напор_воздуха {
            get {
                return ResourceManager.GetString("ADSK_Свободный_напор_воздуха", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PIPING_PRESSURE.
        /// </summary>
        public static string ADSK_Свободный_напор_жидкости {
            get {
                return ResourceManager.GetString("ADSK_Свободный_напор_жидкости", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_POWER.
        /// </summary>
        public static string ADSK_Тепловая_мощность {
            get {
                return ResourceManager.GetString("ADSK_Тепловая_мощность", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HVAC_COOLING_LOAD.
        /// </summary>
        public static string ADSK_Холодильная_мощность {
            get {
                return ResourceManager.GetString("ADSK_Холодильная_мощность", resourceCulture);
            }
        }
    }
}
