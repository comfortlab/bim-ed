﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Geometry;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using Profile = CoLa.BimEd.Infrastructure.Framework.AutoMapper.Profile;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public class ModelProfile : Profile
{
    public ModelProfile()
    {
        CreateMap<Element, ElementProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ReverseMap();
        
        CreateMap<Level, LevelProxy>()
            .IncludeBase<Element, ElementProxy>();

        CreateMap<Room, RoomProxy>()
            .IncludeBase<Element, ElementProxy>()
            .ForMember(dest => dest.LevelId, opt => opt.MapFrom(src => src.LevelId.IntegerValue))
            .ForMember(dest => dest.Segments, opt => opt.MapFrom(src => GetSegments(src)));
        
        CreateMap<Line, SegmentProxy>()
            .ForMember(dest => dest.Point1, opt => opt.MapFrom(src => src.GetEndPoint(0)))
            .ForMember(dest => dest.Point2, opt => opt.MapFrom(src => src.GetEndPoint(1)));
        
        CreateMap<XYZ, XYZProxy>()
            .ReverseMap();
    }

    private static List<List<SegmentProxy>> GetSegments(SpatialElement spatialElement)
    {
        var segments = new List<List<SegmentProxy>>();
        var boundarySegments = spatialElement.GetBoundarySegments(new SpatialElementBoundaryOptions());

        foreach (var area in boundarySegments)
        {
            var areaSegments = new List<SegmentProxy>();
            segments.Add(areaSegments);
            
            foreach (var boundarySegment in area)
            {
                var tessellate = boundarySegment.GetCurve().Tessellate();
                
                for (var i = 1; i < tessellate.Count; i++)
                {
                    var xyz1 = tessellate[i - 1];
                    var xyz2 = tessellate[i];
                    var segment = new SegmentProxy(new XYZProxy(xyz1.X, xyz1.Y, xyz1.Z), new XYZProxy(xyz2.X, xyz2.Y, xyz2.Z));
                    areaSegments.Add(segment);
                }
            }
        }

        return segments;
    }
}