﻿using System;
using Autodesk.Revit.DB;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public static class RevitMappingUtils
{
    public static double ParameterValueAsAsDouble(this Element element, Guid guid, double defaultValue) => element.get_Parameter(guid)?.AsDouble() ?? defaultValue;
    public static int ParameterValueAsInteger(this Element element, Guid guid, int defaultValue) => element.get_Parameter(guid)?.AsInteger() ?? defaultValue;
    public static string ParameterValueAsAsString(this Element element, Guid guid, string defaultValue) => element.get_Parameter(guid)?.AsString() ?? defaultValue;
}