﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Mapping.Utils;
using CoLa.BimEd.DataAccess.Revit.Repositories.Sync;
using CoLa.BimEd.DataAccess.Revit.RevitParameters;
using CoLa.BimEd.Infrastructure.Framework.Math;
using Profile = CoLa.BimEd.Infrastructure.Framework.AutoMapper.Profile;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public class ElectricalEntitiesProfile : Profile
{
    public ElectricalEntitiesProfile()
    {
        CreateMap<FamilyInstance, ElectricalElementProxy>()
            .IncludeBase<Element, ElementProxy>()
            .ForMember(dest => dest.CableReserve, opt => opt.MapFrom(src => src.ParameterValueAsAsDouble(SharedParameters.App.Cable_LengthReserve.Guid, 0)))
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => (BuiltInCategoryProxy)src.Category.Id.IntegerValue))
            .ForMember(dest => dest.Level, opt => opt.Ignore())
            .ForMember(dest => dest.LevelId, opt => opt.Ignore())
            .ForMember(dest => dest.Origin, opt => opt.MapFrom(src => (src.Location as LocationPoint).Point))
            .ForMember(dest => dest.Room, opt => opt.Ignore())
            .ForMember(dest => dest.RoomId, opt => opt.Ignore())
            .ForMember(dest => dest.ServiceType, opt => opt.MapFrom(src => src.ParameterValueAsAsString(SharedParameters.App.ServiceType.Guid, string.Empty)))
            .AfterMap(SetLevelAndRoomId);

        CreateMap<FamilyInstance, ElectricalEquipmentProxy>()
            .IncludeBase<Element, ElementProxy>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => (BuiltInCategoryProxy)src.Category.Id.IntegerValue))
            .AfterMap((src, dest, ctx) =>
            {
                if (!TryGetEquipment(src, out var electricalEquipment) ||
                    !TryGetPrimaryConnector(src, out var primaryConnector))
                    return;

                if (ctx.Items == null || ctx.Items.Count < 2 ||
                    ctx.Items.ElementAt(0).Value is not List<DistributionSystemProxy> distributionSystems ||
                    ctx.Items.ElementAt(1).Value is not List<LoadClassificationProxy> loadClassifications)
                    throw new ArgumentException(
                        $"Mapping context was defined incorrectly.\n" +
                        $"Check for '{nameof(SwitchBoardUnitSync)}', '{nameof(TransformerSync)}' classes.");

                dest.SetBaseConnector(primaryConnector);
                SetLoadClassification(dest, loadClassifications);
                SetDistributionSystem(electricalEquipment, dest, distributionSystems);
            })
            .ReverseMap();

        CreateMap<FamilyInstance, SwitchBoardUnit>()
            .IncludeBase<FamilyInstance, ElectricalEquipmentProxy>()
            .AfterMap((_, dest) => dest.EquipmentSet = new SwitchBoard(ElementProxy.VirtualRevitId, dest.Name, new[] {dest}))
            .ReverseMap()
            .AfterMap((src, dest) =>
            {
                dest.get_Parameter(BuiltInParameter.RBS_ELEC_PANEL_NAME)?.Set(src.Name);
            });
        
        CreateMap<FamilyInstance, Transformer>()
            .IncludeBase<FamilyInstance, ElectricalEquipmentProxy>()
            .ReverseMap();

        CreateMap<ElectricalSystem, ElectricalSystemProxy>()
            .IncludeBase<Element, ElementProxy>()
            .ForMember(dest => dest.CircuitDesignation, opt => opt.MapFrom(src => src.ParameterValueAsAsString(SharedParameters.App.Guids.Circuit_Designation, string.Empty)))
            .ForMember(dest => dest.CableDesignation, opt => opt.MapFrom(src => src.ParameterValueAsAsString(SharedParameters.App.Guids.Cable_Designation, string.Empty)))
            .ForMember(dest => dest.CircuitType, opt => opt.MapFrom(src => (CircuitTypeProxy)src.CircuitType))
            .ForPath(dest => dest.Products.Cable.Diameter, opt => opt.MapFrom(src => src.ParameterValueAsAsDouble(SharedParameters.App.Guids.Cable_Diameter, default).MillimetersFromInternal()))
            .ForPath(dest => dest.Products.CablesCount, opt => opt.MapFrom(src => src.ParameterValueAsInteger(SharedParameters.App.Guids.Cables_Count, 1)))
            .ForMember(dest => dest.Topology, opt => opt.MapFrom(src => (CoLa.BimEd.BusinessLogic.Model.Electrical.Enums.Topology)src.ParameterValueAsInteger(SharedParameters.App.Guids.Circuit_Topology, default)))
            .AfterMap((src, dest, ctx) =>
            {
                if (ctx.Items == null || ctx.Items.Count < 1 ||
                    ctx.Items.ElementAt(0).Value is not ElectricalEquipmentProxy[] electricalEquipments)
                    throw new ArgumentException(
                        $"Mapping context was defined incorrectly.\n" +
                        $"Check for '{nameof(ElectricalSystemSync)}' class.");

                ConnectBaseEquipments(dest, src, electricalEquipments);
            })
            .ReverseMap()
            .IgnoreAllPropertiesWithAnInaccessibleSetter()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
            .AfterMap((proxyElement, revitElement) =>
            {
                revitElement.get_Parameter(SharedParameters.App.Guids.Cables_Count)?.Set(proxyElement.Products.CablesCount);
                revitElement.get_Parameter(SharedParameters.App.Guids.Cable_Length)?.Set(proxyElement.CableLength.MillimetersToInternal());
                revitElement.get_Parameter(SharedParameters.App.Guids.Cable_Length_InCableTray)?.Set(proxyElement.CableLengthInCableTray.MillimetersToInternal());
                revitElement.get_Parameter(SharedParameters.App.Guids.Cable_Length_OutsideCableTray)?.Set(proxyElement.CableLengthOutsideCableTray.MillimetersToInternal());
                revitElement.get_Parameter(SharedParameters.App.Guids.Cable_LengthMax)?.Set(proxyElement.CableLengthMax.MillimetersToInternal());
                revitElement.get_Parameter(SharedParameters.App.Guids.Circuit_Topology)?.Set((int)proxyElement.Topology);
                ElectricalSystemUtils.UpdateBaseEquipment(revitElement, proxyElement);
                ElectricalSystemUtils.UpdateElements(revitElement, proxyElement);
            });
    }

    private static void SetLevelAndRoomId(FamilyInstance src, ElectricalElementProxy dest)
    {
        var levelId = src.GetLevelId();

        dest.LevelId = levelId.IntegerValue;
        dest.RoomId = src.Room?.Id.IntegerValue ?? -1;
        
        if (dest.RoomId > 0 || dest.LevelId == -1 || src.Location is not LocationPoint locationPoint)
            return;

        var document = src.Document;
        var level = (Level)document.GetElement(levelId);
        var point = locationPoint.Point;
        var projectPoint = new XYZ(point.X, point.Y, level.Elevation + 0.01);
        var roomAtPoint = document.GetRoomAtPoint(projectPoint);

        dest.RoomId = roomAtPoint?.Id.IntegerValue ?? -1;
    }

    private static bool TryGetEquipment(FamilyInstance familyInstance, out ElectricalEquipment electricalEquipment)
    {
        electricalEquipment = null;

        if (familyInstance.MEPModel is not ElectricalEquipment equipment ||
            equipment.DistributionSystem == null)
            return false;

        electricalEquipment = equipment;
        return true;
    }

    private static bool TryGetPrimaryConnector(FamilyInstance familyInstance, out ConnectorProxy primaryConnector)
    {
        var connectors = familyInstance.MEPModel.ConnectorManager
            .Connectors.OfType<Connector>()
            .Where(n =>
                n.Domain == Domain.DomainElectrical &&
                n.ConnectorType == ConnectorType.End)
            .Select(ConnectorConverter.GetElectricalConnector);

        primaryConnector = connectors.FirstOrDefault(n => n.IsPrimary);
        return primaryConnector != null;
    }

    private static void SetLoadClassification(
        ElectricalEquipmentProxy electricalEquipment,
        IEnumerable<LoadClassificationProxy> loadClassifications)
    {
        var loadClassificationId = electricalEquipment.BaseConnector.PowerParameters.LoadClassification.RevitId;
        electricalEquipment.BaseConnector.PowerParameters.LoadClassification = loadClassifications.FirstOrDefault(x => x.RevitId == loadClassificationId);
    }

    private static void SetDistributionSystem(
        ElectricalEquipment electricalEquipment,
        ElectricalEquipmentProxy electricalEquipmentProxy,
        IEnumerable<DistributionSystemProxy> distributionSystems)
    {
        var distributionSystemId = electricalEquipment.DistributionSystem?.Id.IntegerValue;
        electricalEquipmentProxy.DistributionSystem = distributionSystemId != null
            ? distributionSystems.FirstOrDefault(x => x.RevitId == distributionSystemId)
            : null;
    }

    private static void ConnectBaseEquipments(
        ElectricalSystemProxy electricalSystemProxy, ElectricalSystem electricalSystem,
        IReadOnlyCollection<ElectricalEquipmentProxy> electricalEquipments)
    {
        if (electricalSystem.CircuitType is CircuitType.Space)
            return;
        
        var baseEquipment = electricalEquipments.FirstOrDefault(x => x.RevitId == electricalSystem.BaseEquipment?.Id.IntegerValue);
        var systemTypeProxy = (ElectricalSystemTypeProxy) electricalSystem.SystemType;
        
        if (baseEquipment != null)
        {
            var connector = systemTypeProxy is ElectricalSystemTypeProxy.PowerCircuit
                ? CreatePowerConnector(baseEquipment, electricalSystemProxy, electricalSystem)
                : new ConnectorProxy(electricalSystemProxy, 1, systemTypeProxy);
            
            electricalSystemProxy.SetBaseConnector(connector);
            electricalSystemProxy.ConnectTo(baseEquipment);
        }
        else
        {
            var connector = systemTypeProxy is ElectricalSystemTypeProxy.PowerCircuit
                ? CreatePowerConnector(electricalSystemProxy, electricalSystem)
                : new ConnectorProxy(electricalSystemProxy, 1, systemTypeProxy);
            
            electricalSystemProxy.SetBaseConnector(connector);
        }

        var elementIds = electricalSystem.Elements.OfType<Element>().Select(x => x.Id.IntegerValue).ToArray();
        
        foreach (var equipment in electricalEquipments.Where(x => elementIds.Contains(x.RevitId)))
            equipment.ConnectTo(electricalSystemProxy);
    }

    private static ConnectorProxy CreatePowerConnector(
        ElectricalEquipmentProxy electricalEquipmentProxy,
        ElectricalSystemProxy electricalSystemProxy, ElectricalSystem electricalSystem)
    {
        var phasesNumber = (PhasesNumber)electricalSystem.PolesNumber;
        var lineToGroundVoltage = electricalEquipmentProxy.DistributionSystem?.GetLineToGroundVoltage() ?? 0;

        return new ConnectorProxy(electricalSystemProxy, 1, phasesNumber, lineToGroundVoltage);
    }

    private static ConnectorProxy CreatePowerConnector(
        ElectricalSystemProxy electricalSystemProxy, ElectricalSystem electricalSystem)
    {
        var phasesNumber = (PhasesNumber)electricalSystem.PolesNumber;
        var lineToGroundVoltage = electricalSystem.Voltage.Convert(Converting.VoltsFromInternal);
        if (phasesNumber < PhasesNumber.Two) lineToGroundVoltage /= MathConstants.Sqrt3;

        return new ConnectorProxy(electricalSystemProxy, 1, phasesNumber, lineToGroundVoltage);
    }
}