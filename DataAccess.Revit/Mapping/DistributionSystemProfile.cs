﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public class DistributionSystemProfile : Profile
{
    public DistributionSystemProfile()
    {
        CreateMap<DistributionSysType, DistributionSystemProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.ElectricalPhaseConfiguration, opt => opt.MapFrom(src => (ElectricalPhaseConfigurationProxy)src.ElectricalPhaseConfiguration))
            .ForMember(dest => dest.PhasesNumber, opt => opt.MapFrom(src => src.ElectricalPhase == ElectricalPhase.SinglePhase ? PhasesNumber.One : PhasesNumber.Three))
            .AfterMap((src, dest, ctx) =>
            {
                if (dest.LineToGroundVoltage?.RevitId == src.VoltageLineToGround?.Id.IntegerValue &&
                    dest.LineToLineVoltage?.RevitId == src.VoltageLineToLine?.Id.IntegerValue)
                    return;
                
                if (ctx.Items?.FirstOrDefault().Value is not List<VoltageTypeProxy> voltageTypes)
                    return;

                if (dest.LineToGroundVoltage?.RevitId != src.VoltageLineToGround?.Id.IntegerValue)
                    dest.LineToGroundVoltage = voltageTypes.FirstOrDefault(x => x.RevitId == src.VoltageLineToGround.Id.IntegerValue);
                
                if (dest.LineToLineVoltage?.RevitId != src.VoltageLineToLine?.Id.IntegerValue)
                    dest.LineToLineVoltage = voltageTypes.FirstOrDefault(x => x.RevitId == src.VoltageLineToLine.Id.IntegerValue);
            })
            .ReverseMap()
            .ForMember(src => src.CanBeDeleted, opt => opt.Ignore())
            .ForMember(src => src.ElectricalPhase, opt => opt.MapFrom(dest => dest.PhasesNumber == PhasesNumber.One ? ElectricalPhase.SinglePhase : ElectricalPhase.ThreePhase))
            .ForMember(src => src.ElectricalPhaseConfiguration, opt => opt.MapFrom(dest => (ElectricalPhaseConfiguration)dest.ElectricalPhaseConfiguration))
            .ForMember(src => src.NumWires, opt => opt.Ignore())
            .AfterMap((src, dest, ctx) =>
            {
                dest.NumWires = src.NumWires;
                
                if (dest.VoltageLineToGround?.Id.IntegerValue == src.LineToGroundVoltage?.RevitId &&
                    dest.VoltageLineToLine?.Id.IntegerValue == src.LineToLineVoltage?.RevitId)
                    return;
                
                if (ctx.Items?.FirstOrDefault().Value is not List<VoltageType> voltageTypes)
                    return;

                if (dest.VoltageLineToGround?.Id.IntegerValue != src.LineToGroundVoltage?.RevitId)
                    dest.VoltageLineToGround = voltageTypes.FirstOrDefault(x => x.Id.IntegerValue == src.LineToGroundVoltage.RevitId);
                
                if (dest.VoltageLineToLine?.Id.IntegerValue != src.LineToLineVoltage?.RevitId)
                    dest.VoltageLineToLine = voltageTypes.FirstOrDefault(x => x.Id.IntegerValue == src.LineToLineVoltage.RevitId);

            });

        CreateMap<VoltageType, VoltageTypeProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ReverseMap()
            .AfterMap((dest, src) =>
            {
                if (!dest.ActualValue.IsEqualTo(src.ActualValue) ||
                    !dest.MaxValue.IsEqualTo(src.MaxValue) ||
                    !dest.MinValue.IsEqualTo(src.MinValue))
                {
                    src.SetVoltageValue(
                        dest.ActualValue,
                        dest.MinValue,
                        dest.MaxValue);
                }
            });
    }
}