﻿using System;
using Autodesk.Revit.DB;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Revit.Converters;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Revit;
using CoLa.BimEd.Infrastructure.Revit.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Families;

namespace CoLa.BimEd.DataAccess.Revit.Mapping.Utils;

internal static class ConnectorConverter
{
    private static readonly int RevitVersionNumber = DI.Get<RevitContext>().Version;
    private static readonly FamilyDocuments FamilyDocuments = DI.Get<FamilyDocuments>();
        
    public static ConnectorProxy GetElectricalConnector(
        Connector connector)
    {
        var electricalConnector = RevitVersionNumber < 2017
            ? GetElectricalConnectorFromConnectorElement(connector)
            : GetElectricalPowerConnector(connector);

        if (connector.Domain == Domain.DomainElectrical)
            electricalConnector.Origin = PointConverter.CreatePoint(connector.Origin);

        return electricalConnector;
    }

    private static ConnectorProxy GetElectricalConnectorFromConnectorElement(
        Connector connector)
    {
        var familyInstance = connector.Owner as FamilyInstance;
        var familyDocument = FamilyDocuments.GetFamilyDocument(familyInstance);
        var connectorElement = connector.GetConnectorElement(familyDocument);

        return connectorElement != null
            ? CreateElectricalConnector(familyInstance, connector.Id, connectorElement)
            : null;
    }

    private static ConnectorProxy GetElectricalPowerConnector(
        Connector connector)
    {
        return connector?.GetMEPConnectorInfo() is MEPFamilyConnectorInfo connectorInfo
            ? CreateElectricalConnector(connector.Id, connectorInfo)
            : null;
    }

    private static ConnectorProxy CreateElectricalConnector(
        FamilyInstance familyInstance, int connectorId, ConnectorElement connectorElement)
    {
        var systemType = (ElectricalSystemTypeProxy)connectorElement.SystemClassification;

        switch (systemType)
        {
            case ElectricalSystemTypeProxy.PowerCircuit:
            case ElectricalSystemTypeProxy.PowerBalanced:
            case ElectricalSystemTypeProxy.PowerUnBalanced:
                return CreatePowerConnector(familyInstance, connectorId, connectorElement, systemType);

            default:
                return new ConnectorProxy(null, connectorId, systemType, isPrimary: connectorElement.IsPrimary);
        }
    }

    private static ConnectorProxy CreateElectricalConnector(
        int connectorId, MEPFamilyConnectorInfo connectorInfo)
    {
        var systemType = (ElectricalSystemTypeProxy)connectorInfo.GetIntegerValue(BuiltInParameter.RBS_ELEC_CIRCUIT_TYPE);

        switch (systemType)
        {
            case ElectricalSystemTypeProxy.PowerCircuit:
            case ElectricalSystemTypeProxy.PowerBalanced:
            case ElectricalSystemTypeProxy.PowerUnBalanced:
                return CreatePowerConnector(connectorId, connectorInfo);

            default:
                return new ConnectorProxy(null, connectorId, systemType, isPrimary: connectorInfo.IsPrimary);
        }
    }

    private static ConnectorProxy CreatePowerConnector(
        FamilyInstance familyInstance, int connectorId,
        ConnectorElement connectorElement, ElectricalSystemTypeProxy systemType)
    {
        var connector = new ConnectorProxy(null, connectorId, systemType, isPrimary: connectorElement.IsPrimary);

        var phasesNumber = (PhasesNumber)connectorElement.GetValueAsInteger(familyInstance, 
            BuiltInParameter.RBS_ELEC_NUMBER_OF_POLES);

        connector.CreatePowerParameters(phasesNumber, 0);
            
        connector.PowerParameters.OwnPowerFactor = connectorElement.GetConnectorElementParameter(
            familyInstance, BuiltInParameter.RBS_ELEC_POWER_FACTOR, out _)?.AsDouble() ?? 1;
            
        connector.PowerParameters.LoadClassification = new LoadClassificationProxy
        {
            RevitId = connectorElement.GetConnectorElementParameter(
                familyInstance, BuiltInParameter.RBS_ELEC_LOAD_CLASSIFICATION, out _)?.AsElementId()?.IntegerValue ?? -1
        };

        switch (connector.SystemType)
        {
            case ElectricalSystemTypeProxy.PowerBalanced:
            {
                var apparentLoad = connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD);
                SetBalancedApparentLoad(connector, apparentLoad);
                break;
            }
                
            case ElectricalSystemTypeProxy.PowerUnBalanced:
            {
                var apparentLoad1 = connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE1);
                var apparentLoad2 = phasesNumber > PhasesNumber.One ? connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE2) : default;
                var apparentLoad3 = phasesNumber > PhasesNumber.Two ? connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE3) : default;
                SetUnbalancedApparentLoad(connector, apparentLoad1, apparentLoad2, apparentLoad3);
                break;
            }
                
            case ElectricalSystemTypeProxy.PowerCircuit:
            {
                var apparentLoad = connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD);
                if (connector.PowerParameters.PhasesNumber == PhasesNumber.One)
                {
                    var apparentLoad1 = connectorElement.GetVoltAmperesFromInternal(familyInstance, BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE1);
                    apparentLoad = Math.Max(apparentLoad, apparentLoad1);
                }
                SetBalancedApparentLoad(connector, apparentLoad);
                break;
            }
        }

        return connector;
    }

    private static ConnectorProxy CreatePowerConnector(
        int connectorId,
        MEPFamilyConnectorInfo connectorInfo)
    {
        var connector = new ConnectorProxy(null, connectorId, ElectricalSystemTypeProxy.PowerCircuit, isPrimary: connectorInfo.IsPrimary)
        {
            SystemType = (ElectricalSystemTypeProxy)connectorInfo.GetIntegerValue(
                BuiltInParameter.RBS_ELEC_CIRCUIT_TYPE)
        };

        var phasesNumber = (PhasesNumber)connectorInfo.GetIntegerValue(
            BuiltInParameter.RBS_ELEC_NUMBER_OF_POLES);

        connector.CreatePowerParameters(phasesNumber, 0);
            
        var powerFactorValue = connectorInfo.GetConnectorParameterValue(new ElementId(
            BuiltInParameter.RBS_ELEC_POWER_FACTOR));
            
        connector.PowerParameters.OwnPowerFactor = ((DoubleParameterValue)powerFactorValue)?.Value ?? 1;

        connector.PowerParameters.LoadClassification = new LoadClassificationProxy
        {
            RevitId = connectorInfo.GetElementIdValue(BuiltInParameter.RBS_ELEC_LOAD_CLASSIFICATION)?.IntegerValue ?? -1
        };
            
        switch (connector.SystemType)
        {
            case ElectricalSystemTypeProxy.PowerBalanced:
            {
                var apparentLoad = connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD);
                SetBalancedApparentLoad(connector, apparentLoad);
                break;
            }
             
            case ElectricalSystemTypeProxy.PowerUnBalanced:
            {
                var apparentLoad1 = connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE1);
                var apparentLoad2 = phasesNumber > PhasesNumber.One ? connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE2) : default;
                var apparentLoad3 = phasesNumber > PhasesNumber.Two ? connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE3) : default;
                SetUnbalancedApparentLoad(connector, apparentLoad1, apparentLoad2, apparentLoad3);
                break;
            }
             
            case ElectricalSystemTypeProxy.PowerCircuit:
            {
                var apparentLoad = connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD);
                if (connector.PowerParameters.PhasesNumber == PhasesNumber.One)
                {
                    var apparentLoad1 = connectorInfo.GetVoltAmperesFromInternal(BuiltInParameter.RBS_ELEC_APPARENT_LOAD_PHASE1);
                    apparentLoad = Math.Max(apparentLoad, apparentLoad1);
                }
                SetBalancedApparentLoad(connector, apparentLoad);
                break;
            }
        }

        return connector;
    }

    private static void SetBalancedApparentLoad(ConnectorProxy connector, double apparentLoad)
    {
        connector.PowerParameters.OwnApparentLoad = apparentLoad;
            
        switch (connector.PowerParameters.PhasesNumber)
        {
            case PhasesNumber.Two:
                var apparentLoadDiv2 = apparentLoad / 2;
                connector.PowerParameters.OwnApparentLoad1 = apparentLoadDiv2;
                connector.PowerParameters.OwnApparentLoad2 = apparentLoadDiv2;
                return;
                
            case PhasesNumber.Three:
                var apparentLoadDiv3 = apparentLoad / 3;
                connector.PowerParameters.OwnApparentLoad1 = apparentLoadDiv3;
                connector.PowerParameters.OwnApparentLoad2 = apparentLoadDiv3;
                connector.PowerParameters.OwnApparentLoad3 = apparentLoadDiv3;
                return;
        }
    }

    private static void SetUnbalancedApparentLoad(
        ConnectorProxy connector, double apparentLoad1, double apparentLoad2, double apparentLoad3)
    {
        switch (connector.PowerParameters.PhasesNumber)
        {
            case PhasesNumber.One:
                connector.PowerParameters.OwnApparentLoad1 = apparentLoad1;
                return;
                
            case PhasesNumber.Two:
                connector.PowerParameters.OwnApparentLoad = apparentLoad1 + apparentLoad2;
                connector.PowerParameters.OwnApparentLoad1 = apparentLoad1;
                connector.PowerParameters.OwnApparentLoad2 = apparentLoad2;
                return;
                
            case PhasesNumber.Three:
                connector.PowerParameters.OwnApparentLoad = apparentLoad1 + apparentLoad2 + apparentLoad3;
                connector.PowerParameters.OwnApparentLoad1 = apparentLoad1;
                connector.PowerParameters.OwnApparentLoad2 = apparentLoad2;
                connector.PowerParameters.OwnApparentLoad3 = apparentLoad3;
                return;
        }
    }
}