﻿using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Revit.Electrical;
using CoLa.BimEd.Infrastructure.Revit.Utils;

namespace CoLa.BimEd.DataAccess.Revit.Mapping.Utils;

internal static class ElectricalSystemUtils
{
    public static void UpdateBaseEquipment(ElectricalSystem electricalSystem, ElectricalSystemProxy electricalSystemProxy)
    {
        if (electricalSystem.BaseEquipment != null)
        {
            if (electricalSystemProxy.Source is { RevitId: >= 0 })
            {
                // dest.SelectPanel();
            }
            else
            {
                electricalSystem.DisconnectPanel();
            }
        }
        else
        {
            if (electricalSystemProxy.Source is { RevitId: > 0 })
            {
                // dest.SelectPanel();
            }
        }
    }

    public static void UpdateElements(ElectricalSystem electricalSystem, ElectricalSystemProxy electricalSystemProxy)
    {
        var revitElements = electricalSystem.Elements.OfType<FamilyInstance>();
        var proxyElementIds = electricalSystemProxy.Consumers.Select(x => x.RevitId).ToList();

        foreach (var revitElement in revitElements)
        {
            if (proxyElementIds.Remove(revitElement.Id.IntegerValue))
                continue;

            var elementConnector = revitElement.GetConnector(electricalSystem);
            var electricalSystemConnector = elementConnector.GetRefConnector();
            elementConnector.DisconnectFrom(electricalSystemConnector);
        }

        var document = electricalSystem.Document;
        var elementSet = new ElementSet();
        proxyElementIds.Where(x => x > 0)
            .Select(x => document.GetElementAs<FamilyInstance>(x))
            .ForEach(x => elementSet.Insert(x));
        
        if (!elementSet.IsEmpty)
            electricalSystem.AddToCircuit(elementSet);
    }
}