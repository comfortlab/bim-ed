﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public class ElectricalProfile : Profile
{
    public ElectricalProfile()
    {
        DisableConstructorMapping();
        
        CreateMap<OperatingMode, OperatingModeDto>()
            .ReverseMap();
        
        CreateMap<OperatingModes, OperatingModesDto>()
            .AfterMap((src, dest, ctx) => dest.Items = ctx.Mapper.Map<OperatingModeDto[]>(src))
            .ReverseMap()
            .ForMember(src => src.Current, opt => opt.Ignore())
            .AfterMap((src, dest, ctx) =>
            {
                if (src.Items?.Any() ?? false)
                    dest.AddRange(ctx.Mapper.Map<List<OperatingMode>>(src.Items));
                
                dest.Current = dest.FirstOrDefault(x => x.Guid == src.CurrentGuid);
            });
        
        // CreateMap<ElectricalSettings, ElectricalSettingsDto>()
        //     .ReverseMap();
        
        CreateMap<ConnectorOperatingMode, ConnectorOperatingModeDto>()
            .ForMember(dest => dest.Current, opt => opt.MapFrom(src => src.EstimatedPowerParameters.Current))
            .ReverseMap()
            .ForPath(src => src.EstimatedPowerParameters.Current, opt => opt.MapFrom(dest => dest.Current));
        
        CreateMap<ConnectorProxy, ConnectorDto>()
            .ForMember(dest => dest.ConnectorOperatingModes, opt => opt.MapFrom(src => src.ConnectorOperatingModes.ToDictionary(n => n.Key.Guid, n => n.Value)))
            .ReverseMap()
            .ForMember(src => src.ConnectorOperatingModes, opt => opt.MapFrom(dest => MapOperatingModes(dest)));
        
        CreateMap<ElectricalBase, ElectricalBaseDto>()
            .ReverseMap();
    }

    private static Dictionary<OperatingMode, ConnectorOperatingModeDto> MapOperatingModes(ConnectorDto dest)
    {
        return dest.ConnectorOperatingModes.ToDictionary(n => new OperatingMode(n.Key), n => n.Value);
    }
}