﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Contracts.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.DataAccess.Model.Contracts.Electrical;
using CoLa.BimEd.DataAccess.Revit.Repositories.Storage;
using CoLa.BimEd.Infrastructure.Framework.AutoMapper;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Math;
using Profile = CoLa.BimEd.Infrastructure.Framework.AutoMapper.Profile;

namespace CoLa.BimEd.DataAccess.Revit.Mapping;

public class LoadClassificationProfile : Profile
{
    public LoadClassificationProfile()
    {
        DisableConstructorMapping();

        CreateMap<ElectricalLoadClassification, LoadClassificationProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .AfterMap((src, dest, ctx) =>
            {
                GetDemandFactorFromRevit(src, dest, ctx);
                GetStorageValue(src, dest, ctx);
            })
            .ReverseMap()
            .AfterMap((src, dest, ctx) =>
            {
                SetDemandFactorToRevit(src, dest, ctx);
                SetStorageValue(src, dest, ctx);
            });

        CreateMap<ElectricalDemandFactorDefinition, DemandFactorProxy>()
            .ForMember(dest => dest.RevitId, opt => opt.MapFrom(src => src.Id.IntegerValue))
            .ForMember(dest => dest.AdditionalLoad, opt => opt.MapFrom(src => src.AdditionalLoad.Convert(Converting.VoltAmperesFromInternal, false)))
            .AfterMap((_, dest) => dest.Values.ForEach(valueProxy => valueProxy.ConvertRangeFromInternal(dest.RuleType)))
            .ReverseMap()
            .ForMember(dest => dest.AdditionalLoad, opt => opt.MapFrom(src => src.AdditionalLoad.Convert(Converting.VoltAmperesToInternal, false)))
            .AfterMap(SetDemandFactorValues);

        CreateMap<ElectricalDemandFactorValue, DemandFactorValueProxy>()
            .ReverseMap();
        
        CreateMap<LoadClassificationProxy, LoadClassificationDto>()
            .ReverseMap();
        
        CreateMap<SelectorProductSetting, SelectorProductSettingDto>()
            .ForMember(
                dest => dest.MinCurrent,
                opt => opt.MapFrom(src => src.MinCurrent.Round(4)))
            .ForMember(
                dest => dest.MaxCurrent,
                opt => opt.MapFrom(src => src.MaxCurrent.Round(4)))
            .ReverseMap();
        
        CreateMap<SelectAndConfigSetting<ElectricalSystemProxy>, SelectAndConfigSettingDto>()
            .ForMember(
                dest => dest.CableSeriesId,
                opt => opt.MapFrom(src => src.CableSeries.Id))
            .ForMember(
                dest => dest.MinCoreSection,
                opt => opt.MapFrom(src => src.MinCoreSection.Round(4)))
            .ReverseMap()
            .ForPath(
                src => src.CableSeries.Id,
                opt => opt.MapFrom(dest => dest.CableSeriesId));
        
        CreateMap<SelectAndConfigSetting<SwitchBoardUnit>, SelectAndConfigSettingDto>()
            .ForMember(
                dest => dest.CableSeriesId,
                opt => opt.MapFrom(src => src.CableSeries.Id))
            .ForMember(
                dest => dest.MinCoreSection,
                opt => opt.MapFrom(src => src.MinCoreSection.Round(4)))
            .ReverseMap()
            .ForPath(
                src => src.CableSeries.Id,
                opt => opt.MapFrom(dest => dest.CableSeriesId));
        
        CreateMap<ProductRange, ProductRangeDto>()
            .ReverseMap();
    }

    private static void GetDemandFactorFromRevit(ElectricalLoadClassification src, LoadClassificationProxy dest, ResolutionContext ctx)
    {
        if ((dest.DemandFactor?.RevitId ?? -1) == (src.DemandFactorId?.IntegerValue ?? -1))
            return;

        if (ctx.Items?.FirstOrDefault().Value is not List<DemandFactorProxy> demandFactors)
            return;

        dest.DemandFactor = demandFactors.FirstOrDefault(x => x.RevitId == src.DemandFactorId?.IntegerValue);
    }

    private static void SetDemandFactorToRevit(LoadClassificationProxy src, ElectricalLoadClassification dest, ResolutionContext ctx)
    {
        if ((dest.DemandFactorId?.IntegerValue ?? -1) == (src.DemandFactor?.RevitId))
            return;

        if (ctx.Items?.FirstOrDefault().Value is not List<ElectricalDemandFactorDefinition> demandFactors)
            return;

        var demandFactor = demandFactors.FirstOrDefault(x => x.Id.IntegerValue == src.DemandFactor.RevitId);

        if (demandFactor != null)
            dest.DemandFactorId = demandFactor.Id;
    }

    private static void SetDemandFactorValues(DemandFactorProxy src, ElectricalDemandFactorDefinition dest)
    {
        var values = new List<ElectricalDemandFactorValue>(src.Values.Capacity);
        
        foreach (var proxy in src.Values)
        {
            proxy.GetInternalRange(src.RuleType, out var minRange, out var maxRange);
            values.Add(new ElectricalDemandFactorValue(minRange, maxRange, proxy.Factor));
        }

        dest.SetValues(values);
    }

    private static void GetStorageValue(Element src, LoadClassificationProxy dest, ResolutionContext ctx)
    {
        var storage = new ElementStorage(src);
        var dto = storage.GetAs<LoadClassificationDto>();
        if (dto != null) ctx.Mapper.Map(dto, dest);
    }

    private static void SetStorageValue(LoadClassificationProxy src, Element dest, ResolutionContext ctx)
    {
        var storage = new ElementStorage(dest);
        var dto = ctx.Mapper.Map<LoadClassificationDto>(src);
        storage.SetAsJson(dto);
    }
}