﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using CoLa.BimEd.Infrastructure.Framework.Math;
using CoLa.BimEd.Infrastructure.Revit.Electrical;

namespace CoLa.BimEd.DataAccess.Revit.Filters;

public static class ElectricalElementFilters
{
    public static readonly Func<FamilyInstance, bool> LowVoltageSwitchBoardFilter =
        familyInstance => familyInstance.IsLowVoltageSwitchBoard();
    
    public static readonly Func<FamilyInstance, bool> MediumVoltageSwitchGearFilter =
        familyInstance => familyInstance.IsMediumVoltageSwitchGear();
    
    public static readonly Func<FamilyInstance, bool> TransformerFilter =
        familyInstance => familyInstance.IsTransformer();
    
    public static bool IsLowVoltageSwitchBoard(this FamilyInstance familyInstance)
    {
        var family = familyInstance?.Symbol?.Family;

        if (family == null ||
            family.FamilyCategoryId.IntegerValue != (int) BuiltInCategory.OST_ElectricalEquipment)
            return false;

        var partType = family.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)?.AsInteger();

        return
            partType != null &&
            partType != (int)PartType.EquipmentSwitch &&
            partType != (int)PartType.OtherPanel &&
            partType != (int)PartType.Transformer &&
            familyInstance.MEPModel is ElectricalEquipment { DistributionSystem: { } } electricalEquipment &&
            electricalEquipment.DistributionSystem.GetInternalVoltageLineToLine().Convert(Converting.VoltsFromInternal) < 1000;
    }

    public static bool IsMediumVoltageSwitchGear(this FamilyInstance familyInstance)
    {
        var family = familyInstance?.Symbol?.Family;
            
        if (family == null ||
            family.FamilyCategoryId.IntegerValue != (int)BuiltInCategory.OST_ElectricalEquipment)
            return false;

        var partType = family.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)?.AsInteger();

        return
            partType != null &&
            partType != (int)PartType.EquipmentSwitch &&
            partType != (int)PartType.OtherPanel &&
            partType != (int)PartType.Transformer &&
            familyInstance.MEPModel is ElectricalEquipment { DistributionSystem: { } distributionSysType } &&
            distributionSysType.GetInternalVoltageLineToLine().Convert(Converting.VoltsFromInternal) >= 1000;
    }

    public static bool IsTransformer(this FamilyInstance familyInstance)
    {
        var family = familyInstance?.Symbol?.Family;

        return
            family != null &&
            family.FamilyCategoryId.IntegerValue == (int) BuiltInCategory.OST_ElectricalEquipment &&
            family.get_Parameter(BuiltInParameter.FAMILY_CONTENT_PART_TYPE)?.AsInteger() == (int) PartType.Transformer;
    }
}