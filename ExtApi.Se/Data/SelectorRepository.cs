﻿using System;
using System.Collections.Generic;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Resources;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Repository;

// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.ExtApi.Se.Data;

public class SelectorRepository : MemoryRepository<Selector>
{
    public const string Acti9_C120 = "KB_GCR_MCB_C120";
    public const string Acti9_iC60 = "KB_GCR_MCB_IC60";
    public const string Acti9_iCT = "KB_GCR_501B";
    public const string Acti9_iDPNVigi = "KB_GCR_MCB_PH_N";
    public const string Acti9_iDPNVigi_INT = "KB_GCR_MCB_PH_N.IDPN";
    public const string Acti9_iEM3000 = "DE_SOL_IEM3000";
    public const string Acti9_iK60 = "KB_GCR_MCB_IK60";
    public const string Acti9_iSW = "KB_GCR_SD_ISW";
    public const string Acti9_NG125 = "KB_GCR_MCB_NG125";
    public const string Altistart_01 = "ATS01";
    public const string Altistart_22 = "ATS22";
    public const string Altistart_48 = "ATS48";
    public const string Altivar_600 = "Altivar 600";
    public const string Altivar_900 = "Altivar 900";
    public const string ComPact_NSX = "KB_GCR_NSX100_630";
    public const string ComPact_NSXm = "KB_GCR_2042";
    public const string ComPact_NSXm_NA = "KB_GCR_2043";
    public const string Enclosure_Pragma = "PRAGMA";
    public const string Enclosure_Kaedra = "KAEDRA";
    public const string MasterPact_MTZ1_CB = "KB_GCR_MTZ1_CB";
    public const string MasterPact_MTZ2_CB = "KB_GCR_MTZ2_CB";
    public const string MasterPact_MTZ3_CB = "KB_GCR_MTZ3_CB";
    public const string MasterPact_MTZ1_SD = "KB_GCR_MTZ1_SD";
    public const string MasterPact_MTZ2_SD = "KB_GCR_MTZ2_SD";
    public const string MasterPact_MTZ3_SD = "KB_GCR_MTZ3_SD";
    public const string Modicon_M221 = "Modicon M221";
    public const string Modicon_M241 = "Modicon M241";
    public const string Modicon_M340 = "Modicon M340";
    public const string Modicon_M580 = "Modicon M580";
    public const string Modicon_TM3 = "Modicon TM3";
    public const string Modicon_X80 = "Modicon X80";
    public const string PowerLogic_PM3200 = "KB_GCR_701";

    public SelectorRepository() :
        base(new SelectorRepositoryInitializer())
    {
    }

    private class SelectorRepositoryInitializer : IRepositoryInitializer<Selector>
    {
        public List<Selector> Initialize()
        {
            return new List<Selector>
            {
                Acti9.C120(),
                Acti9.iC60(),
                Acti9.iCT(),
                Acti9.iDPNVigi(),
                Acti9.iDPNVigiInt(),
                Acti9.iK60(),
                Acti9.iEM3000(),
                Acti9.iSW(),
                Acti9.NG125(),
                
                Altistart.Altistart01(),
                Altistart.Altistart22(),
                Altistart.Altistart48(),
                
                Altivar.Altivar600(),
                Altivar.Altivar900(),
                
                ComPact.NSX(),
                ComPact.NSXm(),
                ComPact.NSXm_NA(),
                
                Enclosure.Pragma(),
                Enclosure.Kaedra(),
                
                MasterPact.MTZ1_CB(),
                MasterPact.MTZ1_SD(),
                MasterPact.MTZ2_CB(),
                MasterPact.MTZ2_SD(),
                MasterPact.MTZ3_CB(),
                MasterPact.MTZ3_SD(),
                
                // Modicon.M221(),
                // Modicon.M241(),
                // Modicon.M340(),
                // Modicon.M580(),
                // Modicon.X80(),
                // Modicon.TM3(),
                
                PowerLogic.PM3200(),
            };
        }

        private static class Acti9
        {
            internal static Selector C120() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_C120,
                Name = "Acti9 C120",
                Description = SelectorDescriptions.MCB_125,
                Image = SelectorImages.Acti9_C120,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_C120,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.RuRu,
                },
                MinCurrent = 63,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.PolesDescription,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                },
                UsedCriteria = new Dictionary<string, string>
                {
                    // [SeApi.Criteria.T_BreakingCapacity] = null,
                    // [SeApi.Criteria.T_CurveCode] = null,
                    // [SeApi.Criteria.T_NetworkType] = null,
                    // [SeApi.Criteria.T_PolesDescription] = null,
                    // [SeApi.Criteria.T_RatedCurrent] = null
                },
            };

            internal static Selector iC60() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iC60,
                Name = "Acti9 iC60",
                Description = SelectorDescriptions.MCB_63,
                Image = SelectorImages.Acti9_iC60,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_iC60,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.RuRu,
                },
                MinCurrent = 05,
                MaxCurrent = 63,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };

            internal static Selector iCT() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iCT,
                Name = "Acti9 iCT",
                Description = SelectorDescriptions.Acti9_iCT,
                Image = SelectorImages.Acti9_iCT,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.Contactors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 6,
                MaxCurrent = 100,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_ControlType,
                        SeApi.Values.ControlType_RemoteControl),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_UtilisationCategory,
                        SeApi.Values.UtilisationCategory_AC7A),
                    new CurrentCriteriaConverter(SeApi.Criteria.T_RatedOperationalCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.ControlCircuitVoltage,
                    SeApi.Characteristics.ControlType,
                    SeApi.Characteristics.NetworkFrequency,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedOperationalCurrent,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.ControlType,
                    SeApi.Characteristics.NetworkFrequency,
                    SeApi.Characteristics.ControlCircuitVoltage,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedOperationalCurrent,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.ControlCircuitVoltage,
                }
            };

            internal static Selector iDPNVigi() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iDPNVigi,
                Name = "Acti9 iDPN Vigi",
                Description = SelectorDescriptions.RCBO_40,
                Image = SelectorImages.Acti9_iDPNVigi,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_iDPNVigi,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.RuRu,
                },
                MinCurrent = 4,
                MaxCurrent = 40,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_EarthLeakageProtectionClass,
                        SeApi.Values.EarthLeakageProtectionClass_AC),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new EarthLeakageSensitivityConverter(),
                    new PolesPlusNeutralDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.EarthLeakageProtectionClass,
                    SeApi.Characteristics.EarthLeakageSensitivity,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.PolesDescription,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.EarthLeakageProtectionClass,
                    SeApi.Characteristics.EarthLeakageSensitivity,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };

            internal static Selector iDPNVigiInt() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iDPNVigi_INT,
                Name = "Acti9 iDPN Vigi",
                Description = SelectorDescriptions.RCBO_40,
                Image = SelectorImages.Acti9_iDPNVigi,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_iDPNVigi,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                MinCurrent = 4,
                MaxCurrent = 40,
                Localizations =
                {
                    Localization.EnInt,
                },
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_EarthLeakageProtectionClass,
                        SeApi.Values.EarthLeakageProtectionClass_AC),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new EarthLeakageSensitivityConverter(),
                    new PolesPlusNeutralDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.EarthLeakageProtectionClass,
                    SeApi.Characteristics.EarthLeakageSensitivity,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.PolesDescription,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.EarthLeakageProtectionClass,
                    SeApi.Characteristics.EarthLeakageSensitivity,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };

            internal static Selector iK60() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iK60,
                Name = "Acti9 iK60",
                Description = SelectorDescriptions.MCB_63,
                Image = SelectorImages.Acti9_iK60,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_iK60,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 6,
                MaxCurrent = 63,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.RatedCurrent,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };

            internal static Selector iEM3000() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Acti9_iEM3000,
                Name = "Acti9 iEM3000",
                Description = SelectorDescriptions.Acti9_iEM3000,
                Image = SelectorImages.Acti9_iEM3000,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.EnergyMetering,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 0,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    // new BreakingCapacityConverter(),
                    // new NetworkTypeConverter(),
                    // new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCriteria = new Dictionary<string, string>
                {
                    // [Selectors.Criteria.T_CurveCode] = null,
                    // [Selectors.Criteria.T_PolesDescription] = null,
                    // [Selectors.Criteria.T_RatedOperationalCurrent] = null,
                    // [Selectors.Criteria.T_RatedCurrent] = null
                },
                ShortNameCharacteristics = new List<string>
                {
                    // Selectors.Criteria.T_CurveCode,
                    // Selectors.Criteria.T_RatedCurrent,
                    // Selectors.Criteria.T_PolesDescription
                }
            };

            internal static Selector iSW() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_iSW,
                Name = "Acti9 iSW",
                Description = SelectorDescriptions.SD_125,
                Image = SelectorImages.Acti9_iSW,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_iSW,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SwitchDisconnectors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 20,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedMakingCapacity,
                    SeApi.Characteristics.RatedOperationalCurrent,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedOperationalCurrent,
                }
            };

            internal static Selector NG125() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = Acti9_NG125,
                Name = "Acti9 NG125",
                Description = SelectorDescriptions.MCB_125,
                Image = SelectorImages.Acti9_NG125,
                DefaultConfiguration = SeApi.DefaultConfigurations.Acti9_NG125,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.RuRu,
                },
                MinCurrent = 10,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new BreakingCapacityConverter(),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CurveCode,
                        SeApi.Values.CurveCode_C),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_NetworkType,
                        SeApi.Values.NetworkType_AC),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.MagneticTrippingLimit,
                    SeApi.Characteristics.NetworkType,
                    SeApi.Characteristics.NumberOfProtectedPoles,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                },
                SelectionCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CurveCode,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };
        }

        private static class Altistart
        {
            internal static Selector Altistart01() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Altistart_01,
                Name = "Altistart 01",
                Description = SelectorDescriptions.Altistart_01,
                Image = SelectorImages.Altistart_01,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SoftStarters,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.MountingMode,
                    SeApi.Characteristics.NetworkNumberOfPhases,
                    SeApi.Characteristics.HeightD,
                    SeApi.Characteristics.WidthD,
                    SeApi.Characteristics.DepthD,
                    SeApi.Characteristics.ProductWeight,
                },
            };

            internal static Selector Altistart22() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Altistart_22,
                Name = "Altistart 22",
                Description = SelectorDescriptions.Altistart_22,
                Image = SelectorImages.Altistart_22,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SoftStarters,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.MountingMode,
                    SeApi.Characteristics.NetworkNumberOfPhases,
                    SeApi.Characteristics.HeightD,
                    SeApi.Characteristics.WidthD,
                    SeApi.Characteristics.DepthD,
                    SeApi.Characteristics.ProductWeight,
                },
            };

            internal static Selector Altistart48() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Altistart_48,
                Name = "Altistart 48",
                Description = SelectorDescriptions.Altistart_48,
                Image = SelectorImages.Altistart_48,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SoftStarters,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.MountingMode,
                    SeApi.Characteristics.NetworkNumberOfPhases,
                    SeApi.Characteristics.HeightD,
                    SeApi.Characteristics.WidthD,
                    SeApi.Characteristics.DepthD,
                    SeApi.Characteristics.ProductWeight,
                },
            };
        }

        private static class Altivar
        {
            internal static Selector Altivar600() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Altivar_600,
                Name = "Altivar Process ATV600",
                Description = SelectorDescriptions.Altivar_600,
                Image = SelectorImages.Altivar,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.VariableSpeedDrivers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.MountingMode,
                    SeApi.Characteristics.NetworkNumberOfPhases,
                    SeApi.Characteristics.HeightD,
                    SeApi.Characteristics.WidthD,
                    SeApi.Characteristics.DepthD,
                    SeApi.Characteristics.ProductWeight,
                },
            };

            internal static Selector Altivar900() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Altivar_900,
                Name = "Altivar Process ATV900",
                Description = SelectorDescriptions.Altivar_900,
                Image = SelectorImages.Altivar,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.VariableSpeedDrivers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.MountingMode,
                    SeApi.Characteristics.NetworkNumberOfPhases,
                    SeApi.Characteristics.HeightD,
                    SeApi.Characteristics.WidthD,
                    SeApi.Characteristics.DepthD,
                    SeApi.Characteristics.ProductWeight,
                },
            };
        }

        private static class ComPact
        {
            internal static Selector NSX() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = ComPact_NSX,
                Name = "ComPact NSX",
                Description = SelectorDescriptions.CB_630,
                Image = SelectorImages.ComPact_NSX,
                DefaultConfiguration = SeApi.DefaultConfigurations.ComPact_NSX,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 100,
                MaxCurrent = 630,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CommonCriteriaConverter(
                        SeApi.Criteria.T_CircuitBreakerApplication,
                        SeApi.Values.Distribution),
                    new CommonCriteriaConverter(
                        SeApi.Criteria.YN_CombinedReference,
                        SeApi.Values.No),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_RatedCurrent),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_TripUnitRating),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.ProtectedPolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                }
            };

            internal static Selector NSXm() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = ComPact_NSXm,
                Name = "ComPact NSXm",
                Description = SelectorDescriptions.CB_160,
                Image = SelectorImages.ComPact_NSXm,
                DefaultConfiguration = SeApi.DefaultConfigurations.ComPact_NSXm,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 16,
                MaxCurrent = 160,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.BreakingCapacity,
                    SeApi.Characteristics.ConnectionsTerminals,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.ProtectedPolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.TripUnitName,
                },
                SelectionCharacteristics =
                {
                    SeApi.Characteristics.TripUnitName,
                    SeApi.Characteristics.ConnectionsTerminals,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                }
            };

            internal static Selector NSXm_NA() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = ComPact_NSXm_NA,
                Name = "ComPact NSXm NA",
                Description = SelectorDescriptions.SD_160,
                Image = SelectorImages.ComPact_NSXm_NA,
                DefaultConfiguration = SeApi.DefaultConfigurations.ComPact_NSXm_NA,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SwitchDisconnectors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 50,
                MaxCurrent = 160,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CommonCriteriaConverter(
                        SeApi.Criteria.TC_Connections,
                        SeApi.Values.Connections_Everlink),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.ConnectionsTerminals,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.RatedMakingCapacity,
                },
                SelectionCharacteristics =
                {
                    SeApi.Characteristics.ConnectionsTerminals,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                }
            };
        }

        private static class Enclosure
        {
            internal static Selector Pragma() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Enclosure_Pragma,
                Name = "Pragma",
                Description = SelectorDescriptions.Pragma,
                Image = SelectorImages.Pragma,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.Enclosures,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 0,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>(),
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.NumberOfModulesPerRow,
                    SeApi.Characteristics.NumberOfHorizontalRows,
                    SeApi.Characteristics.MountingMode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.NumberOfHorizontalRows,
                    SeApi.Characteristics.NumberOfModulesPerRow
                }
            };

            internal static Selector Kaedra() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Enclosure_Kaedra,
                Name = "Kaedra",
                Description = SelectorDescriptions.Kaedra,
                Image = SelectorImages.Kaedra,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.Enclosures,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 0,
                MaxCurrent = 125,
                CriteriaConverters = new List<ICriteriaConverter>(),
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCriteria = new Dictionary<string, string>
                {
                    [SeApi.Criteria.NumberOfHorizontalRows] = "1",
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.NumberOfModulesPerRow,
                    SeApi.Characteristics.NumberOfHorizontalRows,
                    SeApi.Characteristics.MountingMode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.NumberOfHorizontalRows,
                    SeApi.Characteristics.NumberOfModulesPerRow,
                }
            };
        }

        private static class MasterPact
        {
            internal static Selector MTZ1_CB() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ1_CB,
                Name = "MTZ1",
                Description = SelectorDescriptions.CB_630_1600,
                Image = SelectorImages.MTZ1_CB,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ1_CB,
                CanHaveAccessories = false,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 630,
                MaxCurrent = 1600,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                },
            };

            internal static Selector MTZ2_CB() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ2_CB,
                Name = "MTZ2",
                Description = SelectorDescriptions.CB_800_4000,
                Image = SelectorImages.MTZ2_CB,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ2_CB,
                CanHaveAccessories = false,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 800,
                MaxCurrent = 4000,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                },
            };

            internal static Selector MTZ3_CB() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ3_CB,
                Name = "MTZ3",
                Description = SelectorDescriptions.CB_4000_6300,
                Image = SelectorImages.MTZ3_CB,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ3_CB,
                CanHaveAccessories = false,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.CircuitBreakers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 4000,
                MaxCurrent = 6300,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.T_RatedCurrent),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(ElectricalSystemProxy),
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                },
            };

            internal static Selector MTZ1_SD() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ1_SD,
                Name = "MTZ1",
                Description = SelectorDescriptions.SD_630_1600,
                Image = SelectorImages.MTZ1_SD,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ1_SD,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SwitchDisconnectors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 630,
                MaxCurrent = 1600,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_RatedCurrent),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_TripUnitRating),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.RatedMakingCapacity,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                },
            };

            internal static Selector MTZ2_SD() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ2_SD,
                Name = "MTZ2",
                Description = SelectorDescriptions.SD_800_4000,
                Image = SelectorImages.MTZ2_SD,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ2_SD,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SwitchDisconnectors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 800,
                MaxCurrent = 4000,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_RatedCurrent),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_TripUnitRating),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.RatedMakingCapacity,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription,
                },
            };

            internal static Selector MTZ3_SD() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = SeApi.Selectors.MasterPact_MTZ3_SD,
                Name = "MTZ3",
                Description = SelectorDescriptions.SD_4000_6300,
                Image = SelectorImages.MTZ3_SD,
                DefaultConfiguration = SeApi.DefaultConfigurations.MasterPact_MTZ3_SD,
                CanHaveAccessories = true,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.SwitchDisconnectors,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 4000,
                MaxCurrent = 6300,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_RatedCurrent),
                    new CurrentCriteriaConverter(
                        SeApi.Criteria.TA_TripUnitRating),
                    new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCharacteristics = new List<string>
                {
                    SeApi.Characteristics.CircuitBreakerRatingCode,
                    SeApi.Characteristics.PolesDescription,
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.RatedMakingCapacity,
                },
                ShortNameCharacteristics = new List<string>
                {
                    SeApi.Characteristics.RatedCurrent,
                    SeApi.Characteristics.PolesDescription
                }
            };
        }

        private static class Modicon
        {
            internal static Selector M221() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_M221,
                Name = "Modicon M221",
                Description = SelectorDescriptions.Modicon_M221,
                Image = SelectorImages.Modicon_M221,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };

            internal static Selector M241() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_M241,
                Name = "Modicon M241",
                Description = SelectorDescriptions.Modicon_M241,
                Image = SelectorImages.Modicon_M241,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };

            internal static Selector M340() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_M340,
                Name = "Modicon M340",
                Description = SelectorDescriptions.Modicon_M340,
                Image = SelectorImages.Modicon_M340,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };

            internal static Selector M580() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_M580,
                Name = "Modicon M580",
                Description = SelectorDescriptions.Modicon_M580,
                Image = SelectorImages.Modicon_M580,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };

            internal static Selector TM3() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_TM3,
                Name = "Modicon TM3",
                Description = SelectorDescriptions.Modicon_TM3,
                Image = SelectorImages.Modicon_TM3,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };

            internal static Selector X80() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyEz,
                Id = Modicon_X80,
                Name = "Modicon X80",
                Description = SelectorDescriptions.Modicon_X80,
                Image = SelectorImages.Modicon_X80,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.ProgrammableLogicControllers,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
            };
        }

        private static class PowerLogic
        {
            internal static Selector PM3200() => new()
            {
                Technology = SeApi.SelectorConfigs.TechnologyKb,
                Id = PowerLogic_PM3200,
                Name = "PowerLogic PM3200",
                Description = SelectorDescriptions.PowerLogic_PM3200,
                Image = SelectorImages.PowerLogic_PM3200,
                SelectorGroups = new List<SelectorGroup>
                {
                    SelectorGroup.EnergyMetering,
                },
                Localizations =
                {
                    Localization.EnInt,
                    Localization.RuRu,
                },
                MinCurrent = 0,
                MaxCurrent = 3150,
                CriteriaConverters = new List<ICriteriaConverter>
                {
                    // new CurrentCriteriaConverter(
                    //     SeApi.Criteria.T_RatedCurrent),
                    // new BreakingCapacityConverter(),
                    // new NetworkTypeConverter(),
                    // new PolesDescriptionConverter(),
                },
                Applicability = new List<Type>
                {
                    typeof(SwitchBoard),
                    typeof(SwitchBoardUnit),
                },
                UsedCriteria = new Dictionary<string, string>
                {
                    // [Selectors.Criteria.T_CurveCode] = null,
                    // [Selectors.Criteria.T_PolesDescription] = null,
                    // [Selectors.Criteria.T_RatedOperationalCurrent] = null,
                    // [Selectors.Criteria.T_RatedCurrent] = null
                },
                ShortNameCharacteristics = new List<string>
                {
                    // Selectors.Criteria.T_CurveCode,
                    // Selectors.Criteria.T_RatedCurrent,
                    // Selectors.Criteria.T_PolesDescription
                }
            };
        }
    }
}