﻿using CoLa.BimEd.Infrastructure.Framework.Net;

namespace CoLa.BimEd.ExtApi.Se.Communication;

public class SeApiHttpClient : AppHttpClient
{
    public SeApiHttpClient(HttpClientAccessor httpClientAccessor) : base(
        httpClientAccessor,
        Services.SeApi.SelectorConfigs.AuthScheme,
        Services.SeApi.SelectorConfigs.AuthToken,
        Services.SeApi.EndPoints.Base) { }
}