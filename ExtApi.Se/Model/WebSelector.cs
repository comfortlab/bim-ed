﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model;

public class WebSelector
{
    [JsonProperty("authScheme")]
    public string AuthScheme { get; set; }

    [JsonProperty("authToken")]
    public string AuthToken { get; set; }

    [JsonProperty("baseUri")]
    public string BaseUri { get; set; }

    [JsonProperty("configurationFileName")]
    public string ConfigurationFileName { get; set; }
}