﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Widget;

public class WidgetProduct
{
    [JsonProperty("reference")]
    public string Reference { get; set; }

    [JsonProperty("productId")]
    public string ProductId { get; set; }

    [JsonProperty("quantity")]
    public int Quantity { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("pictureUrl")]
    public string PictureUrl { get; set; }

    [JsonProperty("dataSheetUrl")]
    public string DataSheetUrl { get; set; }

    [JsonProperty("ean")]
    public string Ean { get; set; }

    [JsonProperty("enumber")]
    public string Enumber { get; set; }

    [JsonProperty("isCore")]
    public bool? IsCore { get; set; }
}