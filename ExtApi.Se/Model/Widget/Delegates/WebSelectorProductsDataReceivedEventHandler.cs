﻿using System.Threading.Tasks;
using CoLa.BimEd.ExtApi.Se.Model.Widget.EventArgs;

namespace CoLa.BimEd.ExtApi.Se.Model.Widget.Delegates;

public delegate Task WebSelectorProductsDataReceivedEventHandler(object sender, WebSelectorEventArgs args);