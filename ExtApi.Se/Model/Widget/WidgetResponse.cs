﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Widget;

public class WidgetResponse
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("quantity")]
    public int Quantity { get; set; }

    [JsonProperty("products")]
    public ICollection<WidgetProduct> Products { get; set; }
}