﻿using System.Collections.Generic;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Widget.Delegates;
using CoLa.BimEd.ExtApi.Se.Model.Widget.EventArgs;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Widget;

public class WidgetResult
{
    public event WebSelectorProductsDataReceivedEventHandler WebSelectorProductsDataReceived;
    public string Technology { get; set; }
    public string SessionId { get; set; }
    public string Products { get; set; }
    public string Id { get; set; }
    public int Quantity { get; set; }
    public Dictionary<string, EzSelectorData> Data { get; set; } = new();

    public void Receive(string products, string id, int quantity)
    {
        Products = products;
        Id = id;
        Quantity = quantity;

        WebSelectorProductsDataReceived?.Invoke(this,
            new WebSelectorEventArgs
            {
                Result = new WidgetResponse
                {
                    Products = JsonConvert.DeserializeObject<ICollection<WidgetProduct>>(products),
                    Id = this.Id,
                    Quantity = this.Quantity,
                },
                SessionId = this.SessionId,
                Technology = this.Technology,
                EzData = this.Data,
            });

        Data.Clear();
    }

    public override string ToString()
    {
        return $"{nameof(Products)}: {Products}, {nameof(Id)}: {Id}, {nameof(Quantity)}: {Quantity}";
    }
}