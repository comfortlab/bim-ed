﻿using System.Collections.Generic;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Model.Widget.EventArgs;

public class WebSelectorEventArgs : System.EventArgs
{
    public WidgetResponse Result { get; set; }

    public string Technology { get; set; }
    public string SessionId { get; set; }
    public Dictionary<string, EzSelectorData> EzData { get; set; }
}