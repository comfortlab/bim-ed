﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors.SelectorRequest;

public record SelectorConfigRequest : SelectorBase
{
    [JsonProperty("selectionName")]
    public string SelectionName { get; set; }

    [JsonProperty(@"data")]
    public SelectorConfigDataRequest SelectorConfigDataRequest { get; set; }
}