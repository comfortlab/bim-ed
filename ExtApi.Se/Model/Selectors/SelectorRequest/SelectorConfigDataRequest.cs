﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors.SelectorRequest;

public record SelectorConfigDataRequest : SelectorConfigData
{
    [JsonProperty("configId")]
    public string ConfigId { get; set; }
}