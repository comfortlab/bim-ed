﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public class Selector
{
    public string Technology { get; set; }
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Configuration { get; set; }
    public string DefaultConfiguration { get; set; }
    public bool CanHaveAccessories { get; set; }
    public List<string> Localizations { get; set; } = new();
    public Bitmap Image { get; set; }
    public List<string> SelectionCharacteristics { get; set; } = new();
    public List<string> ShortNameCharacteristics { get; set; } = new();
    public List<string> UsedCharacteristics { get; set; } = new();
    public Dictionary<string, string> UsedCriteria { get; set; } = new();
    public List<Type> Applicability { get; set; } = new();
    public List<SelectorGroup> SelectorGroups { get; set; }
    public List<ICriteriaConverter> CriteriaConverters { get; set; } = new();
    public double MinCurrent { get; set; }
    public double MaxCurrent { get; set; }
}