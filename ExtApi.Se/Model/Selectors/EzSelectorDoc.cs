﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record EzSelectorDoc
{
    [JsonProperty("combinatoryId")]
    public string CombinatoryId { get; set; }

    [JsonProperty("results")]
    public ICollection<EzSelectorDocResult> Results { get; set; }
}