﻿using System;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record SelectorBase
{
    [JsonProperty(@"uniqueId")]
    public string UniqueId { get; set; }

    [JsonProperty(@"technology")]
    public string Technology { get; set; }

    [JsonProperty(@"globalId")]
    public string GlobalId { get; set; }

    [JsonProperty(@"software")]
    public string Software { get; set; }

    [JsonProperty(@"country")]
    public string Country { get; set; }

    [JsonProperty(@"language")]
    public string Language { get; set; }

    [JsonProperty(@"version")]
    public string Version { get; set; }

    [JsonProperty(@"host")]
    public string Host { get; set; }

    [JsonProperty(@"vpc")]
    public string Vpc { get; set; }

    [JsonProperty(@"bearer")]
    public string Bearer { get; set; }

    [JsonProperty(@"authToken")]
    public string AuthToken { get; set; }

    [JsonProperty(@"creationDate")]
    [JsonConverter(typeof(SelectorDateTimeConverter))]
    public DateTime CreationDate { get; set; }

    [JsonProperty(@"schemaVersion")]
    public string SchemaVersion { get; set; }
}