﻿using CoLa.BimEd.ExtApi.Se.Resources;
using CoLa.BimEd.Infrastructure.Framework.Attributes;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public enum SelectorGroup
{
    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.CircuitBreakers))]
    CircuitBreakers,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.Contactors))]
    Contactors,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.ControlDevices))]
    ControlDevices,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.Enclosures))]
    Enclosures,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.EnergyMetering))]
    EnergyMetering,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.ProgrammableLogicControllers))]
    ProgrammableLogicControllers,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.SwitchDisconnectors))]
    SwitchDisconnectors,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.SoftStarters))]
    SoftStarters,

    [ResourceDescription(typeof(SelectorGroups),
        nameof(SelectorGroups.VariableSpeedDrivers))]
    VariableSpeedDrivers,
}