﻿using System.Collections.Generic;
using ExtApiComm.WebSelector.Model.Selectors;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record SelectionState
{
    [JsonProperty("criterias")]
    public ICollection<Criteria> Criterias { get; set; }

    [JsonProperty("references")]
    public ICollection<Reference> References { get; set; }

    [JsonProperty("children")]
    public ICollection<SelectorConfigData> Children { get; set; }
}