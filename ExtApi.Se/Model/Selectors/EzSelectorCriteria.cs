﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record EzSelectorCriteria
{
    [JsonProperty("criteriaId")]
    public string CriteriaId { get; set; }

    [JsonProperty("selectedValue")]
    public string ValueId { get; set; }

    [JsonProperty("isAssigned")]
    public bool IsAssigned { get; set; }

    [JsonProperty("values")]
    public ICollection<EzSelectorCriteriaValue> Values { get; set; }
}