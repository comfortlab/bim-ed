﻿using Newtonsoft.Json;

namespace ExtApiComm.WebSelector.Model.Selectors
{
    public record Reference
    {
        [JsonProperty("reference")]
        public string ReferenceId { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
