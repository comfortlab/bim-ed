﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record Selections
{
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("globalId")]
    public string GlobalId { get; set; }

    [JsonProperty("version")]
    public string Version { get; set; }

    [JsonProperty("isComplete")]
    public bool IsComplete { get; set; }

    [JsonProperty("state")]
    public SelectionState State { get; set; }
}