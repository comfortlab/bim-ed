﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record Criteria
{
    [JsonProperty("criteriaId")]
    public string CriteriaId { get; set; }

    [JsonProperty("valueId")]
    public string ValueId { get; set; }

    [JsonProperty("bompath")]
    public string BomPath { get; set; }
}