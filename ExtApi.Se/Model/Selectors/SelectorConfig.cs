﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record SelectorConfig : SelectorBase
{
    [JsonProperty(@"data")]
    public SelectorConfigData Data { get; set; }
}