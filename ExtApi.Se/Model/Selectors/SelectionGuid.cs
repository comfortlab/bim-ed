﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record SelectionGuid
{
    [JsonProperty("guid")]
    public string Guid { get; set; }
}