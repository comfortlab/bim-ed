﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record EzSelectorData
{
    [JsonProperty("globalId")]
    public string GlobalId { get; set; }

    [JsonProperty("country")]
    public string Country { get; set; }

    [JsonProperty("language")]
    public string Language { get; set; }

    [JsonProperty("software")]
    public string Software { get; set; }

    [JsonProperty("version")]
    public string Version { get; set; }

    [JsonProperty("criterias")]
    public ICollection<EzSelectorCriteria> Criterias { get; set; }

    [JsonProperty("docs")]
    public ICollection<EzSelectorDoc> Docs { get; set; }
}