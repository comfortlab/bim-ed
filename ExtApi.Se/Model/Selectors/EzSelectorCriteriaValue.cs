﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record EzSelectorCriteriaValue
{
    [JsonProperty("value")]
    public string Value { get; set; }

    [JsonProperty("valueId")]
    public string ValueId { get; set; }
}