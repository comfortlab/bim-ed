﻿using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record AdministrationSelection : SelectorBase
{
    [JsonProperty("result")]
    public Configuration Result { get; set; }
}