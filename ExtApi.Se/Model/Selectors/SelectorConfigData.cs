﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Selectors;

public record SelectorConfigData
{
    [JsonProperty(@"selections")]
    public Selections Selections { get; set; }
}