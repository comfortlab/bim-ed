﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public class CharacteristicGroupNode
{
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("isComplete")]
    public bool IsComplete { get; set; }

    [JsonProperty("isConflicting")]
    public bool IsConflicting { get; set; }

    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("parentId")]
    public string ParentId { get; set; }

    [JsonProperty("isGhost")]
    public bool IsGhost { get; set; }

    [JsonProperty("position")]
    public int Position { get; set; }

    [JsonProperty("quantity")]
    public int Quantity { get; set; }

    [JsonProperty("displayParent")]
    public bool DisplayParent { get; set; }

    [JsonProperty("bomPath")]
    public string BomPath { get; set; }

    [JsonProperty("characteristicGroups")]
    public ICollection<CharacteristicGroup> CharacteristicGroups { get; set; }
}