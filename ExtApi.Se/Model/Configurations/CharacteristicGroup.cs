﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public record CharacteristicGroup
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("bomPath")]
    public string BomPath { get; set; }

    [JsonProperty("isVisible")]
    public bool IsVisible { get; set; }

    [JsonProperty("groupName")]
    public string GroupName { get; set; }

    [JsonProperty("groupIsComplete")]
    public bool GroupIsComplete { get; set; }

    [JsonProperty("isExpanded")]
    public bool IsExpanded { get; set; }

    [JsonProperty("hasItems")]
    public bool HasItems { get; set; }

    [JsonProperty("hasRequiredCharacteristics")]
    public bool HasRequiredCharacteristics { get; set; }

    [JsonProperty("canBeCleared")]
    public bool CanBeCleared { get; set; }

    [JsonProperty("visibleCharacteristicsCount")]
    public int VisibleCharacteristicsCount { get; set; }

    [JsonProperty("characteristics")]
    public ICollection<Characteristic> Characteristics { get; set; }
}