﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public class HiveInstance
{
    [JsonProperty("instances")]
    public ICollection<Instance> Instances { get; set; }
}