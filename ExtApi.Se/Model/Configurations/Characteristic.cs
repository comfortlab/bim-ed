﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public record Characteristic
{
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("bomPath")]
    public string BomPath { get; set; }

    [JsonProperty("isValueAssigned")]
    public bool IsValueAssigned { get; set; }

    [JsonProperty("isSystemDefault")]
    public bool IsSystemDefault { get; set; }

    [JsonProperty("isSystemEnforced")]
    public bool IsSystemEnforced { get; set; }

    [JsonProperty("quantity")]
    public int Quantity { get; set; }

    [JsonProperty("isVisible")]
    public bool IsVisible { get; set; }

    [JsonProperty("isConflicting")]
    public bool IsConflicting { get; set; }

    [JsonProperty("isReadOnly")]
    public bool IsReadOnly { get; set; }

    [JsonProperty("isRequired")]
    public bool IsRequired { get; set; }

    [JsonProperty("isMultiValued")]
    public bool IsMultiValued { get; set; }

    [JsonProperty("unit")]
    public string Unit { get; set; }

    [JsonProperty("values")]
    public ICollection<ValueData> Values { get; set; }
}