﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public record Instance
{
    [JsonProperty("posnr")]
    public string PositionNumber { get; set; }

    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("qty")]
    public string Quantity { get; set; }

    [JsonProperty("parentId")]
    public string ParentId { get; set; }

    [JsonProperty("bompath")]
    public string BomPath { get; set; }

    [JsonProperty("matnr")]
    public string MatNumber { get; set; }

    [JsonProperty("instances")]
    public ICollection<Instance> Instances { get; set; }
}