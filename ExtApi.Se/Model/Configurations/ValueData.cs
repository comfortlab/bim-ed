﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public class ValueData
{
    [JsonProperty("value")]
    public string Value { get; set; }

    [JsonProperty("valueId")]
    public string ValueId { get; set; }

    [JsonProperty("isAssigned")]
    public bool IsAssigned { get; set; }

    [JsonProperty("isDefault")]
    public bool IsDefault { get; set; }

    [JsonProperty("bomPath")]
    public string BomPath { get; set; }
}