﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations.Requests;

public class ConfigurationKbRequest
{
    [JsonProperty("commRef")]
    public string CommRef { get; set; }

    [JsonProperty("kbName")]
    public string KbName { get; set; }

    [JsonProperty("kbLang")]
    public string KbLang { get; set; }

    [JsonProperty("country")]
    public string Country { get; set; }

    [JsonProperty("project")]
    public string Project { get; set; }
}