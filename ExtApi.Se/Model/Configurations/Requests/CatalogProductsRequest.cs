﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations.Requests;

public class CatalogProductsRequest
{
    [JsonProperty("refIds")]
    public List<string> RefIds { get; set; }
}