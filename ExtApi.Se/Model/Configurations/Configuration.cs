﻿using System.Collections.Generic;
using ExtApiComm.WebSelector.Model.Configurations;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Configurations;

public record Configuration
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("kbId")]
    public string KbId { get; set; }

    [JsonProperty("kbVersion")]
    public string KbVersion { get; set; }

    [JsonProperty("project")]
    public string Project { get; set; }

    [JsonProperty("alias")]
    public string Alias { get; set; }

    [JsonProperty("country")]
    public string Country { get; set; }

    [JsonProperty("kbLang")]
    public string KbLang { get; set; }

    [JsonProperty("isComplete")]
    public bool IsComplete { get; set; }

    [JsonProperty("codification")]
    public string Codification { get; set; }

    [JsonProperty("hasConflictingValues")]
    public bool HasConflictingValues { get; set; }

    [JsonProperty("characteristicGroups")]
    public ICollection<CharacteristicGroup> CharacteristicGroups { get; set; }

    [JsonProperty("characteristicGroupNodes")]
    public ICollection<CharacteristicGroupNode> CharacteristicGroupNodes { get; set; }

    [JsonProperty("unsetCharacteristics")]
    public ICollection<Characteristic> UnsetCharacteristics { get; set; }

    [JsonProperty("references")]
    public ICollection<Reference> References { get; set; }

    [JsonProperty("hiveInstances")]
    public HiveInstance HiveInstances { get; set; }
}