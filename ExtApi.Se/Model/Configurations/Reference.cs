﻿using Newtonsoft.Json;

namespace ExtApiComm.WebSelector.Model.Configurations
{
    public class Reference
    {
        [JsonProperty("reference")]
        public string ReferenceId { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
