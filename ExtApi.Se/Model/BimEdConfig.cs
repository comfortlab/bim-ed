﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model;

public record BimEdConfig
{
    [JsonProperty("webSelector")]
    public WebSelector WebSelector { get; set; }
}