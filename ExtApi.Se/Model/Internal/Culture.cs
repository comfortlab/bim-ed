﻿namespace CoLa.BimEd.ExtApi.Se.Model.Internal;

public class Culture
{
    public string Language { get; set; }

    public string Country { get; set; }
}