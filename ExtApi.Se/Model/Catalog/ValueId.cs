﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class ValueId
{
    [JsonProperty("value_id")]
    public string Id { get; set; }

    [JsonProperty("translation")]
    public Dictionary<string, string> Translation { get; set; }
}