﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public record CatalogCharacteristic
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("short_description")]
    public string ShortDescription { get; set; }

    [JsonProperty("long_description")]
    public string LongDescription { get; set; }

    [JsonProperty("isVisible")]
    public bool IsVisible { get; set; }

    [JsonProperty("translation")]
    public Dictionary<string, string> Translation { get; set; }

    [JsonProperty("valueIds")]
    public ICollection<ValueId> ValueIds { get; set; }
}