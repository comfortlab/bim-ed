﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class CatalogCharacteristicGroupList
{
    [JsonProperty("characteristic_group")]
    public ICollection<CatalogCharacteristicGroup> CharacteristicGroups { get; set; }
}