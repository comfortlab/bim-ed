﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class CatalogCharacteristicList
{
    [JsonProperty("characteristic")]
    public ICollection<CatalogCharacteristic> Characteristics { get; set; }
}