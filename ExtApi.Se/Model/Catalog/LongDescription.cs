﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class LongDescription
{
    [JsonProperty("translation")]
    public Dictionary<string, string> Translation { get; set; }
}