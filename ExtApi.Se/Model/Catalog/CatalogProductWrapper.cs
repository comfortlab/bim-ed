﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public record CatalogProductWrapper
{
    [JsonProperty("catalogue_product")]
    public CatalogProduct CatalogProduct { get; set; }
}