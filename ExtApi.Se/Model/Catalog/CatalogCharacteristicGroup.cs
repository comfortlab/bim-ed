﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class CatalogCharacteristicGroup
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("label")]
    public string Label { get; set; }

    [JsonProperty("characteristic_list")]
    public CatalogCharacteristicList CharacteristicList { get; set; }

    [JsonProperty("translation")]
    public Dictionary<string, string> Translation { get; set; }
}