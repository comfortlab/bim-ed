﻿using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public record CatalogProduct
{
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("product_id")]
    public string ProductId { get; set; }

    [JsonProperty("ean")]
    public string Ean { get; set; }

    [JsonProperty("characteristic_group_list")]
    public CatalogCharacteristicGroupList CharacteristicGroupList { get; set; }
        
    [JsonProperty("isActive")]
    public bool IsActive { get; set; }

    [JsonProperty("long_desc")]
    public LongDescription LongDescription { get; set; }
}