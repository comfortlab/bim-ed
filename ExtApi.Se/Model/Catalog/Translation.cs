﻿using System.Collections.Generic;

namespace CoLa.BimEd.ExtApi.Se.Model.Catalog;

public class Translation
{
    public Dictionary<string, string> Translations { get; set; }
}