﻿using System;

namespace CoLa.BimEd.ExtApi.Se.Exceptions;

public class WebSelectorException : Exception
{
    public WebSelectorException(string message) : base(message)
    {
    }
}