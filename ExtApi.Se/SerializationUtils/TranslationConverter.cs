﻿// using System;
// using System.Globalization;
// using System.Linq;
// using CoLa.BimEd.ExtApi.Se.Model.Catalog;
// using Newtonsoft.Json;
// using Newtonsoft.Json.Linq;
//
// namespace CoLa.BimEd.ExtApi.Se.SerializationUtils
// {
//     // public class TranslationConverter : JsonConverter
//     // {
//     //     private readonly CultureInfo _currentCultureInfo = CultureInfo.CurrentCulture;
//     //     
//     //     public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
//     //     {
//     //         throw new NotImplementedException();
//     //     }
//     //
//     //     public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
//     //     {
//     //         var translation = (Translation)objectType.GetConstructor(Type.EmptyTypes)?.Invoke(null);
//     //
//     //         if (translation == null)
//     //         {
//     //             return null;
//     //         }
//     //
//     //         var jo = JObject.Load(reader);
//     //         var descriptionProperty = jo.Properties().FirstOrDefault(p => string.Equals(p.Name, _currentCultureInfo.Name, StringComparison.OrdinalIgnoreCase));
//     //
//     //         translation.CurrentCultureInfo = _currentCultureInfo;
//     //         translation.Description = descriptionProperty?.Value.Value<string>();
//     //
//     //         return translation;
//     //     }
//     //
//     //     public override bool CanConvert(Type objectType)
//     //     {
//     //         return objectType == typeof(Translation);
//     //     }
//     //
//     //     public override bool CanWrite => false;
//     // }
// }
