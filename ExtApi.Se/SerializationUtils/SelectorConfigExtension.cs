﻿using System.IO;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.SerializationUtils;

public static class SelectorConfigExtension
{
    public static string Serialize(this SelectorConfig selectorConfig)
    {
        return JsonConvert.SerializeObject(selectorConfig, new SelectorDateTimeConverter());
    }

    public static SelectorConfig Deserialize(string serializedSelectorConfig)
    {
        return JsonConvert.DeserializeObject<SelectorConfig>(serializedSelectorConfig, new SelectorDateTimeConverter());
    }

    public static void SaveToFile(this SelectorConfig selectorConfig, string filePath)
    {
        var fileInfo = new FileInfo(filePath);
        fileInfo.Directory?.Create();

        File.WriteAllText(fileInfo.FullName, selectorConfig.Serialize());
    }

    public static SelectorConfig LoadFromFile(string filePath)
    {
        return File.Exists(filePath) 
            ? JsonConvert.DeserializeObject<SelectorConfig>(File.ReadAllText(filePath), new SelectorDateTimeConverter()) 
            : null;
    }
}