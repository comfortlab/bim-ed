﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.SerializationUtils;

public class SelectorDateTimeConverter : JsonConverter
{
    private const string DateTimeFormat = @"ddd, dd MMM yyyy HH:mm:ss " + UtcId;

    private const string UtcId = @"UTC";

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        writer.WriteValue(((DateTime)value).ToString(DateTimeFormat, CultureInfo.CreateSpecificCulture("en-US")));
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var source = (string) reader.Value;
        var dateString = source.Replace($" {UtcId}", "");

        var date = Convert.ToDateTime(dateString);

        return date;
    }

    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(DateTime);
    }
}