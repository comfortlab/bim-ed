﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors;

public interface ICriteriaConverter
{
    Criteria Convert(ElectricalBase source, Configuration digest);
}