﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Configurations.Requests;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework;
using Reference = ExtApiComm.WebSelector.Model.Selectors.Reference;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors;

public class SelectorConfigBuilder
{
    private readonly SeApiHttpClient _seApiHttpClient;

    public SelectorConfig SelectorConfig { get; private set; }
    public BaseProduct Product { get; set; }

    public SelectorConfigBuilder(
        SeApiHttpClient seApiHttpClient,
        Localization localization)
    {
        _seApiHttpClient = seApiHttpClient;
        
        SelectorConfig = new SelectorConfig
        {
            UniqueId = string.Empty,
            Technology = SeApi.SelectorConfigs.TechnologyKb,
            Software = SeApi.SelectorConfigs.Software,
            Language = localization.LanguageCode,
            Country = localization.CountryCode,
            Version = SeApi.SelectorConfigs.Version,
            Host = string.Empty,
            Vpc = string.Empty,
            Bearer = $"{SeApi.SelectorConfigs.AuthScheme} {SeApi.SelectorConfigs.AuthToken}",
            AuthToken = string.Empty,
            CreationDate = DateTime.Now,
            SchemaVersion = SeApi.SelectorConfigs.SchemaVersion,
            Data = new SelectorConfigData
            {
                Selections = new Selections
                {
                    Version = SeApi.SelectorConfigs.Version,
                    IsComplete = false,
                    State = new SelectionState
                    {
                        References = new List<Reference>()
                    }
                }
            },
        };
    }

    public SelectorConfig Build(
        Selector selector,
        string reference)
    {
        SelectorConfig.Technology = selector.Technology;
        SelectorConfig.GlobalId = selector.Id;
        SelectorConfig.Data.Selections.GlobalId = selector.Id;

        SelectorConfig.Data.Selections.State.Criterias = new[]
        {
            new Criteria
            {
                CriteriaId = SeApi.Characteristics.Ref,
                ValueId = reference,
                BomPath = "00000010",
            },
        };
        SelectorConfig.Data.Selections.State.References.Add(new Reference
        {
            ReferenceId = reference,
            Quantity = 1,
        });

        return SelectorConfig;
    }

    public async Task<SelectorConfig> Build(
        string technology,
        string selectorId,
        ElectricalBase source,
        params ICriteriaConverter[] criteriaConverters)
    {
        SelectorConfig.Technology = technology;
        SelectorConfig.GlobalId = selectorId;
        SelectorConfig.Data.Selections.GlobalId = selectorId;

        switch (technology)
        {
            case SeApi.SelectorConfigs.TechnologyKb:
                var configuration = await GetConfiguration();
                SetCriteria(configuration, source, criteriaConverters);
                break;

            case SeApi.SelectorConfigs.TechnologyEz:
                SetCriteria(null, source, criteriaConverters);
                break;
        }

        return SelectorConfig;
    }

    public async Task<Configuration> GetConfiguration(string commRef = null)
    {
        return await _seApiHttpClient.RequestEntityAsync<ConfigurationKbRequest, Configuration>(
            CancellationToken.None,
            SeApi.EndPoints.Configurations,
            new ConfigurationKbRequest
            {
                CommRef = commRef,
                KbName = SelectorConfig.GlobalId,
                KbLang = SelectorConfig.Language,
                Country = SelectorConfig.Country,
                Project = SeApi.SelectorConfigs.Software,
            });
    }

    private void SetCriteria(Configuration configuration, string reference)
    {
        SelectorConfig.Data.Selections.State.Criterias = configuration
            .CharacteristicGroups
            .SelectMany(n => n.Characteristics)
            .Where(n => n.Id == SeApi.Characteristics.Ref || n.IsValueAssigned)
            .Select(n => n.Id == SeApi.Characteristics.Ref
                ? new Criteria
                {
                    CriteriaId = n.Id,
                    BomPath = n.BomPath,
                    ValueId = n.Values.FirstOrDefault(v => v.ValueId == reference)?.ValueId
                }
                : new Criteria
                {
                    CriteriaId = n.Id,
                    BomPath = n.BomPath,
                    ValueId = n.Values.FirstOrDefault(v => v.IsAssigned)?.ValueId
                })
            .Where(x => x.ValueId!= null)
            .ToArray();
    }   

    private void SetCriteria(
        Configuration configuration,
        ElectricalBase source,
        IReadOnlyCollection<ICriteriaConverter> criteriaConverters)
    {
        if (source != null &&
            criteriaConverters != null &&
            criteriaConverters.Any())
        {
            SelectorConfig.Data.Selections.State.Criterias = criteriaConverters
                .Select(x => x.Convert(source, configuration))
                .Where(x => x?.CriteriaId != null)
                .ToArray();
        }
        else
        {
            SelectorConfig.Data.Selections.State.Criterias = new List<Criteria>();
        }
    }
}