﻿using System;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.ExtApi.Se.Exceptions;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Math;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

/// <summary>
/// <see href="https://blog.se.com/energy-regulations/2014/02/06/iec-60898-1-iec-60947-2-tale-two-standards/"/>
/// </summary>
public class BreakingCapacityConverter : ICriteriaConverter
{
    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        var panel = source is SwitchBoardUnit electricalPanel
            ? electricalPanel
            : source?.GetFirstSourceOf<SwitchBoardUnit>();

        if (panel == null)
            return new Criteria();

        double shortCurrent;

        switch (panel.PowerParameters.PhasesNumber)
        {
            case PhasesNumber.Undefined:
                throw new WebSelectorException($"Phases number of the appropriate panel hasn't been determined correctly. Circuit Name={source.Name}, Panel={source.GetFirstSourceOf<SwitchBoardUnit>()?.SwitchBoard.Name}");
                
            case PhasesNumber.One:
                shortCurrent = panel.EstimatedPowerParameters.ShortCurrent1;
                break;
                
            case PhasesNumber.Two:
            case PhasesNumber.Three:
                shortCurrent = panel.EstimatedPowerParameters.ShortCurrent3;
                break;
                
            default:
                throw new ArgumentOutOfRangeException(nameof(PhasesNumber));
        }

        var characteristic = digest.FindRequiredCharacteristic(SeApi.Criteria.T_BreakingCapacity);

        if (characteristic == null)
            return new Criteria();

        var allValues = characteristic.FindSupportedValues(x =>
            x.Value.Contains(SeApi.Values.BreakingCapacityStandard));

        var result = FindValues.FindClosestValue(
            shortCurrent, allValues,
            x => x.ValueId.FirstDouble(defaultValue: 0));

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = characteristic.BomPath,
                CriteriaId = characteristic.Id
            }
            : new Criteria();
    }
}