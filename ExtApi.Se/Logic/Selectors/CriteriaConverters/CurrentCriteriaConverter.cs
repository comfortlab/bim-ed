﻿using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.Infrastructure.Framework.Math;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

public class CurrentCriteriaConverter : ICriteriaConverter
{
    private readonly string _characteristicId;

    public CurrentCriteriaConverter(string characteristicId)
    {
        _characteristicId = characteristicId;
    }
        
    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        var estimateCurrent = source?.BaseConnector.GetEstimatedPowerParameters()?.Current ?? default;
        var characteristic = digest.FindRequiredCharacteristic(_characteristicId);

        if (characteristic == null)
            return new Criteria();

        var appropriateValues = characteristic.FindSupportedValues();
            
        var result = FindValues.FindClosestValue(estimateCurrent, appropriateValues,
            x => x.ValueId.FirstDouble(defaultValue: 0));

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = characteristic.BomPath,
                CriteriaId = characteristic.Id
            }
            : new Criteria();
    }
}