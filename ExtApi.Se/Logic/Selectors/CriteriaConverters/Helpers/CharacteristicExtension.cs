﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.ExtApi.Se.Exceptions;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;

internal static class CharacteristicExtension
{
    public static Characteristic FindRequiredCharacteristic(this Configuration digest, string characteristicId)
    {
        return digest.CharacteristicGroups
            .SelectMany(g => g.Characteristics)
            .FirstOrDefault(c => c.Id == characteristicId);
    }

    public static IEnumerable<ValueData> FindSupportedValues(this Characteristic characteristic,
        Func<ValueData, bool> supportCriteria = null)
    {
        var values = characteristic.Values;

        if (values == null || !values.Any())
        {
            throw new WebSelectorException($"Possible values range for {characteristic.Id} not founded");
        }

        return values.Where(x => supportCriteria?.Invoke(x) ?? true);
    }
}