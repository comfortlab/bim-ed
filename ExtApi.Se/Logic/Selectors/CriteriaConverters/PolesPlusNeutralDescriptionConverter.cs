﻿using System;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.ExtApi.Se.Exceptions;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

public class PolesPlusNeutralDescriptionConverter : ICriteriaConverter
{
    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        string polesNumberId;

        switch (source?.PowerParameters.PhasesNumber)
        {
            case PhasesNumber.Undefined:
                throw new WebSelectorException($"Phases number of the appropriate panel hasn't been determined correctly. Circuit Name={source.Name}, Panel={source.GetFirstSourceOf<SwitchBoardUnit>()?.SwitchBoard?.Name}");
                
            case PhasesNumber.One:
            case PhasesNumber.Two:
                polesNumberId = Services.SeApi.Values.Poles_1P_N;
                break;
                
            case PhasesNumber.Three:
                polesNumberId = Services.SeApi.Values.Poles_3P_N;
                break;

            case null:
                return new Criteria();
                
            default:
                throw new ArgumentOutOfRangeException(nameof(PhasesNumber));
        }

        var characteristic = digest.FindRequiredCharacteristic(Services.SeApi
            .Criteria.T_PolesDescription);

        if (characteristic == null)
            return new Criteria();

        var allValues = characteristic.FindSupportedValues();

        var result = allValues.FirstOrDefault(x => x.ValueId == polesNumberId);

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = characteristic.BomPath,
                CriteriaId = characteristic.Id
            }
            : new Criteria();
    }
}