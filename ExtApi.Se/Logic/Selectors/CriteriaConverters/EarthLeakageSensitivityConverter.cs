﻿using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

public class EarthLeakageSensitivityConverter : ICriteriaConverter
{
    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        var characteristic = digest.FindRequiredCharacteristic(Services.SeApi
            .Criteria.T_EarthLeakageSensitivity);

        if (characteristic == null)
            return new Criteria();

        var isGroupCircuit = source is ElectricalSystemProxy { IsGroupCircuit: true };
        var value = isGroupCircuit
            ? Services.SeApi.Values.EarthLeakageSensitivity_300mA
            : Services.SeApi.Values.EarthLeakageSensitivity_30mA;
        var allValues = characteristic.FindSupportedValues();
        var result = allValues.FirstOrDefault(x => x.ValueId == value);

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = characteristic.BomPath,
                CriteriaId = characteristic.Id
            }
            : new Criteria();
    }
}