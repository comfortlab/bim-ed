﻿using System;
using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.ExtApi.Se.Exceptions;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Services;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

public class PolesDescriptionConverter : ICriteriaConverter
{
    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        string polesNumberId;

        switch (source?.PowerParameters.PhasesNumber)
        {
            case PhasesNumber.Undefined:
                throw new WebSelectorException($"Phases number of the appropriate panel hasn't been determined correctly. Circuit Name={source.Name}, Panel={source.GetFirstSourceOf<SwitchBoardUnit>()?.SwitchBoard.Name}");
                
            case PhasesNumber.One:
            case PhasesNumber.Two:
                polesNumberId = SeApi.Values.Poles_1P;
                break;
                
            case PhasesNumber.Three:
                polesNumberId = SeApi.Values.Poles_3P;
                break;

            case null:
                return new Criteria();
                
            default:
                throw new ArgumentOutOfRangeException(nameof(PhasesNumber));
        }

        var characteristic = digest.FindRequiredCharacteristic(SeApi.Criteria.T_PolesDescription);

        if (characteristic == null)
            return new Criteria();

        var allValues = characteristic.FindSupportedValues();

        var result = allValues.FirstOrDefault(x => x.ValueId == polesNumberId);

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = characteristic.BomPath,
                CriteriaId = characteristic.Id
            }
            : new Criteria();
    }
}