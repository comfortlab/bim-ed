﻿using System.Linq;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters.Helpers;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;

public class CommonCriteriaConverter : ICriteriaConverter
{
    private readonly string _criteriaId;
    private readonly string _defaultValue;
    private readonly bool _firstIfNotDefault;

    public CommonCriteriaConverter(string criteriaId, string defaultValue, bool firstIfNotDefault = false)
    {
        _criteriaId = criteriaId;
        _defaultValue = defaultValue;
        _firstIfNotDefault = firstIfNotDefault;
    }

    public Criteria Convert(ElectricalBase source, Configuration digest)
    {
        var criteria = digest.FindRequiredCharacteristic(_criteriaId);

        if (criteria == null)
            return new Criteria();

        var allValues = criteria.FindSupportedValues().ToList();
        var result = allValues.FirstOrDefault(x => x.ValueId == _defaultValue) ??
                     (_firstIfNotDefault ? allValues.FirstOrDefault() : null);

        return result != default
            ? new Criteria
            {
                ValueId = result.ValueId,
                BomPath = criteria.BomPath,
                CriteriaId = criteria.Id
            }
            : new Criteria();
    }
}