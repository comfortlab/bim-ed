﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products.Characteristics;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Catalog;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using SelectorProduct = CoLa.BimEd.BusinessLogic.Model.Products.SelectorProduct;

namespace CoLa.BimEd.ExtApi.Se.Services;

public static partial class SeApi
{
    public class Converters
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Selectors _selectors;

        private Products _products;
        private Products Products => _products ??= _serviceProvider.Get<Products>();
        
        public Converters(IServiceProvider serviceProvider, Selectors selectors)
        {
            _serviceProvider = serviceProvider;
            _selectors = selectors;
        }

        public async Task<SelectorProduct> ConvertToProduct(SelectorConfig selectorConfig)
        {
            if (selectorConfig == null)
                return null;

            var globalId = selectorConfig.Data.Selections.GlobalId;
            var criterias = selectorConfig.Data.Selections.State.Criterias;
            var references = selectorConfig.Data.Selections.State.References;
            var product = new SelectorProduct(selectorConfig.Serialize());
            var selector = _selectors.GetById(globalId);

            if (selector == null)
                throw new KeyNotFoundException($"Selector was not found in {nameof(SeApi)}.{nameof(Selectors)}: {globalId}");

            product.GlobalId = globalId;
            product.SelectorId = selector.Id;

            var stringReferences = criterias.Where(n => n.CriteriaId == Characteristics.Ref).Select(n => n.ValueId).ToList();

            if (stringReferences.IsEmpty())
                stringReferences = references.Select(n => n.ReferenceId).ToList();

            product.References.AddRange(stringReferences);

            var mainReference = await GetProductMainReference(globalId, product);
            var catalogImage = await Products.GetImage(mainReference);
            var catalogProduct = await Products.GetCatalogProduct(mainReference);
            var productTuple = await GetProduct(selector.Id, catalogProduct, product.References);
            var mainProduct = productTuple.Product;
            var characteristicIds = selector.UsedCharacteristics
                .Union(selector.SelectionCharacteristics)
                .Union(selector.ShortNameCharacteristics)
                .ToList();
            var catalogCharacteristics = Products.GetCharacteristics(catalogProduct);

            mainProduct.AddCharacteristics(catalogCharacteristics
                .Where(characteristic => characteristicIds.Contains(characteristic.Id))
                .Select(Products.GetProductCharacteristic));
                
            mainProduct.Image = catalogImage;

            // foreach (var pair in selector.UsedCriteria.Where(uc => uc.Value != null && product.Criteria.All(c => c.Key != uc.Key)))
            // {
            //     product.Criteria.Add(
            //         pair.Key,
            //         new ProductCharacteristic(pair.Key, pair.Value)
            //     );
            // }
                
            product.Product = mainProduct;

            foreach (var accessoryReference in product.References.Where(r => r != mainReference).Distinct())
            {
                var accessoryProduct = (await GetProduct(selector.Id, accessoryReference, product.References)).Product;
                    
                accessoryProduct.Count = selectorConfig.Data.Selections.State.References
                    .FirstOrDefault(r => r.ReferenceId == accessoryReference)
                    ?.Quantity ?? -1;
                product.Accessories.Add(accessoryProduct);
            }

            if (selectorConfig.Data.Selections.State.Children != null)
            {
                foreach (var child in selectorConfig.Data.Selections.State.Children)
                {
                    foreach (var accessoryReference in child.Selections.State.References)
                    {
                        if (accessoryReference.Quantity < 1)
                            continue;

                        var accessory = product.Accessories.FirstOrDefault(a => a.Reference == accessoryReference.ReferenceId);

                        if (accessory != null)
                        {
                            accessory.Count += accessoryReference.Quantity;

                            continue;
                        }

                        var accessoryProduct = (await GetProduct(selector.Id, accessoryReference.ReferenceId, product.References)).Product;

                        accessoryProduct.Count = accessoryReference.Quantity;
                        product.Accessories.Add(accessoryProduct);
                    }
                }
            }

            product.MountingModeId = mainProduct.GetValue(Criteria.MountingMode);

            return product;
        }

        #region ConvertToProduct

        private async Task<string> GetProductMainReference(
            string selectorId,
            SelectorProduct selectorProduct)
        {
            if (selectorProduct.References.Count == 1)
            {
                return selectorProduct.References.FirstOrDefault();
            }

            switch (selectorId)
            {
                case Selectors.ComPact_NSX:

                    var secondReference = selectorProduct.References.ElementAtOrDefault(1);
                    var secondProduct = await GetProduct(selectorId, secondReference);

                    if (secondProduct.IsMainProduct)
                    {
                        return secondReference;
                    }

                    foreach (var reference in selectorProduct.References.Where(r => r != secondReference))
                    {
                        var product = await GetProduct(selectorId, reference);

                        if (product.IsMainProduct)
                        {
                            return reference;
                        }
                    }

                    return null;

                default:
                    return selectorProduct.References.FirstOrDefault();
            }
        }

        private async Task<(CatalogProduct CatalogProduct, Product Product, bool IsMainProduct)>
            GetProduct(string selectorId, string reference, List<string> references = null)
        {
            var catalogProduct = await Products.GetCatalogProduct(reference);
            var (product, isMainProduct) =  await GetProduct(selectorId, catalogProduct, references);

            return (catalogProduct, product, isMainProduct);
        }

        private async Task<(Product Product, bool IsMainProduct)>
            GetProduct(string selectorId, CatalogProduct catalogProduct, List<string> references = null)
        {
            var product = await Products.GetProduct(catalogProduct, selectorId, references);
            var mainGroup = catalogProduct?
                .CharacteristicGroupList
                .CharacteristicGroups?
                .FirstOrDefault(n => n.Label == CharacteristicsGroup.Main);
            var isMainProduct =
                IsProductType(mainGroup, CharacteristicsGroup.CircuitBreaker) ||
                IsProductType(mainGroup, CharacteristicsGroup.BasicFrame);

            return (product, isMainProduct);
        }

        private static bool IsProductType(CatalogCharacteristicGroup characteristicGroup, string valueId)
        {
            return characteristicGroup
                       ?.CharacteristicList
                       .Characteristics
                       .FirstOrDefault(c => c.Id == Characteristics.ProductOrComponentType)
                       ?.ValueIds
                       .Any(v => v.Id == valueId)
                   ?? false;
        }

        #endregion

        public SelectorConfig ConvertToConfig(string selectorId, string reference)
        {
            var selector = _selectors.GetById(selectorId);
            var selectorConfigBuilder = DI.Get<SelectorConfigBuilder>();

            return selectorConfigBuilder.Build(selector, reference);
        }

        public static ElectricalProduct ConvertToElectricalProduct(BaseProduct product)
        {
            var electricalProduct = new ElectricalProduct(product);

            SetCurrents(product, electricalProduct);
            SetCurve(product, electricalProduct);
            SetPoles(product, electricalProduct);

            return electricalProduct;
        }

        #region ConvertToElectricalProduct

        private static void SetCurrents(BaseProduct product, ElectricalProduct electricalProduct)
        {
            electricalProduct.BreakingCapacity =
                product.GetValueAsDouble(Characteristics.BreakingCapacity) ??
                product.GetValueAsDouble(Characteristics.RatedMakingCapacity) ?? default;

            electricalProduct.EarthLeakageSensitivity =
                product.GetValueAsDouble(Characteristics.EarthLeakageSensitivity) ?? default;

            electricalProduct.RatedCurrent =
                product.GetValueAsDouble(Characteristics.RatedCurrent) ??
                product.GetValueAsDouble(Characteristics.RatedOperationalCurrent) ?? default;

            var magneticTrippingLimit = product.GetValue(Characteristics.MagneticTrippingLimit);

            if (magneticTrippingLimit != default)
            {
                electricalProduct.MagneticTrippingLimit = new MagneticTrippingLimit(magneticTrippingLimit, electricalProduct.RatedCurrent);
            }
        }

        private static void SetCurve(BaseProduct product, ElectricalProduct electricalProduct)
        {
            electricalProduct.CurveCode = product.GetValue(Characteristics.CurveCode);
        }

        private static void SetPoles(BaseProduct product, ElectricalProduct electricalProduct)
        {
            var polesDescription =
                product.GetValue(Characteristics.PolesDescription);

            var protectedPolesDescription =
                product.GetValue(Characteristics.NumberOfProtectedPoles) ??
                product.GetValue(Characteristics.ProtectedPolesDescription);

            electricalProduct.Poles = new Poles(polesDescription, protectedPolesDescription);
        }

        #endregion

        private static readonly Dictionary<string, string> CharacteristicKeyToCriteriaCriteriaId = new()
        {
            { "A_CONTROL_CIRCUIT_VOLTAGE", "TC_CONTROL_CIRCUIT_VOLTAGE" },
            { "A_RATED_OPERATIONAL_CURRENT", "T_RATED_OPERATIONAL_CURRENT" },
            { "D_CONTROL_TYPE", "T_CONTROL_TYPE" },
            { "D_POLES_DESCRIPTION", "T_POLES_DESCRIPTION" }
        };

        private static readonly Dictionary<string, string> CharacteristicKeyToCriteriaCriteriaId2 = new()
        {
            { "A_RATED_OPERATIONAL_CURRENT", "T_UTILISATION_CATEGORY" }
        };

        private static readonly Dictionary<string, string> CharacteristicValueToCriteriaValueId = new()
        {
            { "12_V_AC_50_HZ", "12_V_50_HZ" },
            { "24_V_AC_50_HZ", "24_V_50_HZ" },
            { "48_V_AC_50_HZ", "48_V_50_HZ" },
            { "127_V_AC_60_HZ", "127_V_60_HZ" },
            { "220_V_AC_50_HZ", "220_V_50_HZ" },
            { "230_V_AC_50_HZ", "220_V_50_HZ" },
            { "230...240_V_AC_50_HZ", "220..240_V_50_HZ" },
            { "220...240_V_AC_50_HZ", "220..240_V_50_HZ" },
            { "220...240_V_AC_60_HZ", "220..240_V_60_HZ" }
        };

        private static readonly Dictionary<string, string> CharacteristicValue1ToCriteriaValueId = new()
        {
            { "6_A_AC-7A", "6_A" },
            { "8.5_A_AC-7A", "8.5_A" },
            { "15_A_AC-7A", "15_A" },

            { "16_A_AC-7A", "16_A" },
            { "20_A_AC-7A", "20_A" },
            { "25_A_AC-7A", "25_A" },

            { "40_A_AC-7A", "40_A" },
            { "63_A_AC-7A", "63_A" },
            { "100_A_AC-7A", "100_A" },

            { "6_A_AC-7B", "6_A" },
            { "8.5_A_AC-7B", "8.5_A" },
            { "15_A_AC-7B", "15_A" },

            { "16_A_AC-7B", "16_A" },
            { "20_A_AC-7B", "20_A" },
            { "25_A_AC-7B", "25_A" },

            { "40_A_AC-7B", "40_A" },
            { "63_A_AC-7B", "63_A" },
            { "100_A_AC-7B", "100_A" }
        };

        private static readonly Dictionary<string, string> CharacteristicValue2ToCriteriaValueId = new()
        {
            { "6_A_AC-7A", "AC-7A" },
            { "8.5_A_AC-7A", "AC-7A" },
            { "15_A_AC-7A", "AC-7A" },

            { "16_A_AC-7A", "AC-7A" },
            { "20_A_AC-7A", "AC-7A" },
            { "25_A_AC-7A", "AC-7A" },

            { "40_A_AC-7A", "AC-7A" },
            { "63_A_AC-7A", "AC-7A" },
            { "100_A_AC-7A", "AC-7A" },

            { "6_A_AC-7B", "AC-7B" },
            { "8.5_A_AC-7B", "AC-7B" },
            { "15_A_AC-7B", "AC-7B" },

            { "16_A_AC-7B", "AC-7B" },
            { "20_A_AC-7B", "AC-7B" },
            { "25_A_AC-7B", "AC-7B" },

            { "40_A_AC-7B", "AC-7B" },
            { "63_A_AC-7B", "AC-7B" },
            { "100_A_AC-7B", "AC-7B" }
        };

        private static Model.Selectors.Criteria ConvertToCriteria(ProductCharacteristic characteristic, Dictionary<string, string> keys, Dictionary<string, string> values)
        {
            var criteria = new Model.Selectors.Criteria
            {
                CriteriaId = keys.TryGetValue(characteristic.Key, out var keyValue)
                    ? keyValue
                    : characteristic.Key,
                ValueId = values.TryGetValue(characteristic.Value, out var valueValue)
                    ? valueValue
                    : characteristic.Value
            };

            criteria.BomPath = criteria.CriteriaId.StartsWith("TC_")
                ? "0000"
                : "00000010";

            return criteria;
        }

        public static IEnumerable<Model.Selectors.Criteria> ConvertToCriteriaCollection(ProductCharacteristic characteristic)
        {
            if (!CharacteristicKeyToCriteriaCriteriaId2.ContainsKey(characteristic.Key))
            {
                return new List<Model.Selectors.Criteria>
                {
                    ConvertToCriteria(characteristic, CharacteristicKeyToCriteriaCriteriaId, CharacteristicValueToCriteriaValueId)
                };
            }

            return new List<Model.Selectors.Criteria>
            {
                ConvertToCriteria(characteristic, CharacteristicKeyToCriteriaCriteriaId, CharacteristicValue1ToCriteriaValueId),
                ConvertToCriteria(characteristic, CharacteristicKeyToCriteriaCriteriaId2, CharacteristicValue2ToCriteriaValueId)
            };
        }
    }
}