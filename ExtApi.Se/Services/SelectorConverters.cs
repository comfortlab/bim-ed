﻿using System.Collections.Generic;
using System.Linq;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Selectors.SelectorRequest;
using CoLa.BimEd.ExtApi.Se.Model.Widget;
using ExtApiComm.WebSelector.Model.Selectors;

namespace CoLa.BimEd.ExtApi.Se.Services;

public class SelectorConverters
{
    private readonly SeApi.Selectors _selectors;

    public SelectorConverters(SeApi.Selectors selectors)
    {
        _selectors = selectors;
    }
    
    public SelectorConfigRequest ConvertToSelectorConfigRequest(SelectorConfig source, string configId)
    {
        return new SelectorConfigRequest
        {
            Version = source.Version,
            AuthToken = source.AuthToken,
            Bearer = source.Bearer,
            Country = source.Country,
            Language = source.Language,
            CreationDate = source.CreationDate,
            GlobalId = source.GlobalId,
            Host = source.Host,
            SchemaVersion = source.SchemaVersion,
            Software = source.Software,
            Technology = source.Technology,
            UniqueId = source.UniqueId,
            Vpc = source.Vpc,
            SelectorConfigDataRequest = new SelectorConfigDataRequest
            {
                Selections = null,
                ConfigId = configId
            }
        };
    }

    private static ICollection<Reference> GetReferences(IEnumerable<EzSelectorDoc> docs, IEnumerable<WidgetProduct> products, List<Reference> existingReferences)
    {
        const string catalog = "Catalog";
        const string accessories = "Accessories";

        var results = docs
            .SelectMany(d => d.Results)
            .ToList();

        var catalogReferences = results
            .Where(n => n.Name == catalog)
            .SelectMany(n => n.Value.Split(';'))
            .Select(GetReferenceId)
            .Where(n => !string.IsNullOrWhiteSpace(n))
            .Select(id => GetReference(id, products, existingReferences))
            .Where(n => n.Quantity > 0);

        var accessoryReferences = results
            .Where(n => n.Name == accessories)
            .SelectMany(n => n.Value.Split(';'))
            .Select(GetReferenceId)
            .Where(n => !string.IsNullOrWhiteSpace(n))
            .Select(id => GetReference(id, products, existingReferences))
            .Where(n => n.Quantity > 0);

        var allReferences = catalogReferences.Concat(accessoryReferences).ToList();

        // var newReferences = docs
        //     .FirstOrDefault(d => d.Results.Any(r => r.Name == catalog))
        //     ?.Results
        //     .FirstOrDefault(r => r.Name == catalog)
        //     ?.Value
        //     .Split(';')
        //     .Select(GetReferenceId)
        //     .Select(id => GetReference(id, products, existingReferences))
        //     .ToList() ?? new List<Reference>();

        existingReferences.AddRange(allReferences);

        return allReferences;
    }

    private static string GetReferenceId(string s)
    {
        var delimiterIndex = s.IndexOf('|');

        if (delimiterIndex == -1)
        {
            return s;
        }

        return s.Substring(0, delimiterIndex);
    }

    private static Reference GetReference(string referenceId, IEnumerable<WidgetProduct> products, ICollection<Reference> existingReferences)
    {
        var existingReference = existingReferences.FirstOrDefault(r => r.ReferenceId == referenceId);

        var quantity = 0;

        if (existingReference is { Quantity: > 1 })
        {
            existingReference.Quantity--;
            quantity = 1;
        }

        return new Reference
        {
            ReferenceId = referenceId,
            Quantity = existingReference != null
                ? quantity
                : products.FirstOrDefault(p => p.Reference == referenceId)?.Quantity ?? 0
        };
    }

    private static ICollection<Criteria> GetCriterias(IEnumerable<EzSelectorCriteria> criterias)
    {
        return criterias
            .Where(c => c.IsAssigned
                        || c.ValueId != null
                        || c.Values.Count == 1
                        || c.Values.Any(v => v.ValueId is "no" or "none" or "without"))
            .Select(c => new Criteria
            {
                CriteriaId = c.CriteriaId,
                ValueId = c.ValueId
                          ?? (c.Values.Count() == 1
                              ? c.Values.First().ValueId
                              : c.Values.FirstOrDefault(v => v.ValueId is "no" or "none" or "without")?.ValueId)
            })
            .ToList();
    }

    public static Dictionary<string, string> GetSelectionsNamesDictionary(EzSelectorData ezData)
    {
        var dictionary = new Dictionary<string, string>();
        var globalIds = ezData.Docs
            .FirstOrDefault(d => d.Results.Any(r => r.Name == "Linked"))
            ?.Results
            .FirstOrDefault(r => r.Name == "Linked")
            ?.Value
            .Split(';');

        if (globalIds == null)
            return new Dictionary<string, string>();

        var index = 0;

        foreach (var id in globalIds)
        {
            dictionary.Add(id, $"{id}_{index}_0_0");
            index++;
        }

        return dictionary;
    }

    public static string GetSelectionsName(Dictionary<string, string> selectionsNamesDictionary, string globalId)
    {
        return selectionsNamesDictionary.TryGetValue(globalId, out var name)
            ? name
            : null;
    }

    public SelectorConfig ConvertFromEzData(IReadOnlyCollection<EzSelectorData> ezData, ICollection<WidgetProduct> products)
    {
        var selectors = _selectors.GetAll();
        var firstData = ezData.FirstOrDefault(d => selectors.Any(s => s.Id == d.GlobalId));

        if (firstData == null)
            return null;

        var selectionsNamesDictionary = GetSelectionsNamesDictionary(firstData);
        var existingReferences = new List<Reference>();

        return new SelectorConfig
        {
            Technology = "EZ",
            GlobalId = firstData.GlobalId,
            Country = firstData.Country,
            Language = firstData.Language,
            Software = firstData.Software,
            Version = firstData.Version,

            SchemaVersion = "1.0",

            Data = new SelectorConfigData
            {
                Selections = new Selections
                {
                    Name = "_main_",
                    GlobalId = firstData.GlobalId,
                    Version = firstData.Version,

                    IsComplete = false,

                    State = new SelectionState
                    {
                        Criterias = GetCriterias(firstData.Criterias),
                        References = GetReferences(firstData.Docs, products, existingReferences),
                        Children = ezData
                            .Where(d => d != firstData)
                            .Select((d, index) => new SelectorConfigData
                            {
                                Selections = new Selections
                                {
                                    Name = GetSelectionsName(selectionsNamesDictionary, d.GlobalId),
                                    GlobalId = d.GlobalId,
                                    Version = d.Version,

                                    IsComplete = false,

                                    State = new SelectionState
                                    {
                                        Criterias = GetCriterias(d.Criterias),
                                        References = GetReferences(d.Docs, products, existingReferences),
                                        Children = new List<SelectorConfigData>()
                                    }
                                }
                            })
                            .ToList()
                    }
                }
            }
        };
    }
}