using System.Collections.Generic;
using System.Linq;

namespace CoLa.BimEd.ExtApi.Se.Services;

public class SelectorTranslations
{
    public Dictionary<string, List<Translation>> CharacteristicTranslations { get; } = new();
    public Dictionary<string, List<Translation>> ValueTranslations { get; } = new();

    public void AddCharacteristicTranslation(string id, string culture, string value) =>
        AddTranslation(CharacteristicTranslations, id, culture, value);

    public void AddValueTranslation(string id, string culture, string value) =>
        AddTranslation(ValueTranslations, id, culture, value);

    public string GetCharacteristicTranslation(string id, string culture) =>
        GetTranslation(CharacteristicTranslations, id, culture);

    public string GetValueTranslation(string id, string culture) =>
        GetTranslation(ValueTranslations, id, culture);

    private static void AddTranslation(IDictionary<string, List<Translation>> translations, string id, string culture, string value)
    {
        if (translations.TryGetValue(id, out var values))
        {
            var translation = values.FirstOrDefault(n => n.Culture == culture);

            if (translation == null)
            {
                values.Add(new Translation
                {
                    Culture = culture,
                    Value = value,
                });
            }
            else
            {
                translation.Value = value;
            }
        }
        else
        {
            translations.Add(id, new List<Translation> { new() { Culture = culture, Value = value, }});
        }
    }

    private static string GetTranslation(IDictionary<string, List<Translation>> translations, string id, string culture)
    {
        return translations.TryGetValue(id, out var translation)
            ? translation.FirstOrDefault(n => n.Culture == culture)?.Value
            : null;
    }

    public class Translation
    {
        public string Culture { get; set; }
        public string Value { get; set; }
    }
}