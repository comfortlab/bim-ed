﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoLa.BimEd.BusinessLogic.Model.Common;
using CoLa.BimEd.BusinessLogic.Model.Products;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Comparers;
using CoLa.BimEd.ExtApi.Se.Model.Catalog;
using CoLa.BimEd.ExtApi.Se.Model.Configurations;
using CoLa.BimEd.ExtApi.Se.Model.Configurations.Requests;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Resources;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.ProgressBar;
using Newtonsoft.Json;
using Product = CoLa.BimEd.BusinessLogic.Model.Products.Product;

namespace CoLa.BimEd.ExtApi.Se.Services;

public static partial class SeApi
{
    private const int CacheLifetimeDays = 30;

    public class Products
    {
        private readonly SeApiHttpClient _httpClient;
        private readonly Selectors _selectors;
        private readonly Converters _converters;
        private readonly Localization _localization;
        private readonly Translation _translation;
        private readonly ProgressBarController _progressBarController;
        private const string Description = "description";
        private const string JsonMediaType = "application/json";
        private const string Locale = "locale";

        public Products(
            SeApiHttpClient httpClient,
            Selectors selectors,
            Converters converters,
            Localization localization,
            Translation translation,
            ProgressBarController progressBarController)
        {
            _httpClient = httpClient;
            _selectors = selectors;
            _converters = converters;
            _localization = localization;
            _translation = translation;
            _progressBarController = progressBarController;
        }
        
        public async Task<string> GetDescription(string reference)
        {
            if (reference == null)
                return null;
            
            var culture = _localization.Culture;
            var url = $"{EndPoints.Product}/{reference}/{Description}/{Locale}/{culture}";
            var response = await _httpClient.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var catalogProduct = JsonConvert.DeserializeObject<CatalogProductWrapper>(json);
            var translations = catalogProduct.CatalogProduct.LongDescription.Translation;

            return translations.TryGetValue(culture, out var description)
                ? description
                : translations.FirstOrDefault().Value ?? string.Empty;
        }

        public async Task<Bitmap> GetImage(string reference)
        {
            if (reference == null)
                return null;

            var url = $"{EndPoints.ProductImage}/{reference}";
            var response = await _httpClient.GetAsync(url);
            var bytes = await response.Content.ReadAsByteArrayAsync();
            return bytes.ToImage();
        }

        public async Task<Product> GetProduct(string reference, string selectorId = null)
        {
            var productDataCachePath = DI.Get<AppPaths>().ProductDataCache;
            var productImageCachePath = Path.ChangeExtension(Path.Combine(productDataCachePath, reference), "png");
            var productCachePath = Path.ChangeExtension(Path.Combine(productDataCachePath, reference), "json");
            var productCacheExists = File.Exists(productCachePath);

            if (productCacheExists)
            {
                var productCacheFileInfo = new FileInfo(productCachePath);
                var lastWriteTime = productCacheFileInfo.LastWriteTime;
                var sinceLastWriteTime = DateTime.Now - lastWriteTime;

                if (sinceLastWriteTime.Days < CacheLifetimeDays)
                {
                    var json = File.ReadAllText(productCachePath, Encoding.UTF8);
                    var productFromFile = JsonConvert.DeserializeObject<Product>(json);

                    var bitmap = new Bitmap(productImageCachePath);
                    productFromFile.Image = bitmap;

                    return productFromFile;
                }
            }

            var catalogProduct = await GetCatalogProduct(reference);
            var product = await GetProduct(catalogProduct, selectorId);

            var productJson = JsonConvert.SerializeObject(product).Replace("\"Image\":\"System.Windows.Interop.InteropBitmap\"", "\"Image\":null");
            File.WriteAllText(productCachePath, productJson, Encoding.UTF8);

            var productBitmap = product.Image;
            productBitmap.Save(productImageCachePath, ImageFormat.Png);

            return product;
        }

        public async Task<Product> GetProduct(
            CatalogProduct catalogProduct,
            string selectorId = null,
            List<string> references = null)
        {
            var groups = catalogProduct?.CharacteristicGroupList.CharacteristicGroups;
            var selector = selectorId != null ? _selectors.GetById(selectorId) : null;

            var mainGroup = groups?.FirstOrDefault(n =>
                n.Label == CharacteristicsGroup.Main);
            var sizeGroup = groups?.FirstOrDefault(n =>
                n.CharacteristicList.Characteristics.Any(characteristic => characteristic.Id is Characteristics.WidthA or Characteristics.WidthD));
            var culture = _localization.Culture;
            var image = await GetImage(catalogProduct?.ProductId);

            var product = new Product(catalogProduct?.Id)
            {
                Count = references?.Where(r => r == catalogProduct?.ProductId).Count() ?? 1,
                Name = GetValue(mainGroup, Characteristics.ProductName, culture),
                ProductType = GetValue(mainGroup, Characteristics.ProductOrComponentType, culture),
                ShortName = GetValue(mainGroup, Characteristics.DeviceShortName, culture),
                Description = GetDescription(catalogProduct, culture),
                Image = image,
                RangeOfProduct = GetValue(mainGroup, Characteristics.Range, culture),
                Dimensions = Dimensions.CreateFromMillimeters(
                    heightMillimeters: (GetValue(sizeGroup, Characteristics.HeightA, culture) ?? GetValue(sizeGroup, Characteristics.HeightD, culture))?.FirstDouble() ?? default,
                    widthMillimeters: (GetValue(sizeGroup, Characteristics.WidthA, culture) ?? GetValue(sizeGroup, Characteristics.WidthD, culture))?.FirstDouble() ?? default,
                    depthMillimeters: (GetValue(sizeGroup, Characteristics.DepthA, culture) ?? GetValue(sizeGroup, Characteristics.DepthD, culture))?.FirstDouble() ?? default),
                Weight = GetValue(sizeGroup, Characteristics.ProductWeight, culture).FirstDouble(defaultValue: 0),
            };

            if (groups != null && selector != null)
            {
                var characteristics = groups
                    .SelectMany(g => g.CharacteristicList.Characteristics)
                    .Where(c => selector.UsedCharacteristics.Contains(c.Id));

                foreach (var characteristic in characteristics)
                {
                    product.Characteristics.Add(
                        characteristic.Id,
                        new ProductCharacteristic(characteristic.Id, characteristic.ValueIds.FirstOrDefault()?.Id));
                }
            }

            SetShortName(product, selector);

            return product;
        }

        public async Task<ProductRange> GetProductRanges(string selectorId)
        {
            var allSelectorsCachePath = DI.Get<AppPaths>().SelectorCache;
            var selectorCachePath = Path.ChangeExtension(Path.Combine(allSelectorsCachePath, selectorId), "json");
            var selectorCacheExists = File.Exists(selectorCachePath);

            if (selectorCacheExists)
            {
                var selectorCacheFileInfo = new FileInfo(selectorCachePath);
                var lastWriteTime = selectorCacheFileInfo.LastWriteTime;
                var sinceLastWriteTime = DateTime.Now - lastWriteTime;

                if (sinceLastWriteTime.Days < CacheLifetimeDays)
                {
                    var json = File.ReadAllText(selectorCachePath, Encoding.UTF8);
                    return JsonConvert.DeserializeObject<ProductRange>(json);
                }
            }

            var catalogProducts = await GetCatalogProducts(selectorId);
            var productRange = GetProductRange(selectorId, catalogProducts);

            var catalogProductsJson = JsonConvert.SerializeObject(productRange);
            File.WriteAllText(selectorCachePath, catalogProductsJson, Encoding.UTF8);

            return productRange;
        }

        public ProductRange GetProductRange(string selectorId, List<CatalogProduct> catalogProducts)
        {
            if (catalogProducts == null)
                return new ProductRange(new List<BaseProduct>());

            var selector = _selectors.GetById(selectorId);
            var selectionCharacteristics = selector.SelectionCharacteristics;

            AddCharacteristicTranslations(catalogProducts, selectionCharacteristics);

            var products = catalogProducts.Select(n =>
            {
                var product = new BaseProduct(n.Id);

                product.AddCharacteristics(GetCharacteristics(n)
                    .Where(c => selector.UsedCharacteristics.Contains(c.Id))
                    .Select(c => new ProductCharacteristic(c.Id, c.ValueIds.FirstOrDefault()?.Id)));

                return product;

            }).ToList();

            var productRange = new ProductRange(products);

            foreach (var selectionCharacteristic in selectionCharacteristics)
            {
                var values = products
                    .SelectMany(n => n.Characteristics)
                    .Where(n => n.Key == selectionCharacteristic)
                    .Select(n => n.Value.Value)
                    .Distinct()
                    .ToList();

                productRange.CharacteristicValues.Add(selectionCharacteristic, values);
            }

            return productRange;
        }

        private void AddCharacteristicTranslations(IReadOnlyCollection<CatalogProduct> catalogProducts, List<string> selectionCharacteristics)
        {
            if (catalogProducts.IsEmpty())
                return;

            var allCharacteristics = catalogProducts.SelectMany(n => n.CharacteristicGroupList.CharacteristicGroups)
                .SelectMany(n => n.CharacteristicList.Characteristics)
                .Where(n => selectionCharacteristics.Contains(n.Id))
                .ToList();
            var characteristics = allCharacteristics
                .Distinct(new CatalogCharacteristicComparer())
                .ToList();
            var values = allCharacteristics
                .SelectMany(n => n.ValueIds)
                .Distinct(new ValueIdComparer());

            foreach (var characteristic in characteristics)
            {
                foreach (var translation in characteristic.Translation)
                {
                    _translation.Translations.AddCharacteristicTranslation(
                        characteristic.Id,
                        translation.Key,
                        translation.Value);
                }
            }

            foreach (var value in values)
            {
                foreach (var translation in value.Translation)
                {
                    _translation.Translations.AddValueTranslation(
                        value.Id,
                        translation.Key,
                        translation.Value);
                }
            }

            _translation.WriteTranslations();
        }

        public async Task<CatalogProduct> GetCatalogProduct(string reference, bool image = false, bool tabularDetails = false)
        {
            if (reference == null)
                return null;

            var culture = _localization.Culture;
            var url = $"{EndPoints.Product}/{reference}/{Locale}/{culture}?image={image}&tabularDetails={tabularDetails}";
            var response = await _httpClient.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var catalogProductWrapper = JsonConvert.DeserializeObject<CatalogProductWrapper>(json);

            return catalogProductWrapper.CatalogProduct;
        }

        public async Task<List<CatalogProduct>> GetCatalogProducts(string selectorId)
        {
            var selector = _selectors.GetById(selectorId);
            var selectorName = selector.Name;

            using var process = _progressBarController.StartProcess(2, $"{selectorName}: {ProgressBarDescriptions.Loading_SelectorData}", counter: false);

            var config = await GetSelectorConfigurations(selectorId);

            process.Next($"{selectorName}: {ProgressBarDescriptions.Loading_ProductData}");

            var references = config.Id != null
                ? config.CharacteristicGroups.FirstOrDefault()?
                    .Characteristics.FirstOrDefault(n => n.Id == Characteristics.Ref)?
                    .Values.Select(n => n.Value).ToList() ?? new List<string>()
                : new List<string>();

            var catalogProducts = await GetCatalogProducts(references);

            process.Next();

            return catalogProducts;
        }

        public async Task<List<CatalogProduct>> GetCatalogProducts(List<string> references, bool image = false, bool tabularDetails = false)
        {
            if (references == null || references.IsEmpty())
                return null;
            
            var culture = _localization.Culture;
            var url = $"{EndPoints.Product}/{Locale}/{culture}?image={image}&tabularDetails={tabularDetails}";
            var request = new CatalogProductsRequest { RefIds = references };
            var jsonRequest = JsonConvert.SerializeObject(request);
            var requestContent = new StringContent(jsonRequest, Encoding.UTF8, JsonMediaType);
            var response = await _httpClient.PostAsync(url, requestContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var catalogProductData = JsonConvert.DeserializeObject<IEnumerable<CatalogProductWrapper>>(jsonResponse);

            return catalogProductData.Select(n => n.CatalogProduct).ToList();
        }

        public async Task<Configuration> GetSelectorConfigurations(string selectorId, string commRef = null)
        {
            if (selectorId == null && commRef == null)
                return null;

            var request = new ConfigurationKbRequest
            {
                CommRef = commRef,
                KbName = selectorId,
                KbLang = _localization.LanguageCode,
                Country = _localization.CountryCode,
                Project = SelectorConfigs.Software,
            };

            var jsonRequest = JsonConvert.SerializeObject(request);
            var requestContent = new StringContent(jsonRequest, Encoding.UTF8, JsonMediaType);
            var response = await _httpClient.PostAsync(EndPoints.Configurations, requestContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Configuration>(jsonResponse);
        }

        public async Task<SelectorProduct> GetSelectorProduct(string selectorId, string reference)
        {
            var product = await GetProduct(reference, selectorId);

            return new SelectorProduct
            {
                Product = product,
                SelectorId = selectorId,
                References = new List<string> { reference },
                Accessories = new List<Product>(),
                SerializedConfiguration = _converters.ConvertToConfig(selectorId, reference).Serialize(),
            };
        }

        private static string GetDescription(CatalogProduct catalogProduct, string culture)
        {
            var translations = catalogProduct?.LongDescription.Translation;

            return GetValue(translations, culture);
        }

        public static IEnumerable<CatalogCharacteristic> GetCharacteristics(CatalogProduct catalogProduct)
        {
            return catalogProduct
                .CharacteristicGroupList
                .CharacteristicGroups
                .SelectMany(n => n.CharacteristicList.Characteristics);
        }

        public static ProductCharacteristic GetProductCharacteristic(CatalogCharacteristic characteristic)
        {
            var value = characteristic.ValueIds.FirstOrDefault()?.Id;
            return new ProductCharacteristic(characteristic.Id, value);
        }

        private static void SetShortName(Product product, Selector selector)
        {
            if (selector == null)
                return;

            var shortNameCharacteristics = selector.ShortNameCharacteristics;

            if (selector.SelectorGroups.Count == 1 && selector.SelectorGroups.Contains(SelectorGroup.Enclosures))
            {
                var numberOfHorizontalRows = product.GetValue(shortNameCharacteristics[0]);
                var numberOfModulesPerRows = product.GetValue(shortNameCharacteristics[1]);

                product.ShortName = $"{numberOfHorizontalRows} × {numberOfModulesPerRows}";
            }

            else
            {
                var characteristicsString = shortNameCharacteristics
                    .Select(key =>
                    {
                        var characteristicType = Characteristics.GetCharacteristicType(key);
                        return product.GetCharacteristic(key)?.GetValueAs(characteristicType)?.ToString().Replace(" ", "");
                    })
                    .Where(s => !string.IsNullOrWhiteSpace(s))
                    .JoinToString(" ");

                var shortName = product.ShortName ?? product.Name;

                product.ShortName = $"{shortName} {characteristicsString}";
            }
        }

        public static string GetValue(CatalogCharacteristicGroup characteristicGroup, string characteristicsId, string culture)
        {
            var translations = characteristicGroup?
                .CharacteristicList
                .Characteristics
                .FirstOrDefault(c => c.Id == characteristicsId)?
                .ValueIds
                .FirstOrDefault()?
                .Translation;

            return GetValue(translations, culture);
        }

        private static string GetValue(IReadOnlyDictionary<string, string> translations, string culture)
        {
            if (translations == null || translations.IsEmpty())
                return null;

            return translations.TryGetValue(culture, out var translation)
                ? translation
                : translations.FirstOrDefault().Value;
        }

        public async Task Restore(Product product, string selectorId)
        {
            var prototype = await GetProduct(product.Reference, selectorId);
            product.PullProperties(prototype);
        }

        public static void ResetCache()
        {
            var allSelectorsCachePath = DI.Get<AppPaths>().SelectorCache;
            var folderInfo = new DirectoryInfo(allSelectorsCachePath);
            var allFiles = folderInfo.GetFiles();

            foreach (var file in allFiles)
                File.Delete(file.FullName);
        }
    }
}