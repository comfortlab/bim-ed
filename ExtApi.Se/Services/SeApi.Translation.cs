﻿using System.IO;
using System.Text;
using CoLa.BimEd.Infrastructure.Framework.Application;
using Newtonsoft.Json;

namespace CoLa.BimEd.ExtApi.Se.Services;

public static partial class SeApi
{
    public class Translation
    {
        private readonly string _selectorTranslationsPath;
        private const string TranslationsJson = "Translations.json";
            
        public SelectorTranslations Translations { get; set; }

        public Translation(AppPaths appPaths)
        {
            _selectorTranslationsPath = Path.Combine(appPaths.SelectorCache, TranslationsJson);;
            ReadTranslations();
        }

        public void ReadTranslations()
        {
            if (File.Exists(_selectorTranslationsPath))
            {
                var selectorTranslationsJson = File.ReadAllText(_selectorTranslationsPath, Encoding.UTF8);
                Translations = JsonConvert.DeserializeObject<SelectorTranslations>(selectorTranslationsJson);
            }
            else
            {
                Translations = new SelectorTranslations();
            }
        }

        public void WriteTranslations()
        {
            var selectorTranslationsJson = JsonConvert.SerializeObject(Translations);
            File.WriteAllText(_selectorTranslationsPath, selectorTranslationsJson, Encoding.UTF8);
        }
    }
}