﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Products;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors.CriteriaConverters;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Resources;
using CoLa.BimEd.Infrastructure.Framework;
using CoLa.BimEd.Infrastructure.Framework.Collections;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Extensions;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.ProgressBar;

// ReSharper disable MemberHidesStaticFromOuterClass

namespace CoLa.BimEd.ExtApi.Se.Services;

public static partial class SeApi
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class Selectors
    {
        public const string Acti9_C120 = "KB_GCR_MCB_C120";
        public const string Acti9_iC60 = "KB_GCR_MCB_IC60";
        public const string Acti9_iCT = "KB_GCR_501B";
        public const string Acti9_iDPNVigi = "KB_GCR_MCB_PH_N";
        public const string Acti9_iDPNVigi_INT = "KB_GCR_MCB_PH_N.IDPN";
        public const string Acti9_iEM3000 = "DE_SOL_IEM3000";
        public const string Acti9_iK60 = "KB_GCR_MCB_IK60";
        public const string Acti9_iSW = "KB_GCR_SD_ISW";
        public const string Acti9_NG125 = "KB_GCR_MCB_NG125";
        public const string Altistart_01 = "ATS01";
        public const string Altistart_22 = "ATS22";
        public const string Altistart_48 = "ATS48";
        public const string Altivar_600 = "Altivar 600";
        public const string Altivar_900 = "Altivar 900";
        public const string ComPact_NSX = "KB_GCR_NSX100_630";
        public const string ComPact_NSXm = "KB_GCR_2042";
        public const string ComPact_NSXm_NA = "KB_GCR_2043";
        public const string MasterPact_MTZ1_CB = "KB_GCR_MTZ1_CB";
        public const string MasterPact_MTZ2_CB = "KB_GCR_MTZ2_CB";
        public const string MasterPact_MTZ3_CB = "KB_GCR_MTZ3_CB";
        public const string MasterPact_MTZ1_SD = "KB_GCR_MTZ1_SD";
        public const string MasterPact_MTZ2_SD = "KB_GCR_MTZ2_SD";
        public const string MasterPact_MTZ3_SD = "KB_GCR_MTZ3_SD";
        public const string Modicon_M221 = "Modicon M221";
        public const string Modicon_M241 = "Modicon M241";
        public const string Modicon_M340 = "Modicon M340";
        public const string Modicon_M580 = "Modicon M580";
        public const string Modicon_TM3 = "Modicon TM3";
        public const string Modicon_X80 = "Modicon X80";
        public const string PowerLogic_PM3200 = "KB_GCR_701";
        public const string Pragma = "PRAGMA";
        public const string Kaedra = "KAEDRA";

        private readonly IServiceProvider _serviceProvider;
        private readonly Localization _localization;
        private readonly ProgressBarController _progressBarController;
        private readonly Logger _logger;
        private readonly Selector[] _selectors;
        private readonly SortedDictionary<string, Selector> SelectorsDictionary;

        private Products _products;
        private Products Products => _products ??= _serviceProvider.Get<Products>();

        public Selectors(
            IServiceProvider serviceProvider,
            Localization localization,
            ProgressBarController progressBarController,
            Logger logger)
        {
            _serviceProvider = serviceProvider;
            _localization = localization;
            _progressBarController = progressBarController;
            _logger = logger;

            SelectorsDictionary = new SortedDictionary<string, Selector>();

            try
            {
                SelectorsDictionary.AddRange(SelectorFactory.Acti9CircuitBreakers());
                SelectorsDictionary.AddRange(SelectorFactory.Acti9Contactors());
                SelectorsDictionary.AddRange(SelectorFactory.Acti9SwitchDisconnectors());
                SelectorsDictionary.AddRange(SelectorFactory.Acti9EnergyMeters());
                SelectorsDictionary.AddRange(SelectorFactory.AltistartSoftStarters());
                SelectorsDictionary.AddRange(SelectorFactory.AltivarVariableSpeedDrivers());
                SelectorsDictionary.AddRange(SelectorFactory.ComPactCircuitBreakers());
                SelectorsDictionary.AddRange(SelectorFactory.ComPactSwitchDisconnectors());
                SelectorsDictionary.AddRange(SelectorFactory.MasterPactCircuitBreakers());
                SelectorsDictionary.AddRange(SelectorFactory.MasterPactSwitchDisconnectors());
                SelectorsDictionary.AddRange(SelectorFactory.PlcModicon());
                SelectorsDictionary.AddRange(SelectorFactory.PowerLogicMeters());
                SelectorsDictionary.AddRange(SelectorFactory.PragmaEnclosures());
                SelectorsDictionary.AddRange(SelectorFactory.KaedraEnclosures());

            }
            catch (Exception exception)
            {
                _logger.Error(exception);
            }
            _selectors = SelectorsDictionary.Values.ToArray();
        }

        public Selector[] GetAll(bool onlyCurrentCulture = true)
        {
            return onlyCurrentCulture
                ? _selectors
                    .Where(n => n.Localizations.Contains(_localization.Culture))
                    .ToArray()
                : _selectors;
        }

        public Selector GetById(string selectorId) => SelectorsDictionary.TryGetValue(selectorId, out var selector) ? selector : null;

        public IEnumerable<Selector> GetApplicability<T>(params SelectorGroup[] selectorGroups)
            where T : ElectricalBase
        {
            var selectors = GetSelectors(selectorGroups);

            return selectors.Where(n => n.Applicability.Contains(typeof(T)));
        }

        public IEnumerable<Selector> GetApplicability(Type targetType, params SelectorGroup[] selectorGroups)
        {
            var selectors = GetSelectors(selectorGroups);

            return selectors.Where(n => n.Applicability.Contains(targetType));
        }

        public IEnumerable<Selector> GetSelectors(params SelectorGroup[] selectorGroups)
        {
            var allSelectors = GetAll();

            return selectorGroups.Any()
                ? allSelectors.Where(n => selectorGroups.Intersect(n.SelectorGroups).Any())
                : allSelectors;
        }

        public IEnumerable<Selector> GetSelectorsById(string selectorId)
        {
            var allSelectors = GetAll();

            return allSelectors.Where(s => s.Id == selectorId);
        }

        public async Task VerifyProducts(IReadOnlyCollection<LoadClassificationProxy> loadClassifications)
        {
            var invalidLoadClassifications = loadClassifications
                .Where(n =>
                    n.CircuitSelectAndConfigSetting.AllSetting.SelectMany(s => s).Any(IsInvalidSetting) ||
                    n.PanelSelectAndConfigSetting.AllSetting.SelectMany(s => s).Any(IsInvalidSetting))
                .ToList();

            if (invalidLoadClassifications.IsEmpty())
                return;

            try
            {
                _progressBarController.Title = ProgressBarDescriptions.Verification;
                using var process = _progressBarController.StartProcess(invalidLoadClassifications.Count);

                foreach (var loadClassification in invalidLoadClassifications)
                {
                    process.Next(loadClassification.Name);

                    await Verify(loadClassification.CircuitSelectAndConfigSetting);
                    await Verify(loadClassification.PanelSelectAndConfigSetting);
                }
            }
            catch (TaskCanceledException)
            {
                _logger.Warn($"<{typeof(Selectors)}.{nameof(VerifyProducts)}> was canceled");
            }
        }

        private static bool IsInvalidSetting(SelectorProductSetting s)
        {
            return s.ProductRange.Products.IsEmpty() ||
                   s.MinCurrent.IsEqualToZero() ||
                   s.MaxCurrent.IsEqualToZero();
        }

        public async Task Verify<T>(SelectAndConfigSetting<T> selectAndConfigSetting)
            where T : ElectricalBase
        {
            foreach (var productSettings in selectAndConfigSetting.AllSetting)
            {
                await Verify(productSettings);
            }
        }

        public async Task Verify(SelectorProductSettings productSettings)
        {
            var invalidProductSettings = productSettings.Where(n =>
                    n.ProductRange.Products.IsEmpty() ||
                    n.MinCurrent.IsEqualToZero() ||
                    n.MaxCurrent.IsEqualToZero())
                .ToList();

            if (invalidProductSettings.IsEmpty())
                return;

            foreach (var invalidProductSetting in invalidProductSettings)
            {
                var selector = GetById(invalidProductSetting.SelectorId);

                var productRange = await Products.GetProductRanges(selector.Id);
                var newProductSetting = new SelectorProductSetting(selector.Id, productRange, selector.MinCurrent, selector.MaxCurrent);
                newProductSetting.ProductRange.PullSelectedValues(invalidProductSetting.ProductRange);
                productSettings.Remove(invalidProductSetting);
                productSettings.Add(newProductSetting);
            }
        }

        private static class SelectorFactory
        {
            public static IEnumerable<KeyValuePair<string, Selector>> Acti9CircuitBreakers() => new[]
            {
                Acti9.C120(),
                Acti9.iC60(),
                Acti9.iDPNVigi(),
                Acti9.iDPNVigiInt(),
                Acti9.iK60(),
                Acti9.NG125(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> Acti9Contactors() => new[]
            {
                Acti9.iCT(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> Acti9EnergyMeters() => new[]
            {
                Acti9.iEM3000(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> Acti9SwitchDisconnectors() => new[]
            {
                Acti9.iSW(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> AltistartSoftStarters() => new[]
            {
                Altistart.Altistart01(),
                Altistart.Altistart22(),
                Altistart.Altistart48(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> AltivarVariableSpeedDrivers() => new[]
            {
                Altivar.Altivar600(),
                Altivar.Altivar900(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> ComPactCircuitBreakers() => new[]
            {
                ComPact.NSX(),
                ComPact.NSXm(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> ComPactSwitchDisconnectors() => new[]
            {
                ComPact.NSXm_NA(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> MasterPactCircuitBreakers() => new[]
            {
                MasterPact.MasterPact_MTZ1_CB(),
                MasterPact.MasterPact_MTZ2_CB(),
                MasterPact.MasterPact_MTZ3_CB(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> MasterPactSwitchDisconnectors() => new[]
            {
                MasterPact.MasterPact_MTZ1_SD(),
                MasterPact.MasterPact_MTZ2_SD(),
                MasterPact.MasterPact_MTZ3_SD(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> PlcModicon() => new[]
            {
                Modicon.M221(),
                Modicon.M241(),
                Modicon.M340(),
                Modicon.M580(),
                Modicon.TM3(),
                Modicon.X80(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> PowerLogicMeters() => new[]
            {
                PowerLogic.PM3200(),
            };

            public static IEnumerable<KeyValuePair<string, Selector>> PragmaEnclosures() => new[]
            {
                Pragma()
            };

            public static IEnumerable<KeyValuePair<string, Selector>> KaedraEnclosures() => new[]
            {
                Kaedra()
            };

            private static class Acti9
            {
                internal static KeyValuePair<string, Selector> C120() => new(
                    Acti9_C120,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_C120,
                        Name = "Acti9 C120",
                        Description = SelectorDescriptions.MCB_125,
                        Image = SelectorImages.Acti9_C120,
                        DefaultConfiguration = DefaultConfigurations.Acti9_C120,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.RuRu,
                        },
                        MinCurrent = 63,
                        MaxCurrent = 125,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.RatedCurrent,
                            Characteristics.CurveCode,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.PolesDescription,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        },
                        UsedCriteria = new Dictionary<string, string>
                        {
                            // [SeApi.Criteria.T_BreakingCapacity] = null,
                            // [SeApi.Criteria.T_CurveCode] = null,
                            // [SeApi.Criteria.T_NetworkType] = null,
                            // [SeApi.Criteria.T_PolesDescription] = null,
                            // [SeApi.Criteria.T_RatedCurrent] = null
                        },
                    });

                internal static KeyValuePair<string, Selector> iC60() => new(
                    Acti9_iC60,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iC60,
                        Name = "Acti9 iC60",
                        Description = SelectorDescriptions.MCB_63,
                        Image = SelectorImages.Acti9_iC60,
                        DefaultConfiguration = DefaultConfigurations.Acti9_iC60,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.RuRu,
                        },
                        MinCurrent = 05,
                        MaxCurrent = 63,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.CurveCode,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });

                internal static KeyValuePair<string, Selector> iCT() => new(
                    Acti9_iCT,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iCT,
                        Name = "Acti9 iCT",
                        Description = SelectorDescriptions.Acti9_iCT,
                        Image = SelectorImages.Acti9_iCT,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.Contactors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 6,
                        MaxCurrent = 100,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CommonCriteriaConverter(
                                Criteria.T_ControlType,
                                Values.ControlType_RemoteControl),
                            new CommonCriteriaConverter(
                                Criteria.T_UtilisationCategory,
                                Values.UtilisationCategory_AC7A),
                            new CurrentCriteriaConverter(Criteria.T_RatedOperationalCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.ControlCircuitVoltage,
                            Characteristics.ControlType,
                            Characteristics.NetworkFrequency,
                            Characteristics.PolesDescription,
                            Characteristics.RatedOperationalCurrent,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.ControlType,
                            Characteristics.NetworkFrequency,
                            Characteristics.ControlCircuitVoltage,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedOperationalCurrent,
                            Characteristics.PolesDescription,
                            Characteristics.ControlCircuitVoltage,
                        }
                    });

                internal static KeyValuePair<string, Selector> iDPNVigi() => new(
                    Acti9_iDPNVigi,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iDPNVigi,
                        Name = "Acti9 iDPN Vigi",
                        Description = SelectorDescriptions.RCBO_40,
                        Image = SelectorImages.Acti9_iDPNVigi,
                        DefaultConfiguration = DefaultConfigurations.Acti9_iDPNVigi,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.RuRu,
                        },
                        MinCurrent = 4,
                        MaxCurrent = 40,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_EarthLeakageProtectionClass,
                                Values.EarthLeakageProtectionClass_AC),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new EarthLeakageSensitivityConverter(),
                            new PolesPlusNeutralDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.RatedCurrent,
                            Characteristics.CurveCode,
                            Characteristics.EarthLeakageProtectionClass,
                            Characteristics.EarthLeakageSensitivity,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.PolesDescription,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.EarthLeakageProtectionClass,
                            Characteristics.EarthLeakageSensitivity,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });

                internal static KeyValuePair<string, Selector> iDPNVigiInt() => new(
                    Acti9_iDPNVigi_INT,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iDPNVigi_INT,
                        Name = "Acti9 iDPN Vigi",
                        Description = SelectorDescriptions.RCBO_40,
                        Image = SelectorImages.Acti9_iDPNVigi,
                        DefaultConfiguration = DefaultConfigurations.Acti9_iDPNVigi,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        MinCurrent = 4,
                        MaxCurrent = 40,
                        Localizations =
                        {
                            Localization.EnInt,
                        },
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_EarthLeakageProtectionClass,
                                Values.EarthLeakageProtectionClass_AC),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new EarthLeakageSensitivityConverter(),
                            new PolesPlusNeutralDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.RatedCurrent,
                            Characteristics.CurveCode,
                            Characteristics.EarthLeakageProtectionClass,
                            Characteristics.EarthLeakageSensitivity,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.PolesDescription,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.EarthLeakageProtectionClass,
                            Characteristics.EarthLeakageSensitivity,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });

                internal static KeyValuePair<string, Selector> iK60() => new(
                    Acti9_iK60,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iK60,
                        Name = "Acti9 iK60",
                        Description = SelectorDescriptions.MCB_63,
                        Image = SelectorImages.Acti9_iK60,
                        DefaultConfiguration = DefaultConfigurations.Acti9_iK60,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 6,
                        MaxCurrent = 63,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.CurveCode,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.PolesDescription,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.RatedCurrent,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });

                internal static KeyValuePair<string, Selector> iEM3000() => new(
                    Acti9_iEM3000,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Acti9_iEM3000,
                        Name = "Acti9 iEM3000",
                        Description = SelectorDescriptions.Acti9_iEM3000,
                        Image = SelectorImages.Acti9_iEM3000,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.EnergyMetering,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 0,
                        MaxCurrent = 125,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            // new BreakingCapacityConverter(),
                            // new NetworkTypeConverter(),
                            // new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCriteria = new Dictionary<string, string>
                        {
                            // [Selectors.Criteria.T_CurveCode] = null,
                            // [Selectors.Criteria.T_PolesDescription] = null,
                            // [Selectors.Criteria.T_RatedOperationalCurrent] = null,
                            // [Selectors.Criteria.T_RatedCurrent] = null
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            // Selectors.Criteria.T_CurveCode,
                            // Selectors.Criteria.T_RatedCurrent,
                            // Selectors.Criteria.T_PolesDescription
                        }
                    });

                internal static KeyValuePair<string, Selector> iSW() => new(
                    Acti9_iSW,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_iSW,
                        Name = "Acti9 iSW",
                        Description = SelectorDescriptions.SD_125,
                        Image = SelectorImages.Acti9_iSW,
                        DefaultConfiguration = DefaultConfigurations.Acti9_iSW,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SwitchDisconnectors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 20,
                        MaxCurrent = 125,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedOperationalCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.PolesDescription,
                            Characteristics.RatedMakingCapacity,
                            Characteristics.RatedOperationalCurrent,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.PolesDescription,
                            Characteristics.RatedOperationalCurrent,
                        }
                    });

                internal static KeyValuePair<string, Selector> NG125() => new(
                    Acti9_NG125,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Acti9_NG125,
                        Name = "Acti9 NG125",
                        Description = SelectorDescriptions.MCB_125,
                        Image = SelectorImages.Acti9_NG125,
                        DefaultConfiguration = DefaultConfigurations.Acti9_NG125,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.RuRu,
                        },
                        MinCurrent = 10,
                        MaxCurrent = 125,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new BreakingCapacityConverter(),
                            new CommonCriteriaConverter(
                                Criteria.T_CurveCode,
                                Values.CurveCode_C),
                            new CommonCriteriaConverter(
                                Criteria.T_NetworkType,
                                Values.NetworkType_AC),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.CurveCode,
                            Characteristics.MagneticTrippingLimit,
                            Characteristics.NetworkType,
                            Characteristics.NumberOfProtectedPoles,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                        },
                        SelectionCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.CurveCode,
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });
            }

            private static class Altistart
            {
                internal static KeyValuePair<string, Selector> Altistart01() => new(
                    Altistart_01,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Altistart_01,
                        Name = "Altistart 01",
                        Description = SelectorDescriptions.Altistart_01,
                        Image = SelectorImages.Altistart_01,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SoftStarters,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.MountingMode,
                            Characteristics.NetworkNumberOfPhases,
                            Characteristics.HeightD,
                            Characteristics.WidthD,
                            Characteristics.DepthD,
                            Characteristics.ProductWeight,
                        },
                    });

                internal static KeyValuePair<string, Selector> Altistart22() => new(
                    Altistart_22,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Altistart_22,
                        Name = "Altistart 22",
                        Description = SelectorDescriptions.Altistart_22,
                        Image = SelectorImages.Altistart_22,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SoftStarters,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.MountingMode,
                            Characteristics.NetworkNumberOfPhases,
                            Characteristics.HeightD,
                            Characteristics.WidthD,
                            Characteristics.DepthD,
                            Characteristics.ProductWeight,
                        },
                    });

                internal static KeyValuePair<string, Selector> Altistart48() => new(
                    Altistart_48,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Altistart_48,
                        Name = "Altistart 48",
                        Description = SelectorDescriptions.Altistart_48,
                        Image = SelectorImages.Altistart_48,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SoftStarters,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.MountingMode,
                            Characteristics.NetworkNumberOfPhases,
                            Characteristics.HeightD,
                            Characteristics.WidthD,
                            Characteristics.DepthD,
                            Characteristics.ProductWeight,
                        },
                    });
            }

            private static class Altivar
            {
                internal static KeyValuePair<string, Selector> Altivar600() => new(
                    Altivar_600,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Altivar_600,
                        Name = "Altivar Process ATV600",
                        Description = SelectorDescriptions.Altivar_600,
                        Image = SelectorImages.Altivar,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.VariableSpeedDrivers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.MountingMode,
                            Characteristics.NetworkNumberOfPhases,
                            Characteristics.HeightD,
                            Characteristics.WidthD,
                            Characteristics.DepthD,
                            Characteristics.ProductWeight,
                        },
                    });

                internal static KeyValuePair<string, Selector> Altivar900() => new(
                    Altivar_900,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Altivar_900,
                        Name = "Altivar Process ATV900",
                        Description = SelectorDescriptions.Altivar_900,
                        Image = SelectorImages.Altivar,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.VariableSpeedDrivers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.MountingMode,
                            Characteristics.NetworkNumberOfPhases,
                            Characteristics.HeightD,
                            Characteristics.WidthD,
                            Characteristics.DepthD,
                            Characteristics.ProductWeight,
                        },
                    });
            }

            private static class ComPact
            {
                internal static KeyValuePair<string, Selector> NSX() => new(
                    ComPact_NSX,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = ComPact_NSX,
                        Name = "ComPact NSX",
                        Description = SelectorDescriptions.CB_630,
                        Image = SelectorImages.ComPact_NSX,
                        DefaultConfiguration = DefaultConfigurations.ComPact_NSX,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 100,
                        MaxCurrent = 630,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CommonCriteriaConverter(
                                Criteria.T_CircuitBreakerApplication,
                                Values.Distribution),
                            new CommonCriteriaConverter(
                                Criteria.YN_CombinedReference,
                                Values.No),
                            new CurrentCriteriaConverter(
                                Criteria.TA_RatedCurrent),
                            new CurrentCriteriaConverter(
                                Criteria.TA_TripUnitRating),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.CircuitBreakerRatingCode,
                            Characteristics.PolesDescription,
                            Characteristics.ProtectedPolesDescription,
                            Characteristics.RatedCurrent,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        }
                    });

                internal static KeyValuePair<string, Selector> NSXm() => new(
                    ComPact_NSXm,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = ComPact_NSXm,
                        Name = "ComPact NSXm",
                        Description = SelectorDescriptions.CB_160,
                        Image = SelectorImages.ComPact_NSXm,
                        DefaultConfiguration = DefaultConfigurations.ComPact_NSXm,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 16,
                        MaxCurrent = 160,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.BreakingCapacity,
                            Characteristics.ConnectionsTerminals,
                            Characteristics.PolesDescription,
                            Characteristics.ProtectedPolesDescription,
                            Characteristics.RatedCurrent,
                            Characteristics.TripUnitName,
                        },
                        SelectionCharacteristics =
                        {
                            Characteristics.TripUnitName,
                            Characteristics.ConnectionsTerminals,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        }
                    });

                internal static KeyValuePair<string, Selector> NSXm_NA() => new(
                    ComPact_NSXm_NA,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = ComPact_NSXm_NA,
                        Name = "ComPact NSXm NA",
                        Description = SelectorDescriptions.SD_160,
                        Image = SelectorImages.ComPact_NSXm_NA,
                        DefaultConfiguration = DefaultConfigurations.ComPact_NSXm_NA,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SwitchDisconnectors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 50,
                        MaxCurrent = 160,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CommonCriteriaConverter(
                                Criteria.TC_Connections,
                                Values.Connections_Everlink),
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.ConnectionsTerminals,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                            Characteristics.RatedMakingCapacity,
                        },
                        SelectionCharacteristics =
                        {
                            Characteristics.ConnectionsTerminals,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        }
                    });
            }

            private static class MasterPact
            {
                internal static KeyValuePair<string, Selector> MasterPact_MTZ1_CB() => new(
                    Selectors.MasterPact_MTZ1_CB,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ1_CB,
                        Name = "MTZ1",
                        Description = SelectorDescriptions.CB_630_1600,
                        Image = SelectorImages.MTZ1_CB,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ1_CB,
                        CanHaveAccessories = false,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 630,
                        MaxCurrent = 1600,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                            Characteristics.CircuitBreakerRatingCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        },
                    });

                internal static KeyValuePair<string, Selector> MasterPact_MTZ2_CB() => new(
                    Selectors.MasterPact_MTZ2_CB,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ2_CB,
                        Name = "MTZ2",
                        Description = SelectorDescriptions.CB_800_4000,
                        Image = SelectorImages.MTZ2_CB,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ2_CB,
                        CanHaveAccessories = false,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 800,
                        MaxCurrent = 4000,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                            Characteristics.CircuitBreakerRatingCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        },
                    });

                internal static KeyValuePair<string, Selector> MasterPact_MTZ3_CB() => new(
                    Selectors.MasterPact_MTZ3_CB,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ3_CB,
                        Name = "MTZ3",
                        Description = SelectorDescriptions.CB_4000_6300,
                        Image = SelectorImages.MTZ3_CB,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ3_CB,
                        CanHaveAccessories = false,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.CircuitBreakers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 4000,
                        MaxCurrent = 6300,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.T_RatedCurrent),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(ElectricalSystemProxy),
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                            Characteristics.CircuitBreakerRatingCode,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        },
                    });

                internal static KeyValuePair<string, Selector> MasterPact_MTZ1_SD() => new(
                    Selectors.MasterPact_MTZ1_SD,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ1_SD,
                        Name = "MTZ1",
                        Description = SelectorDescriptions.SD_630_1600,
                        Image = SelectorImages.MTZ1_SD,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ1_SD,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SwitchDisconnectors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 630,
                        MaxCurrent = 1600,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.TA_RatedCurrent),
                            new CurrentCriteriaConverter(
                                Criteria.TA_TripUnitRating),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.CircuitBreakerRatingCode,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                            Characteristics.RatedMakingCapacity,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        },
                    });

                internal static KeyValuePair<string, Selector> MasterPact_MTZ2_SD() => new(
                    Selectors.MasterPact_MTZ2_SD,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ2_SD,
                        Name = "MTZ2",
                        Description = SelectorDescriptions.SD_800_4000,
                        Image = SelectorImages.MTZ2_SD,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ2_SD,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SwitchDisconnectors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 800,
                        MaxCurrent = 4000,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.TA_RatedCurrent),
                            new CurrentCriteriaConverter(
                                Criteria.TA_TripUnitRating),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.CircuitBreakerRatingCode,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                            Characteristics.RatedMakingCapacity,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription,
                        },
                    });

                internal static KeyValuePair<string, Selector> MasterPact_MTZ3_SD() => new(
                    Selectors.MasterPact_MTZ3_SD,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = Selectors.MasterPact_MTZ3_SD,
                        Name = "MTZ3",
                        Description = SelectorDescriptions.SD_4000_6300,
                        Image = SelectorImages.MTZ3_SD,
                        DefaultConfiguration = DefaultConfigurations.MasterPact_MTZ3_SD,
                        CanHaveAccessories = true,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.SwitchDisconnectors,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 4000,
                        MaxCurrent = 6300,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            new CurrentCriteriaConverter(
                                Criteria.TA_RatedCurrent),
                            new CurrentCriteriaConverter(
                                Criteria.TA_TripUnitRating),
                            new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCharacteristics = new List<string>
                        {
                            Characteristics.CircuitBreakerRatingCode,
                            Characteristics.PolesDescription,
                            Characteristics.RatedCurrent,
                            Characteristics.RatedMakingCapacity,
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            Characteristics.RatedCurrent,
                            Characteristics.PolesDescription
                        }
                    });
            }

            private static class Modicon
            {
                internal static KeyValuePair<string, Selector> M221() => new(
                    Modicon_M221,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_M221,
                        Name = "Modicon M221",
                        Description = SelectorDescriptions.Modicon_M221,
                        Image = SelectorImages.Modicon_M221,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });

                internal static KeyValuePair<string, Selector> M241() => new(
                    Modicon_M241,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_M241,
                        Name = "Modicon M241",
                        Description = SelectorDescriptions.Modicon_M241,
                        Image = SelectorImages.Modicon_M241,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });

                internal static KeyValuePair<string, Selector> M340() => new(
                    Modicon_M340,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_M340,
                        Name = "Modicon M340",
                        Description = SelectorDescriptions.Modicon_M340,
                        Image = SelectorImages.Modicon_M340,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });

                internal static KeyValuePair<string, Selector> M580() => new(
                    Modicon_M580,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_M580,
                        Name = "Modicon M580",
                        Description = SelectorDescriptions.Modicon_M580,
                        Image = SelectorImages.Modicon_M580,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });

                internal static KeyValuePair<string, Selector> TM3() => new(
                    Modicon_TM3,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_TM3,
                        Name = "Modicon TM3",
                        Description = SelectorDescriptions.Modicon_TM3,
                        Image = SelectorImages.Modicon_TM3,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });

                internal static KeyValuePair<string, Selector> X80() => new(
                    Modicon_X80,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyEz,
                        Id = Modicon_X80,
                        Name = "Modicon X80",
                        Description = SelectorDescriptions.Modicon_X80,
                        Image = SelectorImages.Modicon_X80,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.ProgrammableLogicControllers,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                    });
            }

            private static class PowerLogic
            {
                internal static KeyValuePair<string, Selector> PM3200() => new(
                    PowerLogic_PM3200,
                    new Selector
                    {
                        Technology = SelectorConfigs.TechnologyKb,
                        Id = PowerLogic_PM3200,
                        Name = "PowerLogic PM3200",
                        Description = SelectorDescriptions.PowerLogic_PM3200,
                        Image = SelectorImages.PowerLogic_PM3200,
                        SelectorGroups = new List<SelectorGroup>
                        {
                            SelectorGroup.EnergyMetering,
                        },
                        Localizations =
                        {
                            Localization.EnInt,
                            Localization.RuRu,
                        },
                        MinCurrent = 0,
                        MaxCurrent = 3150,
                        CriteriaConverters = new List<ICriteriaConverter>
                        {
                            // new CurrentCriteriaConverter(
                            //     SeApi.Criteria.T_RatedCurrent),
                            // new BreakingCapacityConverter(),
                            // new NetworkTypeConverter(),
                            // new PolesDescriptionConverter(),
                        },
                        Applicability = new List<Type>
                        {
                            typeof(SwitchBoard),
                            typeof(SwitchBoardUnit),
                        },
                        UsedCriteria = new Dictionary<string, string>
                        {
                            // [Selectors.Criteria.T_CurveCode] = null,
                            // [Selectors.Criteria.T_PolesDescription] = null,
                            // [Selectors.Criteria.T_RatedOperationalCurrent] = null,
                            // [Selectors.Criteria.T_RatedCurrent] = null
                        },
                        ShortNameCharacteristics = new List<string>
                        {
                            // Selectors.Criteria.T_CurveCode,
                            // Selectors.Criteria.T_RatedCurrent,
                            // Selectors.Criteria.T_PolesDescription
                        }
                    });
            }

            private static KeyValuePair<string, Selector> Pragma() => new(
                Selectors.Pragma,
                new Selector
                {
                    Technology = SelectorConfigs.TechnologyEz,
                    Id = Selectors.Pragma,
                    Name = "Pragma",
                    Description = SelectorDescriptions.Pragma,
                    Image = SelectorImages.Pragma,
                    SelectorGroups = new List<SelectorGroup>
                    {
                        SelectorGroup.Enclosures,
                    },
                    Localizations =
                    {
                        Localization.EnInt,
                        Localization.RuRu,
                    },
                    MinCurrent = 0,
                    MaxCurrent = 125,
                    CriteriaConverters = new List<ICriteriaConverter>(),
                    Applicability = new List<Type>
                    {
                        typeof(SwitchBoard),
                        typeof(SwitchBoardUnit),
                    },
                    UsedCharacteristics = new List<string>
                    {
                        Characteristics.NumberOfModulesPerRow,
                        Characteristics.NumberOfHorizontalRows,
                        Characteristics.MountingMode,
                    },
                    ShortNameCharacteristics = new List<string>
                    {
                        Characteristics.NumberOfHorizontalRows,
                        Characteristics.NumberOfModulesPerRow
                    }
                });

            private static KeyValuePair<string, Selector> Kaedra() => new(
                Selectors.Kaedra,
                new Selector
                {
                    Technology = SelectorConfigs.TechnologyEz,
                    Id = Selectors.Kaedra,
                    Name = "Kaedra",
                    Description = SelectorDescriptions.Kaedra,
                    Image = SelectorImages.Kaedra,
                    SelectorGroups = new List<SelectorGroup>
                    {
                        SelectorGroup.Enclosures,
                    },
                    Localizations =
                    {
                        Localization.EnInt,
                        Localization.RuRu,
                    },
                    MinCurrent = 0,
                    MaxCurrent = 125,
                    CriteriaConverters = new List<ICriteriaConverter>(),
                    Applicability = new List<Type>
                    {
                        typeof(SwitchBoard),
                        typeof(SwitchBoardUnit),
                    },
                    UsedCriteria = new Dictionary<string, string>
                    {
                        [Criteria.NumberOfHorizontalRows] = "1",
                    },
                    UsedCharacteristics = new List<string>
                    {
                        Characteristics.NumberOfModulesPerRow,
                        Characteristics.NumberOfHorizontalRows,
                        Characteristics.MountingMode,
                    },
                    ShortNameCharacteristics = new List<string>
                    {
                        Characteristics.NumberOfHorizontalRows,
                        Characteristics.NumberOfModulesPerRow,
                    }
                });
        }
    }
}