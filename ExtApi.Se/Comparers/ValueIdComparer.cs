﻿using System.Collections.Generic;
using CoLa.BimEd.ExtApi.Se.Model.Catalog;

namespace CoLa.BimEd.ExtApi.Se.Comparers;

public class ValueIdComparer : IEqualityComparer<ValueId>
{
    public bool Equals(ValueId x, ValueId y)
    {
        if (ReferenceEquals(x, y)) return true;
        if (ReferenceEquals(x, null)) return false;
        if (ReferenceEquals(y, null)) return false;
        return x.Id == y.Id;
    }

    public int GetHashCode(ValueId obj)
    {
        return obj.Id != null ? obj.Id.GetHashCode() : 0;
    }
}