﻿using System.Collections.Generic;
using CoLa.BimEd.ExtApi.Se.Model.Catalog;

namespace CoLa.BimEd.ExtApi.Se.Comparers;

public class CatalogCharacteristicComparer : IEqualityComparer<CatalogCharacteristic>
{
    public bool Equals(CatalogCharacteristic x, CatalogCharacteristic y)
    {
        if (ReferenceEquals(x, y)) return true;
        if (ReferenceEquals(x, null)) return false;
        if (ReferenceEquals(y, null)) return false;
        return x.Id == y.Id;
    }

    public int GetHashCode(CatalogCharacteristic obj)
    {
        return obj.Id != null ? obj.Id.GetHashCode() : 0;
    }
}