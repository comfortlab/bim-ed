﻿namespace CoLa.BimEd.UI.Model.Images;

public enum ImageSize
{
    Image16x16,
    Image32x32,
}