﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Media.Imaging;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Resources;

// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.UI.Model.Images;

public static class BuiltInCategoryImage
{
    public static BitmapSource GetBitmapSource(ElectricalEquipmentProxy electricalEquipment, ImageSize imageSize) =>
        GetBitmap(electricalEquipment, imageSize).ToBitmapImage();

    public static BitmapSource GetBitmapSource(ElectricalSystemProxy electricalSystem, ImageSize imageSize) =>
        GetBitmap(electricalSystem, imageSize).ToBitmapImage();

    public static BitmapSource GetBitmapSource(SwitchBoard switchBoard, ImageSize imageSize) =>
        GetBitmap(switchBoard, imageSize).ToBitmapImage();

    public static BitmapSource GetBitmapSource(BuiltInCategoryProxy builtInCategoryProxy, ImageSize imageSize) =>
        GetBitmap(builtInCategoryProxy, imageSize).ToBitmapImage();

    public static Bitmap GetBitmap(SwitchBoard switchBoard, ImageSize imageSize)
    {
        return GetBitmap(switchBoard.FirstUnit, imageSize);
    }

    public static Bitmap GetBitmap(ElectricalEquipmentProxy electricalEquipment, ImageSize imageSize) =>        electricalEquipment switch
    {
        Transformer when imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Transformer_16x20,
        
        Transformer =>
            ElectricalElementImages.Transformer_32x32,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.One &&
                             imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Panel_1_16x20,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.One &&
                             imageSize == ImageSize.Image32x32 =>
            ElectricalElementImages.Panel_1_32x32,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.Two &&
                             imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Panel_2_16x20,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.Two &&
                             imageSize == ImageSize.Image32x32 =>
            ElectricalElementImages.Panel_2_32x32,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.Three &&
                             imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Panel_3_16x20,
        
        SwitchBoardUnit when electricalEquipment.PowerParameters?.PhasesNumber == PhasesNumber.Three &&
                             imageSize == ImageSize.Image32x32 =>
            ElectricalElementImages.Panel_3_32x32,
        
        _ => GetBitmap(BuiltInCategoryProxy.ElectricalEquipment, imageSize)
    };

    public static Bitmap GetBitmap(ElectricalSystemProxy electricalSystem, ImageSize imageSize)
    {
        var bitmap = GetCircuitBitmap(electricalSystem, imageSize);

        bitmap.SetDefaultResolution();

        return bitmap;
    }

    private static Bitmap GetCircuitBitmap(ElectricalSystemProxy electricalSystem, ImageSize imageSize) =>        electricalSystem.CircuitType switch
    {
        CircuitTypeProxy.Space when imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Space_16x16,
        CircuitTypeProxy.Space =>
            ElectricalElementImages.Space_32x32,
        CircuitTypeProxy.Spare when imageSize == ImageSize.Image16x16 =>
            ElectricalElementImages.Spare_16x16,
        CircuitTypeProxy.Spare =>
            ElectricalElementImages.Spare_32x32,
        _ => imageSize == ImageSize.Image16x16
            ? ElectricalElementImages.ElectricalSystem_16x16
            : ElectricalElementImages.ElectricalSystem_32x32
    };

    public static Bitmap GetBitmap(BuiltInCategoryProxy builtInCategoryProxy, ImageSize imageSize)
    {
        try
        {
            var dictionary = imageSize == ImageSize.Image16x16 ? Dictionary16x16 : Dictionary32x32;

            var bitmap = dictionary.ContainsKey(builtInCategoryProxy)
                ? dictionary[builtInCategoryProxy]
                : new Bitmap(1, 1);

            bitmap.SetDefaultResolution();

            return bitmap;
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return new Bitmap(1, 1);
        }
    }

    private static readonly Dictionary<BuiltInCategoryProxy, Bitmap> Dictionary16x16 = new()
    {
        [BuiltInCategoryProxy.CommunicationDevices] = ElectricalElementImages.Communication_16x16,
        [BuiltInCategoryProxy.DataDevices] = ElectricalElementImages.Data_16x16,
        [BuiltInCategoryProxy.ElectricalEquipment] = ElectricalElementImages.Panel_16x16,
        [BuiltInCategoryProxy.ElectricalFixtures] = ElectricalElementImages.ElectricalFixture_16x16,
        [BuiltInCategoryProxy.FireAlarmDevices] = ElectricalElementImages.FireAlarm_16x16,
        [BuiltInCategoryProxy.LightingDevices] = ElectricalElementImages.LightingDevice_16x16,
        [BuiltInCategoryProxy.LightingFixtures] = ElectricalElementImages.LightingFixture_16x16,
        [BuiltInCategoryProxy.MechanicalEquipment] = ElectricalElementImages.MechanicalEquipment_16x16,
        [BuiltInCategoryProxy.NurseCallDevices] = ElectricalElementImages.NurseCall_16x16,
        [BuiltInCategoryProxy.SecurityDevices] = ElectricalElementImages.Security_16x16,
        [BuiltInCategoryProxy.TelephoneDevices] = ElectricalElementImages.Telephone_16x16,
        [BuiltInCategoryProxy.Invalid] = ElectricalElementImages.Family_16x16,
    };

    private static readonly Dictionary<BuiltInCategoryProxy, Bitmap> Dictionary32x32 = new()
    {
        [BuiltInCategoryProxy.CommunicationDevices] = ElectricalElementImages.Communication_32x32,
        [BuiltInCategoryProxy.DataDevices] = ElectricalElementImages.Data_32x32,
        [BuiltInCategoryProxy.ElectricalEquipment] = ElectricalElementImages.Panel_32x32,
        [BuiltInCategoryProxy.ElectricalFixtures] = ElectricalElementImages.ElectricalFixture_32x32,
        [BuiltInCategoryProxy.FireAlarmDevices] = ElectricalElementImages.FireAlarm_32x32,
        [BuiltInCategoryProxy.LightingDevices] = ElectricalElementImages.LightingDevice_32x32,
        [BuiltInCategoryProxy.LightingFixtures] = ElectricalElementImages.LightingFixture_32x32,
        [BuiltInCategoryProxy.MechanicalEquipment] = ElectricalElementImages.MechanicalEquipment_32x32,
        [BuiltInCategoryProxy.NurseCallDevices] = ElectricalElementImages.NurseCall_32x32,
        [BuiltInCategoryProxy.SecurityDevices] = ElectricalElementImages.Security_32x32,
        [BuiltInCategoryProxy.TelephoneDevices] = ElectricalElementImages.Telephone_32x32,
        [BuiltInCategoryProxy.Invalid] = ElectricalElementImages.Family_32x32,
    };
}