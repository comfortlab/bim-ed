﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media.Imaging;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.UI.Resources;

// ReSharper disable InconsistentNaming

namespace CoLa.BimEd.UI.Model.Images;

public static class ElectricalSystemTypeImage
{
    public static BitmapSource GetBitmapSource(ElectricalSystemTypeProxy electricalSystemTypeProxy, ImageSize imageSize) =>
        GetBitmap(electricalSystemTypeProxy, imageSize).ToBitmapImage();

    public static Bitmap GetBitmap(ElectricalSystemTypeProxy electricalSystemTypeProxy, ImageSize imageSize)
    {
        var dictionary = imageSize == ImageSize.Image16x16 ? Dictionary16x16 : Dictionary32x32;

        var bitmap = dictionary.ContainsKey(electricalSystemTypeProxy)
            ? dictionary[electricalSystemTypeProxy]
            : new Bitmap(1, 1);

        bitmap.SetDefaultResolution();

        return bitmap;
    }

    private static readonly Dictionary<ElectricalSystemTypeProxy, Bitmap> Dictionary16x16 = new()
    {
        [ElectricalSystemTypeProxy.PowerCircuit] = ElectricalElementImages.Power_16x16,
        [ElectricalSystemTypeProxy.Communication] = ElectricalElementImages.Communication_16x16,
        [ElectricalSystemTypeProxy.Controls] = ElectricalElementImages.Controls_16x16,
        [ElectricalSystemTypeProxy.Data] = ElectricalElementImages.Data_16x16,
        [ElectricalSystemTypeProxy.FireAlarm] = ElectricalElementImages.FireAlarm_16x16,
        [ElectricalSystemTypeProxy.NurseCall] = ElectricalElementImages.NurseCall_16x16,
        [ElectricalSystemTypeProxy.Security] = ElectricalElementImages.Security_16x16,
        [ElectricalSystemTypeProxy.Telephone] = ElectricalElementImages.Telephone_16x16,
        [ElectricalSystemTypeProxy.UndefinedSystemType] = ElectricalElementImages.ElectricalSystem_16x16,
    };

    private static readonly Dictionary<ElectricalSystemTypeProxy, Bitmap> Dictionary32x32 = new()
    {
        [ElectricalSystemTypeProxy.PowerCircuit] = ElectricalElementImages.Power_32x32,
        [ElectricalSystemTypeProxy.Communication] = ElectricalElementImages.Communication_32x32,
        [ElectricalSystemTypeProxy.Controls] = ElectricalElementImages.Controls_16x16,
        [ElectricalSystemTypeProxy.Data] = ElectricalElementImages.Data_32x32,
        [ElectricalSystemTypeProxy.FireAlarm] = ElectricalElementImages.FireAlarm_32x32,
        [ElectricalSystemTypeProxy.NurseCall] = ElectricalElementImages.NurseCall_32x32,
        [ElectricalSystemTypeProxy.Security] = ElectricalElementImages.Security_32x32,
        [ElectricalSystemTypeProxy.Telephone] = ElectricalElementImages.Telephone_32x32,
        [ElectricalSystemTypeProxy.UndefinedSystemType] = ElectricalElementImages.ElectricalSystem_32x32,
    };
}