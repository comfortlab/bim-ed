using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Resources;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.Model.Converters;

public class PhaseConfigurationConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not ElectricalPhaseConfigurationProxy configuration)
            return null;

        return configuration switch
        {
            ElectricalPhaseConfigurationProxy.Undefined => CommonDictionary.None,
            ElectricalPhaseConfigurationProxy.Wye => ElectricalParameters.PhaseConfigurationWye,
            ElectricalPhaseConfigurationProxy.Delta => ElectricalParameters.PhaseConfigurationDelta,
            _ => null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}