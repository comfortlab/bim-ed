﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;
using CoLa.BimEd.UI.Model.Images;
using Image = System.Windows.Controls.Image;

namespace CoLa.BimEd.UI.Model.Converters;

public class ElectricalElementsImagesConverter : IValueConverter
{
    public ImageSize ImageSize { get; set; } = ImageSize.Image16x16;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not ElectricalSystemProxy electricalCircuit)
            return null;

        if (electricalCircuit.CircuitType != CircuitTypeProxy.Circuit)
            return GetObjectSpare(electricalCircuit);

        var electricalElements = electricalCircuit.Consumers.ToList();

        return electricalElements.Count == 1 &&
               electricalElements[0] is SwitchBoardUnit switchboardUnit
            ? GetObjectPanel(switchboardUnit)
            : GetObjectsElements(electricalCircuit.GetConsumers().ToList());
    }

    private List<object> GetObjectSpare(ElectricalSystemProxy electricalSystem)
    {
        var imageSource = BuiltInCategoryImage.GetBitmapSource(electricalSystem, ImageSize);
         
        return new List<object>
        {
            new Image
            {
                Height = imageSource.Height,
                Width = imageSource.Width, // 40,
                Stretch = Stretch.None,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Source = imageSource,
            }
        };
    }

    private List<object> GetObjectPanel(SwitchBoardUnit switchBoardUnit)
    {
        var objects = new List<object>();
        var imageSource = BuiltInCategoryImage.GetBitmapSource(switchBoardUnit, ImageSize);
        var image = new Image
        {
            Width = imageSource.Width,
            Height = imageSource.Height,
            Stretch = Stretch.None,
            HorizontalAlignment = HorizontalAlignment.Center,
            VerticalAlignment = VerticalAlignment.Center,
            Source = imageSource,
        };

        objects.Add(image);
        objects.Add(new TextBlock(new Run($"{switchBoardUnit.Name}"))
        {
            FontWeight = FontWeights.DemiBold
        });

        return objects;
    }

    private List<object> GetObjectsElements(ICollection<ElectricalElementProxy> electricalElements)
    {
        var objects = new List<object>();
        var categoryProxies = electricalElements.Select(n => n.Category).Distinct().OrderBy(n => n).ToList();

        foreach (var categoryProxy in categoryProxies)
        {
            var imageSource = BuiltInCategoryImage.GetBitmapSource(categoryProxy, ImageSize);
            var image = new Image
            {
                Width = imageSource.Width,
                Height = imageSource.Height,
                Stretch = Stretch.None,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Source = imageSource,
            };

            var stackPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
            };

            stackPanel.Children.Add(image);

            var count = electricalElements.Count(n => n.Category == categoryProxy);

            if (electricalElements.Count > 1 || count > 1)
                stackPanel.Children.Add(new TextBlock(new Run($"×{count} ")));
                
            objects.Add(stackPanel);
        }

        return objects;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}