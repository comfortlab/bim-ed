using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Enums;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.Model.Converters;

public class PhasesNumberConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not PhasesNumber phasesNumber)
            return null;

        return phasesNumber switch
        {
            PhasesNumber.One => ElectricalParameters.PhaseSingle,
            PhasesNumber.Three => ElectricalParameters.PhaseThree,
            _ => null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}