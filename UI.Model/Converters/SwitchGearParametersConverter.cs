﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.MediumVoltage;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.Model.Converters;

public class SwitchGearParametersConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            GasExhaust.Downwards => SwitchGearResources.GasExhaust_Downwards,
            GasExhaust.Rear => SwitchGearResources.GasExhaust_Rear,
            GasExhaust.Upwards => SwitchGearResources.GasExhaust_Upwards,
            SwitchMountType.Fixed => SwitchGearResources.SwitchMountType_Fixed,
            SwitchMountType.Withdrawable => SwitchGearResources.SwitchMountType_Withdrawable,
            _ => throw new ArgumentOutOfRangeException(nameof(value), value, null)
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}