﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.Model.Converters;

public class FunctionConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        Function.BusRiser => Functions.BusRiser,
        Function.CableConnection => Functions.CableConnection,
        Function.GeneralProtection => Functions.GeneralProtection,
        Function.GeneratorIncomer => Functions.GeneratorIncomer,
        Function.LineFeeder => Functions.LineFeeder,
        Function.LineProtection => Functions.LineProtection,
        Function.Metering => Functions.Metering,
        Function.MotorProtection => Functions.MotorProtection,
        Function.SectionSwitch => Functions.SectionSwitch,
        Function.Special => Functions.Special,
        Function.TransformerProtection => Functions.TransformerProtection,
        _ => null,
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}