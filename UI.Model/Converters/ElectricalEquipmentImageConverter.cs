﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Base;
using CoLa.BimEd.UI.Model.Images;

namespace CoLa.BimEd.UI.Model.Converters;

public class ElectricalEquipmentImageConverter : IValueConverter
{
    public ImageSize ImageSize { get; set; } = ImageSize.Image16x16;
        
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            ElectricalEquipmentProxy electricalEquipment => BuiltInCategoryImage.GetBitmapSource(electricalEquipment,
                ImageSize),
            SwitchBoard switchboard => BuiltInCategoryImage.GetBitmapSource(switchboard, ImageSize),
            _ => null
        };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => value;
}