﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.RevitProxy;

namespace CoLa.BimEd.UI.Model.Converters;

public class VoltageTypeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        value is VoltageTypeProxy voltageType
            ? $"{voltageType.Name} | {voltageType.ActualValue} | {voltageType.MinValue} ... {voltageType.MaxValue}"
            : value;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}