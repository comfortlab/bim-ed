﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical.Transformers;
using CoLa.BimEd.UI.Resources.Electrical;

namespace CoLa.BimEd.UI.Model.Converters;

public class CoolingModeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value switch
        {
            CoolingMode.AN => ElectricalParameters.CoolingMode_AN,
            CoolingMode.AF => ElectricalParameters.CoolingMode_AF,
            CoolingMode.AF25 => ElectricalParameters.CoolingMode_AF,
            CoolingMode.AF40 => ElectricalParameters.CoolingMode_AF,
            _ => throw new ArgumentOutOfRangeException(nameof(value), value, null)
        } ;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}