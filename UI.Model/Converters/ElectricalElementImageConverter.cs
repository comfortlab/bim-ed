﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.BusinessLogic.Model.Electrical;
using CoLa.BimEd.UI.Model.Images;

namespace CoLa.BimEd.UI.Model.Converters;

public class ElectricalElementImageConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not ElectricalElementProxy electricalElement)
            return null;

        var categoryProxy = electricalElement.Category;
        return BuiltInCategoryImage.GetBitmapSource(categoryProxy, ImageSize.Image16x16);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
        throw new NotImplementedException();
}