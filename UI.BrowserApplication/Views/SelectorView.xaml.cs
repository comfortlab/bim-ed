﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CefSharp;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Logic.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Selectors.SelectorRequest;
using CoLa.BimEd.ExtApi.Se.Model.Widget;
using CoLa.BimEd.ExtApi.Se.Model.Widget.EventArgs;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.UI.BrowserApplication.CefSharp;
using CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;
using CoLa.BimEd.UI.BrowserApplication.Resources;
using Newtonsoft.Json;
using Localization = CoLa.BimEd.Infrastructure.Framework.Localization;

namespace CoLa.BimEd.UI.BrowserApplication.Views;

public partial class SelectorView
{
    private bool _isReceived;
    private bool _isUpdating;

    private readonly CefSharpBrowser _browser;
    private readonly SeApiHttpClient _seApiHttpClient;
    private readonly SelectorConfigBuilder _selectorConfigBuilder;
    private readonly SeApi.Selectors _selectors;
    private SelectorConfig _selectorConfig;

    public static readonly DependencyProperty CallbackProperty = DependencyProperty.Register(
        nameof(Callback), typeof(Action<SelectorConfig>), typeof(SelectorView));

    public static readonly DependencyProperty SelectorIdProperty = DependencyProperty.Register(
        nameof(SelectorId), typeof(string), typeof(SelectorView), new PropertyMetadata(OnSelectorIdChanged));

    public static readonly DependencyProperty SelectorConfigProperty = DependencyProperty.Register(
        nameof(SelectorConfig), typeof(string), typeof(SelectorView), new PropertyMetadata(OnSelectorConfigChanged));

    public Action<SelectorConfig> Callback
    {
        get => (Action<SelectorConfig>)GetValue(CallbackProperty);
        set => SetValue(CallbackProperty, value);
    }

    public string SelectorId
    {
        get => (string)GetValue(SelectorIdProperty);
        set => SetValue(SelectorIdProperty, value);
    }

    public string SelectorConfig
    {
        get => (string)GetValue(SelectorConfigProperty);
        set => SetValue(SelectorConfigProperty, value);
    }

    private static void OnSelectorIdChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is SelectorView selectorView)
            selectorView.UpdateSelector();
    }
    
    private static void OnSelectorConfigChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is SelectorView selectorView)
            selectorView.UpdateSelector();
    }

    private readonly string _htmlPage;

    public SelectorView()
    {
        InitializeComponent();

        var applicationPaths = DI.Get<AppPaths>();
        _seApiHttpClient = DI.Get<SeApiHttpClient>();
        _selectors = DI.Get<SeApi.Selectors>();
        _isReceived = false;

        Content = _browser = DI.Get<CefSharpBrowser>();
        
        var webSelectorResult = new WidgetResult();
        CefSharpSettings.WcfEnabled = true;
        _browser.ChromiumWebBrowser.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
        _browser.ChromiumWebBrowser.JavascriptObjectRepository.Register("webSelectorResult", webSelectorResult, isAsync: false, options: BindingOptions.DefaultBinder);
        _browser.ChromiumWebBrowser.JavascriptObjectRepository.Register("exceptionMessage", new WebSelectorExceptionMessage(), isAsync: false, options: BindingOptions.DefaultBinder);

        _htmlPage = Path.Combine(applicationPaths.LocalData, "HTMLPage.html");

        if (!File.Exists(_htmlPage))
            File.WriteAllText(_htmlPage, BrowserResources.HtmlPage);
        
        _browser.ChromiumWebBrowser.RequestHandler = new SelectionRequestHandler(webSelectorResult);

        var localization = new Localization(Localization.RuRu);
        _selectorConfigBuilder = new SelectorConfigBuilder(_seApiHttpClient, localization);
        
        webSelectorResult.WebSelectorProductsDataReceived += OnWebSelectorProductsDataReceived;
    }

    private void UpdateSelector()
    {
        if (_isUpdating) return;
        
        try
        {
            _isUpdating = true;
            UpdateProperties();
            _browser.ChromiumWebBrowser.FrameLoadEnd += BrowserOnFrameLoadEnd;
            Dispatcher.Invoke(() => { _browser.ChromiumWebBrowser.Address = _htmlPage; });
        }
        catch (Exception exception)
        {
            App.ShowError(exception);
        }
        finally
        {
            _isUpdating = false;
        }
    }

    private void UpdateProperties()
    {
        if (SelectorConfig != null)
        {
            _selectorConfig = JsonConvert.DeserializeObject<SelectorConfig>(SelectorConfig);
            SelectorId = _selectorConfig.Data.Selections.GlobalId;
        }
        else if (SelectorId != null)
        {
            _selectorConfig = _selectorConfigBuilder.Build(
                _selectors.GetAll().FirstOrDefault(s => s.Id == SelectorId),
                SelectorId);
        }
    }

    private void BrowserOnFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
    {
        try
        {
            if (!e.Frame.IsMain)
                return;

            // Необходима задержка для запуска селектора в Revit
            Thread.Sleep(100);
        
            var token = $"{SeApi.SelectorConfigs.AuthScheme} {SeApi.SelectorConfigs.AuthToken}";
            var selectorConfig = _selectorConfig.Serialize();

            _browser.ChromiumWebBrowser.ExecuteScriptAsync(
                $"initialize(\"{token}\", " +
                $"{selectorConfig})");

            _browser.ChromiumWebBrowser.FrameLoadEnd -= BrowserOnFrameLoadEnd;
        }
        catch (Exception exception)
        {
            App.ShowError(exception);
        }
    }

    private async Task OnWebSelectorProductsDataReceived(object sender, WebSelectorEventArgs e)
    {
        if (_isReceived)
            return;

        Dispatcher.Invoke(() => _isReceived = true);

        SelectorConfig resultSelectorConfig = null;
        
        if (!string.IsNullOrEmpty(e.SessionId))
        {
            resultSelectorConfig = await GetConfigurationBySessionId(e);
        }
        else if (e.EzData.Any())
        {
            resultSelectorConfig = GetConfigurationFromEzData(e);
        }
        
        Dispatcher.Invoke(() =>
        {
            _browser.ChromiumWebBrowser.IsEnabled = false;
            Callback?.Invoke(resultSelectorConfig);
            _isReceived = true;
        });
    }

    private async Task<SelectorConfig> GetConfigurationBySessionId(WebSelectorEventArgs e)
    {
        var selectorConverters = DI.Get<SelectorConverters>();
        var requestConfig = selectorConverters.ConvertToSelectorConfigRequest(_selectorConfig, e.SessionId);
        var guid = await _seApiHttpClient.RequestEntityAsync<SelectorConfigRequest, SelectionGuid>(
            CancellationToken.None,
            SeApi.EndPoints.AdministrationSelectionSave, requestConfig);

        return await _seApiHttpClient.GetEntityAsync<SelectorConfig>(
            CancellationToken.None,
            SeApi.EndPoints.AdministrationSelection + "/" +
            guid.Guid);
    }

    private static SelectorConfig GetConfigurationFromEzData(WebSelectorEventArgs e)
    {
        var selectorConverters = DI.Get<SelectorConverters>();
        return selectorConverters.ConvertFromEzData(e.EzData.Values, e.Result.Products);
    }

}