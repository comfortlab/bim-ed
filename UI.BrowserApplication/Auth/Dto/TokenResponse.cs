﻿namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public class TokenResponse
{
    public bool IsRequestStatusSuccess { get; set; }
    public string ErrorMessage { get; set; }
    public string AccessToken { get; set; }
    public string EncryptedRefreshToken { get; set; }
    public bool IsUserPremium { get; set; }
}