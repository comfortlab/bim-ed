﻿using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public class StatsRequest : TokenRefreshRequest
{
    [JsonProperty("usage_stats")]
    public string UsageStats { get; set; }
}