﻿using System;
using System.Collections.Generic;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public class VersionInfo
{
    public int Id { get; set; }
    //public string VersionNumber { get; set; }
    public Version VersionNumber { get; set; }
    public bool IsBeta { get; set; }
    public DateTime? Date { get; set; }
    public IList<string> Changes { get; set; } = new List<string>();
}