using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public abstract class Request
{
    [JsonProperty("app")]
    public string App { get; set; }

    [JsonProperty("client_id")]
    public string ClientId { get; set; }

    [JsonProperty("client_secret_version")]
    public int ClientSecretVersion { get; set; }

    [JsonProperty("hwid")]
    public string Hwid { get; set; }
}