﻿using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public class TokenRefreshRequest : Request
{
    [JsonProperty("encrypted_refresh_token")]
    public string EncryptedRefreshToken { get; set; }
}