﻿using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Dto;

public class TokenRequest : Request
{
    [JsonProperty("code")]
    public string Code { get; set; }

    [JsonProperty("verifer")]
    public string Verifer { get; set; }
}