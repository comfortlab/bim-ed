using System;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Configs;

public class ProdAuthConfig : AuthConfig
{
    public override string ClientId => "3MVG9yZ.WNe6byQAvlJ.HwD94PRiFsdLN6gOV4y26CzZTRuFoHcaKyQ2pti3oDptp4aamTBChlcXwgNTv6xjB";
    public override int ClientSecretVersion => 1; //0;
    protected override string LogInLink => "https://secureidentity.schneider-electric.com/identity/services/apexrest/App/BIMED/services/oauth2/authorize";
    public override Uri LogInUri => new($"{LogInLink}?" + string.Join("&",
        $"response_type={ResponseType}",
        $"client_id={ClientId}",
        $"code_challenge={CodeChallenge}",
        $"code_challenge_method={CodeChallengeMethod}",
        $"redirect_uri={RedirectUri}"));
    public override Uri LogOutUri => new("https://secureidentity.schneider-electric.com/identity/VFP_IDMS_IDPSloInit?RelayState=" + BimEdEndPoints.Logout);
    public override Uri ChangePasswordUri => new("https://secureidentity.schneider-electric.com/identity/idmspasswordupdate?app=BIMED");
}