using System;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Configs;

public class NewProdAuthConfig : AuthConfig
{
    public override string ClientId => "290f3f5602d3e1d6d8f37e40d14cb92c3b2028c446cebaa45d6449b85b1f5133f97ea300Dg0000006I2OaEAK";
    public override int ClientSecretVersion => 1;
    protected override string LogInLink => "https://secureidentity-uat.schneider-electric.com/identity/idplogin";
    public override Uri LogInUri => new($"{LogInLink}?" + string.Join("&",
        $"app={App}",
        $"response_type={ResponseType}",
        $"client_id={ClientId}",
        $"redirect_uri={RedirectUri}"));
    public override Uri LogOutUri => null;
    public override Uri ChangePasswordUri => null;
}