﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Configs;

public abstract class AuthConfig
{
    protected const string App = "BIMED";
    protected const string RedirectUri = "https%3A%2F%2Fbim-ed.com%2Fapi%2Fv1%2Fauth%2Fcode";
    protected const string ResponseType = "code";
    protected const string CodeChallengeMethod = "S256";

    public abstract string ClientId { get; }
    public abstract int ClientSecretVersion { get; }
    protected abstract string LogInLink { get; }
    public abstract Uri LogInUri { get; }
    public abstract Uri LogOutUri { get; }
    public abstract Uri ChangePasswordUri { get; }

    protected string CodeChallenge
    {
        get
        {
            var sha = new SHA256Managed();
            sha.ComputeHash(Encoding.UTF8.GetBytes(Verifer));
            return Convert.ToBase64String(sha.Hash).Replace('+', '-').Replace('/', '_').Replace("=", "");
        }
    }

    private string _verifer;
    internal string Verifer => _verifer ??= GetVerifer();

    private static string GetVerifer()
    {
        var buffer = new byte[32];
        var rng = new RNGCryptoServiceProvider();
        rng.GetBytes(buffer);
        return Convert.ToBase64String(buffer).Replace('+', '-').Replace('/', '_').Replace("=", "");
    }
}