using System;

namespace CoLa.BimEd.UI.BrowserApplication.Auth.Configs;

public class NewPreProdAuthConfig : AuthConfig
{
    public override string ClientId => "a569229c1bb3321e581b2a5a672fa2418cbf08b244c54a99c1226fd8a1be06ce48288300Dg0000006I3YAEA0";
    public override int ClientSecretVersion => 1;
    protected override string LogInLink => "https://secureidentity-preprod.schneider-electric.com/identity/idplogin";
    public override Uri LogInUri => new($"{LogInLink}?" + string.Join("&",
        $"app={App}",
        $"response_type={ResponseType}",
        $"client_id={ClientId}",
        $"redirect_uri={RedirectUri}"));
    public override Uri LogOutUri => null;
    public override Uri ChangePasswordUri => null;
}