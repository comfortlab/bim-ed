﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Net;
using CoLa.BimEd.UI.BrowserApplication.Auth.Configs;
using CoLa.BimEd.UI.BrowserApplication.Auth.Dto;
using CoLa.BimEd.UI.BrowserApplication.Utils;
using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.Auth;

public class AuthService
{
    private readonly AuthConfig _authConfig;
    private readonly AppHttpClient _httpClient;
    private readonly AppPaths _appPaths;
        
    public string SeIdmsAccessToken { get; private set; }
        
    public AuthService(AppHttpClient httpClient, AppPaths appPaths)
    {
        _authConfig = new NewProdAuthConfig();
        _httpClient = httpClient;
        _appPaths = appPaths;
    }

    public async Task<bool> Request(string applicationKey, RequestMode requestMode, string authenticationCode = null)
    {
        try
        {
            var requestUri = GetRequestUri(requestMode);
            var request = GetRequest(applicationKey, requestMode, authenticationCode);

            if (request == null)
                return false;

            var response = await GetResponse(requestUri, request);

            if (response is not { IsRequestStatusSuccess: true })
                return false;

            SeIdmsAccessToken = response.AccessToken;
            WriteRefreshToken(response.EncryptedRefreshToken);

            return true;
        }
        catch (Exception exception)
        {
            // Logger.Error(exception);
            return false;
        }
    }

    private static string GetRequestUri(RequestMode requestMode) =>
        requestMode switch
        {
            RequestMode.Authenticate => BimEdEndPoints.Token,
            RequestMode.CheckAuthentication => BimEdEndPoints.CheckAuth,
            RequestMode.RefreshToken => BimEdEndPoints.TokenRefresh,
            RequestMode.SendStats => BimEdEndPoints.SendStats,
            _ => throw new ArgumentOutOfRangeException(nameof(requestMode), requestMode, null)
        };

    private Request GetRequest(string applicationKey, RequestMode requestMode, string authenticationCode = null) =>
        requestMode switch
        {
            RequestMode.Authenticate => GetTokenRequest(applicationKey, authenticationCode),
            RequestMode.CheckAuthentication => GetTokenRefreshRequest(applicationKey),
            RequestMode.RefreshToken => GetTokenRefreshRequest(applicationKey),
            RequestMode.SendStats => GetStatsRequest(applicationKey),
            _ => throw new ArgumentOutOfRangeException(nameof(requestMode), requestMode, null)
        };

    private Request GetTokenRequest(string applicationKey, string code)
    {
        if (code != null)
            code = Uri.UnescapeDataString(code);

        if (string.IsNullOrWhiteSpace(code))
            return null;
            
        return new TokenRequest
        {
            App = applicationKey,
            ClientId = _authConfig.ClientId,
            ClientSecretVersion = _authConfig.ClientSecretVersion,
            Code = code,
            Verifer = _authConfig.Verifer,
            Hwid = HwidUtils.GetHwid(),
        };
    }

    private Request GetTokenRefreshRequest(string applicationKey)
    {
        var refreshToken = ReadRefreshToken();

        if (string.IsNullOrWhiteSpace(refreshToken))
            return null;

        return new TokenRefreshRequest
        {
            App = applicationKey,
            ClientId = _authConfig.ClientId,
            ClientSecretVersion = _authConfig.ClientSecretVersion,
            EncryptedRefreshToken = refreshToken,
            Hwid = HwidUtils.GetHwid(),
        };
    }

    private Request GetStatsRequest(string applicationKey)
    {
        var refreshToken = ReadRefreshToken();

        if (string.IsNullOrWhiteSpace(refreshToken))
            return null;

        return new StatsRequest
        {
            App = applicationKey,
            ClientId = _authConfig.ClientId,
            ClientSecretVersion = _authConfig.ClientSecretVersion,
            EncryptedRefreshToken = refreshToken,
            Hwid = HwidUtils.GetHwid(),
            UsageStats = GetSerializedStatsAndDeleteFile(),
        };
    }

    private async Task<TokenResponse> GetResponse(string requestUrl, Request request)
    {
        try
        {
            var requestJson = JsonConvert.SerializeObject(request);
            var requestContent = new StringContent(requestJson, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(requestUrl, requestContent);

            if (response == null)
                return null;

            var result = await response.Content.ReadAsStringAsync();

            if (result == null)
                return null;

            var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(result);

            return tokenResponse;
        }
        catch (Exception exception)
        {
            // Logger.Error(exception);
            return null;
        }
    }

    private string ReadRefreshToken()
    {
        return File.Exists(_appPaths.RefreshToken) ? File.ReadAllText(_appPaths.RefreshToken) : null;
    }

    public void ResetToken()
    {
        SeIdmsAccessToken = null;
        WriteRefreshToken(string.Empty);
    }

    private void WriteRefreshToken(string refreshToken)
    {
        File.WriteAllText(_appPaths.RefreshToken, refreshToken);
    }

    private string GetSerializedStatsAndDeleteFile()
    {
        if (!File.Exists(_appPaths.Stats))
            return string.Empty;

        var stats = JsonConvert.SerializeObject(File.ReadAllLines(_appPaths.Stats));

        if (File.Exists(_appPaths.Stats))
            File.Delete(_appPaths.Stats);

        return stats;
    }
        
    public Uri GetAuthenticationUri(AuthMode authMode) =>
        authMode switch
        {
            AuthMode.LogIn => _authConfig.LogInUri,
            AuthMode.LogOut => _authConfig.LogOutUri,
            AuthMode.ChangePassword => _authConfig.ChangePasswordUri,
            _ => throw new ArgumentOutOfRangeException(nameof(authMode), authMode, null)
        };
}