﻿namespace CoLa.BimEd.UI.BrowserApplication.Auth;

public static class BimEdEndPoints
{
    public const string CheckAuth = "https://bim-ed.com/api/v1/checkauth";
    public const string GetReferences = "https://bim-ed.com/api/v1/getreferences";
    public const string GetVersion = "https://bim-ed.com/api/v1/getversion";
    public const string Logout = "https://bim-ed.com/api/v1/logout/";
    public const string SendStats = "https://bim-ed.com/api/v1/sendstats";
    public const string Token = "https://bim-ed.com/api/v1/token";
    public const string TokenRefresh = "https://bim-ed.com/api/v1/tokenrefresh";
    public const string Update = "https://bim-ed.com/api/v1/update";
    public const string UpdateBeta = "https://bim-ed.com/api/v1/update/beta";
}

public class SchneiderElectricApiUrl
{
    public const string GetCurves = "https://ect.se.com/CBT/Services/CoordinationCurveService.svc/GetCurves";
}