﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoLa.BimEd.UI.BrowserApplication.ViewModels;
using CoLa.BimEd.UI.BrowserApplication.Views;

namespace CoLa.BimEd.UI.BrowserApplication.Converters;

public class ViewConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value switch
    {
        TestCustomViewModel1 => new TestCustomComponent1(),
        TestCustomViewModel2 => new TestCustomComponent2(),
        _ => null
    };

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}