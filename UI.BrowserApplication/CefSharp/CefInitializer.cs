﻿using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using CefSharp;
using CefSharp.Wpf;
using CoLa.BimEd.Infrastructure.Framework.Application;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp;

public class CefInitializer
{
    private const string CefSharp = "CefSharp.dll";
    private const string CefSharpCore = "CefSharp.Core.dll";
    private const string CefSharpCoreRuntime = "CefSharp.Core.Runtime.dll";
    private const string CefSharpWpf = "CefSharp.Wpf.dll";
    private const string CefSharpBrowserSubprocess = "CefSharp.BrowserSubprocess.exe";
    private const string Libcef = "libcef.dll";
    private const string Locales = "locales";

    private readonly AppPaths _appPaths;

    public CefInitializer(AppPaths appPaths) => _appPaths = appPaths;

    public bool Initialize()
    {
        var cefSharpPath = _appPaths.CefSharpPath;

        if (string.IsNullOrWhiteSpace(cefSharpPath))
            return false;

        var browserCefSharp = Path.Combine(cefSharpPath, CefSharp);
        var browserCefSharpCore = Path.Combine(cefSharpPath, CefSharpCore);
        var browserCefSharpCoreRuntime = Path.Combine(cefSharpPath, CefSharpCoreRuntime);
        var browserCefSharpWpf = Path.Combine(cefSharpPath, CefSharpWpf);

        Assembly.LoadFrom(browserCefSharp);
        Assembly.LoadFrom(browserCefSharpCore);
        Assembly.LoadFrom(browserCefSharpCoreRuntime);
        Assembly.LoadFrom(browserCefSharpWpf);

        var assembly = Assembly.GetCallingAssembly().CodeBase.Replace(@"file:///", string.Empty);
        var directory = Path.GetDirectoryName(assembly);

        if (directory == null) throw new DirectoryNotFoundException(assembly);

        CopyIfNotExist(browserCefSharpCore, Path.Combine(directory, CefSharpCore));
        CopyIfNotExist(browserCefSharpCoreRuntime, Path.Combine(directory, CefSharpCoreRuntime));
        CopyIfNotExist(browserCefSharpWpf, Path.Combine(directory, CefSharpWpf));

        var libcef = Path.Combine(cefSharpPath, Libcef);
        using var libraryLoader = new CefLibraryHandle(libcef);
        var isValid = !libraryLoader.IsInvalid;

        return LoadCefSharp();
    }

    private static void CopyIfNotExist(string source, string dest)
    {
        if (!File.Exists(dest)) File.Copy(source, dest);
    }

    [MethodImpl(MethodImplOptions.NoInlining)]
    private bool LoadCefSharp()
    {
        var cefSharpPath = _appPaths.CefSharpPath;

        var settings = new CefSettings
        {
            BrowserSubprocessPath = Path.Combine(cefSharpPath, CefSharpBrowserSubprocess),
            CachePath = _appPaths.CefSharpLocalData,
            LocalesDirPath = Path.Combine(cefSharpPath, Locales),
            PersistSessionCookies = true,
            ResourcesDirPath = cefSharpPath,
        };

        return Cef.Initialize(settings, performDependencyCheck: false);
    }
}