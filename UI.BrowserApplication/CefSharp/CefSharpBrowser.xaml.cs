﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Windows;
using CefSharp;
using CefSharp.Wpf;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.Net.Proxy;
using CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp;

public partial class CefSharpBrowser
{
    private readonly ProxyStorage _proxyStorage;
    public ChromiumWebBrowser ChromiumWebBrowser { get; set; }
    public Action LoadStartAction { get; set; }
    public Action LoadEndAction { get; set; }
    public Uri Address
    {
        get => new(ChromiumWebBrowser?.Address ?? string.Empty);
        set
        {
            if (ChromiumWebBrowser != null)
                ChromiumWebBrowser.Address = value.OriginalString;
        }
    }
    
    public CefSharpBrowser(
        AppPaths appPaths,
        ProxyStorage proxyStorage,
        CookieStorage cookieStorage)
    {
        try
        {
            _proxyStorage = proxyStorage;
            
            InitializeComponent();

            if (!Cef.IsInitialized)
            {
                new CefInitializer(appPaths).Initialize();
            }

            if (Cef.IsInitialized)
            {
                Content = ChromiumWebBrowser = CreateChromiumWebBrowser(cookieStorage);
            }
            else
            {
                Content = new BrowserNotFoundView();
            }
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
        }
    }

    private ChromiumWebBrowser CreateChromiumWebBrowser(CookieStorage cookieStorage = null)
    {
        var chromiumWebBrowser = new ChromiumWebBrowser();

        SetProxy();

        if (cookieStorage != null)
            chromiumWebBrowser.FrameLoadEnd += cookieStorage.ReadCookies;
        
        chromiumWebBrowser.LoadingStateChanged += WebBrowserOnLoadingStateChanged;

        return chromiumWebBrowser;
    }

    private void SetProxy()
    {
        // https://stackoverflow.com/a/51723988/7671820

        var proxyConfig = _proxyStorage?.Read();

        if (proxyConfig == null || proxyConfig.ProxyType == ProxyType.None)
        {
            return;
        }

        // https://stackoverflow.com/a/42728008/7671820
            
        var scheme = proxyConfig.ProxyType == ProxyType.Socks5 ? "socks5" : "http";
        var webProxy = new WebProxy(
            Address: $"{scheme}://{proxyConfig.Host}:{proxyConfig.Port}",
            BypassOnLocal: true,
            BypassList: new[] { "" },
            Credentials: new NetworkCredential(proxyConfig.UserName, proxyConfig.Password));

        ChromiumWebBrowser.RequestHandler = new ProxyRequestHandler(webProxy.Credentials as NetworkCredential);
        ChromiumWebBrowser.IsBrowserInitializedChanged += SetWebProxy(ChromiumWebBrowser, webProxy);
    }

    private static DependencyPropertyChangedEventHandler SetWebProxy(
        IWebBrowser webBrowser, WebProxy webProxy) => (sender, e) =>
    {
        if (e.NewValue is false)
            return;

        Cef.UIThreadTaskFactory.StartNew(delegate
        {
            var value = new Dictionary<string, object>
            {
                ["mode"] = "fixed_servers",
                ["server"] = $"{webProxy.Address.Scheme}://{webProxy.Address.Host}:{webProxy.Address.Port}"
            };

            if (!webBrowser.GetBrowser().GetHost().RequestContext.SetPreference("proxy", value, out var error))
            {
                Debug.WriteLine(error);
            }
        });
    };

    private void WebBrowserOnLoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
    {
        if (e.IsLoading)
            Dispatcher?.InvokeAsync(() => { LoadStartAction?.Invoke(); });
        else
            Dispatcher?.InvokeAsync(() => { LoadEndAction?.Invoke(); });
    }
}