using CefSharp;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class DefaultResourceRequestHandler : IResourceRequestHandler
{
    public void Dispose()
    {
            
    }

    public virtual ICookieAccessFilter GetCookieAccessFilter(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request)
    {
        return null;
    }

    public virtual CefReturnValue OnBeforeResourceLoad(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, IRequestCallback callback)
    {
        callback.Dispose();
        return CefReturnValue.Continue;
    }

    public virtual IResourceHandler GetResourceHandler(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request)
    {
        return null;
    }

    public void OnResourceRedirect(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, IResponse response, ref string newUrl)
    {
            
    }

    public bool OnResourceResponse(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, IResponse response)
    {
        return false;
    }

    public virtual IResponseFilter GetResourceResponseFilter(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, IResponse response)
    {
        return null;
    }

    public virtual void OnResourceLoadComplete(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, IResponse response, UrlRequestStatus status, long receivedContentLength)
    {
            
    }

    public bool OnProtocolExecution(
        IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request)
    {
        return false;
    }
}