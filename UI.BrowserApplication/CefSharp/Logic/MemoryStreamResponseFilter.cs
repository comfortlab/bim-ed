﻿using System;
using System.IO;
using CefSharp;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class MemoryStreamResponseFilter : IResponseFilter
{
    private MemoryStream _memoryStream;

    bool IResponseFilter.InitFilter()
    {
        //NOTE: We could initialize this earlier, just one possible use of InitFilter
        _memoryStream = new MemoryStream();

        return true;
    }

    FilterStatus IResponseFilter.Filter(Stream dataIn, out long dataInRead, Stream dataOut, out long dataOutWritten)
    {
        if (dataIn == null)
        {
            dataInRead = 0;
            dataOutWritten = 0;

            return FilterStatus.Done;
        }

        dataInRead = dataIn.Length;
        dataOutWritten = Math.Min(dataInRead, dataOut.Length);

        //Important we copy dataIn to dataOut
        dataIn.CopyTo(dataOut);

        //Copy data to stream
        dataIn.Position = 0;
        dataIn.CopyTo(_memoryStream);

        return FilterStatus.Done;
    }

    void IDisposable.Dispose()
    {
        // _memoryStream.Dispose();
        // _memoryStream = null;
    }

    public byte[] Data => _memoryStream.ToArray();
}