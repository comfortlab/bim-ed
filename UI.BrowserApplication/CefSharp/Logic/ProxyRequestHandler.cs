﻿using System;
using System.Net;
using CefSharp;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class ProxyRequestHandler : DefaultRequestHandler
{
    private readonly NetworkCredential _credential;

    public ProxyRequestHandler(NetworkCredential credential = null)
    {
        _credential = credential;
    }

    public override bool GetAuthCredentials(
        IWebBrowser chromiumWebBrowser, IBrowser browser,
        string originUrl, bool isProxy, string host, int port, string realm, string scheme,
        IAuthCallback callback)
    {
        if (!isProxy)
            return false;

        if (_credential == null)
            throw new NullReferenceException("credential is null");

        callback.Continue(_credential.UserName, _credential.Password);

        return true;
    }
}