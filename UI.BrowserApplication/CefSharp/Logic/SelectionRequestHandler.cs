using CefSharp;
using CoLa.BimEd.ExtApi.Se.Model.Widget;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class SelectionRequestHandler : DefaultRequestHandler
{
    private readonly IResourceRequestHandler _resourceRequestHandler;
        
    public SelectionRequestHandler(WidgetResult widgetResult, bool isAccessoriesSelector = false)
    {
        _resourceRequestHandler = new SelectionResourceRequestHandler(widgetResult, isAccessoriesSelector);
    }
        
    public override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame,
        IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
    {
        return _resourceRequestHandler;
    }
}