﻿using System.Windows;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class WebSelectorExceptionMessage
{
    public void Send(string message)
    {
        MessageBox.Show(message);
    }
}