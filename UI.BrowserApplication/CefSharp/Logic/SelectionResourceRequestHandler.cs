﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using CefSharp;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.ExtApi.Se.Model.Widget;
using CoLa.BimEd.ExtApi.Se.SerializationUtils;
using CoLa.BimEd.ExtApi.Se.Services;
using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class SelectionResourceRequestHandler : DefaultResourceRequestHandler
{
    // private readonly SettingsController _settingsController;
    private readonly WidgetResult _widgetResult;
    private readonly bool _isAccessoriesSelector;

    private readonly Dictionary<ulong, MemoryStreamResponseFilter> _responseDictionary = new();

    public SelectionResourceRequestHandler(WidgetResult widgetResult, bool isAccessoriesSelector = false)
    {
        _widgetResult = widgetResult;
        // _settingsController = settingsController;
        _isAccessoriesSelector = isAccessoriesSelector;
    }

     public override IResponseFilter GetResourceResponseFilter(
         IWebBrowser browserControl, IBrowser browser,
         IFrame frame, IRequest request, IResponse response)
     {
         if (!IsAdministrationSelectionRequest(request.Url) &&
             !IsConfigurationSelectionRequest(request.Url) &&
             !IsSelectionGuidesRequest(request.Url))
             return null;
             
         var filter = new MemoryStreamResponseFilter();
         _responseDictionary.Add(request.Identifier, filter);
         return filter;
     }

     public override void OnResourceLoadComplete(
         IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request,
         IResponse response, UrlRequestStatus status, long receivedContentLength)
     {
         try
         {
             if (!_responseDictionary.TryGetValue(request.Identifier, out var filter) ||
                 filter == null)
                 return;

             var data = Encoding.UTF8.GetString(filter.Data);

             if (string.IsNullOrEmpty(data))
                 return;

             _responseDictionary.Clear();

             var adminSelection = JsonConvert.DeserializeObject<AdministrationSelection>(data, new SelectorDateTimeConverter());

             if (adminSelection == null)
                 return;

             _widgetResult.Technology = adminSelection.Technology;

             if (!string.IsNullOrEmpty(adminSelection.Result?.Id))
             {
                 _widgetResult.SessionId = adminSelection.Result.Id;
             }
             else
             {
                 AddData(data);
             }

             // Для показа DevTools браузера при отладке
             // browser.ShowDevTools();

             if (_isAccessoriesSelector)
             {
                 frame.ExecuteJavaScriptAsync(@"
                     (function() {
                         function setGroupsStyle(iterationNumber) {
                             if(iterationNumber > 100){
                                 return;
                             }

                             console.info('1-iterationNumber: ' + iterationNumber);

                             let element1 = document.querySelector('md-content.psui-char-content v-accordion.ng-isolate-scope > v-pane.ng-scope');
                     
                             if(!element1) {
                                 setTimeout(function() {
                                     setGroupsStyle(iterationNumber + 1)
                                 }, 200);

                                 return;
                             }

                             console.info('element1:');
                             console.info(element1);

                             element1.style.visibility = 'hidden';
                             element1.style.display = 'none';

                             let element2 = document.querySelector('md-content.ng-scope v-accordion.ng-isolate-scope > v-pane.ng-scope');

                             if(!element2) {
                                 return;
                             }

                             console.info('element2:');
                             console.info(element2);

                             element2.style.visibility = 'hidden';
                             element2.style.display = 'none';
                         };

                         function setButtonStyle(iterationNumber) {
                             if(iterationNumber > 100){
                                 return;
                             }

                             console.info('2-iterationNumber: ' + iterationNumber);

                             let buttonItemsCount = document.querySelector('span.psui-add-to-cart-container.ng-scope > span.psui-action-left-container');
                             let buttonForReset = document.querySelector('div.md-toolbar-tools.psui-title.psui-panel-header > button.psui-action-bar-button.psui-action-bar-button--with-icon.md-button.ng-scope.md-psui-theme.md-ink-ripple');
                     
                             if(!buttonItemsCount || !buttonForReset) {
                                 setTimeout(function() {
                                     setButtonStyle(iterationNumber + 1)
                                 }, 2000);

                                 return;
                             }

                             console.info('buttonItemsCount:');
                             console.info(buttonItemsCount);

                             buttonItemsCount.style.visibility = 'hidden';
                             buttonItemsCount.style.display = 'none';

                             console.info('buttonForReset:');
                             console.info(buttonForReset);

                             buttonForReset.style.visibility = 'hidden';
                             buttonForReset.style.display = 'none';
                         };

                         function setInnerMenuStyle(iterationNumber) {
                             if(iterationNumber > 100){
                                 return;
                             }

                             console.info('3-iterationNumber: ' + iterationNumber);

                             let buttonInnerMenu = document.querySelector('div.md-toolbar-tools.psui-show-solution > md-menu.md-menu.ng-scope._md');

                             if(!buttonInnerMenu) {
                                 setTimeout(function() {
                                     setInnerMenuStyle(iterationNumber + 1)
                                 }, 2000);

                                 return;
                             }

                             console.info('buttonInnerMenu:');
                             console.info(buttonInnerMenu);

                             buttonInnerMenu.style.visibility = 'hidden';
                             buttonInnerMenu.style.display = 'none';
                         };

                         function setProductCodeMenuStyle(iterationNumber) {
                             if(iterationNumber > 100){
                                 return;
                             }

                             console.info('4-iterationNumber: ' + iterationNumber);

                             let productCodeMenu = document.querySelector('psui-solution-panel.psui-flex.psui-full-width.psui-full-height.ng-isolate-scope  div.psui-codification.psui-editable-form.psui-padding-sm.psui-no-padding-right.psui-position-relative.ng-scope.layout-align-center-center.layout-row');

                             if(!productCodeMenu) {
                                 setTimeout(function() {
                                     setProductCodeMenuStyle(iterationNumber + 1)
                                 }, 2000);

                                 return;
                             }

                             console.info('productCodeMenu:');
                             console.info(productCodeMenu);

                             productCodeMenu.style.visibility = 'hidden';
                             productCodeMenu.style.display = 'none';
                         };

                         function removeButtonDisabledClass(iterationNumber) {
                             if(iterationNumber > 100){
                                 return;
                             }

                             console.info('0-iterationNumber: ' + iterationNumber);

                             let button = document.querySelector('.psui-show-solution button.psui-action-bar-button.md-button.ng-scope');
                     
                             if(!button) {
                                 setTimeout(function() {
                                     removeButtonDisabledClass(iterationNumber + 1)
                                 }, 2000);

                                 return;
                             }

                             console.info('button:');
                             console.info(button);

                             console.info('button.classList:');
                             console.info(button.classList);

                             button.classList.remove('psui-action-bar-button--disabled');

                             console.info(button.classList);
                         };

                         removeButtonDisabledClass(1);
                         setGroupsStyle(1);
                         setButtonStyle(1);
                         setInnerMenuStyle(1);
                         setProductCodeMenuStyle(1);
                     }());
                     ");
             }
         }
         catch (Exception exception)
         {
             Debug.WriteLine(exception);
         }
     }

    private void AddData(string data)
    {
        var deserializedData = JsonConvert.DeserializeObject<EzSelectorData>(data);

        if (string.IsNullOrEmpty(deserializedData.GlobalId))
            return;

        var mainKey = _widgetResult.Data.Keys.FirstOrDefault();

        if (mainKey != null && _widgetResult.Data.Any(kv => kv.Key.Contains(mainKey)) && !deserializedData.GlobalId.Contains(mainKey))
            _widgetResult.Data.Clear();

        if (_widgetResult.Data.ContainsKey(deserializedData.GlobalId))
            _widgetResult.Data[deserializedData.GlobalId] = deserializedData;
        else
            _widgetResult.Data.Add(deserializedData.GlobalId, deserializedData);
    }

    private static bool IsAnyBaseRequest(string requestUrl)
    {
        return requestUrl.StartsWith(
            $"{SeApi.EndPoints.Base}",
            StringComparison.InvariantCultureIgnoreCase);
    }

    private static bool IsAdministrationSelectionRequest(string requestUrl)
    {
        return requestUrl.StartsWith(
            $"{SeApi.EndPoints.AdministrationSelection}",
            StringComparison.InvariantCultureIgnoreCase);
    }

    private static bool IsConfigurationSelectionRequest(string requestUrl)
    {
        return requestUrl.StartsWith(
            $"{SeApi.EndPoints.ConfigurationsSelection}",
            StringComparison.InvariantCultureIgnoreCase);
    }

    private static bool IsSelectionGuidesRequest(string requestUrl)
    {
        return requestUrl.StartsWith(
            $"{SeApi.EndPoints.SelectionGuidesSearchById}",
            StringComparison.InvariantCultureIgnoreCase);
    }
}