﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using CefSharp;
using CoLa.BimEd.Infrastructure.Framework.Application;
using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;

public class CookieStorage
{
    private readonly string _path;
        
    public CookieStorage(AppPaths appPaths)
    {
        _path = appPaths.CookieStore;
    }
    
    public void ReadCookies(object sender, FrameLoadEndEventArgs e)
    {
        try
        {
            var filePaths = Directory.GetFiles(_path);
            var cookieManager = Cef.GetGlobalCookieManager();

            foreach (var filePath in filePaths)
            {
                ReadCookieFile(cookieManager, filePath);
            }
            
            var cookieVisitor = new CookieVisitor();
            cookieVisitor.SendCookie += CookieVisitorOnSendCookie;
            cookieManager.VisitAllCookies(cookieVisitor);
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
        }
    }

    private static void ReadCookieFile(ICookieManager cookieManager, string filePath)
    {
        using var reader = new StreamReader(filePath);
        var cookieString = reader.ReadToEnd();

        try
        {
            cookieString = RepairCookieString(cookieString);

            if (string.IsNullOrEmpty(cookieString))
                return;

            var cookie = JsonConvert.DeserializeObject<global::CefSharp.Cookie>(cookieString);
            cookieManager.SetCookie("https://" + cookie.Domain, cookie);
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
            Debug.WriteLine($"cookieString: {cookieString}");
        }
    }

    private static string RepairCookieString(string cookieString)
    {
        if (string.IsNullOrWhiteSpace(cookieString))
            return null;
            
        var b1 = cookieString.Count(n => n == '{');
        var b2 = cookieString.Count(n => n == '}');

        if (b1 != b2)
            return null;

        var i1 = cookieString.IndexOf('{');
        var i2 = cookieString.LastIndexOf('}');

        return cookieString.Substring(i1, i2 - i1 + 1);
    }

    private void CookieVisitorOnSendCookie(global::CefSharp.Cookie cookie)
    {
        try
        {
            var cookieString = JsonConvert.SerializeObject(cookie);
            var fileName = Path.Combine(_path, $"{cookie.Name}.cookie");

            if (File.Exists(fileName) == false)
                return;

            using var file = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
            using var cookieWriter = new StreamWriter(file, Encoding.UTF32);
                
            cookieWriter.WriteLine(cookieString);
            cookieWriter.Close();
        }
        catch (Exception exception)
        {
            Debug.WriteLine(exception);
        }
    }

    private class CookieVisitor : ICookieVisitor
    {
        public event Action<global::CefSharp.Cookie> SendCookie;

        public bool Visit(global::CefSharp.Cookie cookie, int count, int total, ref bool deleteCookie)
        {
            // deleteCookie = false;

            SendCookie?.Invoke(cookie);

            return true;
        }

        public void Dispose()
        {

        }
    }

    public void DeleteCookie()
    {
        var cookies = Directory.GetFiles(_path).Select(n => new FileInfo(n));

        foreach (var cookie in cookies)
            cookie.Delete();
    }
}