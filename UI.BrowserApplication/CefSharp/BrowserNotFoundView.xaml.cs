﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace CoLa.BimEd.UI.BrowserApplication.CefSharp;

public partial class BrowserNotFoundView
{
    public BrowserNotFoundView()
    {
        InitializeComponent();
    }

    private void Hyperlink_OnRequestNavigate(object sender, RoutedEventArgs e)
    {
        if (e.OriginalSource is Hyperlink hyperlink && hyperlink.NavigateUri != null)
            Process.Start(hyperlink.NavigateUri.ToString());

        var parent = VisualTreeHelper.GetParent(this);

        while (parent != null)
        {
            if (parent is Window window)
                window.Close();
            
            parent = VisualTreeHelper.GetParent(parent);
        }
    }

    private void ContentElement_OnPreviewMouseMove(object sender, MouseEventArgs e)
    {
        if (sender is Hyperlink hyperlink && hyperlink.NavigateUri != null)
            StatusBar.Text = hyperlink.NavigateUri.ToString();
    }

    private void ContentElement_OnMouseLeave(object sender, MouseEventArgs e)
    {
        StatusBar.Text = string.Empty;
    }
}