﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using CoLa.BimEd.ExtApi.Se.Communication;
using CoLa.BimEd.ExtApi.Se.Services;
using CoLa.BimEd.Infrastructure.Framework.Application;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Logs;
using CoLa.BimEd.Infrastructure.Framework.Net;
using CoLa.BimEd.Infrastructure.Framework.Net.Proxy;
using CoLa.BimEd.Infrastructure.Framework.Utils;
using CoLa.BimEd.Infrastructure.ProgressBar;
using CoLa.BimEd.UI.BrowserApplication.Auth;
using CoLa.BimEd.UI.BrowserApplication.CefSharp;
using CoLa.BimEd.UI.BrowserApplication.CefSharp.Logic;
using CoLa.BimEd.UI.BrowserApplication.Windows;
using Localization = CoLa.BimEd.Infrastructure.Framework.Localization;

namespace CoLa.BimEd.UI.BrowserApplication;

public partial class App
{
    public const string AppName = "BIM ED: Browser Application";
    private readonly IServiceProvider _serviceProvider;

    public App()
    {
        var serviceCollection = new ServiceCollection();
        
        serviceCollection.AddSingleton(_ => new AppInfo("BIM Electrical Design"));
        serviceCollection.AddSingleton(x => new AppPaths(x.Get<AppInfo>().Name, AssemblyUtils.GetExecutingAssemblyPath()));
        serviceCollection.AddSingleton(x => new AuthService(x.Get<SeApiHttpClient>(), x.Get<AppPaths>()));
        serviceCollection.AddSingleton<CefInitializer>();
        serviceCollection.AddSingleton<CefSharpBrowser>();
        serviceCollection.AddSingleton<CookieStorage>();
        serviceCollection.AddSingleton<HttpClientAccessor>();
        serviceCollection.AddSingleton(_ => new Localization(CultureInfo.CurrentCulture.Name));
        serviceCollection.AddSingleton<Logger>();
        serviceCollection.AddSingleton<ProgressBarController>();
        serviceCollection.AddSingleton<ProxyStorage>();
        serviceCollection.AddSingleton<SeApi.Selectors>();
        serviceCollection.AddSingleton<SeApiHttpClient>();
        serviceCollection.AddSingleton<SelectorConverters>();
        
        _serviceProvider = DI.ServiceProvider = serviceCollection.BuildServiceProvider();
        _serviceProvider.Get<CefInitializer>().Initialize();
    }

    private void ApplicationStartup(object sender, StartupEventArgs e)
    {
        try
        {
            if (!CheckForArguments(e.Args))
                return;
            
            ShutdownMode = ShutdownMode.OnLastWindowClose;

            var mode = e.Args[0];
            var content = e.Args.Length > 1 ? e.Args[1] : null; 
            
            switch ((BrowserStartupMode)int.Parse(mode))
            {
                case BrowserStartupMode.Login:
                    AuthenticationWindow.LogIn("BIM_ED", () => { MessageBox.Show("LOGIN: OK"); }, _serviceProvider);
                    break;
                
                case BrowserStartupMode.Logout:
                    AuthenticationWindow.LogOut("BIM_ED", () => { MessageBox.Show("LOGOUT: OK"); }, _serviceProvider);
                    break;
                
                case BrowserStartupMode.RefreshToken:
                case BrowserStartupMode.ChangedPassword:
                    break;
                    
                case BrowserStartupMode.SelectorEmpty:
                    SelectAndConfigWindow.CreateBySelectorId(
                            content,
                            close: () => Current.Shutdown(200))
                        .ShowDialog();
                    break;
                
                case BrowserStartupMode.SelectorConfigured:
                    SelectAndConfigWindow.CreateBySelectorConfig(
                            content?.Replace('·', ' ').Replace('`', '\"'),
                            close: () => Current.Shutdown(200))
                        .ShowDialog();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        catch (Exception exception)
        {
            ShowError(exception);
            Current.Shutdown(-1);
        }
    }

    private static bool CheckForArguments(IReadOnlyCollection<string> args)
    {
        if (args.Count > 0)
            return true;
        
        ShowError(
            "Wrong command-line args.\n" +
            "Application must be called with parameters.");

        Current.Shutdown(-1);

        return false;
    }

    public static void ShowError(string message) => MessageBox.Show(
        message, AppName, MessageBoxButton.OK, MessageBoxImage.Error);
    
    public static void ShowError(Exception exception) => MessageBox.Show(
        $"{exception.Message}\n{exception.StackTrace}", AppName, MessageBoxButton.OK, MessageBoxImage.Error);
}