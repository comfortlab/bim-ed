﻿using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;

namespace CoLa.BimEd.UI.BrowserApplication.Utils;

public static class HwidUtils
{
    public static string GetHwid()
    {
        var idsCombined = GetCpuId() + GetBaseBoardId() + GetMacAddress();

        byte[] hash;
        using (var md5 = MD5.Create())
        {
            hash = md5.ComputeHash(Encoding.UTF8.GetBytes(idsCombined));
        }

        var stringBuilder = new StringBuilder();

        foreach (var symbol in hash)
        {
            stringBuilder.Append(symbol.ToString("x2"));
        }

        return stringBuilder.ToString();
    }
    private static string GetCpuId()
    {
        try
        {
            return new ManagementObjectSearcher("SELECT ProcessorID FROM Win32_processor").Get()
                .Cast<ManagementObject>().FirstOrDefault()?["ProcessorID"].ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    private static string GetBaseBoardId()
    {
        try
        {
            return new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard").Get()
                .Cast<ManagementObject>().FirstOrDefault()?["SerialNumber"].ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    private static string GetMacAddress()
    {
        try
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(n => n.OperationalStatus == OperationalStatus.Up)?.GetPhysicalAddress().ToString();
        }
        catch
        {
            return string.Empty;
        }
    }
}