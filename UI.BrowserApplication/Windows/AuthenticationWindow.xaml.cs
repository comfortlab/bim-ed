﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CoLa.BimEd.Infrastructure.Framework.DependencyInjection;
using CoLa.BimEd.Infrastructure.Framework.Net;
using CoLa.BimEd.UI.BrowserApplication.Auth;
using CoLa.BimEd.UI.BrowserApplication.CefSharp;

namespace CoLa.BimEd.UI.BrowserApplication.Windows;

public partial class AuthenticationWindow
{
    private readonly string _applicationKey;
    private readonly AuthMode _authMode;
    private readonly AuthService _authService;
    private readonly CefSharpBrowser _browser;

    public static void LogIn(string applicationKey, Action authCallback, IServiceProvider serviceProvider) =>
        new AuthenticationWindow(applicationKey, authCallback, serviceProvider, AuthMode.LogIn).ShowDialog();

    public static void LogOut(string applicationKey, Action authCallback, IServiceProvider serviceProvider) =>
        new AuthenticationWindow(applicationKey, authCallback, serviceProvider, AuthMode.LogOut).ShowDialog();

    public static void ChangePassword(string applicationKey, Action authCallback, IServiceProvider serviceProvider) =>
        new AuthenticationWindow(applicationKey, authCallback, serviceProvider, AuthMode.ChangePassword).ShowDialog();

    private AuthenticationWindow(string applicationKey, Action authCallback, IServiceProvider serviceProvider,
        AuthMode authMode)
    {
        _applicationKey = applicationKey;
        _authMode = authMode;
        _authService = serviceProvider.Get<AuthService>();

        AuthCallback = authCallback;
        WindowStartupLocation = WindowStartupLocation.CenterScreen;
        Icon = BitmapSource.Create(1, 1, 96, 96, PixelFormats.Bgra32, null, new byte[] { 0, 0, 0, 0 }, 4);

        InitializeComponent();
            
        try
        {
            BrowserControl.Content = _browser = serviceProvider.Get<CefSharpBrowser>();
            // Debug.Text += $"InitializeComponent() - DONE\n";
            Debug.Text += $"ApplicationKey: {_applicationKey}\n";
            Debug.Text += $"AuthMode: {_authMode}\n";
            Debug.Text += $"ChromiumWebBrowser: {_browser.ChromiumWebBrowser}\n";
            Start();
        }
        catch (Exception exception)
        {
            Debug.Text += $"{exception.Message}\n{exception.StackTrace}\n";
            // Logger.Warn(exception);
        }
    }

    public Action AuthCallback { get; set; }

    private void Start()
    {
        _browser.LoadEndAction += LoadHandler;

        switch (_authMode)
        {
            case AuthMode.LogIn:
            case AuthMode.ChangePassword:
                _browser.LoadEndAction += GetCode;
                break;

            case AuthMode.LogOut:
                _authService.ResetToken();
                _browser.LoadEndAction += CallbackAndClose;
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(_authMode), _authMode, null);
        }

        var address = _authService.GetAuthenticationUri(_authMode);

        _browser.Address = address;
    }

    private async void GetCode() => await GetCodeTask();

    private async Task GetCodeTask()
    {
        try
        {
            switch (_authMode)
            {
                case AuthMode.LogIn:
                    if (TryGetCodeFromAddress(out var code) &&
                        await _authService.Request(_applicationKey, RequestMode.Authenticate, code))
                        CallbackAndClose();
                    return;

                case AuthMode.ChangePassword:
                    _browser.Address = _authService.GetAuthenticationUri(AuthMode.ChangePassword);
                    return;

                case AuthMode.LogOut:
                    CallbackAndClose();
                    return;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        catch (Exception exception)
        {
            // Logger.Error(exception);
            File.AppendAllLines("log.txt", new [] { exception.Message, exception.StackTrace });
            Close();
        }
    }

    private bool TryGetCodeFromAddress(out string code)
    {
        code = _browser.Address.OriginalString
            .Split('&', '?')
            .FirstOrDefault(n => n.StartsWith("code="))?
            .Replace("code=", string.Empty);
            
        Debug.Text += $"code: {code}\n"; 

        return !string.IsNullOrWhiteSpace(code);
    }

    private void CallbackAndClose()
    {
        try
        {
            AuthCallback?.Invoke();
            AuthCallback = null;
        }
        catch (IOException exception)
        {
            File.AppendAllLines("log.log", new [] { exception.Message, exception.StackTrace });
            // Logger.Warn(exception.Message);
        }
        finally
        {
            Close();
        }
    }

    private void LoadHandler()
    {
        Debug.Text += $"Address: {_browser.Address.OriginalString}\n";

        if (_browser.Address.OriginalString.ToLower().EndsWith("idmserrorfound"))
        {
            Close();
        }

        else // if (_browser.Address.OriginalString.ToLower().Contains("remoteaccessauthorizationpage"))
        {
            // LoadingProgressRing.Visibility = Visibility.Collapsed;
            BrowserControl.Visibility = Visibility.Visible;
        }
    }
}