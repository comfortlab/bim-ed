﻿using System;
using System.Windows;
using CoLa.BimEd.UI.BrowserApplication.ViewModels;
using CoLa.BimEd.UI.Framework.Utils;

namespace CoLa.BimEd.UI.BrowserApplication.Windows;

public partial class SelectAndConfigWindow
{
    public static Window CreateBySelectorId(string selectorId, Action close) => new SelectAndConfigWindow
    {
        DataContext = MainViewModel.CreateBySelectorId(selectorId, close)
    };
    
    public static Window CreateBySelectorConfig(string selectorId, Action close) => new SelectAndConfigWindow
    {
        DataContext = MainViewModel.CreateBySelectorConfig(selectorId, close)
    };

    public SelectAndConfigWindow()
    {
        InitializeComponent();
        this.SetPositionAndSizes(0.9);
    }
}