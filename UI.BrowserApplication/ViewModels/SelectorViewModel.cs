﻿using System.Windows.Media;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.Infrastructure.Framework.Utils;

namespace CoLa.BimEd.UI.BrowserApplication.ViewModels;

public class SelectorViewModel
{
    public Selector Selector { get; set; }
    public string Id { get; set; }
    public ImageSource Image { get; set; }
    public SelectorViewModel(Selector selector)
    {
        Selector = selector;
        Id = Selector.Id;
        Image = selector.Image.ToBitmapSource();
    }
}