﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using CoLa.BimEd.ExtApi.Se.Model.Selectors;
using CoLa.BimEd.UI.Framework.MVVM;
using Newtonsoft.Json;

namespace CoLa.BimEd.UI.BrowserApplication.ViewModels;

public class MainViewModel : ViewModel
{
    public static MainViewModel CreateBySelectorId(string selectorId, Action close) => new()
    {
        SelectorId = selectorId,
        Close = close,
    };
    
    public static MainViewModel CreateBySelectorConfig(string selectorConfig, Action close) => new()
    {
        SelectorConfig = selectorConfig,
        Close = close,
    };

    public MainViewModel()
    {
        CustomComponent1 = new TestCustomViewModel1();
        CustomComponent2 = new TestCustomViewModel2();
    }

    public ViewModel CustomComponent1 { get; set; }
    public ViewModel CustomComponent2 { get; set; }
    public string SelectorId { get; set; }
    public string SelectorConfig { get; set; }
    public Action<SelectorConfig> SelectorCallback => selectorConfig =>
    {
        try
        {
            if (!Console.IsOutputRedirected)
            {
                Debug.WriteLine("ERROR Client: {0}", "standard output must be redirected");
            }
            else
            {
                using var standardOutputStream = Console.OpenStandardOutput();
                using var streamWriter = new StreamWriter(standardOutputStream, Encoding.UTF8);
                var selectorConfigJson = JsonConvert.SerializeObject(selectorConfig); // может jsonStreamWriter использовать?
                streamWriter.WriteLine(selectorConfigJson);
                streamWriter.Flush();
            }
        }
        catch (Exception exception)
        {
            App.ShowError(exception);
        }
        finally
        {
            Close?.Invoke();
        }
    };
}